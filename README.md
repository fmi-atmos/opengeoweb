[![pipeline status](https://gitlab.com/opengeoweb/opengeoweb/badges/master/pipeline.svg)](https://gitlab.com/opengeoweb/opengeoweb/-/commits/master)
[![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/package.json)](https://gitlab.com/opengeoweb/opengeoweb/-/tags)
[![coverage report](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg)](https://gitlab.com/opengeoweb/opengeoweb/-/commits/master)

# Opengeoweb

Nx workspace for geoweb products. This project was generated using [Nx](https://nx.dev).
This workspace contains the following products:

- geoweb: React frontend application (apps/geoweb)
- api: React component library (libs/api) - published to NPM as [@opengeoweb/api](https://www.npmjs.com/package/@opengeoweb/api)
- authentication: React component library (libs/authentication)
- cap: React component library (libs/cap)
- core: React component library (libs/core) - published to NPM as [@opengeoweb/core](https://www.npmjs.com/package/@opengeoweb/core)
- form-fields: React component library (libs/form-fields) - published to NPM as [@opengeoweb/form-fields](https://www.npmjs.com/package/@opengeoweb/form-fields)
- workspace: React component library (libs/workspace) - published to NPM as [@opengeoweb/workspace](https://www.npmjs.com/package/@opengeoweb/workspace)
- shared: React component library (libs/shared) - published to NPM as [@opengeoweb/shared](https://www.npmjs.com/package/@opengeoweb/shared)
- sigmet-airmet: React component library (libs/sigmet-airmet) - published to NPM as [@opengeoweb/sigmet-airmet](https://www.npmjs.com/package/@opengeoweb/sigmet-airmet)
- spaceweather: React component library (libs/spaceweather) - published to NPM as [@opengeoweb/spaceweather](https://www.npmjs.com/package/@opengeoweb/spaceweather)
- taf: React component library (libs/taf) - published to NPM as [@opengeoweb/taf](https://www.npmjs.com/package/@opengeoweb/taf)
- theme: React component library (libs/theme) - published to NPM as [@opengeoweb/theme](https://www.npmjs.com/package/@opengeoweb/theme)
- timeseries: React component library (libs/timeseries) - published to NPM as [@opengeoweb/timeseries](https://www.npmjs.com/package/@opengeoweb/timeseries)
- webmap: React component library (libs/webmap) - published to NPM as [@opengeoweb/webmap](https://www.npmjs.com/package/@opengeoweb/webmap)
- webmap-react: React component library (libs/webmap-react) - published to NPM as [@opengeoweb/webmap-react](https://www.npmjs.com/package/@opengeoweb/webmap-react)

Opengeoweb dependency graph - to see an interactive graph, see [Generating a dependency graph](#generating-a-dependency-graph)

![dep-graph](./assets/depGraph.png)

## Content

[[_TOC_]]

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Installation

### Setup project

To run the opengeoweb commands you need both NPM and Nx. The easiest way to install NPM is via nvm. Please visit https://nvm.sh/ and follow the instructions. When nvm is installed, run the following commands to install NPM and Nx:

```
nvm install 20
npm install -g nx
npm install -g typescript@4.1.4
```

NOTES:

1. Minimum node version is 20!
1. If snapshots don't work and you are on a newer version of node try downgrading to node 20: `nvm use 20`.
1. If you get "command not found: nx": it was probably not installed for the node version you are using at that moment. Run `npm install -g nx` again.

After NPM installation clone the repo on your local machine and install the dependencies.

```
git clone https://gitlab.com/opengeoweb/opengeoweb.git
cd opengeoweb/
npm ci
```

## Configuration

Follow these optional steps to configure backend base URLs and other GeoWeb application settings.

1. Copy the [apps/geoweb/src/assets/config.example.json](apps/geoweb/src/assets/config.example.json) file and rename it to `config.json`
2. Fill in the correct values in the `config.json` file.

If the `config.json` file is not provided, default values will be used instead.

For a complete list of all supported keys, refer to the `ConfigType` type in [apps/geoweb/src/app/utils/loadConfig.ts](apps/geoweb/src/app/utils/loadConfig.ts). Be aware that some values depend on each other, such as the `requiredConfigKeysForAuthentication` constant.

### Authentication configuration

The GeoWeb application uses OpenID Connect (OIDC) for authentication and a role-based access control (RBAC) system for authorization. Authentication is configured using the following keys (all are required):

- `GW_AUTH_CLIENT_ID`: The client ID registered with the OIDC provider.
- `GW_AUTH_LOGIN_URL`: The OIDC provider URL to redirect users for login.
- `GW_AUTH_LOGOUT_URL`: The URL to redirect users after logout.
- `GW_AUTH_TOKEN_URL`: The URL to obtain the tokens.
- `GW_APP_URL`: The base URL of the GeoWeb application to which users are redirected after authentication by the identity provider.

### Roles configuration

The GeoWeb application uses roles in conjunction with authentication to control access to various features and functionalities. Roles are configured using the following keys:

- `GW_AUTH_ROLE_CLAIM_NAME`: The name of the claim in the JWT ID token that specifies the user's roles. This claim can be, for example, `roles` or `groups`, depending on the identity provider in use.
- `GW_AUTH_ROLE_CLAIM_VALUE_PRESETS_ADMIN`: The claim value in the JWT ID token that assigns the user the Preset-admin role. This value should match one of the entries in the list of user roles or security groups, depending on the identity provider in use.

#### Available Roles in GeoWeb application

- **User**: Access to all basic features of the application.
- **Preset-admin**: Provides elevated privileges, including the ability to perform administrative tasks related to system presets.

### Presets configuration

To configure the Presets, use the following keys:

- `GW_PRESET_BACKEND_URL`: The base URL for the Presets backend.
- `GW_INITIAL_WORKSPACE_PRESET`: The initial workspace preset to be loaded when the application starts (optional).
- `GW_INITIAL_PRESETS_FILENAME`: The filename of the initial presets configuration file. This JSON file should contain a list of available base layers, over layers (`layers`) and base services (`baseServices`) for Layer Manager, a list of WMS services for Layer Select (`services`), services for the Time Series Manager (`timeSeriesServices`) and default map settings for the New workspace preset (`defaultMapSettings`). Refer to the default configuration file located at [apps/geoweb/src/assets/initialPresets.json](apps/geoweb/src/assets/initialPresets.json). The structure of this file should conform to the [InitialAppPresetProps](https://opengeoweb.gitlab.io/opengeoweb/docs/core/interfaces/InitialAppPresetProps.html) type.

For detailed information on the presets data model, refer to the following documentation:

- [Workspace-presets Data Model](https://gitlab.com/opengeoweb/backend-services/presets-backend/-/blob/main/docs/w-preset-data-model.md)
- [View-presets Data Model](https://gitlab.com/opengeoweb/backend-services/presets-backend/-/blob/main/docs/v-preset-data-model.md)

For more details, refer to the [Presets Backend API Documentation](https://gitlab.com/opengeoweb/backend-services/presets-backend#geoweb-presets-backend)

### Sigmet and Airmet configuration

To configure the Sigmet and Airmet modules, use the following keys:

- `GW_SIGMET_BASE_URL`: The base URL for the Opmet backend.
- `GW_AIRMET_BASE_URL`: The base URL for the Opmet backend.

For more details, refer to the [Opmet Backend API Documentation](https://gitlab.com/opengeoweb/backend-services/opmet-backend#opmet-backend).

### TAF configuration

To configure the base URL for the TAF backend, use the following key:

- `GW_TAF_BASE_URL`: The base URL for the TAF backend.

For more details, refer to the [Aviation TAF Backend API Documentation](https://gitlab.com/opengeoweb/backend-services/aviation-taf-backend#aviation-taf-backend)

### CAP configuration

To configure the CAP module, use the following keys:

- `GW_CAP_BASE_URL`: The base URL for the CAP backend.
- `GW_CAP_CONFIGURATION_FILENAME`: The filename of the CAP configuration file. This JSON file should contain settings specific to the CAP module, such as poll intervals and feed addresses. Refer to the default configuration file located at [apps/geoweb/src/assets/capWarningPresets.json](apps/geoweb/src/assets/capWarningPresets.json) for an example.

For more details, refer to the [CAP Backend API Documentation](https://gitlab.com/opengeoweb/backend-services/cap-backend#cap-backend).

### Warnings configuration

To configure the base URL for the Warnings backend, use the following key:

- `GW_DRAWINGS_BASE_URL`: The base URL for the Warnings backend.

For more details, refer to the [GeoWeb Warnings Backend](https://gitlab.com/opengeoweb/backend-services/warnings-backend#geoweb-warnings-backend)

### Timeseries configuration

To configure the timeseries module, use the following key:

- `GW_TIMESERIES_CONFIGURATION_FILENAME`: The filename of the timeseries configuration file. This JSON file should contain settings specific to the timeseries module, such as preset locations for time series data. Refer to the default configuration file located at [apps/geoweb/src/assets/timeSeriesPresetLocations.json](apps/geoweb/src/assets/timeSeriesPresetLocations.json) for an example.

## Running

GeoWeb can be run in several ways:

### Running GeoWeb application with NX

After completing the installation and configuration steps, you can start the GeoWeb application by running `nx serve` or `npm run start`.

The `nx serve` command is used to start a development server for a specific project within the Nx workspace. This command compiles the project, watches for changes, and serves the GeoWeb application.

### Build and run with docker

Build Docker image with `docker build -t gw-test .`

Run Docker image with `docker run -it -p 8080:8080 gw-test`. Visit http://localhost:8080 to access the GeoWeb application.

The GeoWeb Docker container can be configured using environment variables prefixed with `GW_`. These variables are reflected in the config.json of the application. You can check the current configuration at http://localhost:8080/assets/config.json.

Run Docker image with environment variable `docker run -it -p 8080:8080 -e GW_TEST="helloworld" gw-test` and visit http://localhost:8080.

#### Docker container image registries

In some cases, a Docker image of the development branch of OpenGeoWeb is needed. There is a manual job in the publish stage of the CI/CD pipeline for development branches, called `docker-images-dev`. This job creates a Docker image and pushes it to the [OpenGeoWeb GitLab registry](https://gitlab.com/opengeoweb/opengeoweb/container_registry/3075858). Note that the Docker image for the development branch is only pushed to the GitLab registry and not to Docker Hub.

From new releases (tags), the Docker image is also pushed to [Docker Hub](https://hub.docker.com/r/opengeoweb/geoweb).

### Build and serve static files

Build and generate a bundle for the GeoWeb application with the following command `rm -rf dist && npm run geoweb:build`
This will create the bundle in the `./dist/apps/geoweb` directory.

Start a local HTTP server to serve the static files using the following command `npx serve -p 5400 -s dist/apps/geoweb`

### Running storybooks

Storybook is a tool for developing and testing UI components in isolation. It can be used to visualize the different states of the components and to interact with them in a sandbox environment. You can run storybooks from different libraries using the following commands:

```
nx run core:storybook
nx run form-fields:storybook
nx run sigmet-airmet:storybook
nx run spaceweather:storybook
nx run taf:storybook
nx run shared:storybook
nx run cap:storybook
nx run authentication:storybook
nx run layer-select:storybook
nx run metronome:storybook
nx run snackbar:storybook
nx run theme:storybook
nx run timeseries:storybook
nx run timeslider:storybook
nx run timesliderlite:storybook
nx run warnings:storybook
nx run webmap-react:storybook
nx run workspace:storybook
```

Each command starts a Storybook instance for a specific module. This enables you to develop and test components independently, ensuring they work correctly before integrating them into the main application.

## Testing

### Running browser test

To run all the cypress tests locally (automatically starts up the corresponding application):

```
nx e2e geoweb-e2e
```

To keep the Cypress GUI open, add the watch flag:

```
nx e2e geoweb-e2e --watch
```

### Unit tests

- More info about testing React components [react-testing-library](https://testing-library.com/docs/react-testing-library/intro/)
- More info about testing React hooks [react-testing-library/renderHook](https://testing-library.com/docs/react-testing-library/api#renderhook)

#### Running unit tests

Run `nx test <lib-name>` to execute the unit tests for a single library.

Run `npm run test:all` to execute the unit tests from all libraries affected by a change.

Run `nx test <lib-name> --test-file <file-pattern>` to execute a specific unit test

### Image snapshot testing

Image snapshot tests verify that the visual appearance of a rendered view remains unchanged. This is specifically useful for components that are hard to unit test otherwise, such as canvas components. If a view is deliberately changed, new snapshots must be created. Up-to-date snapshots should be committed to source control. We run snapshots using Storybook Test-Runner for the following libraries:

- core
- form-fields
- layer-select
- shared
- sigmet-airmet
- spaceweather
- taf
- theme
- timeseries
- timeslider
- timesliderlite
- warnings
- webmap-react
- workspace

In below instructions, replace `<libname>` with `core`, `form-fields`, `layer-select`, `shared`, `sigmet-airmet`, `spaceweather`, `taf`, `theme`, `timeseries`, `timeslider`, `timesliderlite`, `warnings`, `webmap-react` or `workspace` accordingly.

#### Setup snapshot testing for a library

1. Add the following target to the `project.json` of the library and replace all mentions of `<libname>` by the name of the lib.

```
"snapshot": {
  "executor": "nx:run-commands",
  "options": {
    "env": {
      "SNAPSHOT_OUTPUT": "libs/<libname>/src/lib/__image_snapshots__"
    },
    "commands": [
      "npx test-storybook -c libs/<libname>/.storybook --url=http://localhost:6007/<libname>"
    ]
  },
  "configurations": {
    "ci": {
      "commands": [
        "nx run <libname>:static-storybook &",
        "npx wait-on tcp:6007 && npx test-storybook -c libs/<libname>/.storybook --url=http://localhost:6007"
      ],
      "parallel": false
    }
  },
  "dependsOn": ["build-storybook"]
},
"static-storybook": {
  "executor": "@nx/web:file-server",
  "options": {
    "port": 6007,
    "buildTarget": "<libname>:build-storybook",
    "staticFilePath": "dist/storybook/<libname>"
  },
  "configurations": {
    "ci": {
      "buildTarget": "<libname>:build-storybook:ci"
    }
  }
},
```

2. Add the following files to the `.storybook` folder of the library:

test-runner-jest.config.js:

```
module.exports = require('../../../.storybook/test-runner-jest.config');
```

test-runner.js:

```
module.exports = require('../../../.storybook/test-runner');
```

3. Add the npm snapshot tasks to `package.json`, for both testing and updating snapshots

```
// package.json
{
  "test:image-snap-<libname>": "nx run <libname>:snapshot",
  "test:image-snap-<libname>-update": "nx run <libname>:snapshot -u",
}
```

4. The snapshot command will only create snapshots for stories containing the 'snapshot' tag.
   Here is an example how you add the 'snapshot' tag to a story:

```
SwitchLight.storyName = 'Switch light theme';
SwitchLight.tags = ['snapshot'];
```

By default the snapshots will be clipped to the bounding box of the element. This will fail when the element has position fixed or absolute. As a fallback you add the 'fullPageSnapshot' tag to create a snapshot of the entire page.

During snapshot creation the url of the storybook iframe will include the '#snapshot' hash which you can use to change the behaviour of the test.

#### Running snapshot tests and updating snapshots locally

1. You need to have [docker](https://docs.docker.com/get-docker/) installed and running.
2. Start Chromium by running: `npm run start-snapshot-server`. (This will start a docker container with chromium, to run snapshot tests in. We need this to make sure everyone gets the same snapshot results.)
3. Run the snapshot tests: `npm run test:image-snap-<libname>`. This will first create a new static storybook build and then run the tests.
4. If a snapshot test fails, you can find and inspect the differences in `libs/<libname>/src/lib/__image_snapshots__/__diff_output__/`.
5. To update the snapshots, run `npm run test:image-snap-<libname>-update`. Snapshots are saved under `libs/<libname>/src/lib/__image_snapshots__/`. Make sure to commit the new snapshots.
6. Stop Chromium by running: `npm run stop-snapshot-server`.

Tricks if snapshots are failing locally:

- try stopping and restarting the docker snapshot-server container.
- If you have deleted storybooks from the `dist/` folder manually, or wish to force storybooks to rebuild, you can run `nx reset`. If you notice your snapshots are blank, this may be the cause.

#### Tips and tricks for creating snapshots

- Don't use React.Fragments as a container for your component when using snapshots.

```javascript
// don't
export const MyComponent = (): React.ReactElement => (
  <>
    <p>test</p>
    <span>testing</span>
  </>
);

// do
export const MyComponent = (): React.ReactElement => (
  <div>
    <p>test</p>
    <span>testing</span>
  </div>
);
```

- Web elements that look different in different browsers (like scrollbars) can cause issues. Hide the elements for the snapshot story
- Make sure the snapshot is only showing the component that you want to test. Sometimes storybook examples have some additional description and that's perfect for developers but not so good for snapshot tests. Consider exporting the demo part of your story to use in your snapshot test, and use the description and other elements in the main story.

#### Updating snapshot image

The snapshot image used for testing locally and in the gitlab ci is built using the `Dockerfile.snapshots` file in the root of the opengeoweb repository. This image may need to be rebuilt occasionally for example when updating node Playwright versions. Locally this is done automatically as the `start-snapshot-server` rebuilds the Docker image on startup.

To update the Docker image used in the gitlab ci, you need to do the following:

1. Make desired changes to `Dockerfile.snapshots`.
2. Build the docker image with a new tag (check [the container registry](https://gitlab.com/opengeoweb/opengeoweb/container_registry/4512334) to see the latest tag and increment from this), using this command:

`docker build . -f Dockerfile.snapshots -t registry.gitlab.com/opengeoweb/opengeoweb/snapshots-image:<new-tag>`

2. You can now test snapshots locally using this image by updating the tag in this line in `package.json`:

` "start-snapshot-server": "docker run --rm -d --name snapshot-image -v $(pwd):/usr/src/app registry.gitlab.com/opengeoweb/opengeoweb/snapshots-image:<new-tag>",`

**!! WARNING !!** Do not push to the tag currently being used in `.gitlab-ci.yml` on master, otherwise you will break the pipeline for everyone!

And then running the snapshot tests as described above. This will use the image you just built locally on your machine.

3. Once you are happy that the new container works you can push it to the opengeoweb registry by doing:

`docker push registry.gitlab.com/opengeoweb/opengeoweb/snapshots-image:<new-tag>`

Note that if you have not interacted with the gitlab registry before you will need to run `docker login registry.gitlab.com` first and login with your username and an access token with permission to read and write to the registry (see [here](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html) for more info).

4. You will then need to ensure that the tags for the container are updated in `package.json` and `.gitlab-ci.yml`.

**!! WARNING !!** Make sure that the opengeoweb uses the same Playwright version as the Docker image as any mismatch between the playwright test runner and the playwright server in Docker is immediately reported as an error.

## Code linting

The code is linted by Eslint. All projects/libraries currently use the same configuration file: `.eslintrc`, located in the root of the project. If you need more or less rules for the whole project, edit this file. If you need specific rules for a app/lib (when you don't use React for example) then you can edit the `.eslintrc` file in the app/lib root folder.

### lint workspace

`npm run lint`

### lint affected project

`npm run affected:lint`

### lint specific app/lib

`nx lint sigmet-airmet`
or `nx run sigmet-airmet:lint`

### Ignoring ESLint rules

If, for some reason, ESLint needs to be ignored, a reason must be stated. For example:
`// eslint-disable-next-line`
would be considered incomplete. Instead, add the reason for ignoring the line:
`// eslint-disable-next-line no-unused-vars`

### Dependency checks

We use a linter rule, `@nx/dependency-checks`, to make sure that our libraries list the correct dependencies in the libraries' own `package.json` files. We do this so that if (and when) people outside of geoweb use our libraries as npm packages in their own projects, the users get the correct dependencies to run our libraries when running `npm install`.

When our code is changed, the dependencies can fall out of date. **To fix the resulting linter errors, run the failing linter jobs again with the `--fix` option.** This will automatically update the `package.json` files to their correct state.

#### Setup

The linter rule should be automatically set up for new created libraries. If manual setup is needed, follow the instructions here: https://nx.dev/nx-api/eslint-plugin/documents/dependency-checks#manual-setup.

Additionally, make sure that the library's package.json lists `dependencies` before `peerDependencies` (if we have both), since the linter plugin will update whichever of these two is defined first, and our other libraries already use `dependencies` for this purpose.

Also make sure that our build target has "external" set to "all", so we don't bundle dependencies inside of our libraries:

```
{
  "root": "libs/sigmet-airmet",
  "sourceRoot": "libs/sigmet-airmet/src",
  "projectType": "library",
  "generators": {},
  "targets": {
        "build": {
          "executor": "@nx/rollup:rollup",
          "options": {
            "external": ["all"],
          }
        },
    }
}
```

### Pre-commit hook linting

For each commit you do, a pre-commit hook is ran that checks linting and formats your files automatically. If for any reason you'd like to commit without running the hook, you can use the flag `--no-verify`

`git commit  --no-verify -m "commit message"`

## Nx Workspace

### Updating packages

To update all Nx packages:

1. run `npm run update`
2. follow the instructions!
3. commit and merge the changes

To update other packages:

1. to see a list of which packages could need updating, run `npm outdated -l`
2. check if there are any breaking changes to be aware of in the new version of a package
3. update a single package: `npm install <package-name>@<version>` or `npm install <package-name>@<version> --save-dev`
4. commit the updated files package.json and package-lock.json
5. update the code related to changes if needed
6. commit and merge the changes

### Installing new dependencies

Installing new dependencies works the same as every project:
`npm install <package-name>@<version>` or `npm install <package-name>@<version> --save-dev`

If the new dependency is also used in one of our libraries, we need to include the new dependency also in the library's `package.json` file. You don't need to update this file manually! Instead, lint the library with the `--fix` option to update the `package.json` automatically.

### Generating a dependency graph

Run `nx graph` to see a diagram of the dependencies of your projects. Run `nx affected:dep-graph --watch` to see the dependencies that are affected by your changes. The `--watch` flag makes it automatically update on new changes.

### Dependabot integration

This repository has a Dependabot integration with the following behavior specified in the `.gitlab/dependabot.yml` file:

| definition               | behavior                                                                                                                                                                                                                                                                   |
| ------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| schedule block           | Scan the repository daily at random time between 6-11. Add any pending package update as MR and update any MR with merge conflicts by doing a rebase. If there is a newer release of the dependency that already has an open MR, make a new MR and close the previous one. |
| open-pull-requests-limit | Don't open more than 3 MRs per directory, even if there are more package updates available than 3.                                                                                                                                                                         |
| rebase-strategy          | Always rebase open MRs that have dependabot as assignee, if there are merge conflicts present.                                                                                                                                                                             |
| assignee                 | Add dependabot as an assignee for every MR it opens.                                                                                                                                                                                                                       |

More detailed guide on how to work with dependabot can be found in the [opengeoweb wiki](https://gitlab.com/groups/opengeoweb/-/wikis/How-to-work-with-Dependabot-MRs)

### Adding an application to this workspace

Run `nx g @nx/react:app my-app` to generate an application.
Make sure to generate a new dependency graph and add it to this readme.

When using Nx, you can create multiple applications and libraries in the same workspace.

### Adding a new library to this workspace

Libraries are shareable across other libraries and applications. They can be imported from `@opengeoweb/mylib`.

1. Run `nx g @nx/react:lib <name>` to generate a react library. Run `nx g @nx/react:lib <name> --publishable --importPath="@opengeoweb/<name>"` to generate a react library that can be published to npm.
1. Configure storybook by running `nx g @nx/react:storybook-configuration <name>`. Make sure it uses a unique port, you can configure this in `project.json`. Compare the contents of the `.storybook` folder with one of the other libraries and copy/update the files to be similar.
1. Make sure to update the `sync-version` script in package.json, this will keep the versions synchronized automatically. If you made a non-publishable library, you have to manually add the package.json file.
1. Tag the library in its `project.json` file. The tag should be equal to the library name, for example the api lib has the tag `scope:api`, theme has `scope:theme` etc.
1. Now in the **main** `.eslintrc.js` file, search for the rule `@nx/enforce-module-boundaries` and add a new constraint for this specific tag. This will limit importing from other local libraries. Make this constraint as strict as possible. For more info on tags see [Nx tags](https://nx.dev/core-features/enforce-project-boundaries). To see changes in linting rules take effect, you have to close and reopen VSCode.
1. Add the necessary custom scripts in `project.json`: typecheck | docs.
1. Make sure `tsconfig.lib.json` does not exclude stories or tests, and has the necessary types (copy from another lib). And make sure the typecheck script in the `project.json` file points to this `tsconfig.lib.json`.
1. Update the test script in `project.json` to include coverage (copy from one of the other test scripts).
1. Update the `eslintrc.js`, copy the contents from one of the other libs.
1. Make sure the pipeline jobs include the new library where needed, at least create a new job to run the tests.
1. If you run into the error `Buildable libraries cannot import non-buildable libraries`, add a build script for your new library in `project.json`.
1. Make sure to generate a new dependency graph and add it to this readme.
1. To enable snapshots in your new library, follow the steps described in [Setup snapshot testing for a library](#setup-snapshot-testing-for-a-library)
1. Add the required scripts for the new library in `package.json`. For example: `"warnings:docs": "nx run warnings:docs", "warnings:build": "nx build warnings", "warnings:publish": "npm publish ./dist/libs/warnings --access public", "warnings:test": "nx run warnings:test",`.

### Removing a project from this workspace

Run `nx g @nx/workspace:remove <name>`.

### Generating a new React component in an existing app or lib

For example, run `nx g @nx/react:component my-component --project=sigmet-airmet` to generate a new component inside the sigmet-airmet library.

### Custom build scripts

Custom scripts are currently defined in two places; in `project.json` and `package.json`. Tasks need to be defined in workspace, and be used via npm script. We're currently investigating if this is the way to go (Nx builders vs Nx schematics), so it might change in the future. Current tasks (as defined in `project.json`):

To run a command for a specific app or library:
`nx typecheck sigmet-airmet`

To use these commands with the affected:
`nx affected --target=typecheck`

For now the 'global' scripts are prefixed with geoweb, and the project specific with sigmet-airmet. We use these prefixes to differentiate default scripts with our custom ones.

### Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.

## Code guidelines

Please note: when deviating from the standards, use an inline-comment (explaining why) above the related code.

### Functional programming

Use pure functions and make sure values are immutable. This will make the code more robust, easier to read and easier to test. A easy way to write pure functions is to not use `let` and only use `const`. For more info about functional programming check [this article](https://opensource.com/article/17/6/functional-javascript).

### Naming conventions

Regarding our file structure:

- In general, folders shall be named using camelCase
- Files and folders containing components shall be named using PascalCase
- Files with genetic names (such as 'reducer.ts', 'index.ts') shall be named using camelCase.

### (redux) hooks

- When creating a state for your component, consider extracting it to a hook. That way your functionality is nicely bundled, able to be shared and easy to test. For examples [see](https://reactjs.org/docs/hooks-custom.html#extracting-a-custom-hook)
- When creating a new component which is connected to redux, use redux hooks for this instead of wrapping the component with connect. For examples [see](https://react-redux.js.org/api/hooks)

#### Caveats with redux hooks

- When making use of redux useDispatch, use it in combination with React.useCallback to prevent unneccesary re-rendering of your functional component. For examples [see](https://react-redux.js.org/api/hooks#usedispatch)
- When making use of redux useSelector, be careful to use the selector that only returns the necessary information. Otherwise re-renders will occur with information updates you don't need.
- When implementing a selector, try to return a single value as these can be memoized. For details [see here](https://react-redux.js.org/api/hooks#equality-comparisons-and-updates)

Example on how not to work with selectors:

```
  const [currentActiveMapId, isOpenInStore] = useSelector((store: AppStore) =>
    uiSelectors.getisDialogOpenAndMapId(store, DialogTypes.LayerManager),
  );
```

The above will always trigger re-renders. Use instead:

```
    const currentActiveMapId = useSelector((store: AppStore) =>
      uiSelectors.getDialogMapId(store, DialogTypes.LayerManager),
    );

    const isOpenInStore = useSelector((store: AppStore) =>
      uiSelectors.getisDialogOpen(store, DialogTypes.LayerManager),
    );
```

These values can be memoized and will not trigger re-renders if they are unchanged.

## Release a new version and publishing to NPM

We use Semantic Versioning, see the [documentation](https://semver.org) for details.

1. Sync local and remote tags

   - 1.1 Check the [Tags](https://gitlab.com/opengeoweb/opengeoweb/-/tags) in gitlab to find the latest released tag
   - 1.2 Run `git fetch --all --tags` to fetch all tags from remote
   - 1.3 Run `git tag` to see all tags. Check that you can find the latest release tag also locally
   - 1.4 If you have local tags that are not on the remote, remove them with command `git tag -d tagname`

2. Generate the release notes

   - 2.1 Run `git log --no-merges --oneline --format=%s $(git rev-list --tags --max-count=1)..HEAD | uniq`. This command will retrieve the commit messages since the last tag was created and generate the release notes.
   - 2.2 The notes will appear in your command line. Copy the release notes to for example a text file. They will be needed during step 7.
   - 2.3 Compare the list of changes with [GitLab](https://gitlab.com/opengeoweb/opengeoweb/-/commits/master?ref_type=heads) commit list, if it matches
     use it in step 7. (release notes)

3. Decide how to bump the version, is it major, minor or patch?

   ***

   - Major version contains breaking changes
   - Minor version contains functional backwards compatible changes
   - Patch version contains backwards compatible small fixes or updates

   ***

   - 3.1 In Gitlab, go to [Compare revisions](https://gitlab.com/opengeoweb/opengeoweb/-/compare)
   - 3.2 Set the `Source` to master
   - 3.3 Set the `Target` to the latest tag
   - 3.4 Now you can see what merge requests and commits have been made to master and and decide based on this how to bump the version

4. Create a new branch based off master and name it according to the versison update for example `release_vX.Y.Z`

5. Push the new branch and create a Merge Request. You can use `git push --set-upstream origin release_vX.Y.Z`

6. Update the version

   - 6.1 Run `npm version <update_type>`, replacing <update_type> with `major`, `minor` or `patch`. For example: `npm version minor`
   - 6.2 Review the version changes on package-lock.json and package.json files and commit the changes
   - 6.3 Post the release MR into geoweb-intl-developers channel and get it approved and merged

7. Get the version update MR approved and merged before proceeding to the next step

8. Create the tag

   - 8.1 Create the tag manually using the same version number what you see in the MR using command `git tag vX.Y.Z`. For example `git tag v1.2.3` Note that the `v` should be lowercase.
   - 8.2 Push the created tag to remote with `git push origin vX.Y.Z`
   - 8.3 Check [Tags](https://gitlab.com/opengeoweb/opengeoweb/-/tags) in gitlab. The newly created and pushed tag should show on top of the list.

9. Create the new release

   - 9.1 Go to [Tags](https://gitlab.com/opengeoweb/opengeoweb/-/tags) in gitlab.
   - 9.2 Click the `Create release` button on the right side of the newly created tag
   - 9.3 Give the release a title, for example `Release vX.Y.Z` according to the tag
   - 9.4 Copy paste the release notes generated during the step 2. If you forgot to save the generated release notes you can check the changes from [Compare revisions](https://gitlab.com/opengeoweb/opengeoweb/-/compare) and write the release notes manually based on the merge requests.
   - 9.5 Finally click on the `Create release`
   - The libraries will be published to the @opengeoweb organisation on npmjs, this is done as part of the tag pipeline.

10. Confirm that deployment is done succesfylly

    - 10.1 The Frontend release this can be verified by checking that the [tag pipeline](https://gitlab.com/opengeoweb/opengeoweb/-/tags) runs succesfully. After the pipeline is done, check that an automatic message appears on `geoweb-intl-deployments` Slack channel. Backend releases can be confirmed by opening the backend project in gitlab and using the same method.
    - 10.2 After FE release is done, proceed to do the BE release and create the release labels. Follow the instructions [here](https://gitlab.com/groups/opengeoweb/-/wikis/Meetings/Release-&-Deployment)

## Releasing alpha code by publishing a special version to NPM

We use Semantic Versioning, see the [documentation](https://semver.org) for details.

1. You start with the development branch you want to publish on npm

2. Update the version
   You must use a version name that includes the term `spike-.*`, for example `spike-ol` or `spike-tanstack-test` (`spike-YOURSPIKE` in the following instructions). The chosen id will be used as a prerelease id that the `npm version prerelease` command can use.

   - 6.1 Run `npm version prerelease --preid=spike-YOURSPIKE --no-git-tag-version`. This command will update package.json files and push the changes to the remote. It will not create a tag.
   - 6.2 The npm command prints a version string you should use for the npm tag, which would be something like `v9.33.0-spike-ol.0`
   - 6.3 Commit the changed files (package.json and package-lock.json files)
   - 6.4 Create the `git` tag via the command-line using the version number (that `npm version ...` printed) using command `git tag <TAG>`, for example `git tag vX.Y.Z-spike-ol.0` Note that the `v` is mandatory and should be lowercase.
   - 6.5 Push the created tag to remote with `git push origin <TAG>` for example `git push origin vX.Y.Z-spike-ol.0`
   - 6.6 Check [Tags](https://gitlab.com/opengeoweb/opengeoweb/-/tags) in gitlab. The newly created and pushed tag might not appear at the top of the list.

3. The libraries will be published to the @opengeoweb organisation on npmjs as pre-release versions, this is done as part of the tag pipeline. The packages will have a npm tag with `-spike-.*` in it's name, for example and you can use them by specifiying for example `9.33.1-spike-ol.0` as package version. The command `npm ci` always uses package versions with the `latest` tag, so the `spike-*` releases will never be used by `npm ci` by default.

## Link Zeplin designs to storybook

Geoweb is using the `storybook-zeplin` package to link specific design versions to a specific storybook story. The following steps describe how to use this library. Before you start, make sure you have access to ZepLin. More info can be found here https://github.com/mertkahyaoglu/storybook-zeplin

### Install instructions

Follow these steps to configure `storybook-zeplin`, you only have to do this once

#### Retrieve access token

1. Go to https://app.zeplin.io/profile/developer
1. Press the `Create new token` button
1. Give your personal token a name, for example `GeoWeb` and press the `Create` button
1. Copy your personal access token

#### Create env file and link access token

1. Create a new `.env` file inside the project root
1. Add your token to the `.env` file

```
// .env
STORYBOOK_ZEPLIN_TOKEN="myVeryLongPersonalToken"
```

3. Open a storybook of your choice with has links to design, and verify if they work. For example you could try the theme storybook: `nx storybook theme` and go to `http://localhost:4800/?path=/story/components-appheader--app-header-dark`, a design panel should show with the preview of the design

### Retrieve design link with correct version

It's important to link to the correct verison of a design, so design and code are always in sync.

1. Determine which design needs to be linked. As example, we're going to link to the App mode selector (the top navbar). The latest design can be found here `https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68c87ed4860e1d14f5dd`
1. When you're at the design page, press the button on the bottom left of the screen, it says `[designer namer][totalVersions]versions`. If will bring up a side panel with a list of commits to all versions.
1. If want to use the latest design we can use the previous url, but when that a design is updated the content of the url will change too, and that's something we don't want. On the left you can see a list of commits. We want to use the latest design, so press the commit with the latest version at the top. Press the option button, it will give you 2 options: `Copy version web link` and `Copy version app URI`. Both unfortunately don't work currently out of the box in storybook. The first one will return a short url which doesn't show the design preview in the storybook, and the second one does show a preview but you can't click it. Click option 1 `Copy version web link`. Now open a new browser tab and paste the weblink, you will be send to the correct page with a url we can use. Something like: `https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68c87ed4860e1d14f5dd/version/632317948eb865a6a93819ea` (note the url should have a version id included: `https://app.zeplin.io/project/:PROJECTID/screen/SCREENID/version/VERSIONID` ).

### Link a design to a story

1. Go to the story of your component. As an example we pick the `AppHeader` component defined in the `shared` lib
1. Add the link to designs(s) by adding parameters to the story. Note you can pass a list of designs:

```javascript
// libs/shared/src/lib/components/AppHeader/AppHeader.stories.tsx
export const AppHeaderDark = (): React.ReactElement => (
  <AppHeaderDemo theme={darkTheme} />
);
AppHeaderDark.storyName = 'App Header Dark (takeSnapshot)';
AppHeaderDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68cb9e0ee610eaf967ec/version/63231796a76e27a98cdff529',
    },
  ],
};
```

### Tips

- if you can't see the design panel in storybook, press `a` or press the option menu next to the storybook logo and press `Show addons`
- In the design panel, you can press `Change addon orientation`. Since a lot of designs are horizontal, a vertical split screen is much easier to develop
- If you press `Show overlay` button in the design panel, you get a overlay of the design over the code. Slide the `opacity slider` so you can see the design transparent. Now you can move the overlay over your story, to make sure all the pixels are on the correct place. You can also press `lock overlay` so you won't accidentally move it. With the `show differences` button you can see the difference in pixels. All these tools can help achieve getting the correct designs in code, with as much pixel perfection as possible.

## Documentation

All documentation of libs and geoweb are bundled inside the `documentation` lib. To run it:

- start all stories with `npm run documentation:start`
- when the stories are running (this can take some seconds) run the documentation library with `nx storybook documentation`

### Testing out documentation build

- run `npm run affected:build-storybook`, this will build all the stories and puts them in the `./dist` folder
- run `nx static-storybook documentation`, this will serve the `index.html` file inside the `./dist/storybook/documentation/index.html` folder.

## Generate TypeDoc Documentation

It is possible to automatically generate documentation from the source code using [TypeDoc](http://typedoc.org/).

In order to do so, run `nx run <project-name>:docs`.

Please note that nothing will be created if the code contains errors. Otherwise, a new `docs/<project-name>` folder will be created under the root folder.

Documentation is also published from the gitlab pipeline of master and can be found here:

- [Core](https://opengeoweb.gitlab.io/opengeoweb/docs/core/)
- [Sigmet-airmet](https://opengeoweb.gitlab.io/opengeoweb/docs/sigmet-airmet/)
- [Taf](https://opengeoweb.gitlab.io/opengeoweb/docs/taf/)
- [Spaceweather](https://opengeoweb.gitlab.io/opengeoweb/docs/spaceweather/)
- [Workspace](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/)
- [Theme](https://opengeoweb.gitlab.io/opengeoweb/docs/theme/)

## Licence

This project is licensed under the terms of the Apache 2.0 licence.
Every file in this project has a header that specifies the licence and copyright. It is possible to check/add/remove the licence header using the following commands:

`npm run licence:check` => checks all the files for having the header text as specified in the LICENCE file. The format and which files to include is specified in `licence.config.json`.

`npm run licence:add` => adds licence to files without it. This action can require a manual check when the file contains a wrong header (i.g. a simple word is missed), because it just adds another header without removing the wrong one.

`npm run licence:remove` => removes licence from all files. This action can require a manual check when the file contains a wrong header (i.g. a simple word is missed), because it only removes a correct header and not a wrong one.
