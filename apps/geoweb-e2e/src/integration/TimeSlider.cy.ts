/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

describe('TimeSlider', () => {
  beforeEach(() => {
    cy.mockViewpresets();
    cy.mockWorkspaces();
    cy.mockExampleConfigNoFeatures();
  });

  it('should display tooltip showing time at mouse pointer when hovering', () => {
    const fixedClockMillis = Date.parse('2023-04-05T12:00:00Z');
    cy.clock(fixedClockMillis, ['Date']);
    cy.visit('/');
    cy.get('[data-testid="timeSliderLegend"] [data-testid="canvas"]')
      .as('timeSliderCanvasSelector')
      .should('be.visible');

    cy.get('@timeSliderCanvasSelector').trigger('mouseover');
    cy.get('@timeSliderCanvasSelector').trigger('mousemove', {
      eventConstructor: 'MouseEvent',
      clientX: 500,
    });
    // TODO
    const time1 = '00:17';
    const time2 = '01:24';
    cy.findByText(time1).should('not.exist');
    // Restore the clock to allow timers to proceed.
    cy.clock().then((clock) => {
      clock.restore();
    });
    cy.findByText(time1).should('be.visible');
    cy.get('@timeSliderCanvasSelector').trigger('mousemove', {
      eventConstructor: 'MouseEvent',
      clientX: 550,
    });
    cy.findByText(time2).should('be.visible');
    cy.get('@timeSliderCanvasSelector').trigger('mouseout');
    cy.findByText(time2).should('not.exist');
  });
  it('should display animation length of the active layer when Auto animation is enabled', () => {
    cy.mockRadarGetCapabilities();
    cy.visit('/');

    // Add two layers, the first layer is automatically the active layer through the 'Both' option
    cy.findByTestId('layerSelectButton').click();
    cy.findByTestId(
      'layerAddRemoveButton-Radar:radar_vihti_dbzh-cappi_3067',
    ).click();
    cy.findByTestId(
      'layerAddRemoveButton-Weather:mean-sea-level-pressure-forecast-contour',
    ).click();

    // Enable Auto animation
    cy.findByTestId('optionsMenuButton').click();
    cy.findByTestId('animationLengthButton').click();
    cy.findByText('Auto').click();
    cy.findByTestId('animationLengthButton')
      .siblings()
      .should('contain', '8d 5m');

    // Close Layer select popup window
    cy.findByTestId('layerSelectWindow').within(() =>
      cy.findByTestId('closeBtn').click(),
    );

    // Change active layer using 'Auto-timestep' to get a new animation length
    cy.findAllByTestId('toggleMenuButton').eq(1).click();
    cy.findAllByText('Auto-timestep').eq(0).click();
    cy.findByTestId('animationLengthButton')
      .siblings()
      .should('contain', '2d 3h');
  });
  it('should change animation length automatically if a layer contains several intervals', () => {
    cy.mockRadarGetCapabilities();
    cy.visit('/');

    cy.findByTestId('layerSelectButton').click();

    cy.findAllByTestId('serviceChip').should('have.length.greaterThan', 0);

    // Click on the ECMWF
    cy.findAllByTestId('serviceChip').contains('ECMWF').click();

    // Add wind speed layer which contains multiple intervals
    cy.findByTestId('layerAddRemoveButton-850ws').click();
    cy.findByTestId('layerSelectWindow').within(() => {
      cy.findByTestId('closeBtn').click();
    });

    cy.findByTestId('optionsMenuButton').click();
    cy.findByTestId('timeStepButton').siblings().should('contain', '3h');
    cy.findByTestId('timeStepButton').click();

    // Jump forward 1 week to step inside a new interval
    cy.findByText('1 week').click();
    cy.findByTestId('stepForwardButton').click();
    cy.findByTestId('stepForwardButton').click();
    cy.findByTestId('timeStepButton').click();
    cy.findByText('Auto').click();
    cy.findByTestId('timeStepButton').siblings().should('contain', '6h');
  });
  it('should sync animation length when auto update is enabled on synced map', () => {
    cy.mockRadarGetCapabilities();
    cy.visit('/');

    // Split the screen to add a second map
    cy.findByTestId('split-vertical-btn').click();

    // Open menu
    cy.findByTestId('menuButton').click();
    // Open sync group viewer
    cy.findByText('Sync Groups').click();
    // Add both maps to the sync group
    cy.get('input[aria-labelledby="1_screen"]').check();
    cy.get('input[aria-labelledby="2_screen"]').check();
    // Close sync group viewer
    cy.findByTestId('syncGroupViewer').findByTestId('CloseIcon').click();

    // Add layers to the first map
    cy.get('[data-testid="layerSelectButton"]').click();
    cy.findByTestId(
      'layerAddRemoveButton-Radar:radar_vihti_dbzh-cappi_3067',
    ).click();
    // Close Layer select on the first map
    cy.findByTestId('layerSelectWindow').within(() =>
      cy.findByTestId('closeBtn').click(),
    );
    // Open layer manager of the second map
    cy.findAllByTestId('layerManagerButton').last().click();
    // Add layer to the second map
    cy.get('[data-testid="layerSelectButton"]').click();
    cy.findByTestId(
      'layerAddRemoveButton-Weather:mean-sea-level-pressure-forecast-contour',
    ).click();
    // Close layer select on the second map
    cy.findByTestId('layerSelectWindow').within(() =>
      cy.findByTestId('closeBtn').click(),
    );
    // Close layer manager of the second map
    cy.findAllByTestId('layerManagerButton').last().click();

    // Open options menu of the first map
    cy.get('[data-testid="optionsMenuButton"]').first().click();
    // Open animation length menu on the first map
    cy.findByTestId('animationLengthButton').click();
    // Set animation length to 24h on the first map
    cy.findByText('24 h').click();
    // Check animation length on the first map
    cy.get('[data-testid="animationLengthButton"]')
      .first()
      .siblings()
      .should('contain', '1d');
    // Open options menu of the second map
    cy.get('[data-testid="optionsMenuButton"]').last().click();
    // Check animation length on the second map
    cy.get('[data-testid="animationLengthButton"]')
      .last()
      .siblings()
      .should('contain', '4h 50m');

    // Enable auto update on the first map
    cy.get('[data-testid="auto-update-svg-off"]').first().click();
    // auto update should be on on the first map
    cy.get('[data-testid="auto-update-svg-on"]').should('have.length', 1);
    // auto update should be off on the second map
    cy.get('[data-testid="auto-update-svg-off"]').should('have.length', 1);

    // Start the animation on the second map
    cy.get('[data-testid=play-svg-path]').last().click();
    // should run animation on both maps
    cy.get('[data-testid=pause-svg-path]').should('have.length', 2);

    // Check again animation length on the first map
    cy.get('[data-testid="animationLengthButton"]')
      .first()
      .siblings()
      .should('contain', '1d');
    // Check again animation length on the second map to see if it is synced with the first map
    cy.get('[data-testid="animationLengthButton"]')
      .last()
      .siblings()
      .should('contain', '1d');

    // Stop the animation on the first map
    cy.get('[data-testid=pause-svg-path]').first().click();
    // should stop animation on both maps
    cy.get('[data-testid=play-svg-path]').should('have.length', 2);
  });
});
