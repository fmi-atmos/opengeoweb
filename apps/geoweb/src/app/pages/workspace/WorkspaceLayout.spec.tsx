/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Route } from 'react-router-dom';

import { render, screen, waitFor } from '@testing-library/react';
import {
  workspaceActions,
  workspaceListActions,
  viewPresetActions,
  workspaceRoutes,
} from '@opengeoweb/workspace';
import { routerActions, uiActions, uiTypes } from '@opengeoweb/store';
import {
  timeSeriesActions,
  emptyTimeSeriesPreset,
} from '@opengeoweb/timeseries';
import { TestWrapperWithRouter } from '../../components/Providers';
import { WorkspaceLayout } from './WorkspaceLayout';
import { translateKeyOutsideComponents } from '../../../i18n';
import { createMockStore } from '../../utils/testUtils';

describe('geoweb/src/app/pages/workspace/WorkspaceLayout', () => {
  it('should not render object manager while initial presets have not been loaded', async () => {
    const initialWorkspaceState = {
      id: '',
      title: 'Empty Map test',
      views: {
        allIds: [],
        byId: {},
      },
      mosaicNode: 'test',
      isFetching: false,
      syncGroups: [],
    };
    const mockState = {
      workspace: initialWorkspaceState,
    };

    const store = createMockStore(mockState);
    const dispatchSpy = jest.spyOn(store, 'dispatch');

    render(
      <TestWrapperWithRouter store={store}>
        <Route path="/" element={<WorkspaceLayout />} />
      </TestWrapperWithRouter>,
    );

    expect(screen.queryByTestId('appLayout')).toBeFalsy();
    expect(dispatchSpy).not.toHaveBeenCalled();
    await waitFor(() => {
      expect(screen.getByTestId('appLayout')).toBeTruthy();
    });
    // render all correct modules
    expect(dispatchSpy).toHaveBeenCalledWith(
      workspaceListActions.fetchWorkspaceList({}),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      viewPresetActions.fetchViewPresets(expect.any(Object)),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'syncGroups',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'timeSeriesManager',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'timeSeriesSelect',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'keywordFilter',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'layerInfo',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'layerSelect',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'layerManager',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      viewPresetActions.registerViewPreset({
        panelId: 'test',
        viewPresetId: '',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      workspaceActions.changeToEmptyMapWorkspace({
        newMapPresetText: translateKeyOutsideComponents(
          'workspace-mappreset-new',
        ),
        newWorkspaceText: translateKeyOutsideComponents('workspace-new'),
      }),
    );

    expect(dispatchSpy).toHaveBeenCalledWith(
      routerActions.navigateToUrl({ url: workspaceRoutes.root }),
    );

    expect(dispatchSpy).not.toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'objectManager',
        setOpen: false,
        source: 'app',
      }),
    );
  });

  it('render object manager and public warnings when GW_DRAWINGS_BASE_URL is given', async () => {
    const initialWorkspaceState = {
      id: '',
      title: 'Empty Map test',
      views: {
        allIds: [],
        byId: {},
      },
      mosaicNode: 'test',
      isFetching: false,
      syncGroups: [],
    };
    const mockState = {
      workspace: initialWorkspaceState,
    };

    const store = createMockStore(mockState);
    const dispatchSpy = jest.spyOn(store, 'dispatch');

    render(
      <TestWrapperWithRouter store={store}>
        <Route
          path="/"
          element={
            <WorkspaceLayout
              config={{
                GW_DRAWINGS_BASE_URL: 'some url',
              }}
            />
          }
        />
      </TestWrapperWithRouter>,
    );

    expect(screen.queryByTestId('appLayout')).toBeFalsy();
    await waitFor(() => {
      expect(screen.getByTestId('appLayout')).toBeTruthy();
    });
    await waitFor(() => {
      expect(dispatchSpy).toHaveBeenCalledWith(
        uiActions.registerDialog({
          type: 'objectManager',
          setOpen: false,
          source: 'app',
        }),
      );
    });

    await waitFor(() => {
      expect(dispatchSpy).toHaveBeenCalledWith(
        uiActions.registerDialog({
          type: uiTypes.DialogTypes.PublicWarnings,
          setOpen: false,
          source: 'app',
        }),
      );
    });
  });

  it('should render with default props and show title in tab', async () => {
    const store = createMockStore();
    const dispatchSpy = jest.spyOn(store, 'dispatch');

    render(
      <TestWrapperWithRouter store={store}>
        <Route path="/" element={<WorkspaceLayout />} />
      </TestWrapperWithRouter>,
    );

    await screen.findByTestId('workspace');
    await waitFor(() => {
      expect(screen.getByTestId('workspaceTab').textContent).toEqual(
        translateKeyOutsideComponents('workspace-new'),
      );
    });

    // render all correct modules
    expect(dispatchSpy).toHaveBeenCalledWith(
      workspaceListActions.fetchWorkspaceList({}),
    );

    expect(dispatchSpy).toHaveBeenCalledWith(
      viewPresetActions.fetchViewPresets(expect.any(Object)),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'syncGroups',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'timeSeriesSelect',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'timeseriesInfo',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'timeSeriesManager',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      timeSeriesActions.addServices({
        timeSeriesServices: emptyTimeSeriesPreset,
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.KeywordFilter,
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.LayerManager,
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      workspaceActions.changeToEmptyMapWorkspace({
        newMapPresetText: translateKeyOutsideComponents(
          'workspace-mappreset-new',
        ),
        newWorkspaceText: translateKeyOutsideComponents('workspace-new'),
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      routerActions.navigateToUrl({ url: workspaceRoutes.root }),
    );
  });

  it('should fetch correct workspace with workspaceId ', async () => {
    const mockState = {
      workspace: {
        id: '',
        title: 'Empty Map test',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: '',
        isFetching: false,
        syncGroups: [],
      },
    };
    const store = createMockStore(mockState);
    const dispatchSpy = jest.spyOn(store, 'dispatch');
    const props = {
      workspaceId: '12345',
    };

    render(
      <TestWrapperWithRouter store={store}>
        <Route path="/" element={<WorkspaceLayout {...props} />} />
      </TestWrapperWithRouter>,
    );

    await screen.findByTestId('workspace');

    await waitFor(() => {
      expect(dispatchSpy).toHaveBeenCalledWith(
        workspaceActions.fetchWorkspace({ workspaceId: props.workspaceId }),
      );
    });
  });

  it('should fetch initial workspace when the id is not given in the URL', async () => {
    const mockState = {
      workspace: {
        id: '',
        title: 'Empty Map test',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: '',
        isFetching: false,
        syncGroups: [],
      },
    };
    const store = createMockStore(mockState);
    const dispatchSpy = jest.spyOn(store, 'dispatch');
    const props = {
      workspaceId: undefined,
    };
    const config = {
      GW_INITIAL_WORKSPACE_PRESET: 'abcd',
    };

    render(
      <TestWrapperWithRouter store={store}>
        <Route
          path="/"
          element={<WorkspaceLayout {...props} config={config} />}
        />
      </TestWrapperWithRouter>,
    );

    await screen.findByTestId('workspace');

    await waitFor(() => {
      expect(dispatchSpy).toHaveBeenCalledWith(
        workspaceActions.fetchWorkspace({
          workspaceId: config.GW_INITIAL_WORKSPACE_PRESET,
        }),
      );
    });
  });

  it('should fetch empty workspace when id not in URL and initial workspace not configured', async () => {
    const mockState = {
      workspace: {
        id: 'empty',
        title: 'Empty Map test',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: '',
        isFetching: false,
        syncGroups: [],
      },
    };
    const store = createMockStore(mockState);
    const dispatchSpy = jest.spyOn(store, 'dispatch');
    const props = {
      workspaceId: mockState.workspace.id,
    };

    render(
      <TestWrapperWithRouter store={store}>
        <Route path="/" element={<WorkspaceLayout {...props} />} />
      </TestWrapperWithRouter>,
    );

    await screen.findByTestId('workspace');

    await waitFor(() => {
      expect(dispatchSpy).toHaveBeenCalledWith(
        workspaceActions.fetchWorkspace({
          workspaceId: mockState.workspace.id,
        }),
      );
    });
  });
});
