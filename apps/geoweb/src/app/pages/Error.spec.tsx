/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { I18nextProvider } from 'react-i18next';
import Error from './Error';
import i18n, {
  initGeowebI18n,
  translateKeyOutsideComponents,
} from '../../i18n';

beforeAll(() => {
  initGeowebI18n();
});

describe('pages/error', () => {
  it('should render correctly', async () => {
    const { baseElement } = render(
      <I18nextProvider i18n={i18n}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Error />} />
          </Routes>
        </BrowserRouter>
      </I18nextProvider>,
    );
    expect(baseElement).toBeTruthy();
  });

  it('should show woops something wrong message if no error passed', async () => {
    const { baseElement } = render(
      <I18nextProvider i18n={i18n}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Error />} />
          </Routes>
        </BrowserRouter>
        ,
      </I18nextProvider>,
    );

    expect(baseElement).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('geoweb-alert-severity-info-message'),
      ),
    ).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('geoweb-go-back-to-login'),
      ),
    ).toBeTruthy();
  });

  it('should show error message', async () => {
    const error = { message: 'Error message', stack: 'error stack' };

    const { baseElement } = render(
      <I18nextProvider i18n={i18n}>
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={<Error location={{ state: { error } } as History} />}
            />
          </Routes>
        </BrowserRouter>
      </I18nextProvider>,
    );

    expect(baseElement).toBeTruthy();
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('geoweb-alert-severity-error-message'),
      ),
    ).toBeTruthy();
    expect(await screen.findByText('Error message')).toBeTruthy();
    expect(await screen.findByText('error stack')).toBeTruthy();
  });
});
