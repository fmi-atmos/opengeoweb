/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import * as apiModule from '@opengeoweb/api';
import { AuthenticationConfig } from '@opengeoweb/authentication';
import { Credentials } from '@opengeoweb/api';
import { ConnectionIssueBanner } from './ConnectionIssueBanner';
import { createMockStore } from '../utils/testUtils';
import { TestWrapper } from './Providers';

describe('components/ConnectionIssueBanner', () => {
  const auth: Credentials = {
    expires_at: 1640011500,
    refresh_token: '',
    token: 'test',
    username: 'max.verstappen',
    has_connection_issue: true,
  };

  const authConfig: AuthenticationConfig = {
    GW_APP_URL: '',
    GW_AUTH_TOKEN_URL: '',
    GW_AUTH_CLIENT_ID: '',
    GW_AUTH_LOGIN_URL: '',
    GW_AUTH_LOGOUT_URL: '',
  };

  it('should not do anything if there is no connection error', async () => {
    const store = createMockStore();
    const { container } = render(
      <TestWrapper store={store}>
        <ConnectionIssueBanner />
      </TestWrapper>,
    );
    const { childElementCount } = container;
    await waitFor(() => {
      expect(childElementCount).toEqual(0);
    });
  });

  it('should show the alertbanner and not go away when there are connection issues after clicking try again', async () => {
    const store = createMockStore();
    const onSetAuth = jest.fn();
    render(
      <TestWrapper store={store}>
        <ConnectionIssueBanner
          auth={auth}
          onSetAuth={onSetAuth}
          authConfig={authConfig}
        />
      </TestWrapper>,
    );
    expect(await screen.findByTestId('alert-banner')).toBeTruthy();
    expect(screen.getByRole('button')).toBeTruthy();

    jest
      .spyOn(apiModule, 'refreshAccessTokenAndSetAuthContext')
      .mockImplementationOnce(({ onSetAuth }) => {
        onSetAuth!({
          ...auth,
          has_connection_issue: true,
        });
        return Promise.resolve();
      });

    /* Click button try again */
    fireEvent.click(screen.getByText('TRY AGAIN'));
    expect(onSetAuth).toHaveBeenCalledTimes(2);
    expect(onSetAuth).toHaveBeenCalledWith({
      expires_at: 1640011500,
      has_connection_issue: false,
      refresh_token: '',
      token: 'test',
      username: 'max.verstappen',
    });
    expect(onSetAuth).toHaveBeenLastCalledWith({
      expires_at: 1640011500,
      has_connection_issue: true,
      refresh_token: '',
      token: 'test',
      username: 'max.verstappen',
    });
  });

  it('should show snackbar after clicking try again when there are no connection issues anymore', async () => {
    const store = createMockStore();
    const onSetAuth = jest.fn();

    render(
      <TestWrapper store={store}>
        <ConnectionIssueBanner
          auth={auth}
          onSetAuth={onSetAuth}
          authConfig={authConfig}
        />
      </TestWrapper>,
    );
    expect(await screen.findByTestId('alert-banner')).toBeTruthy();
    expect(screen.getByRole('button')).toBeTruthy();

    jest
      .spyOn(apiModule, 'refreshAccessTokenAndSetAuthContext')
      .mockImplementationOnce(({ onSetAuth }) => {
        onSetAuth!({
          ...auth,
          has_connection_issue: false,
        });
        return Promise.resolve();
      });

    /* Click button try again */
    fireEvent.click(screen.getByText('TRY AGAIN'));
    expect(onSetAuth).toHaveBeenCalledTimes(2);
    expect(onSetAuth).toHaveBeenLastCalledWith({
      expires_at: 1640011500,
      has_connection_issue: false,
      refresh_token: '',
      token: 'test',
      username: 'max.verstappen',
    });

    await waitFor(() => {
      expect(screen.queryByTestId('alert-banner')).toBeFalsy();
    });
    await screen.findByTestId('snackbarComponent');
    await screen.findByText('geoweb-connection-restored');
  });
});
