/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';

import { setupServer } from 'msw/node';

import { Button } from '@mui/material';
import { http } from 'msw';
import { makeMockApiCall } from '@opengeoweb/api';
import { NotificationTriggerProvider, useNotificationTriggerContext } from '.';
import { TestWrapper } from '../../utils/testUtils';
import { requestHandlers } from '../../utils/spaceweather-api/fakeApi';
import { fakeEventList } from '../../utils/fakedata';

const server = setupServer(...requestHandlers);
describe('src/components/NotificationTrigger/NotificationTriggerContext', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });

  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
    server.resetHandlers();
  });

  it('should fetch new data if onFetchNewNotificationTriggerData is called ', async () => {
    const TestComponent: React.FC = () => {
      const { onFetchNewNotificationTriggerData } =
        useNotificationTriggerContext();

      return (
        <Button
          data-testid="fakebutton"
          onClick={onFetchNewNotificationTriggerData}
        >
          Hello there
        </Button>
      );
    };
    const mockGetNewNotifications = makeMockApiCall(
      fakeEventList.filter(
        (event) => event.originator === 'METOffice' && event.notacknowledged,
      ),
    );
    server.use(
      http.get(/.*\/notification\/newNotifications$/, mockGetNewNotifications),
    );

    render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <TestComponent />
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(screen.getByTestId('fakebutton')).toBeTruthy();
    });

    fireEvent.click(screen.getByTestId('fakebutton'));

    // Test new data has been fetched
    expect(mockGetNewNotifications).toHaveBeenCalledTimes(1);
  });
});
