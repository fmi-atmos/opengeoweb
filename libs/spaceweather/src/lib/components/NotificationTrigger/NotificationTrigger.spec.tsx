/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, waitFor, screen } from '@testing-library/react';
import { setupServer } from 'msw/node';
import { http, HttpResponse } from 'msw';
import { NotificationTrigger } from './NotificationTrigger';
import { NotificationTriggerProvider } from '.';
import { TestWrapper } from '../../utils/testUtils';
import { fakeEventList } from '../../utils/fakedata';
import { requestHandlers } from '../../utils/spaceweather-api/fakeApi';

const server = setupServer(...requestHandlers);
describe('src/components/NotificationTrigger/NotificationTrigger', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  afterEach(() => {
    server.resetHandlers();
  });
  it('should render text for 2 triggers if 2 triggers received', async () => {
    server.use(
      http.get(/.*\/notification\/newNotifications$/, () => {
        return HttpResponse.json([fakeEventList[1], fakeEventList[4]]);
      }),
    );

    render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <NotificationTrigger />
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(
        screen.getByText('There are 2 unhandled notification triggers.'),
      ).toBeTruthy();
    });
    expect(screen.getByTestId('notificationtrigger-alert')).toBeTruthy();
  });

  it('should render an error message if error passed', async () => {
    server.use(
      http.get(/.*\/notification\/newNotifications$/, () => {
        return HttpResponse.json({ message: 'error' }, { status: 500 });
      }),
    );

    render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <NotificationTrigger />
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(
        screen.getByText('New notification trigger retrieval: error'),
      ).toBeTruthy();
    });
  });

  it('should render text for single trigger if only 1 trigger received', async () => {
    server.use(
      http.get(/.*\/notification\/newNotifications$/, () => {
        return HttpResponse.json([fakeEventList[1]]);
      }),
    );
    render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <NotificationTrigger />
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(
        screen.getByText('There is 1 unhandled notification trigger.'),
      ).toBeTruthy();
    });
    expect(screen.getByTestId('notificationtrigger-alert')).toBeTruthy();
  });
});
