/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Meta, StoryObj } from '@storybook/react/*';
import { createFakeApiInstance } from '@opengeoweb/api';
import TimeSeries from './TimeSeries';
import { ToggleThemeButton } from '../../utils/storybookUtils';
import { SpaceWeatherApi } from '../../utils/api';
import {
  createApi as createFakeApi,
  getDummyTimeSerie,
} from '../../utils/fakeApi';
import { TimeseriesParams, TimeseriesResponseData } from '../../types';

const meta: Meta<typeof TimeSeries> = {
  title: 'components/TimeSeries',
  component: TimeSeries,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeSeries',
      },
    },
  },
  tags: ['!autodocs'],
};
export default meta;

type Story = StoryObj<typeof TimeSeries>;

export const TimeSeriesDummyData: Story = {
  render: () => (
    <>
      <ToggleThemeButton />
      <TimeSeries />
    </>
  ),
};

export const TimeSeriesWithAllErrors: Story = {
  render: () => (
    <>
      <ToggleThemeButton />
      <TimeSeries />
    </>
  ),
  parameters: {
    createApiFunc: (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getTimeSeriesMultiple: () => Promise.reject(new Error('Network error')),
    }),
  },
};

const fakeAxiosInstance = createFakeApiInstance();

export const TimeSeriesWithTwoErrors: Story = {
  render: () => (
    <>
      <ToggleThemeButton />
      <TimeSeries />
    </>
  ),
  parameters: {
    createApiFunc: (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getTimeSeriesMultiple: (
        params: TimeseriesParams[],
      ): Promise<
        {
          data: TimeseriesResponseData;
          message?: string | undefined;
        }[]
      > => {
        return Promise.all(
          params.map((param) => {
            if (param.stream === 'kp' || param.stream === 'xray') {
              return Promise.reject(new Error(`${param.stream} error`)).catch(
                (error) => {
                  return error;
                },
              );
            }
            return fakeAxiosInstance.get('/timeseries/data').then(() => ({
              data: getDummyTimeSerie(param.stream!, param.parameter!),
            }));
          }),
        );
      },
    }),
  },
};

export const TimeSeriesLoading: Story = {
  render: () => (
    <>
      <ToggleThemeButton />
      <TimeSeries />
    </>
  ),
  parameters: {
    createApiFunc: (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getTimeSeriesMultiple: () =>
        new Promise((resolve) => {
          setTimeout(() => {
            resolve([]);
          }, 5000);
        }),
    }),
  },
};
