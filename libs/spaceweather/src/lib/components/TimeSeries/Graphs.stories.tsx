/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { dateUtils } from '@opengeoweb/shared';
import Card from '@mui/material/Card';
import { Meta, StoryObj } from '@storybook/react/*';
import {
  areaGraphWithSeries,
  areaGraphWithSeriesFixedDates,
  bandGraphWithSeries,
  bandGraphWithSeriesFixedDates,
  barGraphWithSeries,
  barGraphWithSeriesFixedDates,
  windDensityWithSeries,
  windDensityWithSeriesFixedDates,
  windPressureWithSeries,
  windPressureWithSeriesFixedDates,
  xrayWithSeries,
  xrayWithSeriesFixedDates,
} from '../../utils/DummyData';
import { beginTime, endTime } from '../../utils/defaultTimeRange';
import { Graphs } from './Graphs';
import { getGraphHeightInPx } from './TimeSeries.utils';
import {
  CHART_TOP_MARGIN,
  PLOT_TOP_MARGIN,
  CHART_BOTTOM_MARGIN,
} from './utils';
import { ToggleThemeButton } from '../../utils/storybookUtils';

const meta: Meta<typeof Graphs> = {
  title: 'components/Timeseries/Graphs',
  component: Graphs,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the Graphs',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof Graphs>;

const graphHeight = getGraphHeightInPx();
const chartHeight =
  CHART_TOP_MARGIN +
  PLOT_TOP_MARGIN * 6 +
  graphHeight * 6 +
  CHART_BOTTOM_MARGIN;

export const Component: Story = {
  args: {
    data: [
      barGraphWithSeries,
      bandGraphWithSeries,
      {
        ...xrayWithSeries,
        threshold: [{ title: '', value: '5e-5' }],
      },
      areaGraphWithSeries,
      windDensityWithSeries,
      windPressureWithSeries,
    ],
    beginTime,
    endTime,
    graphHeight,
    chartHeight,
    onUserAction: (): void => {},
  },
  tags: ['!dev'],
};

export const GraphsDummyData = (): React.ReactElement => {
  return (
    <>
      <ToggleThemeButton />
      <Card
        sx={{
          height: chartHeight,
          border: 'solid 1px',
          borderColor: 'geowebColors.cards.cardContainerBorder',
          boxShadow: 'none',
        }}
      >
        <Graphs
          data={[
            barGraphWithSeries,
            bandGraphWithSeries,
            {
              ...xrayWithSeries,
              threshold: [{ title: '', value: '5e-5' }],
            },
            areaGraphWithSeries,
            windDensityWithSeries,
            windPressureWithSeries,
          ]}
          beginTime={beginTime}
          endTime={endTime}
          graphHeight={graphHeight}
          chartHeight={chartHeight}
          onUserAction={(): void => {}}
        />
      </Card>
    </>
  );
};

export const GraphsLightTheme: Story = {
  render: () => {
    const beginTime = dateUtils.isoStringToDate('2021-12-30T00:00:00Z')!;
    const endTime = dateUtils.isoStringToDate('2022-01-08T00:00:00Z')!;

    return (
      <Card
        sx={{
          height: chartHeight,
          border: 'solid 1px',
          borderColor: 'geowebColors.cards.cardContainerBorder',
          boxShadow: 'none',
        }}
      >
        <Graphs
          data={[
            {
              ...barGraphWithSeriesFixedDates,
              threshold: [
                { title: '', value: 5 },
                { title: '', value: 7 },
              ],
            },
            bandGraphWithSeriesFixedDates,
            {
              ...xrayWithSeriesFixedDates,
              threshold: [{ title: '', value: '5e-5' }],
            },
            areaGraphWithSeriesFixedDates,
            windDensityWithSeriesFixedDates,
            windPressureWithSeriesFixedDates,
          ]}
          beginTime={beginTime}
          endTime={endTime}
          graphHeight={graphHeight}
          chartHeight={chartHeight}
          onUserAction={(): void => {}}
        />
      </Card>
    );
  },
  tags: ['snapshot'],
};

GraphsLightTheme.storyName = 'Graphs Light';

export const GraphsDarkTheme: Story = {
  ...GraphsLightTheme,
  tags: ['snapshot', 'dark'],
};

GraphsDarkTheme.storyName = 'Graphs Dark';
