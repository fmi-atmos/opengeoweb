/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import 'jest-canvas-mock';
import { Graphs } from './Graphs';
import { TestWrapper } from '../../utils/testUtils';
import {
  CHART_TOP_MARGIN,
  CHART_BOTTOM_MARGIN,
  PLOT_TOP_MARGIN,
  config,
} from './utils';
import { barGraphWithSeriesFixedDates } from '../../utils/DummyData';
import { getGraphHeightInPx } from './TimeSeries.utils';

const graphHeight = getGraphHeightInPx();
const chartHeight =
  CHART_TOP_MARGIN +
  PLOT_TOP_MARGIN * 6 +
  graphHeight * 6 +
  CHART_BOTTOM_MARGIN;

describe('components/Timeseries/Graphs', () => {
  it('should render with just the config as data', () => {
    const beginTime = dateUtils.isoStringToDate('2021-12-30T00:00:00Z')!;
    const endTime = dateUtils.isoStringToDate('2022-01-08T00:00:00Z')!;

    const { baseElement } = render(
      <TestWrapper>
        <Graphs
          data={config}
          beginTime={beginTime}
          endTime={endTime}
          graphHeight={graphHeight}
          chartHeight={chartHeight}
          onUserAction={(): void => {}}
        />
      </TestWrapper>,
    );
    // eslint-disable-next-line testing-library/no-node-access
    const canvas = baseElement.querySelector('canvas');
    expect(canvas).toBeInstanceOf(HTMLElement);
  });

  it('should render with series data', () => {
    const beginTime = dateUtils.isoStringToDate('2021-12-30T00:00:00Z')!;
    const endTime = dateUtils.isoStringToDate('2022-01-08T00:00:00Z')!;

    const data = [...config];
    data[0] = barGraphWithSeriesFixedDates;
    const { baseElement } = render(
      <TestWrapper>
        <Graphs
          data={data}
          beginTime={beginTime}
          endTime={endTime}
          graphHeight={graphHeight}
          chartHeight={chartHeight}
          onUserAction={(): void => {}}
        />
      </TestWrapper>,
    );
    // eslint-disable-next-line testing-library/no-node-access
    const canvas = baseElement.querySelector('canvas');
    expect(canvas).toBeInstanceOf(HTMLElement);
  });
});
