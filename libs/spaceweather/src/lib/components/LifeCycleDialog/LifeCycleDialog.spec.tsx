/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';

import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';

import { ThemeWrapper } from '@opengeoweb/theme';
import { setupServer } from 'msw/node';
import { http, HttpResponse } from 'msw';
import LifeCycleDialog, { getDialogTitle } from './LifeCycleDialog';
import {
  fakeEventList,
  mockDraftEvent,
  mockEvent,
  mockEventAcknowledgedExternal,
  mockEventAcknowledgedExternalDraft,
} from '../../utils/fakedata';
import { EventCategory, EventCategoryDetail } from '../../types';

import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { SpaceWeatherApi } from '../../utils/api';
import { i18n, initSpaceweatherI18n } from '../../utils/i18n';
import { requestHandlers } from '../../utils/spaceweather-api/fakeApi';

const server = setupServer(...requestHandlers);

describe('src/components/LifeCycleDialog/LifeCycleDialog', () => {
  beforeAll(() => {
    server.listen();
    initSpaceweatherI18n();
  });
  afterAll(() => {
    server.close();
  });
  afterEach(() => {
    server.resetHandlers();
  });
  it('should show the correct title and content for a new notification', () => {
    const props = {
      open: true,
      dialogMode: 'new',
      event: null!,
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper>
        <LifeCycleDialog {...props} />
      </TestWrapper>,
    );
    expect(screen.getByTestId('lifecycle-dialog')).toBeTruthy();
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      'New Notification',
    );
    expect(screen.getByTestId('edit-lifecycle')).toBeTruthy();
  });

  it('should show the correct title and content for an existing notification from internal provider', async () => {
    const props = {
      open: true,
      dialogMode: mockEvent.eventid,
      event: mockEvent,
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper>
        <LifeCycleDialog {...props} />
      </TestWrapper>,
    );
    expect(screen.getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${
      EventCategoryDetail[
        props.event.categorydetail as keyof typeof EventCategoryDetail
      ]
    }: ${dateUtils.dateToString(
      dateUtils.utc(props.event.lifecycles!.internalprovider!.firstissuetime!),
      `${dateUtils.DATE_FORMAT_DATEPICKER}' UTC'`,
    )}`;
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      expectedTitle,
    );

    await waitFor(() => {
      expect(screen.getByTestId('display-lifecycle')).toBeTruthy();
    });
  });

  it('should show the correct title and content for an existing notification from external provider', async () => {
    const props = {
      open: true,
      dialogMode: mockEventAcknowledgedExternal.eventid,
      event: mockEventAcknowledgedExternal,
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper>
        <LifeCycleDialog {...props} />
      </TestWrapper>,
    );
    expect(screen.getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${
      EventCategoryDetail[
        props.event.categorydetail as keyof typeof EventCategoryDetail
      ]
    }: ${dateUtils.dateToString(
      dateUtils.utc(props.event.lifecycles!.externalprovider!.firstissuetime),
      `${dateUtils.DATE_FORMAT_DATEPICKER}' UTC'`,
    )}`;

    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      expectedTitle,
    );

    await waitFor(() => {
      expect(screen.getByTestId('display-lifecycle')).toBeTruthy();
    });
  });

  it('should show the correct title and content for an existing notification with a draft', async () => {
    const props = {
      open: true,
      dialogMode: mockEventAcknowledgedExternalDraft.eventid,
      event: mockEventAcknowledgedExternalDraft,
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper>
        <LifeCycleDialog {...props} />
      </TestWrapper>,
    );
    expect(screen.getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${
      EventCategoryDetail[
        props.event.categorydetail as keyof typeof EventCategoryDetail
      ]
    }: Draft`;
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      expectedTitle,
    );

    await waitFor(() => {
      expect(screen.getByTestId('display-lifecycle')).toBeTruthy();
    });
  });

  it('should show the confirmation modal when making changes and clicking BACK', async () => {
    const props = {
      open: true,
      dialogMode: mockEventAcknowledgedExternalDraft.eventid,
      event: mockEventAcknowledgedExternalDraft,
      toggleStatus: jest.fn(),
    };
    render(
      <ThemeWrapper>
        <TestWrapper createApi={createFakeApi}>
          <LifeCycleDialog {...props} />
        </TestWrapper>
      </ThemeWrapper>,
    );
    expect(screen.getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${
      EventCategoryDetail[
        props.event.categorydetail as keyof typeof EventCategoryDetail
      ]
    }: Draft`;
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      expectedTitle,
    );

    await waitFor(() => {
      expect(screen.getByTestId('display-lifecycle')).toBeTruthy();
    });

    // make a change
    const textField = screen.getByTestId('notification-text');
    fireEvent.change(textField, {
      target: { value: 'Hello, some new text was added' },
    });
    await screen.findByText('Hello, some new text was added');

    // Click on back button and expect the confirm dialog to be visible
    fireEvent.click(screen.getByTestId('contentdialog-close'));

    await waitFor(() => {
      expect(screen.getByTestId('confirmationDialog')).toBeTruthy();
    });
    expect(screen.getByTestId('customDialog-close')).toBeTruthy();

    // Click on Cancel and expect the confirm dialog to close but the draft to still be visible
    const cancelButton = screen.getByTestId('confirmationDialog-cancel');
    fireEvent.click(cancelButton);
    await waitFor(() => {
      expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
    });
    expect(screen.getByTestId('display-lifecycle')).toBeTruthy();
    expect(screen.getByText('Hello, some new text was added')).toBeTruthy();

    // Click on back button and expect the confirm dialog to be visible
    fireEvent.click(screen.getByTestId('contentdialog-close'));
    await waitFor(() => {
      expect(screen.getByTestId('confirmationDialog')).toBeTruthy();
    });

    // Click on Discard and Close and expect the confirm dialog to close and also the draft to close
    const confirmButton = screen.getByTestId('confirmationDialog-confirm');
    fireEvent.click(confirmButton!);
    await waitFor(() => {
      expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
    });
    expect(props.toggleStatus).toHaveBeenCalled();
  });

  it('should show an error alert banner with the correct content when the call to populate fails', async () => {
    server.use(
      http.get(/.*\/notification\/event\//, () => {
        return HttpResponse.json(fakeEventList[17]);
      }),
    );

    const fakeSpyFunction = jest
      .fn()
      .mockRejectedValue(new Error('test error message for templates'));
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: fakeSpyFunction,
    });

    const props = {
      open: true,
      dialogMode: fakeEventList[17].eventid,
      event: fakeEventList[17],
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleDialog {...props} />
      </TestWrapper>,
    );

    expect(screen.getByTestId('lifecycle-dialog')).toBeTruthy();
    await waitFor(() => {
      expect(screen.getByTestId('updateextend')).toBeTruthy();
    });

    fireEvent.click(screen.getByTestId('updateextend'));

    await waitFor(() => {
      expect(fakeSpyFunction).toHaveBeenCalled();
    });

    expect(
      await screen.findByText('test error message for templates'),
    ).toBeTruthy();
    expect(
      await screen.findByText(
        'An error has occurred while retrieving the notification templates, please try again',
      ),
    ).toBeTruthy();
  });

  it('should show an error alert banner with the correct content when the call to store fails', async () => {
    server.use(
      http.get(/.*\/notification\/event\//, () => {
        return HttpResponse.json(fakeEventList[17]);
      }),
    );

    const fakeIssueFunction = jest
      .fn()
      .mockRejectedValue(
        new Error('test error message for storing notification'),
      );
    const fakeRepopulateFunction = jest.fn().mockResolvedValue({
      data: {
        message: 'fake template returned message',
        title: 'fake template returned title',
      },
    });
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: fakeRepopulateFunction,
      issueNotification: fakeIssueFunction,
    });

    const props = {
      open: true,
      dialogMode: fakeEventList[17].eventid,
      event: fakeEventList[17],
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleDialog {...props} />
      </TestWrapper>,
    );

    expect(screen.getByTestId('lifecycle-dialog')).toBeTruthy();
    await waitFor(() => {
      expect(screen.getByTestId('updateextend')).toBeTruthy();
    });

    fireEvent.click(screen.getByTestId('updateextend'));

    await waitFor(() => {
      expect(fakeRepopulateFunction).toHaveBeenCalled();
    });

    fireEvent.click(screen.getByTestId('draft'));

    expect(
      await screen.findByText('test error message for storing notification'),
    ).toBeTruthy();
    expect(
      await screen.findByText(
        'An error has occurred while saving, please try again',
      ),
    ).toBeTruthy();
  });

  it('should show the correct title and content for an existing notification from internal provider', async () => {
    const props = {
      open: true,
      dialogMode: mockEvent.eventid,
      event: mockEvent,
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper>
        <LifeCycleDialog {...props} />
      </TestWrapper>,
    );
    expect(screen.getByTestId('lifecycle-dialog')).toBeTruthy();

    const expectedTitle = `${
      EventCategoryDetail[
        props.event.categorydetail as keyof typeof EventCategoryDetail
      ]
    }: ${dateUtils.dateToString(
      dateUtils.utc(props.event.lifecycles!.internalprovider!.firstissuetime),
      `${dateUtils.DATE_FORMAT_DATEPICKER}' UTC'`,
    )}`;
    expect(screen.getByTestId('dialogTitle').textContent).toEqual(
      expectedTitle,
    );

    await waitFor(() => {
      expect(screen.getByTestId('display-lifecycle')).toBeTruthy();
    });
  });

  describe('getDialogTitle', () => {
    it('should show the correct title and content for a new notification', () => {
      expect(getDialogTitle(i18n.t, 'new', null!)).toEqual('New Notification');
    });
    it('should show the correct title and content for a draft notification', () => {
      expect(getDialogTitle(i18n.t, 'METRB25', mockDraftEvent)).toEqual(
        `${
          EventCategoryDetail[
            mockDraftEvent.categorydetail as keyof typeof EventCategoryDetail
          ]
        }: Draft`,
      );
    });
    it('should show the correct title and content for a draft notification without categorydetail', () => {
      const eventWithoutCategoryDetail = {
        ...mockDraftEvent,
        categorydetail: null as unknown as EventCategoryDetail,
      };
      expect(
        getDialogTitle(i18n.t, 'METRB25', eventWithoutCategoryDetail),
      ).toEqual(
        `${
          EventCategory[mockDraftEvent.category as keyof typeof EventCategory]
        }: Draft`,
      );
    });
    it('should show the correct title and content for a published notification', () => {
      expect(getDialogTitle(i18n.t, 'METRB1', fakeEventList[3])).toEqual(
        `${
          EventCategory[fakeEventList[3].category as keyof typeof EventCategory]
        }: ${dateUtils.dateToString(
          dateUtils.utc(
            fakeEventList[3].lifecycles!.externalprovider!.firstissuetime,
          ),
          `${dateUtils.DATE_FORMAT_DATEPICKER}' UTC'`,
        )}`,
      );
      expect(getDialogTitle(i18n.t, 'METRB2', fakeEventList[12])).toEqual(
        `${
          EventCategoryDetail[
            fakeEventList[12].categorydetail as keyof typeof EventCategoryDetail
          ]
        }: ${dateUtils.dateToString(
          dateUtils.utc(
            fakeEventList[12].lifecycles!.internalprovider!.firstissuetime,
          ),
          `${dateUtils.DATE_FORMAT_DATEPICKER}' UTC'`,
        )}`,
      );
    });
  });
});
