/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { dateUtils } from '@opengeoweb/shared';
import {
  isCategoryDetailRequired,
  isInPastOrFutureAllowed,
  isRecentPast,
} from './validations';
import {
  i18n,
  initSpaceweatherI18n,
  translateKeyOutsideComponents,
} from '../../../utils/i18n';

beforeAll(() => {
  initSpaceweatherI18n();
});

describe('src/components/LifeCycleDialog/EditFormFields/validations', () => {
  describe('isCategoryDetailRequired', () => {
    it('should give an error when category is other than XRAY_RADIO_BLACKOUT and field is empty', () => {
      expect(isCategoryDetailRequired(i18n.t, '', 'GEOMAGNETIC')).toEqual(
        'This field is required',
      );
      expect(
        isCategoryDetailRequired(i18n.t, 'KP_INDEX', 'GEOMAGNETIC'),
      ).toEqual(true);
      expect(
        isCategoryDetailRequired(i18n.t, '', 'XRAY_RADIO_BLACKOUT'),
      ).toEqual(true);
    });
  });
  describe('isRecentPast', () => {
    it('should give an error when date is more than 1 week in the past', () => {
      const date = dateUtils.dateToString(
        dateUtils.sub(dateUtils.utc(), { hours: 1, days: 7 }),
      )!;
      expect(isRecentPast(i18n.t, date)).toEqual(
        translateKeyOutsideComponents('notification-recent-date'),
      );
    });
    it('should not give an error when date is less than 1 week in the past', () => {
      const date = dateUtils.dateToString(
        dateUtils.sub(dateUtils.utc(), { hours: 23, days: 6 }),
      )!;
      expect(isRecentPast(i18n.t, date)).toEqual(true);
    });
    it('should not give an error when date is in the future', () => {
      const date = dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 1 }),
      )!;
      expect(isRecentPast(i18n.t, date)).toEqual(true);
    });
  });
  describe('isInPastOrFutureAllowed', () => {
    it('should give an error when date is not in the past', () => {
      const date = dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 1 }),
      )!;
      expect(isInPastOrFutureAllowed(i18n.t, date)).toEqual(
        translateKeyOutsideComponents('notification-date-in-past'),
      );
    });
    it('should not give an error when date is in the past', () => {
      const date = dateUtils.dateToString(
        dateUtils.sub(dateUtils.utc(), { hours: 1 }),
      )!;
      expect(isInPastOrFutureAllowed(i18n.t, date)).toEqual(true);
    });
  });
});
