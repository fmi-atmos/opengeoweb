/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';

import { dateUtils } from '@opengeoweb/shared';
import LifeCycleEdit, { WrapperProps } from './LifeCycleEdit';
import { fakeEventList } from '../../utils/fakedata';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { SpaceWeatherApi } from '../../utils/api';
import {
  EventCategory,
  EventCategoryDetail,
  EventLevels,
  ThresholdValues,
} from '../../types';

describe('src/components/LifeCycleDialog/LifeCycleEdit', () => {
  it('should unregister fields for XRAY_RADIO_BLACKOUT when changing category', async () => {
    const mockPopulate = jest.fn().mockResolvedValue({
      data: {
        message: 'fake template message',
        title: 'fake template title',
      },
    });

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: mockPopulate,
    });

    const props: WrapperProps = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: fakeEventList[5], // XRAY_RADIO_BLACKOUT
      setErrorRetrievePopulate: jest.fn(),
      actionMode: 'Summarise',
    };

    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(mockPopulate).toHaveBeenCalled();
    });

    expect(mockPopulate).toHaveBeenCalledWith(
      expect.objectContaining({
        datasource: expect.any(String),
        neweventlevel: expect.any(String),
        peakclass: expect.any(String),
        peakflux: expect.any(Number),
        peakfluxtime: expect.any(String),
        threshold: expect.any(Number),
        thresholdunit: expect.any(String),
        xrayclass: expect.any(String),
        neweventstart: expect.any(String),
        neweventend: expect.any(String),
      }),
    );
    const unregisteredFields1 = [
      'categorydetail',
      'initialgscale',
      'shocktime',
      'observedpolaritybz',
      'observedsolarwind',
    ];
    unregisteredFields1.forEach((field) => {
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: expect.anything() }),
      );
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: undefined }),
      );
    });

    // change category
    fireEvent.mouseDown(screen.getByTestId('category-select'));
    const menuItemCategory = await screen.findByText(EventCategory.GEOMAGNETIC);
    fireEvent.click(menuItemCategory);
    await waitFor(() =>
      expect(screen.getByTestId('category-select').textContent).toEqual(
        EventCategory.GEOMAGNETIC,
      ),
    );

    // fill in required fields
    fireEvent.change(screen.getByTestId('initialgscale-input'), {
      target: { value: '1' },
    });
    fireEvent.change(screen.getByRole('textbox', { name: 'End (UTC Time)' }), {
      target: {
        value: dateUtils.dateToString(
          dateUtils.add(dateUtils.utc(), { hours: 1 }),
          dateUtils.DATE_FORMAT_DATEPICKER,
        ),
      },
    });
    // click populate
    fireEvent.click(screen.getByText('Populate'));

    await waitFor(() => expect(mockPopulate).toHaveBeenCalledTimes(2));
    expect(mockPopulate).toHaveBeenLastCalledWith(
      expect.objectContaining({
        categorydetail: expect.any(String),
        initialgscale: '1',
        neweventend: expect.any(String),
      }),
    );
    const unregisteredFields = [
      'datasource',
      'neweventlevel',
      'threshold',
      'thresholdunit',
      'xrayclass',
      'peakclass',
    ];
    unregisteredFields.forEach((field) => {
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: expect.anything() }),
      );
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: undefined }),
      );
    });
  });

  it('should unregister fields for GEOMAGNETIC when changing category detail', async () => {
    const mockPopulate = jest.fn().mockResolvedValue({
      data: {
        message: 'fake template message',
        title: 'fake template title',
      },
    });

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: mockPopulate,
    });

    const props: WrapperProps = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      setErrorRetrievePopulate: jest.fn(),
    };

    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    // choose category
    fireEvent.mouseDown(screen.getByTestId('category-select'));
    const menuItemCategory = await screen.findByText(EventCategory.GEOMAGNETIC);
    fireEvent.click(menuItemCategory);
    await waitFor(() =>
      expect(screen.getByTestId('category-select').textContent).toEqual(
        EventCategory.GEOMAGNETIC,
      ),
    );

    // fill in required fields
    fireEvent.change(screen.getByTestId('initialgscale-input'), {
      target: { value: '1' },
    });
    fireEvent.change(screen.getByRole('textbox', { name: 'End (UTC Time)' }), {
      target: {
        value: dateUtils.dateToString(
          dateUtils.add(dateUtils.utc(), { hours: 1 }),
          dateUtils.DATE_FORMAT_DATEPICKER,
        ),
      },
    });
    // click populate
    fireEvent.click(screen.getByText('Populate'));
    await waitFor(() => expect(mockPopulate).toHaveBeenCalledTimes(1));

    expect(mockPopulate).toHaveBeenCalledWith(
      expect.objectContaining({
        initialgscale: '1',
        neweventend: expect.any(String),
      }),
    );
    const unregisteredFields = [
      'datasource',
      'neweventlevel',
      'threshold',
      'thresholdunit',
      'xrayclass',
    ];
    unregisteredFields.forEach((field) => {
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: expect.anything() }),
      );
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: undefined }),
      );
    });

    // change category detail
    fireEvent.mouseDown(screen.getByTestId('categorydetail-select'));
    const menuItemCategoryDetail = await screen.findByText(
      EventCategoryDetail.GEOMAGNETIC_SUDDEN_IMPULSE,
    );
    fireEvent.click(menuItemCategoryDetail);
    await waitFor(() =>
      expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
        EventCategoryDetail.GEOMAGNETIC_SUDDEN_IMPULSE,
      ),
    );

    // fill in required fields
    fireEvent.change(screen.getByTestId('datasource-input'), {
      target: { value: '1' },
    });
    fireEvent.change(screen.getByRole('textbox', { name: 'Impulse time' }), {
      target: {
        value: dateUtils.dateToString(
          dateUtils.sub(dateUtils.utc(), { hours: 1 }),
          dateUtils.DATE_FORMAT_DATEPICKER,
        ),
      },
    });
    fireEvent.change(screen.getByTestId('magnetometerdeflection-input'), {
      target: { value: '1' },
    });

    // click populate
    fireEvent.click(screen.getByText('Populate'));
    await waitFor(() => expect(mockPopulate).toHaveBeenCalledTimes(2));

    expect(mockPopulate).toHaveBeenLastCalledWith(
      expect.objectContaining({
        datasource: '1',
        impulsetime: expect.any(String),
        magnetometerdeflection: 1,
      }),
    );
    const unregisteredFields2 = [
      'initialgscale',
      'shocktime',
      'observedpolaritybz',
      'observedsolarwind',
      'neweventend',
    ];
    unregisteredFields2.forEach((field) => {
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: expect.anything() }),
      );
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: undefined }),
      );
    });

    // change category detail
    fireEvent.mouseDown(screen.getByTestId('categorydetail-select'));
    const menuItemCategoryDetailKp = await screen.findByText(
      EventCategoryDetail.KP_INDEX,
    );
    fireEvent.click(menuItemCategoryDetailKp);
    await waitFor(() =>
      expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
        EventCategoryDetail.KP_INDEX,
      ),
    );

    // fill in required fields
    fireEvent.mouseDown(screen.getByTestId('eventlevel-select'));
    const menuItem = await screen.findByText(EventLevels.GEOMAGNETIC[0]);
    fireEvent.click(menuItem);
    await waitFor(() =>
      expect(screen.getByTestId('eventlevel-select').textContent).toEqual(
        EventLevels.GEOMAGNETIC[0],
      ),
    );

    // click populate
    fireEvent.click(screen.getByText('Populate'));
    await waitFor(() => expect(mockPopulate).toHaveBeenCalledTimes(3));

    expect(mockPopulate).toHaveBeenLastCalledWith(
      expect.objectContaining({
        datasource: '1',
        neweventlevel: EventLevels.GEOMAGNETIC[0],
        threshold: ThresholdValues.GEOMAGNETIC[0],
      }),
    );
    const unregisteredFields3 = [
      'impulsetime',
      'magnetometerdeflection',
      'shocktime',
      'observedpolaritybz',
      'observedsolarwind',
      'thresholdunit',
      'neweventend',
    ];
    unregisteredFields3.forEach((field) => {
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: expect.anything() }),
      );
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: undefined }),
      );
    });
  });

  it('should unregister fields for Proton Flux when changing category detail', async () => {
    const mockPopulate = jest.fn().mockResolvedValue({
      data: {
        message: 'fake template message',
        title: 'fake template title',
      },
    });

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: mockPopulate,
    });

    const props: WrapperProps = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      setErrorRetrievePopulate: jest.fn(),
    };

    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    // choose category
    fireEvent.mouseDown(screen.getByTestId('category-select'));
    const menuItemCategory = await screen.findByText(EventCategory.PROTON_FLUX);
    fireEvent.click(menuItemCategory);
    await waitFor(() =>
      expect(screen.getByTestId('category-select').textContent).toEqual(
        EventCategory.PROTON_FLUX,
      ),
    );

    // fill in required fields
    fireEvent.change(screen.getByTestId('datasource-input'), {
      target: { value: '1' },
    });
    fireEvent.mouseDown(screen.getByTestId('threshold-select'));
    const menuItemThreshold = await screen.findByText(
      ThresholdValues.PROTON_FLUX[0],
    );
    fireEvent.click(menuItemThreshold);
    await waitFor(() =>
      expect(screen.getByTestId('threshold-select').textContent).toEqual(
        ThresholdValues.PROTON_FLUX[0].toString(),
      ),
    );

    // click populate
    fireEvent.click(screen.getByText('Populate'));
    await waitFor(() => expect(mockPopulate).toHaveBeenCalledTimes(1));

    expect(mockPopulate).toHaveBeenCalledWith(
      expect.objectContaining({
        datasource: '1',
        threshold: ThresholdValues.PROTON_FLUX[0],
        thresholdunit: expect.any(String),
      }),
    );
    const unregisteredFields = [
      'xrayclass',
      'magnetometerdeflection',
      'observedpolaritybz',
      'observedsolarwind',
      'peakflux',
      'neweventlevel',
      'neweventend',
    ];
    unregisteredFields.forEach((field) => {
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: expect.anything() }),
      );
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: undefined }),
      );
    });

    // change category detail
    fireEvent.mouseDown(screen.getByTestId('categorydetail-select'));
    const menuItemCategoryDetail = await screen.findByText(
      EventCategoryDetail.PROTON_FLUX_10,
    );
    fireEvent.click(menuItemCategoryDetail);
    await waitFor(() =>
      expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
        EventCategoryDetail.PROTON_FLUX_10,
      ),
    );

    // fill in required fields
    fireEvent.mouseDown(screen.getByTestId('eventlevel-select'));
    const menuItemEventLevel = await screen.findByText(
      EventLevels.PROTON_FLUX[0],
    );
    fireEvent.click(menuItemEventLevel);
    await waitFor(() =>
      expect(screen.getByTestId('eventlevel-select').textContent).toEqual(
        EventLevels.PROTON_FLUX[0],
      ),
    );

    // click populate
    fireEvent.click(screen.getByText('Populate'));
    await waitFor(() => expect(mockPopulate).toHaveBeenCalledTimes(2));

    expect(mockPopulate).toHaveBeenLastCalledWith(
      expect.objectContaining({
        datasource: '1',
        threshold: ThresholdValues.PROTON_FLUX[0],
        thresholdunit: expect.any(String),
        neweventlevel: expect.any(String),
      }),
    );
  });

  it('should unregister fields for Electron Flux when changing category detail', async () => {
    const mockPopulate = jest.fn().mockResolvedValue({
      data: {
        message: 'fake template message',
        title: 'fake template title',
      },
    });

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getRePopulateTemplateContent: mockPopulate,
    });

    const props: WrapperProps = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      setErrorRetrievePopulate: jest.fn(),
    };

    render(
      <TestWrapper createApi={fakeApi}>
        <LifeCycleEdit {...props} />
      </TestWrapper>,
    );

    // choose category
    fireEvent.mouseDown(screen.getByTestId('category-select'));
    const menuItemCategory = await screen.findByText(
      EventCategory.ELECTRON_FLUX,
    );
    fireEvent.click(menuItemCategory);
    await waitFor(() =>
      expect(screen.getByTestId('category-select').textContent).toEqual(
        EventCategory.ELECTRON_FLUX,
      ),
    );

    // choose category detail
    fireEvent.mouseDown(screen.getByTestId('categorydetail-select'));
    const menuItemCategoryDetail = await screen.findByText(
      EventCategoryDetail.ELECTRON_FLUENCE,
    );
    fireEvent.click(menuItemCategoryDetail);
    await waitFor(() =>
      expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
        EventCategoryDetail.ELECTRON_FLUENCE,
      ),
    );

    // fill in required fields
    fireEvent.change(screen.getByTestId('datasource-input'), {
      target: { value: '1' },
    });
    fireEvent.change(screen.getByTestId('threshold-input'), {
      target: { value: '1' },
    });
    fireEvent.change(screen.getByRole('textbox', { name: 'End (UTC Time)' }), {
      target: {
        value: dateUtils.dateToString(
          dateUtils.add(dateUtils.utc(), { hours: 1 }),
          dateUtils.DATE_FORMAT_DATEPICKER,
        ),
      },
    });

    // click populate
    fireEvent.click(screen.getByText('Populate'));
    await waitFor(() => expect(mockPopulate).toHaveBeenCalledTimes(1));

    expect(mockPopulate).toHaveBeenLastCalledWith(
      expect.objectContaining({
        datasource: '1',
        threshold: 1,
        thresholdunit: expect.any(String),
        neweventend: expect.any(String),
      }),
    );
    const unregisteredFields = [
      'xrayclass',
      'neweventlevel',
      'magnetometerdeflection',
      'observedpolaritybz',
      'observedsolarwind',
      'peakflux',
    ];
    unregisteredFields.forEach((field) => {
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: expect.anything() }),
      );
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: undefined }),
      );
    });

    // change category detail
    fireEvent.mouseDown(screen.getByTestId('categorydetail-select'));
    const menuItemCategoryDetail2 = await screen.findByText(
      EventCategoryDetail.ELECTRON_FLUX_2,
    );
    fireEvent.click(menuItemCategoryDetail2);
    await waitFor(() =>
      expect(screen.getByTestId('categorydetail-select').textContent).toEqual(
        EventCategoryDetail.ELECTRON_FLUX_2,
      ),
    );

    // click populate
    fireEvent.click(screen.getByText('Populate'));
    await waitFor(() => expect(mockPopulate).toHaveBeenCalledTimes(2));

    expect(mockPopulate).toHaveBeenLastCalledWith(
      expect.objectContaining({
        datasource: '1',
        threshold: 1,
        thresholdunit: expect.any(String),
      }),
    );
    const unregisteredFields2 = ['neweventend'];
    unregisteredFields2.forEach((field) => {
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: expect.anything() }),
      );
      expect(mockPopulate).toHaveBeenLastCalledWith(
        expect.not.objectContaining({ [field]: undefined }),
      );
    });
  });
});
