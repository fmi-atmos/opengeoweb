/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { setupServer } from 'msw/node';
import * as utils from '@opengeoweb/api';
import { http, HttpResponse } from 'msw';
import BulletinHistoryDialog from './BulletinHistoryDialog';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { bulletinEndpoints } from '../../utils/spaceweather-api/fakeApi';

const server = setupServer(...bulletinEndpoints);

describe('src/components/Bulletin/BulletinHistoryDialog', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  afterEach(() => {
    server.resetHandlers();
  });
  it('should render correctly and display the bulletin history list and select the first one ', async () => {
    const props = {
      open: true,
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper createApi={createFakeApi}>
        <BulletinHistoryDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.getAllByTestId('historyItem')).toBeTruthy();
    });
    expect(screen.getByTestId('bulletinviewer')).toBeTruthy();
    expect(screen.queryByTestId('history-dialog-loading')).toBeFalsy();
    expect(screen.queryByTestId('history-dialog-error')).toBeFalsy();

    // check first item is displayed
    expect(screen.getByTestId('bulletinContent').textContent).toContain(
      'First bulletin content',
    );
  });

  it('should display an error if error returned by api', async () => {
    server.use(
      http.get(/.*\/bulletin\/getBulletinHistory$/, () => {
        return HttpResponse.json({ message: 'error' }, { status: 500 });
      }),
    );

    const props = {
      open: true,
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper>
        <BulletinHistoryDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('history-dialog-error')).toBeTruthy();
    });
    expect(screen.queryByTestId('history-dialog-loading')).toBeFalsy();
    expect(screen.queryByTestId('historyItem')).toBeFalsy();
    expect(screen.queryByTestId('bulletinviewer')).toBeFalsy();
  });
  it('should display the loading screen while api is still loading', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValue({
      isLoading: true,
      error: null!,
      result: null,
      fetchApiData: jest.fn(),
    });
    const props = {
      open: true,
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper>
        <BulletinHistoryDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('history-dialog-loading')).toBeTruthy();
    });
    expect(screen.queryByTestId('history-dialog-error')).toBeFalsy();
    expect(screen.queryByTestId('historyItem')).toBeFalsy();
    expect(screen.queryByTestId('bulletinviewer')).toBeFalsy();
  });

  it('should update the selected bulletin when clicking an item in the history list', async () => {
    const props = {
      open: true,
      toggleStatus: jest.fn(),
    };
    render(
      <TestWrapper createApi={createFakeApi}>
        <BulletinHistoryDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.getAllByTestId('historyItem')).toBeTruthy();
    });
    expect(screen.getByTestId('bulletinviewer')).toBeTruthy();
    expect(screen.queryByTestId('history-dialog-loading')).toBeFalsy();
    expect(screen.queryByTestId('history-dialog-error')).toBeFalsy();

    // check first item is displayed
    expect(screen.getByTestId('bulletinContent').textContent).toContain(
      'First bulletin content',
    );

    // click second item
    const historyItems = screen.getAllByTestId('historyItem');
    fireEvent.click(historyItems[1]);

    // check second item is displayed
    expect(screen.getByTestId('bulletinContent').textContent).toContain(
      'Second bulletin content',
    );
  });
});
