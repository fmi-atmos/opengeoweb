/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { setupServer } from 'msw/node';
import * as utils from '@opengeoweb/api';
import { http, HttpResponse } from 'msw';
import Bulletin from './Bulletin';
import { TestWrapper } from '../../utils/testUtils';
import { bulletinEndpoints } from '../../utils/spaceweather-api/fakeApi';

const server = setupServer(...bulletinEndpoints);

describe('src/components/Bulletin/Bulletin', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  afterEach(() => {
    server.resetHandlers();
  });
  it('should show the history dialog', async () => {
    render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('history'));
    expect(screen.getByTestId('history-dialog')).toBeTruthy();

    // When closing the dialog - should be hidden
    fireEvent.click(screen.getByTestId('contentdialog-close'));
    await waitFor(() =>
      expect(screen.queryByTestId('history-dialog')).toBeFalsy(),
    );
  });

  it('should show the bulletin viewer once the latest bulletin has been retrieved', async () => {
    render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    await screen.findByTestId('bulletinviewer');
  });

  it('should show the bulletin tabs and have the technical forecast selected as default', async () => {
    render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(screen.getByTestId('bulletintabs')).toBeTruthy();
    });
    expect(screen.getByTestId('technical').classList).toContain('Mui-selected');
    expect(screen.getByTestId('plain').classList).not.toContain('Mui-selected');
  });
  it('should show as loading as long as the bulletin is still loading', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValue({
      isLoading: true,
      error: null!,
      result: null,
      fetchApiData: jest.fn(),
    });

    render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    expect(screen.getByTestId('bulletintabs')).toBeTruthy();
    expect(screen.getByTestId('bulletin-loading')).toBeTruthy();
    await waitFor(() =>
      expect(screen.queryByTestId('bulletinviewer')).toBeFalsy(),
    );
  });
  it('should show error if bulletin api fails', async () => {
    server.use(
      http.get(/.*\/bulletin\/getBulletin$/, () => {
        return HttpResponse.json({ message: 'error' }, { status: 500 });
      }),
    );

    render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    expect(screen.getByTestId('bulletintabs')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('bulletin-loading')).toBeFalsy();
    });
    expect(screen.getByTestId('bulletin-error')).toBeTruthy();
    expect(screen.getByText('error')).toBeTruthy();
    await waitFor(() =>
      expect(screen.queryByTestId('bulletinviewer')).toBeFalsy(),
    );
  });
  it('should show info message if {} returned from BE for latest bulletin', async () => {
    server.use(
      http.get(/.*\/bulletin\/getBulletin$/, () => {
        return HttpResponse.json({});
      }),
    );

    render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    expect(screen.getByTestId('bulletintabs')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('bulletin-loading')).toBeFalsy();
    });
    expect(screen.getByTestId('bulletin-no-avail-bulletins')).toBeTruthy();
    expect(
      screen.getByText(
        'There are no available bulletins from the last 24 hours',
      ),
    ).toBeTruthy();
    await waitFor(() =>
      expect(screen.queryByTestId('bulletinviewer')).toBeFalsy(),
    );
  });
});
