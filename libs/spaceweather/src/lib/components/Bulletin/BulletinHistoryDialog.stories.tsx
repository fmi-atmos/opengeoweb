/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Theme } from '@mui/material';
import { darkTheme } from '@opengeoweb/theme';
import type { Meta } from '@storybook/react';
import BulletinHistoryDialog from './BulletinHistoryDialog';
import { StoryWrapper } from '../../utils/storybookUtils';

const meta: Meta<typeof BulletinHistoryDialog> = {
  title: 'components/Bulletin Section/BulletinHistoryDialog',
  component: BulletinHistoryDialog,
  tags: ['!autodocs'],
};
export default meta;

interface ThemeProps {
  theme?: Theme;
}

export const BulletinHistoryStory = ({
  theme,
}: ThemeProps): React.ReactElement => (
  <StoryWrapper theme={theme}>
    <BulletinHistoryDialog
      open
      toggleStatus={(): void => {
        /* Do nothing */
      }}
    />
  </StoryWrapper>
);

BulletinHistoryStory.storyName = 'Bulletin History';
BulletinHistoryStory.tags = ['snapshot', '!autodocs'];

export const BulletinHistoryDarkStory = (): React.ReactElement => (
  <BulletinHistoryStory theme={darkTheme} />
);

BulletinHistoryDarkStory.storyName = 'Bulletin History Dark';
BulletinHistoryDarkStory.tags = ['snapshot', '!autodocs'];
