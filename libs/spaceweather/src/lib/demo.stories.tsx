/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Box, useTheme } from '@mui/material';
import { ErrorBoundary } from '@opengeoweb/shared';
import Layout from './components/Layout/Layout';
import { Bulletin } from './components/Bulletin';
import { Notifications } from './components/Notifications';
import {
  NotificationTrigger,
  NotificationTriggerProvider,
} from './components/NotificationTrigger';
import {
  StoryWrapper,
  StoryWrapperWithErrorOnSave,
  ToggleThemeButton,
} from './utils/storybookUtils';
import { TimeSeries } from './components/TimeSeries';

export default { title: 'demo' };

const SpaceWeatherDemoContent: React.FC = () => {
  const theme = useTheme();
  return (
    <NotificationTriggerProvider>
      <Box
        sx={{
          maxHeight: '95vh',
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'hidden',
          backgroundColor: theme.palette.geowebColors.background.surfaceApp,
        }}
      >
        <NotificationTrigger />
        <Box sx={{ overflowY: 'scroll', paddingBottom: '10px' }}>
          <Layout
            leftTop={<Bulletin />}
            leftBottom={<Notifications />}
            right={<TimeSeries />}
          />
        </Box>
      </Box>
    </NotificationTriggerProvider>
  );
};

export const SpaceWeatherDemo = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <ToggleThemeButton />
      <SpaceWeatherDemoContent />
    </StoryWrapper>
  );
};

const SpaceWeatherWithHighLevelErrorBoundaryDemoContent: React.FC = () => {
  const theme = useTheme();
  return (
    <NotificationTriggerProvider>
      <Box
        sx={{
          maxHeight: '100vh',
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'hidden',
          backgroundColor: theme.palette.geowebColors.background.surfaceApp,
        }}
      >
        <NotificationTrigger />
        <ErrorBoundary>
          <Layout
            leftTop={<Bulletin />}
            leftBottom={<Notifications />}
            right={<TimeSeries />}
          />
        </ErrorBoundary>
      </Box>
    </NotificationTriggerProvider>
  );
};

export const SpaceWeatherWithHighLevelErrorBoundaryDemo =
  (): React.ReactElement => {
    return (
      <StoryWrapperWithErrorOnSave>
        <SpaceWeatherWithHighLevelErrorBoundaryDemoContent />
      </StoryWrapperWithErrorOnSave>
    );
  };

const SpaceWeatherWithLowLevelErrorBoundaryDemoContent: React.FC = () => {
  const theme = useTheme();
  return (
    <NotificationTriggerProvider>
      <Box
        sx={{
          maxHeight: '100vh',
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'hidden',
          backgroundColor: theme.palette.geowebColors.background.surfaceApp,
        }}
      >
        <NotificationTrigger />
        <ErrorBoundary>
          <Layout
            leftTop={<Bulletin />}
            leftBottom={<Notifications />}
            right={
              <ErrorBoundary>
                <TimeSeries />
              </ErrorBoundary>
            }
          />
        </ErrorBoundary>
      </Box>
    </NotificationTriggerProvider>
  );
};

export const SpaceWeatherWithLowLevelErrorBoundaryDemo =
  (): React.ReactElement => {
    return (
      <StoryWrapperWithErrorOnSave>
        <SpaceWeatherWithLowLevelErrorBoundaryDemoContent />
      </StoryWrapperWithErrorOnSave>
    );
  };
