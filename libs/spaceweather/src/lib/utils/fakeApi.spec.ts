/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { createApi as createFakeApi, getDummyTimeSerie } from './fakeApi';
import * as apiFuncs from './api';
import { Streams, Parameters } from '../types';
import {
  interplanetaryMagneticFieldBt,
  interplanetaryMagneticFieldBz,
  kpIndexDummyData,
  kpIndexForecastDummyData,
  solarWindDensityDummyData,
  solarWindPressureDummyData,
  solarWindSpeedDummyData,
  xRayFluxDummyData,
} from './DummyData';

describe('src/utils/fakeApi', () => {
  jest.spyOn(console, 'log').mockImplementation();
  describe('createCancelRequestId', () => {
    it('should return correctly formatted request id', async () => {
      const charts = {
        stream: 'test-s-1' as Streams,
        parameter: 'test-p-1' as Parameters,
      };
      expect(apiFuncs.createCancelRequestId(charts)).toEqual(
        '-test-s-1-test-p-1',
      );
    });
  });
  describe('createFakeApi', () => {
    it('should contain all api calls', async () => {
      const api = createFakeApi();
      expect(api.getTimeSeriesMultiple).toBeTruthy();
      expect(api.issueNotification).toBeTruthy();
      expect(api.discardDraftNotification).toBeTruthy();
      expect(api.setAcknowledged).toBeTruthy();
      expect(api.getRePopulateTemplateContent).toBeTruthy();
    });

    it('should getTimeSeries', async () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getTimeSeries');

      expect(spy).not.toHaveBeenCalled();

      const params = {
        parameter: 'bz_gsm' as const,
        stream: 'rtsw_mag' as const,
        time_start: '2022-09-02T14:16:04Z',
        time_stop: '2022-09-06T14:16:04Z',
      };
      const requestId = 'dummy-requestId';
      await api.getTimeSeries(params, requestId);
      expect(spy).toHaveBeenCalledWith(params, requestId);
    });

    it('should getTimeSeriesMultiple', async () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getTimeSeriesMultiple');

      expect(spy).not.toHaveBeenCalled();

      const params = [
        {
          parameter: 'bz_gsm' as const,
          stream: 'rtsw_mag' as const,
          time_start: '2022-09-02T14:16:04Z',
          time_stop: '2022-09-06T14:16:04Z',
        },
      ];
      await api.getTimeSeriesMultiple(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should getStreams', async () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getStreams');

      expect(spy).not.toHaveBeenCalled();

      const params = { stream: 'rtsw_mag' as const };
      await api.getStreams(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should issueNotification', async () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'issueNotification');

      expect(spy).not.toHaveBeenCalled();

      const params = {
        IS_POPULATE_REQUEST: false,
        category: 'ELECTRON_FLUX',
        categorydetail: 'ELECTRON_FLUX_2',
        label: 'ALERT',
        datasource: 'GOES16',
        threshold: 1000,
        thresholdunit: 'particles cm^-2 s^-1 sr^-1',
        neweventstart: '2022-09-05T11:20:00Z',
        message:
          'Vanaf 2022-09-05 11:20:00 UTC (2022-09-05 13:20:00 lokale tijd) is de flux van energetische elektronen (>2 MeV), gemeten door de GOES16 satelliet, boven de drempelwaarde van 1000.0 pfu uitgekomen. Hierdoor is er voor satellieten in de geostationaire ring een verhoogde kans op interne opbouw van elektrische lading, wat kan resulteren in schade aan de elektronica aan boord van deze satellieten. Operators van deze satellieten kunnen hiermee rekening houden bij het plannen van hun operaties.\n',
        changestateto: 'issued',
        draft: false,
        notificationid: '74647448-90f4-41f3-b6f0-d6733c6efd71',
        eventid: '74647448-90f4-41f3-b6f0-d6733c6efd71',
        issuetime: '2022-09-05T11:37:01Z',
        neweventend: '',
      };
      await api.issueNotification(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should discardDraftNotification', async () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'discardDraftNotification');

      expect(spy).not.toHaveBeenCalled();

      const params = { notificationid: 'rtsw_mag', eventid: 'test' };
      await api.discardDraftNotification(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should setAcknowledged', async () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'setAcknowledged');

      expect(spy).not.toHaveBeenCalled();

      const params = 'event-id';
      await api.setAcknowledged(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should getRePopulateTemplateContent', async () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getRePopulateTemplateContent');

      expect(spy).not.toHaveBeenCalled();

      const params = {
        IS_POPULATE_REQUEST: false,
        category: 'ELECTRON_FLUX',
        categorydetail: 'ELECTRON_FLUX_2',
        label: 'ALERT',
        datasource: 'GOES16',
        threshold: 1000,
        thresholdunit: 'particles cm^-2 s^-1 sr^-1',
        neweventstart: '2022-09-05T11:20:00Z',
        message:
          'Vanaf 2022-09-05 11:20:00 UTC (2022-09-05 13:20:00 lokale tijd) is de flux van energetische elektronen (>2 MeV), gemeten door de GOES16 satelliet, boven de drempelwaarde van 1000.0 pfu uitgekomen. Hierdoor is er voor satellieten in de geostationaire ring een verhoogde kans op interne opbouw van elektrische lading, wat kan resulteren in schade aan de elektronica aan boord van deze satellieten. Operators van deze satellieten kunnen hiermee rekening houden bij het plannen van hun operaties.\n',
        changestateto: 'issued',
        draft: false,
        notificationid: '74647448-90f4-41f3-b6f0-d6733c6efd71',
        eventid: '74647448-90f4-41f3-b6f0-d6733c6efd71',
        issuetime: '2022-09-05T11:37:01Z',
        neweventend: '',
      };
      await api.getRePopulateTemplateContent(params);
      expect(spy).toHaveBeenCalledWith(params);
    });
  });
  describe('getDummyTimeSerie', () => {
    it('should return correct data for Kp and KpForecast', async () => {
      expect(getDummyTimeSerie('kp', 'kp')).toEqual(kpIndexDummyData);
      expect(getDummyTimeSerie('kpforecast', 'kpforecast')).toEqual(
        kpIndexForecastDummyData,
      );
    });
    it('should return correct data for Kp and KpForecast', async () => {
      expect(getDummyTimeSerie('kp', 'kp')).toEqual(kpIndexDummyData);
      expect(getDummyTimeSerie('kpforecast', 'kpforecast')).toEqual(
        kpIndexForecastDummyData,
      );
    });
    it('should return correct data for rtsw_mag', async () => {
      expect(getDummyTimeSerie('rtsw_mag', 'bt')).toEqual(
        interplanetaryMagneticFieldBt,
      );
      expect(getDummyTimeSerie('rtsw_mag', 'bz_gsm')).toEqual(
        interplanetaryMagneticFieldBz,
      );
    });
    it('should return correct data for rtsw_wind', async () => {
      expect(getDummyTimeSerie('rtsw_wind', 'proton_speed')).toEqual(
        solarWindSpeedDummyData,
      );
      expect(getDummyTimeSerie('rtsw_wind', 'proton_pressure')).toEqual(
        solarWindPressureDummyData,
      );
      expect(getDummyTimeSerie('rtsw_wind', 'proton_density')).toEqual(
        solarWindDensityDummyData,
      );
    });
    it('should return correct data for xray', async () => {
      expect(getDummyTimeSerie('xray', 'solar_x_ray_flux_long')).toEqual(
        xRayFluxDummyData,
      );
    });
  });
});
