/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { createTheme } from '@mui/material';
import { ThemeWrapper, lightTheme } from '@opengeoweb/theme';
import { ApiProvider, AuthenticationProvider } from '@opengeoweb/api';
import { AxiosError, AxiosResponseHeaders } from 'axios';
import mediaQuery from 'css-mediaquery';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { createApi as createFakeApi } from './fakeApi';
import { SpaceWeatherApi } from './api';
import { SpaceweatherI18nProvider } from '../components/Providers/Providers';
import { MOCK_USERNAME } from './spaceweather-api/fakeApi';

interface WrapperProps {
  children: React.ReactNode;
  createApi?: () => SpaceWeatherApi;
}

export const TestWrapper: React.FC<WrapperProps> = ({
  children,
  createApi = createFakeApi,
}: WrapperProps) => {
  const queryClient = new QueryClient({
    defaultOptions: { queries: { retry: false } },
  });
  return (
    <SpaceweatherI18nProvider>
      <AuthenticationProvider
        value={{
          isLoggedIn: true,
          auth: {
            username: MOCK_USERNAME,
            token: '1223344',
            refresh_token: '33455214',
          },
          onLogin: (): void => null!,
          onSetAuth: (): void => null!,
          sessionStorageProvider: null!,
        }}
      >
        <ApiProvider createApi={createApi}>
          <QueryClientProvider client={queryClient}>
            <ThemeWrapper theme={lightTheme}>
              <ConfirmationServiceProvider>
                {children}
              </ConfirmationServiceProvider>
            </ThemeWrapper>
          </QueryClientProvider>
        </ApiProvider>
      </AuthenticationProvider>
    </SpaceweatherI18nProvider>
  );
};

// helper for testing media queries https://mui.com/components/use-media-query/#testing
function createMatchMedia(width: number) {
  return (query: string): unknown => ({
    matches: mediaQuery.match(query, {
      width,
    }),
    addEventListener: (): void => {},
    removeEventListener: (): void => {},
  });
}

interface SizeWrapperProps {
  children: React.ReactNode;
  width?: 'xs' | 'sm' | 'md' | 'lg' | 'xl';
}
export const SizeWrapper: React.FC<SizeWrapperProps> = ({
  children,
  width = 'lg',
}: SizeWrapperProps) => {
  const theme = createTheme();
  const screenSize = theme.breakpoints.values[width];
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  window.matchMedia = createMatchMedia(screenSize);

  return (
    <SpaceweatherI18nProvider>
      <ThemeWrapper theme={theme}>{children}</ThemeWrapper>
    </SpaceweatherI18nProvider>
  );
};

export const fakeBackendError: AxiosError = {
  isAxiosError: true,
  config: undefined!,
  toJSON: undefined!,
  name: 'API error',
  message: '',
  response: {
    data: 'Unable to store data',
    status: 400,
    statusText: '',
    config: undefined!,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};
