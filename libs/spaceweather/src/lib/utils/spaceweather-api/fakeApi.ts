/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { http, HttpResponse } from 'msw';
import {
  fakeBulletins,
  fakeEventList,
  fakeEventListFixedDates,
} from '../fakedata';
import { EventCategoryParams, SWEvent } from '../../types';

export const MOCK_USERNAME = 'user.name';

// get event list /eventlist - possible option to pass: category
export const fetchEventList = (
  params = {} as EventCategoryParams,
  fixedDates = false,
): SWEvent[] => {
  const list = fixedDates ? fakeEventListFixedDates : fakeEventList;
  if (params?.category) {
    return list.filter((event) => event.category === params.category);
  }
  if (params.originator && params.originator === 'KNMI') {
    return list.filter((event) => event.originator === 'KNMI');
  }
  return list;
};

// Bulletin section
export const bulletinEndpoints = [
  http.get(/.*\/bulletin\/getBulletin$/, () => {
    return HttpResponse.json(fakeBulletins[0]);
  }),
  http.get(/.*\/bulletin\/getBulletinHistory$/, () => {
    return HttpResponse.json(fakeBulletins);
  }),
];

// Notification section
export const notificationEndpoints = [
  http.get(/.*\/notification\/eventList$/, ({ request }) => {
    const url = new URL(request.url);
    const params: Record<string, string> = {};
    for (const [key, value] of url.searchParams.entries()) {
      params[key] = value;
    }

    return HttpResponse.json(fetchEventList(params as EventCategoryParams));
  }),
  http.get(/.*\/notification\/event\//, ({ request }) => {
    const url = new URL(request.url);
    const eventid = url.pathname.split('/').pop();
    return HttpResponse.json(
      fakeEventList.find((fakeEvent) => fakeEvent.eventid === eventid),
    );
  }),
  http.get(/.*\/notification\/newNotifications$/, () => {
    return HttpResponse.json(
      fakeEventList.filter(
        (event) => event.originator === 'METOffice' && event.notacknowledged,
      ),
    );
  }),
];

export const fixedNotificationDateEndpoint = [
  http.get(/.*\/notification\/event\//, ({ request }) => {
    const url = new URL(request.url);
    const eventid = url.pathname.split('/').pop();
    return HttpResponse.json(
      fakeEventListFixedDates.find(
        (fakeEvent) => fakeEvent.eventid === eventid,
      ),
    );
  }),
];

export const requestHandlers = [...bulletinEndpoints, ...notificationEndpoints];
