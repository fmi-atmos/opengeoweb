/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useAuthenticationContext, useAuthQuery } from '@opengeoweb/api';
import { useQuery, UseQueryResult } from '@tanstack/react-query';
import {
  getBulletin,
  getBulletinHistoryList,
  getEvent,
  getEventList,
  getNewNotifications,
} from './api';
import { Bulletin, EventCategoryParams, SWEvent } from '../../types';

// Bulletin section
export const useBulletin = (): UseQueryResult<Bulletin> =>
  useAuthQuery(['bulletin'], getBulletin, false, 5 * 60 * 1000);

export const useBulletinHistory = (): UseQueryResult<Bulletin[]> =>
  useAuthQuery(['bulletin-history-list'], getBulletinHistoryList, false);

// Notification section
export const useEventList = (
  params: EventCategoryParams,
): UseQueryResult<SWEvent[]> => {
  const { auth } = useAuthenticationContext();
  return useQuery({
    queryKey: ['event-list', params],
    queryFn: (): Promise<SWEvent[]> => getEventList(auth!, params),
    enabled: !!auth,
    refetchInterval: 5 * 60 * 1000,
  });
};

export const useEvent = (eventid: string): UseQueryResult<SWEvent> => {
  const { auth } = useAuthenticationContext();
  return useQuery({
    queryKey: ['event', eventid],
    queryFn: (): Promise<SWEvent> => getEvent(auth!, eventid),
    enabled: !!auth,
  });
};

export const useNewNotifications = (): UseQueryResult<SWEvent[]> =>
  useAuthQuery(
    ['new-notifications'],
    getNewNotifications,
    false,
    1 * 60 * 1000,
  );
