/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { SystemScope } from '@opengeoweb/shared';
import { CoreAppStore, serviceTypes } from '@opengeoweb/store';
import { EntityState } from '@reduxjs/toolkit';

export type RootState = CoreAppStore & LayerSelectModuleState;

export interface LayerWithServiceName extends serviceTypes.ServiceLayer {
  serviceName: string;
}

export interface ActiveServiceObject {
  serviceId?: string;
  serviceName?: string;
  serviceUrl?: string;
  abstract?: string;
  enabled?: boolean;
  filterIds?: string[];
  scope?: SystemScope;
  isLoading?: boolean;
  keywords?: string[];
  groups?: string[];
  isInSearch?: boolean;
}

export type FilterType = 'keywords' | 'groups';

export interface Filter {
  id: string;
  name: string;
  checked?: boolean;
  amountVisible?: number;
  type: FilterType;
}

// entities part of ActiveServiceType
export type ActiveServiceObjectEntities = Record<string, ActiveServiceObject>;

export type Filters = EntityState<Filter>;
export type ActiveServiceType = EntityState<ActiveServiceObject>;

export interface FiltersType {
  searchFilter: string;
  activeServices: ActiveServiceType;
  filters: Filters;
}

export type PopupVariant = 'edit' | 'add' | 'save' | 'show';

export interface ServicePopupObject {
  isOpen: boolean;
  variant: PopupVariant;
  url?: string;
  serviceId?: string;
}
export interface LayerSelectStoreType {
  filters: FiltersType;
  allServicesEnabled: boolean;
  servicePopup: ServicePopupObject;
}

export interface LayerSelectModuleState {
  layerSelect?: LayerSelectStoreType;
}

// actions
export interface SetSearchFilterPayload {
  filterText: string;
}

export interface SetAllServicesEnabledPayload {
  allServicesEnabled: boolean;
}
export interface ToggleServicePopupPayload {
  url?: string;
  variant: PopupVariant;
  serviceId?: string;
  isOpen: boolean;
}

export interface LayerSelectRemoveServicePayload {
  serviceId: string;
  serviceUrl?: string;
}

export interface ToggleKeywordsPayload {
  filterId: string;
  only?: boolean;
}
