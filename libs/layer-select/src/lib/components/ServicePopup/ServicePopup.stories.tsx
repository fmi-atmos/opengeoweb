/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import type { Meta, StoryObj } from '@storybook/react';
import { ServicePopup } from './ServicePopup';
import { layerSelectTypes } from '../../store';

const meta: Meta<typeof ServicePopup> = {
  title: 'components/ServicePopup',
  component: ServicePopup,
  parameters: {
    docs: {
      description: {
        component:
          'A component for showing the user ServicePopup. Set the isOpen toggle to true to open the dialog.',
      },
    },
  },
  argTypes: {},
};
export default meta;

type Story = StoryObj<typeof ServicePopup>;

export const Component: Story = {
  args: {
    isOpen: false,
  },
};

const zeplinLight = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62c695a1ed42f01adee34e63/version/632b1738c788c83cc9d5da8d',
    },
  ],
};
const zeplinDark = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d7b06898ff1c11dbcc0d0a/version/632b1ae593c99c3b8f379504',
    },
  ],
};

const serviceId = 'serviceid_1';
const serviceUrl = 'https://halo-wms.met.no/halo/default.map?';
const serviceName = 'Radar Norway';

const ServicePopupTemplate: React.FC<{
  variant: layerSelectTypes.PopupVariant;
}> = ({ variant }) => {
  const isShowPopup = variant === 'show';
  return (
    <ServicePopup
      serviceId={isShowPopup ? serviceId : undefined}
      serviceUrl={isShowPopup ? serviceUrl : undefined}
      serviceName={isShowPopup ? serviceName : undefined}
      servicePopupVariant={variant}
      hideBackdrop
      isOpen={true}
      shouldDisableAutoFocus={true} // Autofocus will be deliberately disabled here to prevent cursor blinking affect snapshots
      serviceSetLayers={(): void => {}}
      services={{}}
    />
  );
};

export const ServicePopupAddLight = (): React.ReactElement => (
  <ServicePopupTemplate variant="add" />
);

ServicePopupAddLight.tags = ['snapshot', '!autodocs'];
ServicePopupAddLight.parameters = zeplinLight;

export const ServicePopupAddDark = (): React.ReactElement => (
  <ServicePopupTemplate variant="add" />
);

ServicePopupAddDark.tags = ['snapshot', '!autodocs', 'dark'];
ServicePopupAddDark.parameters = zeplinDark;

export const ServicePopupShowLight = (): React.ReactElement => (
  <ServicePopupTemplate variant="show" />
);

ServicePopupShowLight.tags = ['snapshot', '!autodocs'];
ServicePopupShowLight.parameters = zeplinLight;

export const ServicePopupShowDark = (): React.ReactElement => (
  <ServicePopupTemplate variant="show" />
);

ServicePopupShowDark.tags = ['snapshot', '!autodocs', 'dark'];
ServicePopupShowDark.parameters = zeplinDark;
