/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as webmap from '@opengeoweb/webmap';

import { setWMSGetCapabilitiesFetcher } from '@opengeoweb/webmap';
import {
  loadWMSService,
  validateServiceName,
  validateServiceUrl,
} from './utils';
import {
  i18n,
  initLayerSelectI18n,
  translateKeyOutsideComponents,
} from '../../utils/i18n';

const { mockGetCapabilities } = webmap;

beforeAll(() => {
  initLayerSelectI18n();
});

describe('src/lib/components/LayerManager/ServicePopup/ServicePopup', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });

  describe('validateServiceName', () => {
    it('should validate ServiceName for existing names', () => {
      expect(validateServiceName(i18n.t, 'test', {})).toBeTruthy();
      expect(
        validateServiceName(i18n.t, 'testing', {
          test: { serviceName: 'test' },
        }),
      ).toBeTruthy();

      expect(
        validateServiceName(i18n.t, 'test', { test: { serviceName: 'test' } }),
      ).toEqual(translateKeyOutsideComponents('validations-name-existing'));
      expect(
        validateServiceName(i18n.t, 'Test', { test: { serviceName: 'test' } }),
      ).toEqual(translateKeyOutsideComponents('validations-name-existing'));
    });
    it('should validate but ignore own name', () => {
      expect(
        validateServiceName(
          i18n.t,
          'test',
          { test: { serviceName: 'test' } },
          'test',
        ),
      ).toBeTruthy();
      expect(
        validateServiceName(
          i18n.t,
          'Test',
          { test: { serviceName: 'test' } },
          'Test',
        ),
      ).toBeTruthy();

      expect(
        validateServiceName(
          i18n.t,
          'testing',
          { testing: { serviceName: 'testing' } },
          'Test',
        ),
      ).toEqual(translateKeyOutsideComponents('validations-name-existing'));
    });
  });

  describe('validateServiceUrl', () => {
    it('should validate validateServiceUrl URL is valid', () => {
      expect(validateServiceUrl(i18n.t, 'http://www.test.nl', {})).toBeTruthy();
      expect(
        validateServiceUrl(i18n.t, 'https://www.test.nl', {}),
      ).toBeTruthy();

      expect(validateServiceUrl(i18n.t, '', {})).toEqual(
        translateKeyOutsideComponents('validations-service-valid-url'),
      );
      expect(validateServiceUrl(i18n.t, undefined!, {})).toEqual(
        translateKeyOutsideComponents('validations-service-valid-url'),
      );
      expect(validateServiceUrl(i18n.t, '://www.test.nl', {})).toEqual(
        translateKeyOutsideComponents('validations-service-valid-url'),
      );
      expect(validateServiceUrl(i18n.t, 'www.test.nl', {})).toEqual(
        translateKeyOutsideComponents('validations-service-valid-url'),
      );
      expect(validateServiceUrl(i18n.t, 'test.nl', {})).toEqual(
        translateKeyOutsideComponents('validations-service-valid-url'),
      );
    });

    it('should validate validateServiceUrl for existing Urls', () => {
      expect(
        validateServiceUrl(i18n.t, 'http://www.test.nl', {
          test: { serviceUrl: 'https://www.test.nl' },
        }),
      ).toBeTruthy();
      expect(
        validateServiceUrl(i18n.t, 'http://www.test.nl', {
          test: { serviceUrl: 'http://www.test.nl' },
        }),
      ).toEqual(translateKeyOutsideComponents('validations-service-existing'));
      expect(
        validateServiceUrl(i18n.t, 'https://www.test.nl', {
          test: { serviceUrl: 'https://www.test.nl' },
        }),
      ).toEqual(translateKeyOutsideComponents('validations-service-existing'));
    });
  });
  describe('loadWMSService', () => {
    it('should fetch layers', async () => {
      const layers = await loadWMSService(
        mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      );
      expect(layers.abstract).toBe('Service Abstract');
      expect(layers.layers).toHaveLength(2);
    });

    it('should fetch and return no layers', async () => {
      const layers = await loadWMSService(
        mockGetCapabilities.MOCK_URL_NO_CHILDREN,
      );
      expect(layers.abstract).toBe('Service Abstract');
      expect(layers.layers).toHaveLength(0);
    });
  });
});
