/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import * as webmap from '@opengeoweb/webmap';

import { setWMSGetCapabilitiesFetcher } from '@opengeoweb/webmap';
import {
  ServicePopup,
  getSuccesMessage,
  ServicePopupProps,
} from './ServicePopup';

import * as utils from './utils';
import { layerSelectTypes } from '../../store';
import {
  initLayerSelectI18n,
  translateKeyOutsideComponents,
} from '../../utils/i18n';
import { DemoWrapper } from '../Providers/Providers';

const { mockGetCapabilities } = webmap;

const mockServiceFromStore = {
  id: 'serviceid_1',
  title: 'Service test title',
  abstract: 'Service Abstract',
};

const serviceUrlLabel = /this service refers to this url/i;
const serviceNameLabel = /service name/i;
const serviceAbstractLabel = /abstracts/i;

describe('src/lib/components/LayerManager/ServicePopup/ServicePopup', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  const mockServices: layerSelectTypes.ActiveServiceObjectEntities = {
    serviceid_1: {
      serviceName: 'Radar Norway',
      serviceUrl: 'https://halo-wms.met.no/halo/default.map?',
      enabled: true,
      filterIds: [],
      scope: 'user',
    },
  };

  jest
    .spyOn(webmap, 'queryWMSLayersTree')
    .mockImplementation(mockGetCapabilities.mockGetLayersFromService);

  jest.spyOn(webmap, 'queryWMSLayers').mockImplementation(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    mockGetCapabilities.mockGetLayersFlattenedFromService as any,
  );

  const user = userEvent.setup();

  it('should render correctly', async () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );
    expect(screen.getByRole('dialog').textContent).toBeTruthy();
  });

  it('should render the add title when using the add variant', () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );
    expect(screen.getByRole('heading').textContent).toBe(
      translateKeyOutsideComponents('add-a-new-service'),
    );

    const input = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    expect(input.getAttribute('disabled')).toBeFalsy();
  });

  it('should render correct abstract', () => {
    const testAbstract = 'testing abstract';
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="edit"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceAbstracts={testAbstract}
        />
      </DemoWrapper>,
    );

    expect(
      screen.getByRole('textbox', {
        name: serviceAbstractLabel,
      }).innerHTML,
    ).toEqual(testAbstract);
  });

  it('should render the edit title when using the edit variant', () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="edit"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );
    expect(screen.getByRole('heading').textContent).toBe(
      translateKeyOutsideComponents('edit-service-title'),
    );

    expect(
      screen
        .getByRole('textbox', {
          name: serviceUrlLabel,
        })
        .getAttribute('disabled'),
    ).toBeDefined();
  });

  it('should render the correct values for edit variant', () => {
    const props = {
      serviceUrl: 'http://www.test.nl',
      serviceName: 'test name',
      serviceAbstracts: 'test abstract',
    };

    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="edit"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
          {...props}
        />
      </DemoWrapper>,
    );
    expect(
      screen
        .getByRole('textbox', {
          name: serviceUrlLabel,
        })
        .getAttribute('value'),
    ).toEqual(props.serviceUrl);
    expect(
      screen
        .getByRole('textbox', {
          name: serviceNameLabel,
        })
        .getAttribute('value'),
    ).toEqual(props.serviceName);
    expect(
      screen.getByRole('textbox', {
        name: serviceAbstractLabel,
      }).innerHTML,
    ).toEqual(props.serviceAbstracts);

    const button = screen.getByTestId('saveServiceButton');
    expect(button.getAttribute('disabled')).toBeFalsy();
  });

  it('should render the save title when using the save variant', () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );
    expect(screen.getByRole('heading').textContent).toBe(
      translateKeyOutsideComponents('save-service-title'),
    );
  });

  it('should call the function that closes the popup on click', () => {
    const closePopup = jest.fn();
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={closePopup}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );

    fireEvent.click(screen.getByRole('button', { name: /close/i }));
    expect(closePopup).toHaveBeenCalledTimes(1);

    fireEvent.click(screen.getByRole('button', { name: /cancel/i }));
    expect(closePopup).toHaveBeenCalledTimes(2);
  });

  it('should not show dialog when isOpen = false', () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={jest.fn()}
          isOpen={false}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );

    expect(screen.queryByRole('dialog')).toBeFalsy();
  });

  it('should prefill the url field when activeService has serviceUrl', async () => {
    const testUrl = 'https://google.com';
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          services={mockServices}
          isOpen
          serviceUrl={testUrl}
          serviceSetLayers={jest.fn()}
        />
      </DemoWrapper>,
    );

    await waitFor(() => {
      const urlField = screen.getByRole('textbox', {
        name: serviceUrlLabel,
      }) as HTMLInputElement;
      expect(urlField.value).toBe(testUrl);
    });
  });

  it('should disable save button if url aready exists in store', async () => {
    const testUrl = 'https://halo-wms.met.no/halo/default.map?';

    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceUrl={testUrl}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );

    const serviceUrlTextField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlTextField, { target: { value: testUrl } });
    await waitFor(() => {
      const saveButton = screen.getByText('save') as HTMLButtonElement;
      expect(saveButton.disabled).toBe(true);
    });
  });

  it('should disable save button if service name aready exists in store', async () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );

    const serviceNameTextField = screen.getByRole('textbox', {
      name: /service name/i,
    });
    fireEvent.change(serviceNameTextField, {
      target: { value: 'Radar Norway' },
    });

    await waitFor(() => {
      const saveButton = screen.getByText('save') as HTMLButtonElement;
      expect(saveButton.disabled).toBe(true);
    });
  });

  it('should show error message, if service URL is already in store', async () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlField, {
      target: { value: 'https://halo-wms.met.no/halo/default.map?' },
    });

    const validationServiceExisting = translateKeyOutsideComponents(
      'validations-service-existing',
    );

    const errorText = await screen.findByText(validationServiceExisting);
    expect(errorText).toBeTruthy();
  });

  it('should show error message, if service name is already in store', async () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: /service name/i,
    });

    fireEvent.change(serviceNameField, {
      target: { value: 'Radar Norway' },
    });

    expect(
      await screen.findByText(
        translateKeyOutsideComponents('validations-name-existing'),
      ),
    ).toBeInTheDocument();
  });

  it('should show error message, if service url is invalid', async () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlField, {
      target: { value: 'asdasdasds' },
    });

    const errorText = await screen.findByText(
      translateKeyOutsideComponents('validations-service-valid-url'),
    );
    expect(errorText).toBeTruthy();
  });

  it('should show error message, if download fails to start', async () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );

    const serviceNameTextField = screen.getByRole('textbox', {
      name: /service name/i,
    });
    fireEvent.change(serviceNameTextField, {
      target: { value: 'New service' },
    });

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlField, {
      target: { value: 'https://asdasd' },
    });
    fireEvent.blur(serviceUrlField, {
      target: { value: 'https://asdasd' },
    });

    const saveButton = screen.getByTestId(
      'saveServiceButton',
    ) as HTMLButtonElement;

    fireEvent.click(saveButton);

    const validationRequestFailed = translateKeyOutsideComponents(
      'validations-request-failed',
    );
    const errorText = await screen.findByText(validationRequestFailed);
    expect(errorText).toBeTruthy();
  });

  it('should hide buttons and disable input if show mode', async () => {
    Object.defineProperty(navigator, 'clipboard', {
      value: {
        writeText: () => {},
      },
      writable: true,
      configurable: true,
    });
    jest.spyOn(navigator.clipboard, 'writeText');

    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="show"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceId="serviceid_1"
        />
      </DemoWrapper>,
    );

    expect(
      screen.getByRole('heading', { name: /service/i }),
    ).toBeInTheDocument();

    expect(
      screen.getAllByRole('button', {
        name: /close/i,
      }),
    ).toHaveLength(2);

    expect(
      screen.queryByRole('button', {
        name: /save/i,
      }),
    ).not.toBeInTheDocument();

    expect(
      screen.getByRole('textbox', { name: /service name/i }),
    ).toHaveAttribute('readonly');
    expect(
      screen.getByRole('textbox', { name: serviceUrlLabel }),
    ).toHaveAttribute('readonly');
    expect(screen.getByRole('textbox', { name: /abstracts/i })).toHaveAttribute(
      'readonly',
    );

    const copyButton = screen.getByRole('button', { name: /copy url/i });
    await user.click(copyButton);
    expect(navigator.clipboard.writeText).toHaveBeenCalled();
  });

  it('should show error message on focus if service name is invalid', async () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceUrl="https://halo-wms.met.no/halo/default.map?"
        />
      </DemoWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    fireEvent.focus(serviceNameField);

    expect(
      await screen.findByText(
        translateKeyOutsideComponents('validations-service-existing'),
      ),
    ).toBeInTheDocument();
  });

  it('should NOT show error message on focus if service name is blank', async () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceUrl=""
        />
      </DemoWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    fireEvent.focus(serviceNameField);
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('validations-field-required'),
      ),
    ).not.toBeInTheDocument();
  });

  it('should start fetching service data when blur on service url field and prefill name and abstract', async () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={{}}
          serviceName="test service"
          serviceUrl="https://mockUrlWithChildren.nl"
        />
      </DemoWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    const serviceNameField = screen.getByRole('textbox', {
      name: serviceNameLabel,
    });

    const serviceAbstractField = screen.getByRole('textbox', {
      name: serviceAbstractLabel,
    });

    expect(serviceNameField.getAttribute('value')).toEqual('test service');
    expect(serviceAbstractField.innerHTML).toEqual('');
    fireEvent.blur(serviceUrlField);

    expect(serviceUrlField.getAttribute('disabled')).toBeDefined();

    expect(
      screen
        .getByRole('textbox', {
          name: serviceUrlLabel,
        })
        .getAttribute('disabled'),
    ).toBeDefined();

    expect(
      screen.getByTestId('saveServiceButton').getAttribute('disabled'),
    ).toBeDefined();

    await waitFor(() => {
      expect(serviceNameField.getAttribute('value')).toEqual(
        mockServiceFromStore.title,
      );
    });
    expect(serviceAbstractField.innerHTML).toEqual(
      mockServiceFromStore.abstract,
    );
  });

  it('should show error message, if download fails to start on blur', async () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );

    const urlInput = screen.getByRole('textbox', { name: serviceUrlLabel });
    fireEvent.change(urlInput, { target: { value: 'not a valid url' } });
    fireEvent.blur(urlInput);

    const errorText = await screen.findByText(
      translateKeyOutsideComponents('validations-service-valid-url'),
    );
    expect(errorText).toBeTruthy();
  });

  it('should start fetching if url is valid', async () => {
    const mockServiceUrl = 'https://mockUrlWithChildren.nl';
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceUrl={mockServiceUrl}
        />
      </DemoWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    const serviceNameField = screen.getByRole('textbox', {
      name: serviceNameLabel,
    });

    const serviceAbstract = screen.getByRole('textbox', {
      name: serviceAbstractLabel,
    });

    expect(serviceUrlField.getAttribute('value')).toEqual(mockServiceUrl);
    expect(serviceNameField.getAttribute('value')).toEqual('');
    expect(serviceAbstract.innerHTML).toEqual('');

    await waitFor(() => {
      expect(serviceNameField.getAttribute('value')).toEqual(
        mockServiceFromStore.title,
      );
    });
    expect(serviceAbstract.innerHTML).toEqual(mockServiceFromStore.abstract);
  });

  it('should fire serviceSetLayers action with correct layer information when adding a service', async () => {
    const mockUrl = 'https://mockUrlWithChildren.nl';
    const props: ServicePopupProps = {
      servicePopupVariant: 'add',
      closePopup: jest.fn(),
      isOpen: true,
      serviceSetLayers: jest.fn(),
      services: mockServices,
      showSnackbar: jest.fn(),
    };
    render(
      <DemoWrapper>
        <ServicePopup {...props} />
      </DemoWrapper>,
    );
    expect(screen.getByRole('dialog').textContent).toBeTruthy();
    const urlInput = screen.getByRole('textbox', { name: serviceUrlLabel });
    fireEvent.input(urlInput, { target: { value: mockUrl } });
    fireEvent.blur(urlInput);

    await waitFor(() => {
      expect(
        screen
          .getByRole('textbox', { name: serviceNameLabel })
          .getAttribute('value'),
      ).toEqual(mockServiceFromStore.title);
    });

    fireEvent.click(screen.getByRole('button', { name: /save/i }));
    await waitFor(() =>
      expect(props.serviceSetLayers).toHaveBeenCalledWith({
        abstract: 'Service Abstract',
        id: 'serviceid_1',
        layers: [
          {
            abstract: '',
            dimensions: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: true,
            name: 'RAD_NL25_PCP_CM',
            path: [],
            styles: [],
            title: 'RAD_NL25_PCP_CM',
          },
          {
            abstract: '',
            dimensions: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: true,
            name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
            path: [],
            styles: [],
            title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
          },
        ],
        name: 'Service test title',
        scope: 'user',
        serviceUrl: 'https://mockUrlWithChildren.nl',
      }),
    );
    await waitFor(() => expect(props.showSnackbar).toHaveBeenCalledTimes(1));
    await waitFor(() => expect(props.closePopup).toHaveBeenCalledTimes(1));
  });

  it('should show snackbar after copy a url', async () => {
    Object.defineProperty(navigator, 'clipboard', {
      value: {
        writeText: () => {},
      },
      writable: true,
      configurable: true,
    });
    jest.spyOn(navigator.clipboard, 'writeText');

    const props: ServicePopupProps = {
      servicePopupVariant: 'show',
      isOpen: true,
      serviceSetLayers: jest.fn(),
      services: mockServices,
      serviceId: 'serviceid_1',
      serviceUrl: 'test.url',
      showSnackbar: jest.fn(),
    };

    render(
      <DemoWrapper>
        <ServicePopup {...props} />
      </DemoWrapper>,
    );

    const copyButton = screen.getByRole('button', { name: /copy url/i });
    await user.click(copyButton);
    expect(navigator.clipboard.writeText).toHaveBeenCalledWith(
      props.serviceUrl,
    );

    expect(props.showSnackbar).toHaveBeenCalledWith(
      translateKeyOutsideComponents('copy-url-message'),
    );
  });

  it('should show snackbar after saving a service', async () => {
    const i18n = initLayerSelectI18n();
    const props: ServicePopupProps = {
      servicePopupVariant: 'save',
      isOpen: true,
      serviceSetLayers: jest.fn(),
      services: {},
      serviceName: 'Service test title',
      serviceUrl: 'https://mockUrlWithChildren.nl',
      showSnackbar: jest.fn(),
      closePopup: jest.fn(),
    };
    render(
      <DemoWrapper>
        <ServicePopup {...props} />
      </DemoWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: serviceNameLabel,
    });

    await waitFor(() => {
      expect(serviceNameField.getAttribute('value')).toEqual(
        mockServiceFromStore.title,
      );
    });

    const saveButton = screen.getByTestId('saveServiceButton');
    fireEvent.click(saveButton);

    await waitFor(() =>
      expect(props.serviceSetLayers).toHaveBeenCalledTimes(1),
    );

    await waitFor(() =>
      expect(props.showSnackbar).toHaveBeenCalledWith(
        getSuccesMessage(mockServiceFromStore.title, i18n.t),
      ),
    );
    await waitFor(() => expect(props.closePopup).toHaveBeenCalledTimes(1));
  });

  it('should autoFocus on serviceUrl for new service', () => {
    render(
      <DemoWrapper>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          services={mockServices}
        />
      </DemoWrapper>,
    );
    const input = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    expect(input.matches(':focus')).toBeTruthy();
  });

  it('should display loading indicator when adding new service is in process', async () => {
    jest.useFakeTimers();
    const url = 'https://geoservices.knmi.nl/wms?dataset=RADAR&';
    jest
      .spyOn(utils, 'loadWMSService')
      .mockImplementationOnce(
        () =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve({
                id: url,
                name: 'Precipitation Radar NL',
                serviceUrl: url,
                abstract: '',
                layers: [],
                scope: 'user',
              });
            }, 1000);
          }),
      )
      .mockImplementationOnce(async () => {
        return {
          id: url,
          name: 'Precipitation Radar NL',
          serviceUrl: url,
          abstract: '',
          layers: [],
          scope: 'user',
        };
      });
    const props: ServicePopupProps = {
      servicePopupVariant: 'save',
      closePopup: jest.fn(),
      isOpen: true,
      serviceSetLayers: jest.fn(),
      services: mockServices,
      showSnackbar: jest.fn(),
    };
    render(
      <DemoWrapper>
        <ServicePopup {...props} />
      </DemoWrapper>,
    );

    const serviceNameTextField = screen.getByRole('textbox', {
      name: /service name/i,
    });
    fireEvent.change(serviceNameTextField, {
      target: { value: 'Precipitation Radar NL' },
    });

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    fireEvent.change(serviceUrlField, {
      target: { value: url },
    });

    fireEvent.blur(serviceUrlField, {
      target: { value: url },
    });

    await waitFor(() => {
      expect(screen.getByTestId('loadingIndicator')).toBeTruthy();
    });

    await act(async () => {
      jest.advanceTimersByTime(1000);
    });

    await waitFor(() => {
      expect(screen.queryByTestId('loadingIndicator')).toBeFalsy();
    });

    fireEvent.click(screen.getByRole('button', { name: 'save' }));

    await waitFor(() => {
      expect(props.serviceSetLayers).toHaveBeenCalled();
    });

    jest.clearAllTimers();
    jest.clearAllMocks();
    jest.useRealTimers();
  });
});
