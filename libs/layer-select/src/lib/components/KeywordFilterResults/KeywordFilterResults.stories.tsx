/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import type { Meta, StoryObj } from '@storybook/react';

import { Box } from '@mui/material';
import {
  KeywordFilterResults,
  KeywordFilterResultsProps,
} from './KeywordFilterResults';
import { LayerSelectStoreProvider } from '../Providers';
import {
  layerSelectTypes,
  layerSelectInitialState,
  createMockStore,
} from '../../store';

const filterId1 = 'keyword-filter-1';
const filter1: layerSelectTypes.Filter = {
  id: filterId1,
  name: 'filter-1',
  amountVisible: 1,
  checked: false,
  type: 'keywords',
};
const filterId2 = 'keyword-filter-2';
const filter2: layerSelectTypes.Filter = {
  id: filterId2,
  name: 'filter-2',
  amountVisible: 1,
  checked: true,
  type: 'keywords',
};
const filterId3 = 'group-filter-3';
const filter3: layerSelectTypes.Filter = {
  id: filterId3,
  name: 'filter-3',
  amountVisible: 1,
  checked: true,
  type: 'groups',
};

const meta: Meta<typeof KeywordFilterResults> = {
  title: 'components/KeywordFilterResults',
  component: KeywordFilterResults,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user KeywordFilterResults',
      },
    },
  },
  argTypes: {},
  render: (props) => (
    <LayerSelectStoreProvider store={store}>
      <Box sx={{ minHeight: 300 }}>
        <KeywordFilterResults {...props} />
      </Box>
    </LayerSelectStoreProvider>
  ),
};
export default meta;

type Story = StoryObj<typeof KeywordFilterResults>;

export const Component: Story = {
  args: {
    isOpen: true,
    filters: { keywords: [filter1, filter2], groups: [filter3] },
  },
};

const props: KeywordFilterResultsProps = {
  isOpen: true,
  filters: { keywords: [filter1, filter2], groups: [filter3] },
};
const mockState: layerSelectTypes.LayerSelectModuleState = {
  layerSelect: {
    ...layerSelectInitialState,
    filters: {
      activeServices: {
        entities: {},
        ids: [],
      },
      searchFilter: '',
      filters: {
        entities: {
          [filterId1]: filter1,
          [filterId2]: filter2,
          [filterId3]: filter3,
        },
        ids: [filterId1, filterId2, filterId3],
      },
    },
    allServicesEnabled: true,
  },
};
const store = createMockStore(mockState);

export const KeywordFilterResultsLight: Story = {
  render: () => (
    <LayerSelectStoreProvider store={store}>
      <Box sx={{ minHeight: 300 }}>
        <KeywordFilterResults {...props} />
      </Box>
    </LayerSelectStoreProvider>
  ),
  tags: ['snapshot'],
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62c695a1ed42f01adee34e63/version/632b1738c788c83cc9d5da8d',
      },
    ],
    layout: 'fullscreen',
  },
};

export const KeywordFilterResultsDark: Story = {
  render: () => (
    <LayerSelectStoreProvider store={store}>
      <Box sx={{ minHeight: 300 }}>
        <KeywordFilterResults {...props} />
      </Box>
    </LayerSelectStoreProvider>
  ),
  tags: ['snapshot', 'dark'],
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d7b06898ff1c11dbcc0d0a/version/632b1ae593c99c3b8f379504',
      },
    ],
    layout: 'fullscreen',
  },
};
