/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { createTheme } from './createTheme';
import { colorsLight, elevationsLight } from './themes';
import { createShadows, parseColors } from './utils';

describe('utils/createTheme', () => {
  it('should create a light theme with geowebColors if empty object is passed', () => {
    const defaultResult = createTheme({});

    expect(defaultResult.palette.mode).toEqual('light');
    expect(defaultResult.palette.geowebColors).toEqual(
      parseColors(colorsLight),
    );
    expect(defaultResult.shadows).toEqual(createShadows(elevationsLight));
  });

  it('should create a custom theme with geowebColors', () => {
    const customGeowebColors = {
      ...colorsLight,
      background: {
        surface: '#FF00FF',
        surfaceApp: '#FF5533',
        surfaceBrowser: '#CCFFEE',
      },
    };

    const defaultResult = createTheme({
      paletteType: 'dark',
      geowebColors: customGeowebColors,
    });

    expect(defaultResult.palette.mode).toEqual('dark');
    expect(defaultResult.palette.geowebColors).toEqual(
      parseColors(customGeowebColors),
    );
    expect(defaultResult.shadows).toEqual(createShadows(elevationsLight));
  });
});
