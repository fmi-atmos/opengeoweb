/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Button, IconButton, Snackbar as MuiSnackbar } from '@mui/material';
import type { StoryObj } from '@storybook/react';
import { Close } from '../components/Icons';

type Story = StoryObj<typeof MuiSnackbar>;

export default {
  title: 'demo/Snackbar',
};

interface SnackbarDemoProps {
  open?: boolean;
  handleClose?: () => void;
}

const SnackbarDemo: React.FC<SnackbarDemoProps> = ({
  open = true,
  handleClose = (): void => {},
}: SnackbarDemoProps) => {
  return (
    <MuiSnackbar
      open={open}
      onClose={handleClose}
      message="This is a snackbar notification"
      sx={{ padding: 3 }}
      action={
        <IconButton
          aria-label="close"
          onClick={handleClose}
          sx={{
            color: 'geowebColors.snackbar.action',
            '&:hover': {
              backgroundColor: 'geowebColors.snackbar.actionHover.rgba',
            },
          }}
        >
          <Close />
        </IconButton>
      }
    />
  );
};

const SnackbarWrapper: React.FC = () => {
  // open the snackbar by default during snapshot tests
  const [open, setOpen] = React.useState(window.location.hash === '#snapshot');

  const handleToggle = (): void => {
    setOpen(!open);
  };

  const handleClose = (): void => {
    setOpen(false);
  };

  return (
    <>
      <Button variant="outlined" onClick={handleToggle}>
        Toggle snackbar
      </Button>
      <SnackbarDemo open={open} handleClose={handleClose} />
    </>
  );
};

export const SnackbarLight: Story = {
  render: () => <SnackbarWrapper />,
  tags: ['snapshot', 'fullPageSnapshot'],
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf926c9c931a2a11621c31',
      },
    ],
  },
};
SnackbarLight.storyName = 'Snackbar light theme';

export const SnackbarDark: Story = {
  render: () => <SnackbarWrapper />,
  tags: ['snapshot', 'dark', 'fullPageSnapshot'],
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e6973d055b0846449d58',
      },
    ],
  },
};
SnackbarDark.storyName = 'Snackbar dark theme';
