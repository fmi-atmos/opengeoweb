/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Box, Card, Tab, Tabs as MuiTabs, Typography } from '@mui/material';
import type { StoryObj } from '@storybook/react';

type Story = StoryObj<typeof MuiTabs>;

export default {
  title: 'demo/Tabs',
};

interface TabsDemoProps {
  value?: number;
  onChange?: (event: React.SyntheticEvent, index: number) => void;
}

const TabsDemo: React.FC<TabsDemoProps> = ({
  value = 0,
  onChange = (): void => {},
}: TabsDemoProps) => {
  return (
    <Box
      sx={{
        width: 600,
        padding: 2,
      }}
    >
      <MuiTabs
        value={value}
        onChange={onChange}
        aria-label="simple tabs example"
      >
        <Tab label="Tab 1" />
        <Tab label="Tab 2" />
      </MuiTabs>
    </Box>
  );
};

const TabsWrapper: React.FC = () => {
  const [value, setValue] = React.useState(0);
  const handleChange = (
    _event: React.SyntheticEvent<Element>,
    newValue: number,
  ): void => {
    setValue(newValue);
  };
  return (
    <Card elevation={0}>
      <TabsDemo value={value} onChange={handleChange} />
      {value === 0 && (
        <Box>
          <Typography>Tab 1</Typography>
        </Box>
      )}
      {value === 1 && (
        <Box>
          <Typography>Tab 2</Typography>
        </Box>
      )}
    </Card>
  );
};

export const TabsLight: Story = {
  render: () => <TabsWrapper />,
  tags: ['snapshot'],
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf926c66b920283321be0e',
      },
    ],
  },
};
TabsLight.storyName = 'Tabs light theme';

export const TabsDark: Story = {
  render: () => <TabsWrapper />,
  tags: ['snapshot', 'dark'],
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e92b2e9366351a1e8580',
      },
    ],
  },
};
TabsDark.storyName = 'Tabs dark theme';
