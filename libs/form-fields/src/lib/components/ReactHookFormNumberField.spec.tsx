/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  createEvent,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { useFormContext } from 'react-hook-form';
import { Button } from '@mui/material';
import ReactHookFormNumberField, {
  convertNumericInputValue,
  getAllowedKeys,
} from './ReactHookFormNumberField';
import ReactHookFormProvider from './ReactHookFormProvider';
import { FormFieldsI18nProvider } from './Providers';
import { initFormFieldsI18n } from '../utils/i18n';

beforeAll(() => {
  initFormFieldsI18n();
});

describe('ReactHookFormNumberField', () => {
  const user = userEvent.setup();
  it('should render successfully', () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>
      );
    };
    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();

    expect((screen.getByRole('textbox') as HTMLInputElement).type).toEqual(
      'text',
    );
  });
  it('should render successfully with given value and inputMode numeric', () => {
    const testValue = '123';
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider
          options={{
            defaultValues: {
              test: testValue,
            },
          }}
        >
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>
      );
    };
    render(<Wrapper />);
    const { type, value, inputMode } = screen.getByRole(
      'textbox',
    ) as HTMLInputElement;
    expect(type).toEqual('text');
    expect(inputMode).toEqual('numeric');
    expect(value).toEqual(testValue);
  });
  it('should convert value to integer for inputMode numeric and call onchange', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    render(<Wrapper />);

    const textField = screen.getByRole('textbox')!;
    await user.type(textField, '10.7');
    expect(textField.getAttribute('value')).toEqual('107');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });
  it('should ignore alphabetic letters', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    render(<Wrapper />);
    const textField = screen.getByRole('textbox')!;
    await user.type(textField, 'D10A.B7');
    expect(textField.getAttribute('value')).toEqual('107');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });
  it('should not handle more then one . seperator', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    render(<Wrapper />);
    const textField = screen.getByRole('textbox')!;
    await user.type(textField, '100.123.07');
    expect(textField.getAttribute('value')).toEqual('100.12307');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });
  it('should only allow - at start of inputfield', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    render(<Wrapper />);
    const textField = screen.getByRole('textbox')!;
    await user.type(textField, '-100.12307-34');
    await waitFor(() =>
      expect(textField.getAttribute('value')).toEqual('-100.1230734'),
    );
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });
  it('should allow negative decimal numbers', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    render(<Wrapper />);
    const textField = screen.getByRole('textbox')!;
    await user.type(textField, '-.123.45-67');
    await waitFor(() =>
      expect(textField.getAttribute('value')).toEqual('-.1234567'),
    );
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });
  it('should handle inputMode decimal and call onchange', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            inputMode="decimal"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };
    render(<Wrapper />);
    const textField = screen.getByRole('textbox')!;
    fireEvent.change(textField, { target: { value: '10.7' } });
    expect(textField.getAttribute('value')).toEqual('10.7');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalled());
  });
  it('should not have the styling if multiline property is false', async () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            multiline={false}
            rules={{ required: true }}
          />
        </ReactHookFormProvider>
      );
    };
    render(<Wrapper />);
    expect(screen.queryByRole('textbox')!.hasAttribute('style')).toBeFalsy();
  });
  it('should set focus when error on submit', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <form
          onSubmit={handleSubmit((formValues) => {
            testFn(formValues);
          })}
        >
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
          />
          <Button type="submit">Validate</Button>
        </form>
      );
    };
    render(
      <FormFieldsI18nProvider>
        <ReactHookFormProvider>
          <Wrapper />
        </ReactHookFormProvider>
      </FormFieldsI18nProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(screen.getByText('This field is required')).toBeTruthy();
    });
    expect(screen.getByText('Test')?.classList).toContain('Mui-error');
    expect(screen.getByText('Test')?.classList).toContain('Mui-focused');
    // fix error and test value
    fireEvent.change(screen.getByRole('textbox')!, {
      target: { value: '-10.23' },
    });

    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: -10.23 });
    });
    expect(screen.queryByText('This field is required')).toBeFalsy();
    expect(screen.getByText('Test').classList).not.toContain('Mui-error');
  });
  it('should return null when passing null as defaultValue of formprovider', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <form
          onSubmit={handleSubmit((formValues) => {
            testFn(formValues);
          })}
        >
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
          />
          <Button type="submit">Validate</Button>
        </form>
      );
    };
    render(
      <ReactHookFormProvider options={{ defaultValues: { test: null } }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: null });
    });
  });
  it('should return null when not passing any initial value', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <form
          onSubmit={handleSubmit((formValues) => {
            testFn(formValues);
          })}
        >
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
          />
          <Button type="submit">Validate</Button>
        </form>
      );
    };
    render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: null });
    });
  });
  it('should return null when passing initial value null as prop', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <form
          onSubmit={handleSubmit((formValues) => {
            testFn(formValues);
          })}
        >
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
            defaultValue={null}
          />
          <Button type="submit">Validate</Button>
        </form>
      );
    };
    render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: null });
    });
  });
  it('should return NaN when passing initial value NaN as prop', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <form
          onSubmit={handleSubmit((formValues) => {
            testFn(formValues);
          })}
        >
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
            defaultValue={NaN}
          />
          <Button type="submit">Validate</Button>
        </form>
      );
    };
    render(
      <ReactHookFormProvider options={{ defaultValues: { test: NaN } }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: NaN });
    });
  });
  it('should handle required when not passing any defaultValue', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <form
          onSubmit={handleSubmit((formValues) => {
            testFn(formValues);
          })}
        >
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{
              required: true,
            }}
            inputMode="decimal"
          />
          <Button type="submit">Validate</Button>
        </form>
      );
    };
    render(
      <FormFieldsI18nProvider>
        <ReactHookFormProvider>
          <Wrapper />
        </ReactHookFormProvider>
      </FormFieldsI18nProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() => {
      expect(screen.getByText('This field is required')).toBeTruthy();
    });
    expect(testFn).not.toHaveBeenCalled();

    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: -12 },
    });
    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: -12 });
    });
  });
  it('should handle required when passing defaultValue null in provider', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <form
          onSubmit={handleSubmit((formValues) => {
            testFn(formValues);
          })}
        >
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{
              required: true,
            }}
            inputMode="decimal"
          />
          <Button type="submit">Validate</Button>
        </form>
      );
    };
    render(
      <ReactHookFormProvider options={{ defaultValues: { test: null } }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() => {
      expect(testFn).not.toHaveBeenCalled();
    });

    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: 12 },
    });
    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: 12 });
    });
  });
  it('should handdle prop isReadOnly', () => {
    render(
      <ReactHookFormProvider>
        <ReactHookFormNumberField
          name="test"
          rules={{ required: true }}
          isReadOnly
        />
      </ReactHookFormProvider>,
    );
    expect(
      screen.getByRole('textbox').classList.contains('MuiInputBase-readOnly'),
    ).toBeTruthy();
  });

  it('should allow using pasting / cutting or copying keyboard actions', async () => {
    const userAgent = jest.spyOn(navigator, 'userAgent', 'get');

    render(
      <ReactHookFormProvider>
        <ReactHookFormNumberField
          name="test"
          rules={{ required: true }}
          inputMode="numeric"
        />
      </ReactHookFormProvider>,
    );

    const textField: HTMLInputElement = screen.getByRole('textbox');

    userAgent.mockReturnValue(
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36',
    );

    for (const key of ['x', 'c', 'v']) {
      const pasteEventMac = createEvent.keyDown(textField, {
        key,
        metaKey: true,
      });

      fireEvent(textField, pasteEventMac);

      expect(pasteEventMac.defaultPrevented).toBe(false);
    }

    userAgent.mockReturnValue(
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.2365',
    );

    for (const key of ['x', 'c', 'v']) {
      const pasteEventWindows = createEvent.keyDown(textField, {
        key,
        ctrlKey: true,
      });

      fireEvent(textField, pasteEventWindows);

      expect(pasteEventWindows.defaultPrevented).toBe(false);
    }

    userAgent.mockClear();
  });

  it('should be possible to paste integers', async () => {
    render(
      <ReactHookFormProvider>
        <ReactHookFormNumberField
          name="test"
          rules={{ required: true }}
          inputMode="numeric"
        />
      </ReactHookFormProvider>,
    );

    const textField: HTMLInputElement = screen.getByRole('textbox');

    const user = await userEvent.setup();

    await user.click(textField);
    await user.paste('20');
    expect(textField.value).toEqual('20');

    // paste another number after the earlier number
    textField.setSelectionRange(2, 2);
    await user.paste('24');
    expect(textField.value).toEqual('2024');

    // paste a negative number
    textField.setSelectionRange(0, textField.value.length);
    await user.paste('-100');
    expect(textField.value).toEqual('-100');
  });

  it('should be possible to paste decimal numbers', async () => {
    render(
      <ReactHookFormProvider>
        <ReactHookFormNumberField
          name="test"
          rules={{ required: true }}
          inputMode="decimal"
        />
      </ReactHookFormProvider>,
    );

    const textField: HTMLInputElement = screen.getByRole('textbox');

    const user = await userEvent.setup();

    await user.click(textField);

    await user.paste('20.32');
    expect(textField.value).toEqual('20.32');

    // paste a negative number
    textField.setSelectionRange(0, textField.value.length);
    await user.paste('-20.320');
    expect(textField.value).toEqual('-20.320');

    // try to paste another negative number before the earlier pasted number
    // this should fail
    textField.setSelectionRange(0, 0);
    await user.paste('-100');
    expect(textField.value).toEqual('-20.320');
  });

  it('should not be possible to paste text', async () => {
    render(
      <ReactHookFormProvider>
        <ReactHookFormNumberField
          name="test"
          rules={{ required: true }}
          value={10}
          inputMode="numeric"
        />
      </ReactHookFormProvider>,
    );

    const textField: HTMLInputElement = screen.getByRole('textbox');

    const user = await userEvent.setup();

    await user.click(textField);
    await user.paste('this is not a number');
    expect(textField.value).toEqual('10');
  });

  describe('convertNumericInputValue', () => {
    it('should return null if "" or NaN', () => {
      expect(convertNumericInputValue('', 'decimal')).toEqual(null);
      expect(convertNumericInputValue('', 'numeric')).toEqual(null);
      expect(convertNumericInputValue('ff', 'decimal')).toEqual(null);
      expect(convertNumericInputValue('ff', 'numeric')).toEqual(null);
      expect(
        convertNumericInputValue(NaN as unknown as string, 'numeric'),
      ).toEqual(null);
    });
    it('should return number if integer passed and non-empty, non-end on dot or non-NaN string is passed', () => {
      expect(typeof convertNumericInputValue('5', 'numeric')).toEqual('number');
      expect(typeof convertNumericInputValue('500.0', 'numeric')).toEqual(
        'number',
      );
    });
    it('should return float if float is passed in', () => {
      expect(typeof convertNumericInputValue('5', 'decimal')).toEqual('number');
      expect(typeof convertNumericInputValue('500.0', 'decimal')).toEqual(
        'number',
      );
    });
  });

  describe('getAllowedKeys', () => {
    it('should return correct allowed keys', () => {
      expect(getAllowedKeys('decimal').includes('.')).toBeTruthy();
      expect(getAllowedKeys('numeric').includes('.')).toBeFalsy();
      expect(getAllowedKeys().includes('.')).toBeFalsy();
      expect(getAllowedKeys('numeric').includes('A')).toBeFalsy();
      expect(getAllowedKeys('numeric').includes('X')).toBeFalsy();
    });
  });
});
