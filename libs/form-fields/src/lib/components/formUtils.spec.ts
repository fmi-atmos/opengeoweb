/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { getDeepProperty, getErrors } from './formUtils';

describe('components/formUtils', () => {
  describe('getDeepProperty', () => {
    it('should return deep values if present', () => {
      const testObject = {
        level1: {
          value: 'level-1',
          level2: {
            value: 'level-2',
            level3: {
              value: 'level-3',
            },
          },
        },
      };

      expect(getDeepProperty('level1.value'.split('.'), testObject)).toEqual(
        'level-1',
      );
      expect(
        getDeepProperty('level1.level2.value'.split('.'), testObject),
      ).toEqual('level-2');
      expect(
        getDeepProperty('level1.level2.level3.value'.split('.'), testObject),
      ).toEqual('level-3');
    });

    it('should return null if value does not exist', () => {
      const testObject = {
        level1: {
          value: 'level-1',
          level2: {
            value: 'level-2',
            level3: {
              value: 'level-3',
            },
          },
        },
      };

      expect(getDeepProperty('not.existing'.split('.'), testObject)).toBeNull();
      expect(getDeepProperty('not'.split('.'), testObject)).toBeNull();
    });
  });

  describe('getErrors', () => {
    it('should return error present', () => {
      const errors = {
        level: {
          type: 'required',
          message: '',
        },
        level2: {
          type: 'minLength',
          message: '',
        },
        deep: {
          level: {
            type: 'required',
            message: '',
          },
          level2: {
            type: 'minLength',
            message: '',
          },
        },
      };
      expect(getErrors('level', errors)).toEqual(errors.level);
      expect(getErrors('level2', errors)).toEqual(errors.level2);
      expect(getErrors('deep.level', errors)).toEqual(errors.deep.level);
      expect(getErrors('deep.level2', errors)).toEqual(errors.deep.level2);
    });

    it('should return undefined when no errors', () => {
      const errors = {
        level: {
          type: 'required',
          message: '',
        },
        level2: {
          type: 'minLength',
          message: '',
        },
      };
      expect(getErrors('non-exist', errors)).toEqual(undefined);
      expect(getErrors('non-exist.2', errors)).toEqual(undefined);
    });
  });
});
