/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Typography, Button } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import type { Meta, StoryObj } from '@storybook/react';
import { ReactHookFormTextField } from '.';
import { FormFieldsWrapper, zeplinLinks } from './Providers';

type Story = StoryObj<typeof ReactHookFormTextField>;

const meta: Meta<typeof ReactHookFormTextField> = {
  title: 'Components/Text Field',
  component: ReactHookFormTextField,
  parameters: {
    docs: {
      description: {
        component: 'ReactHookFormTextField component',
      },
    },
  },
};
export default meta;

export const Component: Story = {
  args: {
    name: 'date-time-example',
    label: 'Textfield label',
    rules: {
      required: false,
    },
    isReadOnly: false,
    disabled: false,
    helperText: '',
    defaultValue: '',
    upperCase: false,
  },
  render: (props) => (
    <FormFieldsWrapper>
      <ReactHookFormTextField {...props} />
    </FormFieldsWrapper>
  ),
};

const ReactHookFormTextFieldDemo = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <form onSubmit={handleSubmit(() => {})}>
      <Typography>Inputmode Text (default)</Typography>
      <ReactHookFormTextField
        name="textdemo"
        label="Text"
        rules={{ required: true }}
      />

      <Typography>Disabled</Typography>
      <ReactHookFormTextField
        name="disableddemo"
        label="Dummy label"
        rules={{
          required: true,
        }}
        disabled
        defaultValue="Dummy value"
      />

      <Typography>With helpertext and default value</Typography>
      <ReactHookFormTextField
        name="helpertextdemo"
        label="Dummy label"
        helperText="Here to help"
        rules={{
          required: true,
        }}
        defaultValue="Default value"
      />

      <Typography>Multiline</Typography>
      <ReactHookFormTextField
        name="helpertextdemo"
        label="Multiline textfield"
        rules={{
          required: true,
        }}
        defaultValue="Default value"
        multiline
        rows={4}
      />

      <Typography>Error</Typography>
      <ReactHookFormTextField
        name="errordemo"
        label="Dummy label"
        rules={{
          required: true,
        }}
        error
        defaultValue="Dummy value"
      />

      <Button variant="contained" color="secondary" type="submit">
        Validate
      </Button>
    </form>
  );
};

export const TextField: Story = {
  render: () => (
    <FormFieldsWrapper>
      <ReactHookFormTextFieldDemo />
    </FormFieldsWrapper>
  ),

  parameters: {
    zeplinLink: zeplinLinks,
    docs: {
      description: {
        story: 'Press the submit button to test out validation',
      },
    },
  },
};

const SnapShotLayout = (): React.ReactElement => {
  return (
    <>
      <ReactHookFormTextField
        name="textdemo"
        label="Text"
        rules={{ required: true }}
      />

      <ReactHookFormTextField
        name="disableddemo"
        label="Dummy label"
        rules={{
          required: true,
        }}
        disabled
        defaultValue="Dummy value"
      />

      <ReactHookFormTextField
        name="readonlydemo"
        label="readonly label"
        rules={{
          required: true,
        }}
        disabled
        isReadOnly
        defaultValue="Dummy value"
      />

      <ReactHookFormTextField
        name="helpertextdemo"
        label="Dummy label"
        helperText="Here to help"
        rules={{
          required: true,
        }}
        defaultValue="Default value"
      />

      <ReactHookFormTextField
        name="helpertextdemo"
        label="Multiline"
        helperText="Here to help"
        rules={{
          required: true,
        }}
        multiline
        rows={4}
        defaultValue="Default value"
      />

      <ReactHookFormTextField
        name="errordemo"
        label="Dummy label"
        rules={{
          required: true,
        }}
        error
        defaultValue="Dummy value"
      />
    </>
  );
};

// light theme
export const TextFieldLightTheme: Story = {
  render: () => (
    <FormFieldsWrapper>
      <SnapShotLayout />
    </FormFieldsWrapper>
  ),
  tags: ['snapshot'],
  parameters: {
    zeplinLink: [zeplinLinks[0]],
  },
};

// dark theme
export const TextFieldDarkTheme: Story = {
  render: () => (
    <FormFieldsWrapper>
      <SnapShotLayout />
    </FormFieldsWrapper>
  ),
  tags: ['snapshot', 'dark'],
  parameters: {
    zeplinLink: [zeplinLinks[1]],
  },
};
