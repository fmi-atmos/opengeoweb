/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n from 'i18next';
import {
  initReactI18next,
  useTranslation,
  UseTranslationResponse,
} from 'react-i18next';

import formFieldsTranslations from '../../../locales/formFields.json';

export const FORM_FIELDS_NAMESPACE = 'form-fields';

export const initFormFieldsI18n = (): void => {
  void i18n.use(initReactI18next).init({
    lng: 'en',
    ns: FORM_FIELDS_NAMESPACE,
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.en,
      },
      fi: {
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.fi,
      },
      no: {
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.no,
      },
      nl: {
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.nl,
      },
    },
  });
};

const ns = [FORM_FIELDS_NAMESPACE];

export const translateKeyOutsideComponents = (
  key: string,
  params: Record<string, string | number> | undefined = undefined,
): string => i18n.t(key, { ns, ...params });

export const useFormFieldsTranslation = (): UseTranslationResponse<
  typeof FORM_FIELDS_NAMESPACE,
  typeof i18n
> => useTranslation(ns);

export { i18n };
