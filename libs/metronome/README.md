# metronome

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test metronome` to execute the unit tests via [Jest](https://jestjs.io).

## Running storybook

Run `nx storybook metronome` to build and view the storybook at http://localhost:5700/.

### TypeScript Documentation

- [TypeScript Docs](https://opengeoweb.gitlab.io/opengeoweb/typescript-docs/metronome/)
