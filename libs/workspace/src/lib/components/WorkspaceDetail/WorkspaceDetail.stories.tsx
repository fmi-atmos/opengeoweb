/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { StoryWrapper } from '../../storyUtils/StoryWrapper';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WorkspacePresetFromBE } from '../../store/workspace/types';
import { PresetsApi } from '../../utils/api';
import { ViewPreset } from '../../store/viewPresets/types';

export default {
  title: 'components/Workspace/WorkspaceDetail',
};

export const ScreenConfigRadarTemp = (): React.ReactElement => {
  return <StoryWrapper workspaceId="screenConfigRadarTemp" />;
};

export const ScreenConfigHarmoniePresetWithoutMultiMapLayerManager =
  (): React.ReactElement => {
    return (
      <StoryWrapper workspaceId="screenConfigHarmoniePresetWithoutMultiMapLayerManager" />
    );
  };

export const ScreenConfigHarmoniePresetWithMultiMapLayerManager =
  (): React.ReactElement => {
    return (
      <StoryWrapper workspaceId="screenConfigHarmoniePresetWithMultiMapLayerManager" />
    );
  };

export const UiDialogsDemo = (): React.ReactElement => {
  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();
    const uiViewPresetId = 'uiDialogExampleView';
    return {
      ...currentFakeApi,
      getWorkspacePreset: (): Promise<{ data: WorkspacePresetFromBE }> => {
        return new Promise((resolve) => {
          setTimeout(() => {
            resolve({
              data: {
                id: 'uidialogs',
                title: 'Ui dialogs',
                scope: 'system',
                abstract: '',
                viewType: 'multiWindow',
                views: [
                  { mosaicNodeId: 'screen_no_1', viewPresetId: 'temp' },
                  { mosaicNodeId: 'screen_no_2', viewPresetId: uiViewPresetId },
                  { mosaicNodeId: 'screen_no_3', viewPresetId: uiViewPresetId },
                ],
                syncGroups: [
                  { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' },
                  { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' },
                ],
                mosaicNode: {
                  direction: 'row',
                  first: 'screen_no_1',
                  second: {
                    direction: 'column',
                    first: 'screen_no_2',
                    second: 'screen_no_3',
                    splitPercentage: 51.75718849840255,
                  },
                  splitPercentage: 25,
                },
              },
            });
          }, 300);
        });
      },
      getViewPreset: (viewId: string): Promise<{ data: ViewPreset }> => {
        if (viewId === uiViewPresetId) {
          return new Promise((resolve) => {
            setTimeout(() => {
              resolve({
                data: {
                  id: uiViewPresetId,
                  title: 'Ui dialog example',
                  componentType: 'UiDialogExample',
                  initialProps: {},
                } as unknown as ViewPreset,
              });
            }, 300);
          });
        }
        return currentFakeApi.getViewPreset(viewId);
      },
    };
  };

  return (
    <StoryWrapper workspaceId="uidialogs" createApi={createFakeApiWithError} />
  );
};

export const ScreenConfigWithNonExistingLayer = (): React.ReactElement => {
  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();
    const uiViewPresetId = 'nonExistingLayer';
    return {
      ...currentFakeApi,
      getWorkspacePreset: (): Promise<{ data: WorkspacePresetFromBE }> => {
        return new Promise((resolve) => {
          setTimeout(() => {
            resolve({
              data: {
                id: 'screenConfigHarmoniePresetWithoutMultiMapLayerManager2',
                title: 'Temperature and precipitation (run compare) 1:1',
                scope: 'system',
                viewType: 'tiledWindow',
                views: [
                  {
                    mosaicNodeId: 'viewA',
                    viewPresetId: uiViewPresetId,
                  },
                ],
                syncGroups: [
                  {
                    id: 'areaLayerGroupA',
                    type: 'SYNCGROUPS_TYPE_SETBBOX',
                  },
                  {
                    id: 'timeLayerGroupA',
                    type: 'SYNCGROUPS_TYPE_SETTIME',
                  },
                  {
                    id: 'layerGroupA',
                    type: 'SYNCGROUPS_TYPE_SETLAYERACTIONS',
                  },
                  {
                    id: 'layerGroupB',
                    type: 'SYNCGROUPS_TYPE_SETLAYERACTIONS',
                  },
                ],
                mosaicNode: 'viewA',
              },
            });
          }, 300);
        });
      },
      getViewPreset: (viewId: string): Promise<{ data: ViewPreset }> => {
        if (viewId === uiViewPresetId) {
          return new Promise((resolve) => {
            setTimeout(() => {
              resolve({
                data: {
                  id: uiViewPresetId,
                  title: 'Temperature and precipitation',
                  scope: 'system',
                  keywords: 'Harmonie,Temperature,Precipitation',
                  initialProps: {
                    layers: {
                      topRow: {
                        name: 'air_temperature__at_2m',
                        enabled: true,
                        service:
                          'https://geoservices.knmi.nl/adaguc-server?DATASET=HARM_N25',
                        layerType: 'mapLayer',
                      },
                      bottomRow: {
                        name: 'precipitation_flux',
                        enabled: true,
                        service:
                          'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserver?dataset=HARM_N25',
                        layerType: 'mapLayer',
                      },
                      topRowSyncGroups: ['timeLayerGroupA', 'areaLayerGroupA'],
                      bottomRowSyncGroups: [
                        'timeLayerGroupA',
                        'areaLayerGroupA',
                      ],
                    },
                  },
                  componentType: 'HarmonieTempAndPrecipPreset',
                },
              });
            }, 300);
          });
        }
        return currentFakeApi.getViewPreset(viewId);
      },
    };
  };

  return (
    <StoryWrapper
      workspaceId="failingLayerTest"
      createApi={createFakeApiWithError}
    />
  );
};
