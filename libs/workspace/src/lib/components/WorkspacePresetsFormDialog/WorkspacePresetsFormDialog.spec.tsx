/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { GEOWEB_ROLE_PRESETS_ADMIN } from '@opengeoweb/authentication';
import ViewPresetsFormDialog, {
  WorkspacePresetsFormDialogProps,
} from './WorkspacePresetsFormDialog';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';
import { WorkspacePresetAction } from '../../store/workspace/types';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';
import {
  TranslatableWorkspaceListError,
  WorkspaceListErrorCategory,
  WorkspaceListErrorType,
} from '../../store/workspaceList/types';

describe('components/ViewPresetsDialog/ViewPresetsFormDialog', () => {
  it('should cancel save as', async () => {
    const props: WorkspacePresetsFormDialogProps = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'Map',
      formValues: {
        title: 'Map preset',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    // should cancel
    fireEvent.click(screen.getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onFormSubmit).not.toHaveBeenCalled();
  });

  it('should show spinner when loading', async () => {
    const props: WorkspacePresetsFormDialogProps = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'Map',
      formValues: {
        title: 'Map preset',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: true,
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('confirm-dialog-spinner')).toBeTruthy();
  });

  it('should close after duplicate', async () => {
    const props = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    // should close
    fireEvent.click(screen.getByTestId('customDialog-close'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onFormSubmit).not.toHaveBeenCalled();
  });

  it('should call onFormSubmit for duplicationg a preset', async () => {
    const testPresetFromBE = {
      title: 'Radar and Radar',
      scope: 'user' as const,
      abstract: 'This can contain an abstract',
      viewType: 'multiWindow' as const,
      views: [
        { mosaicNodeId: 'view-1', viewPresetId: 'radar' },
        { mosaicNodeId: 'view-2', viewPresetId: 'radar' },
      ],
      syncGroups: [
        { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' as const },
        { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' as const },
      ],
      mosaicNode: 'test-view',
    };

    const props = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'Map',
      formValues: testPresetFromBE,
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    } as WorkspacePresetsFormDialogProps;

    render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    // should close
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormSubmit).toHaveBeenCalledWith(
        WorkspacePresetAction.DUPLICATE,
        props.presetId,
        props.formValues,
      );
    });
  });

  it('should call onFormSubmit for saving preset as', async () => {
    const testPresetFromBE = {
      title: 'Radar and Radar',
      scope: 'user' as const,
      abstract: 'This can contain an abstract',
      viewType: 'multiWindow' as const,
      views: [
        {
          mosaicNodeId: 'view-1',
          viewPresetId: 'radar',
          title: 'Existing map preset',
        },
        {
          mosaicNodeId: 'view-2',
          viewPresetId: 'radar',
          title: 'Existing map preset',
        },
      ],
      syncGroups: [
        { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' as const },
        { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' as const },
      ],
      mosaicNode: 'test-view',
    };

    const props = {
      isOpen: true,
      action: WorkspacePresetAction.SAVE_AS,
      presetId: 'Map',
      formValues: testPresetFromBE,
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    expect(screen.getByTestId('customDialog-title')).toBeTruthy();
    expect(screen.getByText('Save as')).toBeTruthy();

    // should close
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormSubmit).toHaveBeenCalledWith(
        WorkspacePresetAction.SAVE_AS,
        props.presetId,
        props.formValues,
      );
    });
  });

  it('should call onFormSubmit for saving preset as for admin users', async () => {
    const testPresetFromBE = {
      title: 'Radar and Radar',
      scope: 'user' as const,
      abstract: 'This can contain an abstract',
      viewType: 'multiWindow' as const,
      views: [
        {
          mosaicNodeId: 'view-1',
          viewPresetId: 'radar',
          title: 'Existing map preset',
        },
        {
          mosaicNodeId: 'view-2',
          viewPresetId: 'radar',
          title: 'Existing map preset',
        },
      ],
      syncGroups: [
        { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' as const },
        { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' as const },
      ],
      mosaicNode: 'test-view',
    };

    const authAdminProps = {
      isLoggedIn: true,
      auth: {
        username: 'user.name',
        token: '1223344',
        refresh_token: '33455214',
      },
      currentRole: GEOWEB_ROLE_PRESETS_ADMIN,
      onLogin: (): void => null!,
      onSetAuth: (): void => null!,
      sessionStorageProvider: null!,
    };

    const props = {
      isOpen: true,
      action: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
      presetId: 'Map',
      formValues: testPresetFromBE,
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore auth={authAdminProps}>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    expect(screen.getByTestId('customDialog-title')).toBeTruthy();
    expect(screen.getByText('Save new system workspace')).toBeTruthy();

    // should close
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormSubmit).toHaveBeenCalledWith(
        WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
        props.presetId,
        props.formValues,
      );
    });
  });

  it('should call onFormSubmit for saving preset', async () => {
    const testPresetFromBE = {
      title: 'Radar and Radar',
      scope: 'user' as const,
      abstract: 'This can contain an abstract',
      viewType: 'multiWindow' as const,
      views: [
        {
          mosaicNodeId: 'view-1',
          viewPresetId: 'radar',
          title: 'Existing map preset',
          scope: 'user',
        },
        {
          mosaicNodeId: 'view-2',
          viewPresetId: 'radar',
          title: 'Existing map preset',
          scope: 'scope',
        },
      ],
      syncGroups: [
        { id: 'Area_RadarTemp', type: 'SYNCGROUPS_TYPE_SETBBOX' as const },
        { id: 'Time_RadarTemp', type: 'SYNCGROUPS_TYPE_SETTIME' as const },
      ],
      mosaicNode: 'test-view',
    };

    const props = {
      isOpen: true,
      action: WorkspacePresetAction.SAVE,
      presetId: 'Map',
      formValues: testPresetFromBE,
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    // should close
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(props.onFormSubmit).toHaveBeenCalledWith(
        WorkspacePresetAction.SAVE,
        props.presetId,
        props.formValues,
      );
    });
  });

  it('should not post empty title', async () => {
    const props = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'Map workspace',
      formValues: {
        title: '',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    const dialog = await screen.findByTestId('workspace-preset-dialog');
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();
    expect(dialog).toBeTruthy();

    // try to post
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });

    expect(props.onClose).toHaveBeenCalledTimes(0);
    expect(await screen.findByText('This field is required')).toBeTruthy();

    fireEvent.change(screen.getByRole('textbox', { name: /Name/i }), {
      target: {
        value: 'new dialog title',
      },
    });

    await waitFor(() => {
      expect(screen.queryByRole('alert')).toBeFalsy();
    });

    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(props.onFormSubmit).toHaveBeenCalledTimes(1);
    });
  });

  it('should show error', async () => {
    const testError: TranslatableWorkspaceListError = {
      type: WorkspaceListErrorType.TRANSLATABLE_ERROR,
      key: 'workspace-error-generic',
      category: WorkspaceListErrorCategory.GENERIC,
    };
    const props: WorkspacePresetsFormDialogProps = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'workspace1',
      formValues: {
        title: 'workspace preset',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
      error: testError,
    };

    const error = new Error('test error');
    const retrieveWorkspaceListPresetAsWatcher = jest.fn(
      () =>
        new Promise((resolve) => {
          resolve(props.presetId);
        }),
    );

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getWorkspacePreset:
          retrieveWorkspaceListPresetAsWatcher as unknown as PresetsApi['getWorkspacePreset'],
        saveWorkspacePresetAs: (): Promise<string> => {
          return new Promise((resolve, reject) => {
            reject(error);
          });
        },
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore createApi={createApi}>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();

    expect(screen.getByTestId('alert-banner')).toBeTruthy();
    expect(screen.getByText(i18n.t('workspace-error-generic'))).toBeTruthy();
  });

  it('should delete a preset', async () => {
    const props: WorkspacePresetsFormDialogProps = {
      isOpen: true,
      action: WorkspacePresetAction.DELETE,
      presetId: 'test123',
      formValues: {
        title: 'workspace preset 1',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace-preset-dialog')).toBeTruthy();

    // should delete
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(props.onFormSubmit).toHaveBeenCalledWith(
        props.action,
        props.presetId,
        props.formValues,
      );
    });
  });

  it('should show correct data when reopening', async () => {
    const props = {
      isOpen: true,
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'workspace123',
      formValues: {
        title: 'test title 1',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
    };

    const { rerender } = render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();
    expect(
      screen.getByRole('textbox', { name: 'Name' }).getAttribute('value'),
    ).toEqual(props.formValues.title);
    expect(
      screen.getByRole('textbox', { name: 'Abstract' }).textContent,
    ).toEqual(props.formValues.abstract);

    // should cancel
    fireEvent.click(screen.getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onFormSubmit).not.toHaveBeenCalled();

    const newProps = {
      ...props,
      formValues: { title: 'test title 2', abstract: 'new abstract' },
    };
    rerender(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetsFormDialog {...newProps} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();
    expect(
      screen.getByRole('textbox', { name: 'Name' }).getAttribute('value'),
    ).toEqual(newProps.formValues.title);
    expect(
      screen.getByRole('textbox', { name: 'Abstract' }).textContent,
    ).toEqual(newProps.formValues.abstract);
  });

  describe('title and confirm button titles', () => {
    const baseProps: WorkspacePresetsFormDialogProps = {
      isOpen: true,
      presetId: 'test123',
      formValues: {
        title: 'workspace preset 1',
        abstract: 'some abstract',
      },
      onClose: jest.fn(),
      onFormSubmit: jest.fn(),
      isLoading: false,
      action: WorkspacePresetAction.DELETE,
    };
    it('should get correct title for delete action', () => {
      const props: WorkspacePresetsFormDialogProps = {
        ...baseProps,
        action: WorkspacePresetAction.DELETE,
      };

      render(
        <WorkspaceWrapperProviderWithStore>
          <ViewPresetsFormDialog {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );

      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-dialog-delete'),
        ),
      ).toBeTruthy();
      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-delete'),
        ),
      ).toBeTruthy();
    });
    it('should get correct title for duplicate action', () => {
      const props: WorkspacePresetsFormDialogProps = {
        ...baseProps,
        action: WorkspacePresetAction.DUPLICATE,
      };

      render(
        <WorkspaceWrapperProviderWithStore>
          <ViewPresetsFormDialog {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );

      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-duplicate'),
        ),
      ).toBeTruthy();
      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-save'),
        ),
      ).toBeTruthy();
    });

    it('should get correct title for save as action', () => {
      const props: WorkspacePresetsFormDialogProps = {
        ...baseProps,
        action: WorkspacePresetAction.SAVE_AS,
      };

      render(
        <WorkspaceWrapperProviderWithStore>
          <ViewPresetsFormDialog {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );

      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-save-as'),
        ),
      ).toBeTruthy();
      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-save'),
        ),
      ).toBeTruthy();
    });

    it('should get correct title for edit action', () => {
      const props: WorkspacePresetsFormDialogProps = {
        ...baseProps,
        action: WorkspacePresetAction.EDIT,
      };

      render(
        <WorkspaceWrapperProviderWithStore>
          <ViewPresetsFormDialog {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );

      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-edit'),
        ),
      ).toBeTruthy();
      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-save'),
        ),
      ).toBeTruthy();
    });

    it('should get correct title for admin save action', () => {
      const props: WorkspacePresetsFormDialogProps = {
        ...baseProps,
        action: WorkspacePresetAction.SAVE_SYSTEM_PRESET,
      };

      render(
        <WorkspaceWrapperProviderWithStore>
          <ViewPresetsFormDialog {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );

      expect(
        screen.getByText(
          translateKeyOutsideComponents(
            i18n.t,
            'workspace-admin-save-workspace',
          ),
        ),
      ).toBeTruthy();
      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-save'),
        ),
      ).toBeTruthy();
    });

    it('should get correct title for admin save as action', () => {
      const props: WorkspacePresetsFormDialogProps = {
        ...baseProps,
        action: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
      };

      render(
        <WorkspaceWrapperProviderWithStore>
          <ViewPresetsFormDialog {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );

      expect(
        screen.getByText(
          translateKeyOutsideComponents(
            i18n.t,
            'workspace-admin-save-new-workspace',
          ),
        ),
      ).toBeTruthy();
      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-save'),
        ),
      ).toBeTruthy();
    });

    it('should get correct title for save action', () => {
      const props: WorkspacePresetsFormDialogProps = {
        ...baseProps,
        action: WorkspacePresetAction.SAVE,
      };

      render(
        <WorkspaceWrapperProviderWithStore>
          <ViewPresetsFormDialog {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );

      expect(
        screen.getByText(
          translateKeyOutsideComponents(
            i18n.t,
            'workspace-admin-save-user-workspace',
          ),
        ),
      ).toBeTruthy();
      expect(
        screen.getByText(
          translateKeyOutsideComponents(i18n.t, 'workspace-save'),
        ),
      ).toBeTruthy();
    });
  });
});
