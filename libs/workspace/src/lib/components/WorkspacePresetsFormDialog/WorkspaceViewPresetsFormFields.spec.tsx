/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';

import { fireEvent, render, screen, within } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { FieldValues, useFormContext } from 'react-hook-form';
import WorkspaceViewPresetsFormFields from './WorkspaceViewPresetsFormFields';
import {
  i18n,
  initWorkspaceTestI18n,
  translateKeyOutsideComponents,
} from '../../utils/i18n';
import { getPresetTitle } from '../../utils/utils';
import {
  ViewPresetErrorComponent,
  ViewPresetErrorType,
} from '../../store/viewPresets/types';
import { WorkspaceDialogView } from '../../store/workspaceList/types';

beforeAll(() => {
  initWorkspaceTestI18n();
});

describe('components/WorkspacePresetsFormDialog/WorkspaceViewPresetsFormFields', () => {
  it('should not show anything with empty viewpreset list', () => {
    render(<WorkspaceViewPresetsFormFields viewPresets={[]} />);
    expect(
      screen.queryByText(
        translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
      ),
    ).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
      ),
    ).toBeFalsy();
  });

  it('should not show anything when there are no viewpresets with changes', () => {
    const viewPresets = [
      {
        mosaicNodeId: 'screen1',
        viewPresetId: 'already-existing',
        hasChanges: false,
        title: 'my existing system preset 1',
        isSelected: false,
        scope: 'system' as const,
      },
      {
        mosaicNodeId: 'screen2',
        viewPresetId: 'already-existing',
        hasChanges: false,
        title: 'my existing system preset 1',
        isSelected: false,
        scope: 'system' as const,
      },
    ];
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <WorkspaceViewPresetsFormFields viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );
    expect(
      screen.queryByText(
        translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
      ),
    ).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
      ),
    ).toBeFalsy();
  });

  it('should show viewpreset when they have changes', async () => {
    const viewPresets = [
      {
        mosaicNodeId: 'screen1',
        viewPresetId: 'already-existing',
        hasChanges: true,
        title: 'my existing system preset 1',
        isSelected: true,
        scope: 'system' as const,
      },
      {
        mosaicNodeId: 'screen2',
        viewPresetId: 'already-existing',
        hasChanges: false,
        title: 'my existing system preset 2',
        isSelected: true,
        scope: 'system' as const,
      },
      {
        mosaicNodeId: 'screen3',
        viewPresetId: '',
        hasChanges: true,
        title: 'new one',
        isSelected: true,
        scope: 'system' as const,
      },
      {
        mosaicNodeId: 'screen4',
        viewPresetId: '',
        hasChanges: true,
        title: '',
        isSelected: false,
        scope: 'system' as const,
      },
    ];

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <WorkspaceViewPresetsFormFields viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );

    // show existing presets with changes
    expect(
      screen.getByText(
        getPresetTitle(viewPresets[0].title, true, false, i18n.t),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-admin-save-new-viewpreset-warning',
        ),
      ),
    ).toBeTruthy();
    // show existing presets without changes
    expect(
      screen.getByText(
        getPresetTitle(viewPresets[1].title, false, false, i18n.t),
      ),
    ).toBeTruthy();
    // show new presets
    expect(
      screen.getByText(
        getPresetTitle(viewPresets[2].title, true, true, i18n.t),
      ),
    ).toBeTruthy();

    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-admin-save-new-viewpreset-warning',
        ),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        getPresetTitle(
          translateKeyOutsideComponents(i18n.t, 'workspace-mappreset-new'),
          true,
          true,
          i18n.t,
        ),
      ),
    ).toBeTruthy();
  });

  it('be able to click a checkbox to show input fields', async () => {
    const viewPresets = [
      {
        mosaicNodeId: 'screen1',
        viewPresetId: '',
        hasChanges: true,
        title: 'my existing system preset 1',
        isSelected: false,
        scope: 'system' as const,
      },
    ];

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <WorkspaceViewPresetsFormFields viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );

    expect(
      screen.queryByRole('textbox', {
        name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
      }),
    ).toBeFalsy();

    expect(
      screen.queryByRole('textbox', {
        name: translateKeyOutsideComponents(
          i18n.t,
          'workspace-dialog-abstract',
        ),
      }),
    ).toBeFalsy();

    fireEvent.click(screen.getByRole('checkbox'), {
      name: getPresetTitle(viewPresets[0].title, true, false, i18n.t),
    });

    expect(
      screen.getByRole('textbox', {
        name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
      }),
    ).toBeTruthy();
  });

  it('should show viewpreset with different statusses', async () => {
    const viewPresets = [
      {
        mosaicNodeId: 'screen1',
        viewPresetId: 'already-existing',
        hasChanges: true,
        title: 'my existing system preset 1',
        isSelected: true,
        scope: 'system' as const,
      },
      {
        mosaicNodeId: 'screen2',
        viewPresetId: 'already-existing',
        hasChanges: false,
        title: 'my existing system preset 2',
        isSelected: true,
        scope: 'system' as const,
      },
      {
        mosaicNodeId: 'screen3',
        viewPresetId: '',
        hasChanges: true,
        title: 'new one',
        isSelected: true,
        scope: 'system' as const,
      },
      {
        mosaicNodeId: 'screen4',
        viewPresetId: '',
        hasChanges: true,
        title: '',
        isSelected: false,
        scope: 'system' as const,
      },
    ];

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <WorkspaceViewPresetsFormFields viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );

    // existing view preset with changes
    const viewPreset1 = screen.getByText(
      getPresetTitle(viewPresets[0].title, true, false, i18n.t),
    );
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      within(viewPreset1.parentNode?.parentNode as HTMLElement)
        .queryByRole('checkbox')!
        .getAttribute('disabled'),
    ).toBeNull();
    expect(viewPreset1).toBeTruthy();
    expect(screen.getByText(viewPresets[0].mosaicNodeId)).toBeTruthy();

    // existing view preset without changes
    const viewPreset2 = screen.getByText(
      getPresetTitle(viewPresets[1].title, false, false, i18n.t),
    );
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      within(viewPreset2.parentNode?.parentNode as HTMLElement)
        .getByRole('checkbox')!
        .getAttribute('disabled'),
    ).not.toBeNull();
    expect(viewPreset2).toBeTruthy();
    expect(screen.getByText(viewPresets[1].mosaicNodeId)).toBeTruthy();

    // new view preset with changes
    const viewPreset3 = screen.getByText(
      getPresetTitle(viewPresets[2].title, true, true, i18n.t),
    );
    expect(
      screen.getByRole('textbox', {
        name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
      }),
    ).toBeTruthy();
    expect(
      screen
        .getByRole('textbox', {
          name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
        })
        .getAttribute('name'),
    ).toEqual('views.2.title');
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      within(viewPreset3.parentNode?.parentNode as HTMLElement)
        .queryByRole('checkbox')!
        .getAttribute('disabled'),
    ).toBeNull();
    expect(viewPreset3).toBeTruthy();
    expect(screen.getByText(viewPresets[2].mosaicNodeId)).toBeTruthy();

    // new view preset without changes
    const viewPreset4 = screen.getByText(
      getPresetTitle(
        translateKeyOutsideComponents(i18n.t, 'workspace-mappreset-new'),
        true,
        true,
        i18n.t,
      ),
    );
    expect(
      screen.getByRole('textbox', {
        name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
      }),
    ).toBeTruthy();
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      within(viewPreset4.parentNode?.parentNode as HTMLElement)
        .queryByRole('checkbox')!
        .getAttribute('disabled'),
    ).toBeNull();
    expect(viewPreset4).toBeTruthy();
    expect(screen.getByText(viewPresets[3].mosaicNodeId)).toBeTruthy();
  });

  it('should show viewpreset with different statusses for normal users', async () => {
    const viewPresets = [
      {
        mosaicNodeId: 'screen1',
        viewPresetId: 'already-existing',
        hasChanges: true,
        title: 'my existing system preset 1',
        isSelected: true,
        scope: 'system' as const,
      },
      {
        mosaicNodeId: 'screen2',
        viewPresetId: 'already-existing',
        hasChanges: true,
        title: 'my existing user preset 2',
        isSelected: true,
        scope: 'user' as const,
      },
      {
        mosaicNodeId: 'screen3',
        viewPresetId: '',
        hasChanges: true,
        title: 'new one',
        isSelected: true,
        scope: 'system' as const,
      },
      {
        mosaicNodeId: 'screen4',
        viewPresetId: '',
        hasChanges: true,
        title: '',
        isSelected: false,
        scope: 'system' as const,
      },
    ];

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <WorkspaceViewPresetsFormFields
          viewPresets={viewPresets}
          isAdmin={false}
        />
      </ReactHookFormProvider>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-select-modified-viewpresets',
        ),
      ),
    ).toBeTruthy();

    // existing view preset with changes
    const viewPreset1 = screen.getByText(
      getPresetTitle(viewPresets[0].title, true, false, i18n.t),
    );
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      within(viewPreset1.parentNode?.parentNode as HTMLElement)
        .queryByRole('checkbox')!
        .getAttribute('disabled'),
    ).toBeNull();
    expect(viewPreset1).toBeTruthy();
    expect(screen.getByText(viewPresets[0].mosaicNodeId)).toBeTruthy();

    expect(
      screen.getAllByText(
        translateKeyOutsideComponents(
          i18n.t,
          'workspace-save-new-viewpreset-warning-system',
        ),
      ),
    ).toBeTruthy();

    // check titles
    expect(
      screen
        .getAllByRole('textbox', {
          name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
        })[1]
        .getAttribute('name'),
    ).toEqual('views.2.title');

    // existing user view preset with changes
    const viewPreset2 = screen.getByText(
      `${getPresetTitle(viewPresets[1].title, false, false, i18n.t)} (modified)`,
    );
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      within(viewPreset2.parentNode?.parentNode as HTMLElement)
        .queryByRole('checkbox')!
        .getAttribute('disabled'),
    ).toBeNull();
    expect(viewPreset2).toBeTruthy();
    expect(screen.getByText(viewPresets[1].mosaicNodeId)).toBeTruthy();

    // new view preset with changes
    const viewPreset3 = screen.getByText(
      getPresetTitle(viewPresets[2].title, true, true, i18n.t),
    );
    expect(
      screen.getAllByRole('textbox', {
        name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
      }),
    ).toBeTruthy();
    expect(
      screen
        .getAllByRole('textbox', {
          name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
        })[1]
        .getAttribute('name'),
    ).toEqual('views.2.title');
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      within(viewPreset3.parentNode?.parentNode as HTMLElement)
        .queryByRole('checkbox')!
        .getAttribute('disabled'),
    ).toBeNull();
    expect(viewPreset3).toBeTruthy();
    expect(screen.getByText(viewPresets[2].mosaicNodeId)).toBeTruthy();

    // new view preset without changes
    const viewPreset4 = screen.getByText(
      getPresetTitle(
        translateKeyOutsideComponents(i18n.t, 'workspace-mappreset-new'),
        true,
        true,
        i18n.t,
      ),
    );
    expect(
      screen.getAllByRole('textbox', {
        name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
      }),
    ).toBeTruthy();
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      within(viewPreset4.parentNode?.parentNode as HTMLElement)
        .queryByRole('checkbox')!
        .getAttribute('disabled'),
    ).toBeNull();
    expect(viewPreset4).toBeTruthy();
    expect(screen.getByText(viewPresets[3].mosaicNodeId)).toBeTruthy();
  });

  it('should show viewpreset with error', async () => {
    const erorrMessage1 = 'something went wrong';
    const erorrMessage2 = 'title already exists';
    const viewPresets = [
      {
        mosaicNodeId: 'screen1',
        viewPresetId: 'already-existing',
        hasChanges: true,
        title: 'my existing system preset 1',
        isSelected: true,
        scope: 'system' as const,
        error: {
          message: erorrMessage1,
          component: ViewPresetErrorComponent.PRESET_LIST,
          errorType: ViewPresetErrorType.GENERIC,
        },
      },
      {
        mosaicNodeId: 'screen2',
        viewPresetId: '',
        hasChanges: true,
        title: 'my new preset',
        isSelected: true,
        scope: 'system' as const,
        error: {
          message: erorrMessage2,
          component: ViewPresetErrorComponent.PRESET_LIST,
          errorType: ViewPresetErrorType.GENERIC,
        },
      },
    ];

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <WorkspaceViewPresetsFormFields viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );

    expect(screen.getByText(erorrMessage1)).toBeTruthy();
    expect(screen.getByText(erorrMessage2)).toBeTruthy();
    const input = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-name'),
    });
    expect(input.getAttribute('name')).toEqual('views.1.title');
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      input.parentElement!.classList.contains('Mui-error'),
    ).toBeTruthy();
  });

  it('should update view id, but not title from outside component', async () => {
    const viewPreset = {
      mosaicNodeId: 'screen1',
      viewPresetId: 'already-existing',
      hasChanges: true,
      title: 'my existing system preset 1',
      isSelected: true,
      scope: 'system' as const,
    };

    const viewPresets = [viewPreset];

    let watchResult: FieldValues;
    const TestComponent: React.FC<{ viewPresets: WorkspaceDialogView[] }> = ({
      viewPresets,
    }) => {
      const { watch } = useFormContext();
      watchResult = watch();
      return (
        <WorkspaceViewPresetsFormFields
          viewPresets={viewPresets}
          isAdmin={false}
        />
      );
    };
    const { rerender } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <TestComponent viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );

    expect(watchResult!.views[0].title).toEqual(
      `${viewPreset.title} ${viewPreset.mosaicNodeId}`,
    );
    expect(watchResult!.views[0].viewPresetId).toEqual(viewPreset.viewPresetId);

    rerender(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <TestComponent
          viewPresets={[
            { ...viewPreset, title: 'new updated', viewPresetId: 'my-new-id' },
          ]}
        />
      </ReactHookFormProvider>,
    );
    expect(watchResult!.views[0].title).toEqual(
      `${viewPreset.title} ${viewPreset.mosaicNodeId}`,
    );
    expect(watchResult!.views[0].viewPresetId).toEqual('my-new-id');
  });

  it('should not update title suffix for admin presets', async () => {
    const viewPreset = {
      mosaicNodeId: 'screen1',
      viewPresetId: 'already-existing',
      hasChanges: true,
      title: 'my existing system preset 1',
      isSelected: true,
      scope: 'system' as const,
    };

    const viewPresets = [viewPreset];

    let watchResult: FieldValues;
    const TestComponent: React.FC<{ viewPresets: WorkspaceDialogView[] }> = ({
      viewPresets,
    }) => {
      const { watch } = useFormContext();
      watchResult = watch();
      return (
        <WorkspaceViewPresetsFormFields
          viewPresets={viewPresets}
          isAdmin={true}
        />
      );
    };
    const { rerender } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <TestComponent viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );

    expect(watchResult!.views[0].title).toEqual(`${viewPreset.title}`);
    expect(watchResult!.views[0].viewPresetId).toEqual(viewPreset.viewPresetId);

    rerender(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <TestComponent
          viewPresets={[
            { ...viewPreset, title: 'new updated', viewPresetId: 'my-new-id' },
          ]}
        />
      </ReactHookFormProvider>,
    );
    expect(watchResult!.views[0].title).toEqual(`${viewPreset.title}`);
    expect(watchResult!.views[0].viewPresetId).toEqual('my-new-id');
  });

  it('should show abstract input field when view preset is selected', async () => {
    const viewPresets = [
      {
        mosaicNodeId: 'screen1',
        viewPresetId: '',
        hasChanges: true,
        title: 'my existing system preset 1',
        isSelected: true,
        scope: 'system' as const,
      },
    ];

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <WorkspaceViewPresetsFormFields viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );

    expect(
      screen.getByRole('textbox', {
        name: translateKeyOutsideComponents(
          i18n.t,
          'workspace-dialog-abstract',
        ),
      }),
    ).toBeTruthy();
  });

  it('should not show abstract input field when view preset is not selected', async () => {
    const viewPresets = [
      {
        mosaicNodeId: 'screen1',
        viewPresetId: '',
        hasChanges: true,
        title: 'my existing system preset 1',
        isSelected: false,
        scope: 'system' as const,
      },
    ];

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <WorkspaceViewPresetsFormFields viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );

    expect(
      screen.queryByRole('textbox', {
        name: translateKeyOutsideComponents(
          i18n.t,
          'workspace-dialog-abstract',
        ),
      }),
    ).toBeFalsy();
  });

  it('should update abstract field value correctly', async () => {
    const viewPresets = [
      {
        mosaicNodeId: 'screen1',
        viewPresetId: '',
        hasChanges: true,
        title: 'my existing system preset 1',
        isSelected: true,
        scope: 'system' as const,
      },
    ];

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <WorkspaceViewPresetsFormFields viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );

    const abstractInput = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
    });

    fireEvent.change(abstractInput, { target: { value: 'New abstract' } });

    expect(abstractInput.textContent).toEqual('New abstract');
  });

  it('should disable abstract input field when view preset has no changes', async () => {
    const viewPresets = [
      {
        mosaicNodeId: 'screen1',
        viewPresetId: '',
        hasChanges: false,
        title: 'my existing system preset 1',
        isSelected: true,
        scope: 'system' as const,
      },
    ];

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            views: viewPresets,
          },
        }}
      >
        <WorkspaceViewPresetsFormFields viewPresets={viewPresets} />
      </ReactHookFormProvider>,
    );

    const abstractInput = screen.getByRole('textbox', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-dialog-abstract'),
    });

    expect(abstractInput.getAttribute('disabled')).toBeDefined();
  });
});
