/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  act,
  waitFor,
} from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { GEOWEB_ROLE_PRESETS_ADMIN } from '@opengeoweb/authentication';
import {
  defaultAuthConfig,
  WorkspaceWrapperProviderWithStore,
} from '../Providers/Providers';
import {
  WorkspaceOptionsConnect,
  WorkspaceOptionsConnectProps,
} from './WorkspaceOptionsConnect';
import {
  WorkspaceState,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import { getWorkspaceData } from '../../store/workspace/selectors';
import { workspaceActions } from '../../store/workspace/reducer';
import { viewPresetActions } from '../../store/viewPresets';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store/store';
import { workspacePresetOptionId } from './WorkspaceOptions';

describe('workspace/components/WorkspaceOptions/WorkspaceOptionsConnect', () => {
  const screenConfig: WorkspaceState = {
    id: 'preset1',
    title: 'Preset 1',
    scope: 'system',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode: {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
    },
  };

  it('should render WorkspaceOptionsConnect', async () => {
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText(props.title)).toBeTruthy();
    const tab = screen.queryByTestId(workspacePresetOptionId);
    const styles = getComputedStyle(tab!);
    expect(styles.fontStyle).not.toEqual('italic');

    expect(screen.getByText(props.title)).toBeTruthy();
  });

  it('should show title in italic when new workspace has changes', async () => {
    const testWorkspacePreset: WorkspaceState = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1'],
        byId: {
          screen1: {
            title: 'screen 1',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
        },
      },
      hasChanges: true,
      mosaicNode: 'map',
    };
    const props = {
      title: testWorkspacePreset.title,
      workspaceId: '',
    };
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.setActiveViewPresetId({
          viewPresetId: 'test-1',
          panelId: 'panel-2',
        }),
      ),
    );
    const tab = screen.queryByTestId(workspacePresetOptionId);
    const styles = getComputedStyle(tab!);
    expect(styles.fontStyle).toEqual('italic');

    expect(
      screen.getByText(
        `${props.title} (${translateKeyOutsideComponents(i18n.t, 'workspace-modified-existing')})`,
      ),
    ).toBeTruthy();
  });

  it('should show title in italic when the workspace has changes', async () => {
    const testWorkspacePreset: WorkspaceState = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1', 'screen2'],
        byId: {
          screen1: {
            title: 'screen 1',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen2: {
            title: 'screen 2',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
        },
      },
      hasChanges: true,
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
      },
    };
    const props = {
      title: testWorkspacePreset.title,
      workspaceId: 'preset1',
    };
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.setActiveViewPresetId({
          viewPresetId: 'test-1',
          panelId: 'panel-2',
        }),
      ),
    );
    const tab = screen.queryByTestId(workspacePresetOptionId);
    const styles = getComputedStyle(tab!);
    expect(styles.fontStyle).toEqual('italic');

    expect(
      screen.getByText(
        `${props.title} (${translateKeyOutsideComponents(i18n.t, 'workspace-modified-new')})`,
      ),
    ).toBeTruthy();
  });

  it('should disable save option for system preset', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveMenuItem = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveMenuItem.getAttribute('aria-disabled')).toBe('true');
  });

  it('should enable delete option for user preset and open dialog on click', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: { ...screenConfig, scope: 'user' },
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const deleteMenuItem = screen.getByRole('menuitem', { name: 'Delete' });
    expect(deleteMenuItem.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(deleteMenuItem);
    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.DELETE,
        presetId: props.workspaceId,
        formValues: { title: props.title },
        isFetching: false,
      }),
    );
  });

  it('should enable save option for user preset and save preset with changes', async () => {
    const store = createMockStore();

    const testWorkspacePreset: WorkspaceState = {
      id: 'preset1',
      title: 'Preset 1',
      scope: 'system',
      views: {
        allIds: ['screen1', 'screen2'],
        byId: {
          screen1: {
            title: 'screen 1',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
            scope: 'system',
          },
          screen2: {
            title: 'screen 2',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
            scope: 'user',
          },
        },
      },
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
      },
    };

    const props = {
      title: testWorkspacePreset.title,
      workspaceId: 'preset1',
    };
    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveWorkspacePreset: mockSave,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        createApi={createApi}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: {
            ...testWorkspacePreset,
            scope: 'user',
          },
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.setActiveViewPresetId({
          viewPresetId: 'test-1',
          panelId: 'panel-2',
        }),
      ),
    );
    expect(
      screen.getByText(
        `${props.title} (${translateKeyOutsideComponents(i18n.t, 'workspace-modified-new')})`,
      ),
    ).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveButton = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveButton);

    const formValues = getWorkspaceData(store.getState());

    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.SAVE,
        presetId: props.workspaceId,
        formValues: {
          ...formValues,
          views: formValues.views.map((view, index) => ({
            ...view,
            isSelected: false,
            scope: index === 0 ? 'system' : 'user',
          })),
        },
        isFetching: false,
        hasSaved: false,
      }),
    );
  });

  it('should enable save option for user preset and save even if there no are changes', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveWorkspacePreset: mockSave,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        createApi={createApi}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: {
            ...screenConfig,
            scope: 'user',
          },
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveButton = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveButton);

    const formValues = getWorkspaceData(store.getState());

    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.SAVE,
        presetId: props.workspaceId,
        formValues: {
          ...formValues,
          views: formValues.views.map((view) => ({
            ...view,
            isSelected: false,
            scope: 'system',
          })),
        },
        isFetching: false,
        hasSaved: false,
      }),
    );
  });

  it('should enable save as for user preset and open dialog click', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: { ...screenConfig, scope: 'user' },
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveAsButton = screen.getByRole('menuitem', { name: 'Save as' });
    expect(saveAsButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveAsButton);

    const formValues = getWorkspaceData(store.getState());

    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.SAVE_AS,
        presetId: props.workspaceId,
        formValues: {
          ...formValues,
          views: formValues.views.map((view) => ({
            ...view,
            isSelected: false,
            scope: 'system',
          })),
        },
        isFetching: false,
        hasSaved: false,
      }),
    );
  });

  it('should disable the workspace option button when no viewpresets in store', async () => {
    const testWorkspacePreset: WorkspaceState = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: [],
        byId: {},
      },
      hasChanges: true,
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
      },
    };
    const props = {
      title: testWorkspacePreset.title,
      workspaceId: 'preset1',
    };
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: testWorkspacePreset,
        }),
      ),
    );
    const tab = screen.getByRole('button');
    expect(tab.classList).toContain('Mui-disabled');
  });

  it('should disable the workspace option button when the user is not logged in', async () => {
    const store = createMockStore();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    const authLoggedOutProps = {
      isLoggedIn: false, // user is not logged in
      auth: {
        username: 'user.name',
        token: '1223344',
        refresh_token: '33455214',
      },
      onLogin: (): void => null!,
      onSetAuth: (): void => null!,
      sessionStorageProvider: null!,
    };
    render(
      <WorkspaceWrapperProviderWithStore
        auth={authLoggedOutProps}
        store={store}
        theme={lightTheme}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    const tab = screen.getByRole('button');
    expect(tab.classList).toContain('Mui-disabled');
  });

  it('should open dialog when pressing Edit for user workspace', async () => {
    const store = createMockStore();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: { ...screenConfig, scope: 'user' },
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const editButton = screen.getByRole('menuitem', {
      name: 'Edit (name and abstract)',
    });
    expect(editButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(editButton);
    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.EDIT,
        presetId: props.workspaceId,
        formValues: getWorkspaceData(store.getState()),
        isFetching: false,
      }),
    );
  });

  it('should disable Edit for system presets', async () => {
    const store = createMockStore();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const editButton = screen.getByRole('menuitem', {
      name: 'Edit (name and abstract)',
    });
    expect(editButton.getAttribute('aria-disabled')).toBe('true');
  });

  it('should disable save for system preset for admins for new workspace', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: '',
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        auth={{
          ...defaultAuthConfig,
          currentRole: GEOWEB_ROLE_PRESETS_ADMIN,
        }}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: { ...screenConfig, scope: 'user' },
        }),
      ),
    );

    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveBtn = screen.getByRole('menuitem', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-save'),
    });
    expect(saveBtn.getAttribute('aria-disabled')).toBe('true');
  });

  it('should enable save as for system preset for admins and open dialog click', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        auth={{
          ...defaultAuthConfig,
          currentRole: GEOWEB_ROLE_PRESETS_ADMIN,
        }}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveAsButton = screen.getByRole('menuitem', {
      name: translateKeyOutsideComponents(
        i18n.t,
        'workspace-admin-save-new-workspace',
      ),
    });
    expect(saveAsButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveAsButton);

    const formValues = getWorkspaceData(store.getState());

    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
        presetId: props.workspaceId,
        formValues: {
          ...formValues,
          views: formValues.views.map((view) => ({
            ...view,
            isSelected: false,
          })),
        },
        isFetching: false,
        hasSaved: false,
      }),
    );
  });

  it('should enable save for system preset for admins and open dialog click', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        auth={{
          ...defaultAuthConfig,
          currentRole: GEOWEB_ROLE_PRESETS_ADMIN,
        }}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveAsButton = screen.getByRole('menuitem', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-save'),
    });
    expect(saveAsButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveAsButton);

    const formValues = getWorkspaceData(store.getState());

    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.SAVE_SYSTEM_PRESET,
        presetId: props.workspaceId,
        formValues: {
          ...formValues,
          views: formValues.views.map((view) => ({
            ...view,
            isSelected: false,
          })),
        },
        isFetching: false,
        hasSaved: false,
      }),
    );
  });

  it('should enable delete option for system preset for admins and open dialog on click', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        auth={{
          ...defaultAuthConfig,
          currentRole: GEOWEB_ROLE_PRESETS_ADMIN,
        }}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const deleteMenuItem = screen.getByRole('menuitem', { name: 'Delete' });
    expect(deleteMenuItem.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(deleteMenuItem);
    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.DELETE_SYSTEM_PRESET,
        presetId: props.workspaceId,
        formValues: { title: props.title },
        isFetching: false,
      }),
    );
  });

  it('should disable delete option for system preset for normal users', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const deleteMenuItem = screen.getByRole('menuitem', { name: 'Delete' });
    expect(deleteMenuItem.getAttribute('aria-disabled')).toBe('true');
  });

  it('should disable delete option for new preset', async () => {
    const store = createMockStore();
    const props: WorkspaceOptionsConnectProps = {
      title: screenConfig.title,
      workspaceId: undefined!,
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        auth={{
          ...defaultAuthConfig,
          currentRole: GEOWEB_ROLE_PRESETS_ADMIN,
        }}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const deleteMenuItem = screen.getByRole('menuitem', { name: 'Delete' });
    expect(deleteMenuItem.getAttribute('aria-disabled')).toBe('true');
  });

  it('should enable reset option for system preset for admins and open dialog on click', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        auth={{
          ...defaultAuthConfig,
          currentRole: GEOWEB_ROLE_PRESETS_ADMIN,
        }}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const resetMenuItem = screen.getByRole('menuitem', { name: 'Reset' });
    expect(resetMenuItem.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(resetMenuItem);
    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.RESET,
        presetId: props.workspaceId,
        formValues: { title: props.title, scope: 'system' },
        isFetching: false,
        error: undefined,
      }),
    );
  });

  it('should enable reset option for user preset for normal users and open dialog on click', async () => {
    const store = createMockStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const resetMenuItem = screen.getByRole('menuitem', {
      name: translateKeyOutsideComponents(i18n.t, 'workspace-reset'),
    });
    expect(resetMenuItem.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(resetMenuItem);
    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.RESET,
        presetId: props.workspaceId,
        formValues: { title: props.title, scope: 'user' },
        isFetching: false,
        error: undefined,
      }),
    );
  });
});
