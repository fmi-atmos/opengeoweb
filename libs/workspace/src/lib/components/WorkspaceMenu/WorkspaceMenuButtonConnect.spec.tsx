/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  render,
  fireEvent,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { GEOWEB_ROLE_PRESETS_ADMIN } from '@opengeoweb/authentication';
import {
  defaultAuthConfig,
  WorkspaceWrapperProviderWithStore,
} from '../Providers/Providers';
import { WorkspaceMenuButtonConnect } from './WorkspaceMenuButtonConnect';
import { PresetsApi } from '../../utils/api';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { createMockStore } from '../../store/store';
import { WorkspaceListFilter } from '../../store/workspaceList/types';
import { workspaceListActions } from '../../store/workspaceList';
import { constructFilterParams } from '../../store/workspaceList/utils';

const workspaceList = [
  {
    id: 'workspaceList-1',
    scope: 'system' as const,
    title: 'Workspace list item 1',
    date: '2022-06-01T12:34:27.787192',
    viewType: 'singleWindow' as const,
    abstract: '',
  },
  {
    id: 'workspaceList-2',
    scope: 'system' as const,
    title: 'Workspace list item 2',
    date: '2022-06-01T12:34:27.787192',
    viewType: 'singleWindow' as const,
    abstract: '',
  },
  {
    id: 'workspaceList-3',
    scope: 'user' as const,
    title: 'Workspace list item 3',
    date: '2022-06-01T12:34:27.787192',
    viewType: 'singleWindow' as const,
    abstract: '',
  },
];

describe('WorkspaceMenuButtonConnect', () => {
  it('should render correctly upon first opening', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceMenuButton')).toBeTruthy();
    expect(store.getState().workspaceList.isFetching).toEqual(true);
    expect(store.getState().workspaceList.isWorkspaceListDialogOpen).toEqual(
      false,
    );
  });

  it('should be able to trigger dialog', async () => {
    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceMenuButton')).toBeTruthy();
    expect(store.getState().workspaceList.isFetching).toEqual(true);
    expect(store.getState().workspaceList.isWorkspaceListDialogOpen).toEqual(
      false,
    );

    // toggle
    fireEvent.click(screen.getByTestId('workspaceMenuButton'));

    expect(store.getState().workspaceList.isWorkspaceListDialogOpen).toEqual(
      true,
    );
  });

  it('should fetch initial workspace presets and change filter after changing to admin', async () => {
    const store = createMockStore();
    const mockGetWorkspaces = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getWorkspacePresets: mockGetWorkspaces,
      };
    };
    const { rerender } = render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    await waitFor(() =>
      expect(mockGetWorkspaces).toHaveBeenLastCalledWith({
        scope: 'user,system',
      }),
    );

    store
      .getState()
      .workspaceList.workspaceListFilters.forEach(
        (filter: WorkspaceListFilter) => {
          expect(filter.isSelected).toBeTruthy();
        },
      );

    // rerender to admin
    rerender(
      <WorkspaceWrapperProviderWithStore
        store={store}
        createApi={createApi}
        auth={{ ...defaultAuthConfig, currentRole: GEOWEB_ROLE_PRESETS_ADMIN }}
      >
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    const result = store.getState().workspaceList.workspaceListFilters;
    expect(result[0].id).toEqual('user');
    expect(result[0].isSelected).toBeFalsy();
    expect(result[1].id).toEqual('system');
    expect(result[1].isSelected).toBeTruthy();

    await waitFor(() =>
      expect(mockGetWorkspaces).toHaveBeenLastCalledWith({
        scope: 'system',
      }),
    );

    // rerender to normal user
    rerender(
      <WorkspaceWrapperProviderWithStore
        store={store}
        createApi={createApi}
        auth={{ ...defaultAuthConfig }}
      >
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    const result2 = store.getState().workspaceList.workspaceListFilters;
    expect(result2[0].id).toEqual('user');
    expect(result2[0].isSelected).toBeTruthy();
    expect(result2[1].id).toEqual('system');
    expect(result2[1].isSelected).toBeTruthy();
    await waitFor(() =>
      expect(mockGetWorkspaces).toHaveBeenLastCalledWith({
        scope: 'user,system',
      }),
    );
  });

  it('should fetch workspace presets with search param', async () => {
    const testSearchQuery = 'testing test,test2';
    const store = createMockStore();

    const mockGetWorkspaces = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getWorkspacePresets: mockGetWorkspaces,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.searchFilter({
          searchQuery: testSearchQuery,
        }),
      ),
    );

    await waitFor(() =>
      expect(mockGetWorkspaces).toHaveBeenLastCalledWith(
        constructFilterParams(
          store.getState().workspaceList.workspaceListFilters,
          testSearchQuery,
        ),
      ),
    );
  });

  it('should fetch correct list if All is selected and My presets filter chip is clicked', async () => {
    const store = createMockStore();

    const mockGetWorkspaces = jest
      .fn()
      .mockResolvedValue({ data: workspaceList });
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getWorkspacePresets: mockGetWorkspaces,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore createApi={createApi} store={store}>
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(mockGetWorkspaces).toHaveBeenCalledTimes(1);

    await act(() =>
      store.dispatch(
        workspaceListActions.toggleSelectFilterChip({
          id: 'user',
          isSelected: true,
        }),
      ),
    );
    await waitFor(() => expect(mockGetWorkspaces).toHaveBeenCalledTimes(2));
    await waitFor(() =>
      expect(mockGetWorkspaces).toHaveBeenLastCalledWith({ scope: 'user' }),
    );
  });

  it('should fetch correct list if all is selected and System presets filterchip is clicked', async () => {
    const store = createMockStore();

    const mockGetWorkspaces = jest
      .fn()
      .mockResolvedValue({ data: workspaceList });
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getWorkspacePresets: mockGetWorkspaces,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore createApi={createApi} store={store}>
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(mockGetWorkspaces).toHaveBeenCalledTimes(1);

    await act(() =>
      store.dispatch(
        workspaceListActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: true,
        }),
      ),
    );
    await waitFor(() => expect(mockGetWorkspaces).toHaveBeenCalledTimes(2));
    await waitFor(() =>
      expect(mockGetWorkspaces).toHaveBeenLastCalledWith({ scope: 'system' }),
    );
  });

  it('should not fetch new list if filters did not change', async () => {
    const store = createMockStore();

    const mockGetWorkspaces = jest
      .fn()
      .mockResolvedValue({ data: workspaceList });
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getWorkspacePresets: mockGetWorkspaces,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore createApi={createApi} store={store}>
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(mockGetWorkspaces).toHaveBeenCalledTimes(1);

    await act(() =>
      store.dispatch(
        workspaceListActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: true,
        }),
      ),
    );
    expect(mockGetWorkspaces).toHaveBeenCalledTimes(2);
    await act(() =>
      store.dispatch(
        workspaceListActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: true,
        }),
      ),
    );
    expect(mockGetWorkspaces).toHaveBeenCalledTimes(2);
  });

  it('should fetch correct list if one filter chip is selected and all is chosen', async () => {
    const store = createMockStore();

    const mockGetWorkspaces = jest
      .fn()
      .mockResolvedValue({ data: workspaceList });
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getWorkspacePresets: mockGetWorkspaces,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore createApi={createApi} store={store}>
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    // make sure my presets is selected
    await act(() =>
      store.dispatch(
        workspaceListActions.toggleSelectFilterChip({
          id: 'user',
          isSelected: true,
        }),
      ),
    );
    await waitFor(() =>
      expect(mockGetWorkspaces).toHaveBeenLastCalledWith({
        scope: 'user',
      }),
    );
    expect(mockGetWorkspaces).toHaveBeenCalledTimes(2);

    await act(() =>
      store.dispatch(
        workspaceListActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: true,
        }),
      ),
    );
    // It should fetch new list
    await waitFor(() => expect(mockGetWorkspaces).toHaveBeenCalledTimes(3));
    await waitFor(() =>
      expect(mockGetWorkspaces).toHaveBeenLastCalledWith({
        scope: 'user,system',
      }),
    );
  });
});
