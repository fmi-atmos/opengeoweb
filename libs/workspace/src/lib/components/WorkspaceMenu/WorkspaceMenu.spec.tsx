/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { WorkspaceMenu } from './WorkspaceMenu';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';

describe('WorkspaceMenu', () => {
  it('should render correctly closed', async () => {
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenu />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(
      screen.queryByText(
        translateKeyOutsideComponents(i18n.t, 'workspace-menu'),
      ),
    ).toBeFalsy();
    expect(screen.queryByTestId('workspace-selectList')).toBeFalsy();
  });

  it('should render correctly opened', async () => {
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenu isOpen />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents(i18n.t, 'workspace-menu')),
    ).toBeTruthy();
    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();
  });

  it('should call closeMenu when close button is pressed', async () => {
    const props = {
      closeMenu: jest.fn(),
      isOpen: true,
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('closeBtn'));
    expect(props.closeMenu).toHaveBeenCalled();
  });

  it('should render the list', async () => {
    const props = {
      closeMenu: jest.fn(),
      isOpen: true,
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();
  });

  it('should call closeMenu when backdrop is clicked', async () => {
    const props = {
      closeMenu: jest.fn(),
      isOpen: true,
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('workspace-menu')!.firstChild!);
    expect(props.closeMenu).toHaveBeenCalled();
  });
});
