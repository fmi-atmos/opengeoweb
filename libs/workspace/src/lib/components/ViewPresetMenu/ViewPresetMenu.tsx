/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { CustomDialog, renderCounter, ToolContainer } from '@opengeoweb/shared';
import { ViewPresetMenuListConnect } from './ViewPresetMenuListConnect';
import { useWorkspaceTranslation } from '../../utils/i18n';

export const DIALOG_TITLE = 'View presets menu';

export interface ViewPresetMenuProps {
  panelId: string;
  closeMenu?: () => void;
  isOpen?: boolean;
}

export const ViewPresetMenu: React.FC<ViewPresetMenuProps> = ({
  closeMenu,
  panelId,
  isOpen = false,
}: ViewPresetMenuProps) => {
  const { t } = useWorkspaceTranslation();

  renderCounter.count(ViewPresetMenu.name);

  return (
    <CustomDialog
      open={isOpen}
      onClose={(): void => {
        if (closeMenu) {
          closeMenu();
        }
      }}
      isResizable
      hasLayout={false}
      resizableDefaultSize={{ width: 380, height: '90%' }}
      shouldRenderInline
      data-testid="workspace-viewpreset-menu"
    >
      <ToolContainer
        title={t('workspace-viewpreset-menu')}
        onClose={closeMenu}
        style={{ width: '100%', height: '100%' }}
        onClick={(event): void => {
          event.stopPropagation();
        }}
      >
        <ViewPresetMenuListConnect panelId={panelId} />
      </ToolContainer>
    </CustomDialog>
  );
};
