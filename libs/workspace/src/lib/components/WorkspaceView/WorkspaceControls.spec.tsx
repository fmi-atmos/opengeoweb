/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
  act,
} from '@testing-library/react';
import * as mosaic from 'react-mosaic-component';
import WorkspaceControls from './WorkspaceControls';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { workspaceActions } from '../../store/workspace/reducer';
import { createMockStore } from '../../store/store';

jest.mock('react-mosaic-component');

const { MosaicContext } = mosaic;

const props = {
  createNode: jest.fn(),
  path: ['testing'] as unknown as mosaic.MosaicBranch[],
  panelId: 'test-ing',
};

describe('components/WorkspaceView/WorkspaceControls', () => {
  const screenConfig = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'Map',
          initialProps: {
            mapPreset: {
              layers: [
                {
                  service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                  name: 'RAD_NL25_PCP_CM',
                  format: 'image/png',
                  enabled: true,
                  layerType: 'mapLayer',
                },
              ],
              proj: {
                bbox: {
                  left: -450651.2255879827,
                  bottom: 6490531.093143953,
                  right: 1428345.8183648037,
                  top: 7438773.776232235,
                },
                srs: 'EPSG:3857',
              },
            },
          },
        },
      },
    },
    mosaicNode: {
      direction: 'row' as const,
      first: 'screen1',
      second: 'screen2',
      splitPercentage: 33.3,
    },
  };

  it('should render button content', () => {
    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
      blueprintNamespace: '',
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <MosaicContext.Provider value={mosaicActions}>
          <WorkspaceControls {...props} />,
        </MosaicContext.Provider>
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('split-vertical-btn')).toBeTruthy();
    expect(screen.getByTestId('split-horizontal-btn')).toBeTruthy();
    expect(screen.getByTestId('expand-btn')).toBeTruthy();
    expect(screen.getByTestId('close-btn')).toBeTruthy();
  });

  it('should split window vertically', () => {
    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
      blueprintNamespace: '',
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <MosaicContext.Provider value={mosaicActions}>
          <WorkspaceControls {...props} />,
        </MosaicContext.Provider>
      </WorkspaceWrapperProviderWithStore>,
    );
    const splitButton = screen.getByTestId('split-vertical-btn');
    expect(splitButton).toBeTruthy();
    fireEvent.click(splitButton);
    expect(mosaicActions.mosaicActions.replaceWith).toHaveBeenCalledWith(
      props.path,
      {
        direction: 'row',
      },
    );
  });

  it('should split window horizontally', () => {
    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
      blueprintNamespace: '',
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <MosaicContext.Provider value={mosaicActions}>
          <WorkspaceControls {...props} />,
        </MosaicContext.Provider>
      </WorkspaceWrapperProviderWithStore>,
    );
    const splitButton = screen.getByTestId('split-horizontal-btn');
    expect(splitButton).toBeTruthy();
    fireEvent.click(splitButton);
    expect(mosaicActions.mosaicActions.replaceWith).toHaveBeenCalledWith(
      props.path,
      {
        direction: 'column',
      },
    );
  });

  it('should expand and unexpand window', async () => {
    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
      blueprintNamespace: '',
    };

    const store = createMockStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <MosaicContext.Provider value={mosaicActions}>
          <WorkspaceControls {...props} />,
        </MosaicContext.Provider>
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    const expandButton = screen.getByTestId('expand-btn');
    expect(expandButton).toBeTruthy();
    fireEvent.click(expandButton);
    expect(mosaicActions.mosaicActions.expand).toHaveBeenCalledWith(
      props.path,
      70,
    );

    // Now we expect unexpand to take us back to the original split size
    const unexpandButton = screen.getByTestId('unexpand-btn');
    expect(unexpandButton).toBeTruthy();
    fireEvent.click(unexpandButton);
    expect(mosaicActions.mosaicActions.replaceWith).toHaveBeenCalledWith(
      [],
      screenConfig.mosaicNode,
    );
  });

  it('should close window', () => {
    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
      blueprintNamespace: '',
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <MosaicContext.Provider value={mosaicActions}>
          <WorkspaceControls {...props} />,
        </MosaicContext.Provider>
      </WorkspaceWrapperProviderWithStore>,
    );
    const closeButton = screen.getByTestId('close-btn');
    expect(closeButton).toBeTruthy();
    fireEvent.click(closeButton);
    expect(mosaicActions.mosaicActions.remove).toHaveBeenCalledWith(props.path);
  });

  it('should ask for confirmation before closing window', async () => {
    const preventCloseProps = {
      ...props,
      shouldPreventCloseView: true,
    };

    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
      blueprintNamespace: '',
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <MosaicContext.Provider value={mosaicActions}>
          <WorkspaceControls {...preventCloseProps} />,
        </MosaicContext.Provider>
      </WorkspaceWrapperProviderWithStore>,
    );
    const closeButton = screen.getByTestId('close-btn');
    expect(closeButton).toBeTruthy();
    fireEvent.click(closeButton);
    expect(mosaicActions.mosaicActions.remove).not.toHaveBeenCalledWith(
      props.path,
    );

    let confirmationDialog: HTMLElement;
    await waitFor(() => {
      confirmationDialog = screen.getByTestId('confirmationDialog');
      expect(confirmationDialog).toBeTruthy();
    });
    fireEvent.click(
      within(confirmationDialog!).getByTestId('confirmationDialog-confirm'),
    );
    await waitFor(() => {
      expect(mosaicActions.mosaicActions.remove).toHaveBeenCalledWith(
        props.path,
      );
    });
  });
});
