/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { GEOWEB_ROLE_PRESETS_ADMIN } from '@opengeoweb/authentication';
import { routerActions } from '@opengeoweb/store';
import { lightTheme } from '@opengeoweb/theme';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { createMockStore } from '../../store/store';
import { initialState, workspaceActions } from '../../store/workspace/reducer';
import {
  WorkspacePreset,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import {
  WorkspaceListErrorCategory,
  WorkspaceListErrorType,
} from '../../store/workspaceList/types';
import { getEmptyMapWorkspace } from '../../store/workspaceList/utils';
import { PresetsApi } from '../../utils/api';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';
import { getWorkspaceRouteUrl } from '../../utils/routes';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspaceSelectListConnect } from './WorkspaceSelectListConnect';

describe('workspace/components/WorkspaceSelectListConnect', () => {
  const workspaceConfig: WorkspacePreset = {
    id: 'workspaceList-2',
    title: 'Workspace list item 2',
    viewType: 'singleWindow' as const,
    abstract: '',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode: {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
    },
  };
  const workspaceList = [
    {
      id: 'workspaceList-1',
      scope: 'system' as const,
      title: 'Workspace list item 1',
      date: '2022-06-01T12:34:27.787192',
      viewType: 'singleWindow' as const,
      abstract: '',
    },
    {
      id: 'workspaceList-2',
      scope: 'system' as const,
      title: 'Workspace list item 2',
      date: '2022-06-01T12:34:27.787192',
      viewType: 'singleWindow' as const,
      abstract: '',
    },
    {
      id: 'workspaceList-3',
      scope: 'user' as const,
      title: 'Workspace list item 3',
      date: '2022-06-01T12:34:27.787192',
      viewType: 'singleWindow' as const,
      abstract: '',
    },
  ];

  it('should render the component', () => {
    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();
    expect(screen.getByText(getEmptyMapWorkspace(i18n.t).title)).toBeTruthy();
  });

  it('should show the list items and selected workspace', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({ workspaceList }),
      ),
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: workspaceConfig }),
      ),
    );

    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();

    expect(screen.getByText(workspaceList[1].title)).toBeTruthy();
    expect(screen.getByText(workspaceList[2].title)).toBeTruthy();

    const rows = screen.getAllByTestId('workspace-selectListRow');
    // Third row should be selected
    expect(rows[2].classList).toContain('Mui-selected');
  });

  it('should only show new workspace as selected if no workspace selected', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({ workspaceList }),
      ),
    );
    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();
    expect(screen.getByText(workspaceList[1].title)).toBeTruthy();
    expect(screen.getByText(workspaceList[2].title)).toBeTruthy();
    const rows = screen.getAllByTestId('workspace-selectListRow');
    // only new workspace should be selected
    expect(rows[0].textContent).toEqual('New workspace');
    expect(rows[0].classList).toContain('Mui-selected');
    expect(rows[1].classList).not.toContain('Mui-selected');
  });

  it('should fetch selected workspace', async () => {
    const store = createMockStore();
    const mockGetWorkspace = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getWorkspacePreset: mockGetWorkspace,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({ workspaceList }),
      ),
    );

    fireEvent.click(screen.getByText(workspaceList[0].title));
    await waitFor(() =>
      expect(mockGetWorkspace).toHaveBeenLastCalledWith(workspaceList[0].id),
    );
  });

  it('should open the preset as defined in the FE and close the dialog when clicking the `New Workspace` preset from the list', async () => {
    const store = createMockStore();
    const spy = jest.spyOn(routerActions, 'navigateToUrl');

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceListActions.toggleWorkspaceDialog({
          isWorkspaceListDialogOpen: true,
        }),
      ),
    );
    fireEvent.click(await screen.findByText('New workspace'));

    await waitFor(() =>
      expect(spy).toHaveBeenCalledWith({ url: getWorkspaceRouteUrl() }),
    );

    await waitFor(() =>
      expect(store.getState().workspaceList.isWorkspaceListDialogOpen).toEqual(
        false,
      ),
    );
    await waitFor(() =>
      expect(store.getState().workspace).toEqual({
        ...initialState,
        id: undefined,
        syncGroups: [],
        ...getEmptyMapWorkspace(i18n.t),
      }),
    );
  });

  it('should do nothing when clicking a workspace while list is loading', async () => {
    const store = createMockStore();
    const mockGetWorkspace = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        getWorkspacePreset: mockGetWorkspace,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({ workspaceList }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceListActions.fetchWorkspaceList({})),
    );

    await act(() =>
      store.dispatch(
        workspaceListActions.toggleWorkspaceDialog({
          isWorkspaceListDialogOpen: true,
        }),
      ),
    );
    expect(store.getState().workspaceList.isFetching).toEqual(true);
    expect(store.getState().workspaceList.isWorkspaceListDialogOpen).toEqual(
      true,
    );
    fireEvent.click(screen.getByText(workspaceList[0].title));
    expect(store.getState().workspaceList.isFetching).toEqual(true);

    expect(store.getState().workspaceList.isWorkspaceListDialogOpen).toEqual(
      true,
    );
    expect(mockGetWorkspace).not.toHaveBeenCalled();
  });

  it('should show loading bar when list is loading', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(workspaceListActions.fetchWorkspaceList({})),
    );
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
  });

  it('should open dialog when clicking delete', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({ workspaceList }),
      ),
    );
    const deleteButtons = screen.getAllByTestId('workspace-listDeleteButton');
    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();
    expect(deleteButtons.length).toBe(1);
    fireEvent.click(deleteButtons[0]);

    expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
      action: WorkspacePresetAction.DELETE,
      presetId: 'workspaceList-3',
      formValues: { title: 'Workspace list item 3', scope: 'user' },
      isFetching: false,
    });
  });

  it('should open dialog when clicking Duplicate for FE defined `New workspace`', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    fireEvent.click(screen.getAllByTestId('workspace-listOptionsButton')[0]);
    expect(screen.getByText('Options')).toBeTruthy();

    expect(screen.getByText('Duplicate')).toBeTruthy();
    fireEvent.click(screen.getByText('Duplicate'));

    expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
      action: WorkspacePresetAction.DUPLICATE,
      formValues: {
        title: getEmptyMapWorkspace(i18n.t).title,
        abstract: getEmptyMapWorkspace().abstract,
        scope: 'user',
      },
      isFetching: false,
    });
  });

  it('should open dialog when clicking edit', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({ workspaceList }),
      ),
    );
    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();
    fireEvent.click(screen.queryAllByTestId('workspace-listOptionsButton')[3]);
    fireEvent.click(await screen.findByText('Edit (name and abstract)'));

    expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
      action: WorkspacePresetAction.EDIT,
      presetId: 'workspaceList-3',
      formValues: {
        title: 'Workspace list item 3',
        abstract: '',
        scope: 'user',
      },
      isFetching: false,
    });
  });

  it('should open the dialog when clicking Duplicate for a workspace', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({ workspaceList }),
      ),
    );
    fireEvent.click(screen.getAllByTestId('workspace-listOptionsButton')[3]);
    expect(screen.getByText('Options')).toBeTruthy();
    expect(screen.getByText('Duplicate')).toBeTruthy();
    fireEvent.click(screen.getByText('Duplicate'));

    expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
      action: WorkspacePresetAction.DUPLICATE,
      presetId: 'workspaceList-3',
      formValues: {
        title: 'Workspace list item 3',
        abstract: '',
        scope: 'user',
      },
      isFetching: false,
    });
  });

  it('should show alert banner and try again button when list has a generic error', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.errorWorkspaceList({
          error: {
            category: WorkspaceListErrorCategory.GENERIC,
            key: 'workspace-error-fetch',
            type: WorkspaceListErrorType.TRANSLATABLE_ERROR,
          },
        }),
      ),
    );
    expect(screen.getByTestId('alert-banner')).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents(i18n.t, 'workspace-retry').toUpperCase(),
      ),
    ).toBeTruthy();
  });

  it('should show alert banner and no try again button when list has an auth error', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.errorWorkspaceList({
          error: {
            category: WorkspaceListErrorCategory.AUTH,
            message: 'Forbidden',
            type: WorkspaceListErrorType.VERBATIM_ERROR,
          },
        }),
      ),
    );
    expect(screen.getByTestId('alert-banner')).toBeTruthy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents(i18n.t, 'workspace-retry').toUpperCase(),
      ),
    ).toBeFalsy();
  });

  it('should not show filter options when there is an error', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({ workspaceList }),
      ),
    );
    expect(screen.getByText('All')).toBeTruthy();
    expect(screen.getByText('Search')).toBeTruthy();
    expect(screen.getByTestId('filterList')).toBeTruthy();

    await act(() =>
      store.dispatch(
        workspaceListActions.errorWorkspaceList({
          error: {
            category: WorkspaceListErrorCategory.GENERIC,
            key: 'workspace-error-fetch',
            type: WorkspaceListErrorType.TRANSLATABLE_ERROR,
          },
        }),
      ),
    );
    expect(screen.getByTestId('alert-banner')).toBeTruthy();
    expect(screen.queryByText('All')).toBeFalsy();
    expect(screen.queryByText('Search')).toBeFalsy();
    expect(screen.queryByTestId('filterList')).toBeFalsy();
  });

  it('should hide new workspace when searching', async () => {
    const store = createMockStore();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await screen.findByText(getEmptyMapWorkspace(i18n.t).title);

    await act(() =>
      store.dispatch(
        workspaceListActions.searchFilter({
          searchQuery: 'testing',
        }),
      ),
    );
    expect(screen.queryByText(getEmptyMapWorkspace(i18n.t).title)).toBeFalsy();
  });

  it('should disable editing if the user is not logged in', async () => {
    const store = createMockStore();

    const authLoggedOutProps = {
      isLoggedIn: false, // user is not logged in
      auth: {
        username: 'user.name',
        token: '1223344',
        refresh_token: '33455214',
      },
      onLogin: (): void => null!,
      onSetAuth: (): void => null!,
      sessionStorageProvider: null!,
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        auth={authLoggedOutProps}
      >
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({ workspaceList }),
      ),
    );
    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();
    expect(screen.queryByTestId('workspace-listOptionsButton')).toBeFalsy();
  });

  it('should enable editing if the user is admin', async () => {
    const store = createMockStore();

    const authAdminProps = {
      isLoggedIn: true, // user is logged in
      auth: {
        username: 'user.name',
        token: '1223344',
        refresh_token: '33455214',
      },
      currentRole: GEOWEB_ROLE_PRESETS_ADMIN,
      onLogin: (): void => null!,
      onSetAuth: (): void => null!,
      sessionStorageProvider: null!,
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        auth={authAdminProps}
      >
        <WorkspaceSelectListConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({ workspaceList }),
      ),
    );
    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();
    expect(screen.getAllByTestId('workspace-listOptionsButton')).toBeTruthy();

    expect(screen.getByText('All')).toBeTruthy();
    expect(screen.queryByText('My presets')).toBeFalsy();
    expect(screen.getByText('System presets')).toBeTruthy();
  });
});
