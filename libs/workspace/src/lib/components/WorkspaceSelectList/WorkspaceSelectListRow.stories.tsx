/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Box } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { createMockStore } from '../../store/store';
import workspacePresetsList from '../../utils/fakeApi/workspacePresetsList.json';
import { WorkspacePresetListItem } from '../../store/workspace/types';
import WorkspacePresetsSelectListRow from './WorkspaceSelectListRow';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';

export default {
  title: 'components/Workspace/WorkspaceSelectList',
};

export const WorkspacePresetsSelectListRowLight = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={lightTheme}
    >
      <Box sx={{ width: '500px', height: '200px' }}>
        <Box sx={{ width: '320px' }}>
          <WorkspacePresetsSelectListRow
            workspacePreset={workspacePresetsList[8] as WorkspacePresetListItem}
            onClickWorkspacePreset={(): void => {}}
            onClickWorkspacePresetOption={(): void => {}}
            isOptionsMenuOpen
            isSelected
            isEditable
          />
        </Box>
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const WorkspacePresetsSelectListRowDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      store={createMockStore()}
      theme={darkTheme}
    >
      <Box sx={{ width: '500px', height: '200px' }}>
        <Box sx={{ width: '320px' }}>
          <WorkspacePresetsSelectListRow
            workspacePreset={workspacePresetsList[8] as WorkspacePresetListItem}
            onClickWorkspacePreset={(): void => {}}
            onClickWorkspacePresetOption={(): void => {}}
            isOptionsMenuOpen
            isSelected
            isEditable
          />
        </Box>
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

WorkspacePresetsSelectListRowLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6405b435ec5def65d04cef1c',
    },
  ],
};
WorkspacePresetsSelectListRowLight.tags = ['snapshot'];

WorkspacePresetsSelectListRowDark.tags = ['snapshot'];
