/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n, { TFunction } from 'i18next';
import { SHARED_NAMESPACE, sharedTranslations } from '@opengeoweb/shared';
import {
  WEBMAP_REACT_NAMESPACE,
  webmapReactTranslations,
} from '@opengeoweb/webmap-react';
import {
  UseTranslationResponse,
  initReactI18next,
  useTranslation,
} from 'react-i18next';

import { coreTranslations, CORE_NAMESPACE } from '@opengeoweb/core';
import {
  FORM_FIELDS_NAMESPACE,
  formFieldsTranslations,
} from '@opengeoweb/form-fields';
import { WARN_NAMESPACE, warningsTranslations } from '@opengeoweb/warnings';
import workspaceTranslations from '../../../locales/workspace.json';

export const WORKSPACE_NAMESPACE = 'workspace';

export const initWorkspaceTestI18n = (): void => {
  void i18n.use(initReactI18next).init({
    lng: 'en',
    ns: WORKSPACE_NAMESPACE,
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        [WORKSPACE_NAMESPACE]: workspaceTranslations.en,
        [SHARED_NAMESPACE]: sharedTranslations.en,
        [CORE_NAMESPACE]: coreTranslations.en,
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.en,
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.en,
        [WARN_NAMESPACE]: warningsTranslations.en,
      },
      fi: {
        [WORKSPACE_NAMESPACE]: workspaceTranslations.fi,
        [SHARED_NAMESPACE]: sharedTranslations.fi,
        [CORE_NAMESPACE]: coreTranslations.fi,
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.fi,
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.fi,
        [WARN_NAMESPACE]: warningsTranslations.fi,
      },
      no: {
        [WORKSPACE_NAMESPACE]: workspaceTranslations.no,
        [SHARED_NAMESPACE]: sharedTranslations.no,
        [CORE_NAMESPACE]: coreTranslations.no,
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.no,
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.no,
        [WARN_NAMESPACE]: warningsTranslations.no,
      },
      nl: {
        [WORKSPACE_NAMESPACE]: workspaceTranslations.nl,
        [SHARED_NAMESPACE]: sharedTranslations.nl,
        [CORE_NAMESPACE]: coreTranslations.nl,
        [WEBMAP_REACT_NAMESPACE]: webmapReactTranslations.nl,
        [FORM_FIELDS_NAMESPACE]: formFieldsTranslations.nl,
        [WARN_NAMESPACE]: warningsTranslations.nl,
      },
    },
  });
};

const ns = [WORKSPACE_NAMESPACE, SHARED_NAMESPACE];

export const translateKeyOutsideComponents = (
  t: TFunction,
  key: string,
  params: Record<string, string | number> | undefined = undefined,
): string => t(key, { ns, ...params });

export const useWorkspaceTranslation = (): UseTranslationResponse<
  typeof WORKSPACE_NAMESPACE,
  typeof i18n
> => useTranslation(ns);

export { i18n };
