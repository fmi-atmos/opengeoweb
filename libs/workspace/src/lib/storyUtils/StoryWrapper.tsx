/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { List, ListItemButton, ListItemText, Theme } from '@mui/material';
import { AppHeader, AppLayout } from '@opengeoweb/shared';
import { lightTheme } from '@opengeoweb/theme';
import { LayerManagerConnect, SyncGroupViewerConnect } from '@opengeoweb/core';
import { uiActions, uiTypes } from '@opengeoweb/store';

import React from 'react';
import { useDispatch } from 'react-redux';
import {
  GEOWEB_ROLE_PRESETS_ADMIN,
  GEOWEB_ROLE_USER,
} from '@opengeoweb/authentication';
import { WorkspaceTopBar } from '../components/WorkspaceTopBar';
import {
  WorkspaceWrapperProviderWithStore,
  defaultAuthConfig,
} from '../components/Providers';
import { WorkspaceDetail } from '../components/WorkspaceDetail';
import { componentsLookUp } from './componentsLookUp';
import { workspaceListActions } from '../store/workspaceList/reducer';

const Menu = (): React.ReactElement => {
  const dispatch = useDispatch();
  const openSyncgroupsDialog = React.useCallback(() => {
    dispatch(
      uiActions.setToggleOpenDialog({
        type: uiTypes.DialogTypes.SyncGroups,
        setOpen: true,
      }),
    );
  }, [dispatch]);

  return (
    <List>
      <ListItemButton onClick={openSyncgroupsDialog}>
        <ListItemText primary="Sync Groups" />
      </ListItemButton>
    </List>
  );
};

interface WorkspaceAppProps {
  isSnapshot?: boolean;
  workspaceId?: string;
  shouldShowListOnOpen?: boolean;
}

export const WorkspaceApp = ({
  isSnapshot = false,
  workspaceId,
  shouldShowListOnOpen = false,
}: WorkspaceAppProps): React.ReactElement => {
  const dispatch = useDispatch();
  React.useEffect(() => {
    if (shouldShowListOnOpen) {
      dispatch(
        workspaceListActions.toggleWorkspaceDialog({
          isWorkspaceListDialogOpen: true,
        }),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div style={{ overflow: 'hidden', height: 'calc(100vh)' }}>
      <AppLayout
        header={
          <AppHeader
            title="Header"
            workspaceTopBar={<WorkspaceTopBar workspaceId={workspaceId} />}
            menu={<Menu />}
          />
        }
      >
        <SyncGroupViewerConnect />
        <LayerManagerConnect bounds="parent" showMapIdInTitle />
        <WorkspaceDetail
          workspaceId={workspaceId}
          isSnapshot={isSnapshot}
          componentsLookUp={componentsLookUp}
        />
      </AppLayout>
    </div>
  );
};

interface StoryWrapperProps {
  theme?: Theme;
  isSnapshot?: boolean;
  createApi?: () => void;
  workspaceId?: string;
  isLoggedIn?: boolean;
  shouldShowListOnOpen?: boolean;
  isAdmin?: boolean;
}

export const StoryWrapper = ({
  theme = lightTheme,
  isSnapshot = false,
  createApi,
  workspaceId,
  isLoggedIn = true,
  shouldShowListOnOpen = false,
  isAdmin = false,
}: StoryWrapperProps): React.ReactElement => (
  <WorkspaceWrapperProviderWithStore
    theme={theme}
    createApi={createApi}
    auth={{
      ...defaultAuthConfig,
      isLoggedIn,
      currentRole: isAdmin ? GEOWEB_ROLE_PRESETS_ADMIN : GEOWEB_ROLE_USER,
    }}
  >
    <WorkspaceApp
      workspaceId={workspaceId}
      isSnapshot={isSnapshot}
      shouldShowListOnOpen={shouldShowListOnOpen}
    />
  </WorkspaceWrapperProviderWithStore>
);
