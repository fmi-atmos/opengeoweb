/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { PresetScope } from '@opengeoweb/shared';
import {
  WorkspacePresetAction,
  WorkspacePresetListItem,
} from '../workspace/types';
import {
  initialState,
  workspaceListActions,
  workspaceListReducer,
} from './reducer';
import {
  VerbatimWorkspaceListError,
  WorkspaceListError,
  WorkspaceListErrorCategory,
  WorkspaceListErrorType,
  WorkspaceListState,
} from './types';

describe('store/workspaceList/reducer', () => {
  describe('fetchWorkspaceList', () => {
    it('should set isFetching', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.fetchWorkspaceList({}),
      );

      expect(result.isFetching).toBeTruthy();
    });
  });

  describe('fetchedWorkspaceList', () => {
    it('should set workspaceList', () => {
      const workspaceList: WorkspacePresetListItem[] = [
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset',
          scope: 'system',
          title: 'Layer manager preset',
          viewType: 'multiWindow',
          abstract: '',
        },
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset_2',
          scope: 'system',
          title: 'Layer manager preset 2',
          viewType: 'multiWindow',
          abstract: '',
        },
      ];
      const result = workspaceListReducer(
        undefined,
        workspaceListActions.fetchedWorkspaceList({
          workspaceList,
        }),
      );

      expect(result).toEqual({
        ids: [workspaceList[0].id, workspaceList[1].id],
        entities: {
          [workspaceList[0].id]: workspaceList[0],
          [workspaceList[1].id]: workspaceList[1],
        },
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        searchQuery: '',
        error: undefined,
        workspaceListFilters: [
          {
            label: 'shared-filter-my-presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'shared-filter-system-presets',

            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
      });
    });

    it('should set isFetching', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.fetchedWorkspaceList({
          workspaceList: [],
        }),
      );

      expect(result.isFetching).toBeFalsy();
    });

    it('should return initialState when empty workspaceList passed', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        searchQuery: '',
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
        workspaceListFilters: [
          {
            label: 'shared-filter-my-presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'shared-filter-system-presets',

            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
      };
      const result = workspaceListReducer(
        state,
        workspaceListActions.fetchedWorkspaceList({
          workspaceList: [],
        }),
      );

      expect(result).toEqual(initialState);
    });
  });

  describe('openWorkspaceActionDialogOptions', () => {
    it('should set workspaceActionDialog to passed payload for DELETE action', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
      };
      const payload = {
        action: WorkspacePresetAction.DELETE,
        presetId: 'someId',
        formValues: { title: 'Workspace 1' },
      };
      const result = workspaceListReducer(
        state,
        workspaceListActions.openWorkspaceActionDialogOptions(payload),
      );

      expect(result.workspaceActionDialog).toEqual({
        ...payload,
        error: undefined,
        isFetching: false,
      });
    });

    it('should set workspaceActionDialog to passed payload for SAVE_AS action', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
      };
      const payload = {
        action: WorkspacePresetAction.DUPLICATE,
        presetId: 'someId',
        formValues: {
          title: 'Workspace 1',
          abstract: 'Here is an abstract',
          scope: 'user' as PresetScope,
        },
      };
      const result = workspaceListReducer(
        state,
        workspaceListActions.openWorkspaceActionDialogOptions(payload),
      );

      expect(result.workspaceActionDialog).toEqual({
        ...payload,
        error: undefined,
        isFetching: false,
      });
    });
  });

  describe('submitFormWorkspaceActionDialogOptions', () => {
    it('should reset error and start loading when submitting form', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
        workspaceActionDialog: {
          action: WorkspacePresetAction.DELETE,
          presetId: 'someId',
          formValues: { title: 'Workspace 1' },
          error: {
            category: WorkspaceListErrorCategory.GENERIC,
            message: 'test error',
            type: WorkspaceListErrorType.VERBATIM_ERROR,
          },
          isFetching: false,
        },
      };
      const payload = {
        presetAction: WorkspacePresetAction.DELETE,
        presetId: 'someId',
        data: {
          id: '',
          title: '',
          views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
          mosaicNode: 'testviewA',
          viewType: 'multiWindow' as const,
        },
        emptyMapPresetTitle: 'New map preset',
        emptyWorkspacePresetTitle: 'New workspace',
      };
      const result = workspaceListReducer(
        state,
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload),
      );

      expect(result.workspaceActionDialog!.error).toBeUndefined();
      expect(result.workspaceActionDialog!.isFetching).toBeTruthy();
    });
  });

  describe('closeWorkspaceActionDialogOptions', () => {
    it('should remove workspaceActionDialog', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        workspaceActionDialog: {
          action: WorkspacePresetAction.DELETE,
          presetId: 'someId',
          formValues: { title: 'Workspace 1' },
        },
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.closeWorkspaceActionDialogOptions(),
      );
      const { workspaceActionDialog, ...rest } = state;
      expect(result).not.toBe(rest);
    });

    it('should remove workspaceActionDialog even if not set', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.closeWorkspaceActionDialogOptions(),
      );

      expect(result).toBe(state);
    });
  });

  describe('onSuccessWorkspacePresetAction', () => {
    it('should remove workspaceActionDialog', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        workspaceActionDialog: {
          action: WorkspacePresetAction.DELETE,
          presetId: 'someId',
          formValues: { title: 'Workspace 1' },
        },
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.onSuccessWorkspacePresetAction(undefined!),
      );
      const { workspaceActionDialog, ...rest } = state;
      expect(result).toEqual(rest);
    });

    it('should remove workspaceActionDialog even if not set', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.onSuccessWorkspacePresetAction(undefined!),
      );

      expect(result).toBe(state);
    });
  });

  describe('onErrorWorkspacePresetAction', () => {
    it('should stop fetching and register error', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        workspaceActionDialog: {
          action: WorkspacePresetAction.DELETE,
          presetId: 'someId',
          formValues: { title: 'Workspace 1' },
          isFetching: true,
        },
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
      };
      const error: VerbatimWorkspaceListError = {
        category: WorkspaceListErrorCategory.GENERIC,
        message: 'test error message',
        type: WorkspaceListErrorType.VERBATIM_ERROR,
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.onErrorWorkspacePresetAction({ error }),
      );
      expect(result.workspaceActionDialog!.isFetching).toBeFalsy();
      expect(result.workspaceActionDialog!.error).toEqual(error);
    });
  });

  describe('toggleWorkspaceDialog', () => {
    it('should toggle isListDialogOpen for closed dialog ', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.toggleWorkspaceDialog({
          isWorkspaceListDialogOpen: true,
        }),
      );

      expect(result.isWorkspaceListDialogOpen).toBeTruthy();
    });
    it('should toggle isListDialogOpen for open dialog ', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: true,
        entities: {},
        ids: [],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.toggleWorkspaceDialog({
          isWorkspaceListDialogOpen: false,
        }),
      );

      expect(result.isWorkspaceListDialogOpen).toBeFalsy();
    });
  });

  describe('errorWorkspaceList', () => {
    it('should set isFetching and error', () => {
      const testError: WorkspaceListError = {
        message: 'test error',
        category: WorkspaceListErrorCategory.GENERIC,
        type: WorkspaceListErrorType.VERBATIM_ERROR,
      };
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.errorWorkspaceList({
          error: testError,
        }),
      );

      expect(result.isFetching).toBeFalsy();
      expect(result.error).toEqual(testError);
    });
  });

  describe('toggleSelectFilterChip', () => {
    it('should not do anything if id is not found', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.toggleSelectFilterChip({
          id: 'fakeid',
          isSelected: true,
        }),
      );

      expect(result.workspaceListFilters).toEqual(state.workspaceListFilters);
    });

    it('should set passed filter to true if passed and not touch the others', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.toggleSelectFilterChip({
          id: 'user',
          isSelected: true,
        }),
      );

      expect(result.workspaceListFilters![0].isSelected).toEqual(true);
      expect(result.workspaceListFilters![1].isSelected).toEqual(true);
    });

    it('should set passed filter to false if passed and not touch others', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets 2',
            id: 'system',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
        ],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: false,
        }),
      );

      expect(result.workspaceListFilters![0].isSelected).toEqual(true);
      expect(result.workspaceListFilters![1].isSelected).toEqual(false);
    });

    it('should set passed filter to false if passed and set other to true if all others are false as well', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: false,
        }),
      );

      expect(result.workspaceListFilters![0].isSelected).toEqual(true);
      expect(result.workspaceListFilters![1].isSelected).toEqual(true);
    });
  });

  describe('setSelectAllFilterChip', () => {
    it('should set all filters to selected true', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
        ],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.setSelectAllFilterChip(),
      );

      expect(result.workspaceListFilters![0].isSelected).toEqual(true);
      expect(result.workspaceListFilters![1].isSelected).toEqual(true);
    });
  });

  describe('setFilterChips', () => {
    it('should set a single filter to given value', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
        ],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.setFilterChips([{ id: 'user', isSelected: true }]),
      );

      expect(result.workspaceListFilters![0].isSelected).toEqual(true);
      expect(result.workspaceListFilters![1].isSelected).toEqual(false);
    });

    it('should set multiple filters to given value', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
        ],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.setFilterChips([
          { id: 'user', isSelected: true },
          { id: 'system', isSelected: true },
        ]),
      );

      expect(result.workspaceListFilters![0].isSelected).toEqual(true);
      expect(result.workspaceListFilters![1].isSelected).toEqual(true);
    });

    it('should not do anything if the filter can not be found', () => {
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: true,
        isWorkspaceListDialogOpen: false,
        entities: {},
        ids: [],
        workspaceListFilters: [
          {
            label: 'My presets',
            id: 'user',
            type: 'scope',
            isSelected: true,
            isDisabled: false,
          },
          {
            label: 'System presets',
            id: 'system',
            type: 'scope',
            isSelected: false,
            isDisabled: false,
          },
        ],
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.setFilterChips([
          { id: 'users', isSelected: true },
          { id: 'systems', isSelected: true },
        ]),
      );

      expect(result.workspaceListFilters![0].isSelected).toEqual(true);
      expect(result.workspaceListFilters![1].isSelected).toEqual(false);
    });
  });

  describe('searchFilter', () => {
    it('should search filter', () => {
      const testSearchQuery = 'testing';
      const state: WorkspaceListState = {
        error: undefined,
        isFetching: false,
        isWorkspaceListDialogOpen: false,
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
            viewType: 'multiWindow',
            abstract: '',
          },
        },
        ids: ['preset_1'],
        searchQuery: '',
      };

      const result = workspaceListReducer(
        state,
        workspaceListActions.searchFilter({ searchQuery: testSearchQuery }),
      );

      expect(result.searchQuery).toEqual(testSearchQuery);
    });
  });
});
