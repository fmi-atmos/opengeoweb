/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { PresetScope } from '@opengeoweb/shared';
import { snackbarActions } from '@opengeoweb/snackbar';
import { routerActions } from '@opengeoweb/store';
import { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';
import { waitFor } from '@testing-library/react';
import { workspaceListSelectors } from '.';
import { PresetsApi } from '../../utils/api';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { i18n } from '../../utils/i18n';
import { getWorkspaceRouteUrl } from '../../utils/routes';
import { createMockStore } from '../store';
import { WorkspaceModuleStore } from '../types';
import { ViewPresetEntity } from '../viewPresets/types';
import { workspaceActions } from '../workspace/reducer';
import * as workspaceSelectors from '../workspace/selectors';
import {
  WorkspacePresetAction,
  WorkspacePresetFromBE,
} from '../workspace/types';
import * as workSpaceUtils from '../workspace/utils';
import { emptyWorkspaceBEData } from './listener';
import { workspaceListActions } from './reducer';
import { getIsWorkspaceListFetching } from './selectors';
import {
  FetchWorkspaceParams,
  SubmitFormWorkspaceActionDialogPayload,
  WorkspaceDialogView,
  WorkspaceListErrorCategory,
  WorkspaceListErrorType,
  WorkspaceListFilter,
} from './types';
import * as workspaceListUtils from './utils';

const { constructFilterParams, getEmptyMapWorkspace, getSnackbarMessage } =
  workspaceListUtils;

const getMockStoreAndApi = (
  preloadedState?: WorkspaceModuleStore,
): [ToolkitStore, PresetsApi] => {
  const store = createMockStore(preloadedState);

  const mockApi = createFakeApi();
  jest
    .spyOn(workSpaceUtils, 'getWorkspaceApi')
    .mockImplementation(() => mockApi);

  return [store, mockApi];
};

describe('store/workspaceList/listener', () => {
  describe('fetchWorkspaceList listener', () => {
    it('should handle fetchWorkspaceList', async () => {
      // Test data
      const payload: FetchWorkspaceParams = {};

      const fakeResponse = {
        data: [
          {
            id: 'mapPreset-1',
            scope: 'system' as const,
            title: 'Layer manager preset',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
          {
            id: 'mapPreset-2',
            scope: 'system' as const,
            title: 'Layer manager preset',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
        ],
      };

      // Setup stuff
      const [store, mockApi] = getMockStoreAndApi();

      jest
        .spyOn(mockApi, 'getWorkspacePresets')
        .mockResolvedValue(fakeResponse);

      // Check
      expect(store.getState().workspaceList.entities).toEqual({});
      expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();

      // Do fetchWorkspaceList
      store.dispatch(workspaceListActions.fetchWorkspaceList(payload));
      expect(getIsWorkspaceListFetching(store.getState())).toBeTruthy();

      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      // Check state
      expect(store.getState().workspaceList.entities).toEqual({
        'mapPreset-1': fakeResponse.data[0],
        'mapPreset-2': fakeResponse.data[1],
      });
    });

    it('should handle fetchWorkspaceListener with filters', async () => {
      // Setup stuff
      const [store, mockApi] = getMockStoreAndApi();

      // Test data
      const payload: FetchWorkspaceParams = {
        scope: 'user',
      };

      const fakeResponse = {
        data: [
          {
            id: 'mapPreset-1',
            scope: 'system' as const,
            title: 'Layer manager preset',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
          {
            id: 'mapPreset-2',
            scope: 'system' as const,
            title: 'Layer manager preset',
            date: '2022-06-01T12:34:27.787192',
            viewType: 'singleWindow' as const,
            abstract: '',
          },
        ],
      };

      jest
        .spyOn(mockApi, 'getWorkspacePresets')
        .mockResolvedValue(fakeResponse);

      // Check
      expect(store.getState().workspaceList.entities).toEqual({});
      expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();

      // Do fetchWorkspaceList
      store.dispatch(workspaceListActions.fetchWorkspaceList(payload));
      expect(getIsWorkspaceListFetching(store.getState())).toBeTruthy();

      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      // Check state
      expect(store.getState().workspaceList.entities).toEqual({
        'mapPreset-1': fakeResponse.data[0],
        'mapPreset-2': fakeResponse.data[1],
      });
    });

    it('should handle errors', async () => {
      // Setup stuff
      const [store, mockApi] = getMockStoreAndApi();

      const payload: FetchWorkspaceParams = {};

      jest
        .spyOn(mockApi, 'getWorkspacePresets')
        .mockRejectedValueOnce('error fetching!');

      // Check
      expect(store.getState().workspaceList.entities).toEqual({});
      expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();

      // Do fetchWorkspaceList
      store.dispatch(workspaceListActions.fetchWorkspaceList(payload));
      expect(getIsWorkspaceListFetching(store.getState())).toBeTruthy();

      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      // Check state
      expect(store.getState().workspaceList.error).toEqual({
        key: 'workspace-list-error',
        category: WorkspaceListErrorCategory.GENERIC,
        type: WorkspaceListErrorType.TRANSLATABLE_ERROR,
      });
    });
  });

  const emptyMapPresetTitle = 'New map preset';
  const emptyWorkspacePresetTitle = 'New workspace';

  describe('onSuccessWorkspacePresetsActionListener', () => {
    it('should handle success for delete action (for non-selected workspace)', async () => {
      // Setup stuff
      const [store] = getMockStoreAndApi();

      const payload = {
        action: WorkspacePresetAction.DELETE,
        workspaceId: 'preset-1',
        title: 'test preset',
        currentActiveWorkspaceId: 'otherpreset',
        scope: 'user' as PresetScope,
        emptyWorkspacePresetTitle,
      };

      expect(store.getState().workspaceList.ids.length).toBeFalsy();

      expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();

      const openSnackBar = jest.spyOn(snackbarActions, 'openSnackbar');

      store.dispatch(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      expect(openSnackBar).toHaveBeenCalledWith(
        getSnackbarMessage(payload.action, payload.title),
      );

      expect(getIsWorkspaceListFetching(store.getState())).toBeTruthy();
      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      // workspaceList ids and entitites should be defined
      expect(store.getState().workspaceList.ids.length).toBeTruthy();
    });

    it('should select new preset after deleting selected workspace preset', () => {
      // Setup stuff
      const [store] = getMockStoreAndApi();

      const payload = {
        action: WorkspacePresetAction.DELETE,
        workspaceId: 'preset-1',
        title: 'test preset',
        currentActiveWorkspaceId: 'preset-1',
        scope: 'user' as PresetScope,
        emptyWorkspacePresetTitle,
      };

      const navigateToUrlSpy = jest.spyOn(routerActions, 'navigateToUrl');
      const openSnackBar = jest.spyOn(snackbarActions, 'openSnackbar');

      store.dispatch(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      expect(openSnackBar).toHaveBeenCalledWith(
        getSnackbarMessage(payload.action, payload.title),
      );
      expect(navigateToUrlSpy).toHaveBeenCalledWith({
        url: getWorkspaceRouteUrl(),
      });
    });

    it('should handle success for duplicate action', () => {
      // Setup stuff
      const [store] = getMockStoreAndApi();

      const payload = {
        action: WorkspacePresetAction.DUPLICATE,
        workspaceId: 'preset-1',
        title: 'test preset',
        scope: 'user' as PresetScope,
        emptyWorkspacePresetTitle,
      };

      const openSnackBar = jest.spyOn(snackbarActions, 'openSnackbar');

      store.dispatch(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      expect(openSnackBar).toHaveBeenCalledWith(
        getSnackbarMessage(payload.action, payload.title),
      );
    });

    it('should handle success for save as action', async () => {
      const payload = {
        action: WorkspacePresetAction.SAVE_AS,
        workspaceId: 'preset-1',
        title: 'test preset',
        scope: 'user' as PresetScope,
        emptyWorkspacePresetTitle,
      };
      const nav = jest.spyOn(routerActions, 'navigateToUrl');
      const openSnackBar = jest.spyOn(snackbarActions, 'openSnackbar');

      const [store] = getMockStoreAndApi();

      const spyFetchWorkspaceList = jest.spyOn(
        workspaceListActions,
        'fetchWorkspaceList',
      );

      store.dispatch(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      expect(openSnackBar).toHaveBeenCalledWith(
        getSnackbarMessage(payload.action, payload.title),
      );

      expect(spyFetchWorkspaceList).toHaveBeenCalledWith(
        constructFilterParams(
          workspaceListSelectors.getWorkspaceListFilters(store.getState()),
          workspaceListSelectors.getWorkspaceListSearchQuery(store.getState()),
        ),
      );

      expect(nav).toHaveBeenCalledWith({
        url: getWorkspaceRouteUrl(payload.workspaceId),
      });
    });

    it('should handle success for save action', async () => {
      const payload = {
        action: WorkspacePresetAction.SAVE,
        workspaceId: 'preset-1',
        title: 'test preset save',
        scope: 'user' as PresetScope,
        emptyWorkspacePresetTitle,
      };
      const openSnackBar = jest.spyOn(snackbarActions, 'openSnackbar');

      const [store] = getMockStoreAndApi();

      const spyFetchWorkspaceList = jest.spyOn(
        workspaceListActions,
        'fetchWorkspaceList',
      );

      store.dispatch(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      expect(openSnackBar).toHaveBeenCalledWith(
        getSnackbarMessage(payload.action, payload.title),
      );

      expect(spyFetchWorkspaceList).toHaveBeenCalledWith(
        constructFilterParams(
          workspaceListSelectors.getWorkspaceListFilters(store.getState()),
          workspaceListSelectors.getWorkspaceListSearchQuery(store.getState()),
        ),
      );
    });

    it('should handle success for save action and fetchWorkspaceList with current filter', async () => {
      const payload = {
        action: WorkspacePresetAction.SAVE,
        workspaceId: 'preset-1',
        title: 'test preset save',
        scope: 'user' as PresetScope,
        emptyWorkspacePresetTitle,
      };

      const openSnackBar = jest.spyOn(snackbarActions, 'openSnackbar');

      const [store] = getMockStoreAndApi();

      store.dispatch(
        workspaceListActions.onSuccessWorkspacePresetAction(payload),
      );

      expect(openSnackBar).toHaveBeenCalledWith(
        getSnackbarMessage(payload.action, payload.title),
      );
      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      expect(
        workspaceListSelectors.getWorkspaceListSearchQuery(store.getState()),
      ).toEqual('');

      const selectedFilters: WorkspaceListFilter[] = [
        {
          label: 'shared-filter-my-presets',
          id: 'user',
          type: 'scope',
          isSelected: true,
          isDisabled: false,
        },
        {
          label: 'shared-filter-system-presets',
          id: 'system',
          type: 'scope',
          isSelected: true,
          isDisabled: false,
        },
      ];

      expect(
        workspaceListSelectors.getWorkspaceListFilters(store.getState()),
      ).toEqual(selectedFilters);

      const searchQuery = 'test';

      store.dispatch(
        workspaceListActions.fetchWorkspaceList(
          constructFilterParams(selectedFilters, searchQuery),
        ),
      );

      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      expect(store.getState().workspaceList?.entities).toEqual({});
    });
  });

  describe('submitFormWorkspaceDialogListener', () => {
    it('should handle save as', async () => {
      const [store, mockApi] = getMockStoreAndApi();
      const workspaceState: WorkspacePresetFromBE = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
      };

      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.SAVE_AS,
        presetId: 'workspace-test-1',
        data: workspaceState,
        emptyMapPresetTitle,
        emptyWorkspacePresetTitle,
      };

      const newId = 'test-1';
      const saveWorkspacePresetAsSpy = jest
        .spyOn(mockApi, 'saveWorkspacePresetAs')
        .mockResolvedValueOnce(newId);

      const onSuccessWorkspacePresetActionSpy = jest.spyOn(
        workspaceListActions,
        'onSuccessWorkspacePresetAction',
      );

      await store.dispatch(
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload),
      );
      expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(workspaceState);

      await waitFor(() => {
        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
      });

      expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
        workspaceId: newId,
        action: payload.presetAction,
        scope: 'user',
        title: payload.data.title,
      });
    });

    it('should duplicate a existing workspace', async () => {
      const [store, mockApi] = getMockStoreAndApi();
      const workspaceState: WorkspacePresetFromBE = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
      };

      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.DUPLICATE,
        presetId: 'workspace-test-1',
        data: workspaceState,
        emptyMapPresetTitle,
        emptyWorkspacePresetTitle,
      };

      const newId = 'test-1';

      const getWorkspacePresetSpy = jest
        .spyOn(mockApi, 'getWorkspacePreset')
        .mockResolvedValueOnce({ data: payload.data as WorkspacePresetFromBE });

      const saveWorkspacePresetAsSpy = jest
        .spyOn(mockApi, 'saveWorkspacePresetAs')
        .mockResolvedValueOnce(newId);

      const onSuccessWorkspacePresetActionSpy = jest.spyOn(
        workspaceListActions,
        'onSuccessWorkspacePresetAction',
      );

      await store.dispatch(
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload),
      );

      expect(getWorkspacePresetSpy).toHaveBeenCalledWith(payload.presetId);

      await waitFor(() => {
        expect(saveWorkspacePresetAsSpy).toHaveBeenCalled();
      });

      expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(workspaceState);

      await waitFor(() => {
        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
      });

      expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
        workspaceId: newId,
        action: payload.presetAction,
        scope: 'user',
        title: payload.data.title,
      });
    });

    it('should duplicate a new workspace', async () => {
      const [store, mockApi] = getMockStoreAndApi();

      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.DUPLICATE,
        presetId: getEmptyMapWorkspace(i18n.t).id!,
        data: { ...emptyWorkspaceBEData, title: 'test title' },
        emptyMapPresetTitle,
        emptyWorkspacePresetTitle,
      };

      const newId = 'test-1';

      const saveWorkspacePresetAsSpy = jest
        .spyOn(mockApi, 'saveWorkspacePresetAs')
        .mockResolvedValueOnce(newId);

      const onSuccessWorkspacePresetActionSpy = jest.spyOn(
        workspaceListActions,
        'onSuccessWorkspacePresetAction',
      );

      await store.dispatch(
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload),
      );

      await waitFor(() => {
        expect(saveWorkspacePresetAsSpy).toHaveBeenCalled();
      });

      expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(payload.data);

      await waitFor(() => {
        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
      });

      expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
        workspaceId: newId,
        action: payload.presetAction,
        scope: 'system',
        title: payload.data.title,
      });
    });

    it('should handle delete', async () => {
      const [store, mockApi] = getMockStoreAndApi();
      const workspaceState: WorkspacePresetFromBE = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
      };
      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.DELETE,
        presetId: 'workspace-test-1',
        data: workspaceState,
        emptyMapPresetTitle,
        emptyWorkspacePresetTitle,
      };

      const deleteWorkspacePresetSpy = jest.spyOn(
        mockApi,
        'deleteWorkspacePreset',
      );

      const onSuccessWorkspacePresetActionSpy = jest.spyOn(
        workspaceListActions,
        'onSuccessWorkspacePresetAction',
      );

      await store.dispatch(
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload),
      );
      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      await waitFor(() => {
        expect(deleteWorkspacePresetSpy).toHaveBeenCalled();
      });

      await waitFor(() => {
        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
      });

      expect(deleteWorkspacePresetSpy).toHaveBeenCalledWith(payload.presetId);

      expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
        workspaceId: payload.presetId,
        action: payload.presetAction,
        scope: 'user',
        title: payload.data.title,
        currentActiveWorkspaceId: '',
        emptyWorkspacePresetTitle: payload.emptyWorkspacePresetTitle,
      });
    });

    it('should handle edit', async () => {
      const [store, mockApi] = getMockStoreAndApi();

      const workspaceState: WorkspacePresetFromBE = {
        title: 'updated title',
        abstract: 'new abstract',
        mosaicNode: 'mosaic-id',
        views: [],
      };
      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.EDIT,
        presetId: 'workspace-test-1',
        data: workspaceState,
        emptyMapPresetTitle,
        emptyWorkspacePresetTitle,
      };

      const onSuccessWorkspacePresetActionSpy = jest.spyOn(
        workspaceListActions,
        'onSuccessWorkspacePresetAction',
      );

      const saveWorkspacePresetSpy = jest
        .spyOn(mockApi, 'saveWorkspacePreset')
        .mockResolvedValueOnce();

      const oldWorkspace: WorkspacePresetFromBE = {
        title: 'Old title',
        abstract: 'Old abstract',
        mosaicNode: 'mosaic-id-old',
        views: [],
      };

      const dataToSendToBE: WorkspacePresetFromBE = {
        title: payload.data.title,
        abstract: payload.data.abstract,
        mosaicNode: oldWorkspace.mosaicNode,
        views: oldWorkspace.views,
      };
      jest
        .spyOn(mockApi, 'getWorkspacePreset')
        .mockResolvedValue({ data: oldWorkspace });

      await store.dispatch(
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload),
      );
      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      await waitFor(() => {
        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
      });

      expect(saveWorkspacePresetSpy).toHaveBeenCalledWith(
        payload.presetId,
        dataToSendToBE,
      );

      expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
        workspaceId: payload.presetId,
        action: payload.presetAction,
        scope: 'user',
        title: payload.data.title,
        abstract: payload.data.abstract,
      });
    });

    it('should handle reset', async () => {
      const [store] = getMockStoreAndApi();
      const workspaceState: WorkspacePresetFromBE = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
        abstract: '',
      };
      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.RESET,
        presetId: 'workspace-test-1',
        data: workspaceState,
        emptyMapPresetTitle,
        emptyWorkspacePresetTitle,
      };

      const fetchWorkspaceActionSpy = jest.spyOn(
        workspaceActions,
        'fetchWorkspace',
      );

      const onSuccessWorkspacePresetActionSpy = jest.spyOn(
        workspaceListActions,
        'onSuccessWorkspacePresetAction',
      );

      await store.dispatch(
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload),
      );
      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      await waitFor(() => {
        expect(fetchWorkspaceActionSpy).toHaveBeenCalled();
      });
      await waitFor(() => {
        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
      });

      expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
        workspaceId: payload.presetId,
        action: payload.presetAction,
        scope: 'user',
        title: payload.data.title,
        abstract: payload.data.abstract,
      });
    });

    it('should handle reset of "New workspace" preset', async () => {
      const [store] = getMockStoreAndApi();
      const workspaceState: WorkspacePresetFromBE = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
        abstract: '',
      };
      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.RESET,
        presetId: getEmptyMapWorkspace().id as string,
        data: workspaceState,
        emptyMapPresetTitle,
        emptyWorkspacePresetTitle,
      };

      const fetchWorkspaceActionSpy = jest.spyOn(
        workspaceActions,
        'fetchWorkspace',
      );

      const changeToEmptyMapWorkspaceActionSpy = jest.spyOn(
        workspaceActions,
        'changeToEmptyMapWorkspace',
      );

      const onSuccessWorkspacePresetActionSpy = jest.spyOn(
        workspaceListActions,
        'onSuccessWorkspacePresetAction',
      );

      await store.dispatch(
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload),
      );
      // Wait till api returns
      await waitFor(() => {
        expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
      });

      await waitFor(() => {
        expect(changeToEmptyMapWorkspaceActionSpy).toHaveBeenCalled();
      });

      await waitFor(() => {
        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
      });

      expect(fetchWorkspaceActionSpy).not.toHaveBeenCalled();

      expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
        workspaceId: payload.presetId,
        action: payload.presetAction,
        scope: 'user',
        title: payload.data.title,
        abstract: payload.data.abstract,
      });
    });

    it('should handle error', async () => {
      const [store] = getMockStoreAndApi();
      const workspaceState: WorkspacePresetFromBE = {
        title: 'test title',
        mosaicNode: 'mosaic-id',
        views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
      };

      const payload: SubmitFormWorkspaceActionDialogPayload = {
        presetAction: WorkspacePresetAction.SAVE_AS,
        presetId: 'workspace-test-1',
        data: workspaceState,
        emptyMapPresetTitle,
        emptyWorkspacePresetTitle,
      };

      jest
        .spyOn(workSpaceUtils, 'getWorkspaceApi')
        .mockResolvedValueOnce(undefined!);

      const onErrorWorkspacePresetActionSpy = jest.spyOn(
        workspaceListActions,
        'onErrorWorkspacePresetAction',
      );

      jest.spyOn(console, 'error').mockImplementationOnce(() => {});
      await store.dispatch(
        workspaceListActions.submitFormWorkspaceActionDialogOptions(payload),
      );

      await waitFor(() => {
        expect(onErrorWorkspacePresetActionSpy).toHaveBeenCalled();
      });

      expect(onErrorWorkspacePresetActionSpy).toHaveBeenCalledWith({
        error: {
          category: WorkspaceListErrorCategory.GENERIC,
          type: WorkspaceListErrorType.VERBATIM_ERROR,
          message: 'workspaceApi.saveWorkspacePresetAs is not a function',
        },
      });
    });

    describe('save system presets', () => {
      it('should handle save system presets for existing workspace presets', async () => {
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
          scope: 'system',
        };
        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_SYSTEM_PRESET,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const views: ViewPresetEntity[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          {
            panelId: '2_screen',
            activeViewPresetId: 'viewHarmonie',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        ];

        const saveWorkspacePresetSpy = jest
          .spyOn(mockApi, 'saveWorkspacePreset')
          .mockResolvedValueOnce([
            payload.presetId,
            payload.data,
          ] as unknown as Promise<void>);

        const onSuccessWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onSuccessWorkspacePresetAction',
        );

        const getViewPresetToSendToBackendSpy = jest.spyOn(
          workspaceSelectors,
          'getViewPresetToSendToBackend',
        );

        const navigateToUrlSpy = jest.spyOn(routerActions, 'navigateToUrl');

        store.dispatch(action);

        expect(saveWorkspacePresetSpy).toHaveBeenCalledWith(
          payload.presetId,
          payload.data,
        );

        await waitFor(() => {
          expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
        });
        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        expect(getViewPresetToSendToBackendSpy).toHaveBeenNthCalledWith(
          1,
          expect.anything(),
          views[0].panelId,
        );

        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
          action: WorkspacePresetAction.SAVE_SYSTEM_PRESET,
          workspaceId: payload.presetId,
          title: payload.data.title,
          abstract: payload.data.abstract,
          scope: 'system',
        });

        expect(navigateToUrlSpy).toHaveBeenCalledWith({
          url: getWorkspaceRouteUrl(payload.presetId),
        });
      });

      it('should handle errors in workspace', async () => {
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
        };

        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_SYSTEM_PRESET,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const saveWorkspacePresetSpy = jest
          .spyOn(mockApi, 'saveWorkspacePreset')
          .mockResolvedValueOnce(Promise.reject(new Error('nooo')));

        const getViewPresetsSpy = jest
          .spyOn(workspaceListSelectors, 'getWorkspaceDialogIsSaved')
          .mockReturnValueOnce(false);

        const onErrorWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        jest.spyOn(console, 'error').mockImplementationOnce(() => {});

        await store.dispatch(action);

        await waitFor(() => {
          expect(getViewPresetsSpy).toHaveBeenCalled();
        });

        expect(saveWorkspacePresetSpy).toHaveBeenCalledWith(
          payload.presetId,
          payload.data,
        );

        // Wait till api returns
        await waitFor(() => {
          expect(onErrorWorkspacePresetActionSpy).toHaveBeenCalled();
        });
      });

      it('should handle errors in saving view presets for system workspace', async () => {
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [
            {
              mosaicNodeId: '1_screen',
              viewPresetId: 'test-id',
              isSelected: true,
              hasChanges: true,
              title: 'testing',
            } as WorkspaceDialogView,
          ],
          scope: 'system',
        };
        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_SYSTEM_PRESET,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const views: ViewPresetEntity[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: true,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          {
            panelId: '2_screen',
            activeViewPresetId: 'viewHarmonie',
            hasChanges: true,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        ];

        const saveWorkspacePresetSpy = jest
          .spyOn(mockApi, 'saveWorkspacePreset')
          .mockResolvedValueOnce([
            payload.presetId,
            payload.data,
          ] as unknown as Promise<void>);

        jest.spyOn(mockApi, 'saveViewPreset').mockImplementation(() => {
          throw new Error('something went wrong');
        });

        const getViewPresetToSendToBackendSpy = jest.spyOn(
          workspaceSelectors,
          'getViewPresetToSendToBackend',
        );

        const errorResultSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        const onErrorWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        await store.dispatch(action);

        jest.spyOn(console, 'error').mockImplementationOnce(() => {});

        expect(saveWorkspacePresetSpy).toHaveBeenCalledWith(
          payload.presetId,
          payload.data,
        );

        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        expect(getViewPresetToSendToBackendSpy).toHaveBeenNthCalledWith(
          1,
          expect.anything(),
          views[0].panelId,
        );

        expect(errorResultSpy).toHaveBeenCalledWith({
          error: undefined,
        });

        expect(onErrorWorkspacePresetActionSpy).toHaveBeenCalledWith({
          error: {
            type: WorkspaceListErrorType.VERBATIM_ERROR,
            message: expect.anything(),
            category: WorkspaceListErrorCategory.GENERIC,
          },
        });
      });
    });

    describe('save system presets as', () => {
      it('should handle save system presets as for new workspace presets', async () => {
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
          scope: 'system',
        };
        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const views: ViewPresetEntity[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          {
            panelId: '2_screen',
            activeViewPresetId: 'viewHarmonie',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        ];

        const newWorkspaceId = 'workspace-id-1';

        const saveWorkspacePresetAsSpy = jest
          .spyOn(mockApi, 'saveWorkspacePresetAs')
          .mockResolvedValueOnce(newWorkspaceId);

        const onSuccessWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onSuccessWorkspacePresetAction',
        );

        const getViewPresetToSendToBackendSpy = jest.spyOn(
          workspaceSelectors,
          'getViewPresetToSendToBackend',
        );

        const navigateToUrlSpy = jest.spyOn(routerActions, 'navigateToUrl');

        await store.dispatch(action);

        expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(payload.data);

        await waitFor(() => {
          expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
        });
        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        expect(getViewPresetToSendToBackendSpy).toHaveBeenNthCalledWith(
          1,
          expect.anything(),
          views[0].panelId,
        );

        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
          action: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
          workspaceId: newWorkspaceId,
          title: payload.data.title,
          abstract: payload.data.abstract,
          scope: 'system',
        });

        expect(navigateToUrlSpy).toHaveBeenCalledWith({
          url: getWorkspaceRouteUrl(newWorkspaceId),
        });
      });

      it('should handle errors in workspace', async () => {
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
        };

        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const saveWorkspacePresetAsSpy = jest
          .spyOn(mockApi, 'saveWorkspacePresetAs')
          .mockResolvedValueOnce(Promise.reject(new Error('nooo')));

        const getViewPresetsSpy = jest
          .spyOn(workspaceListSelectors, 'getWorkspaceDialogIsSaved')
          .mockReturnValueOnce(false);

        const onErrorWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        jest.spyOn(console, 'error').mockImplementationOnce(() => {});

        await store.dispatch(action);

        await waitFor(() => {
          expect(getViewPresetsSpy).toHaveBeenCalled();
        });

        expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(payload.data);

        // Wait till api returns
        await waitFor(() => {
          expect(onErrorWorkspacePresetActionSpy).toHaveBeenCalled();
        });
      });

      it('should handle errors in saving view presets for system workspace', async () => {
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [
            {
              mosaicNodeId: '1_screen',
              viewPresetId: 'test-id',
              isSelected: true,
              hasChanges: true,
              title: 'testing',
            } as WorkspaceDialogView,
          ],
          scope: 'system',
        };
        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const views: ViewPresetEntity[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: true,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          {
            panelId: '2_screen',
            activeViewPresetId: 'viewHarmonie',
            hasChanges: true,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        ];

        const newWorkspaceId = 'workspace-id-1';

        const saveWorkspacePresetAsSpy = jest
          .spyOn(mockApi, 'saveWorkspacePresetAs')
          .mockResolvedValueOnce(newWorkspaceId);

        jest.spyOn(mockApi, 'saveViewPreset').mockImplementation(() => {
          throw new Error('something went wrong');
        });

        const getViewPresetToSendToBackendSpy = jest.spyOn(
          workspaceSelectors,
          'getViewPresetToSendToBackend',
        );

        const errorResultSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        const onErrorWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        await store.dispatch(action);

        jest.spyOn(console, 'error').mockImplementationOnce(() => {});

        expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(payload.data);

        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        expect(getViewPresetToSendToBackendSpy).toHaveBeenNthCalledWith(
          1,
          expect.anything(),
          views[0].panelId,
        );

        expect(errorResultSpy).toHaveBeenCalledWith({
          error: undefined,
        });

        expect(onErrorWorkspacePresetActionSpy).toHaveBeenCalledWith({
          error: {
            type: WorkspaceListErrorType.VERBATIM_ERROR,
            message: expect.anything(),
            category: WorkspaceListErrorCategory.GENERIC,
          },
        });
      });
    });

    describe('save user presets as', () => {
      it('should handle save presets as a user for new workspace presets', async () => {
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
          scope: 'user',
        };
        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_AS,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const views: ViewPresetEntity[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          {
            panelId: '2_screen',
            activeViewPresetId: 'viewHarmonie',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        ];

        const newWorkspaceId = 'workspace-id-1';

        const saveWorkspacePresetAsSpy = jest
          .spyOn(mockApi, 'saveWorkspacePresetAs')
          .mockResolvedValueOnce(newWorkspaceId);

        const onSuccessWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onSuccessWorkspacePresetAction',
        );

        const getViewPresetToSendToBackendSpy = jest.spyOn(
          workspaceSelectors,
          'getViewPresetToSendToBackend',
        );

        const navigateToUrlSpy = jest.spyOn(routerActions, 'navigateToUrl');

        await store.dispatch(action);

        expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(payload.data);

        await waitFor(() => {
          expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
        });
        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        expect(getViewPresetToSendToBackendSpy).toHaveBeenNthCalledWith(
          1,
          expect.anything(),
          views[0].panelId,
        );

        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
          action: WorkspacePresetAction.SAVE_AS,
          workspaceId: newWorkspaceId,
          title: payload.data.title,
          abstract: payload.data.abstract,
          scope: 'user',
        });

        expect(navigateToUrlSpy).toHaveBeenCalledWith({
          url: getWorkspaceRouteUrl(newWorkspaceId),
        });
      });

      it('should handle save system presets as a user presets', async () => {
        const testViews = [
          {
            mosaicNodeId: 'system-1_screen',
            viewPresetId: 'system-id',
            isSelected: true,
            hasChanges: true,
            title: 'this is a system preset',
            scope: 'system' as const,
            componentType: 'Map',
          },
          {
            mosaicNodeId: 'user-1_screen',
            viewPresetId: 'user-test-id',
            isSelected: true,
            hasChanges: true,
            title: 'this is a user preset',
            scope: 'user' as const,
            componentType: 'Map',
          },
          {
            mosaicNodeId: 'user-1_screen',
            viewPresetId: 'user-test-id',
            isSelected: false,
            hasChanges: false,
            title: 'this is a user preset',
            scope: 'user' as const,
            componentType: 'Map',
          },
        ];

        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: testViews,
          scope: 'user',
        };
        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_AS,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const [store, mockApi] = getMockStoreAndApi({
          workspace: {
            ...workspaceState,
            views: {
              byId: {
                [testViews[0].mosaicNodeId]: testViews[0],
                [testViews[1].mosaicNodeId]: testViews[1],
                [testViews[1].mosaicNodeId]: testViews[2],
              },
              allIds: [
                testViews[0].mosaicNodeId,
                testViews[1].mosaicNodeId,
                testViews[2].mosaicNodeId,
              ],
            },
          },
        });

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const newWorkspaceId = 'workspace-id-1';

        const saveWorkspacePresetAsSpy = jest
          .spyOn(mockApi, 'saveWorkspacePresetAs')
          .mockResolvedValueOnce(newWorkspaceId);

        const onSuccessWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onSuccessWorkspacePresetAction',
        );

        const getViewPresetToSendToBackendSpy = jest.spyOn(
          workspaceSelectors,
          'getViewPresetToSendToBackend',
        );

        const saveViewPresetToSendToBackendSpy = jest.spyOn(
          mockApi,
          'saveViewPreset',
        );

        const saveViewPresetAsToSendToBackendSpy = jest.spyOn(
          mockApi,
          'saveViewPresetAs',
        );

        const navigateToUrlSpy = jest.spyOn(routerActions, 'navigateToUrl');

        await store.dispatch(action);

        expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(payload.data);

        await waitFor(() => {
          expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
        });
        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        expect(getViewPresetToSendToBackendSpy).toHaveBeenNthCalledWith(
          1,
          expect.anything(),
          testViews[0].mosaicNodeId,
        );

        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
          action: WorkspacePresetAction.SAVE_AS,
          workspaceId: newWorkspaceId,
          title: payload.data.title,
          abstract: payload.data.abstract,
          scope: 'user',
        });

        expect(saveViewPresetAsToSendToBackendSpy).toHaveBeenCalledWith(
          expect.objectContaining({
            title: testViews[0].title,
            scope: 'user',
          }),
        );

        expect(saveViewPresetAsToSendToBackendSpy).toHaveBeenCalledTimes(1);

        expect(saveViewPresetToSendToBackendSpy).toHaveBeenCalledWith(
          testViews[1].viewPresetId,
          expect.objectContaining({
            title: testViews[1].title,
            scope: 'user',
          }),
        );
        expect(saveViewPresetToSendToBackendSpy).toHaveBeenCalledTimes(1);

        expect(navigateToUrlSpy).toHaveBeenCalledWith({
          url: getWorkspaceRouteUrl(newWorkspaceId),
        });
      });

      it('should handle errors in workspace', async () => {
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
          scope: 'user',
        };

        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_AS,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const saveWorkspacePresetAsSpy = jest
          .spyOn(mockApi, 'saveWorkspacePresetAs')
          .mockResolvedValueOnce(Promise.reject(new Error('nooo')));

        const getViewPresetsSpy = jest
          .spyOn(workspaceListSelectors, 'getWorkspaceDialogIsSaved')
          .mockReturnValueOnce(false);

        const onErrorWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        jest.spyOn(console, 'error').mockImplementationOnce(() => {});

        await store.dispatch(action);

        await waitFor(() => {
          expect(getViewPresetsSpy).toHaveBeenCalled();
        });

        expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(payload.data);

        // Wait till api returns
        await waitFor(() => {
          expect(onErrorWorkspacePresetActionSpy).toHaveBeenCalled();
        });
      });

      it('should handle errors in saving view presets for normal users', async () => {
        // jest.resetAllMocks();
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [
            {
              mosaicNodeId: '1_screen',
              viewPresetId: 'test-id',
              isSelected: true,
              hasChanges: true,
              title: 'testing',
            } as WorkspaceDialogView,
          ],
          scope: 'user',
        };
        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE_AS,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const views: ViewPresetEntity[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: true,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          {
            panelId: '2_screen',
            activeViewPresetId: 'viewHarmonie',
            hasChanges: true,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        ];

        const newWorkspaceId = 'workspace-id-1';

        const saveWorkspacePresetAsSpy = jest
          .spyOn(mockApi, 'saveWorkspacePresetAs')
          .mockResolvedValueOnce(newWorkspaceId);

        jest.spyOn(mockApi, 'saveViewPresetAs').mockImplementation(() => {
          throw new Error('something went wrong');
        });

        const getViewPresetToSendToBackendSpy = jest.spyOn(
          workspaceSelectors,
          'getViewPresetToSendToBackend',
        );

        const errorResultSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        const onErrorWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        await store.dispatch(action);

        jest.spyOn(console, 'error').mockImplementationOnce(() => {});

        expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(payload.data);

        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        expect(getViewPresetToSendToBackendSpy).toHaveBeenNthCalledWith(
          1,
          expect.anything(),
          views[0].panelId,
        );

        expect(errorResultSpy).toHaveBeenCalledWith({
          error: undefined,
        });

        expect(onErrorWorkspacePresetActionSpy).toHaveBeenCalledWith({
          error: {
            type: WorkspaceListErrorType.VERBATIM_ERROR,
            message: expect.anything(),
            category: WorkspaceListErrorCategory.GENERIC,
          },
        });
      });
    });

    describe('save user presets', () => {
      it('should handle save presets a user for workspace presets', async () => {
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
          scope: 'user',
        };
        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const views: ViewPresetEntity[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          {
            panelId: '2_screen',
            activeViewPresetId: 'viewHarmonie',
            hasChanges: false,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        ];

        const saveWorkspacePresetSpy = jest.spyOn(
          mockApi,
          'saveWorkspacePreset',
        );

        const onSuccessWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onSuccessWorkspacePresetAction',
        );

        const getViewPresetToSendToBackendSpy = jest.spyOn(
          workspaceSelectors,
          'getViewPresetToSendToBackend',
        );

        const navigateToUrlSpy = jest.spyOn(routerActions, 'navigateToUrl');

        await store.dispatch(action);

        expect(saveWorkspacePresetSpy).toHaveBeenCalledWith(
          payload.presetId,
          payload.data,
        );

        await waitFor(() => {
          expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
        });
        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        expect(getViewPresetToSendToBackendSpy).toHaveBeenNthCalledWith(
          1,
          expect.anything(),
          views[0].panelId,
        );

        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
          action: WorkspacePresetAction.SAVE,
          workspaceId: payload.presetId,
          title: payload.data.title,
          abstract: payload.data.abstract,
          scope: 'user',
        });

        expect(navigateToUrlSpy).toHaveBeenCalledWith({
          url: getWorkspaceRouteUrl(payload.presetId),
        });
      });

      it('should handle save system presets as a user presets', async () => {
        const testViews = [
          {
            mosaicNodeId: 'system-1_screen',
            viewPresetId: 'system-id',
            isSelected: true,
            hasChanges: true,
            title: 'this is a system preset',
            scope: 'system' as const,
            componentType: 'Map',
          },
          {
            mosaicNodeId: 'user-1_screen',
            viewPresetId: 'user-test-id',
            isSelected: true,
            hasChanges: true,
            title: 'this is a user preset',
            scope: 'user' as const,
            componentType: 'Map',
            abstract: 'some test abstract',
          },
          {
            mosaicNodeId: 'user-1_screen',
            viewPresetId: 'user-test-id',
            isSelected: false,
            hasChanges: false,
            title: 'this is a user preset',
            scope: 'user' as const,
            componentType: 'Map',
          },
        ];

        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: testViews,
          scope: 'user',
        };
        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const [store, mockApi] = getMockStoreAndApi({
          workspace: {
            ...workspaceState,
            views: {
              byId: {
                [testViews[0].mosaicNodeId]: testViews[0],
                [testViews[1].mosaicNodeId]: testViews[1],
                [testViews[1].mosaicNodeId]: testViews[2],
              },
              allIds: [
                testViews[0].mosaicNodeId,
                testViews[1].mosaicNodeId,
                testViews[2].mosaicNodeId,
              ],
            },
          },
        });

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const saveWorkspacePresetSpy = jest.spyOn(
          mockApi,
          'saveWorkspacePreset',
        );

        const onSuccessWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onSuccessWorkspacePresetAction',
        );

        const getViewPresetToSendToBackendSpy = jest.spyOn(
          workspaceSelectors,
          'getViewPresetToSendToBackend',
        );

        const saveViewPresetToSendToBackendSpy = jest.spyOn(
          mockApi,
          'saveViewPreset',
        );

        const saveViewPresetAsToSendToBackendSpy = jest.spyOn(
          mockApi,
          'saveViewPresetAs',
        );

        const navigateToUrlSpy = jest.spyOn(routerActions, 'navigateToUrl');

        await store.dispatch(action);

        expect(saveWorkspacePresetSpy).toHaveBeenCalledWith(
          payload.presetId,
          payload.data,
        );

        await waitFor(() => {
          expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalled();
        });
        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        expect(getViewPresetToSendToBackendSpy).toHaveBeenNthCalledWith(
          1,
          expect.anything(),
          testViews[0].mosaicNodeId,
        );

        expect(onSuccessWorkspacePresetActionSpy).toHaveBeenCalledWith({
          action: WorkspacePresetAction.SAVE,
          workspaceId: payload.presetId,
          title: payload.data.title,
          abstract: payload.data.abstract,
          scope: 'user',
        });

        expect(saveViewPresetAsToSendToBackendSpy).toHaveBeenCalledWith(
          expect.objectContaining({
            title: testViews[0].title,
            scope: 'user',
          }),
        );

        expect(saveViewPresetAsToSendToBackendSpy).not.toHaveBeenCalledWith(
          expect.objectContaining({
            abstract: expect.anything(),
          }),
        );
        expect(saveViewPresetAsToSendToBackendSpy).toHaveBeenCalledTimes(1);

        expect(saveViewPresetToSendToBackendSpy).toHaveBeenCalledWith(
          testViews[1].viewPresetId,
          expect.objectContaining({
            title: testViews[1].title,
            scope: 'user',
            abstract: testViews[1].abstract,
          }),
        );

        expect(saveViewPresetToSendToBackendSpy).toHaveBeenCalledTimes(1);

        expect(navigateToUrlSpy).toHaveBeenCalledWith({
          url: getWorkspaceRouteUrl(payload.presetId),
        });
      });

      it('should handle errors in workspace', async () => {
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [{ mosaicNodeId: '1_screen', viewPresetId: 'test-id' }],
          scope: 'user',
        };

        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const saveWorkspacePresetSpy = jest
          .spyOn(mockApi, 'saveWorkspacePreset')
          .mockResolvedValueOnce(Promise.reject(new Error('nooo')));

        const getViewPresetsSpy = jest
          .spyOn(workspaceListSelectors, 'getWorkspaceDialogIsSaved')
          .mockReturnValueOnce(false);

        const onErrorWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        jest.spyOn(console, 'error').mockImplementationOnce(() => {});

        await store.dispatch(action);

        await waitFor(() => {
          expect(getViewPresetsSpy).toHaveBeenCalled();
        });

        expect(saveWorkspacePresetSpy).toHaveBeenCalledWith(
          payload.presetId,
          payload.data,
        );

        // Wait till api returns
        await waitFor(() => {
          expect(onErrorWorkspacePresetActionSpy).toHaveBeenCalled();
        });
      });

      it('should handle errors in saving view presets for normal users', async () => {
        // jest.resetAllMocks();
        const [store, mockApi] = getMockStoreAndApi();
        const workspaceState: WorkspacePresetFromBE = {
          title: 'test title',
          mosaicNode: 'mosaic-id',
          abstract: 'some abstract',
          views: [
            {
              mosaicNodeId: '1_screen',
              viewPresetId: 'test-id',
              isSelected: true,
              hasChanges: true,
              title: 'testing',
            } as WorkspaceDialogView,
          ],
          scope: 'user',
        };
        const payload: SubmitFormWorkspaceActionDialogPayload = {
          presetAction: WorkspacePresetAction.SAVE,
          presetId: 'workspace-test-1',
          data: workspaceState,
          emptyMapPresetTitle: 'New map preset',
          emptyWorkspacePresetTitle,
        };

        const action =
          workspaceListActions.submitFormWorkspaceActionDialogOptions(payload);

        const views: ViewPresetEntity[] = [
          {
            panelId: '1_screen',
            activeViewPresetId: 'radar',
            hasChanges: true,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          {
            panelId: '2_screen',
            activeViewPresetId: 'viewHarmonie',
            hasChanges: true,
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: undefined,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        ];

        const saveWorkspacePresetAsSpy = jest
          .spyOn(mockApi, 'saveWorkspacePreset')
          .mockResolvedValueOnce(payload.data as unknown as void);

        jest.spyOn(mockApi, 'saveViewPresetAs').mockImplementation(() => {
          throw new Error('something went wrong');
        });

        const getViewPresetToSendToBackendSpy = jest.spyOn(
          workspaceSelectors,
          'getViewPresetToSendToBackend',
        );

        const errorResultSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        const onErrorWorkspacePresetActionSpy = jest.spyOn(
          workspaceListActions,
          'onErrorWorkspacePresetAction',
        );

        await store.dispatch(action);

        jest.spyOn(console, 'error').mockImplementationOnce(() => {});

        expect(saveWorkspacePresetAsSpy).toHaveBeenCalledWith(
          payload.presetId,
          payload.data,
        );

        // Wait till api returns
        await waitFor(() => {
          expect(getIsWorkspaceListFetching(store.getState())).toBeFalsy();
        });

        expect(getViewPresetToSendToBackendSpy).toHaveBeenNthCalledWith(
          1,
          expect.anything(),
          views[0].panelId,
        );

        expect(errorResultSpy).toHaveBeenCalledWith({
          error: undefined,
        });

        expect(onErrorWorkspacePresetActionSpy).toHaveBeenCalledWith({
          error: {
            type: WorkspaceListErrorType.VERBATIM_ERROR,
            message: expect.anything(),
            category: WorkspaceListErrorCategory.GENERIC,
          },
        });
      });
    });
  });
});
