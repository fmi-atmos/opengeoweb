/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { PresetScope } from '@opengeoweb/shared';
import { viewPresetActions } from '../viewPresets';
import { viewPresetsListReducer, initialState } from './reducer';
import { ViewPresetListItem, ViewPresetListState } from './types';

describe('store/viewPresetsList/reducer', () => {
  describe('fetchedMapPresets', () => {
    it('should set viewPresets if no filter/search params present', () => {
      const viewPresets: ViewPresetListItem[] = [
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset',
          scope: 'system',
          title: 'Layer manager preset',
        },
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset_2',
          scope: 'system',
          title: 'Layer manager preset 2',
        },
      ];
      const result = viewPresetsListReducer(
        undefined,
        viewPresetActions.fetchedViewPresets({
          viewPresets,
          panelId: undefined!,
          filterParams: {},
        }),
      );

      expect(result).toEqual({
        ids: [viewPresets[0].id, viewPresets[1].id],
        entities: {
          [viewPresets[0].id]: viewPresets[0],
          [viewPresets[1].id]: viewPresets[1],
        },
      });
    });

    it('should do nothing if filter/search params present and store is undefined', () => {
      const viewPresets: ViewPresetListItem[] = [
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset',
          scope: 'system',
          title: 'Layer manager preset',
        },
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset_2',
          scope: 'system',
          title: 'Layer manager preset 2',
        },
      ];
      const result = viewPresetsListReducer(
        undefined,
        viewPresetActions.fetchedViewPresets({
          viewPresets,
          panelId: undefined!,
          filterParams: { scope: 'user' },
        }),
      );

      expect(result).toEqual({
        ids: [],
        entities: {},
      });
    });

    it('should do nothing if filter/search params present and leave whatever is already there', () => {
      const state: ViewPresetListState = {
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset_1',
            scope: 'system' as PresetScope,
            title: 'Layer manager preset',
          },
        },
        ids: ['preset_1'],
      };
      const result1 = viewPresetsListReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          viewPresets: null!,
          panelId: undefined!,
          filterParams: { scope: 'user' },
        }),
      );
      expect(result1).toEqual(state);
      const result2 = viewPresetsListReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          viewPresets: [],
          panelId: undefined!,
          filterParams: { scope: 'system' },
        }),
      );
      expect(result2).toEqual(state);
    });

    it('should return initialState when empty viewPresets passed', () => {
      const state: ViewPresetListState = {
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset_1',
            scope: 'system' as PresetScope,
            title: 'Layer manager preset',
          },
        },
        ids: ['preset_1'],
      };
      const result1 = viewPresetsListReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          viewPresets: null!,
          panelId: undefined!,
          filterParams: {},
        }),
      );
      expect(result1).toEqual(initialState);
      const result2 = viewPresetsListReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          viewPresets: [],
          panelId: undefined!,
          filterParams: {},
        }),
      );
      expect(result2).toEqual(initialState);
    });
  });
});
