/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { PresetAction } from './types';
import { getSnackbarMessage } from './utils';

describe('store/mapPresets/utils/getSnackbarMessage', () => {
  it('should return the correct message', async () => {
    expect(getSnackbarMessage(PresetAction.DELETE, 'PRESETTITLE').key).toEqual(
      'workspace-viewpreset-success-delete',
    );
    expect(getSnackbarMessage(PresetAction.SAVE, 'PRESETTITLE').key).toEqual(
      'workspace-viewpreset-success-save',
    );
    expect(
      getSnackbarMessage(PresetAction.SAVE_AS, ' PRESETTITLE ').key,
    ).toEqual('workspace-viewpreset-success-save-as');
    expect(getSnackbarMessage(PresetAction.EDIT, ' PRESETTITLE ').key).toEqual(
      'workspace-viewpreset-success-edit',
    );

    expect(
      getSnackbarMessage(PresetAction.EDIT, ' PRESETTITLE ').params,
    ).toEqual({ TITLE: 'PRESETTITLE' });
  });
});
