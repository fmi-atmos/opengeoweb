/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  layerActions,
  layerTypes,
  mapActions,
  syncGroupsActions,
  uiActions,
  mapEnums,
  genericActions,
} from '@opengeoweb/store';
import { Action } from '@reduxjs/toolkit';
import { LayerType } from '@opengeoweb/webmap';
import { Parameter, timeSeriesActions } from '@opengeoweb/timeseries';
import { publicWarningActions } from '@opengeoweb/warnings';
import {
  initialState,
  viewPresetsReducer,
  viewPresetActions,
  supportedTimeSeriesActionOrigins,
} from './reducer';
import {
  ViewPresetError,
  ViewPresetState,
  ViewPresetErrorComponent,
  ViewPreset,
  ViewPresetDialog,
  PresetAction,
  ViewPresetErrorType,
} from './types';
import { ViewPresetListItem } from '../viewPresetsList/types';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';
import { workspaceListActions } from '../workspaceList/reducer';
import { WorkspacePresetAction } from '../workspace/types';

describe('store/viewPresets/reducer', () => {
  it('should return initialState', () => {
    expect(viewPresetsReducer(undefined, {} as Action)).toEqual(initialState);
  });

  describe('fetchMapPresets', () => {
    it('should set isFetchingMapPresets on fetchMapPresets', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchViewPresets({
          panelId,
          filterParams: {},
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeTruthy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeFalsy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should reset isFetchingMapPresets and errorMapPresets on fetchMapPresets', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: {
              message: 'error fetching BE',
              component: ViewPresetErrorComponent.PRESET_LIST,
              errorType: ViewPresetErrorType.GENERIC,
            },
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchViewPresets({
          panelId,
          filterParams: {},
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeTruthy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeFalsy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if map can not be found', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError: ViewPresetError = {
        message: 'error fetching BE',
        component: ViewPresetErrorComponent.PRESET_LIST,
        errorType: ViewPresetErrorType.GENERIC,
      };
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: testError,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchViewPresets({
          panelId: 'map-3',
          filterParams: { scope: '' },
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toEqual(testError);
      expect(result.entities[panelId2]!.isFetching).toBeFalsy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('fetchedMapPresets', () => {
    it('should set isFetchingMapPresets to false and do nothing else if filterParams empty', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: [],
          filterParams: {},
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should set isFetchingMapPresets to false and set filterResults if filterParams non empty', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const viewPresets: ViewPresetListItem[] = [
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset',
          scope: 'system',
          title: 'Layer manager preset',
        },
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset_2',
          scope: 'system',
          title: 'Layer manager preset 2',
        },
      ];

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets,
          filterParams: { scope: 'system' },
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
      expect(result.entities[panelId]!.filterResults).toStrictEqual({
        ids: viewPresets.map((viewpreset) => viewpreset.id),
        entities: {
          preset: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset',
            scope: 'system',
            title: 'Layer manager preset',
          },
          preset_2: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset_2',
            scope: 'system',
            title: 'Layer manager preset 2',
          },
        },
      });
      expect(result.entities[panelId2]!.filterResults).toStrictEqual({
        ids: [],
        entities: {},
      });
    });

    it('should set isFetchingMapPresets to false and clear out filter results if these are present but no filters set anymore', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: {
              ids: ['preset', 'preset_2'],
              entities: {
                preset: {
                  date: '2022-06-01T12:34:27.787192',
                  id: 'preset',
                  scope: 'system',
                  title: 'Layer manager preset',
                },
                preset_2: {
                  date: '2022-06-01T12:34:27.787192',
                  id: 'preset_2',
                  scope: 'system',
                  title: 'Layer manager preset 2',
                },
              },
            },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const viewPresets: ViewPresetListItem[] = [
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset',
          scope: 'system',
          title: 'Layer manager preset',
        },
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset_2',
          scope: 'system',
          title: 'Layer manager preset 2',
        },
      ];

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets,
          filterParams: {},
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
      expect(result.entities[panelId]!.filterResults).toStrictEqual({
        ids: [],
        entities: {},
      });
      expect(result.entities[panelId2]!.filterResults).toStrictEqual({
        ids: [],
        entities: {},
      });
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPresets({
          panelId: 'map-3',
          viewPresets: [],
          filterParams: {},
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('fetchedMapPreset', () => {
    it('should set isFetchingMapPresets to false and reset hasChanges and set active preset id', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newPresetTestId = 'test-1';

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPreset({
          panelId,
          viewPresetId: newPresetTestId,
          viewPreset: {} as ViewPreset,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId]!.activeViewPresetId).toEqual(
        newPresetTestId,
      );
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.fetchedViewPreset({
          panelId: 'map-3',
          viewPresetId: '',
          viewPreset: {} as ViewPreset,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('saveMapPreset', () => {
    it('should set fetching true and clear error', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: {
              component: ViewPresetErrorComponent.PRESET_DETAIL,
              message: 'test error',
              errorType: ViewPresetErrorType.GENERIC,
            },
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newPresetTestId = 'test-1';

      const result = viewPresetsReducer(
        state,
        viewPresetActions.saveViewPreset({
          panelId,
          viewPresetId: newPresetTestId,
          viewPreset: {} as ViewPreset,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeTruthy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.saveViewPreset({
          panelId: 'map-3',
          viewPresetId: '',
          viewPreset: {} as ViewPreset,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('savedMapPreset', () => {
    it('should set fetching false and reset changes', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: {
              component: ViewPresetErrorComponent.PRESET_DETAIL,
              message: 'test error',
              errorType: ViewPresetErrorType.GENERIC,
            },
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newPresetTestId = 'test-1';

      const result = viewPresetsReducer(
        state,
        viewPresetActions.savedViewPreset({
          panelId,
          viewPresetId: newPresetTestId,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId]!.error).toEqual(
        state.entities[panelId]?.error,
      );
      expect(result.entities[panelId]!.activeViewPresetId).toEqual(
        newPresetTestId,
      );
      expect(result.entities[panelId2]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.savedViewPreset({
          panelId: 'map-3',
          viewPresetId: '',
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('errorMapPreset', () => {
    it('should set error and isFetchingMapPresets false', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError: ViewPresetError = {
        message: 'something went wrong fetching view presets',
        component: ViewPresetErrorComponent.PRESET_LIST,
        errorType: ViewPresetErrorType.GENERIC,
      };

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.errorViewPreset({
          panelId,
          error: testError,
          viewPresetId: 'test01',
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toEqual(testError);
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError: ViewPresetError = {
        message: 'something went wrong fetching view presets',
        component: ViewPresetErrorComponent.PRESET_LIST,
        errorType: ViewPresetErrorType.GENERIC,
      };

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.errorViewPreset({
          panelId: 'map-3',
          error: testError,
          viewPresetId: 'test01',
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeTruthy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('setMapPresetHasChanges', () => {
    it('should set changes to map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setViewPresetHasChanges({
          panelId,
          hasChanges: false,
        }),
      );

      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });

    it('should set changes to map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setViewPresetHasChanges({
          panelId,
          hasChanges: false,
        }),
      );

      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setViewPresetHasChanges({
          panelId: 'map-3',
          hasChanges: false,
        }),
      );

      expect(result.entities[panelId]!.hasChanges).toBeTruthy();
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });
  });

  describe('setActiveMapPresetId', () => {
    it('should set active map preset id and reset hasChanges', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newId = 'test-new';
      const result = viewPresetsReducer(
        state,
        viewPresetActions.setActiveViewPresetId({
          panelId,
          viewPresetId: newId,
        }),
      );

      expect(result.entities[panelId]!.activeViewPresetId).toEqual(newId);
      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId2]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });

    it('should do not nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newId = 'test-new';
      const result = viewPresetsReducer(
        state,
        viewPresetActions.setActiveViewPresetId({
          panelId: 'map-3',
          viewPresetId: newId,
        }),
      );

      expect(result.entities[panelId]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId]!.hasChanges).toBeTruthy();
      expect(result.entities[panelId2]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });
  });

  describe('selectMapPreset', () => {
    it('should select a new map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: false,
            activeViewPresetId: 'test1',
            isFetching: false,
            isViewPresetListDialogOpen: true,
            error: {
              component: ViewPresetErrorComponent.PRESET_DETAIL,
              message: 'test error',
              errorType: ViewPresetErrorType.GENERIC,
            },
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newId = '';
      const result = viewPresetsReducer(
        state,
        viewPresetActions.selectViewPreset({
          panelId,
          viewPresetId: newId,
        }),
      );

      expect(result.entities[panelId]!.activeViewPresetId).toEqual(newId);
      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId]!.isViewPresetListDialogOpen).toBeFalsy();

      expect(result.entities[panelId2]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
      expect(
        result.entities[panelId2]!.isViewPresetListDialogOpen,
      ).toBeTruthy();
    });

    it('should do nothing with selecting an existing map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: false,
            activeViewPresetId: '',
            isFetching: false,
            isViewPresetListDialogOpen: false,
            error: {
              component: ViewPresetErrorComponent.PRESET_DETAIL,
              message: 'test error',
              errorType: ViewPresetErrorType.GENERIC,
            },
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newId = 'test-1';
      const result = viewPresetsReducer(
        state,
        viewPresetActions.selectViewPreset({
          panelId,
          viewPresetId: newId,
        }),
      );
      const expectedError: ViewPresetError = {
        component: ViewPresetErrorComponent.PRESET_DETAIL,
        message: 'test error',
        errorType: ViewPresetErrorType.GENERIC,
      };

      expect(result.entities[panelId]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId]!.hasChanges).toBeFalsy();
      expect(result.entities[panelId]!.error).toEqual(expectedError);

      expect(result.entities[panelId2]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });

    it('should do nothing if panelId can not be found in the store', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const newId = 'test-new';
      const result = viewPresetsReducer(
        state,
        viewPresetActions.selectViewPreset({
          panelId: 'map-3',
          viewPresetId: newId,
        }),
      );

      expect(result.entities[panelId]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId]!.hasChanges).toBeTruthy();
      expect(result.entities[panelId2]!.activeViewPresetId).toEqual('');
      expect(result.entities[panelId2]!.hasChanges).toBeTruthy();
    });
  });

  describe('registerViewPreset', () => {
    it('should register new entity when map is registered', () => {
      const panelId = 'test-1';

      const result = viewPresetsReducer(
        undefined,
        viewPresetActions.registerViewPreset({
          panelId,
          viewPresetId: '',
        }),
      );

      expect(result).toEqual({
        ids: [panelId],
        entities: {
          [panelId]: {
            panelId,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            hasChanges: false,
            filters: [
              {
                label: 'shared-filter-my-presets',

                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'shared-filter-system-presets',

                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
      } as ViewPresetState);
    });

    it('should register new entity when map is registered with activeViewPresetId', () => {
      const panelId = 'test-1';
      const mapPresetId = 'test-mappreset-1';
      const result = viewPresetsReducer(
        undefined,
        viewPresetActions.registerViewPreset({
          panelId,
          viewPresetId: mapPresetId,
        }),
      );

      expect(result).toEqual({
        ids: [panelId],
        entities: {
          [panelId]: {
            panelId,
            activeViewPresetId: mapPresetId,
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            hasChanges: false,
            filters: [
              {
                label: 'shared-filter-my-presets',

                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'shared-filter-system-presets',

                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
      } as ViewPresetState);
    });
  });

  describe('unregisterMapPreset', () => {
    it('should unregister entity when map is unregistered', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.unregisterViewPreset({
          panelId,
        }),
      );

      expect(result).toEqual({
        entities: {
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId2],
      } as ViewPresetState);
    });
  });
  describe('mapPresetChangeActions', () => {
    it('should detect changes in the syncgroup targets', () => {
      const mapId = 'test-1';
      const mapId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [mapId]: {
            panelId: mapId,
            hasChanges: false,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [mapId2]: {
            panelId: mapId2,
            hasChanges: false,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId, mapId2],
      };
      // If origin is system, don't do anything
      const result = viewPresetsReducer(
        state,
        syncGroupsActions.syncGroupAddTarget({
          groupId: '1',
          targetId: mapId,
          origin: 'system',
        }),
      );
      expect(result.entities[mapId]!.hasChanges).toBeFalsy();

      const result1 = viewPresetsReducer(
        state,
        syncGroupsActions.syncGroupRemoveTarget({
          groupId: '1',
          targetId: mapId,
          origin: 'user',
        }),
      );
      expect(result1.entities[mapId]!.hasChanges).toBeTruthy();

      const result2 = viewPresetsReducer(
        state,
        syncGroupsActions.syncGroupAddTarget({
          groupId: '1',
          targetId: mapId2,
          origin: 'user',
        }),
      );
      expect(result2.entities[mapId2]!.hasChanges).toBeTruthy();
    });

    it('should detect changes from mapPresetChangeActions', () => {
      const mapId = 'test-1';
      const state: ViewPresetState = {
        entities: {
          [mapId]: {
            panelId: mapId,
            hasChanges: false,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId],
      };

      // layerActions.setBaseLayers
      expect(
        viewPresetsReducer(
          state,
          layerActions.setBaseLayers({
            mapId,
            origin: layerTypes.LayerActionOrigin.layerManager,
            layers: [],
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.setBaseLayers({
            mapId,
            origin: layerTypes.LayerActionOrigin.toggleAutoUpdateListener,
            layers: [],
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerDelete
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerDelete({
            mapId,
            origin: layerTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            layerIndex: 0,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerDelete({
            mapId,
            origin: layerTypes.LayerActionOrigin.toggleAutoUpdateListener,
            layerId: 'test',
            layerIndex: 0,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.addLayer
      expect(
        viewPresetsReducer(
          state,
          layerActions.addLayer({
            mapId,
            origin: layerTypes.LayerActionOrigin.layerManager,
            layer: { id: 'test' },
            layerId: 'test',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.addLayer({
            mapId,
            origin: layerTypes.LayerActionOrigin.toggleAutoUpdateListener,
            layer: { id: 'test' },
            layerId: 'test',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerChangeEnabled
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeEnabled({
            mapId,
            origin: layerTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            enabled: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeEnabled({
            mapId,
            origin: layerTypes.LayerActionOrigin.toggleAutoUpdateListener,
            layerId: 'test',
            enabled: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerChangeName
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeName({
            mapId,
            origin: layerTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            name: 'new name',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeName({
            mapId,
            origin: layerTypes.LayerActionOrigin.toggleAutoUpdateListener,
            layerId: 'test',
            name: 'new name',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerChangeStyle
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeStyle({
            mapId,
            origin: layerTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            style: 'red',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeStyle({
            mapId,
            origin: layerTypes.LayerActionOrigin.toggleAutoUpdateListener,
            layerId: 'test',
            style: 'red',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerChangeOpacity
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeOpacity({
            mapId,
            origin: layerTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            opacity: 0.3,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeOpacity({
            mapId,
            origin: layerTypes.LayerActionOrigin.toggleAutoUpdateListener,
            layerId: 'test',
            opacity: 0.3,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // layerActions.layerChangeDimension
      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeDimension({
            mapId,
            origin: layerTypes.LayerActionOrigin.layerManager,
            layerId: 'test',
            dimension: { currentValue: 'test' },
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          layerActions.layerChangeDimension({
            mapId,
            origin: layerTypes.LayerActionOrigin.toggleAutoUpdateListener,
            layerId: 'test',
            dimension: { currentValue: 'test' },
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.layerChangeDimension
      expect(
        viewPresetsReducer(
          state,
          mapActions.layerMoveLayer({
            mapId,
            origin: layerTypes.LayerActionOrigin.layerManager,
            newIndex: 0,
            oldIndex: 1,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.layerMoveLayer({
            mapId,
            origin: layerTypes.LayerActionOrigin.toggleAutoUpdateListener,
            newIndex: 0,
            oldIndex: 1,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.setAutoLayerId
      expect(
        viewPresetsReducer(
          state,
          mapActions.setAutoLayerId({
            mapId,
            origin: layerTypes.LayerActionOrigin.layerManager,
            autoTimeStepLayerId: 'test-1',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.setAutoLayerId({
            mapId,
            origin: layerTypes.LayerActionOrigin.toggleAutoUpdateListener,
            autoTimeStepLayerId: 'test-1',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.toggleAutoUpdate
      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleAutoUpdate({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            shouldAutoUpdate: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleAutoUpdate({
            mapId,
            shouldAutoUpdate: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.mapStartAnimation
      expect(
        viewPresetsReducer(
          state,
          mapActions.mapStartAnimation({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            start: 'test',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.mapStartAnimation({
            mapId,
            start: 'test',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.mapStopAnimation
      expect(
        viewPresetsReducer(
          state,
          mapActions.mapStopAnimation({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.mapStopAnimation({
            mapId,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.toggleTimeSliderIsVisible
      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleTimeSliderIsVisible({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            isTimeSliderVisible: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleTimeSliderIsVisible({
            mapId,
            isTimeSliderVisible: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.setTimeStep
      expect(
        viewPresetsReducer(
          state,
          mapActions.setTimeStep({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            timeStep: 1,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.setTimeStep({
            mapId,
            timeStep: 1,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.setTimeSliderSpan
      expect(
        viewPresetsReducer(
          state,
          mapActions.setTimeSliderSpan({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            timeSliderSpan: 1234,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.setTimeSliderSpan({
            mapId,
            timeSliderSpan: 1234,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.setAnimationDelay
      expect(
        viewPresetsReducer(
          state,
          mapActions.setAnimationDelay({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            animationDelay: 100,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.setAnimationDelay({
            mapId,
            animationDelay: 100,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.toggleTimestepAuto
      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleTimestepAuto({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            timestepAuto: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleTimestepAuto({
            mapId,
            timestepAuto: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // mapActions.toggleTimeSpanAuto
      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleTimeSpanAuto({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            timeSpanAuto: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          mapActions.toggleTimeSpanAuto({
            mapId,
            timeSpanAuto: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // uiActions.setActiveMapIdForDialog
      expect(
        viewPresetsReducer(
          state,
          uiActions.setActiveMapIdForDialog({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            type: ViewPresetErrorComponent.PRESET_DETAIL,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          uiActions.setActiveMapIdForDialog({
            mapId,
            type: ViewPresetErrorComponent.PRESET_DETAIL,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // uiActions.setToggleOpenDialog
      expect(
        viewPresetsReducer(
          state,
          uiActions.setToggleOpenDialog({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            setOpen: true,
            type: 'layerManager',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          uiActions.setToggleOpenDialog({
            mapId,
            setOpen: true,
            type: 'layerManager',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      // syncGroupActions.setToggleOpenDialog
      expect(
        viewPresetsReducer(
          state,
          genericActions.setBbox({
            mapId,
            origin: mapEnums.MapActionOrigin.map,
            bbox: { left: 999, right: 999, bottom: 999, top: 999 },
            sourceId: '',
            srs: '',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeTruthy();

      expect(
        viewPresetsReducer(
          state,
          genericActions.setBbox({
            mapId,
            bbox: { left: 999, right: 999, bottom: 999, top: 999 },
            sourceId: '',
            srs: '',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();
    });
  });

  describe('timeSeriesPresetChangeActions', () => {
    const mapId = 'test-1';
    const state: ViewPresetState = {
      entities: {
        [mapId]: {
          panelId: mapId,
          hasChanges: false,
          activeViewPresetId: '',
          isFetching: false,
          error: undefined,
          isViewPresetListDialogOpen: false,
          filters: [],
          searchQuery: '',
          filterResults: { ids: [], entities: {} },
        },
      },
      ids: [mapId],
    };

    const plotId = 'test-plot-1';
    const parameter: Parameter = {
      plotId,
      propertyName: 'test-property-1',
      plotType: 'bar',
      serviceId: 'test-service-1',
      collectionId: 'test-collection-1',
    };

    interface ActionReturnType {
      payload: unknown;
      type: string;
    }
    const supportedActions: ((
      panelId: string,
      origin: string,
    ) => ActionReturnType)[] = [
      (panelId, origin): ActionReturnType =>
        timeSeriesActions.addParameter({
          panelId,
          origin,
          parameter,
        }),
      (panelId, origin): ActionReturnType =>
        timeSeriesActions.addPlot({
          panelId,
          origin,
          title: 'Test Plot Title',
        }),
      (panelId, origin): ActionReturnType =>
        timeSeriesActions.deleteParameter({
          panelId,
          origin,
          parameterId: 'test-parameter-1',
        }),
      (panelId, origin): ActionReturnType =>
        timeSeriesActions.deletePlot({
          panelId,
          origin,
          plotId,
        }),
      (panelId, origin): ActionReturnType =>
        timeSeriesActions.moveParameter({
          panelId,
          origin,
          plotId,
          toPlotId: 'test-plot-2',
          oldIndex: 0,
          newIndex: 1,
        }),
      (panelId, origin): ActionReturnType =>
        timeSeriesActions.movePlot({
          panelId,
          origin,
          oldIndex: 0,
          newIndex: 1,
        }),
      (panelId, origin): ActionReturnType =>
        timeSeriesActions.toggleParameter({
          panelId,
          origin,
          parameterId: 'test-parameter-1',
        }),
      (panelId, origin): ActionReturnType =>
        timeSeriesActions.togglePlot({
          panelId,
          origin,
          plotId,
        }),
      (panelId, origin): ActionReturnType =>
        timeSeriesActions.patchParameter({
          panelId,
          origin,
          parameter: {
            id: 'test-parameter-1',
            propertyName: 'patched-test-property-1',
          },
        }),
      (panelId, origin): ActionReturnType =>
        timeSeriesActions.updateTitle({
          panelId,
          origin,
          plotId,
          title: 'Updated Test Plot Title',
        }),
    ];

    supportedActions.forEach((action) => {
      supportedTimeSeriesActionOrigins.forEach((origin) => {
        const actionResult = action(mapId, origin);
        it(`should detect changes on action ${actionResult.type} with mapId '${mapId}' and origin ´'${origin}'`, () => {
          expect(
            viewPresetsReducer(state, actionResult).entities[mapId]!.hasChanges,
          ).toBeTruthy();
        });
      });
      supportedTimeSeriesActionOrigins.forEach((origin) => {
        const actionResult = action('otherMapId', origin);
        it(`should not detect changes on action ${actionResult.type} with mapId 'otherMapId' and origin ´'${origin}'`, () => {
          expect(
            viewPresetsReducer(state, actionResult).entities[mapId]!.hasChanges,
          ).toBeFalsy();
        });
      });
      const actionResult = action(mapId, 'unsupportedOrigin');
      it(`should not detect changes on action ${actionResult.type} with mapId '${mapId}' and unkown origin`, () => {
        expect(
          viewPresetsReducer(state, actionResult).entities[mapId]!.hasChanges,
        ).toBeFalsy();
      });
    });

    it('should not detect changes on an irrelevant parameter property change', () => {
      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.patchParameter({
            panelId: mapId,
            origin: supportedTimeSeriesActionOrigins[0],
            parameter: {
              id: 'test-parameter-1',
              instanceId: '2000-01-01T00:00:00Z',
            },
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();
    });

    it('should not detect changes on an unrelated action', () => {
      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.registerTimeSeriesPreset({}),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.setSearchFilter({ filterText: 'test filter' }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.setServiceFilterChipSelected({
            serviceId: 'test-service-1',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.setAllServiceFilterChipSelected(),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.setOnlyThisServiceFilterChipSelected({
            serviceId: 'test-service-1',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.setServiceFilterChipUnselected({
            serviceId: 'test-service-1',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.removeServiceFilterChipFromStore({
            id: 'test-service-1',
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.setCurrentParameterInfoDisplayed({ parameter }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.addServices({ timeSeriesServices: [] }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.upsertService({
            id: 'test-service-1',
            description: 'test description',
            url: 'https://test-url.invalid',
            type: 'EDR',
            name: 'test name',
            isUpdate: false,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.removeService({ id: 'test-service-1' }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(state, timeSeriesActions.closeServicePopup())
          .entities[mapId]!.hasChanges,
      ).toBeFalsy();

      expect(
        viewPresetsReducer(
          state,
          timeSeriesActions.setServicePopup({
            variant: 'edit',
            isOpen: true,
          }),
        ).entities[mapId]!.hasChanges,
      ).toBeFalsy();
    });
  });

  describe('openViewPresetDialog', () => {
    it('should open a delete dialog', () => {
      const panelId = 'test-1';
      const testViewPresetDialog: ViewPresetDialog = {
        title: translateKeyOutsideComponents(
          i18n.t,
          'workspace-mappreset-dialog-title-delete',
        ),
        action: PresetAction.DELETE,
        panelId,
        viewPresetId: 'view-1',
        formValues: {
          title: '',
        },
      };

      const result = viewPresetsReducer(
        undefined,
        viewPresetActions.openViewPresetDialog({
          viewPresetDialog: testViewPresetDialog,
        }),
      );

      expect(result.viewPresetDialog).toEqual(testViewPresetDialog);
    });

    it('should open a save as dialog', () => {
      const panelId = 'test-1';
      const testViewPresetDialog: ViewPresetDialog = {
        title: translateKeyOutsideComponents(
          i18n.t,
          'workspace-mappreset-save-as',
        ),
        action: PresetAction.SAVE_AS,
        panelId,
        viewPresetId: 'view-1',
        formValues: {
          title: 'my view preset',
          initialProps: {
            mapPreset: {
              layers: [
                {
                  name: 'WorldMap_Light_Grey_Canvas',
                  type: 'twms',
                  dimensions: [],
                  id: 'layerid_46',
                  opacity: 1,
                  enabled: true,
                  layerType: LayerType.baseLayer,
                },
                {
                  service:
                    'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
                  name: 'countryborders',
                  format: 'image/png',
                  dimensions: [],
                  id: 'layerid_55',
                  opacity: 1,
                  enabled: true,
                  layerType: LayerType.overLayer,
                },
              ],
              activeLayerId: 'layerid_53',
              autoTimeStepLayerId: 'layerid_53',
              autoUpdateLayerId: 'layerid_53',
              proj: {
                bbox: {
                  left: -450651.2255879827,
                  bottom: 6490531.093143953,
                  right: 1428345.8183648037,
                  top: 7438773.776232235,
                },
                srs: 'EPSG:3857',
              },
              shouldAnimate: false,
              shouldAutoUpdate: false,
              showTimeSlider: true,
              displayMapPin: false,
              shouldShowZoomControls: true,
              animationPayload: {
                interval: 5,
                speed: 4,
              },
              toggleTimestepAuto: true,
              shouldShowLegend: false,
            },
            syncGroupsIds: [],
          },
        },
      };

      const result = viewPresetsReducer(
        undefined,
        viewPresetActions.openViewPresetDialog({
          viewPresetDialog: testViewPresetDialog,
        }),
      );

      expect(result.viewPresetDialog).toEqual(testViewPresetDialog);
    });
  });

  describe('closeViewPresetDialog', () => {
    it('should close current dialog', () => {
      const panelId = 'test-1';
      const testViewPresetDialog: ViewPresetDialog = {
        title: translateKeyOutsideComponents(
          i18n.t,
          'workspace-mappreset-save-as',
        ),
        action: PresetAction.SAVE_AS,
        panelId,
        viewPresetId: 'view-1',
        formValues: {
          title: 'my view preset',
          initialProps: {
            mapPreset: {
              layers: [
                {
                  name: 'WorldMap_Light_Grey_Canvas',
                  type: 'twms',
                  dimensions: [],
                  id: 'layerid_46',
                  opacity: 1,
                  enabled: true,
                  layerType: LayerType.baseLayer,
                },
                {
                  service:
                    'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
                  name: 'countryborders',
                  format: 'image/png',
                  dimensions: [],
                  id: 'layerid_55',
                  opacity: 1,
                  enabled: true,
                  layerType: LayerType.overLayer,
                },
              ],
              activeLayerId: 'layerid_53',
              autoTimeStepLayerId: 'layerid_53',
              autoUpdateLayerId: 'layerid_53',
              proj: {
                bbox: {
                  left: -450651.2255879827,
                  bottom: 6490531.093143953,
                  right: 1428345.8183648037,
                  top: 7438773.776232235,
                },
                srs: 'EPSG:3857',
              },
              shouldAnimate: false,
              shouldAutoUpdate: false,
              showTimeSlider: true,
              displayMapPin: false,
              shouldShowZoomControls: true,
              animationPayload: {
                interval: 5,
                speed: 4,
              },
              toggleTimestepAuto: true,
              shouldShowLegend: false,
            },
            syncGroupsIds: [],
          },
        },
      };
      const state: ViewPresetState = {
        entities: {},
        ids: [],
        viewPresetDialog: testViewPresetDialog,
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.closeViewPresetDialog(),
      );

      expect(result.viewPresetDialog).toBeUndefined();
    });
  });

  describe('toggleViewPresetListDialog', () => {
    it('should open viewpreset list dialog', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleViewPresetListDialog({
          isViewPresetListDialogOpen: true,
          panelId,
        }),
      );

      expect(result.entities[panelId]!.isViewPresetListDialogOpen).toBeTruthy();
      expect(result.entities[panelId2]!.isViewPresetListDialogOpen).toBeFalsy();
    });

    it('should close viewpreset list dialog', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleViewPresetListDialog({
          isViewPresetListDialogOpen: false,
          panelId,
        }),
      );

      expect(result.entities[panelId]!.isViewPresetListDialogOpen).toBeFalsy();
      expect(
        result.entities[panelId2]!.isViewPresetListDialogOpen,
      ).toBeTruthy();
    });

    it('should do nothing when viewpreset can not be found', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: true,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleViewPresetListDialog({
          isViewPresetListDialogOpen: false,
          panelId: 'test-3',
        }),
      );

      expect(result.entities[panelId]!.isViewPresetListDialogOpen).toBeTruthy();
      expect(
        result.entities[panelId2]!.isViewPresetListDialogOpen,
      ).toBeTruthy();
    });
  });

  describe('toggleSelectFilterChip', () => {
    it('should not do anything if id is not found', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleSelectFilterChip({
          id: 'fakeid',
          isSelected: true,
          panelId: 'panel1234566',
        }),
      );

      expect(result).toEqual(state);
    });

    it('should set passed filter to true if passed and not touch the others', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleSelectFilterChip({
          id: 'user',
          isSelected: true,
          panelId,
        }),
      );

      // expect(result.entities[panelId]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId]?.filters[1].isSelected).toEqual(false);
      expect(result.entities[panelId2]?.filters[0].isSelected).toEqual(false);
      expect(result.entities[panelId2]?.filters[1].isSelected).toEqual(true);
    });

    it('should set passed filter to false if passed and not touch others', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets 2',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: false,
          panelId,
        }),
      );

      expect(result.entities[panelId]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId]?.filters[1].isSelected).toEqual(false);
      expect(result.entities[panelId2]?.filters[0].isSelected).toEqual(false);
      expect(result.entities[panelId2]?.filters[1].isSelected).toEqual(true);
    });

    it('should set passed filter to true if passed and set other to true if all others are false as well', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: false,
          panelId,
        }),
      );

      expect(result.entities[panelId]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId]?.filters[1].isSelected).toEqual(true);
      expect(result.entities[panelId2]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId2]?.filters[1].isSelected).toEqual(false);
    });

    it('should set passed filter to true if passed and set other to false if they are true', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.toggleSelectFilterChip({
          id: 'system',
          isSelected: true,
          panelId,
        }),
      );

      expect(result.entities[panelId]?.filters[0].isSelected).toEqual(false);
      expect(result.entities[panelId]?.filters[1].isSelected).toEqual(true);
      expect(result.entities[panelId2]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId2]?.filters[1].isSelected).toEqual(true);
    });
  });

  describe('setSelectAllFilterChip', () => {
    it('should set all filters to selected true', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: true,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setSelectAllFilterChip({ panelId }),
      );

      expect(result.entities[panelId]?.filters[0].isSelected).toEqual(true);
      expect(result.entities[panelId]?.filters[1].isSelected).toEqual(true);
    });
  });

  describe('setFilterChips', () => {
    it('should set a single filter to given value', () => {
      const state: ViewPresetState = {
        entities: {
          panel1: {
            panelId: 'panel1',
            error: undefined,
            isFetching: false,
            activeViewPresetId: '1',
            hasChanges: false,
            isViewPresetListDialogOpen: true,
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
          },
        },
        ids: ['panel1'],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setFilterChips({
          filters: [{ id: 'user', isSelected: true }],
          panelId: 'panel1',
        }),
      );

      expect(result.entities.panel1!.filters[0].isSelected).toEqual(true);
      expect(result.entities.panel1!.filters[1].isSelected).toEqual(false);
    });

    it('should set multiple filters to given value', () => {
      const state: ViewPresetState = {
        entities: {
          panel1: {
            panelId: 'panel1',
            error: undefined,
            isFetching: false,
            activeViewPresetId: '1',
            hasChanges: false,
            isViewPresetListDialogOpen: true,
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
          },
        },
        ids: ['panel1'],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setFilterChips({
          filters: [
            { id: 'user', isSelected: true },
            { id: 'system', isSelected: true },
          ],
          panelId: 'panel1',
        }),
      );

      expect(result.entities.panel1!.filters[0].isSelected).toEqual(true);
      expect(result.entities.panel1!.filters[1].isSelected).toEqual(true);
    });

    it('should not do anything if the filter can not be found', () => {
      const state: ViewPresetState = {
        entities: {
          panel1: {
            panelId: 'panel1',
            error: undefined,
            isFetching: false,
            activeViewPresetId: '1',
            hasChanges: false,
            isViewPresetListDialogOpen: true,
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
          },
        },
        ids: ['panel1'],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setFilterChips({
          filters: [
            { id: 'users', isSelected: true },
            { id: 'systems', isSelected: true },
          ],
          panelId: 'panel1',
        }),
      );

      expect(result.entities.panel1!.filters[0].isSelected).toEqual(false);
      expect(result.entities.panel1!.filters[1].isSelected).toEqual(false);
    });
    it('should not do anything if the panelId can not be found', () => {
      const state: ViewPresetState = {
        entities: {
          panel1: {
            panelId: 'panel1',
            error: undefined,
            isFetching: false,
            activeViewPresetId: '1',
            hasChanges: false,
            isViewPresetListDialogOpen: true,
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
            filters: [
              {
                label: 'My presets',
                id: 'user',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
              {
                label: 'System presets',
                id: 'system',
                type: 'scope',
                isSelected: false,
                isDisabled: false,
              },
            ],
          },
        },
        ids: ['panel1'],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.setFilterChips({
          filters: [
            { id: 'user', isSelected: true },
            { id: 'system', isSelected: true },
          ],
          panelId: 'panel2',
        }),
      );

      expect(result.entities.panel1!.filters[0].isSelected).toEqual(false);
      expect(result.entities.panel1!.filters[1].isSelected).toEqual(false);
    });
  });

  describe('searchFilter', () => {
    it('should do nothing if passed panel cannot be found', () => {
      const testSearchQuery = 'testing';
      const panelId = 'test-1';
      const state: ViewPresetState = {
        entities: {},
        ids: [],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.searchFilter({
          searchQuery: testSearchQuery,
          panelId,
        }),
      );

      expect(result).toEqual(state);
    });

    it('should set search filter for passed panel and leave other panels ', () => {
      const testSearchQuery = 'testing';
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '12345',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        viewPresetActions.searchFilter({
          searchQuery: testSearchQuery,
          panelId,
        }),
      );

      expect(result.entities[panelId]!.searchQuery).toEqual(testSearchQuery);
      expect(result.entities[panelId2]!.searchQuery).toEqual(
        state.entities[panelId2]!.searchQuery,
      );
    });
  });

  describe('setMapPresetError', () => {
    it('should set error on setMapPresetError', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError = 'something went wrong setting map preset';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        mapActions.setMapPresetError({
          mapId: panelId,
          error: testError,
        }),
      );
      const expectedError: ViewPresetError = {
        message: testError,
        component: ViewPresetErrorComponent.PRESET_DETAIL,
        errorType: ViewPresetErrorType.GENERIC,
      };

      expect(result.entities[panelId]!.isFetching).toBeFalsy();
      expect(result.entities[panelId]!.error).toEqual(expectedError);
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if can not find map preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError = 'something went wrong setting map preset';

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        mapActions.setMapPresetError({
          mapId: 'test-3',
          error: testError,
        }),
      );

      expect(result.entities[panelId]!.isFetching).toBeTruthy();
      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.isFetching).toBeTruthy();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });
  });

  describe('submitFormWorkspaceActionDialogOptions', () => {
    it('should reset errors on form submit', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError = {
        message: 'something wrong',
        component: ViewPresetErrorComponent.PRESET_DETAIL,
        errorType: ViewPresetErrorType.GENERIC,
      };

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: testError,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: testError,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        workspaceListActions.submitFormWorkspaceActionDialogOptions({
          presetAction: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
          presetId: 'workspaceList-1',
          emptyMapPresetTitle: 'New map preset',
          data: {
            title: 'Workspace list item',
            abstract: 'some abs',
            views: [
              {
                mosaicNodeId: panelId,
                viewPresetId: '',
              },
              {
                mosaicNodeId: panelId2,
                viewPresetId: '',
              },
            ],
            mosaicNode: 'test',
          },
          emptyWorkspacePresetTitle: translateKeyOutsideComponents(
            i18n.t,
            'workspace-new',
          ),
        }),
      );

      expect(result.entities[panelId]!.error).toBeUndefined();
      expect(result.entities[panelId2]!.error).toBeUndefined();
    });

    it('should do nothing if can not find view preset', () => {
      const panelId = 'test-1';
      const panelId2 = 'test-2';
      const testError = {
        message: 'something wrong',
        component: ViewPresetErrorComponent.PRESET_DETAIL,
        errorType: ViewPresetErrorType.GENERIC,
      };

      const state: ViewPresetState = {
        entities: {
          [panelId]: {
            panelId,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: testError,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [panelId2]: {
            panelId: panelId2,
            hasChanges: true,
            activeViewPresetId: '',
            isFetching: true,
            error: testError,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [panelId, panelId2],
      };

      const result = viewPresetsReducer(
        state,
        workspaceListActions.submitFormWorkspaceActionDialogOptions({
          presetAction: WorkspacePresetAction.SAVE_SYSTEM_PRESET_AS,
          presetId: 'workspaceList-1',
          emptyMapPresetTitle: 'New map preset',
          data: {
            title: 'Workspace list item',
            abstract: 'some abs',
            views: [
              {
                mosaicNodeId: 'panelId',
                viewPresetId: '',
              },
              {
                mosaicNodeId: 'panelId2',
                viewPresetId: '',
              },
            ],
            mosaicNode: 'test',
          },
          emptyWorkspacePresetTitle: translateKeyOutsideComponents(
            i18n.t,
            'workspace-new',
          ),
        }),
      );

      expect(result.entities[panelId]!.error).toEqual(testError);
      expect(result.entities[panelId2]!.error).toEqual(testError);
    });
  });

  describe('warningsPresetChangeActions', () => {
    it('should detect changes in the warning list filters', () => {
      const mapId = 'test-1';
      const mapId2 = 'test-2';
      const state: ViewPresetState = {
        entities: {
          [mapId]: {
            panelId: mapId,
            hasChanges: false,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
          [mapId2]: {
            panelId: mapId2,
            hasChanges: false,
            activeViewPresetId: '',
            isFetching: false,
            error: undefined,
            isViewPresetListDialogOpen: false,
            filters: [],
            searchQuery: '',
            filterResults: { ids: [], entities: {} },
          },
        },
        ids: [mapId, mapId2],
      };
      // If origin is system, don't do anything
      const result = viewPresetsReducer(
        state,
        publicWarningActions.setWarningFilters({
          filterState: {
            domain: { aviation: true, public: false, maritime: false },
          },
          panelId: mapId,
          systemUpdate: true,
        }),
      );
      expect(result.entities[mapId]!.hasChanges).toBeFalsy();
      expect(result.entities[mapId2]!.hasChanges).toBeFalsy();

      const result1 = viewPresetsReducer(
        state,
        publicWarningActions.setWarningFilters({
          filterState: {
            domain: { aviation: true, public: false, maritime: true },
          },
          panelId: mapId,
          systemUpdate: false,
        }),
      );
      expect(result1.entities[mapId]!.hasChanges).toBeTruthy();
      expect(result1.entities[mapId2]!.hasChanges).toBeFalsy();
    });
  });
});
