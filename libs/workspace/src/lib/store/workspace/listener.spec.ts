/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { PresetScope } from '@opengeoweb/shared';
import { genericActions, mapActions, MapPreset } from '@opengeoweb/store';
import { waitFor } from '@testing-library/react';
import { MosaicNode } from 'react-mosaic-component';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { createMockStore } from '../store';
import { ViewPresetEntity, ViewPresetState } from '../viewPresets/types';
import { WorkspaceListState } from '../workspaceList/types';
import { createCustomEmptyMapWorkspace } from '../workspaceList/utils';
import { workspaceActions } from './reducer';
import {
  ViewType,
  WorkspacePreset,
  WorkspacePresetFromBE,
  WorkspaceState,
} from './types';
import * as workSpaceUtils from './utils';

const viewId = 'test-1';
const preloadedStateDefault = {
  workspace: {
    title: 'test title',
    mosaicNode: viewId,
    viewType: 'tiledWindow' as ViewType,
    views: {
      byId: {
        [viewId]: {
          id: viewId,
          componentType: 'Map',
        },
      },
      allIds: [viewId],
    },
  } as WorkspaceState,
  viewPresets: {
    ids: [viewId],
    entities: {
      [viewId]: {
        panelId: viewId,
        activeViewPresetId: 'someview',
        hasChanges: false,
        isFetching: false,
      } as ViewPresetEntity,
    },
  } as ViewPresetState,
};

describe('store/workspace/listener', () => {
  const preloadedStateTiledWindow = {
    workspace: {
      title: 'test title',
      mosaicNode: viewId,
      viewType: 'tiledWindow' as ViewType,
      views: {
        byId: {
          [viewId]: {
            id: viewId,
            componentType: 'Map',
          },
        },
        allIds: [viewId],
      },
    },
    viewPresets: { ids: [], entities: {} },
  };
  describe('registerMap listener', () => {
    it('should register mapPresets when viewType is tiledWindow', async () => {
      // Setup stuff

      const store = createMockStore(preloadedStateTiledWindow);

      // Check
      expect(store.getState().workspace.viewType).toEqual('tiledWindow');
      expect(store.getState().viewPresets.ids).toHaveLength(0);

      store.dispatch(mapActions.registerMap({ mapId: viewId }));
      await waitFor(() => {
        expect(store.getState().viewPresets.ids).toHaveLength(1);
      });
    });
  });
  describe('unregisterMap listener', () => {
    it('should unregister mapPresets when viewType is tiledWindow', async () => {
      // Setup stuff
      const store = createMockStore(preloadedStateDefault);

      // Check
      expect(store.getState().workspace.viewType).toEqual('tiledWindow');
      expect(store.getState().viewPresets.ids).toHaveLength(1);

      store.dispatch(mapActions.unregisterMap({ mapId: viewId }));
      await waitFor(() => {
        expect(store.getState().viewPresets.ids).toHaveLength(0);
      });
    });
    it('should not unregister mapPresets when viewType is not tiledWindow', async () => {
      // Setup stuff
      const preloadedStateNoTiledWindow = {
        workspace: {
          title: 'test title',
          mosaicNode: viewId,
          viewType: 'singleWindow' as ViewType,
          views: {
            byId: {
              [viewId]: {
                id: viewId,
                componentType: 'Map',
              },
            },
            allIds: [viewId],
          },
        },
        viewPresets: {
          ids: [viewId],
          entities: {
            [viewId]: {
              panelId: viewId,
              activeViewPresetId: 'someview',
              hasChanges: false,
              isFetching: false,
            } as ViewPresetEntity,
          },
        },
      };

      const store = createMockStore(preloadedStateNoTiledWindow);

      // Check
      expect(store.getState().workspace.viewType).toEqual('singleWindow');
      expect(store.getState().viewPresets.ids).toHaveLength(1);

      store.dispatch(mapActions.unregisterMap({ mapId: viewId }));
      await waitFor(() => {
        expect(store.getState().viewPresets.ids).toHaveLength(1);
      });
    });
  });

  describe('changePreset', () => {
    const workspaceStore: WorkspacePreset = {
      id: 'radarSingleMapScreenConfig',
      title: 'Radar view',
      views: {
        allIds: ['radarView'],
        byId: {
          radarView: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [],
                proj: {
                  bbox: {
                    left: 58703.6377,
                    bottom: 6408480.4514,
                    right: 3967387.5161,
                    top: 11520588.9031,
                  },
                  srs: 'EPSG:3857',
                },
                activeLayerId: 'radar_precipitation_intensity_nordic_id',
                toggleTimestepAuto: false,
                setTimestep: 5,
              },
            },
          },
        },
      },
      mosaicNode: 'radarView',
      syncGroups: [],
    };

    const changeActionPayload = {
      id: 'harmSingleMapScreenConfig',
      title: 'HARM',
      views: {
        allIds: ['harm'],
        byId: {
          harm: {
            title: 'HARM',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [],
                proj: {
                  bbox: {
                    left: -450651.2255879827,
                    bottom: 6490531.093143953,
                    right: 1428345.8183648037,
                    top: 7438773.776232235,
                  },
                  srs: 'EPSG:3857',
                },
              },
            },
          },
        },
      },
      mosaicNode: 'harm',
      linking: { harm: [] },
      isTimeScrollingEnabled: true,
    };

    const emptySyncGroupsState = genericActions.initialSyncState;

    it('should trigger setPreset when switching presets and register default sync groups if none passed', async () => {
      const preloadedState = {
        workspace: workspaceStore,
        syncGroups: emptySyncGroupsState,
      };
      const store = createMockStore(preloadedState);

      // Check setup
      expect(store.getState().workspace.id).toEqual(
        'radarSingleMapScreenConfig',
      );
      expect(store.getState().syncGroups).toStrictEqual(emptySyncGroupsState);

      store.dispatch(
        workspaceActions.changePreset({
          workspacePreset: changeActionPayload,
        }),
      );

      // Should have set new workspace preset
      await waitFor(() =>
        expect(store.getState().workspace.id).toEqual(
          'harmSingleMapScreenConfig',
        ),
      );
      // Should have registered new sync groups
      expect(store.getState().syncGroups.sources.allIds).toHaveLength(0);
      expect(store.getState().syncGroups.groups.allIds).toHaveLength(2);
      expect(store.getState().syncGroups.isTimeScrollingEnabled).toBeTruthy();
      expect(store.getState().syncGroups.linkedState.links).toEqual({
        harm: [],
      });
    });

    it('should trigger setPreset when switching presets and register given workspace sync groups', async () => {
      const testActionWithSyncGroups: WorkspacePreset = {
        id: 'harmSingleMapScreenConfig',
        title: 'HARM',
        views: {
          allIds: ['harm'],
          byId: {
            harm: {
              title: 'HARM',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: -450651.2255879827,
                      bottom: 6490531.093143953,
                      right: 1428345.8183648037,
                      top: 7438773.776232235,
                    },
                    srs: 'EPSG:3857',
                  },
                },
              },
            },
          },
        },
        mosaicNode: 'harm',
        syncGroups: [
          { id: 'areaLayerGroupA', type: 'SYNCGROUPS_TYPE_SETBBOX' as const },
          { id: 'timeLayerGroupA', type: 'SYNCGROUPS_TYPE_SETTIME' as const },
          {
            id: 'layerGroupA',
            type: 'SYNCGROUPS_TYPE_SETLAYERACTIONS' as const,
          },
          {
            id: 'layerGroupB',
            type: 'SYNCGROUPS_TYPE_SETLAYERACTIONS' as const,
          },
        ],
      };

      const preloadedState = {
        workspace: workspaceStore,
        syncGroups: emptySyncGroupsState,
      };
      const store = createMockStore(preloadedState);

      // Check setup
      expect(store.getState().workspace.id).toEqual(workspaceStore.id);
      expect(store.getState().workspace.title).toEqual(workspaceStore.title);
      // expect(store.getState().syncGroups).toStrictEqual({});

      store.dispatch(
        workspaceActions.changePreset({
          workspacePreset: testActionWithSyncGroups,
        }),
      );

      // Should have set new preset
      await waitFor(() =>
        expect(store.getState().workspace.id).toEqual(
          testActionWithSyncGroups.id,
        ),
      );
      expect(store.getState().workspace.title).toEqual(
        testActionWithSyncGroups.title,
      );
      // Should have registered passed sync groups
      expect(store.getState().syncGroups.sources.allIds).toHaveLength(0);
      expect(store.getState().syncGroups.groups.allIds).toHaveLength(4);
      expect(store.getState().syncGroups.isTimeScrollingEnabled).toBeFalsy();
      expect(store.getState().syncGroups.linkedState.links).toEqual({});
    });
  });

  describe('fetchWorkspaceViewPreset', () => {
    const viewPresetId = 'screenA';
    const workspaceStore: WorkspacePreset = {
      id: 'radarSingleMapScreenConfig',
      title: 'Radar view',
      views: {
        allIds: [viewPresetId],
        byId: {
          [viewPresetId]: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [],
                proj: {
                  bbox: {
                    left: 58703.6377,
                    bottom: 6408480.4514,
                    right: 3967387.5161,
                    top: 11520588.9031,
                  },
                  srs: 'EPSG:3857',
                },
                activeLayerId: 'radar_precipitation_intensity_nordic_id',
                toggleTimestepAuto: false,
                setTimestep: 5,
              },
            },
          },
        },
      },
      mosaicNode: viewPresetId,
      syncGroups: [],
    };

    const viewPresetsState = {
      entities: {
        [viewPresetId]: {
          panelId: viewPresetId,
          hasChanges: false,
          activeViewPresetId: '',
          isFetching: true,
          error: undefined,
          isViewPresetListDialogOpen: false,
          filters: [],
          searchQuery: '',
          filterResults: { ids: [], entities: {} },
        },
      },
      ids: [viewPresetId],
    };

    const syncGroupsState = {
      ...genericActions.initialSyncState,
      groups: {
        byId: {
          Area_radarTemp: {
            id: 'Area_radarTemp',
            type: 'SYNCGROUPS_TYPE_SETBBOX' as const,
            title: 'area temp',
            targets: { allIds: [], byId: {} },
            payloadByType: {},
          },
          Time_radarTemp: {
            id: 'Time_radarTemp',
            type: 'SYNCGROUPS_TYPE_SETTIME' as const,
            title: 'time box',
            targets: { allIds: [], byId: {} },
            payloadByType: {},
          },
        },
        allIds: ['Area_radarTemp', 'Time_radarTemp'],
      },
    };

    const mockApi = createFakeApi();
    jest
      .spyOn(workSpaceUtils, 'getWorkspaceApi')
      .mockImplementation(() => mockApi);

    it('should trigger fetching of full viewPresets', async () => {
      const testIds = ['Area_radarTemp', 'Time_radarTemp'];

      const fakeResponse = {
        data: {
          id: viewPresetId,
          initialProps: { syncGroupsIds: testIds },
          componentType: 'Map' as const,
          title: 'test',
          keywords: '',
        },
      };

      const mockApi = createFakeApi();
      jest
        .spyOn(workSpaceUtils, 'getWorkspaceApi')
        .mockImplementation(() => mockApi);

      jest.spyOn(mockApi, 'getViewPreset').mockResolvedValue(fakeResponse);

      const preloadedState = {
        workspace: workspaceStore,
        syncGroups: syncGroupsState,
        viewPresets: viewPresetsState,
      };

      const store = createMockStore(preloadedState);

      // Check setup
      expect(store.getState().workspace.id).toEqual(
        'radarSingleMapScreenConfig',
      );
      expect(store.getState().viewPresets.ids).toHaveLength(1);
      expect(
        store.getState().viewPresets.entities[viewPresetId]?.isFetching,
      ).toBeTruthy();
      expect(store.getState().syncGroups).toStrictEqual(syncGroupsState);
      expect(
        store.getState().workspace.views.byId![viewPresetId].title,
      ).toEqual('Radar view');

      store.dispatch(
        workspaceActions.fetchWorkspaceViewPreset({
          viewPresetId,
          mosaicNodeId: viewPresetId,
        }),
      );

      // Should set the view panel as loading
      expect(store.getState().workspace.views.byId![viewPresetId].title).toBe(
        `Loading ${viewPresetId}`,
      );

      // Should fetch the view
      await waitFor(() => {
        expect(mockApi.getViewPreset).toHaveBeenCalled();
      });

      // Should have fetched the viewpreset and set isFetching to false
      await waitFor(() => {
        expect(
          store.getState().viewPresets.entities[viewPresetId]?.isFetching,
        ).toBeFalsy();
      });
      // Should sync all connected syncgroups of the viewpreset to the store
      await waitFor(() => {
        expect(
          store.getState().syncGroups.groups.byId['Area_radarTemp'].targets
            .allIds,
        ).toHaveLength(1);
      });

      expect(
        store.getState().syncGroups.groups.byId['Time_radarTemp'].targets
          .allIds,
      ).toHaveLength(1);

      // Should update the workspace with the fetched preset data
      expect(
        store.getState().workspace.views.byId![viewPresetId].title,
      ).toEqual('test');
    });

    it('should handle errors', async () => {
      const fakeResponse = {
        data: {
          error: 'error',
        },
      };

      const mockApi = createFakeApi();
      jest
        .spyOn(workSpaceUtils, 'getWorkspaceApi')
        .mockImplementation(() => mockApi);

      jest.spyOn(mockApi, 'getViewPreset').mockRejectedValue(fakeResponse);

      const preloadedState = {
        workspace: workspaceStore,
        viewPresets: viewPresetsState,
      };
      const store = createMockStore(preloadedState);

      // Check setup
      expect(store.getState().workspace.id).toEqual(
        'radarSingleMapScreenConfig',
      );
      expect(store.getState().viewPresets.ids).toHaveLength(1);
      expect(
        store.getState().viewPresets.entities[viewPresetId]?.isFetching,
      ).toBeTruthy();

      store.dispatch(
        workspaceActions.fetchWorkspaceViewPreset({
          viewPresetId,
          mosaicNodeId: viewPresetId,
        }),
      );

      // Should set the view panel as loading
      expect(store.getState().workspace.views.byId![viewPresetId].title).toBe(
        `Loading ${viewPresetId}`,
      );

      // Should fetch the view
      await waitFor(() => {
        expect(mockApi.getViewPreset).toHaveBeenCalled();
      });

      // Should error
      await waitFor(() => {
        expect(store.getState().workspace.views.byId![viewPresetId].title).toBe(
          `Error ${viewPresetId}`,
        );
      });
    });
  });

  describe('fetchWorkspace', () => {
    const viewPresetId = 'screenA';
    const workspaceStore: WorkspacePreset = {
      id: 'radarSingleMapScreenConfig',
      title: 'Radar view',
      abstract: '',
      views: {
        allIds: [viewPresetId],
        byId: {
          [viewPresetId]: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [],
                proj: {
                  bbox: {
                    left: 58703.6377,
                    bottom: 6408480.4514,
                    right: 3967387.5161,
                    top: 11520588.9031,
                  },
                  srs: 'EPSG:3857',
                },
                activeLayerId: 'radar_precipitation_intensity_nordic_id',
                toggleTimestepAuto: false,
                setTimestep: 5,
              },
            },
          },
        },
      },
      mosaicNode: viewPresetId,
      syncGroups: [],
    };

    const viewPresetsState = {
      entities: {
        [viewPresetId]: {
          panelId: viewPresetId,
          hasChanges: false,
          activeViewPresetId: '',
          isFetching: true,
          error: undefined,
          isViewPresetListDialogOpen: false,
          filters: [],
          searchQuery: '',
          filterResults: { ids: [], entities: {} },
        },
      },
      ids: [viewPresetId],
    };

    const mockApi = createFakeApi();
    jest
      .spyOn(workSpaceUtils, 'getWorkspaceApi')
      .mockImplementation(() => mockApi);

    it('should handle fetchWorkspace', async () => {
      const workspaceId = 'workspaceHarmonie';
      const fakeResponse = {
        data: {
          id: workspaceId,
          title: 'Harmonie',
          scope: 'system' as PresetScope,
          abstract: 'abstract you know',
          viewType: 'multiWindow' as ViewType,
          views: [
            { mosaicNodeId: 'viewA', viewPresetId: 'screenConfigRadarTemp' },
            { mosaicNodeId: 'viewB', viewPresetId: 'screenConfigTimeSeries' },
            { mosaicNodeId: 'viewC', viewPresetId: 'screenConfigRadarTemp-2' },
            { mosaicNodeId: 'viewD', viewPresetId: 'screenConfigTimeSeries-2' },
            { mosaicNodeId: 'viewE', viewPresetId: 'screenConfigRadarTemp-3' },
            { mosaicNodeId: 'viewF', viewPresetId: 'screenConfigTimeSeries-3' },
          ],
          syncGroups: [],
          mosaicNode: 'screen1',
        },
      };

      const mockApi = createFakeApi();
      jest
        .spyOn(workSpaceUtils, 'getWorkspaceApi')
        .mockImplementation(() => mockApi);

      jest.spyOn(mockApi, 'getWorkspacePreset').mockResolvedValue(fakeResponse);
      const getViewPreset = jest.spyOn(mockApi, 'getViewPreset');

      const preloadedState = {
        workspace: workspaceStore,
        workspaceList: {
          isFetching: true,
          isWorkspaceListDialogOpen: true,
          ids: [],
          entities: {},
          error: undefined,
        } as WorkspaceListState,
        viewPresets: viewPresetsState,
      };

      const store = createMockStore(preloadedState);

      // Check setup
      expect(store.getState().workspace.title).toEqual('Radar view');
      expect(store.getState().workspace.abstract).toEqual('');
      expect(
        store.getState().workspaceList.isWorkspaceListDialogOpen,
      ).toBeTruthy();
      expect(store.getState().workspace.views.allIds).toHaveLength(1);
      expect(store.getState().workspace.views.allIds[0]).toBe(viewPresetId);

      store.dispatch(
        workspaceActions.fetchWorkspace({
          workspaceId,
        }),
      );

      // Close workspace dialog
      expect(
        store.getState().workspaceList.isWorkspaceListDialogOpen,
      ).toBeFalsy();

      // Should set as loaded workspace
      await waitFor(() => {
        expect(store.getState().workspace.title).toBe(fakeResponse.data.title);
      });
      expect(store.getState().workspace.abstract).toBe(
        fakeResponse.data.abstract,
      );
      // Should fetch all view presets in batches
      await waitFor(() => {
        expect(store.getState().workspace.views.allIds).toHaveLength(6);
      });
      expect(store.getState().workspace.views.allIds[0]).toBe('viewA');

      expect(getViewPreset).toHaveBeenCalledTimes(6);
    });

    it('should handle error if no workspace returned', async () => {
      const workspaceId = 'workspaceHarmonie';
      const fakeResponse = {
        data: undefined as unknown as WorkspacePresetFromBE,
      };

      const mockApi = createFakeApi();
      jest
        .spyOn(workSpaceUtils, 'getWorkspaceApi')
        .mockImplementation(() => mockApi);

      jest.spyOn(mockApi, 'getWorkspacePreset').mockResolvedValue(fakeResponse);

      const preloadedState = {
        workspace: workspaceStore,
      };
      const store = createMockStore(preloadedState);

      // Check setup
      expect(store.getState().workspace.id).toEqual(
        'radarSingleMapScreenConfig',
      );

      expect(
        store.getState().workspace.views.byId![viewPresetId].title,
      ).toEqual('Radar view');

      store.dispatch(
        workspaceActions.fetchWorkspace({
          workspaceId,
        }),
      );

      // Should error
      await waitFor(() => {
        expect(store.getState().workspace.error).toStrictEqual({
          message: 'Request failed with status code 400',
          type: 'NOT_FOUND',
        });
      });
    });

    it('should handle error if error thrown in the try', async () => {
      const workspaceId = 'workspaceHarmonie';
      const fakeResponse = {
        message: 'error',
      };

      const mockApi = createFakeApi();
      jest
        .spyOn(workSpaceUtils, 'getWorkspaceApi')
        .mockImplementation(() => mockApi);

      jest.spyOn(mockApi, 'getWorkspacePreset').mockRejectedValue(fakeResponse);

      const preloadedState = {
        workspace: workspaceStore,
      };
      const store = createMockStore(preloadedState);

      // Check setup
      expect(store.getState().workspace.id).toEqual(
        'radarSingleMapScreenConfig',
      );

      expect(
        store.getState().workspace.views.byId![viewPresetId].title,
      ).toEqual('Radar view');

      store.dispatch(
        workspaceActions.fetchWorkspace({
          workspaceId,
        }),
      );

      // Should error
      await waitFor(() => {
        expect(store.getState().workspace.error).toStrictEqual({
          message: 'error',
          type: 'GENERIC',
        });
      });
    });
  });

  describe('saveWorkspacePreset', () => {
    const workspaceData: WorkspacePresetFromBE = {
      abstract: 'some abstract',
      id: 'workspaceId',
      mosaicNode: 'test',
      scope: 'user',
      syncGroups: [
        {
          id: 'Default group for SYNCGROUPS_TYPE_SETBBOX',
          type: 'SYNCGROUPS_TYPE_SETBBOX',
        },
        {
          id: 'Default group for SYNCGROUPS_TYPE_SETTIME',
          type: 'SYNCGROUPS_TYPE_SETTIME',
        },
      ],
      title: 'test workspace',
      viewType: 'singleWindow',
      views: [{ mosaicNodeId: 'test', viewPresetId: 'screenSat' }],
    };

    it('should handle saveWorkspacePreset', async () => {
      const workspaceStore = {
        title: 'test title',
        isLoading: true,
        mosaicNode: viewId,
        viewType: 'tiledWindow' as ViewType,
        views: {
          byId: {
            [viewId]: {
              id: viewId,
              componentType: 'Map',
            },
          },
          allIds: [viewId],
        },
      } as WorkspaceState;

      const mockApi = createFakeApi();
      jest
        .spyOn(workSpaceUtils, 'getWorkspaceApi')
        .mockImplementation(() => mockApi);

      const preloadedState = {
        workspace: workspaceStore,
      };
      const store = createMockStore(preloadedState);

      // Check setup
      expect(store.getState().workspace.isLoading).toBeTruthy();
      expect(store.getState().workspace.title).toEqual('test title');

      store.dispatch(
        workspaceActions.saveWorkspacePreset({
          workspace: workspaceData,
        }),
      );

      // Should have set isLoading to false
      await waitFor(() => {
        expect(store.getState().workspace.isLoading).toBeFalsy();
      });
    });

    it('should handle errors', async () => {
      const fakeResponse = {
        message: 'error',
      };

      const mockApi = createFakeApi();
      jest
        .spyOn(workSpaceUtils, 'getWorkspaceApi')
        .mockImplementation(() => mockApi);

      jest
        .spyOn(mockApi, 'saveWorkspacePreset')
        .mockRejectedValue(fakeResponse);

      const store = createMockStore(preloadedStateDefault);

      // Check setup
      expect(store.getState().workspace.title).toEqual('test title');

      store.dispatch(
        workspaceActions.saveWorkspacePreset({
          workspace: workspaceData,
        }),
      );

      // Should error
      await waitFor(() => {
        expect(store.getState().workspace.error).toStrictEqual({
          message: 'error',
          type: 'SAVE',
        });
      });
    });
  });

  describe('changeToEmptyMapWorkspace', () => {
    it('should handle change preset to empty mapworkspace', () => {
      const emptyWorkspaceTitle = 'New workspace';
      const emptyMapWorkspaceTitle = 'New map preset';
      const mapPresetOfNL: MapPreset = {
        proj: {
          bbox: {
            top: 7342085.640241,
            left: -450651.2255879827,
            right: 1428345.8183648037,
            bottom: 6393842.957153378,
          },
          srs: 'EPSG:3857',
        },
      };

      const emptyWorkspace = createCustomEmptyMapWorkspace(
        emptyWorkspaceTitle,
        emptyMapWorkspaceTitle,
        mapPresetOfNL,
      );

      const store = createMockStore(preloadedStateDefault);
      // Check setup
      expect(store.getState().workspace.title).toEqual('test title');

      store.dispatch(
        workspaceActions.changeToEmptyMapWorkspace({
          newMapPresetText: emptyMapWorkspaceTitle,
          newWorkspaceText: emptyWorkspaceTitle,
        }),
      );

      expect(store.getState().workspace.title).toEqual(emptyWorkspace.title);
    });
  });
  describe('setNewWorkspaceView', () => {
    it('should add new workspace view', async () => {
      const mosaicNodeId = 'view-1';
      const componentType = 'Map';
      const newId = 'view-2';

      const bbox = {
        left: -450651.2255879827,
        bottom: 6490531.093143953,
        right: 1428345.8183648037,
        top: 7438773.776232235,
      };
      const srs = 'EPSG:3857';
      // Setup stuff
      const mapId = 'map_1';

      const preloadedState = {
        workspace: preloadedStateDefault.workspace,
      };
      const store = createMockStore(preloadedState);

      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(mapActions.setBbox({ mapId, bbox, srs }));

      // Check setup
      expect(store.getState().workspace.title).toEqual('test title');
      expect(store.getState().webmap.byId[mapId].srs).toBe('EPSG:3857');
      expect(store.getState().workspace.views.allIds).toHaveLength(1);

      store.dispatch(
        workspaceActions.setNewWorkspaceView({
          mosaicNodeId,
          componentType,
          newId,
        }),
      );

      // A new view should be added
      await waitFor(() => {
        expect(store.getState().workspace.views.allIds).toHaveLength(2);
      });
      expect(store.getState().workspace.hasChanges).toBeTruthy();
    });
  });

  describe('updateViews', () => {
    it('should handle updateViews', async () => {
      jest.spyOn(window, 'dispatchEvent');

      const newViews = {
        direction: 'row',
        first: 'testviewA',
        second: 'testviewB',
      };
      const store = createMockStore(preloadedStateDefault);
      // Check setup
      expect(store.getState().workspace.title).toEqual('test title');

      store.dispatch(
        workspaceActions.updateWorkspaceViews({
          mosaicNode: newViews as MosaicNode<string>,
        }),
      );
      await waitFor(() => {
        expect(window.dispatchEvent).toHaveBeenCalled();
      });
    });
  });
});
