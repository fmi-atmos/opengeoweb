/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { fireEvent, render, screen } from '@testing-library/react';
import { UserMenuRoles } from './UserMenuRoles';
import { DemoWrapper } from '../Providers/Providers';
import { Role } from '../ApiContext/types';

describe('components/UserMenuRoles', () => {
  const roles: Role[] = [
    { name: 'admin', getTitle: () => 'Admin' },
    { name: 'user', getTitle: () => 'User' },
  ];

  const onChangeRoleMock = jest.fn();
  const isLoading = false;

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('renders component without crashing', () => {
    render(
      <DemoWrapper>
        <UserMenuRoles
          roles={roles}
          onChangeRole={onChangeRoleMock}
          isLoading={isLoading}
        />
      </DemoWrapper>,
    );
  });

  it('renders loading bar when isLoading is true', () => {
    render(
      <DemoWrapper>
        <UserMenuRoles
          roles={roles}
          onChangeRole={onChangeRoleMock}
          isLoading={true}
        />
      </DemoWrapper>,
    );
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
  });

  it('calls onChangeRole when radio is clicked', () => {
    render(
      <DemoWrapper>
        <UserMenuRoles
          roles={roles}
          onChangeRole={onChangeRoleMock}
          isLoading={isLoading}
        />
      </DemoWrapper>,
    );
    const radio = screen.getByLabelText('Admin');
    fireEvent.click(radio);
    expect(onChangeRoleMock).toHaveBeenCalledWith(roles[0]);
  });

  it('radio is disabled when loading', () => {
    render(
      <DemoWrapper>
        <UserMenuRoles
          roles={roles}
          onChangeRole={onChangeRoleMock}
          isLoading={isLoading}
        />
      </DemoWrapper>,
    );
    expect(screen.getByLabelText('User').getAttribute('checked')).toBeDefined();
    expect(
      screen.getByLabelText('Admin').getAttribute('checked'),
    ).toBeDefined();
  });

  it('radio is checked if currentRole prop is same as role', () => {
    render(
      <DemoWrapper>
        <UserMenuRoles
          roles={roles}
          onChangeRole={onChangeRoleMock}
          isLoading={isLoading}
          currentRole="user"
        />
      </DemoWrapper>,
    );

    expect(screen.getByLabelText('User').getAttribute('checked')).toEqual('');
    expect(screen.getByLabelText('Admin').getAttribute('checked')).toEqual(
      null,
    );
  });
});
