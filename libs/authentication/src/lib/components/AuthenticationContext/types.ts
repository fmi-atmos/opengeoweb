/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { SessionStorageProvider } from '../../utils/session';
import { Credentials, Role } from '../ApiContext/types';

export interface AuthenticationConfig {
  GW_AUTH_LOGIN_URL: string;
  GW_AUTH_LOGOUT_URL: string;
  GW_AUTH_TOKEN_URL: string;
  GW_AUTH_ROLE_CLAIM_NAME?: string;
  GW_AUTH_ROLE_CLAIM_VALUE_PRESETS_ADMIN?: string;
  GW_APP_URL: string;
  GW_BE_VERSION_BASE_URL?: string;
  GW_AUTH_CLIENT_ID: string;
}

export interface AuthenticationDefaultStateProps {
  isLoggedIn: boolean;
  onLogin: (isLoggedIn: boolean) => void;
  auth: Credentials | null;
  onSetAuth: (auth: Credentials) => void;
  sessionStorageProvider: SessionStorageProvider;
  currentRole?: Role;
  setCurrentRole?: (newRole: Role) => void;
}

export interface AuthenticationContextProps
  extends AuthenticationDefaultStateProps {
  authConfig: AuthenticationConfig;
}
