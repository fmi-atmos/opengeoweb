/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { I18nextProvider } from 'react-i18next';
import i18n from 'i18next';
import {
  snackbarMiddlewares,
  snackbarReducersMap,
  SnackbarWrapperConnect,
} from '@opengeoweb/snackbar';
import { ThemeProvider } from '@opengeoweb/theme';
import { Provider } from 'react-redux';
import { combineReducers, configureStore, Store } from '@reduxjs/toolkit';
import { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';
import { initAuthTestI18n } from '../../utils/i18n';

interface AuthTranslationWrapperProps {
  children?: React.ReactNode;
}

export const AuthI18nProvider: React.FC<AuthTranslationWrapperProps> = ({
  children,
}) => {
  initAuthTestI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};

export const DemoWrapper: React.FC<AuthTranslationWrapperProps> = ({
  children,
}) => (
  <AuthI18nProvider>
    <ThemeProvider>{children as React.ReactElement}</ThemeProvider>
  </AuthI18nProvider>
);

export const DemoWrapperConnect: React.FC<
  AuthTranslationWrapperProps & { store: Store }
> = ({ children, store }) => (
  <DemoWrapper>
    <Provider store={store}>
      <SnackbarWrapperConnect>
        {children as React.ReactElement}
      </SnackbarWrapperConnect>
    </Provider>
  </DemoWrapper>
);

const rootReducer = combineReducers({
  ...snackbarReducersMap,
});

export const createTestAuthenticationStore = (): ToolkitStore => {
  return configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat([...snackbarMiddlewares]),
  });
};
