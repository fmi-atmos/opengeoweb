/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, waitFor, screen } from '@testing-library/react';
import { http, HttpResponse } from 'msw';
import { setupServer } from 'msw/node';
import { AuthenticationProvider } from '../AuthenticationContext';
import Code, { axiosInstance } from './Code';
import {
  mockloginSuccess,
  mockloginFailed,
  mockloginSuccessGitlab,
} from '../../utils/mockdata';
import {
  SessionStorageKey,
  getSessionStorageProvider,
} from '../../utils/session';
import { GEOWEB_ROLE_USER } from '../ApiContext/utils';

// mocking the Redirect function to make it easy to test
jest.mock('react-router-dom', () => {
  return {
    Navigate: jest.fn(({ to }) =>
      to.pathname ? `Redirected to ${to.pathname}` : `Redirected to ${to}`,
    ),
  };
});

// make sure the tests use the actual code for Router and Routes
const { BrowserRouter, Routes, Route } = jest.requireActual('react-router-dom');
const sessionStorageProvider = getSessionStorageProvider();

const props = {
  onSetAuth: jest.fn(),
  isLoggedIn: false,
  onLogin: jest.fn(),
  auth: {
    username: 'user',
    token: 'token',
    refresh_token: 'refresh_token',
  },
  sessionStorageProvider,
};

const configURLS = {
  GW_AUTH_LOGIN_URL: '/login&state=&code_challenge=',
  GW_AUTH_TOKEN_URL: '/token',
  GW_AUTH_LOGOUT_URL: '/logout',
  GW_AUTH_CLIENT_ID: 'someID',
  GW_APP_URL: 'localhost',
};

// declare which API requests to mock
const server = setupServer(
  // capture "POST /token" requests
  http.post(configURLS.GW_AUTH_TOKEN_URL, () => {
    // respond using a mocked JSON body
    return HttpResponse.json(mockloginSuccess);
  }),
);
const storedWindowLocation = window.location;

// establish API mocking before all tests
beforeAll(() => server.listen());
beforeEach(() => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  delete window.location;
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  window.location = new URL('http://localhost/code?code=someId&state=someId');
  // mock session storage
  jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('someId');
});
afterEach(() => {
  // reset any request handlers that are declared as a part of our tests
  server.resetHandlers();
  // restore location
  window.location = storedWindowLocation;
});
// clean up once the tests are done
afterAll(() => server.close());

describe('Code', () => {
  it('should redirect to the homepage when user is logged in', async () => {
    // making sure the state in the url is different, should not matter when already logged in
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(
      'http://localhost/code?code=someOtherId&state=someOtherId',
    );
    const mockProps = {
      ...props,
      isLoggedIn: true,
    };
    const spySetAuthenticated = jest.spyOn(
      mockProps.sessionStorageProvider,
      'setHasAuthenticated',
    );
    const spyGetState = jest.spyOn(
      mockProps.sessionStorageProvider,
      'getOauthState',
    );
    const testCallbackUrl = '/testRoute';
    jest
      .spyOn(sessionStorageProvider, 'getCallbackUrl')
      .mockReturnValue(testCallbackUrl);

    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...mockProps }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(screen.getByText(`Redirected to ${testCallbackUrl}`)).toBeTruthy();
    });

    expect(spyGetState).not.toHaveBeenCalled();
    expect(mockProps.onSetAuth).not.toHaveBeenCalled();
    expect(mockProps.onLogin).not.toHaveBeenCalled();
    expect(spySetAuthenticated).not.toHaveBeenCalled();
  });

  it('should handle a succesful login for cognito user', async () => {
    const mockProps = props;
    const spySetAuthenticated = jest.spyOn(
      mockProps.sessionStorageProvider,
      'setHasAuthenticated',
    );

    jest
      .spyOn(Date, 'now')
      .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());

    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...mockProps }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(mockProps.onSetAuth).toHaveBeenCalledWith({
        username: 'max.verstappen',
        roles: [GEOWEB_ROLE_USER],
        token: mockloginSuccess.access_token,
        refresh_token: mockloginSuccess.refresh_token,
        expires_at: 1640011500,
        has_connection_issue: false,
      });
    });
    expect(mockProps.onLogin).toHaveBeenCalledWith(true);
    expect(spySetAuthenticated).toHaveBeenCalledWith('true');
  });

  it('should handle a succesful login for non-cognito user', async () => {
    server.use(
      // override the initial "POST /token" request handler
      // to use a non-cognito user
      http.post(configURLS.GW_AUTH_TOKEN_URL, () => {
        return HttpResponse.json(mockloginSuccessGitlab);
      }),
    );
    const mockProps = props;
    const spySetAuthenticated = jest.spyOn(
      mockProps.sessionStorageProvider,
      'setHasAuthenticated',
    );
    jest
      .spyOn(Date, 'now')
      .mockReturnValue(new Date(`2021-12-20T14:00:00Z`).valueOf());
    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...mockProps }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(mockProps.onSetAuth).toHaveBeenCalledWith({
        username: 'max.verstappen@fakeemail.nl',
        roles: [GEOWEB_ROLE_USER],
        token: mockloginSuccessGitlab.access_token,
        refresh_token: mockloginSuccessGitlab.refresh_token,
        expires_at: 1640011500,
        has_connection_issue: false,
      });
    });
    expect(mockProps.onLogin).toHaveBeenCalledWith(true);
    expect(spySetAuthenticated).toHaveBeenCalledWith('true');
  });

  it('should handle a server error', async () => {
    server.use(
      // override the initial "POST /token" request handler
      // to return a 500 Server Error
      http.post(configURLS.GW_AUTH_TOKEN_URL, () => {
        return HttpResponse.json(
          { error: 'Internal server error' },
          { status: 500 },
        );
      }),
    );

    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...props }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await screen.findByText('Redirected to /error');
  });

  it('should handle a failed login', async () => {
    server.use(
      // override the initial "POST /token" request handler
      // to return a failed login
      http.post(configURLS.GW_AUTH_TOKEN_URL, () => {
        return HttpResponse.json(mockloginFailed);
      }),
    );

    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...props }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await screen.findByText('Redirected to /error');
  });

  it('should handle matching oauth states when state param configured', async () => {
    // mock window location with oauth state param
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    const oauthStateParam = 'state123456';
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(`http://localhost/code?state=${oauthStateParam}`);

    // configure and require oauth state param
    const configWithState = {
      ...configURLS,
      GW_AUTH_LOGIN_URL: `/login/code&state=${oauthStateParam}`,
    };

    // mock session storage oauth state
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue(oauthStateParam);
    const mockProps = props;
    render(
      <AuthenticationProvider
        configURLS={configWithState}
        value={{ ...mockProps }}
      >
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(mockProps.onSetAuth).toHaveBeenCalled();
    });
    expect(mockProps.onLogin).toHaveBeenCalledWith(true);
  });

  it('should redirect to the error page when state is configured, but not returned', async () => {
    // mock window location without oauth state param
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(`http://localhost/code`);

    // configure and require oauth state param
    const configWithState = {
      ...configURLS,
      GW_AUTH_LOGIN_URL: `/login/code&state={state}`,
    };

    // mock session storage oauth state
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('xyz');

    render(
      <AuthenticationProvider configURLS={configWithState} value={{ ...props }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );
    await screen.findByText('Redirected to /error');
  });

  it('should redirect to the error page when oauth states do not match', async () => {
    // mock window location with oauth state param
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    const stateQueryParam = '123456';
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(
      `http://localhost:4300/code?state=${stateQueryParam}`,
    );

    // configure and require oauth state param
    const sessionState = 'abcde';
    const configWithState = {
      ...configURLS,
      GW_AUTH_LOGIN_URL: `/login/code&state=${sessionState}`,
    };

    // mock session storage oauth state
    Storage.prototype.getItem = jest.fn(() => sessionState);

    render(
      <AuthenticationProvider configURLS={configWithState} value={{ ...props }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );
    await screen.findByText('Redirected to /error');
  });

  it('should redirect to the error page when oauth state is missing', async () => {
    // mock window location without oauth state param
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(`http://localhost:4300/code`);

    // configure and require oauth state param in login url
    const sessionState = 'abcde';
    const configWithState = {
      ...configURLS,
      GW_AUTH_LOGIN_URL: `/login/code&state=${sessionState}`,
    };

    render(
      <AuthenticationProvider configURLS={configWithState} value={{ ...props }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );
    await screen.findByText('Redirected to /error');
  });

  it('should redirect to the error page when oauth code verifier is missing', async () => {
    // mock session storage
    Storage.prototype.getItem = jest
      .fn()
      .mockReturnValueOnce('someId') // first call: state
      .mockReturnValueOnce(null); // second call: no code verifier

    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...props }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await screen.findByText('Redirected to /error');
  });

  it('should redirect to the error page when oauth code is missing', async () => {
    // mock window location without oauth code param
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(`http://localhost:4300/code?state=someId`);

    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...props }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );
    await screen.findByText('Redirected to /error');
  });

  it('should clear and not reuse generated values for oauth if login successful', async () => {
    const spy = jest.spyOn(Storage.prototype, 'removeItem');
    const mockProps = props;
    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...mockProps }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(mockProps.onSetAuth).toHaveBeenCalled();
    });

    expect(mockProps.onLogin).toHaveBeenCalledWith(true);
    // remove internal library calls
    const geowebCalls = spy.mock.calls
      .flat()
      .filter((call) => !/msw_/i.test(call));
    expect(geowebCalls[0]).toBe(SessionStorageKey.OAUTH_STATE);
    expect(geowebCalls[1]).toBe(SessionStorageKey.OAUTH_CODE_VERIFIER);
    expect(geowebCalls[2]).toBe(SessionStorageKey.OAUTH_CODE_CHALLENGE);
  });

  it('should clear and not reuse generated values for oauth if login failed', async () => {
    server.use(
      // override the initial "POST /token" request handler
      // to return a failed login
      http.post(configURLS.GW_AUTH_TOKEN_URL, () => {
        return HttpResponse.json(mockloginFailed);
      }),
    );

    const spy = jest.spyOn(Storage.prototype, 'removeItem');

    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...props }}>
        <BrowserRouter>
          <Routes>
            <Route path="/code" element={<Code />} />
          </Routes>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await screen.findByText('Redirected to /error');
    // remove internal library calls
    const geowebCalls = spy.mock.calls
      .flat()
      .filter((call) => !/msw_/i.test(call));
    expect(geowebCalls[0]).toBe(SessionStorageKey.OAUTH_STATE);
    expect(geowebCalls[1]).toBe(SessionStorageKey.OAUTH_CODE_VERIFIER);
    expect(geowebCalls[2]).toBe(SessionStorageKey.OAUTH_CODE_CHALLENGE);
  });

  it.each(new Array(10).fill(null))(
    'should only allow one login attempt at a time (attempt no. %#)',
    async () => {
      const axiosSpy = jest.spyOn(axiosInstance, 'post');

      const mockProps = props;
      const { rerender } = render(
        <AuthenticationProvider
          configURLS={configURLS}
          value={{ ...mockProps }}
        >
          <BrowserRouter>
            <Routes>
              <Route path="/code" element={<Code />} />
            </Routes>
          </BrowserRouter>
        </AuthenticationProvider>,
      );
      rerender(
        <AuthenticationProvider
          configURLS={configURLS}
          // change props to force rerender
          value={{ ...mockProps, onLogin: jest.fn() }}
        >
          <BrowserRouter>
            <Routes>
              <Route path="/code" element={<Code />} />
            </Routes>
          </BrowserRouter>
        </AuthenticationProvider>,
      );

      await waitFor(() => expect(axiosSpy).toHaveBeenCalledTimes(1));
    },
  );
});
