/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { capReducer, initialState, capActions } from './reducer';
import {
  AddManyAlertsToFeedPayload,
  CapAlert,
  FeedLastUpdatedPayload,
} from './types';

const sampleAlert1 = {
  certainty: 'certain',
  feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
  expires: '2022-10-19T00:00Z',
  identifier: '1',
  onset: '2022-10-18T00:00Z',
  severity: 'moderate',
  languages: [
    {
      areaDesc: 'Uusimaa ja Kymenlaakso',
      language: 'en-US',
      senderName: 'FMI',
      event: 'test event 1',
      headline: 'headline 1',
      description: 'description 1',
    },
  ],
};

const sampleAlert2 = {
  certainty: 'certain',
  feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
  expires: '2022-10-20T00:00Z',
  identifier: '2',
  onset: '2022-10-19T00:00Z',
  severity: 'moderate',
  languages: [
    {
      areaDesc: 'Uusimaa and Kymenlaakso',
      language: 'en-US',
      senderName: 'KNMI',
      event: 'test event 2',
      headline: 'headline 2',
      description: 'description 2',
    },
  ],
};

describe('store/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(capReducer(undefined, {})).toEqual(initialState);
  });

  it('should add many new alerts', () => {
    const testAlerts: CapAlert[] = [sampleAlert1, sampleAlert2];
    const result = capReducer(undefined, capActions.addManyAlerts(testAlerts));
    expect(result.alerts.ids.length).toBe(testAlerts.length);
  });

  it('should update already added alerts', () => {
    const updatedExpires = '2022-10-25T00:00Z';
    const sampleState = {
      alerts: {
        ids: ['1', '2'],
        entities: { '1': sampleAlert1, '2': sampleAlert2 },
      },
      feeds: { ids: [], entities: {} },
    };
    const result = capReducer(
      sampleState,
      capActions.addManyAlerts([{ ...sampleAlert1, expires: updatedExpires }]),
    );
    expect(result.alerts.ids.length).toBe(sampleState.alerts.ids.length);
    expect(result.alerts.entities['1']!.expires).toBe(updatedExpires);
  });

  it('should create new feed with alerts', () => {
    const sampleAction: AddManyAlertsToFeedPayload = {
      alertIds: ['1'],
      feedId: '123',
    };
    const result = capReducer(
      undefined,
      capActions.addManyAlertsToFeed(sampleAction),
    );
    expect(result.feeds.ids.length).toBe(1);
    expect(result.feeds.entities['123']!.alertIds).toEqual(
      sampleAction.alertIds,
    );
  });

  it('should update already added feed', () => {
    const sampleTimestamp = 1666126800; // 2022-10-19 00:00:00
    const sampleState = {
      alerts: {
        ids: [],
        entities: {},
      },
      feeds: {
        ids: ['123'],
        entities: {
          '123': { id: '123', lastUpdated: sampleTimestamp, alertIds: ['1'] },
        },
      },
    };
    const result = capReducer(
      sampleState,
      capActions.addManyAlertsToFeed({ feedId: '123', alertIds: ['1', '2'] }),
    );
    expect(result.feeds.entities['123']!.lastUpdated).toBeGreaterThan(
      sampleTimestamp,
    );
    expect(result.feeds.ids.length).toBe(1);
    expect(result.feeds.entities['123']!.alertIds.length).toBe(2);
  });

  it('should update last updated time on an existing feed', () => {
    const sampleTimestamp = 1666126800; // 2022-10-19 00:00:00
    const sampleState = {
      alerts: {
        ids: ['1'],
        entities: { '1': sampleAlert1 },
      },
      feeds: {
        ids: ['123'],
        entities: {
          '123': { id: '123', lastUpdated: sampleTimestamp, alertIds: ['1'] },
        },
      },
    };
    const updateAction: FeedLastUpdatedPayload[] = [
      {
        id: '123',
        changes: {
          lastUpdated: 1666731600, // 2022-10-26 00:00:00
        },
      },
    ];

    const result = capReducer(
      sampleState,
      capActions.setFeedLastUpdatedTimes(updateAction),
    );
    expect(result.feeds.entities['123']!.lastUpdated).toEqual(
      updateAction[0].changes.lastUpdated,
    );
  });
});
