/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { act, render, screen, waitFor } from '@testing-library/react';
import { WarningListCapConnectWrapper } from './WarningListCapConnectWrapper';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { CapApi } from '../../utils/api';
import { CapThemeStoreProvider, CapApiProvider } from '../Providers';
import capTrans from '../../../../locales/cap.json';
import { createMockStore } from '../../store';

describe('components/WarningListCap/WarningListCapConnectWrapper', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  const store = createMockStore();

  it('should render correctly with config', async () => {
    const capConfigKey = '/capConfiguration';

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={createFakeApi}>
          <WarningListCapConnectWrapper productConfigKey={capConfigKey} />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    await act(async () => jest.advanceTimersByTime(2000));
    await waitFor(() => {
      expect(screen.queryByTestId('loadingSpinner')).toBeFalsy();
    });
    expect(screen.getByTestId('warningList')).toBeTruthy();
  });

  it('should fetch cap config from api', async () => {
    const capRequest = jest.fn();
    const fakeApi = (): CapApi =>
      ({
        ...createFakeApi(),
        getCapConfiguration: capRequest,
      }) as unknown as CapApi;

    const capConfigKey = '/capConfiguration';

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={fakeApi}>
          <WarningListCapConnectWrapper productConfigKey={capConfigKey} />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );
    expect(screen.getByTestId('loadingSpinner')).toBeTruthy();

    await waitFor(() => {
      expect(screen.queryByTestId('warningList')).toBeFalsy();
    });

    expect(capRequest).toHaveBeenCalledWith(
      '/capConfiguration',
      expect.any(String),
    );
  });

  it('should show error if configuration is missing', async () => {
    const capRequest = jest.fn();
    const fakeApi = (): CapApi =>
      ({
        ...createFakeApi(),
        getCapConfiguration: capRequest,
      }) as unknown as CapApi;

    const emptyConfig = '';

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={fakeApi}>
          <WarningListCapConnectWrapper productConfigKey={emptyConfig} />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    expect(
      await screen.findByText(capTrans['en']['warning-list-cap-error-title']),
    ).toBeTruthy();
  });

  it('should not render without config', async () => {
    const capRequest = jest.fn();
    const fakeApi = (): CapApi =>
      ({
        ...createFakeApi(),
        getCapConfiguration: capRequest,
      }) as unknown as CapApi;

    const emptyConfig = '';

    render(
      <CapThemeStoreProvider store={store}>
        <CapApiProvider createApi={fakeApi}>
          <WarningListCapConnectWrapper productConfigKey={emptyConfig} />
        </CapApiProvider>
      </CapThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('warningList')).toBeFalsy();
    });
  });
});
