/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';

import { screen, render, act } from '@testing-library/react';
import { QueryClientProvider } from '@tanstack/react-query';
import { wmsQueryClient } from '@opengeoweb/webmap';
import { CapApi } from '../utils/api';
import { createApi } from '../utils/fakeApi';
import { CapApiProvider, CapThemeStoreProvider } from './Providers';
import { WarningListCapConnect } from './WarningListCap/WarningListCapConnect';
import {
  mockCapWarningPresets,
  mockCapWarnings,
  mockCapWarnings2,
  mockCapWarnings3,
} from '../utils/mockCapData';
import { MapViewCapConnect } from './MapViewCap/MapViewCapConnect';
import { createMockStore } from '../store';

describe('components/CapMapAndListIntegration', () => {
  it('should render list of warning rows with received contents', async () => {
    jest.useFakeTimers();

    const alert1url = 'https://alert1.example.com';
    const alert2url = 'https://alert2.example.com';
    const alert3url = 'https://alert3.example.com';
    const getLinksFn = jest.fn(() =>
      Promise.resolve({ data: [alert1url, alert2url, alert3url] }),
    );
    const getUpdatedFn = jest.fn();
    const getSingleAlertFn = jest.fn();
    getSingleAlertFn
      .mockImplementationOnce(() => Promise.resolve({ data: mockCapWarnings }))
      .mockImplementationOnce(() => ({
        data: mockCapWarnings2,
      }))
      .mockImplementationOnce(() => ({
        data: mockCapWarnings3,
      }));
    const createFakeApi = (): CapApi => ({
      ...createApi,
      getLastUpdated: getUpdatedFn,
      getSingleAlert: getSingleAlertFn,
      getFeedLinks: getLinksFn,
    });

    const store = createMockStore();

    const { container } = render(
      <QueryClientProvider client={wmsQueryClient}>
        <CapApiProvider createApi={createFakeApi}>
          <CapThemeStoreProvider store={store}>
            {/* include the map since it handles fetching */}
            <MapViewCapConnect capWarningPresets={mockCapWarningPresets} />
            <WarningListCapConnect capWarningPresets={mockCapWarningPresets} />
          </CapThemeStoreProvider>
        </CapApiProvider>
      </QueryClientProvider>,
    );

    expect(getUpdatedFn).not.toHaveBeenCalled();
    await act(async () => {
      jest.runOnlyPendingTimers();
    });
    expect(container).toBeTruthy();

    expect(getUpdatedFn).toHaveBeenCalled();
    expect(getLinksFn).toHaveBeenCalled();
    expect(getSingleAlertFn).toHaveBeenCalledWith(alert1url);
    expect(getSingleAlertFn).toHaveBeenCalledWith(alert2url);

    const alertEvents = screen.getAllByTestId('event');
    expect(alertEvents.length).toBe(3);
    expect(alertEvents[0].textContent).toBe(
      mockCapWarnings.alert.languages[2].event,
    );
    expect(alertEvents[1].textContent).toBe(
      mockCapWarnings2.alert.languages[2].event,
    );
    expect(alertEvents[2].textContent).toBe(
      mockCapWarnings3.alert.languages[2].event,
    );
    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
