/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { dateUtils } from '@opengeoweb/shared';
import { Task } from './Task';

const currentDate = dateUtils.createDate('2022-10-26T10:23:24.583Z');
export const tasks = [
  {
    id: 'Task 1',
    name: 'Task with custom color',
    start: dateUtils
      .createDate(currentDate.getFullYear(), currentDate.getMonth(), 1)
      .toISOString(),
    end: dateUtils
      .createDate(currentDate.getFullYear(), currentDate.getMonth(), 15)
      .toISOString(),
    progress: 10,
    dependencies: '',
    custom_class: 'geoweb-red',
    severity: 'Extreme',
  },
  {
    id: 'Task 2',
    name: 'Test task 2',
    start: dateUtils
      .createDate(currentDate.getFullYear(), currentDate.getMonth(), 5)
      .toISOString(),
    end: dateUtils
      .createDate(currentDate.getFullYear(), currentDate.getMonth(), 10)
      .toISOString(),
    progress: 20,
    dependencies: 'Task 1',
  },
  {
    id: 'Task 3',
    name: 'Test task 3',
    start: dateUtils
      .createDate(currentDate.getFullYear(), currentDate.getMonth(), 8)
      .toISOString(),
    end: dateUtils
      .createDate(currentDate.getFullYear(), currentDate.getMonth(), 12)
      .toISOString(),
    progress: 0,
    dependencies: 'Task 2, Task 1',
  },
] as Task[];
