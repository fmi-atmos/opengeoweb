/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { dateUtils } from '@opengeoweb/shared';
import { CustomFeature } from '../types';
import { Task } from './Task';
import {
  convertFeaturesToTasks,
  convertSeverityToColorClass,
  getEndDateFromHeadline,
} from './utils';

describe('components/GanttChart/utils', () => {
  describe('convertSeverityToColorClass', () => {
    it('should convert serverity to correct color class', () => {
      expect(convertSeverityToColorClass('Minor')).toEqual('geoweb-green');
      expect(convertSeverityToColorClass('Moderate')).toEqual('geoweb-yellow');
      expect(convertSeverityToColorClass('Severe')).toEqual('geoweb-orange');
      expect(convertSeverityToColorClass('Extreme')).toEqual('geoweb-red');
    });
  });

  describe('getEndDateFromHeadline', () => {
    it('should derive correct enddate from headline', () => {
      const year = dateUtils.createDate().getUTCFullYear();
      expect(
        getEndDateFromHeadline(
          'Gale, yellow level, Ona - Fr\u00f8ya, 10 November 14:00 UTC to 12 November 15:00 UTC.',
        ),
      ).toEqual(dateUtils.createDate(`${year}-11-12T15:00:00.000Z`));
      expect(
        getEndDateFromHeadline(
          `Very heavy rain, orange level, Middle and inner parts of Vestland, ${year}-11-10T18:00:00+00:00, ${year}-11-11T21:00:00+00:00`,
        ),
      ).toEqual(dateUtils.createDate(`${year}-11-11T21:00:00.000Z`));
    });
  });

  describe('convertFeaturesToTasks', () => {
    it('should handle empty list', () => {
      expect(convertFeaturesToTasks([])).toEqual([]);
    });
    it('should convert cap features to gantt tasks', () => {
      const mockFeature: CustomFeature = {
        type: 'Feature',
        properties: {
          fill: '',
          stroke: '',
          'stroke-width': 0,
          'stroke-opacity': 0,
          details: {
            identifier: '2.49.0.1.578.0.221111114809830.2420',
            feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
            severity: 'Moderate',
            certainty: 'Likely',
            onset: '2022-11-10T14:00:00+00:00',
            expires: '',
            languages: [
              {
                areaDesc: 'Ona - Fr\u00f8ya',
                language: 'no',
                event: 'Kuling',
                senderName: 'Meteorologisk Institutt',
                headline:
                  'Kuling, gult niv\u00e5, Ona - Fr\u00f8ya, 10 November 14:00 UTC til 12 November 15:00 UTC.',
                description:
                  'I dag, fredag, fortsatt s\u00f8rvest stiv kuling 15 m/s. I ettermiddag forbig\u00e5ende minking. Fra i kveld s\u00f8rvest stiv til sterk kuling 20. Tidlig l\u00f8rdag ettermiddag minkende, f\u00f8rst i s\u00f8r.',
              },
              {
                areaDesc: 'Ona - Fr\u00f8ya',
                language: 'en-GB',
                event: 'Gale',
                senderName: 'MET Norway',
                headline:
                  'Gale, yellow level, Ona - Fr\u00f8ya, 10 November 14:00 UTC to 12 November 15:00 UTC.',
                description:
                  'Today, Friday, still southwest near gale force 7. This afternoon temporary decrease. From this evening southwest near gale to gale force 8. Early Saturday afternoon decreasing, first in south.',
              },
            ],
          },
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [21.9287109375, 59.689926220143356],
              [28.256835937499996, 59.689926220143356],
              [28.256835937499996, 62.30879369102805],
              [21.9287109375, 62.30879369102805],
              [21.9287109375, 59.689926220143356],
            ],
          ],
        },
      };

      const { languages, severity, identifier, onset } =
        mockFeature.properties.details;
      const { areaDesc, headline, description, senderName, event } =
        languages[1];

      expect(convertFeaturesToTasks([mockFeature])).toEqual([
        {
          area: areaDesc,
          author: senderName,
          custom_class: convertSeverityToColorClass(
            severity as Task['severity'],
          ),
          dependencies: '',
          description,
          end: getEndDateFromHeadline(headline),
          id: identifier,
          name: `${areaDesc}, ${event}`,
          phenomenon: event,
          progress: 100,
          severity,
          start: dateUtils.createDate(onset),
          title: headline,
        },
      ]);
    });
  });
});
