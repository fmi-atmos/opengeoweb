/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Box } from '@mui/material';
import { CustomToggleButton } from '@opengeoweb/shared';
import React from 'react';

import { GanttReactWrapper } from './GanttReactWrapper';
import { Task } from './Task';
import { ViewMode } from './ViewMode';

interface ModeButtonProps {
  mode: ViewMode;
  activeMode: ViewMode;
  onChangeMode: (mode: ViewMode) => void;
}

const ModeButton: React.FC<ModeButtonProps> = ({
  mode,
  activeMode,
  onChangeMode,
}: ModeButtonProps) => (
  <CustomToggleButton
    variant="flat"
    onClick={(): void => {
      onChangeMode(mode);
    }}
    selected={mode === activeMode}
  >
    {mode}
  </CustomToggleButton>
);

interface GantWrapperProps {
  tasks: Task[];
  defaultViewMode?: ViewMode;
}

const GantWrapper: React.FC<GantWrapperProps> = ({
  tasks,
  defaultViewMode = ViewMode.Day,
}: GantWrapperProps) => {
  const [viewMode, onChangeView] = React.useState(defaultViewMode);
  const taskList = !tasks || !tasks.length ? ([{}] as Task[]) : tasks;

  return (
    <Box sx={{ height: '100%', padding: 1, button: { margin: 0.5 } }}>
      <ModeButton
        mode={ViewMode.Hour}
        onChangeMode={onChangeView}
        activeMode={viewMode}
      />
      <ModeButton
        mode={ViewMode.QuarterDay}
        onChangeMode={onChangeView}
        activeMode={viewMode}
      />
      <ModeButton
        mode={ViewMode.HalfDay}
        onChangeMode={onChangeView}
        activeMode={viewMode}
      />
      <ModeButton
        mode={ViewMode.Day}
        onChangeMode={onChangeView}
        activeMode={viewMode}
      />
      <ModeButton
        mode={ViewMode.Week}
        onChangeMode={onChangeView}
        activeMode={viewMode}
      />
      <ModeButton
        mode={ViewMode.Month}
        onChangeMode={onChangeView}
        activeMode={viewMode}
      />
      <ModeButton
        mode={ViewMode.Year}
        onChangeMode={onChangeView}
        activeMode={viewMode}
      />
      <GanttReactWrapper
        tasks={taskList}
        viewMode={viewMode}
        onClick={(task): void => {
          // eslint-disable-next-line no-console
          console.log('click', task);
        }}
        onDateChange={(task, start, end): void =>
          // eslint-disable-next-line no-console
          console.log('ondate change', task, start, end)
        }
        onProgressChange={(task, progress): void =>
          // eslint-disable-next-line no-console
          console.log('on progress change', task, progress)
        }
        // eslint-disable-next-line no-console
        onTasksChange={(tasks): void => console.log('on tasks change', tasks)}
      />
    </Box>
  );
};

export default GantWrapper;
