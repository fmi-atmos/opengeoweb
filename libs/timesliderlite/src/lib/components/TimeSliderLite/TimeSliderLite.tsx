/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React, { useEffect, useState, ReactNode } from 'react';
import { Divider, Grid2 as Grid, SxProps } from '@mui/material';
import HideButton from '../TimeSliderLiteButtons/HideButton/HideButton';
import ControlButtonGroup from '../TimeSliderLiteButtons/ControlButtonGroup/ControlButtonGroup';

import TimeSliderLiteBackground from '../TimeSliderLiteBackground/TimeSliderLiteBackground';
import {
  boundedValue,
  MINUTE_TO_SECOND,
  MILLISECOND_TO_SECOND,
  TimeSliderLiteCustomSettings,
  floorLocalSeconds,
  SECOND_TO_MILLISECOND,
  HOUR_TO_SECOND,
  useAutoUpdateSelectedTime,
} from './timeSliderLiteUtils';
import StepButton from '../TimeSliderLiteButtons/StepButton/StepButton';
import PlayButton from '../TimeSliderLiteButtons/PlayButton/PlayButton';
import MenuButton from '../TimeSliderLiteButtons/MenuButton/MenuButton';
import { TimeZone } from '../TimeSliderLiteOptionsMenu/TimeZoneSwitch/TimeZoneSwitch';

export interface TimeSliderLiteProps {
  controlButtons?: ReactNode;
  currentTime?: number;
  endTime: number;
  height?: number;
  hideButton?: ReactNode;
  hideMenuButton?: boolean;
  hideStepButtons?: boolean;
  hidePlayButton?: boolean;
  isAnimating?: boolean;
  isVisible?: boolean;
  mapId: string;
  menuOpen?: boolean;
  mouseHoverNeedleOpacity?: number;
  overrideAnimation?: boolean;
  onToggleAnimation?: () => void;
  onToggleMenu: () => void;
  onToggleTimeSlider: () => void;
  selectedTime: number;
  setSelectedTime: (time: number) => void;
  settings?: TimeSliderLiteCustomSettings;
  needleWidth?: number;
  needleDragAreaWidth?: number;
  needleLabelZIndex?: number;
  needleLabelOffset?: [number, number];
  draggableTimeStamp?: boolean;
  startTime: number;
  sx?: SxProps;
  timeStep?: [number, number];
  timeZone?: TimeZone;
  animationSpeed?: number;
  needleDisabled?: boolean;
}

export const TIME_SLIDER_LITE_DEFAULT_HEIGHT = 40;
export const TIME_SLIDER_LITE_DEFAULT_TIME_STEP = 5;
export const TIME_SLIDER_LITE_TOOLTIP_DELAY = 3000;

const styles = {
  container: {
    display: 'grid',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '4px',
    width: '100%',
  },
};

const TimeSliderLite: React.FC<TimeSliderLiteProps> = ({
  controlButtons,
  currentTime = Date.now() * MILLISECOND_TO_SECOND,
  endTime,
  height = TIME_SLIDER_LITE_DEFAULT_HEIGHT,
  hideButton,
  hideMenuButton,
  hideStepButtons,
  hidePlayButton,
  isAnimating,
  isVisible,
  mapId,
  menuOpen,
  mouseHoverNeedleOpacity,
  overrideAnimation,
  onToggleAnimation,
  onToggleMenu,
  onToggleTimeSlider,
  needleWidth,
  needleDragAreaWidth,
  needleLabelZIndex,
  needleLabelOffset,
  draggableTimeStamp,
  selectedTime,
  setSelectedTime,
  settings,
  startTime,
  sx,
  needleDisabled,
  timeStep = [
    TIME_SLIDER_LITE_DEFAULT_TIME_STEP,
    TIME_SLIDER_LITE_DEFAULT_TIME_STEP,
  ],
  timeZone = 'LT',
  animationSpeed = 1,
}) => {
  const [observedTimeStep, forecastTimeStep] = timeStep;
  const timeZoneOffset = timeZone === 'UTC' ? 0 : undefined;

  const [rawSelectedTime, setRawSelectedTime] = useState<number>(currentTime);

  const forecastTimeStepSeconds = forecastTimeStep * MINUTE_TO_SECOND;
  const observedTimeStepSeconds = observedTimeStep * MINUTE_TO_SECOND;
  const lastObservedTime = floorLocalSeconds(
    currentTime,
    observedTimeStepSeconds,
    timeZoneOffset,
  );
  const firstForecastTime =
    floorLocalSeconds(currentTime, forecastTimeStepSeconds, timeZoneOffset) +
    forecastTimeStepSeconds;
  const timeStepSeconds =
    rawSelectedTime >= currentTime
      ? forecastTimeStepSeconds
      : observedTimeStepSeconds;
  const roundedStartTime = floorLocalSeconds(
    startTime +
      Math.max(
        forecastTimeStepSeconds,
        observedTimeStepSeconds,
        HOUR_TO_SECOND,
      ),
    HOUR_TO_SECOND,
    timeZoneOffset,
  );
  const isObservedTimeRange = endTime - currentTime < HOUR_TO_SECOND;
  const roundedEndTime = floorLocalSeconds(
    endTime + HOUR_TO_SECOND,
    HOUR_TO_SECOND,
    timeZoneOffset,
  );

  const autoUpdateSelectedTime = useAutoUpdateSelectedTime(
    lastObservedTime,
    selectedTime,
    setRawSelectedTime,
  );

  const runningExternalAnimation = overrideAnimation && isAnimating;
  const runningInternalAnimation = !overrideAnimation && isAnimating;

  useEffect(() => {
    if (needleDisabled && selectedTime !== lastObservedTime) {
      setRawSelectedTime(lastObservedTime);
      setSelectedTime(lastObservedTime);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedTime, needleDisabled, lastObservedTime]);

  useEffect(() => {
    if (!runningExternalAnimation) {
      const roundedSelectedTime = floorLocalSeconds(
        rawSelectedTime,
        timeStepSeconds,
        timeZoneOffset,
      );
      setSelectedTime(roundedSelectedTime);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rawSelectedTime, timeStepSeconds, runningExternalAnimation]);

  useEffect(() => {
    if (runningInternalAnimation) {
      const interval = setInterval(() => {
        handleTimeStep(timeStepSeconds, true);
      }, SECOND_TO_MILLISECOND / animationSpeed);
      return (): void => clearInterval(interval);
    }
    if (runningExternalAnimation) {
      setRawSelectedTime(selectedTime);
    }
    return (): void => {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [runningInternalAnimation, runningExternalAnimation, selectedTime]);

  const handleTimeStep = (stepSeconds: number, asAnimation?: boolean): void => {
    const newSteppedTime =
      stepSeconds >= 0
        ? floorLocalSeconds(rawSelectedTime, timeStepSeconds, timeZoneOffset) +
          stepSeconds
        : floorLocalSeconds(
            rawSelectedTime - 1,
            timeStepSeconds,
            timeZoneOffset,
          );

    if (asAnimation && newSteppedTime > roundedEndTime + 1) {
      setRawSelectedTime(roundedStartTime);
      return;
    }

    // Handle stepping over current time
    if (
      newSteppedTime >= currentTime &&
      currentTime > rawSelectedTime &&
      !isObservedTimeRange
    ) {
      setRawSelectedTime(firstForecastTime);
    } else if (
      newSteppedTime < currentTime &&
      currentTime <= rawSelectedTime &&
      newSteppedTime >= roundedStartTime
    ) {
      setRawSelectedTime(lastObservedTime);
    } else {
      // Handle normal stepping
      setRawSelectedTime(
        boundedValue(newSteppedTime, [
          roundedStartTime,
          isObservedTimeRange ? lastObservedTime : roundedEndTime,
        ]),
      );
    }

    if (!asAnimation && isAnimating && onToggleAnimation) {
      onToggleAnimation();
    }
  };

  const handleTimeSelect = (value: number): void => {
    setRawSelectedTime(boundedValue(value, [roundedStartTime, roundedEndTime]));
    if (isAnimating && onToggleAnimation) {
      onToggleAnimation();
    }
  };

  // Handle time range selection
  useEffect(() => {
    const userDraggingNeedle =
      rawSelectedTime !== selectedTime || rawSelectedTime === roundedEndTime;

    if (!userDraggingNeedle) {
      const newRawSelectedTime = isObservedTimeRange
        ? lastObservedTime
        : roundedEndTime;
      const isValid = newRawSelectedTime >= roundedStartTime;
      setRawSelectedTime(
        boundedValue(selectedTime, [
          roundedStartTime,
          isValid ? newRawSelectedTime : roundedStartTime,
        ]),
      );
    }

    if (needleDisabled) {
      setRawSelectedTime(selectedTime);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [startTime, endTime, needleDisabled, timeStep]);

  const tooltipDelay = menuOpen ? TIME_SLIDER_LITE_TOOLTIP_DELAY : undefined;

  return (
    <Grid
      container
      sx={{
        ...styles.container,
        ...sx,
        gridTemplateColumns: isVisible ? 'auto 1fr auto' : `1fr auto`,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        boxShadow: isVisible ? (sx as any)?.boxShadow : undefined,
      }}
      data-testid={`timeSliderLite-${mapId}`}
    >
      {isVisible ? (
        <>
          <Grid sx={{ height: `${height}px` }}>
            {controlButtons || (
              <ControlButtonGroup left>
                {!hideMenuButton && (
                  <>
                    <MenuButton
                      settings={settings?.menuButton}
                      menuOpen={menuOpen}
                      onClick={onToggleMenu}
                    />
                    <Divider
                      orientation="vertical"
                      sx={{ height: `${height}px`, margin: '2px' }}
                    />
                  </>
                )}
                {!hidePlayButton && (
                  <PlayButton
                    settings={settings?.playButton}
                    isAnimating={isAnimating}
                    onTogglePlayButton={onToggleAnimation as () => void}
                    tooltipDelay={tooltipDelay}
                    isDisabled={needleDisabled}
                  />
                )}
                {!hideStepButtons && (
                  <>
                    <StepButton
                      settings={settings?.stepButton}
                      onClick={(): void => handleTimeStep(-timeStepSeconds)}
                      tooltipDelay={tooltipDelay}
                      isDisabled={needleDisabled}
                    />
                    <StepButton
                      forward
                      settings={settings?.stepButton}
                      onClick={(): void => handleTimeStep(timeStepSeconds)}
                      tooltipDelay={tooltipDelay}
                      isDisabled={needleDisabled}
                    />
                  </>
                )}
              </ControlButtonGroup>
            )}
          </Grid>
          <Grid>
            <TimeSliderLiteBackground
              draggableTimeStamp={draggableTimeStamp}
              currentTime={currentTime}
              currentTimeSelected={autoUpdateSelectedTime && !isAnimating}
              startTime={roundedStartTime}
              timeZone={timeZone}
              endTime={roundedEndTime}
              disableForecast={isObservedTimeRange}
              mouseHoverNeedleOpacity={mouseHoverNeedleOpacity}
              needleWidth={needleWidth}
              needleDragAreaWidth={needleDragAreaWidth}
              needleLabelZIndex={needleLabelZIndex}
              needleLabelOffset={needleLabelOffset}
              timeStep={[observedTimeStepSeconds, forecastTimeStepSeconds]}
              selectedTime={
                runningExternalAnimation ? selectedTime : rawSelectedTime
              }
              setSelectedTime={handleTimeSelect}
              resetSelectedTime={() => setRawSelectedTime(lastObservedTime)}
              height={height}
              zerorange={needleDisabled}
            />
          </Grid>
        </>
      ) : (
        <Grid />
      )}
      <Grid
        sx={{
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          boxShadow: isVisible ? 'none' : (sx as any)?.boxShadow,
          borderRadius: isVisible ? 'none' : '4px',
          height: `${height}px`,
        }}
      >
        {hideButton || (
          <ControlButtonGroup
            className={`timeSliderLite-hideButtonGroup-${
              isVisible ? 'hide' : 'show'
            }`}
            right={isVisible}
          >
            <HideButton
              settings={settings?.hideButton}
              onClick={onToggleTimeSlider}
              isVisible={isVisible}
            />
          </ControlButtonGroup>
        )}
      </Grid>
    </Grid>
  );
};

export default TimeSliderLite;
