/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  CardContent,
  FormControlLabel,
  Grid2 as Grid,
  Radio,
  RadioGroup,
} from '@mui/material';
import React, { useEffect } from 'react';
import TimeStepSelector from './TimeStepSelector';
import SubmenuSelectLabel from '../SubmenuSelectLabel';
import { useTimesliderLiteTranslation } from '../../../locales/i18n';

export interface TimeStepOptionsProps {
  useTimeStep?: [
    [number, number] | null,
    (timeStep: [number, number] | null) => void,
  ];
  defaultTimeStep: [number, number];
  timeStepOptions?: Record<string, string>;
  dropdownButtonIcon?: React.ElementType;
}

const TimeStepOptions: React.FC<TimeStepOptionsProps> = ({
  useTimeStep,
  defaultTimeStep = [5, 15],
  timeStepOptions,
  dropdownButtonIcon,
}) => {
  const { t } = useTimesliderLiteTranslation();
  const isReadOnly = useTimeStep === undefined;
  const timeStep = useTimeStep?.[0];
  const setTimeStep = useTimeStep?.[1];
  const observedTimeStepProp = timeStep?.[0];
  const forecastTimeStepProp = timeStep?.[1];
  const isDefault = !useTimeStep || timeStep === null;

  const defaultObservedTimeStep = defaultTimeStep[0];
  const defaultForecastTimeStep = defaultTimeStep[1];
  const [observedTimeStep, setObservedTimeStep] = React.useState<number>(
    observedTimeStepProp ?? defaultObservedTimeStep,
  );
  const [forecastTimeStep, setForecastTimeStep] = React.useState<number>(
    forecastTimeStepProp ?? defaultForecastTimeStep,
  );

  useEffect(() => {
    if (!isDefault && setTimeStep) {
      setTimeStep([observedTimeStep, forecastTimeStep]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [observedTimeStep, forecastTimeStep]);

  const handleIsDefaultChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    if (event.target.value === 'default') {
      setTimeStep && setTimeStep(null);
      setObservedTimeStep(defaultTimeStep[0]);
      setForecastTimeStep(defaultTimeStep[1]);
    }
    if (event.target.value === 'custom' && setTimeStep) {
      setTimeStep(defaultTimeStep);
    }
  };

  return (
    <CardContent
      data-testid="TimeSliderLite-timeStepOptions"
      sx={{
        paddingTop: '4px',
        '& label': {
          height: '36px',
        },
        '&:last-child': {
          paddingBottom: '16px',
        },
      }}
    >
      <RadioGroup
        defaultValue={isDefault ? 'default' : 'custom'}
        name="time-range-radio-button-group"
        row
        sx={{ marginTop: 1 }}
        value={isDefault ? 'default' : 'custom'}
        onChange={handleIsDefaultChange}
      >
        <FormControlLabel
          disabled={isReadOnly}
          value="default"
          control={<Radio />}
          label={t('common.default')}
        />
        <FormControlLabel
          disabled={isReadOnly}
          value="custom"
          control={<Radio />}
          label={t('common.custom')}
        />
      </RadioGroup>
      <Grid container width={250}>
        <Grid
          size={{
            xs: 4,
          }}
          sx={{ marginY: 1 }}
        >
          <SubmenuSelectLabel isDisabled={isDefault}>
            {t('common.observed')}
          </SubmenuSelectLabel>
        </Grid>
        <Grid
          size={{
            xs: 8,
          }}
          paddingX="4px"
          sx={{ marginY: 1 }}
        >
          <TimeStepSelector
            disabled={isDefault}
            value={observedTimeStep}
            setValue={setObservedTimeStep}
            defaultTimeStep={defaultTimeStep[0]}
            dropdownButtonIcon={dropdownButtonIcon}
            timeStepOptions={timeStepOptions}
          />
        </Grid>
        <Grid
          size={{
            xs: 4,
          }}
        >
          <SubmenuSelectLabel isDisabled={isDefault}>
            {t('common.forecast')}
          </SubmenuSelectLabel>
        </Grid>
        <Grid
          size={{
            xs: 8,
          }}
          paddingX="4px"
        >
          <TimeStepSelector
            disabled={isDefault}
            value={forecastTimeStep}
            setValue={setForecastTimeStep}
            defaultTimeStep={defaultTimeStep[1]}
            dropdownButtonIcon={dropdownButtonIcon}
            timeStepOptions={timeStepOptions}
          />
        </Grid>
      </Grid>
    </CardContent>
  );
};

export default TimeStepOptions;
