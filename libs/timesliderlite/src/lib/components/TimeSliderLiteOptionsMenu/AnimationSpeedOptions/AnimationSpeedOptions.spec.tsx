/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { speedFactors, SpeedFactorType } from '@opengeoweb/timeslider';
import AnimationSpeedOptions, {
  AnimationSpeedSelectorProps,
} from './AnimationSpeedOptions';
import { ThemeI18nProvider } from '../../../testUtils/Providers';

describe('components/TimeSliderLiteOptionsMenu/AnimationSpeedOptions', () => {
  const defaultProps: AnimationSpeedSelectorProps = {
    defaultSpeedFactor: 4,
    useAnimationSpeed: [2, jest.fn()],
  };

  const user = userEvent.setup();

  const getSpeedSelectListBox = async (): Promise<HTMLElement> => {
    const selectButton = within(
      screen.getByTestId(`TimeSliderLite-animationSpeedSelect`),
    ).getByRole('combobox', { hidden: true });
    await user.click(selectButton);
    return within(screen.getByRole('presentation')).getByRole('listbox');
  };

  it('should render with selected animationSpeed', () => {
    const selectedanimationSpeedDefaultLabel = 'x 2';
    render(
      <ThemeI18nProvider>
        <AnimationSpeedOptions {...defaultProps} />
      </ThemeI18nProvider>,
    );

    const select = screen.getByTestId('TimeSliderLite-animationSpeedSelect');
    expect(select).toBeInTheDocument();

    expect(
      screen.getByText(selectedanimationSpeedDefaultLabel),
    ).toBeInTheDocument();
  });

  it('should render using dropdownButtonIcon', () => {
    const Icon: React.FC = () => <div>Test icon</div>;
    render(
      <ThemeI18nProvider>
        <AnimationSpeedOptions {...defaultProps} dropdownButtonIcon={Icon} />
      </ThemeI18nProvider>,
    );

    expect(screen.getByText('Test icon')).toBeInTheDocument();
  });

  test.each(speedFactors)(
    'should render x %s as default default selectable option',
    async (speedFactor) => {
      const { defaultSpeedFactor } = defaultProps;
      const speedFactorLabel =
        speedFactor !== defaultSpeedFactor
          ? `x ${speedFactor}`
          : `x ${speedFactor} (default)`;
      const initialSpeedFactor =
        speedFactor !== defaultSpeedFactor
          ? defaultSpeedFactor
          : speedFactors[0];

      render(
        <ThemeI18nProvider>
          <AnimationSpeedOptions
            {...defaultProps}
            useAnimationSpeed={[initialSpeedFactor as number, jest.fn()]}
          />
        </ThemeI18nProvider>,
      );

      const speedOptionListBox = await getSpeedSelectListBox();

      await user.click(
        // eslint-disable-next-line testing-library/no-node-access
        within(speedOptionListBox).getByText(speedFactorLabel).parentElement!,
      );
      expect(screen.getByText(speedFactorLabel)).toBeTruthy();
    },
  );

  it('should render custom options', async () => {
    const speedFactorsWithout3x: SpeedFactorType[] = [2, 4];

    render(
      <ThemeI18nProvider>
        <AnimationSpeedOptions
          {...defaultProps}
          speedOptions={speedFactorsWithout3x}
        />
      </ThemeI18nProvider>,
    );

    const speedOptionListBox = await getSpeedSelectListBox();

    expect(within(speedOptionListBox).queryByText('x 3')).toBeNull();

    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      within(speedOptionListBox).getByText('x 4 (default)').parentElement!,
    );
    expect(screen.getByText('x 4 (default)')).toBeTruthy();
  });

  it('should render custom prefix', async () => {
    const expectedOptionLabel = 'speed 4 (default)';
    render(
      <ThemeI18nProvider>
        <AnimationSpeedOptions
          {...defaultProps}
          speedOptionItemPrefix="speed "
        />
      </ThemeI18nProvider>,
    );

    const speedOptionListBox = await getSpeedSelectListBox();

    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      within(speedOptionListBox).getByText(expectedOptionLabel).parentElement!,
    );
    expect(screen.getByText(expectedOptionLabel)).toBeTruthy();
  });

  it('should render default custom postfix', async () => {
    const expectedOptionLabel = 'x 4 speed';
    render(
      <ThemeI18nProvider>
        <AnimationSpeedOptions
          {...defaultProps}
          defaultSpeedOptionPostfix=" speed"
        />
      </ThemeI18nProvider>,
    );

    const speedOptionListBox = await getSpeedSelectListBox();

    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      within(speedOptionListBox).getByText(expectedOptionLabel).parentElement!,
    );
    expect(screen.getByText(expectedOptionLabel)).toBeTruthy();
  });

  it('should render custom postfix', async () => {
    const expectedOptionLabel = 'x 2 speed';
    render(
      <ThemeI18nProvider>
        <AnimationSpeedOptions
          {...defaultProps}
          useAnimationSpeed={[4, jest.fn()]}
          speedOptionItemPostfix=" speed"
        />
      </ThemeI18nProvider>,
    );

    const speedOptionListBox = await getSpeedSelectListBox();

    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      within(speedOptionListBox).getByText(expectedOptionLabel).parentElement!,
    );
    expect(screen.getByText(expectedOptionLabel)).toBeTruthy();
  });

  it('should render disabled if readonly', () => {
    render(
      <ThemeI18nProvider>
        <AnimationSpeedOptions {...defaultProps} useAnimationSpeed={[2]} />
      </ThemeI18nProvider>,
    );

    const selectInputWithoutSetter = screen.getByTestId(
      'TimeSliderLite-animationSpeedSelect-input',
    );
    expect(selectInputWithoutSetter).toBeDisabled();
  });

  it('should render disabled if only one option', () => {
    render(
      <ThemeI18nProvider>
        <AnimationSpeedOptions
          {...defaultProps}
          speedOptions={[4]}
          useAnimationSpeed={[4, jest.fn()]}
        />
      </ThemeI18nProvider>,
    );
    const selectInputWithoutOptions = screen.getByTestId(
      'TimeSliderLite-animationSpeedSelect-input',
    );
    expect(selectInputWithoutOptions).toBeDisabled();
  });
});
