/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { ToggleButton, ToggleButtonGroup } from '@mui/material';
import React from 'react';
import { useTimesliderLiteTranslation } from '../../../locales/i18n';

const availableTimeZones = ['LT', 'UTC'] as const;
export type TimeZone = (typeof availableTimeZones)[number];

export interface TimeZoneSwitchProps {
  useTimeZone?: [TimeZone, (timeZone: TimeZone) => void] | [TimeZone];
}

const styles = {
  root: {
    marginY: 0.5,
    marginRight: 1,
    '> button': {
      borderRadius: 10,
      height: '36px',
      textTransform: 'none',
    },
    '> button:first-of-type': {
      paddingRight: 1,
    },
    '> button:last-of-type': {
      paddingLeft: 1,
    },
  },
};

const TimeZoneSwitch: React.FC<TimeZoneSwitchProps> = ({
  useTimeZone: [timeZone, setTimeZone] = ['LT', undefined],
}) => {
  const { t } = useTimesliderLiteTranslation();
  return (
    <ToggleButtonGroup
      disabled={!setTimeZone}
      className="TimeSliderLite-timeZoneSwitch"
      data-testid="TimeSliderLite-timeZoneSwitch"
      value={timeZone ?? 'LT'}
      exclusive
      sx={styles.root}
      onChange={(
        _event: React.MouseEvent<HTMLElement>,
        newZone: TimeZone,
      ): void => {
        if (newZone && setTimeZone) {
          setTimeZone(newZone);
        }
      }}
    >
      {availableTimeZones.map((zone: TimeZone) => (
        <ToggleButton value={zone} key={zone}>
          {t(`menu.${zone}`)}
        </ToggleButton>
      ))}
    </ToggleButtonGroup>
  );
};

export default TimeZoneSwitch;
