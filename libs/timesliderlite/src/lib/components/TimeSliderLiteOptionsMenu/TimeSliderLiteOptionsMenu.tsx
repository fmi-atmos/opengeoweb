/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  Box,
  Divider,
  IconButton,
  SxProps,
  ToggleButton,
  ToggleButtonGroup,
} from '@mui/material';
import { Close } from '@opengeoweb/theme';
import React from 'react';
import SubmenuWrapper from './SubmenuWrapper';
import { MILLISECOND_TO_SECOND } from '../TimeSliderLite/timeSliderLiteUtils';
import TimeStepOptions, {
  TimeStepOptionsProps,
} from './TimeStepOptions/TimeStepOptions';
import TimeRangeOptions, {
  TimeRangeOptionsProps,
} from './TimeRangeOptions/TimeRangeOptions';
import AnimationSpeedOptions, {
  AnimationSpeedSelectorProps,
} from './AnimationSpeedOptions/AnimationSpeedOptions';
import UpdateIntervalOptions, {
  UpdateIntervalOptionsProps,
} from './UpdateIntervalOptions/UpdateIntervalOptions';
import DefaultTimeZoneSwitch, {
  TimeZoneSwitchProps,
} from './TimeZoneSwitch/TimeZoneSwitch';
import { useTimesliderLiteTranslation } from '../../locales/i18n';

export interface TimeSliderLiteOptionsMenuProps
  extends TimeStepOptionsProps,
    TimeRangeOptionsProps,
    AnimationSpeedSelectorProps,
    UpdateIntervalOptionsProps,
    TimeZoneSwitchProps {
  closeButtonIcon?: React.ReactNode;
  defaultTimeStep: [number, number];
  menuOpen?: boolean;
  setMenuOpen?: (menuOpen: boolean) => void;
  subMenuSx?: SxProps;
  sx?: SxProps;
  TimeZoneSwitch?: React.FC<TimeZoneSwitchProps>;
}

const styles = {
  button: {
    height: '36px',
    lineHeight: '1em',
    border: 'none',
    textTransform: 'none',
    borderRadius: '2px !important',
    color: 'geowebColors.typographyAndIcons.text',
    '&.Mui-selected': {
      backgroundColor: 'geowebColors.timeSliderLite.selected.backgroundColor',
    },
    '&:disabled': {
      border: 'none',
    },
  },
};

// TODO: Add translations
export enum SubmenuTitleTranslations {
  TIME_RANGE = 'menu.timeRange',
  TIME_STEP = 'menu.timeStep',
  SPEED = 'menu.speed',
  UPDATE_INTERVAL = 'menu.updateInterval',
}

const TimeSliderLiteOptionsMenu: React.FC<TimeSliderLiteOptionsMenuProps> = ({
  closeButtonIcon,
  currentTime = Date.now().valueOf() * MILLISECOND_TO_SECOND,
  defaultTimeRange,
  defaultTimeStep,
  defaultSpeedFactor,
  defaultUpdateInterval,
  dropdownButtonIcon,
  menuOpen,
  setMenuOpen,
  speedOptionItemPostfix,
  speedOptionItemPrefix,
  speedOptions,
  updateIntervalOptions,
  subMenuSx,
  sx,
  TimeZoneSwitch = DefaultTimeZoneSwitch,
  timeStepOptions,
  useAnimationSpeed,
  useTimeRange,
  useTimeStep,
  useTimeZone,
  useUpdateInterval,
}) => {
  const { t } = useTimesliderLiteTranslation();
  const [submenu, setSubmenu] = React.useState<string | null>(null);
  const [submenuLeft, setSubmenuLeft] = React.useState<number>(0);

  return (
    <>
      {submenu && (
        <SubmenuWrapper
          sx={{
            left: submenuLeft,
            marginLeft: '-12px',
            ...subMenuSx,
          }}
          setSubmenu={setSubmenu}
          title={t(`${submenu}.title`)}
          closeButtonIcon={closeButtonIcon}
        >
          {submenu === SubmenuTitleTranslations.TIME_RANGE && (
            <TimeRangeOptions
              currentTime={currentTime}
              useTimeRange={useTimeRange}
              defaultTimeRange={defaultTimeRange}
              dropdownButtonIcon={dropdownButtonIcon}
            />
          )}
          {submenu === SubmenuTitleTranslations.TIME_STEP && (
            <TimeStepOptions
              defaultTimeStep={defaultTimeStep}
              useTimeStep={useTimeStep}
              dropdownButtonIcon={dropdownButtonIcon}
              timeStepOptions={timeStepOptions}
            />
          )}
          {submenu === SubmenuTitleTranslations.SPEED && (
            <AnimationSpeedOptions
              useAnimationSpeed={useAnimationSpeed}
              dropdownButtonIcon={dropdownButtonIcon}
              speedOptionItemPostfix={speedOptionItemPostfix}
              speedOptionItemPrefix={speedOptionItemPrefix}
              speedOptions={speedOptions}
              defaultSpeedFactor={defaultSpeedFactor}
            />
          )}
          {submenu === SubmenuTitleTranslations.UPDATE_INTERVAL && (
            <UpdateIntervalOptions
              defaultUpdateInterval={defaultUpdateInterval}
              dropdownButtonIcon={dropdownButtonIcon}
              updateIntervalOptions={updateIntervalOptions}
              useUpdateInterval={useUpdateInterval}
            />
          )}
        </SubmenuWrapper>
      )}
      <Box
        sx={{
          backgroundColor: 'geowebColors.background.surface',
          borderRadius: 1,
          display: 'flex',
          flexDirection: 'row',
          marginY: 1,
          visibility: menuOpen ? 'visible' : 'hidden',
          ...sx,
        }}
      >
        <ToggleButtonGroup
          data-testid="TimeSliderLite-optionsMenu"
          value={submenu}
          exclusive
          sx={{
            padding: 0.5,
            gap: 0.5,
          }}
          onChange={(
            event: React.MouseEvent<HTMLElement>,
            newOption: SubmenuTitleTranslations | null,
          ): void => {
            setSubmenu(newOption);
            const button = event.currentTarget;
            const buttonPosition = button.getBoundingClientRect();
            setSubmenuLeft(buttonPosition.x);
          }}
        >
          {Object.values(SubmenuTitleTranslations).map((value) => (
            <ToggleButton value={value} key={value} sx={styles.button}>
              {t(`${value}.button`)}
            </ToggleButton>
          ))}
        </ToggleButtonGroup>
        <TimeZoneSwitch useTimeZone={useTimeZone ?? ['LT']} />
        <Divider orientation="vertical" flexItem />
        <IconButton
          data-testid="TimeSliderLite-optionsMenu-closeButton"
          size="small"
          sx={{ margin: 0.5 }}
          onClick={(): void => setMenuOpen && setMenuOpen(false)}
        >
          {closeButtonIcon ?? <Close />}
        </IconButton>
      </Box>
    </>
  );
};

export default TimeSliderLiteOptionsMenu;
