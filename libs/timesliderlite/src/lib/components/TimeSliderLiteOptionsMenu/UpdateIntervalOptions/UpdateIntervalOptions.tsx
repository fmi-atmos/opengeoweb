/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  CardContent,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import React from 'react';
import { useTimesliderLiteTranslation } from '../../../locales/i18n';

export interface UpdateIntervalOptionsProps {
  dropdownButtonIcon?: React.ElementType;
  updateIntervalOptions?: number[];
  defaultUpdateInterval?: number;
  useUpdateInterval?: [
    number | null,
    ((updateInterval: number | null) => void) | undefined,
  ];
  intervalOptionPrefix?: string;
  intervalOptionPostfix?: string;
  defaultOptionPostfix?: string;
}

const defaultUpdateIntervalOptions = [1, 2, 5, 10, 15, 30, 60];

const UpdateIntervalOptions: React.FC<UpdateIntervalOptionsProps> = ({
  defaultUpdateInterval = 1,
  dropdownButtonIcon,
  intervalOptionPrefix = '',
  intervalOptionPostfix,
  defaultOptionPostfix,
  updateIntervalOptions = defaultUpdateIntervalOptions.sort((a, b) => a - b),
  useUpdateInterval,
}) => {
  const { t } = useTimesliderLiteTranslation();
  const translatedIntervalOptionPostfix =
    intervalOptionPostfix ?? ` ${t('units.minutes')}`;
  const translatedDefaultOptionPostfix =
    defaultOptionPostfix ??
    ` ${t('units.minutes')} (${t('common.default').toLowerCase()})`;

  const updateInterval = useUpdateInterval?.[0] ?? defaultUpdateInterval;
  const setUpdateInterval = useUpdateInterval?.[1];
  const readOnly = !setUpdateInterval || updateIntervalOptions.length < 2;
  const defaultValue =
    updateIntervalOptions.find((option) => option === defaultUpdateInterval) ??
    Math.min(...updateIntervalOptions);

  return (
    <CardContent
      className="TimeSliderLite-updateIntervalOptions"
      data-testid="TimeSliderLite-updateIntervalOptions"
      sx={{
        paddingTop: '12px',
        marginBottom: '-8px',
        minWidth: '200px',
      }}
    >
      <Select
        disabled={readOnly}
        className="TimeSliderLite-updateIntervalSelect"
        data-testid="TimeSliderLite-updateIntervalSelect"
        inputProps={{
          'data-testid': 'TimeSliderLite-updateIntervalSelect-input',
        }}
        sx={{ height: '40px' }}
        value={String(updateInterval ?? defaultValue)}
        onChange={(event: SelectChangeEvent): void => {
          const newValue = Number(event?.target?.value);
          setUpdateInterval &&
            setUpdateInterval(newValue === defaultValue ? null : newValue);
        }}
        IconComponent={dropdownButtonIcon}
      >
        {updateIntervalOptions.map((option) => {
          const label =
            option === defaultValue
              ? `${intervalOptionPrefix}${option}${translatedDefaultOptionPostfix}`
              : `${intervalOptionPrefix}${option}${translatedIntervalOptionPostfix}`;
          return (
            <MenuItem key={String(option)} value={String(option)}>
              {label}
            </MenuItem>
          );
        })}
      </Select>
    </CardContent>
  );
};

export default UpdateIntervalOptions;
