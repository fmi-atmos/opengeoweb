/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Box, useTheme } from '@mui/material';
import React from 'react';
import {
  reformatFinnishDateString,
  MILLISECOND_TO_SECOND,
  floorLocalSeconds,
  shortUTCTime,
} from '../TimeSliderLite/timeSliderLiteUtils';
import { TimeZone } from '../TimeSliderLiteOptionsMenu/TimeZoneSwitch/TimeZoneSwitch';
import { useTimesliderLiteTranslation } from '../../locales/i18n';

const styles = {
  centeredBox: {
    borderRadius: '4px',
    bottom: 'calc(50% - 0.5rem - 2px)',
    left: '50%',
    padding: '0px 3px 0px 3px',
    position: 'absolute',
    transform: 'translateX(-50%)',
  },
} as const;

interface Props {
  timeZone?: TimeZone;
  currentTime?: number;
  timeStep: [number, number];
  timestampZIndex?: number;
}

const TimeSliderZeroTimestamp: React.FC<Props> = ({
  timeZone = 'LT',
  currentTime = Date.now() * MILLISECOND_TO_SECOND,
  timeStep,
  timestampZIndex,
}: Props) => {
  const { i18n } = useTimesliderLiteTranslation();
  const { language } = i18n;
  const {
    palette: {
      geowebColors: { timeSliderLite },
    },
  } = useTheme();

  const observedTimeStepSeconds = timeStep[0];
  const lastObservedTime = floorLocalSeconds(
    currentTime,
    observedTimeStepSeconds,
  );

  const dateString = new Date(lastObservedTime * 1000).toLocaleDateString(
    language,
    {
      weekday: 'short',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      timeZone: timeZone === 'UTC' ? 'UTC' : undefined,
    },
  );

  const localLabel =
    language === 'fi' || language === 'sv-FI'
      ? reformatFinnishDateString(dateString)
      : dateString;
  const label = timeZone === 'UTC' ? shortUTCTime(localLabel) : localLabel;
  return (
    <Box
      className="TimeSliderLite-zeroTimeStamp"
      sx={{
        ...styles.centeredBox,
        ...timeSliderLite.needleLabel,
        fontSize: '13px',
        opacity: 0.6,
        pointerEvents: null,
        zIndex: timestampZIndex ?? null,
      }}
    >
      {label}
    </Box>
  );
};

export default TimeSliderZeroTimestamp;
