/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Box, useTheme } from '@mui/material';
import React, { useRef } from 'react';
import Draggable from 'react-draggable';
import {
  reformatFinnishDateString,
  getSelectedTimeFromPx,
  getSelectedTimePx,
  MILLISECOND_TO_SECOND,
  floorLocalSeconds,
  shortUTCTime,
} from '../TimeSliderLite/timeSliderLiteUtils';
import { TimeZone } from '../TimeSliderLiteOptionsMenu/TimeZoneSwitch/TimeZoneSwitch';
import { useTimesliderLiteTranslation } from '../../locales/i18n';

interface Props {
  draggableLabel?: boolean;
  width: number;
  height: number;
  timeZone?: TimeZone;
  startTime: number;
  endTime: number;
  disableForecast?: boolean;
  currentTime?: number;
  currentTimeSelected?: boolean;
  needleDragAreaWidth?: number;
  needleLabelZIndex?: number;
  needleLabelOffset?: [number, number];
  needleWidth?: number;
  timeStep: [number, number];
  selectedTime: number;
  style: React.CSSProperties;
  onDoubleClick?: React.MouseEventHandler<HTMLDivElement>;
  setSelectedTime: (time: number) => void;
}

interface NeedleProps {
  height: number;
  stroke: string;
  x: number;
  width: number;
}

export const DRAG_AREA_WIDTH = 10;
const NEEDLE_WIDTH = 5;
const SHADOW_SPREAD = 2;

const NeedleShadow: React.FC<NeedleProps> = ({ x, stroke, height, width }) => (
  <line
    x1={x}
    y1={height - (height - SHADOW_SPREAD / 2)}
    x2={x}
    y2={height - SHADOW_SPREAD / 2}
    strokeWidth={width + SHADOW_SPREAD}
    stroke={stroke}
    strokeLinecap="round"
    filter="blur(0.5px)"
    data-testid="TimeSliderLite-NeedleShadow"
  />
);

const NeedleLine: React.FC<NeedleProps> = ({ x, stroke, height, width }) => (
  <line
    x1={x}
    y1={width / 2}
    x2={x}
    y2={height - width / 2}
    strokeWidth={width}
    strokeLinecap="round"
    stroke={stroke}
    data-testid="TimeSliderLite-NeedleLine"
  />
);

const TimeSliderDraggableNeedle: React.FC<Props> = ({
  draggableLabel,
  width,
  height,
  timeZone = 'LT',
  currentTime = Date.now() * MILLISECOND_TO_SECOND,
  currentTimeSelected,
  startTime,
  endTime,
  disableForecast,
  needleDragAreaWidth = DRAG_AREA_WIDTH,
  needleLabelZIndex,
  needleLabelOffset = [0, -5],
  needleWidth = NEEDLE_WIDTH,
  timeStep,
  selectedTime,
  setSelectedTime,
  style,
  onDoubleClick,
}: Props) => {
  const { i18n } = useTimesliderLiteTranslation();
  const { language } = i18n;
  const draggableRef = useRef(null);
  const {
    palette: {
      geowebColors: { timeSliderLite },
    },
  } = useTheme();
  const timeZoneOffset = timeZone === 'UTC' ? 0 : undefined;

  const observedTimeStepSeconds = timeStep[0];
  const forecastTimeStepSeconds = timeStep[1];
  const timeStepSeconds =
    selectedTime >= currentTime
      ? forecastTimeStepSeconds
      : observedTimeStepSeconds;
  const lastObservedTime = floorLocalSeconds(
    currentTime,
    observedTimeStepSeconds,
    timeZoneOffset,
  );
  const firstForecastTime =
    floorLocalSeconds(currentTime, forecastTimeStepSeconds, timeZoneOffset) +
    forecastTimeStepSeconds;

  const selectedTimePx = getSelectedTimePx(
    startTime,
    endTime,
    selectedTime,
    width,
  );
  const x =
    selectedTimePx > 0
      ? selectedTimePx - needleDragAreaWidth / 2
      : -needleDragAreaWidth / 2;

  const isBetweenObservedAndForecast =
    selectedTime >= lastObservedTime && selectedTime < firstForecastTime;
  const roundedSelectedTime = isBetweenObservedAndForecast
    ? lastObservedTime
    : floorLocalSeconds(selectedTime, timeStepSeconds, timeZoneOffset);
  const dateString = new Date(roundedSelectedTime * 1000).toLocaleDateString(
    language,
    {
      weekday: 'short',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      timeZone: timeZone === 'UTC' ? 'UTC' : undefined,
    },
  );

  const localLabel =
    language === 'fi' || language === 'sv-FI'
      ? reformatFinnishDateString(dateString)
      : dateString;
  const label = timeZone === 'UTC' ? shortUTCTime(localLabel) : localLabel;

  return (
    <Draggable
      handle=".TimeSliderLite-DraggableNeedle"
      axis="x"
      nodeRef={draggableRef}
      bounds={{
        left: -needleDragAreaWidth / 2,
        right: width - needleDragAreaWidth / 2,
      }}
      position={{
        x,
        y: 0,
      }}
      onDrag={(_, position): void => {
        const positionOffsetToCompensateFlooring = 0.5;
        const needleCenterPosition =
          position.x +
          needleDragAreaWidth / 2 +
          positionOffsetToCompensateFlooring;
        const newSelectedTime = getSelectedTimeFromPx(
          startTime,
          endTime,
          needleCenterPosition,
          width,
        );
        if (disableForecast && newSelectedTime >= lastObservedTime) {
          setSelectedTime(lastObservedTime);
        }
        setSelectedTime(newSelectedTime);
      }}
      onStop={(_, position): void => {
        const positionOffsetToCompensateFlooring = 0.5;
        const needleCenterPosition =
          position.x +
          needleDragAreaWidth / 2 +
          positionOffsetToCompensateFlooring;
        const newSelectedTime = getSelectedTimeFromPx(
          startTime,
          endTime,
          needleCenterPosition,
          width,
        );
        const isLastObservedTime =
          (newSelectedTime >= lastObservedTime &&
            newSelectedTime < firstForecastTime) ||
          (newSelectedTime >= lastObservedTime && disableForecast);
        const roundedSelectedTime = isLastObservedTime
          ? lastObservedTime
          : floorLocalSeconds(newSelectedTime, timeStepSeconds, timeZoneOffset);
        setSelectedTime(roundedSelectedTime);
      }}
    >
      {/* Draggable ref is div for safari support */}
      <Box
        className="TimeSliderLite-DraggableNeedle"
        ref={draggableRef}
        data-testid="TimeSliderLite-DraggableNeedle"
        onDoubleClick={onDoubleClick}
        sx={{
          overflow: 'visible',
        }}
      >
        <svg
          style={style}
          cursor="ew-resize"
          width={needleDragAreaWidth}
          height={height}
          data-testid="TimeSliderLite-NeedleSvg"
        >
          <NeedleShadow
            x={needleDragAreaWidth / 2}
            stroke={timeSliderLite.needleShadow.stroke as string}
            height={height}
            width={needleWidth}
          />
          <NeedleLine
            x={needleDragAreaWidth / 2}
            stroke={
              currentTimeSelected
                ? (timeSliderLite.needleDefault.stroke as string)
                : (timeSliderLite.needle.stroke as string)
            }
            height={height}
            width={needleWidth}
          />
        </svg>
        <Box
          className="TimeSliderLite-timeStamp"
          position="absolute"
          bottom="-1px"
          sx={{
            backgroundColor: timeSliderLite.needleLabel.backgroundColor,
            border: `1px solid ${timeSliderLite.needleLabel.borderColor}`,
            borderRadius: '4px',
            className: draggableLabel ? 'TimeSliderLite-DraggableNeedle' : '',
            color: timeSliderLite.needleLabel.color,
            cursor: draggableLabel ? 'ew-resize' : null,
            fontSize: '12px',
            padding: '0px 4px 0px 4px',
            pointerEvents: draggableLabel ? null : 'none',
            transform: `translate(calc(-50% + ${needleWidth}px + ${needleLabelOffset[0]}px), ${needleLabelOffset[1]}px)`,
            zIndex: needleLabelZIndex ?? null,
          }}
        >
          {label}
          <Box
            className="TimeSliderLite-timeStamp-tail"
            position="absolute"
            width="10px"
            height="10px"
            bottom="-4px"
            sx={{
              backgroundColor: timeSliderLite.needleLabel.backgroundColor,
              transform: 'rotate(45deg)',
              left: `calc(50% - 5px - ${needleLabelOffset[0]}px)`,
              zIndex: -1,
            }}
          />
        </Box>
      </Box>
    </Draggable>
  );
};

export default TimeSliderDraggableNeedle;
