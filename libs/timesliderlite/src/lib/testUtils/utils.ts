/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { createToolkitMockStore } from '@opengeoweb/shared';
import {
  WebMapStateModuleState,
  storeMiddlewares,
  storeReducerMap,
  syncGroupsTypes,
  uiTypes,
} from '@opengeoweb/store';
import { Store } from '@reduxjs/toolkit';

const reducer = {
  ...storeReducerMap,
};

export const createMockStore = (
  mockState?: WebMapStateModuleState &
    syncGroupsTypes.SynchronizationGroupModuleState &
    uiTypes.UIModuleState,
): Store =>
  createToolkitMockStore({
    reducer,
    preloadedState: mockState,
    middleware: [...storeMiddlewares],
  });
