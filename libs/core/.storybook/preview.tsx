/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2021 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';

import type { Preview } from '@storybook/react';
import { initialize as mswInitialize, mswLoader } from 'msw-storybook-addon';

import * as previewHelpers from '../../../.storybook/preview';
import './style.css';
import { locationApiEndpoints } from '../src/lib/utils/location-api/fakeApi';

const preview: Preview = {
  decorators: [
    (Story) => {
      document.body.className += ' overflow-hiddden';
      return <Story />;
    },
  ],
};

const parameters = {
  ...previewHelpers.parameters,
  msw: {
    handlers: {
      locations: locationApiEndpoints,
    },
  },
};

mswInitialize({
  serviceWorker: {
    url: './mockServiceWorker.js',
  },
});

const loaders = [mswLoader];
export { parameters, loaders };

export default preview;
