![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/core/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-core)

# Core

React component library with Core components for the opengeoweb project.
This library was generated with [Nx](https://nx.dev).

## Installation

```
npm install @opengeoweb/core @opengeoweb/store @opengeoweb/timeslider
```

## Use

You can use any component exported from core by importing them. Some components need to be wrapped in the CoreThemeProvider, or CoreThemeStoreProvider for a connected component. You can also pass in your own theme if needed. Below you can find a simplified example on how the MapViewConnect component and several mapActions could be used:

```javascript
import { useDispatch } from 'react-redux';
import {
  MapViewConnect,
  LegendConnect,
  LegendMapButtonConnect,
  CoreThemeStoreProvider,
} from '@opengeoweb/core';
import { storeMiddlewares, storeReducerMap } from '@opengeoweb/store';
import { TimeSliderConnect } from '@opengeoweb/timeslider';
import { configureStore } from '@reduxjs/toolkit';

const store = configureStore({
  reducer: {
    ...storeReducerMap,
  },
  middleware: [...storeMiddlewares],
});

const ConnectedMapWithTimeSlider = ({ mapId }) => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    // set layers
    dispatch(mapActions.setLayers({ layers: [radarLayer], mapId }));
    // baseLayers
    dispatch(
      mapActions.setBaseLayers({
        mapId,
        layers: [baseLayerGrey, overLayer],
      }),
    );
  }, []);

  return (
    <CoreThemeStoreProvider store={store}>
      <LegendConnect initialActiveMapId={mapId} />
      <LegendMapButtonConnect mapId={mapId} />
      <MapViewConnect mapId={mapId} displayTimeInMap />
      <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
    </CoreThemeStoreProvider>
  );
};

export default ConnectedMapWithTimeSlider;
```

### TypeScript Documentation

- [TypeScript Docs](https://opengeoweb.gitlab.io/opengeoweb/typescript-docs/core/)
