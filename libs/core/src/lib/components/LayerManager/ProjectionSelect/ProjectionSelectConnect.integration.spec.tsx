/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { createMockStore, mapActions } from '@opengeoweb/store';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockGetCapabilities,
  setWMSGetCapabilitiesFetcher,
} from '@opengeoweb/webmap';
import { DemoWrapperConnect } from '../../Providers/Providers';
import LayerManagerMapButtonConnect from '../LayerManagerMapButtonConnect';
import LayerManagerConnect from '../LayerManagerConnect';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('src/components/LayerManager/ProjectionSelect/ProjectionSelectConnect', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  const store = createMockStore({});
  it('should show the correct drawings', async () => {
    const mapId = 'map123';
    store.dispatch(mapActions.registerMap({ mapId }));
    render(
      <DemoWrapperConnect store={store}>
        <LayerManagerConnect showMapIdInTitle />
        <LayerManagerMapButtonConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );
    const layerManagerButton = screen.getByRole('button', {
      name: translateKeyOutsideComponents('layermanager-title'),
    });
    const user = userEvent.setup();
    await user.click(layerManagerButton);
    expect(
      await screen.findByText(
        `${translateKeyOutsideComponents('layermanager-title')} ${mapId}`,
      ),
    ).toBeTruthy();
    expect(await screen.findByText(`Europe Mercator`)).toBeTruthy();
    await user.click(
      screen.getByRole('combobox', {
        name: 'projectionSelect',
      }),
    );
    expect(await screen.findByText(`Europe Robinson`)).toBeTruthy();
    await user.click(
      screen.getByRole('option', {
        name: /Europe Robinson/i,
      }),
    );
    expect(screen.queryByText(`Europe Mercator`)).toBeFalsy();
    expect(await screen.findByText(`Europe Robinson`)).toBeTruthy();
  });
});
