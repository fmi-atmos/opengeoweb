/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import { uiActions, uiTypes } from '@opengeoweb/store';
import LayerManager from './LayerManager';
import { DemoWrapperConnect } from '../Providers/Providers';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store';

describe('src/components/LayerManager', () => {
  const props = {
    mapId: 'mapId_1',
    isOpen: true,
    onClose: (): void => {},
    showTitle: false,
    onMouseDown: jest.fn(),
    onToggleDock: jest.fn(),
  };

  it('should render required components and by default render undocked version', async () => {
    const mockState = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as uiTypes.DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: ['legend', 'layerManager'],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('layerManagerRowContainer')).toBeTruthy();
    expect(screen.getByTestId('layerContainerRow')).toBeTruthy();
    expect(screen.getByTestId('baseLayerRow')).toBeTruthy();
    expect(screen.getByTestId('descriptionRow')).toBeTruthy();
    expect(screen.getByTestId('dockedLayerManager-uncollapse')).toBeTruthy();

    fireEvent.mouseDown(screen.queryByTestId('layerManagerWindow')!);
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should show the loading indicator when loading', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      isLoading: true,
    };
    const mockState = {
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'mapId_3',
            isOpen: true,
          },
        },
        order: ['layerManager'],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} />
      </DemoWrapperConnect>,
    );

    expect(await screen.findByTestId('loading-bar')).toBeTruthy();
  });

  it('should show the alert banner when error given', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      error: 'Test error message.',
    };
    const mockState = {
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'mapId_3',
            isOpen: true,
          },
        },
        order: ['layerManager'],
      },
    };
    const store = createMockStore(mockState);
    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} />
      </DemoWrapperConnect>,
    );

    expect(await screen.findByText(props.error)).toBeTruthy();
    expect(await screen.findByTestId('alert-banner')).toBeTruthy();
  });

  it('should fire appropriate actions when clicking docking button', async () => {
    const mockState = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as uiTypes.DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: ['legend', 'layerManager'],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} />
      </DemoWrapperConnect>,
    );

    fireEvent.click(screen.queryByTestId('dockedBtn')!);
    expect(props.onToggleDock).toHaveBeenCalled();
  });

  it('should show the correct icon for docked version', async () => {
    const mockState = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as uiTypes.DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: ['legend', 'layerManager'],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} isDockedLayerManager />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId('dockedLayerManager-collapse')).toBeTruthy();
  });

  it('should show default title', async () => {
    const mockState = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as uiTypes.DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: ['legend', 'layerManager'],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} isDockedLayerManager />
      </DemoWrapperConnect>,
    );
    await screen.findByText(
      translateKeyOutsideComponents('layermanager-title'),
    );
  });
  it('should show custom title', async () => {
    const mockState = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as uiTypes.DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: ['legend', 'layerManager'],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} isDockedLayerManager title="Test title" />
      </DemoWrapperConnect>,
    );

    await screen.findByText('Test title');
  });
  it('should auto adjust size of docked Layer Manager to sizeLarge depending on the size of the map', async () => {
    const store = createMockStore();

    act(() => {
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: 'layerManager',
          mapId: 'map1',
          setOpen: true,
          source: 'app',
        }),
      );
    });

    global.innerWidth = 1000;

    // Use this mock object in your render function
    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} isDockedLayerManager />
      </DemoWrapperConnect>,
    );

    await waitFor(() => {
      const layerManagerWindow = screen.getByTestId('layerManagerWindow');
      expect(layerManagerWindow).toHaveAttribute('data-size', 'sizeLarge');
    });
  });

  it('should auto adjust size of docked Layer Manager to sizeMedium depending on the size of the map', async () => {
    const store = createMockStore();

    act(() => {
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: 'layerManager',
          mapId: 'map1',
          setOpen: true,
          source: 'app',
        }),
      );
    });

    global.innerWidth = 500;

    // Use this mock object in your render function
    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} isDockedLayerManager />
      </DemoWrapperConnect>,
    );

    await waitFor(() => {
      const layerManagerWindow = screen.getByTestId('layerManagerWindow');
      expect(layerManagerWindow).toHaveAttribute('data-size', 'sizeMedium');
    });
  });

  it('should auto adjust size of docked Layer Manager to sizeSmall depending on the size of the map', async () => {
    const store = createMockStore();

    act(() => {
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: 'layerManager',
          mapId: 'map1',
          setOpen: true,
          source: 'app',
        }),
      );
    });

    global.innerWidth = 200;

    // Use this mock object in your render function
    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} isDockedLayerManager />
      </DemoWrapperConnect>,
    );

    await waitFor(() => {
      const layerManagerWindow = screen.getByTestId('layerManagerWindow');
      expect(layerManagerWindow).toHaveAttribute('data-size', 'sizeSmall');
    });
  });

  it('should hide and unhide columns', async () => {
    const mockState = {
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: props.mapId,
            isOpen: true,
          },
        },
        order: ['layerManager'],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerManager {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('layerManagerRowContainer')).toBeTruthy();
    expect(screen.getByTestId('layerContainerRow')).toBeTruthy();
    expect(screen.getByTestId('descriptionRow')).toBeTruthy();

    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-opacity-title'),
      ),
    );
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('layermanager-opacity-title'),
      ),
    ).toBeFalsy();
    fireEvent.click(screen.getByLabelText('Expand Opacity'));
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-opacity-title'),
      ),
    ).toBeTruthy();
  });
});
