/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { mapUtils, uiTypes } from '@opengeoweb/store';
import {
  setWMSGetCapabilitiesFetcher,
  mockGetCapabilities,
} from '@opengeoweb/webmap';
import { LayerManagerConnect } from '.';
import { DemoWrapperConnect } from '../Providers/Providers';
import { getDialogType } from './LayerManagerConnect';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store';

describe('src/components/LayerManager/LayerManagerConnect', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });

  it('should register the dialog when mounting', async () => {
    const mockState = {
      webmap: {
        byId: { mapId123: mapUtils.createMap({ id: 'mapId123' }) },
        allIds: ['mapId123'],
      },
    };
    const store = createMockStore(mockState);
    expect(store.getState().ui.dialogs).toEqual({});

    render(
      <DemoWrapperConnect store={store}>
        <LayerManagerConnect />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.KeywordFilter],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerSelect],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager],
    ).toBeDefined();
  });

  it('should not register additional dialogs for docked layermanager', async () => {
    const mockState = {
      webmap: {
        byId: { mapId123: mapUtils.createMap({ id: 'mapId123' }) },
        allIds: ['mapId123'],
      },
    };
    const store = createMockStore(mockState);
    expect(store.getState().ui.dialogs).toEqual({});
    expect(
      store.getState().ui.dialogs[
        `${uiTypes.DialogTypes.DockedLayerManager}-mapId123`
      ],
    ).toBeUndefined();

    render(
      <DemoWrapperConnect store={store}>
        <LayerManagerConnect isDocked mapId="mapId123" />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().ui.dialogs[
        `${uiTypes.DialogTypes.DockedLayerManager}-mapId123`
      ],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager],
    ).toBeUndefined();
  });

  it('should include the docking button and trigger actions when pressing', async () => {
    const mapId = 'mapId123';
    const mockState = {
      webmap: {
        byId: { mapId123: mapUtils.createMap({ id: 'mapId123' }) },
        allIds: [mapId],
      },
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: mapId,
            isOpen: true,
          },
        },
        order: [uiTypes.DialogTypes.LayerManager],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerManagerConnect
          preloadedAvailableBaseLayers={[]}
          preloadedMapServices={[]}
        />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.KeywordFilter],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerSelect],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager],
    ).toBeDefined();

    fireEvent.click(screen.getByTestId('dockedBtn'));

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager].isOpen,
    ).toBeFalsy();
  });

  it('should show mapId in title', async () => {
    const mapId = 'mapId123';
    const mockState = {
      webmap: {
        byId: { mapId123: mapUtils.createMap({ id: 'mapId123' }) },
        allIds: [mapId],
      },
      ui: {
        dialogs: {
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: mapId,
            isOpen: true,
            focused: false,
          },
        },
        order: [],
      },
    };
    const store = createMockStore(mockState);
    render(
      <DemoWrapperConnect store={store}>
        <LayerManagerConnect showMapIdInTitle />
      </DemoWrapperConnect>,
    );

    await screen.findByText(
      `${translateKeyOutsideComponents('layermanager-title')} ${mapId}`,
    );
  });

  it('should register isMultiMap', async () => {
    const mapId = 'mapId123';
    const mockState = {
      webmap: {
        byId: {
          mapId123: {
            ...mapUtils.createMap({ id: mapId }),
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: [mapId],
      },
      ui: {
        dialogs: {
          [`${uiTypes.DialogTypes.LayerManager}-${mapId}`]: {
            type: `${uiTypes.DialogTypes.LayerManager}-${mapId}`,
            activeMapId: mapId,
            isOpen: true,
            focused: false,
          },
        },
        order: [`${uiTypes.DialogTypes.LayerManager}-${mapId}`],
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerManagerConnect mapId={mapId} isMultiMap={true} />
      </DemoWrapperConnect>,
    );

    await waitFor(() => {
      expect(
        store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo],
      ).toBeDefined();
    });

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.KeywordFilter],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerSelect],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[
        `${uiTypes.DialogTypes.LayerManager}-${mapId}`
      ],
    ).toBeDefined();
  });

  it('should register 2x isMultiMap dialogs', async () => {
    const mapId1 = 'mapId123';
    const mapId2 = 'mapId456';
    const mockState = {
      webmap: {
        byId: {
          mapId123: mapUtils.createMap({ id: mapId1 }),
          mapId456: mapUtils.createMap({ id: mapId2 }),
        },
        allIds: [mapId1, mapId2],
      },
    };
    const store = createMockStore(mockState);

    render(
      <>
        <DemoWrapperConnect store={store}>
          <LayerManagerConnect mapId={mapId1} isMultiMap={true} />
        </DemoWrapperConnect>
        ,
        <DemoWrapperConnect store={store}>
          <LayerManagerConnect mapId={mapId2} isMultiMap={true} />
        </DemoWrapperConnect>
        ,
      </>,
    );

    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerInfo],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.KeywordFilter],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[uiTypes.DialogTypes.LayerSelect],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[
        `${uiTypes.DialogTypes.LayerManager}-${mapId1}`
      ],
    ).toBeDefined();
    expect(
      store.getState().ui.dialogs[
        `${uiTypes.DialogTypes.LayerManager}-${mapId2}`
      ],
    ).toBeDefined();
  });

  describe('Docked LayerManager', () => {
    it('should register dialog', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`;
      const mockState = {};
      const store = createMockStore(mockState);

      expect(store.getState().ui.dialogs[dialogType]).toBeUndefined();

      render(
        <DemoWrapperConnect store={store}>
          <LayerManagerConnect
            mapId={mapId}
            preloadedAvailableBaseLayers={[]}
            isDocked
          />
        </DemoWrapperConnect>,
      );

      expect(store.getState().ui.dialogs[dialogType]).toBeDefined();
    });

    it('should register dialog with source if passed in', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`;
      const mockState = {};
      const store = createMockStore(mockState);

      expect(store.getState().ui.dialogs[dialogType]).toBeUndefined();

      render(
        <DemoWrapperConnect store={store}>
          <LayerManagerConnect
            mapId={mapId}
            preloadedAvailableBaseLayers={[]}
            source="module"
            isDocked
          />
        </DemoWrapperConnect>,
      );

      expect(store.getState().ui.dialogs[dialogType]).toBeDefined();
      expect(store.getState().ui.dialogs[dialogType].source).toEqual('module');
    });

    it('should close docked layer manager when pressing X', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`;
      const mockState = {
        ui: {
          dialogs: {
            [dialogType]: {
              isOpen: true,
              activeMapId: '',
              type: dialogType,
            },
          },
          order: [dialogType],
        },
      };

      const store = createMockStore(mockState);
      expect(store.getState().ui.dialogs[dialogType].isOpen).toBeTruthy();
      render(
        <DemoWrapperConnect store={store}>
          <LayerManagerConnect
            mapId={mapId}
            preloadedAvailableBaseLayers={[]}
            isDocked
          />
        </DemoWrapperConnect>,
      );

      fireEvent.click(screen.getByTestId('closeBtn'));

      expect(store.getState().ui.dialogs[dialogType].isOpen).toBeFalsy();
    });

    it('should open floating layer manager and trigger action to close docked layer manager when pressing dock', async () => {
      const mapId = 'mapId123';
      const dialogType = `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`;
      const mockState = {
        ui: {
          dialogs: {
            [dialogType]: {
              isOpen: true,
              activeMapId: '',
              type: dialogType,
            },
            layerManager: {
              type: uiTypes.DialogTypes.LayerManager,
              activeMapId: mapId,
              isOpen: false,
              focused: false,
            },
          },
          order: [dialogType, uiTypes.DialogTypes.LayerManager],
        },
      };
      const store = createMockStore(mockState);
      render(
        <DemoWrapperConnect store={store}>
          <LayerManagerConnect
            mapId={mapId}
            preloadedAvailableBaseLayers={[]}
            isDocked
          />
        </DemoWrapperConnect>,
      );

      expect(store.getState().ui.dialogs[dialogType].isOpen).toBeTruthy();
      expect(
        store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager].isOpen,
      ).toBeFalsy();

      fireEvent.click(screen.getByTestId('dockedBtn'));

      expect(store.getState().ui.dialogs[dialogType].isOpen).toBeFalsy();
      expect(
        store.getState().ui.dialogs[uiTypes.DialogTypes.LayerManager].isOpen,
      ).toBeTruthy();
    });
  });

  describe('getDialogType', () => {
    const mapId = 'map123';
    it('should return the correct dialog type', async () => {
      // multimap
      expect(getDialogType(mapId, true, false)).toEqual(
        `${uiTypes.DialogTypes.LayerManager}-${mapId}`,
      );
      // docked
      expect(getDialogType(mapId, false, true)).toEqual(
        `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`,
      );
      // docked multimap
      expect(getDialogType(mapId, true, true)).toEqual(
        `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`,
      );
      // default
      expect(getDialogType(mapId, false, false)).toEqual(
        uiTypes.DialogTypes.LayerManager,
      );
    });
  });
});
