/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import DescriptionRow from './DescriptionRow';
import { DemoWrapper } from '../../Providers/Providers';
import {
  columnClasses,
  LayerManagerCustomSettings,
} from '../LayerManagerUtils';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('src/components/LayerManager/DescriptionRow', () => {
  const props = {};
  const propsWithLayerSelect = {
    ...props,
  };

  it('should render correct components without layerSelect parameter', () => {
    render(
      <DemoWrapper>
        <DescriptionRow {...props} />
      </DemoWrapper>,
    );
    expect(screen.getByTestId('descriptionRow')).toBeTruthy();
    expect(screen.getByTestId('layerSelectButton')).toBeTruthy();
    expect(
      screen.getAllByText(translateKeyOutsideComponents('layermanager-layer')),
    ).toHaveLength(2);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-style-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-opacity-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-dimensions-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-acceptance-time'),
      ),
    ).toBeTruthy();
  });

  it('should render correct components with layerSelect parameter', () => {
    render(
      <DemoWrapper>
        <DescriptionRow {...propsWithLayerSelect} />
      </DemoWrapper>,
    );
    expect(screen.getByTestId('descriptionRow')).toBeTruthy();
    expect(screen.getByTestId('layerSelectButton')).toBeTruthy();
    expect(
      screen.getAllByText(translateKeyOutsideComponents('layermanager-layer')),
    ).toHaveLength(2);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-style-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-opacity-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-dimensions-title'),
      ),
    ).toBeTruthy();
  });

  it('should render correctly with no settings prop', async () => {
    render(
      <DemoWrapper>
        <DescriptionRow {...props} />
      </DemoWrapper>,
    );

    expect(
      screen.getAllByText(translateKeyOutsideComponents('layermanager-layer')),
    ).toHaveLength(2);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-style-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-opacity-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-dimensions-title'),
      ),
    ).toBeTruthy();
  });

  it('should render headers according to settings prop', async () => {
    const customSettings: LayerManagerCustomSettings['header'] = {
      addLayer: {
        tooltipTitle: 'addLayerTooltip',
        icon: <div data-testid="testAddLayerIcon" />,
      },
      layerName: {
        title: 'layerNameTitle',
      },
      layerStyle: {
        title: 'layerStyleTitle',
      },
      opacity: {
        title: 'opacityTitle',
      },
      dimensions: {
        title: 'dimensionsTitle',
      },
      acceptanceTime: { title: 'acceptanceTimeTitle' },
    };

    render(
      <DemoWrapper>
        <DescriptionRow {...props} settings={customSettings} />
      </DemoWrapper>,
    );

    expect(
      screen.queryByText(translateKeyOutsideComponents('layermanager-layer')),
    ).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('layermanager-style-title'),
      ),
    ).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('layermanager-opacity-title'),
      ),
    ).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('layermanager-dimensions-title'),
      ),
    ).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('layermanager-acceptance-time'),
      ),
    ).toBeFalsy();

    expect(screen.getByTestId('testAddLayerIcon')).toBeTruthy();
    expect(screen.getAllByText(customSettings.layerName!.title)).toHaveLength(
      2,
    );
    expect(screen.getByText(customSettings.layerStyle!.title)).toBeTruthy();
    expect(screen.getByText(customSettings.opacity!.title)).toBeTruthy();
    expect(screen.getByText(customSettings.dimensions!.title)).toBeTruthy();
    expect(screen.getByText(customSettings.acceptanceTime!.title)).toBeTruthy();
  });

  it('should render correct headers with partial settings given', async () => {
    const customSettings: LayerManagerCustomSettings['header'] = {
      layerName: {
        title: 'layerNameTitle',
      },
      layerStyle: {
        title: 'layerStyleTitle',
      },
    };

    render(
      <DemoWrapper>
        <DescriptionRow {...props} settings={customSettings} />
      </DemoWrapper>,
    );
    expect(screen.getAllByText(customSettings.layerName!.title)).toHaveLength(
      2,
    );
    expect(screen.getByText(customSettings.layerStyle!.title)).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-opacity-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-dimensions-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-acceptance-time'),
      ),
    ).toBeTruthy();
  });

  it('should toggle columns', async () => {
    const onToggle = jest.fn();
    render(
      <DemoWrapper>
        <DescriptionRow
          {...props}
          onToggleCollapsed={onToggle}
          collapsedColumns={{ [columnClasses.column3]: true }}
        />
      </DemoWrapper>,
    );

    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-opacity-title'),
      ),
    );
    expect(onToggle).toHaveBeenCalledWith(columnClasses.column4);
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('layermanager-style-title'),
      ),
    ).toBeFalsy();
    fireEvent.click(screen.getByLabelText('Expand Style'));
    expect(onToggle).toHaveBeenCalledWith(columnClasses.column3);
  });
});
