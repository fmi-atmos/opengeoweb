/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { storeTestSettings } from '@opengeoweb/store';

import { LayerProps } from '@opengeoweb/webmap';
import { translateKeyOutsideComponents } from '../../../../../utils/i18n';
import RenderLayers, { RenderLayersProps } from './RenderLayers';
import { defaultReduxLayerRadarColor } from '../../../../../utils/defaultTestSettings';
import { DemoWrapper } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/RenderLayers/RenderLayers', () => {
  const defaultProps: RenderLayersProps = {
    layers: storeTestSettings.defaultReduxServices['serviceid_1'].layers!,
    layerName: defaultReduxLayerRadarColor.name!,
    onChangeLayerName: jest.fn(),
  };

  it('should show a message if no layers are provided', async () => {
    const mockProps = {
      ...defaultProps,
      layers: [],
    };

    render(
      <DemoWrapper>
        <RenderLayers {...mockProps} />
      </DemoWrapper>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-baselayers-no-available'),
      ),
    ).toBeTruthy();
    expect(screen.queryByTestId('selectLayer')).toBeFalsy();
  });

  it('should show the select layer component with the first layer selected if layers are provided', () => {
    render(
      <DemoWrapper>
        <RenderLayers {...defaultProps} />
      </DemoWrapper>,
    );
    const selectLayer = screen.getByTestId('selectLayer');

    expect(
      screen.queryByText(
        translateKeyOutsideComponents('layermanager-baselayers-no-available'),
      ),
    ).toBeFalsy();
    expect(selectLayer).toBeTruthy();
    expect(selectLayer.textContent).toEqual(defaultProps.layers[0].title);
  });

  it('should trigger onChangeLayerName if a new layer is selected', async () => {
    render(
      <DemoWrapper>
        <RenderLayers {...defaultProps} />
      </DemoWrapper>,
    );
    const newLayer = defaultProps.layers[1];
    const selectLayer = screen.getByTestId('selectLayer');

    fireEvent.mouseDown(selectLayer);

    const menuItem = await screen.findByText(newLayer.title!);
    fireEvent.click(menuItem);

    expect(defaultProps.onChangeLayerName).toHaveBeenCalledWith(newLayer.name);
  });

  it('should call onChangeLayerName on wheel scroll only if the Ctrl key is pressed', async () => {
    render(
      <DemoWrapper>
        <RenderLayers {...defaultProps} />
      </DemoWrapper>,
    );
    const select = screen.getByTestId('selectLayer');

    fireEvent.wheel(select, { deltaY: 1 });
    await waitFor(() =>
      expect(defaultProps.onChangeLayerName).toHaveBeenCalledTimes(0),
    );

    fireEvent.wheel(select, { ctrlKey: true, deltaY: 1 });
    await waitFor(() =>
      expect(defaultProps.onChangeLayerName).toHaveBeenCalledTimes(1),
    );
    expect(defaultProps.onChangeLayerName).toHaveBeenCalledWith(
      defaultProps.layers[1].name,
    );

    fireEvent.wheel(select, { ctrlKey: true, deltaY: -1 });
    await waitFor(() =>
      expect(defaultProps.onChangeLayerName).toHaveBeenCalledTimes(1),
    );
  });

  it('should show only categories as disabled', async () => {
    const mockProps = {
      ...defaultProps,
      layers: [
        {
          name: 'Category',
          title: 'Category',
          leaf: false,
          path: [],
        },
        {
          name: 'Layername',
          title: 'Layername',
          leaf: true,
          path: ['Category'],
        },
      ],
    };
    render(
      <DemoWrapper>
        <RenderLayers {...mockProps} />
      </DemoWrapper>,
    );

    const selectLayer = screen.getByTestId('selectLayer');
    fireEvent.mouseDown(selectLayer);

    const category = await screen.findByText(mockProps.layers[0].title);
    expect(category.className).toContain('disabled');

    const layer = await screen.findByText(mockProps.layers[1].title);
    expect(layer.className).not.toContain('disabled');
  });

  it('should show passed in layerName as selected layer even if missing from original layer list', () => {
    const extraLayerName = 'Name of missing layer';
    const propsWithExtraLayer = { ...defaultProps, layerName: extraLayerName };
    render(
      <DemoWrapper>
        <RenderLayers {...propsWithExtraLayer} />
      </DemoWrapper>,
    );
    const selectLayer = screen.getByTestId('selectLayer');
    expect(selectLayer).toBeTruthy();
    expect(selectLayer.textContent).toBe(extraLayerName);
  });

  it('should show passed in layerName among layer options even if missing from original layer list', async () => {
    const extraLayerName = 'Name of missing layer';
    const propsWithExtraLayer = { ...defaultProps, layerName: extraLayerName };
    render(
      <DemoWrapper>
        <RenderLayers {...propsWithExtraLayer} />
      </DemoWrapper>,
    );
    const selectLayer = screen.getByTestId('selectLayer');
    expect(selectLayer).toBeTruthy();
    fireEvent.mouseDown(selectLayer);

    const options = await screen.findAllByRole('option');
    const optionTexts = options.map((option) => option.textContent);
    const expectedLayerTexts = defaultProps.layers.map(
      (l: LayerProps) => l.title,
    );

    expect(optionTexts).toEqual(expect.arrayContaining(expectedLayerTexts));
    expect(optionTexts).toContain(extraLayerName);
  });

  it('should show an error icon if the layer is missing', async () => {
    const extraLayerName = 'Name of missing layer';
    const propsWithExtraLayer = { ...defaultProps, layerName: extraLayerName };
    render(
      <DemoWrapper>
        <RenderLayers {...propsWithExtraLayer} />
      </DemoWrapper>,
    );
    const selectLayer = screen.getByTestId('selectLayer');
    expect(selectLayer).toBeTruthy();
    fireEvent.mouseDown(selectLayer);

    expect(screen.getByTestId('alert-icon')).toBeTruthy();
  });

  it('should not show an error icon if the layer is not missing', async () => {
    const propsWithExtraLayer = { ...defaultProps };
    render(
      <DemoWrapper>
        <RenderLayers {...propsWithExtraLayer} />
      </DemoWrapper>,
    );

    const selectLayer = screen.getByTestId('selectLayer');
    expect(selectLayer).toBeTruthy();
    fireEvent.mouseDown(selectLayer);

    expect(screen.queryByTestId('alert-icon')).toBeNull();
  });

  it('should show custom tooltip prefix', async () => {
    render(
      <DemoWrapper>
        <RenderLayers {...defaultProps} tooltipPrefix="Test tooltip:" />
      </DemoWrapper>,
    );
    fireEvent.mouseOver(screen.getByTestId('selectLayer'));
    expect(await screen.findByText(/^Test tooltip:.*/)).toBeTruthy();
  });
});
