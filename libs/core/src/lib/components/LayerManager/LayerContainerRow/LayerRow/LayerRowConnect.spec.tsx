/* eslint-disable @typescript-eslint/no-unused-vars */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { storeTestUtils } from '@opengeoweb/store';
import { LayerType } from '@opengeoweb/webmap';
import { defaultReduxLayerRadarKNMI } from '../../../../utils/defaultTestSettings';
import { DemoWrapperConnect } from '../../../Providers/Providers';
import LayerRowConnect from './LayerRowConnect';
import DragHandle from './DragHandle/DragHandle';
import { createMockStore } from '../../../../store';

const missingLayer = {
  service: 'https://testservice',
  name: 'missing layer name',
  title: 'missing layer title',
  format: 'image/png',
  style: 'knmiradar/nearest',
  enabled: true,
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ],
  styles: [
    {
      title: 'knmiradar/nearest',
      name: 'knmiradar/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=knmiradar/nearest',
      abstract: 'No abstract available',
    },
  ],
  id: 'layerid_2',
};

describe('core/LayerManger/LayerContainerRow/LayerRow/LayerRowConnect', () => {
  it('should create LayerRow with no alert if layer is not missing from service', () => {
    const mapId = 'mapid_1';
    // the given layer is included in default services
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerRowConnect
          layerId={layer.id!}
          dragHandle={<DragHandle hideTooltip />}
          mapId={mapId}
        />
      </DemoWrapperConnect>,
    );

    expect(screen.queryByRole('alert')).toBeNull();
  });

  it('should create LayerRow with alert if layer is missing from service', () => {
    const mapId = 'mapid_1';
    // the missing layer is not included in default services
    const mockState = storeTestUtils.mockStateMapWithLayer(missingLayer, mapId);
    const store = createMockStore(mockState);

    // jest is giving warning for out of bounds value for select box, which is correct since it's missing
    jest.spyOn(console, 'warn').mockImplementation(() => {});

    render(
      <DemoWrapperConnect store={store}>
        <LayerRowConnect
          layerId={missingLayer.id}
          dragHandle={<DragHandle hideTooltip />}
          mapId={mapId}
        />
      </DemoWrapperConnect>,
    );

    expect(screen.getByRole('alert')).toBeTruthy();
    const layerInfoButtons = screen.getAllByTestId('layerInfoButton');
    layerInfoButtons.forEach((layerInfoButton) => {
      expect(layerInfoButton).toBeDisabled();
    });
  });

  it('should use a custom renderLayers element', () => {
    const mapId = 'mapid_1';
    // the given layer is included in default services
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStore(mockState);

    const testId = 'testRenderLayer';
    const TestElement: React.FC = () => <span data-testid={testId} />;
    const settings = {
      renderLayer: {
        Element: TestElement,
      },
    };

    render(
      <DemoWrapperConnect store={store}>
        <LayerRowConnect
          layerId={layer.id!}
          dragHandle={<DragHandle />}
          mapId={mapId}
          settings={settings}
        />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId(testId)).toBeTruthy();
  });

  it('should render LayerInfoButton', () => {
    const mapId = 'mapid_1';
    // the given layer is included in default services
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStore(mockState);

    const testId = 'testRenderLayer';
    const TestElement: React.FC = () => <span data-testid={testId} />;
    const settings = {
      renderLayer: {
        Element: TestElement,
      },
    };

    render(
      <DemoWrapperConnect store={store}>
        <LayerRowConnect
          layerId={layer.id!}
          dragHandle={<DragHandle />}
          mapId={mapId}
          settings={settings}
        />
      </DemoWrapperConnect>,
    );

    expect(screen.getByRole('button', { name: 'layer info' })).toBeTruthy();
  });
});
