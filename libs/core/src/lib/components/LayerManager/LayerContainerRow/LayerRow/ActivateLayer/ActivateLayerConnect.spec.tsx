/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { produce } from 'immer';
import { storeTestUtils } from '@opengeoweb/store';

import { translateKeyOutsideComponents } from '../../../../../utils/i18n';
import { defaultReduxLayerRadarKNMI } from '../../../../../utils/defaultTestSettings';
import { DemoWrapperConnect } from '../../../../Providers/Providers';
import ActivateLayerConnect from './ActivateLayerConnect';
import { createMockStore } from '../../../../../store';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/ActivateLayer/ActivateLayerConnect', () => {
  const mapId = 'mapid_1';
  const layer = defaultReduxLayerRadarKNMI;
  const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);

  it('should activate a layer', async () => {
    const mockState1 = produce(mockState, (draft) => {
      draft.webmap!.byId[mapId].autoTimeStepLayerId = undefined;
      draft.webmap!.byId[mapId].autoUpdateLayerId = undefined;
    });
    const user = userEvent.setup();

    const store = createMockStore({
      webmap: mockState1.webmap,
      layers: mockState1.layers,
    });

    render(
      <DemoWrapperConnect store={store}>
        <ActivateLayerConnect mapId={mapId} layerId={layer.id!} />
      </DemoWrapperConnect>,
    );

    await user.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('layermanager-layer-leading'),
      }),
    );
    expect(
      screen.getByRole('menuitem', {
        name: translateKeyOutsideComponents('layermanager-layer-leading-none'),
      }).classList,
    ).toContain('Mui-selected');
    await user.click(
      screen.getByRole('menuitem', {
        name: translateKeyOutsideComponents('layermanager-layer-leading-both'),
      }),
    );
    await user.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('layermanager-layer-leading'),
      }),
    );
    expect(
      screen.getByRole('menuitem', {
        name: translateKeyOutsideComponents('layermanager-layer-leading-both'),
      }).classList,
    ).toContain('Mui-selected');
    await user.click(screen.getByRole('menuitem', { name: /Auto-timestep/i }));
    await user.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('layermanager-layer-leading'),
      }),
    );
    expect(
      screen.getByRole('menuitem', { name: /Auto-timestep/i }).classList,
    ).toContain('Mui-selected');
    await user.click(screen.getByRole('menuitem', { name: /Auto-update/i }));
    await user.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('layermanager-layer-leading'),
      }),
    );
    expect(
      screen.getByRole('menuitem', { name: /Auto-update/i }).classList,
    ).toContain('Mui-selected');
    await user.click(
      screen.getByRole('menuitem', {
        name: translateKeyOutsideComponents('layermanager-layer-leading-none'),
      }),
    );
    await user.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('layermanager-layer-leading'),
      }),
    );
    expect(
      screen.getByRole('menuitem', {
        name: translateKeyOutsideComponents('layermanager-layer-leading-none'),
      }).classList,
    ).toContain('Mui-selected');
  });

  it('should not show for a layer without time dimension', async () => {
    const mockState1 = produce(mockState, (draft) => {
      draft.layers!.byId[layer.id!]!.dimensions = [];
    });

    const store = createMockStore({
      webmap: mockState1.webmap,
      layers: mockState1.layers,
    });
    const user = userEvent.setup();

    render(
      <DemoWrapperConnect store={store}>
        <ActivateLayerConnect mapId={mapId} layerId={layer.id!} />
      </DemoWrapperConnect>,
    );

    const noTimeDimension = screen.getByTestId('noTimeDimension');
    await user.hover(noTimeDimension);
    await screen.findByRole('tooltip', { name: 'Layer has no time dimension' });
  });
});
