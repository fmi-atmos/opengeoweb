/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { storeTestUtils, storeTestSettings } from '@opengeoweb/store';
import {
  setWMSGetCapabilitiesFetcher,
  mockGetCapabilities,
} from '@opengeoweb/webmap';
import { translateKeyOutsideComponents } from '../../../../../utils/i18n';
import { defaultReduxLayerRadarColor } from '../../../../../utils/defaultTestSettings';

import RenderLayersConnect from './RenderLayersConnect';
import { DemoWrapperConnect } from '../../../../Providers/Providers';
import { createMockStore } from '../../../../../store';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/RenderLayers/RenderLayersConnect', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });

  it('should set layer name', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStore(mockState);
    const props = {
      mapId: 'test',
      layerId: layer.id,
    };

    render(
      <DemoWrapperConnect store={store}>
        <RenderLayersConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().layers.byId[layer.id].name).toEqual(layer.name);

    const select = screen.getByTestId('selectLayer');

    fireEvent.mouseDown(select);
    const newName =
      storeTestSettings.defaultReduxServices['serviceid_1'].layers![1];

    const menuItem = await screen.findByText(newName.title!);

    fireEvent.click(menuItem);

    expect(store.getState().layers.byId[layer.id].name).toEqual(newName.name);
  });

  it('should set service layers in store when layer service is not yet available in store', async () => {
    const mapId = 'mapid_1';
    const layer = {
      ...defaultReduxLayerRadarColor,
      service: 'https://mock-test-service.nl',
    };

    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStore({
      ...mockState,
      services: { byId: {}, allIds: [] },
    });
    const props = {
      mapId: 'test',
      layerId: layer.id,
    };

    render(
      <DemoWrapperConnect store={store}>
        <RenderLayersConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-baselayers-no-available'),
      ),
    ).toBeTruthy();
    await waitFor(() => {
      return expect(
        store.getState().services.byId['serviceid_1'].serviceUrl,
      ).toEqual(layer.service);
    });

    expect(store.getState().services.byId['serviceid_1'].scope).toEqual('user');
  });
});
