/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { storeTestUtils } from '@opengeoweb/store';
import { webmapUtils } from '@opengeoweb/webmap';
import { translateKeyOutsideComponents } from '../../../../../utils/i18n';
import { defaultReduxLayerRadarColor } from '../../../../../utils/defaultTestSettings';
import { DemoWrapperConnect } from '../../../../Providers/Providers';
import LayerManagerMenuButtonConnect from './MenuButtonConnect';
import { createMockStore } from '../../../../../store';

describe('src/lib/components/LayerManager/LayerContainerRow/LayerRow/Menubutton/LayerManagerMenuBottonConnect', () => {
  it('should duplicate a layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LayerManagerMenuButtonConnect mapId={mapId} layerId={layer.id} />
      </DemoWrapperConnect>,
    );

    const initialState = store.getState().layers;
    expect(initialState.allIds).toHaveLength(1);
    expect(initialState.allIds.includes('layerid_1')).toBeTruthy();

    const newGeneratedLayerId = 'layer_2';
    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce(newGeneratedLayerId);

    fireEvent.click(screen.getByTestId('openMenuButton'));
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('layermanager-layer-duplicate'),
      ),
    );

    const result = store.getState().layers;
    expect(result.allIds).toHaveLength(2);
    expect(result.allIds.includes('layerid_1')).toBeTruthy();
    expect(result.allIds.includes(newGeneratedLayerId)).toBeTruthy();

    const { id, ...newLayerResult } = result.byId[newGeneratedLayerId];
    const { id: oldId, ...oldLayer } = result.byId['layerid_1'];

    expect(newLayerResult).toEqual(oldLayer);
  });
});
