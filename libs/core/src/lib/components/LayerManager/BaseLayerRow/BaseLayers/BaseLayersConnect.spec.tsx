/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { LayerType, webmapUtils } from '@opengeoweb/webmap';
import { storeTestUtils } from '@opengeoweb/store';
import BaseLayersConnect, {
  areAvailableBaseLayersSame,
  constructLayerId,
  constructListAvailableBaseLayers,
} from './BaseLayersConnect';
import {
  baseLayer,
  baseLayerGrey,
  baseLayerWorldMap,
} from '../../../../utils/testLayers';
import { DemoWrapperConnect } from '../../../Providers/Providers';
import { createMockStore } from '../../../../store';

describe('src/components/LayerManager/BaseLayerRow/BaseLayers/BaseLayersConnect', () => {
  it('should should add available baseLayers on mount if not present in store', async () => {
    const mapId = 'mapid_1';
    const preloadedAvailableBaseLayers = [
      { ...baseLayerGrey, mapId },
      { ...baseLayerWorldMap, mapId },
    ];
    const mockState = storeTestUtils.mockStateMapWithLayer(
      preloadedAvailableBaseLayers[0],
      mapId,
    );
    const store = createMockStore(mockState);

    expect(store.getState().layers.availableBaseLayers.allIds).toHaveLength(0);
    const newGeneratedLayerId = 'base-test';
    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce(newGeneratedLayerId);
    const { rerender } = render(
      <DemoWrapperConnect store={store}>
        <BaseLayersConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </DemoWrapperConnect>,
    );

    expect(store.getState().layers.availableBaseLayers.allIds).toHaveLength(
      preloadedAvailableBaseLayers.length,
    );

    expect(
      store.getState().layers.availableBaseLayers.byId[
        preloadedAvailableBaseLayers[0].id
      ],
    ).toEqual(expect.objectContaining(preloadedAvailableBaseLayers[0]));

    expect(
      store.getState().layers.availableBaseLayers.byId[newGeneratedLayerId],
    ).toEqual(
      expect.objectContaining({
        ...preloadedAvailableBaseLayers[1],
        id: newGeneratedLayerId,
      }),
    );

    // rerender with same preloaded baseLayers
    rerender(
      <DemoWrapperConnect store={store}>
        <BaseLayersConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </DemoWrapperConnect>,
    );

    // rerender with new preloaded baseLayers
    const newPreloadedAvailableBaseLayers = preloadedAvailableBaseLayers.map(
      (layer) => ({ ...layer, name: `base-layer-new-${layer.name}` }),
    );

    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce(newGeneratedLayerId);
    rerender(
      <DemoWrapperConnect store={store}>
        <BaseLayersConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={newPreloadedAvailableBaseLayers}
        />
      </DemoWrapperConnect>,
    );

    expect(store.getState().layers.availableBaseLayers.allIds).toHaveLength(
      newPreloadedAvailableBaseLayers.length,
    );
    expect(
      store.getState().layers.availableBaseLayers.byId[
        newPreloadedAvailableBaseLayers[0].id
      ],
    ).toEqual(expect.objectContaining(newPreloadedAvailableBaseLayers[0]));

    expect(
      store.getState().layers.availableBaseLayers.byId[newGeneratedLayerId],
    ).toEqual(
      expect.objectContaining({
        ...newPreloadedAvailableBaseLayers[1],
        id: newGeneratedLayerId,
      }),
    );
  });

  it('should add a base layer', async () => {
    const mapId = 'mapid_1';
    const preloadedAvailableBaseLayers = [
      { ...baseLayerGrey, mapId },
      { ...baseLayerWorldMap, mapId },
    ];
    const mockState = storeTestUtils.mockStateMapWithLayer(
      preloadedAvailableBaseLayers[0],
      mapId,
    );
    mockState.layers!.availableBaseLayers = {
      allIds: [baseLayerGrey.id, baseLayerWorldMap.id],
      byId: {
        [baseLayerGrey.id]: { ...baseLayerGrey, mapId },
        [baseLayerWorldMap.id]: {
          ...baseLayerWorldMap,
          mapId,
        },
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <BaseLayersConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </DemoWrapperConnect>,
    );
    const baseLayerSelect = screen.getByTestId('selectBaseLayer');
    fireEvent.mouseDown(baseLayerSelect);

    const newBaseLayer = preloadedAvailableBaseLayers[1];
    const menuItem = await screen.findByText(newBaseLayer.name);
    fireEvent.click(menuItem);

    const result =
      store.getState().layers.availableBaseLayers.byId[newBaseLayer.id];
    expect(result).toBeTruthy();
    expect(result).toEqual(expect.objectContaining(newBaseLayer));
  });

  describe('constructLayerId', () => {
    it('should use the layerId of the selected base layer in case id or name match', async () => {
      expect(
        constructLayerId(
          {
            id: 'somerandomid',
            name: 'WorldMap_Light_Grey_Canvas',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
        ),
      ).toEqual('base-layer-1');
      expect(
        constructLayerId(
          {
            id: 'base-layer-1',
            name: 'WorldMap_Light_Grey_Canvas',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
        ),
      ).toEqual('base-layer-1');
      jest
        .spyOn(webmapUtils, 'generateLayerId')
        .mockImplementationOnce((): string => {
          return 'layer12222';
        });
      expect(
        constructLayerId(
          {
            id: 'someRandomId',
            name: 'someRandomName',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
        ),
      ).toEqual('layer12222');

      jest
        .spyOn(webmapUtils, 'generateLayerId')
        .mockReturnValueOnce('id_when_it_was_missing');
      expect(
        constructLayerId(
          {
            name: 'someRandomName2',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
        ),
      ).toEqual('id_when_it_was_missing');
    });
  });

  describe('constructListAvailableBaseLayers', () => {
    it('should construct a list of available baselayers with mapId and unique id from the passed in selected layers and available layers', async () => {
      jest
        .spyOn(webmapUtils, 'generateLayerId')
        .mockImplementation((): string => {
          return 'layer12222';
        });
      const expectedMapIdAndLayerId = { mapId: 'mapid-1', id: 'layer12222' };
      const selectedLayer = {
        id: 'somerandomid',
        name: 'WorldMap_Light_Grey_Canvas',
        type: 'twms',
        layerType: LayerType.baseLayer,
      };
      expect(
        constructListAvailableBaseLayers(
          [selectedLayer],
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
          'mapid-1',
        ),
      ).toEqual([
        // Because selected layer has same name as this passed in initialAvailableBaseLayer, the layer id of the selected layer should be used
        { ...baseLayerGrey, mapId: 'mapid-1', id: 'somerandomid' },
        { ...baseLayer, ...expectedMapIdAndLayerId },
        { ...baseLayerWorldMap, ...expectedMapIdAndLayerId },
      ]);
    });
    it('should add currently selected base layer if not in the list of availableBaseLayers', async () => {
      jest
        .spyOn(webmapUtils, 'generateLayerId')
        .mockImplementation((): string => {
          return 'layer12222';
        });
      const expectedMapIdAndLayerId = { mapId: 'mapid-1', id: 'layer12222' };
      const selectedLayer = {
        id: 'somerandomid',
        name: 'some_different_baselayer',
        type: 'twms',
        layerType: LayerType.baseLayer,
      };
      expect(
        constructListAvailableBaseLayers(
          [selectedLayer],
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
          'mapid-1',
        ),
      ).toEqual([
        // Because selected layer has different id and name than any layer passed in initialAvailableBaseLayer, the layer selectedBaseLayer is added to the list at the bottom
        { ...baseLayerGrey, ...expectedMapIdAndLayerId },
        { ...baseLayer, ...expectedMapIdAndLayerId },
        { ...baseLayerWorldMap, ...expectedMapIdAndLayerId },
        { ...selectedLayer, mapId: 'mapid-1' },
      ]);
    });

    it('should add currently selected base layer if the layer id is missing', async () => {
      jest
        .spyOn(webmapUtils, 'generateLayerId')
        .mockImplementation((): string => {
          return 'when_missing';
        });
      const expectedMapIdAndLayerId = { mapId: 'mapid-1', id: 'when_missing' };
      const selectedLayer = {
        name: 'some_different_baselayer',
        type: 'twms',
        layerType: LayerType.baseLayer,
      };
      expect(
        constructListAvailableBaseLayers(
          [selectedLayer],
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
          'mapid-1',
        ),
      ).toEqual([
        // Because selected layer has different id and name than any layer passed in initialAvailableBaseLayer, the layer selectedBaseLayer is added to the list at the bottom
        { ...baseLayerGrey, ...expectedMapIdAndLayerId },
        { ...baseLayer, ...expectedMapIdAndLayerId },
        { ...baseLayerWorldMap, ...expectedMapIdAndLayerId },
        { ...selectedLayer, mapId: 'mapid-1' },
      ]);
    });
  });

  describe('areAvailableBaseLayersSame', () => {
    it('should return true if available baseLayers are the same', () => {
      expect(areAvailableBaseLayersSame([], [])).toBeTruthy();
      expect(
        areAvailableBaseLayersSame(
          [
            {
              id: 'testId',
              name: 'testName',
              type: 'twms',
              layerType: LayerType.baseLayer,
            },
          ],
          [
            {
              id: 'testId',
              name: 'testName',
              type: 'twms',
              layerType: LayerType.baseLayer,
            },
          ],
        ),
      ).toBeTruthy();

      expect(
        areAvailableBaseLayersSame(
          [
            {
              id: 'testId',
              name: 'testName',
              type: 'twms',
              layerType: LayerType.baseLayer,
            },
          ],
          [
            {
              id: 'testId2',
              name: 'testName',
              type: 'twms',
              layerType: LayerType.baseLayer,
            },
          ],
        ),
      ).toBeTruthy();
    });
  });

  it('should return false if available baseLayers are not same', () => {
    expect(
      areAvailableBaseLayersSame(
        [],
        [
          {
            id: 'testId',
            name: 'testName',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
      ),
    ).toBeFalsy();
    expect(
      areAvailableBaseLayersSame(
        [
          {
            id: 'testIdd',
            name: 'testName',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
        [],
      ),
    ).toBeFalsy();

    expect(
      areAvailableBaseLayersSame(
        [
          {
            id: 'testId',
            name: 'testName2',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
        [
          {
            id: 'testId',
            name: 'testName',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
      ),
    ).toBeFalsy();

    expect(
      areAvailableBaseLayersSame(
        [
          {
            id: 'testId',
            name: 'testName',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
        [
          {
            id: 'testId2',
            name: 'testName2',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
      ),
    ).toBeFalsy();
  });
});
