/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  LayerType,
  TileSettings,
  tilesettings,
  webmapUtils,
} from '@opengeoweb/webmap';
import { Delete, DrawFIRLand } from '@opengeoweb/theme';
import { publicLayers, MapControlButton } from '@opengeoweb/webmap-react';
import { ConfigurableMapConnect } from './ConfigurableMapConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { LayerManagerConnect } from '../LayerManager';
import { createMockStore } from '../../store';

export default {
  title: 'components/ConfigurableMap',
};

const store = createMockStore();

export const ConfigurableMapDefault = (): React.ReactElement => {
  const mapId = webmapUtils.generateMapId();
  return (
    <div style={{ height: '100vh' }}>
      <DemoWrapperConnect store={store}>
        <LayerManagerConnect />
        <ConfigurableMapConnect
          id={mapId}
          layers={[
            {
              ...publicLayers.radarLayer,
              style: 'precip-blue-transparent/nearest',
            },
          ]}
        />
      </DemoWrapperConnect>
    </div>
  );
};

const customMaxLevelTileSettings = {
  Positron_NL: {
    'EPSG:3857': {
      ...(tilesettings['Positron_NL']['EPSG:3857'] as TileSettings),
      maxLevel: 8,
    },
  },
};

export const ConfigurableMapCustomized = (): React.ReactElement => {
  const mapId = webmapUtils.generateMapId();
  return (
    <div style={{ height: '100vh' }}>
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect
          id={mapId}
          layers={[
            {
              ...publicLayers.radarLayer,
              style: 'precip-blue-transparent/nearest',
            },
            {
              name: 'Positron_NL',
              type: 'twms',
              id: 'tilesetting-Positron_NL',
              layerType: LayerType.baseLayer,
            },
            {
              service: 'https://eccharts.ecmwf.int/wms/?token=public',
              name: 'boundaries',
              layerType: LayerType.overLayer,
              style: 'red_boundaries',
            },
          ]}
          displayTimeInMap
          shouldAutoUpdate
          shouldAnimate
          showTimeSlider={false}
          shouldShowZoomControls={false}
          tileServerSettings={customMaxLevelTileSettings}
          displayLayerManagerAndLegendButtonInMap={false}
          initialBbox={{
            left: -450651.2255879827,
            bottom: 6393842.957153378,
            right: 1428345.8183648037,
            top: 7342085.640241,
          }}
          title={`Custom title for map ${mapId}`}
        />
      </DemoWrapperConnect>
    </div>
  );
};

export const ConfigurableMapDrawControls = (): React.ReactElement => {
  const mapId = webmapUtils.generateMapId();
  return (
    <div style={{ height: '100vh' }}>
      <DemoWrapperConnect store={store}>
        <LayerManagerConnect />
        <ConfigurableMapConnect
          id={mapId}
          layers={[
            {
              ...publicLayers.radarLayer,
              style: 'precip-blue-transparent/nearest',
            },
          ]}
          shouldDisplayDrawControls
        />
      </DemoWrapperConnect>
    </div>
  );
};

export const ConfigurableMapMapControls = (): React.ReactElement => {
  const mapId = webmapUtils.generateMapId();
  return (
    <div style={{ height: '100vh' }}>
      <DemoWrapperConnect store={store}>
        <LayerManagerConnect />
        <ConfigurableMapConnect
          id={mapId}
          layers={[
            {
              ...publicLayers.radarLayer,
              style: 'precip-blue-transparent/nearest',
            },
          ]}
          shouldDisplayDrawControls
          mapControls={
            <>
              <MapControlButton
                title="Fake delete"
                data-testid="drawingToolButton"
                isActive={false}
              >
                <Delete />
              </MapControlButton>{' '}
              <MapControlButton
                title="Fake fir"
                data-testid="drawingToolButton"
                isActive={false}
              >
                <DrawFIRLand />
              </MapControlButton>
            </>
          }
        />
      </DemoWrapperConnect>
    </div>
  );
};
