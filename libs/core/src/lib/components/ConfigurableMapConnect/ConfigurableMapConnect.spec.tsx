/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, waitFor, screen } from '@testing-library/react';

import {
  uiTypes,
  defaultLayers,
  CoreAppStore,
  storeTestUtils,
  getSingularDrawtoolDrawLayerId,
  IS_LEGEND_OPEN_BY_DEFAULT,
  mapUtils,
} from '@opengeoweb/store';
import { publicLayers, emptyGeoJSON } from '@opengeoweb/webmap-react';
import {
  LayerType,
  mockGetCapabilities,
  setWMSGetCapabilitiesFetcher,
  webmapUtils,
} from '@opengeoweb/webmap';
import { dateUtils } from '@opengeoweb/shared';
import { getSpeedDelay } from '@opengeoweb/timeslider';
import {
  ConfigurableMapConnect,
  ConfigurableMapConnectProps,
} from './ConfigurableMapConnect';
import { DemoWrapperConnect } from '../Providers/Providers';
import { mockStateMapWithDimensions } from '../../utils/testUtils';
import { createMockStore } from '../../store';
import { multiDimensionLayer } from '../../utils/defaultTestSettings';
import { translateKeyOutsideComponents } from '../../utils/i18n';

describe('components/ConfigurableMap/ConfigurableMapConnect', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  it('should render map with default props', () => {
    const store = createMockStore();
    const mapId = 'test-map-1';
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: mapId,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('mapTitle')).toBeFalsy();
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();
    expect(screen.getByTestId('open-Legend')).toBeTruthy();
    expect(screen.queryByTestId('map-time')).toBeFalsy();
    expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();
    expect(screen.getByTestId('timeSlider')).toBeTruthy();

    const state: CoreAppStore = store.getState();
    expect(Object.keys(state.ui!.dialogs)).toEqual(
      expect.arrayContaining([
        `dockedLayerManager-${mapId}`,
        `legend-${mapId}`,
      ]),
    );

    expect(Object.keys(state.webmap!.byId)).toEqual([mapId]);

    expect(Object.keys(state.syncGroups!)).toEqual(
      expect.arrayContaining(['groups', 'sources', 'viewState']),
    );

    expect(screen.queryByTestId('mapControls')?.getAttribute('style')).toEqual(
      'top: 8px;',
    );
  });

  it('should not show the timeSlider', () => {
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-1',
      disableTimeSlider: true,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('mapTitle')).toBeFalsy();
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();
    expect(screen.getByTestId('open-Legend')).toBeTruthy();
    expect(screen.queryByTestId('map-time')).toBeFalsy();
    expect(screen.queryByTestId('timeSlider')).toBeFalsy();
    expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();
  });

  it('should render map with custom title', () => {
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-0',
      title: 'Custom map title',
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('mapTitle')!.textContent).toEqual(props.title);
    expect(screen.queryByTestId('mapControls')?.getAttribute('style')).toEqual(
      'top: 24px;',
    );
  });

  it('should render map with custom bbox', () => {
    const store = createMockStore({});
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-2',
      srs: 'EPSG:3575',
      initialBbox: {
        left: -13000000,
        bottom: -13000000,
        right: 13000000,
        top: 13000000,
      },
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();

    expect(store.getState().webmap.byId[props.id!].bbox).toEqual(
      props.initialBbox,
    );
    expect(store.getState().webmap.byId[props.id!].srs).toEqual(props.srs);
  });

  it('should set autoUpdate to false if not passed', async () => {
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      id: 'map1',
      layers: [],
    };

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().webmap.byId[props.id!].shouldAutoUpdate,
    ).toBeFalsy();
  });

  it('should set autoUpdate to true if passed as true', async () => {
    const store = createMockStore();
    const testShouldAutoUpdate = true;
    const props: ConfigurableMapConnectProps = {
      id: 'map1',
      layers: [],
      shouldAutoUpdate: testShouldAutoUpdate,
    };

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().webmap.byId[props.id!].isAutoUpdating).toBeTruthy();
  });

  it('should include zoomControls if shouldShowZoomControls not passed', () => {
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
    };

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();
  });

  it('should not include zoomControls if shouldShowZoomControls is not visible in store', () => {
    const mapId = 'test';
    const newMockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            shouldShowZoomControls: false,
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
    };
    const store = createMockStore(newMockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      shouldShowZoomControls: false,
      id: mapId,
    };

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('zoom-reset')).toBeFalsy();
    expect(screen.queryByTestId('zoom-in')).toBeFalsy();
    expect(screen.queryByTestId('zoom-out')).toBeFalsy();
  });

  it('should include timeSlider if showTimeSlider not passed', () => {
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
    };

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
  });

  it('should not include timeSlider is not visible', async () => {
    const mapId = 'test-1';
    const newMockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            isTimeSliderVisible: false,
            mapLayers: [],
            overLayers: [],
            baseLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
    };
    const store = createMockStore(newMockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      showTimeSlider: false,
      id: mapId,
    };

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('timeSlider')).toBeFalsy();
    });
  });

  it('should render map with autoanimate enabled', () => {
    const end = `2021-12-20T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(end).valueOf());
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
      shouldAnimate: true,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(store.getState().webmap.byId[props.id!].isAnimating).toBeTruthy();
  });

  it('should render map with autoanimate disabled when not given', () => {
    const end = `2021-12-20T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(end).valueOf());
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();

    expect(store.getState().webmap.byId[props.id!].isAnimating).toBeFalsy();
  });

  it('should render map with autoanimate enabled and animationPayload if passed', () => {
    jest.spyOn(Date, 'now').mockReturnValue(new Date().setHours(14, 0, 0, 0));
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
      shouldAnimate: true,
      animationPayload: { interval: 15, duration: 2 * 60 },
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();

    expect(store.getState().webmap.byId[props.id!].timeStep).toEqual(
      props.animationPayload?.interval,
    );

    expect(store.getState().webmap.byId[props.id!].isAnimating).toBeTruthy();
    const { animationStartTime, animationEndTime } =
      store.getState().webmap.byId[props.id!];

    expect(
      dateUtils.differenceInMinutes(animationEndTime, animationStartTime),
    ).toEqual(props.animationPayload?.duration);
  });

  it('should render map with autoanimate enabled and pass speed if passed in payload', () => {
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
      shouldAnimate: true,
      animationPayload: { speed: 8 },
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();

    expect(store.getState().webmap.byId[props.id!].isAnimating).toBeTruthy();
    expect(store.getState().webmap.byId[props.id!].animationDelay).toEqual(
      getSpeedDelay(props.animationPayload!.speed!),
    );
  });

  it('should render map with custom layers', async () => {
    const store = createMockStore();
    const autoLayerId = 'active';
    const props: ConfigurableMapConnectProps = {
      layers: [
        { ...publicLayers.radarLayer, id: autoLayerId },
        publicLayers.baseLayerWorldMap,
        defaultLayers.overLayer,
      ],
      id: 'test-map-4',
      autoTimeStepLayerId: autoLayerId,
      autoUpdateLayerId: autoLayerId,
    };

    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce(autoLayerId)
      .mockReturnValueOnce('layerid_geojsonlayer');

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('mapTitle')).toBeFalsy();

    await waitFor(() => {
      expect(store.getState().webmap.byId[props.id!].mapLayers).toHaveLength(1);
    });

    expect(store.getState().webmap.byId[props.id!].autoUpdateLayerId).toEqual(
      props.autoUpdateLayerId,
    );
    expect(store.getState().webmap.byId[props.id!].autoTimeStepLayerId).toEqual(
      props.autoTimeStepLayerId,
    );
  });

  it('should render map with default baselayer and overlayer if none given', async () => {
    const store = createMockStore();
    const autoLayerId = 'active';
    const props: ConfigurableMapConnectProps = {
      layers: [{ ...publicLayers.radarLayer, id: autoLayerId }],
      id: 'test-map-4',
      autoTimeStepLayerId: autoLayerId,
      autoUpdateLayerId: autoLayerId,
    };

    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce(autoLayerId)
      .mockReturnValueOnce('layerid_1')
      .mockReturnValueOnce('layerid_3')
      .mockReturnValueOnce('layerid_2');

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('mapTitle')).toBeFalsy();

    await waitFor(() => {
      expect(store.getState().webmap.byId[props.id!].mapLayers).toHaveLength(1);
    });

    const result = store.getState().webmap.byId[props.id!];
    expect(result.mapLayers.includes(props.layers[0].id)).toBeTruthy();
    expect(
      result.baseLayers.includes(defaultLayers.baseLayerGrey.id),
    ).toBeTruthy();
    expect(result.overLayers.includes(defaultLayers.overLayer.id)).toBeTruthy();
  });

  it('should render map with default baselayer and overlayer from defaultMapSettings', async () => {
    const state = {
      webmap: {
        byId: {},
        allIds: [],
        defaultMapSettings: {
          layers: [
            {
              name: 'WorldMap_Light_Grey_Canvas',
              type: 'twms',
              enabled: true,
              layerType: LayerType.baseLayer,
            },
            {
              service:
                'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserver?dataset=baselayers',
              name: 'countryborders',
              format: 'image/png',
              enabled: true,
              layerType: LayerType.overLayer,
            },
          ],
        },
      },
    };
    const store = createMockStore(state);
    const autoLayerId = 'active';
    const props: ConfigurableMapConnectProps = {
      layers: [{ ...publicLayers.radarLayer, id: autoLayerId }],
      id: 'test-map-4',
      autoTimeStepLayerId: autoLayerId,
      autoUpdateLayerId: autoLayerId,
    };
    const baseLayerId = 'layerid_2';
    const overLayerId = 'layerid_3';
    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce(autoLayerId)
      .mockReturnValueOnce('layerid_1')
      .mockReturnValueOnce(overLayerId)
      .mockReturnValueOnce(baseLayerId);

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    await waitFor(() => {
      expect(store.getState().webmap.byId[props.id!].mapLayers).toHaveLength(1);
    });
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('mapTitle')).toBeFalsy();

    const result = store.getState().webmap.byId[props.id!];

    expect(result.mapLayers.includes(autoLayerId)).toBeTruthy();
    expect(result.baseLayers.includes(baseLayerId)).toBeTruthy();
    expect(result.overLayers.includes(overLayerId)).toBeTruthy();

    const { byId } = store.getState().layers;
    expect(byId[autoLayerId].layerType).toEqual(LayerType.mapLayer);
    expect(byId[baseLayerId].layerType).toEqual(LayerType.baseLayer);
    expect(byId[overLayerId].layerType).toEqual(LayerType.overLayer);
  });
  it('should render map with maptime visible', () => {
    const mapId = 'test-map-5';
    const layer = {
      ...publicLayers.radarLayer,
      dimensions: [{ name: 'time', currentValue: '2021-12-07T13:30:00Z' }],
    };
    const customMockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStore(customMockState);
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: mapId,
      displayTimeInMap: true,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} useOl={false} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.getByTestId('map-time')).toBeTruthy();
  });

  it('should render map with LayerManagerButton and legend button', () => {
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      displayLayerManagerAndLegendButtonInMap: true,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();
    expect(screen.getByTestId('open-Legend')).toBeTruthy();
  });

  it('should render map with DimensionSelectButton', async () => {
    const mapId = 'test-map-7';
    const customMockState = mockStateMapWithDimensions(
      multiDimensionLayer,
      mapId,
    );
    const newMockState = { ...customMockState };
    const store = createMockStore(newMockState);
    const props: ConfigurableMapConnectProps = {
      layers: [multiDimensionLayer],
      id: mapId,
      displayDimensionSelectButtonInMap: true,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} useOl={true} />
      </DemoWrapperConnect>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('dimensionMapBtn-elevation')).toBeTruthy();
    });
  });

  it('should render map without LayerManagerButton or legend button', () => {
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      displayLayerManagerAndLegendButtonInMap: false,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    expect(screen.queryByTestId('layerManagerButton')).toBeFalsy();
    expect(screen.queryByTestId('open-Legend')).toBeFalsy();
  });

  it('should render map without DimensionSelectButton', async () => {
    const mapId = 'test-map-8';
    const customMockState = mockStateMapWithDimensions(
      multiDimensionLayer,
      mapId,
    );
    const newMockState = { ...customMockState };
    const store = createMockStore(newMockState);
    const props: ConfigurableMapConnectProps = {
      layers: [multiDimensionLayer],
      id: mapId,
      displayDimensionSelectButtonInMap: false,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    await waitFor(() => {
      expect(screen.getByTestId('ConfigurableMap')).toBeTruthy();
    });
    expect(screen.queryByTestId('dimensionMapBtn-elevation')).toBeFalsy();
  });

  it('should render map with toggleTimestepAuto using preset values', () => {
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'radarView',
      displayDimensionSelectButtonInMap: false,
      toggleTimestepAuto: false,
    };

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().webmap.byId[props.id!].isTimestepAuto).toEqual(
      props.toggleTimestepAuto,
    );

    expect(screen.queryByTestId('dimensionMapBtn-elevation')).toBeNull();
  });

  it('should render map with mapid in legend type', async () => {
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'radarView',
      displayDimensionSelectButtonInMap: false,
      toggleTimestepAuto: false,
      multiLegend: true,
    };

    const store = createMockStore();

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().ui.dialogs[`legend-${props.id}`]).toBeDefined();
    expect(store.getState().ui.dialogs[`legend-${props.id}`].isOpen).toEqual(
      IS_LEGEND_OPEN_BY_DEFAULT,
    );
  });

  it('should pass setTimestep value using preset animation interval values', () => {
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      displayDimensionSelectButtonInMap: false,
      id: 'radarView',
      animationPayload: {
        interval: 5,
      },
    };

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().webmap.byId[props.id!].timeStep).toEqual(
      props.animationPayload!.interval,
    );
  });

  it('should show layermanager', () => {
    const store = createMockStore({
      ui: {
        dialogs: {
          layerManager: {
            isOpen: false,
            type: 'layerManager',
            activeMapId: '',
            source: 'module' as const,
          },
        },
        order: ['layerManager'],
      },
    });
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'radarView',
      shouldShowLayerManager: true,
    };

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    const result = store.getState().ui.dialogs[`layerManager`];

    expect(result.type).toEqual(uiTypes.DialogTypes.LayerManager);
    expect(result.activeMapId).toEqual(props.id);
    expect(result.isOpen).toEqual(props.shouldShowLayerManager);
    expect(result.source).toEqual('app');
  });

  it('should show hide normal LayerManager and show dockedLayerManager', () => {
    const mapId = 'radarView';
    const store = createMockStore();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: mapId,
      shouldShowLayerManager: true,
      shouldShowDockedLayerManager: true,
    };

    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().ui.dialogs[`layerManager`]).toBeUndefined();
    expect(
      store.getState().ui.dialogs[
        `${uiTypes.DialogTypes.DockedLayerManager}-${'radarView'}`
      ].isOpen,
    ).toBeTruthy();
  });

  it('should register the DockedLayerManager', () => {
    const store = createMockStore();
    const mapId = 'mapid_1';
    const props: ConfigurableMapConnectProps = {
      id: mapId,
      layers: [],
      displayLayerManagerAndLegendButtonInMap: false,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().ui.dialogs[
        `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`
      ],
    ).toBeDefined();
  });

  it('should not show draw controls if not given', () => {
    const store = createMockStore();
    const mapId = 'mapid_1';
    const props: ConfigurableMapConnectProps = {
      id: mapId,
      layers: [],
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(screen.queryByTestId('drawingToolButton')).toBeFalsy();
  });

  it('should add a draw layer if shouldDisplayDrawControls is given', () => {
    const store = createMockStore();
    const mapId = 'mapid_1';
    const props: ConfigurableMapConnectProps = {
      id: mapId,
      layers: [],
      shouldDisplayDrawControls: true,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );

    const featureLayerId = getSingularDrawtoolDrawLayerId(mapId);

    expect(store.getState().layers.byId[featureLayerId]).toBeDefined();
    expect(store.getState().layers.byId[featureLayerId].geojson).toEqual(
      emptyGeoJSON,
    );
    expect(store.getState().layers.byId[featureLayerId].layerType).toEqual(
      LayerType.featureLayer,
    );

    expect(store.getState().webmap.byId[mapId].featureLayers).toEqual([
      featureLayerId,
    ]);
  });

  it('should render given mapControls', async () => {
    const buttonTitle = 'my new button';
    const store = createMockStore();
    const mapId = 'mapid_1';
    const props: ConfigurableMapConnectProps = {
      id: mapId,
      layers: [],
      mapControls: <p>{buttonTitle}</p>,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    expect(await screen.findByText(buttonTitle)).toBeTruthy();
  });

  it('should show location search', async () => {
    const store = createMockStore();
    const mapId = 'mapid_1';
    const props: ConfigurableMapConnectProps = {
      id: mapId,
      layers: [],
      displaySearchButtonInMap: true,
    };
    render(
      <DemoWrapperConnect store={store}>
        <ConfigurableMapConnect {...props} />
      </DemoWrapperConnect>,
    );
    await screen.findByRole('button', {
      name: translateKeyOutsideComponents('search-title-aria'),
    });
    expect(
      store.getState().ui.dialogs[`${uiTypes.DialogTypes.Search}-${mapId}`],
    ).toBeDefined();
  });
});
