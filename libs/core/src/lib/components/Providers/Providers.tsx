/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import i18n from 'i18next';
import { I18nextProvider } from 'react-i18next';
import { Theme } from '@mui/material';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Store } from '@reduxjs/toolkit';

import { SnackbarWrapperConnect } from '@opengeoweb/snackbar';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { initCoreReactI18n } from '../../utils/i18n';

interface CoreThemeProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
}

interface CoreTranslationWrapperProps {
  children?: React.ReactNode;
}

/**
 * A Provider component which provides the GeoWeb theme
 * @param children
 * @returns
 */
export const CoreThemeProvider: React.FC<CoreThemeProviderProps> = ({
  children,
  theme = lightTheme,
}: CoreThemeProviderProps) => {
  const queryClient = new QueryClient({
    defaultOptions: { queries: { retry: false } },
  });
  return (
    <ThemeWrapper theme={theme}>
      <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
    </ThemeWrapper>
  );
};

interface CoreThemeStoreProviderProps extends CoreThemeProviderProps {
  store: Store;
}

/**
 * A Provider component which provides the GeoWeb theme and store for the core.
 * Note: Should only be used with core components, as the provided store is only for core.
 * @param children
 * @returns
 */
export const CoreThemeStoreProvider: React.FC<CoreThemeStoreProviderProps> = ({
  children,
  theme = lightTheme,
  store,
}: CoreThemeStoreProviderProps) => (
  <Provider store={store}>
    <CoreThemeProvider theme={theme}>
      <SnackbarWrapperConnect>
        {children as React.ReactElement}
      </SnackbarWrapperConnect>
    </CoreThemeProvider>
  </Provider>
);

export const CoreI18nProvider: React.FC<CoreTranslationWrapperProps> = ({
  children,
}) => {
  initCoreReactI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};

// used for react stories/tests
export const DemoWrapper: React.FC<CoreThemeProviderProps> = (props) => (
  <CoreI18nProvider>
    <CoreThemeProvider {...props} />
  </CoreI18nProvider>
);

// used for redux stories/tests
export const DemoWrapperConnect: React.FC<CoreThemeStoreProviderProps> = (
  props,
) => (
  <CoreI18nProvider>
    <CoreThemeStoreProvider {...props} />
  </CoreI18nProvider>
);
