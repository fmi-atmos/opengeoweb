/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { ImageWMS } from 'ol/source';

import { layerTypes } from '@opengeoweb/store';
import {
  getWMSGetFeatureInfoRequestURL,
  webmapUtils,
  getMapDimURL,
  IWMJSMap,
} from '@opengeoweb/webmap';
import { MapLocation } from '@opengeoweb/webmap-react';

export interface GFIResult {
  data: string;
  isLoading: boolean;
}

export interface GFILayer {
  layerId: string;
  url: string;
  title: string;
}

/**
 * Get the title for this WMS layer based on its layerId. If not defined, layerId is used instead.
 * @param layerId
 * @returns Title for the layer
 */
const getTitle = (layerId: string): string => {
  const wmjsLayer = webmapUtils.getWMLayerById(layerId!);
  return (wmjsLayer ? wmjsLayer.title || wmjsLayer.name : layerId)!;
};

export const getFeatureInfoUrl = (
  layer: layerTypes.Layer,
  wmjsMap: IWMJSMap | undefined,
  mapPinLocation: MapLocation | null,
): string | undefined => {
  /*
    We need a wmjsLayer to build the getfeatureinfo url (getWMSGetFeatureInfoRequestURL function).
    But it seems that its dimension are lazely updated (by ReactMapView).
    Therefore make a clone of this layer and set its dimensions based on the layer object.
    Otherwise we risk making a getfeatureinfo call with outdated dimension values.
  */
  const wmjsLayer = webmapUtils.getWMLayerById(layer.id!).cloneLayer();
  if (wmjsMap) {
    // WebMapJS version
    return getWMSGetFeatureInfoRequestURL(
      wmjsLayer,
      wmjsMap.getMapPin().getXY().x,
      wmjsMap.getMapPin().getXY().y,
      'text/html',
    );
  }
  if (mapPinLocation) {
    // OpenLayers WMS version
    const source = new ImageWMS({
      url: wmjsLayer.getmapURL,
      params: {
        LAYERS: wmjsLayer.getLayerName(),
      },
    });

    return `${source.getFeatureInfoUrl(
      [mapPinLocation.projectionX!, mapPinLocation.projectionY!],
      mapPinLocation.resolution!,
      mapPinLocation.srs,
      {
        INFO_FORMAT: 'text/html',
      },
    )}&${getMapDimURL(wmjsLayer).toString()}`;
  }

  return undefined;
};

/**
 * Make a list of GFI Layers to display.
 * @param layers
 * @param mapId
 * @returns
 */
export const getLayersToUpdate = (
  mapPinLocation: MapLocation | null,
  layers: layerTypes.Layer[],
  mapId: string,
): GFILayer[] => {
  const wmjsMap = webmapUtils.getWMJSMapById(mapId);
  if (!layers || layers.length === 0 || (!wmjsMap && !mapPinLocation)) {
    return [];
  }

  return layers
    .filter(
      (layer: layerTypes.Layer) =>
        layer && layer.enabled && !!webmapUtils.getWMLayerById(layer.id!),
    )
    .map((layer: layerTypes.Layer): GFILayer => {
      const url = getFeatureInfoUrl(layer, wmjsMap, mapPinLocation);

      return {
        layerId: layer.id!,
        url: url!,
        title: getTitle(layer.id!),
      };
    });
};
