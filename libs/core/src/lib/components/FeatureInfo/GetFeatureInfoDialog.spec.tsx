/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import { WMJSMap, WMLayer, webmapUtils } from '@opengeoweb/webmap';
import axios from 'axios';
import { mapUtils, uiTypes } from '@opengeoweb/store';
import {
  defaultReduxLayerRadarColor,
  defaultReduxLayerRadarKNMI,
} from '../../utils/defaultTestSettings';

import { DemoWrapperConnect } from '../Providers/Providers';
import GetFeatureInfoDialog from './GetFeatureInfoDialog';
import { createMockStore } from '../../store';

describe('src/components/GetFeatureInfo/GetFeatureInfoDialog', () => {
  it('should follow the isOpen property to determine if open or closed, clicking on the cross should call onClose prop', () => {
    const mockState = {
      ui: {
        dialogs: {
          getfeatureinfo: {
            type: 'getfeatureinfo' as uiTypes.DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },
          layerManager: {
            type: uiTypes.DialogTypes.LayerManager,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: ['layerManager', 'getfeatureinfo'],
      },
    };
    const store = createMockStore(mockState);

    const props = {
      layers: [defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };
    render(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoDialog {...props} />
      </DemoWrapperConnect>,
    );

    // getfeatureinfo dialog should be opened
    expect(screen.getByTestId('moveable-getfeatureinfo')).toBeTruthy();

    // close the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
  });

  it('should show layer results', async () => {
    const mapId1 = 'map1';
    const layerId1 = 'layerId1';
    const mockUiState = {
      dialogs: {
        getfeatureinfo: {
          type: 'getfeatureinfo' as uiTypes.DialogType,
          activeMapId: mapId1,
          isOpen: false,
        },
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: ['layerManager', 'getfeatureinfo'],
    };
    const mockState = {
      webmap: {
        byId: {
          mapId123: {
            ...mapUtils.createMap({ id: 'mapId123' }),
            baseLayers: [],
            mapLayers: [layerId1],
          },
        },
        allIds: ['mapId123'],
      },
      ui: mockUiState,
    };
    const store = createMockStore(mockState);

    const props = {
      layers: [defaultReduxLayerRadarColor],
      isOpen: true,
      onClose: jest.fn(),
      mapId: mapId1,
      mapPinLocation: { lat: 1, lon: 2 },
    };
    axios.get = jest
      .fn()
      .mockResolvedValueOnce({ data: 'result-for-layerid-1-OK' });

    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);

    webmapUtils.registerWMJSMap(wmjsmap, mapId1);
    const radarLayer = new WMLayer(defaultReduxLayerRadarColor);
    radarLayer.parentMap = wmjsmap;
    webmapUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);
    wmjsmap.mapPin.setMapPin(1, 2);

    const spy = jest.spyOn(window, 'open').mockImplementationOnce(() => null);

    const { rerender } = render(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoDialog {...props} />
      </DemoWrapperConnect>,
    );

    // getfeatureinfo dialog should be opened
    expect(screen.getByTestId('moveable-getfeatureinfo')).toBeTruthy();

    expect(screen.getByTestId('click-on-map-for-info')).toBeTruthy();

    /* Now click on the map */
    const props2 = {
      ...props,
      mapPinLocation: { lat: 3, lon: 3 },
    };
    wmjsmap.mapPin.setMapPin(3, 3);

    rerender(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoDialog {...props2} />
      </DemoWrapperConnect>,
    );

    /* Make sure proper text content was set */
    await waitFor(() => {
      expect(
        screen.getByTestId('layer-result-ready-layerid_1').textContent,
      ).toBe('result-for-layerid-1-OK');
    });

    expect(spy).not.toHaveBeenCalled();
    fireEvent.click(
      screen.getByLabelText('Open layer featureinfo in another page'),
    );

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining(defaultReduxLayerRadarColor.service),
    );
    expect(spy).toHaveBeenCalledWith(
      expect.stringContaining('REQUEST=GetFeatureInfo'),
    );

    // close the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
    webmapUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
    webmapUtils.unRegisterWMJSMap(mapId1);
  });

  it('should show layer error', async () => {
    const mapId1 = 'map1';
    const layerId1 = 'layerId1';
    const mockUiState = {
      dialogs: {
        getfeatureinfo: {
          type: 'getfeatureinfo' as uiTypes.DialogType,
          activeMapId: mapId1,
          isOpen: false,
        },
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: ['layerManager', 'getfeatureinfo'],
    };
    const mockState = {
      webmap: {
        byId: {
          mapId123: {
            ...mapUtils.createMap({ id: 'mapId123' }),
            baseLayers: [],
            mapLayers: [layerId1],
          },
        },
        allIds: ['mapId123'],
      },
      ui: mockUiState,
    };
    const store = createMockStore(mockState);

    const props = {
      layers: [defaultReduxLayerRadarColor],
      isOpen: true,
      onClose: jest.fn(),
      mapId: mapId1,
      mapPinLocation: { lat: 1, lon: 2 },
    };
    axios.get = jest.fn().mockRejectedValueOnce(new Error('Layer error'));

    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);

    webmapUtils.registerWMJSMap(wmjsmap, mapId1);
    const radarLayer = new WMLayer(defaultReduxLayerRadarColor);
    radarLayer.parentMap = wmjsmap;
    webmapUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);

    wmjsmap.mapPin.setMapPin(1, 2);

    const { rerender } = render(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoDialog {...props} />
      </DemoWrapperConnect>,
    );

    // getfeatureinfo dialog should be opened
    expect(screen.getByTestId('moveable-getfeatureinfo')).toBeTruthy();
    expect(screen.getByTestId('click-on-map-for-info')).toBeTruthy();

    /* Now click on the map */
    const props2 = {
      ...props,
      mapPinLocation: { lat: 3, lon: 3 },
    };
    wmjsmap.mapPin.setMapPin(3, 3);

    rerender(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoDialog {...props2} />
      </DemoWrapperConnect>,
    );

    /* Make sure proper error text was set */
    await waitFor(() => {
      expect(
        screen.getByTestId('layer-result-ready-layerid_1').textContent,
      ).toBe('Layer error');
    });
    webmapUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
    webmapUtils.unRegisterWMJSMap(mapId1);
  });

  it('should not crash when using empty layers', async () => {
    const mapId1 = 'map1';
    const layerId1 = 'layerId1';
    const mockUiState = {
      dialogs: {
        getfeatureinfo: {
          type: 'getfeatureinfo' as uiTypes.DialogType,
          activeMapId: mapId1,
          isOpen: false,
        },
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: ['layerManager', 'getfeatureinfo'],
    };
    const mockState = {
      webmap: {
        byId: {
          mapId123: {
            ...mapUtils.createMap({ id: 'mapId123' }),
            baseLayers: [],
            mapLayers: [layerId1],
          },
        },
        allIds: ['mapId123'],
      },
      ui: mockUiState,
    };
    const store = createMockStore(mockState);

    const props = {
      layers: [defaultReduxLayerRadarColor],
      isOpen: true,
      onClose: jest.fn(),
      mapId: mapId1,
      mapPinLocation: { lat: 1, lon: 2 },
    };
    axios.get = jest
      .fn()
      .mockResolvedValueOnce({ data: 'result-for-layerid-1-OK' });

    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);

    webmapUtils.registerWMJSMap(wmjsmap, mapId1);
    const radarLayer = new WMLayer({
      ...defaultReduxLayerRadarColor,
      name: undefined,
    });
    radarLayer.parentMap = wmjsmap;

    webmapUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);
    wmjsmap.mapPin.setMapPin(1, 2);

    const { rerender } = render(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoDialog {...props} />
      </DemoWrapperConnect>,
    );

    // getfeatureinfo dialog should be opened
    await waitFor(() => {
      expect(screen.getByTestId('moveable-getfeatureinfo')).toBeTruthy();
    });
    expect(screen.getByTestId('click-on-map-for-info')).toBeTruthy();

    /* Now click on the map */
    const props2 = {
      ...props,
      mapPinLocation: { lat: 3, lon: 3 },
    };
    wmjsmap.mapPin.setMapPin(3, 3);

    rerender(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoDialog {...props2} />
      </DemoWrapperConnect>,
    );

    /* Wait for 'layer-result-ready-layerid_1' not to be present */
    await waitFor(() => {
      expect(screen.queryByTestId('layer-result-ready-layerid_1')).toBeFalsy();
    });

    // close the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('closeBtn'));
    await waitFor(() => expect(props.onClose).toHaveBeenCalled());
    webmapUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
    webmapUtils.unRegisterWMJSMap(mapId1);
  });

  it('should filter XSS attempts', async () => {
    const mapId1 = 'map1';
    const layerId1 = 'layerId1';
    const mockUiState = {
      dialogs: {
        getfeatureinfo: {
          type: 'getfeatureinfo' as uiTypes.DialogType,
          activeMapId: mapId1,
          isOpen: false,
        },
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
      order: ['layerManager', 'getfeatureinfo'],
    };
    const mockState = {
      webmap: {
        byId: {
          mapId123: {
            ...mapUtils.createMap({ id: 'mapId123' }),
            baseLayers: [],
            mapLayers: [layerId1],
          },
        },
        allIds: ['mapId123'],
      },
      ui: mockUiState,
    };
    const store = createMockStore(mockState);

    const props = {
      layers: [defaultReduxLayerRadarColor],
      isOpen: true,
      onClose: jest.fn(),
      mapId: mapId1,
      mapPinLocation: { lat: 1, lon: 2 },
    };
    axios.get = jest.fn().mockResolvedValueOnce({
      data: '<script>alert("bla");</script>XSS is filtered out',
    });

    const baseElement = document.createElement('div');
    const wmjsmap = new WMJSMap(baseElement);

    webmapUtils.registerWMJSMap(wmjsmap, mapId1);
    const radarLayer = new WMLayer(defaultReduxLayerRadarColor);
    radarLayer.parentMap = wmjsmap;
    webmapUtils.registerWMLayer(radarLayer, defaultReduxLayerRadarColor.id);
    wmjsmap.mapPin.setMapPin(1, 2);

    const { rerender } = render(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoDialog {...props} />
      </DemoWrapperConnect>,
    );

    // getfeatureinfo dialog should be opened
    expect(screen.getByTestId('moveable-getfeatureinfo')).toBeTruthy();

    expect(screen.getByTestId('click-on-map-for-info')).toBeTruthy();

    /* Now click on the map */
    const props2 = {
      ...props,
      mapPinLocation: { lat: 3, lon: 3 },
    };
    wmjsmap.mapPin.setMapPin(3, 3);

    rerender(
      <DemoWrapperConnect store={store}>
        <GetFeatureInfoDialog {...props2} />
      </DemoWrapperConnect>,
    );

    /* Make sure proper text content was set */
    await waitFor(() => {
      expect(screen.getByTestId('layer-result-ready-layerid_1').innerHTML).toBe(
        'XSS is filtered out',
      );
    });

    // close the getfeatureinfo dialog
    fireEvent.click(screen.getByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
    webmapUtils.unRegisterWMJSLayer(defaultReduxLayerRadarColor.id);
    webmapUtils.unRegisterWMJSMap(mapId1);
  });
});
