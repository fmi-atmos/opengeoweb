/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { produce } from 'immer';
import { mapTypes, storeTestUtils } from '@opengeoweb/store';

import { webmapTestSettings } from '@opengeoweb/webmap';
import { DemoWrapperConnect } from '../Providers/Providers';
import { TimeSliderClockConnect } from './TimeSliderClockConnect';
import { createMockStore } from '../../store';

describe('components/TimeSlider/TimeSliderClock/TimeSliderClockConnect', () => {
  const mapId = 'mapid_1';
  it('renders correctly', () => {
    const mockState = {
      webmap: {
        allIds: [],
        byId: {
          [mapId]: {
            dimensions: [
              {
                name: 'time',
                units: 'ISO8601',
                currentValue: '2020-03-13T13:30:00Z',
              },
            ],
            isTimeSliderVisible: true,
          } as mapTypes.WebMap,
        },
      } as mapTypes.WebMapState,
    };
    const store = createMockStore(mockState);
    render(
      <DemoWrapperConnect store={store}>
        <TimeSliderClockConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );
    screen.getByText('Fri 13 Mar 13:30 UTC');

    expect(
      screen.queryByRole('button', { name: /animation options/i }),
    ).not.toBeInTheDocument();
  });

  it('handles button clicks', () => {
    const mockState = produce(
      storeTestUtils.mockStateMapWithLayer(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        mapId,
      ),
      (draft) => {
        draft.webmap!.byId[mapId].isTimeSliderVisible = false;
      },
    );
    const store = createMockStore(mockState);

    expect(store.getState().webmap.byId[mapId].isAnimating).toBeFalsy();
    render(
      <DemoWrapperConnect store={store}>
        <TimeSliderClockConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );

    fireEvent.click(screen.getByRole('button', { name: /animation options/i }));

    fireEvent.click(screen.getByTestId('play-svg-path'));

    expect(store.getState().webmap.byId[mapId].isAnimating).toBeTruthy();
  });
});
