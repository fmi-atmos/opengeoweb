/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { setupServer } from 'msw/node';
import { layerTypes, mapActions } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';
import { emptyGeoJSON, MapFeatureClass } from '@opengeoweb/webmap-react';
import {
  registerWMJSMap,
  unRegisterAllWMJSLayersAndMaps,
  WMJSMap,
} from '@opengeoweb/webmap';
import { DemoWrapperConnect } from '../Providers/Providers';
import { SearchControlButtonConnect } from './SearchControlButtonConnect';
import { SearchDialogConnect } from './SearchDialogConnect';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store';
import { MY_LOCATION } from './MyMapLocation';
import {
  fakeLocationDetailList,
  fakeLocationList,
  locationApiEndpoints,
} from '../../utils/location-api/fakeApi';
import { geometryStyling } from './utils';

const server = setupServer(...locationApiEndpoints);

describe('src/components/Search/SearchDialogConnect', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  it('should open and close search dialog', async () => {
    const store = createMockStore();
    const mapId = 'search1';
    render(
      <DemoWrapperConnect store={store}>
        <SearchControlButtonConnect mapId={mapId} />
        <SearchDialogConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );

    expect(
      screen.queryByLabelText(
        translateKeyOutsideComponents('search-title-long'),
      ),
    ).toBeFalsy();

    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    );

    expect(
      screen.getByLabelText(translateKeyOutsideComponents('search-title-long')),
    ).toBeTruthy();

    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    );

    expect(
      screen.queryByLabelText(
        translateKeyOutsideComponents('search-title-long'),
      ),
    ).toBeFalsy();
  });

  it('should select my location and keep it when closing the search', async () => {
    unRegisterAllWMJSLayersAndMaps();
    const store = createMockStore();
    const mapId = 'search2';
    Object.defineProperty(global.navigator, 'geolocation', {
      value: {
        getCurrentPosition: (success: PositionCallback) =>
          success({
            coords: {
              latitude: 12,
              longitude: 24,
            } as GeolocationCoordinates,
          } as GeolocationPosition),
      },
      configurable: true,
    });

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
    });

    const map = new WMJSMap(document.createElement('div'));
    registerWMJSMap(map, mapId);
    const zoomSpy = jest.spyOn(map, 'calculateBoundingBoxAndZoom');

    render(
      <DemoWrapperConnect store={store}>
        <SearchControlButtonConnect mapId={mapId} />
        <SearchDialogConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );

    // open search
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    );
    expect(
      screen.getByLabelText(translateKeyOutsideComponents('search-title-long')),
    ).toBeTruthy();

    // select my location
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-my-location'),
      }),
    );

    const reduxLayer = Object.values(
      store.getState().layers.byId,
    )[1] as layerTypes.ReduxLayer;

    expect(reduxLayer.geojson?.features[0].geometry).toEqual({
      coordinates: [24, 12],
      type: 'Point',
    });

    expect(reduxLayer.geojson!.features[0].properties).toEqual({
      drawFunctionId: expect.any(String),
      hoverDrawFunctionId: expect.any(String),
      name: MY_LOCATION,
      mapFeatureClass: MapFeatureClass.MyLocation,
    });

    // it should zoom to my location
    expect(zoomSpy).toHaveBeenCalledWith(12, 24);

    // close search
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    );

    expect(
      screen.queryByLabelText(
        translateKeyOutsideComponents('search-title-long'),
      ),
    ).toBeFalsy();
    const reduxLayer2 = Object.values(
      store.getState().layers.byId,
    )[1] as layerTypes.ReduxLayer;
    expect(reduxLayer2.geojson?.features[0].geometry).toEqual({
      coordinates: [24, 12],
      type: 'Point',
    });

    // open it again
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    );
    expect(
      screen.getByLabelText(translateKeyOutsideComponents('search-title-long')),
    ).toBeTruthy();

    // unselect my location
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-my-location'),
      }),
    );

    const reduxLayer3 = Object.values(
      store.getState().layers.byId,
    )[1] as layerTypes.ReduxLayer;

    expect(reduxLayer3.geojson).toEqual(emptyGeoJSON);
  });

  it('should select a location from the list and keep it when closing the search', async () => {
    unRegisterAllWMJSLayersAndMaps();
    const store = createMockStore();
    const mapId = 'search3';
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
    });
    const map = new WMJSMap(document.createElement('div'));
    registerWMJSMap(map, mapId);
    const zoomSpy = jest.spyOn(map, 'calculateBoundingBoxAndZoom');

    render(
      <DemoWrapperConnect store={store}>
        <SearchControlButtonConnect mapId={mapId} />
        <SearchDialogConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );

    // open search
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    );
    await userEvent.type(
      screen.getByLabelText(translateKeyOutsideComponents('search-title-long')),
      'A',
    );
    fireEvent.click(await screen.findByText(fakeLocationList[0].name));

    await waitFor(() =>
      expect(
        screen
          .getByLabelText(translateKeyOutsideComponents('search-title-long'))
          .getAttribute('value'),
      ).toEqual(fakeLocationList[0].name),
    );

    // wait for the layer to be updated
    await waitFor(() =>
      expect(
        (
          Object.values(
            store.getState().layers.byId,
          )[0] as layerTypes.ReduxLayer
        ).geojson?.features[0],
      ).toBeTruthy(),
    );

    const reduxLayer = Object.values(
      store.getState().layers.byId,
    )[0] as layerTypes.ReduxLayer;
    expect(reduxLayer.geojson?.features[0].geometry).toEqual({
      coordinates: [
        fakeLocationDetailList[0].lon,
        fakeLocationDetailList[0].lat,
      ],
      type: 'Point',
    });

    expect(reduxLayer.geojson?.features[0].properties).toEqual({
      drawFunctionId: expect.any(String),
      hoverDrawFunctionId: expect.any(String),
      name: fakeLocationDetailList[0].name,
      mapFeatureClass: MapFeatureClass.GenericMarker,
    });

    // it should zoom to selected location
    expect(zoomSpy).toHaveBeenCalledWith(
      fakeLocationDetailList[0].lat,
      fakeLocationDetailList[0].lon,
    );

    // close search
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    );

    // check selected location is still visible
    const reduxLayer2 = Object.values(
      store.getState().layers.byId,
    )[0] as layerTypes.ReduxLayer;
    expect(reduxLayer2.geojson?.features[0].geometry).toEqual({
      coordinates: [
        fakeLocationDetailList[0].lon,
        fakeLocationDetailList[0].lat,
      ],
      type: 'Point',
    });
  });

  it('should remove location and input on clear', async () => {
    const store = createMockStore();
    const mapId = 'search4';
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
    });
    render(
      <DemoWrapperConnect store={store}>
        <SearchControlButtonConnect mapId={mapId} />
        <SearchDialogConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );

    // open search
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    );
    await userEvent.type(
      screen.getByLabelText(translateKeyOutsideComponents('search-title-long')),
      'A',
    );

    await userEvent.click(await screen.findByText(fakeLocationList[0].name));

    expect(
      screen
        .getByLabelText(translateKeyOutsideComponents('search-title-long'))
        .getAttribute('value'),
    ).toEqual(fakeLocationList[0].name);

    // wait for the layer to be updated
    await waitFor(() => {
      const reduxLayer = Object.values(
        store.getState().layers.byId,
      )[0] as layerTypes.ReduxLayer;
      expect(reduxLayer.geojson?.features[0].geometry).toEqual({
        coordinates: [
          fakeLocationDetailList[0].lon,
          fakeLocationDetailList[0].lat,
        ],
        type: 'Point',
      });
    });

    await userEvent.click(await screen.findByLabelText('Clear'));
    expect(
      screen
        .getByLabelText(translateKeyOutsideComponents('search-title-long'))
        .getAttribute('value'),
    ).toEqual('');
    // wait for the layer to be updated
    await waitFor(() => {
      const reduxLayer = Object.values(
        store.getState().layers.byId,
      )[0] as layerTypes.ReduxLayer;
      expect(reduxLayer.geojson).toEqual(emptyGeoJSON);
    });
  });

  it('should select a location from the list and display the geometry', async () => {
    const store = createMockStore();
    const mapId = 'search5';
    const location = fakeLocationList[5];
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
    });

    render(
      <DemoWrapperConnect store={store}>
        <SearchControlButtonConnect mapId={mapId} />
        <SearchDialogConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );

    // open search
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    );
    await userEvent.type(
      screen.getByLabelText(translateKeyOutsideComponents('search-title-long')),
      'A',
    );

    await userEvent.click(await screen.findByText(location.name));

    expect(
      screen
        .getByLabelText(translateKeyOutsideComponents('search-title-long'))
        .getAttribute('value'),
    ).toEqual(location.name);

    // wait for the layer to be updated
    await waitFor(() =>
      expect(
        (
          Object.values(
            store.getState().layers.byId,
          )[0] as layerTypes.ReduxLayer
        ).geojson?.features[0],
      ).toBeTruthy(),
    );

    const reduxLayer = Object.values(
      store.getState().layers.byId,
    )[0] as layerTypes.ReduxLayer;
    const detail = fakeLocationDetailList.find(
      (item) => item.id === location.id,
    );
    expect(reduxLayer.geojson?.features[0]).toEqual({
      ...detail!.geometry,
      properties: {
        name: detail!.name,
        hoverDrawFunctionId: expect.any(String),
        ...geometryStyling,
      },
    });
  });
});
