/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { produce } from 'immer';
import { storeTestUtils, WebMapStateModuleState } from '@opengeoweb/store';
import { webmapTestSettings } from '@opengeoweb/webmap';
import { defaultTimeStep } from '@opengeoweb/timeslider';
import { TimeStepButtonConnect } from './TimeStepButtonConnect';
import { DemoWrapper } from '../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { createMockStore } from '../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/TimeStepButton/TimeStepButtonConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };
  const user = userEvent.setup();

  it('should render an enabled time step menu and show a correct time step text', () => {
    const mockState = storeTestUtils.mockStateMapWithMultipleLayers(
      [webmapTestSettings.multiDimensionLayer3],
      mapId,
    );
    const store = createMockStore(mockState);

    render(
      <DemoWrapper>
        <Provider store={store}>
          <TimeStepButtonConnect {...props} />
        </Provider>
      </DemoWrapper>,
    );
    const timeStepButton = screen.getByRole('button', {
      name: translateKeyOutsideComponents('timeslider-time-step'),
    });

    expect(timeStepButton).toBeInTheDocument();

    expect(timeStepButton).not.toHaveClass('Mui-disabled');

    expect(timeStepButton).toHaveTextContent('5m');
  });

  it('TimeStepMenu renders, when clicked open and should change time step in store', async () => {
    const mockStateMap: WebMapStateModuleState =
      storeTestUtils.mockStateMapWithMultipleLayers(
        [webmapTestSettings.multiDimensionLayer3],
        mapId,
      );
    const mockState = produce(mockStateMap, (draft) => {
      draft.webmap!.byId[mapId].isTimestepAuto = false;
    });
    const store = createMockStore(mockState);
    render(
      <DemoWrapper>
        <Provider store={store}>
          <TimeStepButtonConnect {...props} />
        </Provider>
      </DemoWrapper>,
    );

    const timeStepButton = screen.getByRole('button', {
      name: translateKeyOutsideComponents('timeslider-time-step'),
    });

    expect(store.getState().webmap.byId[mapId].timeStep).toEqual(
      defaultTimeStep,
    );
    await user.click(timeStepButton);
    expect(screen.getByRole('menu')).toBeInTheDocument();

    await user.click(
      screen.getByRole('menuitem', {
        name: `10 ${translateKeyOutsideComponents('timeslider-years')}`,
      }),
    );

    expect(store.getState().webmap.byId[mapId].timeStep).toEqual(5256000);

    await user.click(timeStepButton);
    await user.click(screen.getByRole('menuitem', { name: /1 min/i }));

    expect(store.getState().webmap.byId[mapId].timeStep).toEqual(1);
  });
});
