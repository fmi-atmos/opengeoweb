/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { act, render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { mapActions } from '@opengeoweb/store';
import { renderCounter } from '@opengeoweb/shared';
import { BackwardForwardStepButton } from '@opengeoweb/timeslider';
import { BackwardForwardStepButtonConnect } from './BackwardForwardStepButtonConnect';
import { DemoWrapper } from '../../Providers/Providers';
import { createMockStore } from '../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/BackwardForwardStepButtonConnect', () => {
  beforeEach(() => {
    renderCounter.reset();
  });
  it('should not re-render when time dimension is updated', async () => {
    const mapId = 'mapId-1';
    const currentTime = '2024-01-01T00:00:00Z';
    const store = createMockStore();
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <Provider store={store}>
        <DemoWrapper>
          <BackwardForwardStepButtonConnect mapId={mapId} />
        </DemoWrapper>
      </Provider>,
    );

    expect(renderCounter.get(BackwardForwardStepButton.name)).toBe(1);

    // change map time to be outside layer acceptance time
    act(() => {
      store.dispatch(
        mapActions.mapChangeDimension({
          mapId,
          dimension: {
            name: 'time',
            currentValue: currentTime,
          },
          origin: 'map',
        }),
      );
    });

    // Button should not have been re-rendered
    expect(renderCounter.get(BackwardForwardStepButton.name)).toBe(1);
  });
  it('should trigger setStepBackwardOrForward action when clicked', async () => {
    const mapId = 'mapId-1';
    const store = createMockStore();
    store.dispatch(mapActions.registerMap({ mapId }));

    expect(renderCounter.get(BackwardForwardStepButton.name)).toBe(0);

    const setStepBackwardOrForwardActionSpy = jest.spyOn(
      mapActions,
      'setStepBackwardOrForward',
    );
    render(
      <Provider store={store}>
        <DemoWrapper>
          <BackwardForwardStepButtonConnect mapId={mapId} />
        </DemoWrapper>
      </Provider>,
    );

    expect(setStepBackwardOrForwardActionSpy).not.toHaveBeenCalled();

    const button = screen.getByTestId('step-backward-svg-path');

    fireEvent.click(button);

    expect(setStepBackwardOrForwardActionSpy).toHaveBeenCalledWith({
      isForwardStep: false,
      mapId: 'mapId-1',
    });

    // Button should not have been re-rendered
    expect(renderCounter.get(BackwardForwardStepButton.name)).toBe(1);
  });
});
