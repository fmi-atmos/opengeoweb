/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import {
  initialState,
  mapUtils,
  syncConstants,
  syncGroupsTypes,
} from '@opengeoweb/store';
import { PlayButtonConnect } from './PlayButtonConnect';
import { DemoWrapperConnect } from '../../Providers/Providers';
import { createMockStore } from '../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/PlayButton/PlayButtonConnect', () => {
  const dimensions = [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ];

  const mapId = 'map-1';
  it('should start playing animation with the correct start and end times when clicked while map is not yet animating', () => {
    const props = {
      mapId,
      isAnimating: false,
    };
    const startValue = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { hours: 6 }),
    );
    const endValue = dateUtils.dateToString(
      dateUtils.sub(dateUtils.utc(), { minutes: 10 }),
    );
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            isAnimating: false,
            timeStep: 15,
            animationStartTime: startValue,
            animationEndTime: endValue,
            dimensions,
          },
        },
        allIds: [mapId],
      },
      syncGroups: {
        ...initialState,
        groups: {
          ...initialState.groups,
          byId: {
            Time_A: {
              payloadByType:
                syncConstants.SYNCGROUPS_TYPE_SETBBOX as unknown as syncGroupsTypes.SynchronizationSourcePayLoadByType,
              type: 'SYNCGROUPS_TYPE_SETTIME' as const,
              targets: {
                byId: {
                  [mapId]: {
                    linked: false,
                  },
                },
                allIds: [mapId],
              },
            } as syncGroupsTypes.SynchronizationGroup,
          },
          allIds: ['Time_A'],
        },
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <PlayButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().webmap.byId[mapId].isAnimating).toBeFalsy();

    const button = screen.getByRole('button');
    expect(button).toBeTruthy();

    fireEvent.click(button);

    const result = store.getState().webmap.byId[mapId];

    expect(result.isAnimating).toBeTruthy();
    expect(result.animationStartTime).toEqual(startValue);
    expect(result.animationEndTime).toEqual(endValue);
    expect(result.timeStep).toEqual(15);
  });

  it('should stop playing animation when clicked while map is animating', () => {
    const props = {
      mapId,
      isAnimating: true,
    };
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            ...mapUtils.createMap({ id: mapId }),
            isAnimating: true,
            timeStep: 15,
            dimensions,
          },
        },
        allIds: [mapId],
      },
      syncGroups: {
        ...initialState,
        groups: {
          ...initialState.groups,
          byId: {
            Time_A: {
              payloadByType:
                syncConstants.SYNCGROUPS_TYPE_SETBBOX as unknown as syncGroupsTypes.SynchronizationSourcePayLoadByType,
              type: 'SYNCGROUPS_TYPE_SETTIME' as const,
              targets: {
                byId: {
                  [mapId]: {
                    linked: false,
                  },
                },
                allIds: [mapId],
              },
            } as syncGroupsTypes.SynchronizationGroup,
          },
          allIds: ['Time_A'],
        },
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <PlayButtonConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(store.getState().webmap.byId[mapId].isAnimating).toBeTruthy();
    const button = screen.getByRole('button');
    expect(button).toBeTruthy();

    fireEvent.click(button);

    expect(store.getState().webmap.byId[mapId].isAnimating).toBeFalsy();
  });
});
