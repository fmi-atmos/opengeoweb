/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import { IS_LEGEND_OPEN_BY_DEFAULT, mapUtils } from '@opengeoweb/store';
import { LegendConnect } from '.';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

describe('src/components/Legend/LegendConnect', () => {
  it('should register dialog', async () => {
    const mockState = {
      webmap: {
        byId: {
          mapId123: {
            ...mapUtils.createMap({ id: 'mapId123' }),
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: ['mapId123'],
      },
      ui: {
        dialogs: {},
        order: [],
      },
      layers: {
        byId: {},
        allIds: [],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LegendConnect mapId="mapId123" />
      </DemoWrapperConnect>,
    );

    const result = store.getState().ui.dialogs['legend'];
    expect(result).toBeTruthy();
    expect(result.type).toEqual('legend');
    expect(result.activeMapId).toEqual('');
    expect(result.isOpen).toEqual(IS_LEGEND_OPEN_BY_DEFAULT);
    expect(result.source).toEqual('app');
  });

  it('should register multilegend', async () => {
    const mockState = {
      webmap: {
        byId: {
          mapId123: {
            ...mapUtils.createMap({ id: 'mapId123' }),
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: ['mapId123'],
      },
      ui: {
        dialogs: {
          legend: {
            type: 'legend',
            isOpen: true,
            activeMapId: '',
          },
        },
        order: ['legend'],
      },
      layers: {
        byId: {},
        allIds: [],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LegendConnect mapId="mapId123" multiLegend={true} />
      </DemoWrapperConnect>,
    );

    const result = store.getState().ui.dialogs['legend-mapId123'];
    expect(result).toBeTruthy();
    expect(result.type).toEqual('legend-mapId123');
    expect(result.activeMapId).toEqual('');
    expect(result.isOpen).toEqual(IS_LEGEND_OPEN_BY_DEFAULT);
    expect(result.source).toEqual('app');
  });

  it('should register 2 x multilegends', async () => {
    const mockState = {
      webmap: {
        byId: {
          mapId123: {
            ...mapUtils.createMap({ id: 'mapId123' }),
            baseLayers: [],
            mapLayers: [],
          },
          mapId456: {
            ...mapUtils.createMap({ id: 'mapId456' }),
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: ['mapId123', 'mapId456'],
      },
      ui: {
        dialogs: {
          legend: {
            type: 'legend',
            isOpen: true,
            activeMapId: '',
          },
        },
        order: ['legend'],
      },
      layers: {
        byId: {},
        allIds: [],
        availableBaseLayers: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStore(mockState);

    render(
      <DemoWrapperConnect store={store}>
        <LegendConnect mapId="mapId123" multiLegend={true} />
        <LegendConnect mapId="mapId456" multiLegend={true} />
      </DemoWrapperConnect>,
    );

    const result = store.getState().ui.dialogs['legend-mapId123'];
    expect(result).toBeTruthy();
    expect(result.type).toEqual('legend-mapId123');
    expect(result.activeMapId).toEqual('');
    expect(result.isOpen).toEqual(IS_LEGEND_OPEN_BY_DEFAULT);
    expect(result.source).toEqual('app');

    const resultLegend2 = store.getState().ui.dialogs['legend-mapId456'];
    expect(resultLegend2).toBeTruthy();
    expect(resultLegend2.type).toEqual('legend-mapId456');
    expect(resultLegend2.activeMapId).toEqual('');
    expect(resultLegend2.isOpen).toEqual(IS_LEGEND_OPEN_BY_DEFAULT);
    expect(resultLegend2.source).toEqual('app');
  });
});
