/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { storeTestSettings, uiTypes } from '@opengeoweb/store';
import { LayerType, webmapUtils } from '@opengeoweb/webmap';
import {
  dimensionConfig,
  WEBMAP_REACT_NAMESPACE,
} from '@opengeoweb/webmap-react';
import DimensionSelectDialogConnect from './DimensionSelectDialogConnect';
import {
  flDimensionLayer,
  multiDimensionLayer,
  WmMultiDimensionLayer3,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithDimensions } from '../../utils/testUtils';
import { createMockStore } from '../../store';
import { DemoWrapperConnect } from '../Providers/Providers';
import { i18n, initCoreReactI18n } from '../../utils/i18n';

beforeAll(() => {
  initCoreReactI18n();
});

describe('src/components/MultiMapDimensionSelect/DimensionSelectDialogConnect', () => {
  const webmapReactT = i18n.getFixedT('en', WEBMAP_REACT_NAMESPACE);
  it('should render a dialog for elevation when map has elevation dimension and a slider if enabled layer present with that dimension', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      storeTestSettings.WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = createMockStore(mockState);
    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectDialogConnect {...props} />
      </DemoWrapperConnect>,
    );

    const dimCnf = dimensionConfig(webmapReactT).find(
      (cnf) => cnf.name === props.dimensionName,
    )!;
    expect(screen.getAllByText(dimCnf.label).length).toBe(1);
    expect(screen.getByTestId('slider-dimensionSelect')).toBeTruthy();
  });

  it('should not render a dialog for elevation when map has no enabled layer with elevation dimension', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      storeTestSettings.WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const disabledLayer = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: false,
      dimensions: [
        {
          name: 'flight level',
          units: 'hft',
          currentValue: '625',
          values: '25,325,625',
        },
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
        },
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2020-03-13T14:40:00Z',
        },
      ],
      styles: [
        {
          title: 'rainbow/nearest',
          name: 'rainbow/nearest',
          legendURL:
            'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
          abstract: 'No abstract available',
        },
      ],
    };
    const mockState2 = mockStateMapWithDimensions(disabledLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = createMockStore(mockState);
    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectDialogConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.queryByTestId('slider-dimensionSelect')).toBeFalsy();
  });

  it('should only render slider for elevation for layers with elevation dimension', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      WmMultiDimensionLayer3,
      'multiDimensionLayerMock',
    );
    const mockState = mockStateMapWithDimensions(flDimensionLayer, mapId);
    const store = createMockStore(mockState);
    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectDialogConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(screen.queryByTestId('slider-dimensionSelect')).toBeFalsy();
  });

  it('should call handleDimensionValueChanged when clicking on the slider', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      storeTestSettings.WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [uiTypes.DialogTypes.DimensionSelectElevation, ''],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = createMockStore(mockState);
    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectDialogConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().layers.byId['multiDimensionLayerMock'].dimensions[1]
        .currentValue,
    ).toEqual(multiDimensionLayer.dimensions![1].currentValue);

    const dimCnf = dimensionConfig(webmapReactT).find(
      (cnf) => cnf.name === props.dimensionName,
    )!;
    expect(screen.getAllByText(dimCnf.label).length).toBe(1);
    expect(screen.getByTestId('slider-dimensionSelect')).toBeTruthy();

    const slider = screen.getByTestId('verticalSlider');
    expect(slider).toBeTruthy();

    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);

    expect(
      store.getState().layers.byId['multiDimensionLayerMock'].dimensions[1]
        .currentValue,
    ).toEqual('1000');
  });

  it('should call handleSyncChanged when clicking on the sync button', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      storeTestSettings.WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = createMockStore(mockState);
    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    render(
      <DemoWrapperConnect store={store}>
        <DimensionSelectDialogConnect {...props} />
      </DemoWrapperConnect>,
    );

    expect(
      store.getState().layers.byId['multiDimensionLayerMock'].dimensions[1]
        .synced,
    ).toBeUndefined();

    const dimCnf = dimensionConfig(webmapReactT).find(
      (cnf) => cnf.name === props.dimensionName,
    )!;
    expect(screen.getAllByText(dimCnf.label).length).toBe(1);
    expect(screen.getByTestId('slider-dimensionSelect')).toBeTruthy();
    expect(screen.getByTestId('syncButton')).toBeTruthy();

    fireEvent.click(screen.getByTestId('syncButton'));

    expect(
      store.getState().layers.byId['multiDimensionLayerMock'].dimensions[1]
        .synced,
    ).toBeTruthy();
    expect(
      store.getState().layers.byId['multiDimensionLayerMock'].dimensions[1]
        .currentValue,
    ).toEqual(multiDimensionLayer.dimensions![1].currentValue);
  });
});
