/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Grid2 as Grid, Divider } from '@mui/material';

import { useDispatch, useSelector } from 'react-redux';
import {
  CoreAppStore,
  defaultLayers,
  drawingToolActions,
  drawingToolSelectors,
  layerActions,
  layerSelectors,
} from '@opengeoweb/store';

import { CustomIconButton } from '@opengeoweb/shared';
import {
  DrawMode,
  EditModeButtonField,
  GeoJSONTextField,
  defaultModes,
  publicLayers,
  MapDrawToolOptions,
  basicExampleDrawOptions,
  basicExampleMultipleShapeDrawOptions,
  getGeoJSONPropertyValue,
  FeatureLayers,
  StoryLayoutGrid,
  SelectField,
  fillOptions,
  getToolIcon,
  opacityOptions,
  strokeWidthOptions,
  basicExampleMultipleShapeWithValuesDrawOptions,
} from '@opengeoweb/webmap-react';
import { clamp } from 'lodash';
import { useDefaultMapSettings } from '@opengeoweb/webmap-redux';
import { DemoWrapperConnect } from '../Providers/Providers';

import { MapViewConnect } from '../MapViewConnect';
import { useCoreTranslation } from '../../utils/i18n';
import { createMockStore } from '../../store';

export default {
  title: 'components/MapDrawToolRedux',
};

const store = createMockStore();
const mapId = 'test-map-id';
const drawToolId = `drawtool-1`;

const BasicMapDrawToolStory: React.FC<{
  drawOptions?: MapDrawToolOptions;
}> = ({
  drawOptions = {
    defaultDrawModes: defaultModes,
  },
}) => {
  const dispatch = useDispatch();

  useDefaultMapSettings({
    mapId,
    layers: [{ ...publicLayers.radarLayer, id: `radar-${mapId}` }],
    baseLayers: [
      { ...defaultLayers.baseLayerGrey, id: `baseGrey-${mapId}` },
      defaultLayers.overLayer,
    ],
  });
  React.useEffect(() => {
    dispatch(
      drawingToolActions.registerDrawTool({
        mapId,
        drawToolId,
        defaultDrawModes: drawOptions.defaultDrawModes || defaultModes,
        geoJSONLayerId: 'draw-layer',
        ...drawOptions,
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const drawTool = useSelector((store: CoreAppStore) =>
    drawingToolSelectors.selectDrawToolById(store, drawToolId),
  );

  const activeDrawMode = useSelector((store: CoreAppStore) =>
    drawingToolSelectors.getDrawModeById(
      store,
      drawToolId,
      drawTool?.activeDrawModeId!,
    ),
  ) as DrawMode;

  const selectedFeatureIndex = useSelector((store: CoreAppStore) =>
    layerSelectors.getSelectedFeatureIndex(store, drawTool?.geoJSONLayerId),
  );

  const isLayerInEditMode = useSelector((store: CoreAppStore) =>
    layerSelectors.getIsLayerInEditMode(store, drawTool?.geoJSONLayerId),
  );

  const layerFeatureGeoJSON = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(store, drawTool?.geoJSONLayerId),
  );

  const featureLayerGeoJSONProperties = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSONProperties(
      store,
      drawTool?.geoJSONLayerId,
    ),
  );

  const activeToolId = drawTool?.activeDrawModeId;

  const onChangeDrawMode = (tool: DrawMode): void => {
    dispatch(
      drawingToolActions.changeDrawToolMode({
        drawToolId,
        drawModeId: tool.drawModeId,
      }),
    );
  };

  const onChangeEditMode = (isInEditMode: boolean): void => {
    dispatch(
      layerActions.toggleFeatureMode({
        layerId: drawTool?.geoJSONLayerId!,
        isInEditMode,
      }),
    );
  };

  const onChangeGeoJSON = (geoJSON: GeoJSON.FeatureCollection): void => {
    dispatch(
      layerActions.layerChangeGeojson({
        layerId: drawTool?.geoJSONLayerId!,
        geojson: geoJSON,
      }),
    );
  };

  const onChangeGeoJSONProperties = (
    properties: GeoJSON.GeoJsonProperties,
  ): void => {
    dispatch(
      layerActions.updateFeatureProperties({
        layerId: drawTool?.geoJSONLayerId!,
        properties,
        selectedFeatureIndex,
      }),
    );
  };

  const onChangeFeatureLayer = (
    newIndex: number,
    newFeature: GeoJSON.Feature,
  ): void => {
    dispatch(
      layerActions.setSelectedFeature({
        layerId: drawTool?.geoJSONLayerId!,
        selectedFeatureIndex: newIndex,
      }),
    );

    const newSelectionType = newFeature.properties?.selectionType;
    const newDrawTool = drawOptions.defaultDrawModes?.find(
      (mode) => mode.selectionType === newSelectionType,
    );

    if (newDrawTool && newDrawTool.isSelectable) {
      dispatch(
        drawingToolActions.changeDrawToolMode({
          drawToolId,
          drawModeId: newDrawTool.drawModeId,
          shouldUpdateShape: false,
        }),
      );
      if (drawTool?.geoJSONLayerId) {
        dispatch(
          layerActions.toggleFeatureMode({
            layerId: drawTool?.geoJSONLayerId,
            isInEditMode: true,
            drawMode: newDrawTool?.value,
          }),
        );
      }
    } else {
      // fallback for if it can not find properties.selectionType in the geoJSON: set draw mode based on type of geoJSON
      const geometryType = newFeature.geometry.type;
      const fallbackDrawTool = drawTool?.drawModes.find(
        ({ shape }) =>
          shape.type === 'Feature' && shape.geometry.type === geometryType,
      );
      if (drawTool?.geoJSONLayerId) {
        dispatch(
          layerActions.toggleFeatureMode({
            layerId: drawTool?.geoJSONLayerId,
            isInEditMode: true,
            drawMode: fallbackDrawTool?.value,
          }),
        );
      }
      dispatch(
        drawingToolActions.changeDrawToolMode({
          drawToolId,
          drawModeId: fallbackDrawTool!.drawModeId,
          shouldUpdateShape: false,
        }),
      );
    }
  };

  const defaultPolygonShape = drawTool?.drawModes.find(
    (mode) => mode.value === 'POLYGON',
  );
  // geoJSON properties
  const opacity = getGeoJSONPropertyValue(
    'fill-opacity',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const opacityString = (opacity * 100).toString();

  const fill = getGeoJSONPropertyValue(
    'fill',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as string;

  const strokeOpacity = getGeoJSONPropertyValue(
    'stroke-opacity',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const strokeOpacityString = (strokeOpacity * 100).toString();

  const strokeColor = getGeoJSONPropertyValue(
    'stroke',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as string;

  const strokeWidth = getGeoJSONPropertyValue(
    'stroke-width',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as string;

  // change geoJSON properties
  const onChangeOpacity = (value: string): void => {
    const newOpacity = parseInt(value, 10) / 100;
    const additionalStrokeOpacity = 0.8;
    onChangeGeoJSONProperties({
      'fill-opacity': newOpacity,
      'stroke-opacity': clamp(newOpacity + additionalStrokeOpacity, 0.01, 1),
    });
  };

  const onChangeFill = (value: string): void => {
    onChangeGeoJSONProperties({
      fill: value,
    });
  };

  const onChangeStrokeOpacity = (value: string): void => {
    const newOpacity = parseInt(value, 10) / 100;
    onChangeGeoJSONProperties({
      'stroke-opacity': newOpacity,
    });
  };

  const onChangeStroke = (value: string): void => {
    onChangeGeoJSONProperties({
      stroke: value,
    });
  };

  const onChangeStrokeWidth = (value: string): void => {
    onChangeGeoJSONProperties({
      'stroke-width': value,
    });
  };
  const { t } = useCoreTranslation();
  return (
    <StoryLayoutGrid mapComponent={<MapViewConnect mapId={mapId} />}>
      <Grid size={12}>
        {drawTool?.drawModes.map((mode) => (
          <CustomIconButton
            key={mode.drawModeId}
            variant="tool"
            tooltipTitle={t(mode.title)}
            onClick={(): void => onChangeDrawMode(mode)}
            isSelected={activeToolId === mode.drawModeId}
            sx={{ marginRight: 1, marginBottom: 1 }}
          >
            {getToolIcon(mode.selectionType)}
          </CustomIconButton>
        ))}

        <Divider />
      </Grid>
      <Grid size={12}>
        <SelectField
          label="Fill opacity"
          value={opacityString}
          onChangeValue={onChangeOpacity}
          options={opacityOptions}
          labelSuffix=" %"
        />

        <SelectField
          label="Fill color"
          value={fill}
          onChangeValue={onChangeFill}
          options={fillOptions}
          showValueAsColor
        />

        <SelectField
          label="Stroke opacity"
          value={strokeOpacityString}
          onChangeValue={onChangeStrokeOpacity}
          options={opacityOptions.filter((option) => option !== '0')}
          width="33.3%"
          labelSuffix=" %"
        />

        <SelectField
          label="Stroke color"
          value={strokeColor}
          onChangeValue={onChangeStroke}
          options={fillOptions}
          width="33.3%"
          showValueAsColor
        />

        <SelectField
          label="Stroke width"
          value={strokeWidth}
          onChangeValue={onChangeStrokeWidth}
          options={strokeWidthOptions}
          width="33.3%"
        />
      </Grid>
      <EditModeButtonField
        isInEditMode={isLayerInEditMode}
        onToggleEditMode={onChangeEditMode}
        drawMode={activeDrawMode?.value}
      />
      <FeatureLayers
        geojson={layerFeatureGeoJSON!}
        onChangeLayerIndex={onChangeFeatureLayer}
        activeFeatureLayerIndex={selectedFeatureIndex || 0}
        getToolIcon={getToolIcon}
      />
      <GeoJSONTextField
        onChangeGeoJSON={onChangeGeoJSON}
        geoJSON={layerFeatureGeoJSON!}
      />
    </StoryLayoutGrid>
  );
};

export const BasicMapDrawTool = (): React.ReactElement => (
  <DemoWrapperConnect store={store}>
    <BasicMapDrawToolStory />
  </DemoWrapperConnect>
);

export const BasicMapDrawToolShape = (): React.ReactElement => (
  <DemoWrapperConnect store={store}>
    <BasicMapDrawToolStory drawOptions={basicExampleDrawOptions} />
  </DemoWrapperConnect>
);

export const BasicMapDrawToolMultipleShape = (): React.ReactElement => (
  <DemoWrapperConnect store={store}>
    <BasicMapDrawToolStory drawOptions={basicExampleMultipleShapeDrawOptions} />
  </DemoWrapperConnect>
);

export const BasicMapDrawToolWithMultipleShapesValues =
  (): React.ReactElement => (
    <DemoWrapperConnect store={store}>
      <BasicMapDrawToolStory
        drawOptions={basicExampleMultipleShapeWithValuesDrawOptions}
      />
    </DemoWrapperConnect>
  );
