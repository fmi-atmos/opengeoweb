/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';

import {
  layerActions,
  layerTypes,
  CoreAppStore,
  drawingToolSelectors,
} from '@opengeoweb/store';
import { StyleLike } from 'ol/style/Style';
import { useDispatch, useStore } from 'react-redux';
import {
  DrawModeExitCallback,
  OpenLayersFeatureLayer,
  OpenLayersMapDraw,
  DrawModeValue,
  HoverSelect,
} from '@opengeoweb/webmap-react';

interface OpenLayersFeatureLayerConnectProps {
  layer: layerTypes.FeatureLayer;
  layerId?: string;
  zIndex: number;
  style: StyleLike;
  hoverSelect?: HoverSelect;
}

export const OpenLayersFeatureLayerConnect: React.FC<
  OpenLayersFeatureLayerConnectProps
> = ({
  layer,
  layerId,
  zIndex,
  style,
  hoverSelect,
}: OpenLayersFeatureLayerConnectProps) => {
  const store = useStore<CoreAppStore>();
  const dispatch = useDispatch();

  const updateFeature = React.useCallback(
    (geojson: GeoJSON.FeatureCollection, reason: string): void => {
      if (!layerId) {
        window.console.error('updateFeature without layerId');
        return;
      }
      const activeDrawToolId = drawingToolSelectors.getActiveDrawToolId(
        store.getState(),
      );
      const activeDrawMode = drawingToolSelectors.getActiveDrawMode(
        store.getState(),
        activeDrawToolId,
      );
      dispatch(
        layerActions.updateFeature({
          geojson,
          reason,
          layerId,
          shouldAllowMultipleShapes:
            drawingToolSelectors.getShouldAllowMultipleShapes(
              store.getState(),
              activeDrawToolId,
            ),
          geoJSONIntersectionLayerId:
            drawingToolSelectors.getGeoJSONIntersectionLayerId(
              store.getState(),
              activeDrawToolId,
            ),
          geoJSONIntersectionBoundsLayerId:
            drawingToolSelectors.getGeoJSONIntersectionBoundsLayerId(
              store.getState(),
              activeDrawToolId,
            ),
          selectionType: activeDrawMode?.selectionType,
        }),
      );
    },
    [dispatch, store, layerId],
  );

  const exitFeatureDrawMode = React.useCallback(
    (reason: DrawModeExitCallback): void => {
      if (!layerId) {
        window.console.error('exitFeatureDrawMode without layerId');
        return;
      }
      const activeDrawToolId = drawingToolSelectors.getActiveDrawToolId(
        store.getState(),
      );

      dispatch(
        layerActions.exitFeatureDrawMode({
          reason,
          layerId,
          shouldAllowMultipleShapes:
            drawingToolSelectors.getShouldAllowMultipleShapes(
              store.getState(),
              activeDrawToolId,
            ),
        }),
      );
    },
    [dispatch, store, layerId],
  );

  return (
    <>
      <OpenLayersFeatureLayer
        featureCollection={!layer.isInEditMode ? layer.geojson : undefined}
        zIndex={zIndex}
        style={style}
        hoverSelect={hoverSelect}
      />
      <OpenLayersMapDraw
        drawMode={layer.drawMode as DrawModeValue}
        viewOnlyStyle={style}
        exitDrawModeCallback={exitFeatureDrawMode}
        geojson={layer.geojson!}
        isInEditMode={layer.isInEditMode || false}
        selectedFeatureIndex={layer.selectedFeatureIndex || 0}
        updateGeojson={updateFeature}
      />
    </>
  );
};
