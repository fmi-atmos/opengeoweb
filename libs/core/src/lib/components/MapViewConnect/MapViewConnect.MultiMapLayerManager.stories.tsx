/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { Provider, connect } from 'react-redux';
import { defaultLayers } from '@opengeoweb/store';
import { MapControls } from '@opengeoweb/webmap-react';
import { useDefaultMapSettings } from '@opengeoweb/webmap-redux';
import MapViewConnect from './MapViewConnect';

import { DemoWrapperConnect } from '../Providers/Providers';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';
import { createMockStore } from '../../store';

export default {
  title: 'components/MapViewConnect/MapViewConnect',
  component: MapViewConnect,
};
const store = createMockStore();
interface MapDemoProps {
  mapId: string;
}

const connectRedux = connect(null, {});
const MapDemo: React.FC<MapDemoProps> = ({ mapId }) => {
  useDefaultMapSettings({
    mapId,
    baseLayers: [
      { ...defaultLayers.baseLayerGrey, id: `baseLayer-2-${mapId}` },
      {
        ...defaultLayers.overLayer,
        id: `overLayer-1-${mapId}`,
      },
    ],
  });
  return (
    <DemoWrapperConnect store={store}>
      <MapControls>
        <LayerManagerMapButtonConnect mapId={mapId} isMultiMap />
      </MapControls>
      <LayerManagerConnect bounds="parent" mapId={mapId} isMultiMap />
      <MapViewConnect mapId={mapId} />
    </DemoWrapperConnect>
  );
};

const MapDemoConnect = connectRedux(MapDemo);

export const MultiMapLayerManager: React.FC = () => {
  return (
    <Provider store={store}>
      <div style={{ display: 'flex' }}>
        <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
          <MapDemoConnect mapId="mapid_1" />
        </div>
        <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
          <MapDemoConnect mapId="mapid_2" />
        </div>
      </div>
    </Provider>
  );
};
