/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  CoreAppStore,
  getViewObjectForMapId,
  getViewObjectForSyncGroup,
  mapSelectors,
  syncConstants,
  syncGroupsSelectors,
} from '@opengeoweb/store';
import { Store } from '@reduxjs/toolkit';
import { View } from 'ol';
import { unByKey } from 'ol/Observable';
import { useEffect, useMemo, useState } from 'react';

interface UseGetOpenLayersViewValue {
  view?: View;
  name?: string;
}

export const useGetOpenLayersView = (
  mapId: string,
  store: Store<CoreAppStore>,
): UseGetOpenLayersViewValue => {
  const [view, setView] = useState<View>();
  const [name, setName] = useState<string>();

  const syncGroupState = syncGroupsSelectors.getSynchronizationGroupState(
    store.getState(),
  );

  const syncGroupView = useMemo(() => {
    const thisAreaSyncGroupId = syncGroupState.groups.allIds.find((id) => {
      const group = syncGroupState.groups.byId[id];
      return (
        group.type === syncConstants.SYNCGROUPS_TYPE_SETBBOX &&
        group.targets.allIds.indexOf(mapId) !== -1
      );
    });

    return thisAreaSyncGroupId
      ? getViewObjectForSyncGroup(thisAreaSyncGroupId)
      : null;
  }, [mapId, syncGroupState.groups]);

  useEffect(() => {
    const mapBbox = mapSelectors.getBbox(store.getState(), mapId);
    const storedView = syncGroupView || getViewObjectForMapId(mapId);
    if (
      storedView.view.getCenter() === undefined &&
      mapBbox.left !== undefined
    ) {
      storedView.view.fit([
        mapBbox.left,
        mapBbox.bottom,
        mapBbox.right,
        mapBbox.top,
      ]);
    }
    setName(storedView.name);

    const setViewFromStoredView = (): void => {
      setView(storedView.view);
    };

    setViewFromStoredView();
    const eventKey = storedView.on('change', setViewFromStoredView);

    return (): void => {
      unByKey(eventKey);
    };
  }, [syncGroupView, mapId, store]);

  return { view, name };
};
