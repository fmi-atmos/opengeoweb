/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

import { Box, Chip } from '@mui/material';

import { CoreAppStore, layerActions, mapSelectors } from '@opengeoweb/store';
import { webmapUtils } from '@opengeoweb/webmap';
import { publicLayers } from '@opengeoweb/webmap-react';
import { useDefaultMapSettings, ActionCard } from '@opengeoweb/webmap-redux';
import MapViewConnect from './MapViewConnect';

import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

const store = createMockStore();
export default {
  title: 'components/MapViewConnect/MapViewConnect',
  component: MapViewConnect,
};

const connectRedux = connect(
  (state: CoreAppStore, props: { mapId: string }) => ({
    layerIds: mapSelectors.getLayerIds(state, props.mapId),
  }),
  {
    setLayers: layerActions.setLayers,
    addLayer: layerActions.addLayer,
    deleteLayer: layerActions.layerDelete,
  },
);

type Props = ConnectedProps<typeof connectRedux> & { mapId: string };
const Presets = connectRedux(
  ({ mapId, setLayers, addLayer, deleteLayer, layerIds }: Props) => {
    useDefaultMapSettings({ mapId });

    return (
      <>
        <ActionCard
          name="setLayers"
          exampleLayers={[
            {
              layers: [publicLayers.radarLayer],
              title: 'Radar',
            },
            {
              layers: [
                publicLayers.harmoniePrecipitation,
                publicLayers.harmoniePressure,
              ],
              title: 'Precip + Obs',
            },
            {
              layers: [publicLayers.radarLayer, publicLayers.dwdWarningLayer],
              title: 'Radar + DWD Warnings',
            },
          ]}
          description="sets new layers on a map while removing all current ones"
          onClickBtn={({ layers }): void => {
            setLayers({ layers, mapId });
          }}
        />

        <ActionCard
          name="addLayer"
          exampleLayers={[
            {
              layers: [publicLayers.radarLayer],
              title: 'Radar',
            },
            {
              layers: [publicLayers.msgCppLayer],
              title: 'MSGCPP',
            },
            {
              layers: [publicLayers.dwdWarningLayer],
              title: 'DWD Warnings',
            },
          ]}
          description="adds a new layer on a map while keeping the current ones"
          onClickBtn={({ layers }): void => {
            addLayer({
              layer: layers[0],
              layerId: webmapUtils.generateLayerId(),
              mapId,
              origin: 'story',
            });
          }}
        />

        <ActionCard name="removeLayer" description="removes a layer on a map ">
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'start',
              flexWrap: 'wrap',
            }}
          >
            {layerIds.map((layerId) => (
              <Chip
                key={layerId}
                label={layerId}
                onDelete={(): void => {
                  deleteLayer({ layerId, mapId, layerIndex: 0 });
                }}
                color="secondary"
              />
            ))}
          </Box>
        </ActionCard>
      </>
    );
  },
);

export const LayerActions = (): React.ReactElement => {
  return (
    <DemoWrapperConnect store={store}>
      <div style={{ height: '100vh' }}>
        <MapViewConnect mapId="mapid_1" />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 99910000,
        }}
      >
        <Presets mapId="mapid_1" />
      </div>
    </DemoWrapperConnect>
  );
};
