/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { dateUtils } from '@opengeoweb/shared';
import { TimeSliderStartCenterEndTime } from '@opengeoweb/store';
import { Dimension, WMJSDimension } from '@opengeoweb/webmap';
import {
  MapLocation,
  OnInitializeLayerProps,
  TimeContextType,
  UpdateLayerInfoPayload,
} from '@opengeoweb/webmap-react';
import { Feature } from 'ol';
import { Point } from 'ol/geom';
import { toLonLat } from 'ol/proj';

export const makePrefetchTimeSpan = (
  timeSliderStartCenterEndTime: TimeSliderStartCenterEndTime,
  animationStartTime: string | undefined,
  animationEndTime: string | undefined,
  isAnimating: boolean,
): TimeContextType => {
  const timeSliderTimeStepInS = timeSliderStartCenterEndTime.step * 60;

  const animationTimeStartInS =
    dateUtils.parseISO(animationStartTime!).getTime() / 1000;
  const animationTimeEndInS =
    dateUtils.parseISO(animationEndTime!).getTime() / 1000;

  const prefetchStart = isAnimating
    ? animationTimeStartInS
    : timeSliderStartCenterEndTime.start;
  const prefetchEnd = isAnimating
    ? animationTimeEndInS
    : timeSliderStartCenterEndTime.end;
  if (!prefetchStart || !prefetchEnd) {
    return {
      timespan: {
        start: 0,
        end: 0,
        step: 0,
      },
      isAnimating,
    };
  }
  const roundStart =
    Math.round(prefetchStart / timeSliderTimeStepInS) * timeSliderTimeStepInS;

  const roundEnd =
    Math.round(prefetchEnd / timeSliderTimeStepInS) * timeSliderTimeStepInS;

  // eslint-disable-next-line no-constant-condition
  return {
    timespan: {
      start: roundStart * 1000,
      end: roundEnd * 1000,
      step: timeSliderTimeStepInS * 1000,
    },
    isAnimating,
  };
};

export const makeLayerInfoPayload = (
  payload: OnInitializeLayerProps,
  mapDimensions: Dimension[],
  mapId: string,
): UpdateLayerInfoPayload => {
  // During intialization, try to base the layer dimension on the current map dimension value.
  const updatedPayLoad: UpdateLayerInfoPayload = {
    origin: 'OnInitializeLayerProps in MapViewConnect',
    layerDimensions: {
      layerId: payload.layerId,
      origin: 'OnInitializeLayerProps in MapViewConnect',
      dimensions: (payload.dimensions || []).map((layerDim: Dimension) => {
        const currentMapDim =
          (mapDimensions || []).find((mapDim) => {
            return mapDim.name === layerDim.name;
          })?.currentValue || layerDim.currentValue;
        const wmDim = new WMJSDimension(layerDim);
        return {
          ...layerDim,
          currentValue: wmDim.getClosestValue(currentMapDim),
        };
      }),
    },
    mapDimensions: {
      mapId,
      origin: 'OnInitializeLayerProps in MapViewConnect',
      dimensions: (payload.dimensions || []).map((layerDim: Dimension) => {
        const currentMapDim =
          (mapDimensions || []).find((mapDim) => {
            return mapDim.name === layerDim.name;
          })?.currentValue || layerDim.currentValue;
        const wmDim = new WMJSDimension(layerDim);
        return {
          ...layerDim,
          currentValue: wmDim.getClosestValue(currentMapDim),
        };
      }),
    },
  };
  return updatedPayLoad;
};

export const getCenterOfLinkedFeature = (
  feature: Feature | undefined,
  srs: string,
): MapLocation | null => {
  if (feature && feature.getGeometry()?.getType() === 'Point') {
    const projectionCoords = (feature.getGeometry() as Point).getCoordinates();
    const coords = toLonLat(projectionCoords, srs);
    const id = feature?.getId()?.toString() || '';

    return {
      lon: coords[0],
      lat: coords[1],
      id,
      serviceId: feature.getProperties().serviceId,
      collectionId: feature.getProperties().collectionId,
    };
  }
  return null;
};
