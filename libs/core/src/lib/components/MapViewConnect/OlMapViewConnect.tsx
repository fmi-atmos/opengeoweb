/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { useDispatch, useSelector, useStore } from 'react-redux';
import { unByKey } from 'ol/Observable';

import {
  layerActions,
  mapActions,
  mapSelectors,
  genericActions,
  layerTypes,
  CoreAppStore,
  syncConstants,
  uiSelectors,
  syncGroupsTypes,
  loadingIndicatorActions,
  syncGroupsSelectors,
  genericSelectors,
  uiTypes,
  useSetupDialog,
} from '@opengeoweb/store';

import { Dimension, getWMLayerById } from '@opengeoweb/webmap';

import {
  MapControls,
  OpenLayersLayer,
  WMSLayer,
  TimeContext,
  UpdateLayerInfoPayload,
  useSetIntervalWhenVisible,
  getLayerUpdateInfo,
  openLayersGetMapImageStore,
  OpenLayersMapView,
  OpenLayersGetFeatureInfo,
  MapLocation,
  OpenLayersZoomControl,
  genericOpenLayersFeatureStyle,
  OpenLayersFeatureLayer,
} from '@opengeoweb/webmap-react';

import { FeatureCollection } from 'geojson';
import { Feature } from 'ol';
import { useTouchZoomPan } from './useTouchZoomPan';
import { useKeyboardZoomAndPan } from './useKeyboardZoomAndPan';
import { SearchControlButtonConnect, SearchDialogConnect } from '../Search';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';
import { LegendConnect, LegendMapButtonConnect } from '../LegendConnect';
import { MultiDimensionSelectMapButtonsConnect } from '../MultiMapDimensionSelectConnect';
import {
  GetFeatureInfoButtonConnect,
  GetFeatureInfoConnect,
} from '../FeatureInfo';
import { useGetOpenLayersView } from './useGetOpenLayersView';
import { MapViewConnectProps } from './MapViewConnect';
import { getCenterOfLinkedFeature, makePrefetchTimeSpan } from './olMapUtils';
import { OpenLayersFeatureLayerConnect } from '../MapDrawToolRedux/OpenLayersFeatureLayerConnect';
import { OlMapViewLayerConnect } from './OlMapViewLayerConnect';

/**
 * Connected component used to display the map and selected layers.
 * Includes options to disable the map controls and legend.
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {object} [controls.zoomControls] **optional** controls: object - toggle the map controls, zoomControls defaults to true
 * @param {boolean} [displayTimeInMap] **optional** displayTimeInMap: boolean, toggles the mapTime, defaults to true
 * @param {boolean} [showScaleBar] **optional** showScaleBar: boolean, toggles the scaleBar, defaults to true
 * @example
 * ```<MapViewConnect mapId={mapId} controls={{ zoomControls: false }} displayTimeInMap={false} showScaleBar={false}/>```
 */
const OlMapViewConnect: React.FC<MapViewConnectProps> = ({
  mapId,
  controls = {
    mapControlsPositionTop: 0,
    zoomControls: true,
  },
  initialBbox,
  children,
  holdShiftToScroll,
}: MapViewConnectProps) => {
  const store = useStore<CoreAppStore>();

  const mapRef = React.useRef(null);
  const { view } = useGetOpenLayersView(mapId, store);
  const [hoverLinkedFeature, setHoverLinkedFeature] = React.useState<Feature>();

  const linkedPanelId = useSelector((store: CoreAppStore) =>
    genericSelectors.selectLinkedPanelId(store, mapId),
  );

  const mapLayerIds = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapLayerIdsEnabled(store, mapId),
  );
  const baseLayers = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapBaseLayers(store, mapId),
  );
  const overLayers = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapOverLayers(store, mapId),
  );
  const featureLayers = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapFeatureLayers(store, mapId),
  );
  const linkedFeaturesFromState = useSelector((store: CoreAppStore) =>
    genericSelectors.selectLinkedFeatures(store.syncGroups?.linkedState, mapId),
  );

  const linkedFeatures: FeatureCollection = React.useMemo(() => {
    return {
      type: 'FeatureCollection',
      features: linkedFeaturesFromState.flatMap(
        (feature) => feature!.geoJSON.features,
      ),
    };
  }, [linkedFeaturesFromState]);

  const srs = useSelector((store: CoreAppStore) =>
    mapSelectors.getSrs(store, mapId),
  );

  const timeSliderStartCenterEndTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapTimeSliderStartCenterEndAndStep(store, mapId),
  );

  const animationStartTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getAnimationStartTime(store, mapId),
  );
  const animationEndTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getAnimationEndTime(store, mapId),
  );

  const isTimeScrollingEnabled = useSelector(
    syncGroupsSelectors.isTimeScrollingEnabled,
  );

  const isAnimating = useSelector((store: CoreAppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );

  const dispatch = useDispatch();

  const updateLayerInformation = React.useCallback(
    (payload: UpdateLayerInfoPayload): void => {
      const updatedPayLoad: UpdateLayerInfoPayload = {
        ...payload,
        origin: 'OnInitializeLayerProps in MapViewConnect',
        layerDimensions: {
          layerId: payload.layerDimensions?.layerId || '',
          origin: 'OnInitializeLayerProps in MapViewConnect',
          dimensions: (payload.layerDimensions?.dimensions || []).map(
            (layerDim: Dimension) => {
              return {
                ...layerDim,
              };
            },
          ),
        },
      };

      dispatch(layerActions.onUpdateLayerInformation(updatedPayLoad));
    },
    [dispatch],
  );

  const registerMap = React.useCallback(
    (payload: { mapId: string }): void => {
      dispatch(mapActions.registerMap(payload));
    },
    [dispatch],
  );
  const syncGroupsAddSource = React.useCallback(
    (payload: syncGroupsTypes.SyncGroupsAddSourcePayload): void => {
      dispatch(genericActions.syncGroupAddSource(payload));
    },
    [dispatch],
  );

  const hoverSelect = React.useMemo(
    () => ({
      active: true,
      onHover: setHoverLinkedFeature,
    }),
    [],
  );

  const displayMapPin = useSelector((store: CoreAppStore) =>
    mapSelectors.getDisplayMapPin(store, mapId),
  );

  const mapPinLocation = useSelector((store: CoreAppStore) =>
    mapSelectors.getPinLocation(store, mapId),
  );

  const mapPinChangeLocation = React.useCallback(
    (clickedLocation: MapLocation): void => {
      const mapPinLocation =
        getCenterOfLinkedFeature(hoverLinkedFeature, srs) || clickedLocation;

      const selectedFeatureId = hoverLinkedFeature?.getId()?.toString() || '';

      if (hoverLinkedFeature) {
        const selectedFeatureIndex = linkedFeatures.features.findIndex(
          (feature) => feature.id === selectedFeatureId,
        );
        dispatch(
          layerActions.setSelectedFeature({
            layerId: `drawlayer-${mapId}`,
            selectedFeatureIndex,
          }),
        );
      }
      dispatch(
        genericActions.addSharedData({
          panelId: linkedPanelId!,
          data: {
            selectedFeatureId,
          },
        }),
      );
      dispatch(mapActions.setMapPinLocation({ mapId, mapPinLocation }));
    },
    [
      dispatch,
      hoverLinkedFeature,
      linkedFeatures.features,
      linkedPanelId,
      mapId,
      srs,
    ],
  );

  const isActiveWindowId = useSelector((store: CoreAppStore) =>
    uiSelectors.getIsActiveWindowId(store, mapId),
  );
  useKeyboardZoomAndPan(isActiveWindowId, mapId, mapRef!.current);
  useTouchZoomPan(isActiveWindowId, mapId);
  React.useEffect(() => {
    registerMap({ mapId });
    syncGroupsAddSource({
      id: mapId,
      type: [
        syncConstants.SYNCGROUPS_TYPE_SETTIME,
        syncConstants.SYNCGROUPS_TYPE_SETBBOX,
      ],
    });
    dispatch(
      loadingIndicatorActions.setGetMapIsLoading({
        id: mapId,
        isGetMapLoading: false,
      }),
    );
  }, [dispatch, mapId, registerMap, syncGroupsAddSource]);

  const onGetMapLoadStart = React.useCallback((): void => {
    dispatch(
      loadingIndicatorActions.setGetMapIsLoading({
        id: mapId,
        isGetMapLoading: true,
      }),
    );
  }, [dispatch, mapId]);

  const onGetMapLoadReady = React.useCallback((): void => {
    dispatch(
      loadingIndicatorActions.setGetMapIsLoading({
        id: mapId,
        isGetMapLoading: false,
      }),
    );
  }, [dispatch, mapId]);

  React.useEffect(() => {
    if (!view) {
      return (): void => {};
    }

    const eventKey = view.on('change', () => {
      const { extent } = view.getViewStateAndExtent();
      dispatch(
        genericActions.setBbox({
          bbox: {
            left: extent[0],
            bottom: extent[1],
            right: extent[2],
            top: extent[3],
          },
          sourceId: mapId,
          srs,
        }),
      );
    });

    return (): void => {
      unByKey(eventKey);
    };
  }, [view, mapId, srs, dispatch]);

  const prefetchTimespan = makePrefetchTimeSpan(
    timeSliderStartCenterEndTime,
    animationStartTime,
    animationEndTime,
    isAnimating,
  );

  const layerIds = mapLayerIds.join();
  const refetchLayers = React.useCallback(() => {
    mapLayerIds?.forEach(async (layerId) => {
      if (layerId) {
        try {
          const wmLayer = getWMLayerById(layerId);
          if (!wmLayer) {
            console.warn('No wmLayer exists for layer id', layerId);
          }
          await wmLayer.parseLayer(true);
          const layerInfo = getLayerUpdateInfo(wmLayer, mapId);
          updateLayerInformation && updateLayerInformation(layerInfo);
        } catch (e) {
          window.console.error(e);
        }
      } else {
        console.warn('Not updating layer, something is wrong.', layerId);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mapId, updateLayerInformation, layerIds]);

  useSetIntervalWhenVisible(refetchLayers, 60000);

  const isLoading = React.useRef(false);

  // UseEffect for loading indicator
  React.useEffect(() => {
    openLayersGetMapImageStore.addImageEventCallback(() => {
      const checkLoading = !!(
        openLayersGetMapImageStore.getNumImagesLoading() > 0
      );
      if (checkLoading !== isLoading.current) {
        if (checkLoading === true) {
          onGetMapLoadStart && onGetMapLoadStart();
        } else {
          onGetMapLoadReady && onGetMapLoadReady();
        }
        isLoading.current = checkLoading;
      }
    }, mapId);

    return (): void => {
      openLayersGetMapImageStore.removeEventCallback(mapId);
    };
  }, [mapId, onGetMapLoadReady, onGetMapLoadStart]);

  const [debugMode, setDebugMode] = React.useState(false);
  React.useEffect(() => {
    const handleKeyPress = (event: KeyboardEvent): void => {
      if (
        event.isTrusted &&
        event.key === 'd' &&
        document.activeElement === document.body
      ) {
        setDebugMode((prev) => !prev);
      }
    };
    document.addEventListener('keyup', handleKeyPress);
    return (): void => {
      document.removeEventListener('keyup', handleKeyPress);
    };
  }, [debugMode]);

  const homeExtent = React.useMemo(
    () =>
      initialBbox && [
        initialBbox.left,
        initialBbox.bottom,
        initialBbox.right,
        initialBbox.top,
      ],
    [initialBbox],
  );

  const dialogType = `${uiTypes.DialogTypes.Search}-${mapId}`;
  const { isDialogOpen: isSearchDialogOpen } = useSetupDialog(dialogType);

  const layerDivStyle = React.useMemo((): React.CSSProperties => {
    const style: React.CSSProperties = {
      position: 'absolute',
      zIndex: 1,
    };

    if (isSearchDialogOpen) {
      style.top = `${8 + 40}px`;
    }

    return style;
  }, [isSearchDialogOpen]);

  return (
    <TimeContext.Provider value={prefetchTimespan}>
      <OpenLayersMapView
        view={view}
        isFocused={isActiveWindowId}
        isMouseWheelZoomEnabled={!isTimeScrollingEnabled}
        holdShiftToScroll={holdShiftToScroll}
      >
        <div style={layerDivStyle}>
          {baseLayers.map((layer: layerTypes.Layer, zIndex) => (
            <OpenLayersLayer
              id={`baselayer-${layer?.id}`}
              key={`baselayer-${layer?.id}`}
              zIndex={baseLayers.length - zIndex + 10}
              {...layer}
            />
          ))}
          {mapLayerIds.map((layerId: string, zIndex) => (
            <OlMapViewLayerConnect
              layerId={layerId}
              key={`layer-${layerId}`}
              zIndex={mapLayerIds.length - zIndex + 1000}
              debugMode={debugMode}
              mapId={mapId}
            />
          ))}
          {overLayers.map((layer: layerTypes.Layer, zIndex) => (
            <WMSLayer
              url={layer.service!}
              layerName={layer.name!}
              styleName={layer.style!}
              key={`overlayer-${layer?.id}`}
              layerOptions={{ zIndex: overLayers.length - zIndex + 2000 }}
            />
          ))}
          {featureLayers.map((layer, zIndex) => (
            <OpenLayersFeatureLayerConnect
              key={`featurelayer-${layer?.id}`}
              layerId={layer?.id}
              layer={layer}
              zIndex={featureLayers.length - zIndex + 3000}
              style={genericOpenLayersFeatureStyle}
              hoverSelect={{ active: true }}
            />
          ))}
          <OpenLayersFeatureLayer
            style={genericOpenLayersFeatureStyle}
            featureCollection={linkedFeatures}
            zIndex={4000}
            hoverSelect={hoverSelect}
          />
          <OpenLayersGetFeatureInfo
            mapPinLocation={mapPinLocation}
            displayMapPin={displayMapPin}
            selectLocation={mapPinChangeLocation}
          />
        </div>

        <MapControls
          data-testid="mapControls"
          style={{ top: controls?.mapControlsPositionTop }}
        >
          {controls?.search && <SearchControlButtonConnect mapId={mapId} />}
          {controls?.zoomControls && (
            <OpenLayersZoomControl homeExtent={homeExtent} />
          )}
          {controls?.layerManagerAndLegend && (
            <>
              <LayerManagerMapButtonConnect mapId={mapId} />
              <LegendMapButtonConnect
                mapId={mapId}
                multiLegend={controls?.multiLegend}
              />
            </>
          )}
          {controls?.dimensionSelect && (
            <MultiDimensionSelectMapButtonsConnect mapId={mapId} />
          )}
          {controls?.getFeatureInfo && (
            <GetFeatureInfoButtonConnect mapId={mapId} />
          )}
          {controls?.additionalMapControls}
        </MapControls>
        {controls?.multiLegend && (
          <LegendConnect
            showMapId
            mapId={mapId}
            multiLegend={controls?.multiLegend}
          />
        )}
        {controls?.search && <SearchDialogConnect mapId={mapId} />}
        {controls?.getFeatureInfo && (
          <GetFeatureInfoConnect showMapId mapId={mapId} />
        )}
        <LayerManagerConnect mapId={mapId} bounds="parent" isDocked />
        {children}
      </OpenLayersMapView>
    </TimeContext.Provider>
  );
};

export default OlMapViewConnect;
