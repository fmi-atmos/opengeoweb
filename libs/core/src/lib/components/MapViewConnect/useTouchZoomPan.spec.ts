/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useCanvasTarget } from '@opengeoweb/timeslider';
import { renderHook } from '@testing-library/react';

describe('src/components/MapViewConnect/MapViewConnect', () => {
  it('should be called with "touchstart", "touchmove" and "touchend" events when adding document listeners', async () => {
    jest.spyOn(document, 'addEventListener');

    renderHook(() => useCanvasTarget('touchstart'));
    expect(document.addEventListener).toHaveBeenCalledWith(
      'touchstart',
      expect.any(Function),
    );

    renderHook(() => useCanvasTarget('touchmove'));
    expect(document.addEventListener).toHaveBeenCalledWith(
      'touchmove',
      expect.any(Function),
    );

    renderHook(() => useCanvasTarget('touchend'));
    expect(document.addEventListener).toHaveBeenCalledWith(
      'touchend',
      expect.any(Function),
    );
  });
});
