/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  act,
  fireEvent,
  render,
  waitFor,
  screen,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {
  WebMapStateModuleState,
  uiTypes,
  syncConstants,
  storeTestUtils,
} from '@opengeoweb/store';
import {
  LayerOptions,
  LayerType,
  webmapUtils,
  WMLayer,
  mockGetCapabilities,
  setWMSGetCapabilitiesFetcher,
} from '@opengeoweb/webmap';
import { defaultReduxLayerRadarKNMI } from '../../utils/defaultTestSettings';
import { baseLayerGrey, overLayer } from '../../utils/testLayers';
import { DemoWrapperConnect } from '../Providers/Providers';
import MapViewConnect from './MapViewConnect';
import { createMockStore } from '../../store';
import {
  isDialogOpen,
  isMapKeyboardControlsAllowed,
} from './useKeyboardZoomAndPan';

import * as keyboardControls from './useKeyboardZoomAndPan';

describe('src/components/MapViewConnect/MapViewConnect', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  it('should zoom and pan the map using keyboard', async () => {
    jest.useFakeTimers();

    const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });

    const mapId = 'map-1';

    const radarLayer = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      name: 'RAD_NL25_PCP_CM',
    } as LayerOptions);
    const layers = [radarLayer, baseLayerGrey, overLayer];
    webmapUtils.registerWMLayer(radarLayer, radarLayer.id!);

    await radarLayer.parseLayer();

    radarLayer.layerType = LayerType.mapLayer;

    const mockState: WebMapStateModuleState & uiTypes.UIModuleState = {
      ...storeTestUtils.mockStateMapWithMultipleLayers(layers, mapId),
      ui: {
        order: [],
        dialogs: {},
        activeWindowId: mapId,
      },
    };

    const clonedState = JSON.parse(JSON.stringify(mockState));
    const store = createMockStore(clonedState);

    render(
      <DemoWrapperConnect store={store}>
        <MapViewConnect mapId={mapId} />
      </DemoWrapperConnect>,
    );

    await waitFor(() => {
      expect(store.getState().webmap.byId[mapId].mapLayers).toHaveLength(1);
    });

    await waitFor(() => {
      expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    });
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();

    expect(expect(store.getState().webmap.byId[mapId])).toBeDefined();

    expect(store.getState().syncGroups.sources.byId[mapId].types).toEqual([
      syncConstants.SYNCGROUPS_TYPE_SETTIME,
      syncConstants.SYNCGROUPS_TYPE_SETBBOX,
    ]);

    /* Now check the bbox action */
    fireEvent.click(screen.getByTestId('zoom-out'));

    await act(async () => jest.advanceTimersToNextTimer());

    await waitFor(() =>
      expect(store.getState().webmap.byId[mapId].bbox).toEqual({
        bottom: -25333333.333333332,
        left: -25333333.333333332,
        right: 25333333.333333332,
        top: 25333333.333333332,
      }),
    );

    await user.keyboard('-');

    await act(async () => jest.advanceTimersToNextTimer());

    const newTestBbox = {
      bottom: -33777777.777777776,
      left: -33777777.777777776,
      right: 33777777.777777776,
      top: 33777777.777777776,
    };

    await waitFor(() =>
      expect(store.getState().webmap.byId[mapId].bbox).toEqual(newTestBbox),
    );

    await user.keyboard('{ArrowLeft}');
    await act(async () => jest.advanceTimersToNextTimer());

    // If control key is pressed then ignore keypress
    await user.keyboard('{Control>}{ArrowLeft}');
    await act(async () => jest.advanceTimersToNextTimer());

    await waitFor(() =>
      expect(store.getState().webmap.byId[mapId].bbox.left).toEqual(
        -35128888.88888889,
      ),
    );

    expect(store.getState().webmap.byId[mapId].bbox.right).toEqual(
      32426666.666666664,
    );

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  describe('isDialogOpen', () => {
    it('should return true if a backdrop exists containing the mapRef', () => {
      const mapRef = document.createElement('div');
      const backdrop = document.createElement('div');
      backdrop.classList.add('MuiBackdrop-root');
      const backdropContainer = document.createElement('div');
      backdropContainer.appendChild(backdrop);
      document.body.appendChild(backdropContainer);
      backdropContainer.appendChild(mapRef); // Ensuring mapRef is within backdrop

      const result = isDialogOpen(mapRef);
      expect(result).toBe(true);
    });

    it('should return false if no backdrop contains the mapRef', () => {
      const mapRef = document.createElement('div');
      const backdrop = document.createElement('div');
      backdrop.classList.add('MuiBackdrop-root');
      document.body.appendChild(backdrop); // Backdrop exists, but mapRef is not inside it

      const result = isDialogOpen(mapRef);
      expect(result).toBe(false);
    });

    it('should return false if no backdrop exists', () => {
      const mapRef = document.createElement('div');
      // No backdrop added
      const result = isDialogOpen(mapRef);
      expect(result).toBe(false);
    });
  });

  describe('isMapKeyboardControlsAllowed', () => {
    let mapRef: HTMLElement;

    beforeEach(() => {
      mapRef = document.createElement('div');
    });

    it('should return false if a dialog is open', () => {
      const element = document.createElement('div') as Element;
      jest
        .spyOn(document, 'querySelectorAll')
        .mockReturnValue([element] as unknown as NodeListOf<Element>);
      const target = document.createElement('div');

      // Mocking isDialogOpen to return true (dialog open)
      jest.spyOn(keyboardControls, 'isDialogOpen').mockReturnValue(true);

      const result = isMapKeyboardControlsAllowed(target, mapRef);
      expect(result).toBe(false);
    });

    it('should return true if target is <body> and no dialog is open', () => {
      const target = document.body;

      // Mocking isDialogOpen to return false (dialog not open)
      jest.spyOn(keyboardControls, 'isDialogOpen').mockReturnValue(false);

      const result = isMapKeyboardControlsAllowed(target, mapRef);
      expect(result).toBe(true);
    });

    it('should return false if target is not <body> and is a <button> with focus visible', () => {
      const target = document.createElement('button');
      target.classList.add('Mui-focusVisible'); // Adding the class to indicate focus is visible

      const result = isMapKeyboardControlsAllowed(target, mapRef);
      expect(result).toBe(false);
    });

    it('should return true if target is a <button> without focus visible', () => {
      const target = document.createElement('button');
      target.classList.remove('Mui-focusVisible'); // Removing class to simulate no visible focus

      const result = isMapKeyboardControlsAllowed(target, mapRef);
      expect(result).toBe(true);
    });

    it('should return false if target is neither <body> nor <button>', () => {
      const target = document.createElement('div'); // not a body or button

      const result = isMapKeyboardControlsAllowed(target, mapRef);
      expect(result).toBe(false);
    });
  });
});
