/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useDispatch, useSelector } from 'react-redux';
import {
  layerActions,
  mapSelectors,
  CoreAppStore,
  layerSelectors,
} from '@opengeoweb/store';
import {
  getIsInsideAcceptanceTime,
  OpenLayersLayer,
  OnInitializeLayerProps,
} from '@opengeoweb/webmap-react';
import React, { useCallback } from 'react';
import { makeLayerInfoPayload } from './olMapUtils';

export interface OlMapViewLayerConnectProps {
  layerId: string | undefined;
  mapId: string;
  zIndex: number;
  debugMode: boolean;
}

export const OlMapViewLayerConnect = (
  layerProps: OlMapViewLayerConnectProps,
): React.ReactElement | null => {
  const { layerId, mapId, zIndex, debugMode } = layerProps;

  const layer = useSelector((store: CoreAppStore) =>
    layerSelectors.getLayerById(store, layerId),
  );
  const mapDimensions = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapDimensions(store, mapId),
  );

  const dispatch = useDispatch();

  const onInitializeLayer = useCallback(
    (initProps: OnInitializeLayerProps): void => {
      const updatedPayLoad = makeLayerInfoPayload(
        initProps,
        mapDimensions || [],
        mapId,
      );
      dispatch(layerActions.onUpdateLayerInformation(updatedPayLoad));
    },
    [dispatch, mapDimensions, mapId],
  );

  if (!layerId || !layer) {
    return null;
  }

  const isInsideAcceptanceTime = getIsInsideAcceptanceTime(
    layer.acceptanceTimeInMinutes,
    mapDimensions,
    layer.dimensions,
  );

  return (
    <OpenLayersLayer
      id={`layer-${layer?.id}`}
      key={`layer-${layer?.id}`}
      zIndex={zIndex}
      {...layer}
      enabled={isInsideAcceptanceTime && layer.enabled}
      onInitializeLayer={onInitializeLayer}
      debugMode={debugMode}
    />
  );
};
