/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { makePrefetchTimeSpan } from './olMapUtils';

describe('olMapUtils', () => {
  describe('makePrefetchTimeSpan', () => {
    it('should make correct timespan when not animating', () => {
      const timeSpan = makePrefetchTimeSpan(
        {
          center: 1739366604.5384614,
          end: 1739387664.5384614,
          start: 1739345544.5384614,
          step: 60,
        },
        '2025-02-12T08:23:24Z',
        '2025-02-12T13:13:24Z',
        false,
      );
      expect(timeSpan).toEqual({
        timespan: { end: 1739386800000, start: 1739347200000, step: 3600000 },
        isAnimating: false,
      });
    });

    it('should make correct timespan when  animating', () => {
      const timeSpan = makePrefetchTimeSpan(
        {
          center: 1739366604.5384614,
          end: 1739387664.5384614,
          start: 1739345544.5384614,
          step: 60,
        },
        '2025-02-12T08:23:24Z',
        '2025-02-12T13:13:24Z',
        true,
      );
      expect(timeSpan).toEqual({
        timespan: { end: 1739365200000, start: 1739347200000, step: 3600000 },
        isAnimating: true,
      });
    });
  });
});
