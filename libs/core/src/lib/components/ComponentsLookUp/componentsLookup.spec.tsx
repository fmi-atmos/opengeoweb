/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { getSingularDrawtoolDrawLayerId } from '@opengeoweb/store';
import { componentsLookUp } from './componentsLookUp';
import { DemoWrapperConnect } from '../Providers/Providers';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store';

describe('components/ComponentsLookUp', () => {
  const store = createMockStore();
  it('should return the HarmonieTempAndPrecipPreset component', () => {
    const payload = {
      title: 'title',
      componentType: 'HarmonieTempAndPrecipPreset' as const,
      id: 'viewid-1',
      initialProps: {
        syncGroupsIds: ['test-1'],
        layers: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props['data-testid']).toEqual(
      'coreHarmonieTempAndPrecipPreset',
    );
  });
  it('should return the MultiMapView component when using a mapPreset', () => {
    const payload = {
      title: 'title',
      componentType: 'MultiMap' as const,
      id: 'viewid-1',
      initialProps: {
        mapPreset: [{}],
        shouldShowZoomControls: false,
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props['data-testid']).toEqual('coreMultiMapViewConnect');
  });
  it('should not return the MultiMapView component when not using a mapPreset', () => {
    const payload = {
      title: 'title',
      componentType: 'MultiMap' as const,
      id: 'viewid-1',
      initialProps: {
        shouldShowZoomControls: false,
        mapPreset: undefined,
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeNull();
  });
  it('should return the ConfigurableMap component', () => {
    const payload = {
      title: 'title',
      componentType: 'Map' as const,
      id: 'viewid-1',
      initialProps: {
        mapPreset: undefined,
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props.id).toEqual('viewid-1');
    expect(component.props['data-testid']).toEqual(
      'coreConfigurableMapConnect',
    );
  });

  it('should return the ConfigurableMap component with mapControls', async () => {
    const buttonTitles = ['button 1', 'button 2'];
    const payload = {
      title: 'title',
      componentType: 'Map' as const,
      id: 'viewid-1',
      initialProps: {
        mapPreset: undefined,
        syncGroupsIds: [],
        mapControls: (
          <>
            <p>{buttonTitles[0]}</p>
            <p>{buttonTitles[1]}</p>
          </>
        ),
      },
    };

    render(
      <DemoWrapperConnect store={store}>
        {componentsLookUp(payload)}
      </DemoWrapperConnect>,
    );

    expect(await screen.findByText('button 1')).toBeTruthy();
    expect(await screen.findByText('button 2')).toBeTruthy();
    expect(screen.queryByTestId('drawingToolButton')).toBeFalsy();
  });

  it('should return the ConfigurableMap component with shouldDisplayDrawControls', async () => {
    const payload = {
      title: 'title',
      componentType: 'Map' as const,
      id: 'viewid-1',
      initialProps: {
        mapPreset: undefined,
        syncGroupsIds: [],
        shouldDisplayDrawControls: true,
      },
    };

    render(
      <DemoWrapperConnect store={store}>
        {componentsLookUp(payload)}
      </DemoWrapperConnect>,
    );
    const result =
      store.getState().layers.byId[getSingularDrawtoolDrawLayerId(payload.id)];
    expect(result).toBeDefined();
  });

  it('should return the ConfigurableMap component with search button in map', async () => {
    const payload = {
      title: 'title',
      componentType: 'Map' as const,
      id: 'mapid-1',
      initialProps: {
        mapPreset: undefined,
        syncGroupsIds: [],
        displaySearchButtonInMap: true,
      },
    };

    render(
      <DemoWrapperConnect store={store}>
        {componentsLookUp(payload)}
      </DemoWrapperConnect>,
    );
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    ).toBeDefined();
  });

  it('should return the ConfigurableMap component without search button in map', async () => {
    const payload = {
      title: 'title',
      componentType: 'Map' as const,
      id: 'mapid-1',
      initialProps: {
        mapPreset: undefined,
        syncGroupsIds: [],
        displaySearchButtonInMap: false,
      },
    };

    render(
      <DemoWrapperConnect store={store}>
        {componentsLookUp(payload)}
      </DemoWrapperConnect>,
    );
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('search-title-aria'),
      }),
    ).toBeFalsy();
  });

  it('should return the ModelRunInterval component', () => {
    const payload = {
      title: 'title',
      componentType: 'ModelRunInterval' as const,
      id: 'viewid-1',
      initialProps: {
        layers: [],
        syncGroupsIds: [''],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props['data-testid']).toEqual('coreModelRunInterval');
  });

  it('should return the null if not recognized', () => {
    const payload = {
      title: 'title',
      componentType: undefined!,
      id: 'viewid-1',
      initialProps: undefined!,
    };
    const component = componentsLookUp(payload);
    expect(component).toBeNull();
  });

  it('should return the TimeSlider component', () => {
    const payload = {
      title: 'title',
      componentType: 'TimeSlider' as const,
      id: 'viewid-1',
      initialProps: {
        sliderPreset: {
          mapId: 'test-1',
        },
        syncGroupsIds: [],
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props.sourceId).toEqual('viewid-1');
    expect(component.props['data-testid']).toEqual('coreTimeSliderConnect');
  });
});
