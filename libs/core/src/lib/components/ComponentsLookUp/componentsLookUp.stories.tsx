/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';

import {
  Paper,
  Card,
  CardContent,
  Typography,
  Select,
  MenuItem,
  SelectChangeEvent,
} from '@mui/material';
import { publicLayers } from '@opengeoweb/webmap-react';

import { PROJECTION } from '@opengeoweb/shared';
import {
  componentsLookUp,
  InitialProps,
  SupportedComponentTypes,
} from './componentsLookUp';
import { DemoWrapperConnect } from '../Providers/Providers';
import { createMockStore } from '../../store';

export default {
  title: 'components/ComponentsLookUp',
};
const store = createMockStore();
const componentTypesWithInitialPresetProps: Record<string, InitialProps> = {
  Map: {
    mapPreset: undefined,
    syncGroupsIds: [],
  },
  HarmonieTempAndPrecipPreset: {
    layers: {
      topRow: publicLayers.harmonieAirTemperature,
      bottomRow: publicLayers.harmoniePrecipitation,
    },
  },
  ModelRunInterval: {
    layers: [
      publicLayers.harmonieAirTemperature,
      publicLayers.harmoniePrecipitation,
    ],
    syncGroupsIds: [],
  },
  MultiMap: {
    shouldShowZoomControls: false,
    mapPreset: [
      {
        layers: [publicLayers.radarLayer],
        proj: {
          bbox: {
            left: -7529663.50832266,
            bottom: 308359.5390525013,
            right: 7493930.85787452,
            top: 11742807.68245839,
          },
          srs: PROJECTION.EPSG_3857.value,
        },
      },
      {
        layers: [publicLayers.obsAirTemperature],
        proj: {
          bbox: {
            left: -7529663.50832266,
            bottom: 308359.5390525013,
            right: 7493930.85787452,
            top: 11742807.68245839,
          },
          srs: PROJECTION.EPSG_3857.value,
        },
      },
      {
        layers: [publicLayers.harmoniePrecipitation],
        proj: {
          bbox: {
            left: -7529663.50832266,
            bottom: 308359.5390525013,
            right: 7493930.85787452,
            top: 11742807.68245839,
          },
          srs: PROJECTION.EPSG_3857.value,
        },
      },
      {
        layers: [publicLayers.msgNaturalenhncdEUMETSAT],
        proj: {
          bbox: {
            left: -7529663.50832266,
            bottom: 308359.5390525013,
            right: 7493930.85787452,
            top: 11742807.68245839,
          },
          srs: PROJECTION.EPSG_3857.value,
        },
      },
    ],
    syncGroupsIds: ['Area_screenRadAndObs', 'Time_screenRadAndObs'],
  },
  TimeSlider: {
    sliderPreset: { mapId: 'map-1' },
    syncGroupsIds: [],
  },
};

const availableComponentTypes = Object.keys(
  componentTypesWithInitialPresetProps,
).map((key) => key);

const DEFAULT_COMPONENT_TYPE = 'Map';

interface SelectComponentTypeProps {
  selectedComponentType: SupportedComponentTypes;
  onChangeComponentType: (preset: SupportedComponentTypes) => void;
}

const SelectComponentType: React.FC<SelectComponentTypeProps> = ({
  selectedComponentType,
  onChangeComponentType,
}: SelectComponentTypeProps) => {
  const onChangeSelect = (event: SelectChangeEvent): void => {
    onChangeComponentType(event.target.value as SupportedComponentTypes);
  };

  return (
    <Paper style={{ position: 'absolute', left: 50, top: 40, zIndex: 51 }}>
      <Card>
        <CardContent>
          <Typography variant="subtitle1">ComponentType:</Typography>
          <Select value={selectedComponentType} onChange={onChangeSelect}>
            {availableComponentTypes.map((componentType) => (
              <MenuItem key={componentType} value={componentType}>
                {componentType}
              </MenuItem>
            ))}
          </Select>
        </CardContent>
      </Card>
    </Paper>
  );
};

// Demo of componentsLookUp. This only shows the layout, button logic is not working
const Demo: React.FC = () => {
  const [selectedComponentType, onChangeComponentType] =
    React.useState<SupportedComponentTypes>(DEFAULT_COMPONENT_TYPE);
  const initialProps =
    componentTypesWithInitialPresetProps[selectedComponentType];

  return (
    <div style={{ width: '100vw', height: '100vh' }}>
      <SelectComponentType
        selectedComponentType={selectedComponentType}
        onChangeComponentType={onChangeComponentType}
      />
      {componentsLookUp({
        componentType: selectedComponentType,
        initialProps,
        id: selectedComponentType,
      })}
    </div>
  );
};

export const ComponentsLookUpLightTheme: React.FC = () => (
  <DemoWrapperConnect store={store}>
    <Demo />
  </DemoWrapperConnect>
);

export const ComponentsLookUpDarkTheme: React.FC = () => (
  <DemoWrapperConnect store={store} theme={darkTheme}>
    <Demo />
  </DemoWrapperConnect>
);
