/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';

import { LayerManagerConnect } from '../../LayerManager';
import { DemoWrapperConnect } from '../../Providers/Providers';
import { MultiMapDimensionSelectConnect } from '../../MultiMapDimensionSelectConnect';
import { ModelRunInterval } from './ModelRunInterval';
import { createMockStore } from '../../../store';

export default { title: 'components/MultiMapViewConnect/ModelRunInterval' };
const store = createMockStore();
export const ModelRunIntervalDefault = (): React.ReactElement => {
  return (
    <DemoWrapperConnect store={store}>
      <div style={{ width: '100%', height: '100vh' }}>
        <ModelRunInterval syncGroupsIds={[]} />
        <MultiMapDimensionSelectConnect />
        <LayerManagerConnect showMapIdInTitle />
      </div>
    </DemoWrapperConnect>
  );
};

export const ModelRunIntervalHourly = (): React.ReactElement => {
  return (
    <DemoWrapperConnect store={store}>
      <div style={{ width: '100%', height: '100vh' }}>
        <ModelRunInterval syncGroupsIds={[]} interval={1} />
        <MultiMapDimensionSelectConnect />
        <LayerManagerConnect showMapIdInTitle />
      </div>
    </DemoWrapperConnect>
  );
};

export const ModelRunIntervalCustomStart = (): React.ReactElement => {
  return (
    <DemoWrapperConnect store={store}>
      <div style={{ width: '100%', height: '100vh' }}>
        <ModelRunInterval
          syncGroupsIds={[]}
          interval={1}
          startTimeIncrement={0}
        />
        <MultiMapDimensionSelectConnect />
        <LayerManagerConnect showMapIdInTitle />
      </div>
    </DemoWrapperConnect>
  );
};
