/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, fireEvent, screen, within } from '@testing-library/react';
import { initialState } from '@opengeoweb/store';
import { SyncGroupViewerConnect } from './SyncGroupViewerConnect';
import { createMockStore } from '../../store';
import { initCoreReactI18n } from '../../utils/i18n';
import { CoreThemeStoreProvider } from '../Providers/Providers';

describe('SyncGroupViewerConnect', () => {
  beforeAll(() => {
    initCoreReactI18n();
  });

  const mockStore = {
    ui: {
      order: [],
      dialogs: {
        syncGroups: {
          isOpen: true,
          type: 'syncGroups',
          activeMapId: 'someMapId',
        },
      },
    },
    syncGroups: {
      ...initialState,
    },
  };

  it('should dispatch correct actions when toggleTimeScroll is called', () => {
    const store = createMockStore(mockStore);

    render(
      <CoreThemeStoreProvider store={store}>
        <SyncGroupViewerConnect />
      </CoreThemeStoreProvider>,
    );

    const switchElement = within(
      screen.getByTestId('syncGroupViewer'),
    ).getByRole('checkbox');

    // Assert initial state (unchecked)
    expect(switchElement).not.toBeChecked();
    expect(store.getState().syncGroups.isTimeScrollingEnabled).toBeFalsy();

    fireEvent.click(switchElement);

    expect(switchElement).toBeChecked();
    expect(store.getState().syncGroups.isTimeScrollingEnabled).toBeTruthy();

    fireEvent.click(switchElement);

    expect(switchElement).not.toBeChecked();
    expect(store.getState().syncGroups.isTimeScrollingEnabled).toBeFalsy();
  });

  it('should toggle workspace time scroll ON and OFF using keyboard', async () => {
    const store = createMockStore(mockStore);

    render(
      <CoreThemeStoreProvider store={store}>
        <SyncGroupViewerConnect />
      </CoreThemeStoreProvider>,
    );

    const switchElement = screen.getByRole('checkbox', {
      name: /workspace time scroll toggle/i,
    });
    expect(switchElement).toBeTruthy();
    expect(switchElement).not.toBeChecked();

    fireEvent.keyDown(window, {
      key: 's',
      code: 'KeyS',
      charCode: 83,
    });

    expect(switchElement).toBeChecked();

    fireEvent.keyDown(window, {
      key: 's',
      code: 'KeyS',
      charCode: 83,
    });

    expect(switchElement).not.toBeChecked();
  });
});
