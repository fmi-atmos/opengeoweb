/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

export const QUERYWMS_GETCAPABILITIES = 'QUERYWMS_GETCAPABILITIES';
export const QUERYWMS_LAYERSTREE = 'QUERYWMS_LAYERSTREE';
export const QUERYWMS_SERVICEINFO = 'QUERYWMS_SERVICEINFO';
export const QUERYWMS_LAYERS = 'QUERYWMS_LAYERS';
export const QUERYWMS_LAYER = 'QUERYWMS_LAYER';

export const queryWMSKeys = [
  QUERYWMS_GETCAPABILITIES,
  QUERYWMS_LAYERSTREE,
  QUERYWMS_SERVICEINFO,
  QUERYWMS_LAYERS,
  QUERYWMS_LAYER,
];
