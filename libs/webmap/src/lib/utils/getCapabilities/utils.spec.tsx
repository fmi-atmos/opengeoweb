/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { waitFor } from '@testing-library/react';

import { WMS111GetCapabilitiesGeoServicesRADAR } from '../../specs/getCapabilitySpecs/WMS111GetCapabilitiesGeoServicesRADAR';

import {
  LayerTree,
  WMSLayerFromGetCapabilities,
  WMSVersion,
} from '../../components';
import WMS130GetCapabilitiesRadarTestWithInheritLayerprops from '../../specs/getCapabilitySpecs/WMS130GetCapabilitiesRadarTestWithInheritLayerprops';
import WMS130GetCapabilitiesRadarTestWithInheritLayerpropsToLayerTree from '../../specs/getCapabilitySpecs/WMS130GetCapabilitiesRadarTestWithInheritLayerpropsToLayerTree';

import { WMXMLStringToJson } from '../../components/WMXMLParser';
import { queryWMSLayers } from '../../query/queryWMS';
import {
  privateGetOGCCapabilitiesUrl,
  privateWmsFlattenLayerTree,
  privateWmsGetCapabilityElement,
  privateWmsGetCapabilities,
  privateCheckVersion,
  privateWmsBuildLayerTreeFromNestedWMSLayer,
  privateWmsGetRootLayerFromGetCapabilities,
  privateWmtsGetCapabilities,
} from './utils';
import {
  crsListForRADNL25PCPCMLayer,
  styleListForRADNL25PCPCMLayer,
} from '../../specs';

describe('utils/getCapabilities', () => {
  const storedFetch = global['fetch'];

  afterEach(() => {
    global['fetch'] = storedFetch;
  });

  const expectedLayerTree = [
    {
      name: 'RAD_NL25_PCP_CM',
      title: 'Precipitation Radar NL',
      leaf: true,
      path: ['WMS of  RADAR'],
      keywords: ['Radar'],
      abstract: 'Radar NL',
      styles: styleListForRADNL25PCPCMLayer,
      dimensions: [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2021-05-17T00:00:00Z',
          values: '2021-03-31T09:25:00Z/2021-05-18T07:45:00Z/PT5M',
        },
      ],
      geographicBoundingBox: {
        east: '10.856452',
        west: '0.000000',
        north: '55.973600',
        south: '48.895303',
      },
      queryable: true,
      crs: crsListForRADNL25PCPCMLayer,
    },
  ];

  describe('privateGetOGCCapabilitiesUrl', () => {
    it('should return correct service url', () => {
      const url = 'https://www.testurl.com/wms';
      const result = privateGetOGCCapabilitiesUrl(url);
      expect(result).toEqual(`${url}?service=WMS&request=GetCapabilities`);
    });
    it('should return correct service url with custom params', () => {
      const url = 'https://www.testurl.com/wms';
      const random = Math.random();
      const params = { random };
      const result = privateGetOGCCapabilitiesUrl(url, 'WMS', params);
      expect(result).toEqual(
        `${url}?service=WMS&request=GetCapabilities&random=${random}`,
      );
    });
    it('should not overwrite any service or request params already in the url and add if not there', () => {
      const url =
        'https://www.testurl.com/wms?request=GetCapabilitiesThatIsAlreadyThere';
      const result = privateGetOGCCapabilitiesUrl(url);
      expect(result).toEqual(`${url}&service=WMS`);
    });
  });
  describe('privateWmsFlattenLayerTree', () => {
    it('should flatten and sort the LayerTree', () => {
      const layerTree: LayerTree = {
        leaf: false,
        title: 'A',
        name: undefined!,
        path: [],
        keywords: [],
        abstract: '',
        styles: [],
        dimensions: [],
        geographicBoundingBox: null!,
        children: [
          {
            leaf: false,
            title: 'B',
            name: undefined!,
            path: ['A'],
            keywords: [],
            abstract: '',
            styles: [],
            dimensions: [],
            geographicBoundingBox: null!,
            children: [
              {
                leaf: false,
                name: undefined!,
                title: 'C',
                path: ['A', 'B'],
                keywords: [],
                abstract: '',
                styles: [],
                dimensions: [],
                geographicBoundingBox: null!,
                children: [
                  {
                    leaf: true,
                    name: '2',
                    title: '2: Layer',
                    path: ['A', 'B', 'C'],
                    children: [],
                    keywords: [],
                    styles: [],
                    dimensions: [],
                    geographicBoundingBox: null!,
                    abstract: '',
                  },
                  {
                    leaf: true,
                    name: '1',
                    title: '1: Layer',
                    path: ['A', 'B', 'C'],
                    keywords: [],
                    styles: [],
                    dimensions: [],
                    geographicBoundingBox: null!,
                    abstract: '',
                  } as unknown as LayerTree,
                  {
                    leaf: true,
                    name: '3',
                    title: '3: Layer',
                    path: ['A', 'B', 'C'],
                    children: [],
                    keywords: [],
                    styles: [],
                    dimensions: [],
                    geographicBoundingBox: null!,
                    abstract: '',
                  },
                  {
                    leaf: false,
                    name: undefined!,
                    title: 'D',
                    path: ['A', 'B', 'C'],
                    children: [
                      {
                        leaf: true,
                        name: 'd',
                        title: '4: Layer',
                        path: ['A', 'B', 'C', 'D'],
                        children: [],
                        keywords: [],
                        styles: [],
                        dimensions: [],
                        geographicBoundingBox: null!,
                        abstract: '',
                      },
                    ],
                    keywords: [],
                    styles: [],
                    dimensions: [],
                    geographicBoundingBox: null!,
                    abstract: '',
                  },
                ],
              },
            ],
          },
        ],
      };
      const layerProps = privateWmsFlattenLayerTree(layerTree);
      expect(layerProps).toEqual([
        {
          name: '1',
          leaf: true,
          path: ['A', 'B', 'C'],
          title: '1: Layer',
          abstract: '',
          keywords: [],
          styles: [],
          dimensions: [],
          geographicBoundingBox: null,
        },
        {
          name: '2',
          leaf: true,
          path: ['A', 'B', 'C'],
          title: '2: Layer',
          abstract: '',
          keywords: [],
          styles: [],
          dimensions: [],
          geographicBoundingBox: null,
        },
        {
          name: '3',
          leaf: true,
          path: ['A', 'B', 'C'],
          title: '3: Layer',
          abstract: '',
          keywords: [],
          styles: [],
          dimensions: [],
          geographicBoundingBox: null,
        },
        {
          name: 'd',
          leaf: true,
          path: ['A', 'B', 'C', 'D'],
          title: '4: Layer',
          abstract: '',
          keywords: [],
          styles: [],
          dimensions: [],
          geographicBoundingBox: null,
        },
      ]);
    });
    it('should not crash if empty array is given', () => {
      const layerProps = privateWmsFlattenLayerTree([] as unknown as LayerTree);
      expect(layerProps).toHaveLength(0);
    });
    it('should not crash if a string is given instead of an array', () => {
      const layerProps = privateWmsFlattenLayerTree({
        children: 'test',
      } as unknown as LayerTree);
      expect(layerProps).toHaveLength(0);
    });
    it('should not crash if no children are given', () => {
      const layerProps = privateWmsFlattenLayerTree([
        { children: [] },
      ] as unknown as LayerTree);
      expect(layerProps).toHaveLength(0);
    });
    it('should not crash if children is not an array', () => {
      const layerProps = privateWmsFlattenLayerTree([
        { children: 'not an array but a string' },
      ] as unknown as LayerTree);
      expect(layerProps).toHaveLength(0);
    });
  });

  describe('queryWMSLayers', () => {
    it('should return layers flattened', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest.fn().mockResolvedValueOnce({
        text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
        headers,
      });
      const spy = jest.spyOn(window, 'fetch');

      await waitFor(async () => {
        await queryWMSLayers('https://testurlnodes/wms?').then((nodes) => {
          expect(nodes).toEqual(expectedLayerTree);
        });
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    it('should handle empty layers', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest.fn().mockResolvedValueOnce({
        text: () =>
          Promise.resolve(
            `<?xml version="1.0" encoding="utf-8"?>
            <WMS_Capabilities version="1.3.0">
              <Capability>
                <Layer/>
              </Capability>
            </WMS_Capabilities>`,
          ),
        headers,
      });
      const spy = jest.spyOn(window, 'fetch');
      await waitFor(async () => {
        await queryWMSLayers('https://testurlempty/wms?').then((nodes) => {
          expect(nodes).toEqual([]);
        });
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    // TODO:!!!
    // it('should not reload allready loaded services', async () => {
    //   // mock the WMXMLParser fetch
    //   const headers = new Headers();
    //   window.fetch = jest
    //     .fn()
    //     .mockResolvedValueOnce({
    //       text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
    //       headers,
    //     })
    //     .mockResolvedValueOnce({
    //       text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
    //       headers,
    //     });
    //   const spy = jest.spyOn(window, 'fetch');

    //   await waitFor(async () => {
    //     await queryWMSLayers('https://sameurltwice/wms?').then(
    //       (nodes) => {
    //         expect(nodes).toEqual(expectedLayerTree);
    //       },
    //     );
    //     await queryWMSLayers('https://sameurltwice/wms?').then(
    //       (nodes) => {
    //         expect(nodes).toEqual(expectedLayerTree);
    //       },
    //     );
    //     expect(spy).toHaveBeenCalledTimes(1);
    //   });
    // });

    it('should reload previously loaded services', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest
        .fn()
        .mockResolvedValueOnce({
          text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
          headers,
        })
        .mockResolvedValueOnce({
          text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
          headers,
        });
      const spy = jest.spyOn(window, 'fetch');

      await waitFor(async () => {
        await queryWMSLayers('https://sameurltwice/wms?', true).then(
          (nodes) => {
            expect(nodes).toEqual(expectedLayerTree);
          },
        );
        await queryWMSLayers('https://sameurltwice/wms?', true).then(
          (nodes) => {
            expect(nodes).toEqual(expectedLayerTree);
          },
        );
        expect(spy).toHaveBeenCalledTimes(2);
      });
    });

    it('should return an error message when the request fails', async () => {
      // mock the WMXMLParser fetch with a failed request
      window.fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.reject(new Error('failed')));

      await waitFor(() => {
        queryWMSLayers('https://testfailedurl/wms?').catch((error) => {
          expect(error.message).toEqual('failed');
        });
      });
    });
  });
  describe('privateWmsGetCapabilityElement', () => {
    it('should fail when no Capability data available', () => {
      const jsonData = {};
      expect(() => privateWmsGetCapabilityElement(jsonData)).toThrow(
        new Error('No Capability element found in service'),
      );
    });

    it('should return Capability object for version 1.1.1', () => {
      const jsonData = {
        WMT_MS_Capabilities: {
          attr: { version: WMSVersion.version111 },
          Capability: { Layer: {} },
        },
      };
      expect(privateWmsGetCapabilityElement(jsonData)).toEqual(
        jsonData.WMT_MS_Capabilities.Capability,
      );
    });
    it('should return Capability object for version 1.3.0', () => {
      const jsonData = {
        WMS_Capabilities: {
          attr: { version: WMSVersion.version130 },
          Capability: { Layer: {} },
        },
      };
      expect(privateWmsGetCapabilityElement(jsonData)).toEqual(
        jsonData.WMS_Capabilities.Capability,
      );
    });
  });
  describe('privateWmsGetCapabilities', () => {
    const mockRandom = 123;
    beforeEach(() => {
      jest.spyOn(global.Math, 'random').mockReturnValue(mockRandom);
    });
    afterEach(() => {
      jest.spyOn(global.Math, 'random').mockRestore();
    });
    it('should call fail when service undefined', async () => {
      const service = undefined;
      const succes = jest.fn();
      const fail = jest.fn();
      const options = undefined;

      window.fetch = jest.fn();

      privateWmsGetCapabilities(service!, options!).then(succes).catch(fail);

      await waitFor(() => {
        expect(succes).not.toHaveBeenCalled();
      });
      expect(fail).toHaveBeenCalledWith(Error('No service defined'));
    });

    it('should call fail when service empty', async () => {
      const service = '';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = undefined;

      window.fetch = jest.fn();

      privateWmsGetCapabilities(service, options!).then(succes).catch(fail);

      await waitFor(() => {
        expect(succes).not.toHaveBeenCalled();
      });
      expect(fail).toHaveBeenCalledWith(Error('Service URL is empty'));
    });

    it('should call fail when service doesnt start with http, https, / or //', async () => {
      const service = 'htt://serviceurl';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = undefined;

      window.fetch = jest.fn();

      privateWmsGetCapabilities(service, options!).then(succes).catch(fail);

      await waitFor(() => {
        expect(succes).not.toHaveBeenCalled();
      });
      expect(fail).toHaveBeenCalledWith(Error('Service URL is empty'));
    });

    it('should call succes when capabilities are loaded', async () => {
      const service = 'https://testservice.nl/wms';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = {
        headers: [{ name: 'content-type', value: 'application/xml' }],
      };
      const expectedHeaders = new Headers();
      expectedHeaders.append(options.headers[0].name, options.headers[0].value);

      // mock the WMXMLParser fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
          ),
        headers: expectedHeaders,
      });
      const spy = jest.spyOn(window, 'fetch');

      privateWmsGetCapabilities(service, options.headers)
        .then(succes)
        .catch(fail);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(
          expect.stringContaining(
            `${service}?service=WMS&request=GetCapabilities`,
          ),
          {
            cache: 'no-cache',
            headers: expectedHeaders,
            method: 'GET',
            mode: 'cors',
          },
        );
      });
      expect(succes).toHaveBeenCalledWith({
        testelement: {
          attr: { myattr: 'myattrvalue' },
          value: 'myelementvalue',
        },
      });
      expect(fail).not.toHaveBeenCalled();
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should call success with disabled cache', async () => {
      const service = 'https://testservice.com/wms';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = {
        headers: [{ name: 'content-type', value: 'application/xml' }],
      };
      const expectedHeaders = new Headers();
      expectedHeaders.append(options.headers[0].name, options.headers[0].value);

      // mock the WMXMLParser fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
          ),
        headers: expectedHeaders,
      });

      const spy = jest.spyOn(window, 'fetch');

      privateWmsGetCapabilities(service, options.headers, true)
        .then(succes)
        .catch(fail);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(
          expect.stringContaining(
            `${service}?service=WMS&request=GetCapabilities&random=${mockRandom}`,
          ),
          {
            cache: 'no-cache',
            headers: expectedHeaders,
            method: 'GET',
            mode: 'cors',
          },
        );
      });
      expect(succes).toHaveBeenCalledWith({
        testelement: {
          attr: { myattr: 'myattrvalue' },
          value: 'myelementvalue',
        },
      });
      expect(fail).not.toHaveBeenCalled();
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });

  describe('privateCheckVersion', () => {
    it('should fail when no version data available', () => {
      const jsonData = {};

      expect(
        () =>
          privateCheckVersion(jsonData) === 'Unable to determine WMS version',
      );
    });
    it('should return version 1.1.1 when version is 1.0.0', () => {
      const jsonData = {
        WMT_MS_Capabilities: { attr: { version: WMSVersion.version100 } },
      };

      expect(privateCheckVersion(jsonData)).toEqual(WMSVersion.version111);
    });

    it('should return version 1.1.1 when version is 1.1.1', () => {
      const jsonData = {
        WMT_MS_Capabilities: {
          attr: { version: WMSVersion.version111 },
          Capability: { Layer: {} },
        },
      };

      expect(privateCheckVersion(jsonData)).toEqual(WMSVersion.version111);
    });

    it('should return version 1.3.0 when version is 1.3.0', () => {
      const jsonData = {
        WMS_Capabilities: {
          attr: { version: WMSVersion.version130 },
          Capability: { Layer: {} },
        },
      };

      expect(privateCheckVersion(jsonData)).toEqual(WMSVersion.version130);
    });
    it('should fail when version 1.3.0 has no Capability', () => {
      const jsonData = {
        WMS_Capabilities: {
          attr: { version: WMSVersion.version130 },
        },
      };
      expect(() => privateCheckVersion(jsonData)).toThrow(
        new Error('No WMS Capability element found'),
      );
    });

    it('should fail when version 1.1.1 has no Capability', () => {
      const jsonData = {
        WMT_MS_Capabilities: {
          attr: { version: WMSVersion.version111 },
        },
      };

      expect(() => privateCheckVersion(jsonData)).toThrow(
        new Error('No WMS Capability element found'),
      );
    });
    it('should fail when version 1.3.0 Capability has no Layer', () => {
      const jsonData = {
        WMS_Capabilities: {
          attr: { version: WMSVersion.version130 },
          Capability: {},
        },
      };
      expect(() => privateCheckVersion(jsonData)).toThrow(
        new Error('No WMS Layer element found'),
      );
    });

    it('should fail when version 1.1.1 Capability has no Layer', () => {
      const jsonData = {
        WMT_MS_Capabilities: {
          attr: { version: WMSVersion.version111 },
          Capability: {},
        },
      };

      expect(() => privateCheckVersion(jsonData)).toThrow(
        new Error('No WMS Layer element found'),
      );
    });
  });
  describe('privateWmsBuildLayerTreeFromNestedWMSLayer', () => {
    it('should not do anything when no layers', () => {
      const wmsLayer = {} as unknown as WMSLayerFromGetCapabilities;
      const rootNode = privateWmsBuildLayerTreeFromNestedWMSLayer(wmsLayer);
      expect(rootNode).toEqual({
        abstract: undefined,
        children: [],
        dimensions: [],
        crs: [],
        geographicBoundingBox: undefined,
        keywords: [],
        leaf: false,
        name: null,
        path: [],
        queryable: false,
        styles: [],
        title: undefined,
      });
    });
    it('should add given layers to the rootNode', () => {
      const wmsLayer = {
        Name: { value: 'nameWithChildren' },
        Title: { value: 'titleWithChildren' },
        Layer: [
          {
            Name: { value: 'layer1' },
            Title: { value: 'title1' },
            Layer: [
              {
                Name: { value: 'subLayer1' },
                Title: { value: 'subTitle1' },
              },
            ],
          },
          {
            Name: { value: 'layer2' },
            Title: { value: 'title2' },
          },
        ],
      } as unknown as WMSLayerFromGetCapabilities;

      const rootNode = privateWmsBuildLayerTreeFromNestedWMSLayer(wmsLayer);

      expect(rootNode).toEqual({
        abstract: undefined,
        children: [
          {
            abstract: undefined,
            children: [
              {
                abstract: undefined,
                children: [],
                dimensions: [],
                crs: [],
                geographicBoundingBox: undefined,
                keywords: [],
                leaf: true,
                name: 'subLayer1',
                path: ['titleWithChildren', 'title1'],
                queryable: false,
                styles: [],
                title: 'subTitle1',
              },
            ],
            dimensions: [],
            crs: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: false,
            name: 'layer1',
            path: ['titleWithChildren'],
            queryable: false,
            styles: [],
            title: 'title1',
          },
          {
            abstract: undefined,
            children: [],
            dimensions: [],
            crs: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: true,
            name: 'layer2',
            path: ['titleWithChildren'],
            queryable: false,
            styles: [],
            title: 'title2',
          },
        ],
        dimensions: [],
        crs: [],
        geographicBoundingBox: undefined,
        keywords: [],
        leaf: false,
        name: 'nameWithChildren',
        path: [],
        queryable: false,
        styles: [],
        title: 'titleWithChildren',
      });
    });

    it('should handle layers with empty names and titles', () => {
      const wmsLayer = {
        Layer: [
          {
            Name: null,
            Title: null,
          },
          {
            Name: null,
            Title: { value: null },
          },
        ],
      } as unknown as WMSLayerFromGetCapabilities;

      const rootNode = privateWmsBuildLayerTreeFromNestedWMSLayer(wmsLayer);

      expect(rootNode).toEqual({
        abstract: undefined,
        children: [
          {
            abstract: undefined,
            children: [],
            dimensions: [],
            crs: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: null,
            name: null,
            path: [''],
            queryable: false,
            styles: [],
            title: undefined,
          },
          {
            abstract: undefined,
            children: [],
            dimensions: [],
            crs: [],
            geographicBoundingBox: undefined,
            keywords: [],
            leaf: null,
            name: null,
            path: [''],
            queryable: false,
            styles: [],
            title: undefined,
          },
        ],
        crs: [],
        dimensions: [],
        geographicBoundingBox: undefined,
        keywords: [],
        leaf: false,
        queryable: false,
        name: null,
        path: [],
        styles: [],
        title: undefined,
      });
    });

    it('should inherit properties', () => {
      expect(
        privateWmsBuildLayerTreeFromNestedWMSLayer(
          privateWmsGetRootLayerFromGetCapabilities(
            WMXMLStringToJson(
              WMS130GetCapabilitiesRadarTestWithInheritLayerprops,
            ),
          ),
        ),
      ).toEqual(WMS130GetCapabilitiesRadarTestWithInheritLayerpropsToLayerTree);
    });
  });

  describe('privateWmtsGetCapabilities', () => {
    const mockRandom = 123;
    beforeEach(() => {
      jest.spyOn(global.Math, 'random').mockReturnValue(mockRandom);
    });
    afterEach(() => {
      jest.spyOn(global.Math, 'random').mockRestore();
    });
    it('should call fail when service undefined', async () => {
      const service = undefined;
      const succes = jest.fn();
      const fail = jest.fn();
      const options = undefined;

      window.fetch = jest.fn();

      privateWmtsGetCapabilities(service!, options!).then(succes).catch(fail);

      await waitFor(() => {
        expect(succes).not.toHaveBeenCalled();
      });
      expect(fail).toHaveBeenCalledWith(Error('No service defined'));
    });

    it('should call fail when service empty', async () => {
      const service = '';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = undefined;

      window.fetch = jest.fn();

      privateWmtsGetCapabilities(service, options!).then(succes).catch(fail);

      await waitFor(() => {
        expect(succes).not.toHaveBeenCalled();
      });
      expect(fail).toHaveBeenCalledWith(Error('Service URL is empty'));
    });

    it('should call fail when service doesnt start with http, https, / or //', async () => {
      const service = 'htt://serviceurl';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = undefined;

      window.fetch = jest.fn();

      privateWmtsGetCapabilities(service, options!).then(succes).catch(fail);

      await waitFor(() => {
        expect(succes).not.toHaveBeenCalled();
      });
      expect(fail).toHaveBeenCalledWith(Error('Service URL is empty'));
    });

    it('should call succes when capabilities are loaded', async () => {
      const service = 'https://testservice.nl/wmts';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = {
        headers: [{ name: 'content-type', value: 'application/xml' }],
      };
      const expectedHeaders = new Headers();
      expectedHeaders.append(options.headers[0].name, options.headers[0].value);

      // mock the WMXMLParser fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
          ),
        headers: expectedHeaders,
      });
      const spy = jest.spyOn(window, 'fetch');

      privateWmtsGetCapabilities(service, options.headers)
        .then(succes)
        .catch(fail);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(
          expect.stringContaining(
            `${service}?service=WMTS&request=GetCapabilities`,
          ),
          {
            headers: expectedHeaders,
          },
        );
      });
      expect(succes).toHaveBeenCalledWith(
        '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
      );
      expect(fail).not.toHaveBeenCalled();
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should call succes when capabilities are loaded without headers', async () => {
      const service = 'https://testservice.nl/wmts';
      const succes = jest.fn();
      const fail = jest.fn();

      // mock the WMXMLParser fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
          ),
        headers: {},
      });
      const spy = jest.spyOn(window, 'fetch');

      privateWmtsGetCapabilities(service).then(succes).catch(fail);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(
          `${service}?service=WMTS&request=GetCapabilities`,
          { headers: new Headers() },
        );
      });
      expect(succes).toHaveBeenCalledWith(
        '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
      );
      expect(fail).not.toHaveBeenCalled();
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should call success with disabled cache', async () => {
      const service = 'https://testservice.com/wmts';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = {
        headers: [{ name: 'content-type', value: 'application/xml' }],
      };
      const expectedHeaders = new Headers();
      expectedHeaders.append(options.headers[0].name, options.headers[0].value);

      // mock the WMXMLParser fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
          ),
        headers: expectedHeaders,
      });

      const spy = jest.spyOn(window, 'fetch');

      privateWmtsGetCapabilities(service, options.headers, true)
        .then(succes)
        .catch(fail);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(
          expect.stringContaining(
            `${service}?service=WMTS&request=GetCapabilities&random=${mockRandom}`,
          ),
          {
            headers: expectedHeaders,
          },
        );
      });
      expect(succes).toHaveBeenCalledWith(
        '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
      );
      expect(fail).not.toHaveBeenCalled();
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
});
