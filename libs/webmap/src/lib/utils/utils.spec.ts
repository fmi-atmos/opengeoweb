/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  getWMJSTimeDimensionForLayerId,
  getWMLayerById,
  registerWMLayer,
  registerWMJSMap,
  unRegisterWMJSMap,
  getWMJSMapById,
  getWMJSMapIds,
  getCorrectWMSDimName,
} from './utils';

import { WMJSMap } from '../components';
import { WmMultiDimensionLayer, WmLayerWithoutTimeDimension } from '../specs';

describe('store/mapStore/utils/helpers', () => {
  const layerIdWithTime = 'test_id_1';
  WmMultiDimensionLayer.id = layerIdWithTime;
  registerWMLayer(WmMultiDimensionLayer, WmMultiDimensionLayer.id);

  const layerIdWithoutTime = 'test_id_2';
  WmLayerWithoutTimeDimension.id = layerIdWithoutTime;
  registerWMLayer(WmLayerWithoutTimeDimension, WmLayerWithoutTimeDimension.id);

  describe('getWMLayerById', () => {
    it('should return the WMJSLayer for a layer id', () => {
      const wmLayer = getWMLayerById(layerIdWithTime);

      expect(wmLayer).toBeDefined();
    });
    it('should return the same id for given layer id', () => {
      const wmLayer = getWMLayerById(layerIdWithTime);

      expect(wmLayer.id).toEqual(layerIdWithTime);
    });
    it('should return undefined when layer does not exist', () => {
      const wmLayer = getWMLayerById('hello');
      expect(wmLayer).toBeUndefined();
    });
  });

  describe('getWMJSTimeDimensionForLayerId', () => {
    it('should return the WMJSTimeDimension for a layer id', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension).toBeDefined();
    });

    it('should have the name time', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension.name).toEqual('time');
    });

    it('should have a currentValue', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension.currentValue).toEqual('2020-03-13T14:40:00Z');
    });

    it('should have a size', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension.size()).toEqual(49);
    });

    it('should return undefined if the layer has no time dimension', () => {
      const wmjsTimeDimension =
        getWMJSTimeDimensionForLayerId(layerIdWithoutTime);
      expect(wmjsTimeDimension).toBeUndefined();
    });

    it('should return null if the layer does not exist', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId('hello');
      expect(wmjsTimeDimension).toBeNull();
    });
  });
  describe('registerWMJSMap', () => {
    it('should register a map', () => {
      const mapId1 = 'mapid1';
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      jest.spyOn(console, 'warn').mockImplementation();
      registerWMJSMap(wmjsmap, mapId1);
      expect(console.warn).not.toHaveBeenCalled();
      /* Second time should trigger a warning */
      registerWMJSMap(wmjsmap, mapId1);
      expect(console.warn).toHaveBeenCalled();
      unRegisterWMJSMap(mapId1);
    });

    it('should unregister a map', () => {
      const mapId1 = 'mapid1';
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      const mapDestroySpy = jest.spyOn(wmjsmap, 'destroy');
      registerWMJSMap(wmjsmap, mapId1);
      unRegisterWMJSMap(mapId1);
      expect(mapDestroySpy).toHaveBeenCalled();
    });
  });
  describe('getWMJSMapById', () => {
    it('should return a map with given id', () => {
      const mapId1 = 'mapid1';
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      registerWMJSMap(wmjsmap, mapId1);
      expect(getWMJSMapById(mapId1)).toBe(wmjsmap);
      unRegisterWMJSMap(mapId1);
    });
  });

  describe('getWMJSMapIds', () => {
    it('should return all registered mapids', () => {
      const mapId1 = 'mapid1';
      const baseElement1 = document.createElement('div');
      const wmjsmap1 = new WMJSMap(baseElement1);
      registerWMJSMap(wmjsmap1, mapId1);
      expect(getWMJSMapIds()).toStrictEqual([mapId1]);

      const mapId2 = 'mapid2';
      const baseElement2 = document.createElement('div');
      const wmjsmap2 = new WMJSMap(baseElement2);
      registerWMJSMap(wmjsmap2, mapId2);

      expect(getWMJSMapIds()).toStrictEqual([mapId1, mapId2]);

      unRegisterWMJSMap(mapId1);

      expect(getWMJSMapIds()).toStrictEqual([mapId2]);

      unRegisterWMJSMap(mapId2);

      expect(getWMJSMapIds()).toStrictEqual([]);
    });
  });
  describe('getCorrectWMSDimName', () => {
    it('should return DIM_ passed in parameter unless time or elevation passed and always convert to upper case', () => {
      expect(getCorrectWMSDimName('time')).toEqual('TIME');
      expect(getCorrectWMSDimName('elevation')).toEqual('ELEVATION');
      expect(getCorrectWMSDimName('otherdimension')).toEqual(
        'DIM_OTHERDIMENSION',
      );
      expect(getCorrectWMSDimName('timee')).toEqual('DIM_TIMEE');
    });
  });
});
