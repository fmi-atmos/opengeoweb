/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

export default {
  name: null,
  leaf: false,
  path: [],
  keywords: [],
  styles: [],
  dimensions: [],
  queryable: false,
  crs: [],
  children: [
    {
      name: null,
      title: 'WMS of  RADAR',
      leaf: false,
      path: [''],
      keywords: [],
      styles: [
        {
          title: 'precip-gray/nearest',
          name: 'precip-gray/nearest',
          legendURL:
            'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-gray/nearest',
          abstract: 'No abstract available',
        },
        {
          title: 'precip-blue/nearest',
          name: 'precip-blue/nearest',
          legendURL:
            'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-blue/nearest',
          abstract: 'No abstract available',
        },
      ],
      dimensions: [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2021-05-14T07:35:00Z',
          values: '2021-03-31T09:25:00Z/2021-05-14T07:35:00Z/PT5M',
        },
      ],
      queryable: false,
      crs: [
        {
          name: 'EPSG:3411',
          bbox: {
            left: 2682754.74362,
            right: 3759536.917562,
            bottom: -3245034.014141,
            top: -2168251.989038,
          },
        },
        {
          name: 'EPSG:3412',
          bbox: {
            left: 0,
            right: 7413041.166015,
            bottom: 32318824.826266,
            top: 40075258.815074,
          },
        },
        {
          name: 'EPSG:3575',
          bbox: {
            left: -770622.801471,
            right: 56845.766135,
            bottom: -4485814.811314,
            top: -3684039.44362,
          },
        },
        {
          name: 'EPSG:3857',
          bbox: {
            left: 0,
            right: 1208534.698398,
            bottom: 6257115.219364,
            top: 7553161.958695,
          },
        },
        {
          name: 'EPSG:4258',
          bbox: { left: 48.895303, right: 55.9736, bottom: 0, top: 10.856452 },
        },
        {
          name: 'EPSG:4326',
          bbox: { left: 48.895303, right: 55.9736, bottom: 0, top: 10.856452 },
        },
        {
          name: 'CRS:84',
          bbox: { left: 48.895303, right: 55.9736, bottom: 0, top: 10.856452 },
        },
        {
          name: 'EPSG:25831',
          bbox: {
            left: 282182.345905,
            right: 997135.658653,
            bottom: 5433247.394267,
            top: 6207204.592736,
          },
        },
        {
          name: 'EPSG:25832',
          bbox: {
            left: -153083.019482,
            right: 617595.626092,
            bottom: 5415817.312927,
            top: 6239769.309937,
          },
        },
        {
          name: 'EPSG:28992',
          bbox: {
            left: -236275.338083,
            right: 501527.918656,
            bottom: 106727.731651,
            top: 900797.079725,
          },
        },
        {
          name: 'EPSG:7399',
          bbox: {
            left: 0,
            right: 763611.971696,
            bottom: 5757301.056717,
            top: 6483919.801602,
          },
        },
        {
          name: 'EPSG:50001',
          bbox: {
            left: -2000000,
            right: 10000000,
            bottom: -2000000,
            top: 8500000,
          },
        },
        {
          name: 'EPSG:54030',
          bbox: {
            left: 0,
            right: 853649.695106,
            bottom: 5211855.054125,
            top: 5936394.291427,
          },
        },
        {
          name: 'EPSG:32661',
          bbox: {
            left: 2000000,
            right: 2745713.040381,
            bottom: -2703305.597319,
            top: -1888346.21671,
          },
        },
        {
          name: 'EPSG:40000',
          bbox: {
            left: 0,
            right: 750214.326339,
            bottom: -4731695.771951,
            top: -3911817.119426,
          },
        },
        {
          name: 'EPSG:900913',
          bbox: {
            left: 0,
            right: 1208534.698398,
            bottom: 6257115.219364,
            top: 7553161.958695,
          },
        },
        {
          name: 'PROJ4:%2Bproj%3Dstere%20%2Blat_0%3D90%20%2Blon_0%3D0%20%2Blat_ts%3D60%20%2Ba%3D6378%2E14%20%2Bb%3D6356%2E75%20%2Bx_0%3D0%20y_0%3D0',
          bbox: {
            left: 0,
            right: 700.00242,
            bottom: -3649.999338,
            top: -4415.002986,
          },
        },
      ],
      children: [
        {
          name: 'RAD_NL25_PCP_CM-inherited',
          title: 'Precipitation Radar NL',
          leaf: true,
          path: ['', 'WMS of  RADAR'],
          keywords: [],
          styles: [
            {
              title: 'precip-gray/nearest',
              name: 'precip-gray/nearest',
              legendURL:
                'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-gray/nearest',
              abstract: 'No abstract available',
            },
            {
              title: 'precip-blue/nearest',
              name: 'precip-blue/nearest',
              legendURL:
                'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-blue/nearest',
              abstract: 'No abstract available',
            },
            {
              title: 'radar/nearest',
              name: 'radar/nearest',
              legendURL:
                'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=radar/nearest',
              abstract: 'No abstract available',
            },
            {
              title: 'precip-rainbow/nearest',
              name: 'precip-rainbow/nearest',
              legendURL:
                'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
              abstract: 'No abstract available',
            },
          ],
          dimensions: [
            {
              name: 'time',
              units: 'ISO8601',
              currentValue: '2021-05-14T07:35:00Z',
              values: '2021-03-31T09:25:00Z/2021-05-14T07:35:00Z/PT5M',
            },
          ],
          geographicBoundingBox: {
            east: '10.856452',
            west: '0.000000',
            north: '55.973600',
            south: '48.895303',
          },
          queryable: true,
          crs: [
            {
              name: 'EPSG:3411',
              bbox: {
                left: 2682754.74362,
                right: 3759536.917562,
                bottom: -3245034.014141,
                top: -2168251.989038,
              },
            },
            {
              name: 'EPSG:3412',
              bbox: {
                left: 0,
                right: 7413041.166015,
                bottom: 32318824.826266,
                top: 40075258.815074,
              },
            },
            {
              name: 'EPSG:3575',
              bbox: {
                left: -770622.801471,
                right: 56845.766135,
                bottom: -4485814.811314,
                top: -3684039.44362,
              },
            },
            {
              name: 'EPSG:3857',
              bbox: {
                left: 0,
                right: 1208534.698398,
                bottom: 6257115.219364,
                top: 7553161.958695,
              },
            },
            {
              name: 'EPSG:4258',
              bbox: {
                left: 48.895303,
                right: 55.9736,
                bottom: 0,
                top: 10.856452,
              },
            },
            {
              name: 'EPSG:4326',
              bbox: {
                left: 48.895303,
                right: 55.9736,
                bottom: 0,
                top: 10.856452,
              },
            },
            {
              name: 'CRS:84',
              bbox: {
                left: 48.895303,
                right: 55.9736,
                bottom: 0,
                top: 10.856452,
              },
            },
            {
              name: 'EPSG:25831',
              bbox: {
                left: 282182.345905,
                right: 997135.658653,
                bottom: 5433247.394267,
                top: 6207204.592736,
              },
            },
            {
              name: 'EPSG:25832',
              bbox: {
                left: -153083.019482,
                right: 617595.626092,
                bottom: 5415817.312927,
                top: 6239769.309937,
              },
            },
            {
              name: 'EPSG:28992',
              bbox: {
                left: -236275.338083,
                right: 501527.918656,
                bottom: 106727.731651,
                top: 900797.079725,
              },
            },
            {
              name: 'EPSG:7399',
              bbox: {
                left: 0,
                right: 763611.971696,
                bottom: 5757301.056717,
                top: 6483919.801602,
              },
            },
            {
              name: 'EPSG:50001',
              bbox: {
                left: -2000000,
                right: 10000000,
                bottom: -2000000,
                top: 8500000,
              },
            },
            {
              name: 'EPSG:54030',
              bbox: {
                left: 0,
                right: 853649.695106,
                bottom: 5211855.054125,
                top: 5936394.291427,
              },
            },
            {
              name: 'EPSG:32661',
              bbox: {
                left: 2000000,
                right: 2745713.040381,
                bottom: -2703305.597319,
                top: -1888346.21671,
              },
            },
            {
              name: 'EPSG:40000',
              bbox: {
                left: 0,
                right: 750214.326339,
                bottom: -4731695.771951,
                top: -3911817.119426,
              },
            },
            {
              name: 'EPSG:900913',
              bbox: {
                left: 0,
                right: 1208534.698398,
                bottom: 6257115.219364,
                top: 7553161.958695,
              },
            },
            {
              name: 'PROJ4:%2Bproj%3Dstere%20%2Blat_0%3D90%20%2Blon_0%3D0%20%2Blat_ts%3D60%20%2Ba%3D6378%2E14%20%2Bb%3D6356%2E75%20%2Bx_0%3D0%20y_0%3D0',
              bbox: {
                left: 0,
                right: 700.00242,
                bottom: -3649.999338,
                top: -4415.002986,
              },
            },
          ],
          children: [],
        },
      ],
    },
  ],
};
