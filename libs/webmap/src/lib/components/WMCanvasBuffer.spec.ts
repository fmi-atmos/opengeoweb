/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import WMCanvasBuffer, {
  makeQueryStringWithoutGeoInfo,
  getSortedListOfImages,
  CanvasGeoLayer,
} from './WMCanvasBuffer';
import { getCurrentImageTime } from './WMImage';
import WMJSMap from './WMJSMap';

describe('components/WMCanvasBuffer', () => {
  it('should create class with correct properties', () => {
    const wmsJSMap = new WMJSMap(document.createElement('div'));
    const canvasBuffer = new WMCanvasBuffer(wmsJSMap, 10, 10, {
      beforecanvasstartdraw: (): void => {},
      beforecanvasdisplay: (): void => {},
      canvasonerror: (): void => {},
    });

    expect(canvasBuffer).toBeDefined();
  });
  it('makeQueryStringWithoutGeoInfo should remove bbox, width and height from the url', () => {
    const urlToAdjust =
      'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&LAYERS=RAD_NL25_PCP_CM&WIDTH=946&HEIGHT=900&CRS=EPSG%3A3857&BBOX=-422843.6583413242,6074810.282795019,1456153.3856114622,7862439.605794499&STYLES=radar%2Fnearest&FORMAT=image/png&TRANSPARENT=TRUE&&time=2021-05-28T09%3A00%3A00Z';
    const expectedUrl =
      'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&LAYERS=RAD_NL25_PCP_CM&WIDTH=&HEIGHT=&CRS=EPSG%3A3857&BBOX=&STYLES=radar%2Fnearest&FORMAT=image%2Fpng&TRANSPARENT=TRUE&time=2021-05-28T09%3A00%3A00Z';
    expect(makeQueryStringWithoutGeoInfo(urlToAdjust)).toBe(expectedUrl);
  });
  it('addAlternativeImage and removeAlternativeImage', () => {
    const layerAV1 = 'http://t/w?name=A&BBOX=-10,-10,10,10&WIDTH=5&HEIGHT=5';
    const layerAV2 = 'http://t/w?name=A&BBOX=-12,-12,12,12&WIDTH=5&HEIGHT=5';
    const layerBV1 = 'http://t/w?name=B&BBOX=-12,-12,12,12&WIDTH=5&HEIGHT=5';
    const layerCV1 = 'http://t/w?name=C&BBOX=-12,-12,12,12&WIDTH=5&HEIGHT=5';

    const wmsJSMap = new WMJSMap(document.createElement('div'));
    const canvasBuffer = new WMCanvasBuffer(wmsJSMap, 10, 10, {
      beforecanvasstartdraw: (): void => {},
      beforecanvasdisplay: (): void => {},
      canvasonerror: (): void => {},
    });
    /* Add a first image and check shared images list */
    canvasBuffer.addAlternativeImage({
      imageSource: layerAV1,
      bbox: { left: -10, bottom: -10, right: 10, top: 10 },
      opacity: 1,
      imageAge: getCurrentImageTime(),
      width: 5,
      height: 5,
      areaFraction: 1,
      intersect: 1,
      areaFractionClosest: 1,
    });

    const sharedImagesList = canvasBuffer.getSharedImagesList();
    expect(Object.keys(sharedImagesList).length).toBe(1);

    const firstKey = Object.keys(sharedImagesList)[0];
    expect(sharedImagesList[firstKey].length).toBe(1);

    /* Add another image with the same layer and check shared images list */
    canvasBuffer.addAlternativeImage({
      imageSource: layerAV2,
      bbox: { left: -12, bottom: -12, right: 12, top: 12 },
      opacity: 1,
      imageAge: getCurrentImageTime(),
      width: 5,
      height: 5,
      areaFraction: 1,
      intersect: 1,
      areaFractionClosest: 1,
    });

    expect(Object.keys(sharedImagesList).length).toBe(1);
    expect(sharedImagesList[firstKey].length).toBe(2);

    /* Add another image with the different layer and check shared images list */
    canvasBuffer.addAlternativeImage({
      imageSource: layerBV1,
      bbox: { left: -12, bottom: -12, right: 12, top: 12 },
      opacity: 1,
      imageAge: getCurrentImageTime(),
      width: 5,
      height: 5,
      areaFraction: 1,
      intersect: 1,
      areaFractionClosest: 1,
    });

    expect(Object.keys(sharedImagesList).length).toBe(2);
    const secondKey = Object.keys(sharedImagesList)[1];
    expect(sharedImagesList[secondKey].length).toBe(1);

    /* Adding the same again should not do anything */
    canvasBuffer.addAlternativeImage({
      imageSource: layerBV1,
      bbox: { left: -12, bottom: -12, right: 12, top: 12 },
      opacity: 1,
      imageAge: getCurrentImageTime(),
      width: 5,
      height: 5,
      areaFraction: 1,
      intersect: 1,
      areaFractionClosest: 1,
    });

    expect(Object.keys(sharedImagesList).length).toBe(2);
    expect(sharedImagesList[firstKey].length).toBe(2);
    expect(sharedImagesList[secondKey].length).toBe(1);

    /* Now check if removal works OK */
    canvasBuffer.removeAlternativeImage(layerAV1);
    expect(Object.keys(sharedImagesList).length).toBe(2);
    expect(sharedImagesList[firstKey].length).toBe(1);

    /* Now check if removal again works OK */
    canvasBuffer.removeAlternativeImage(layerAV1);
    expect(Object.keys(sharedImagesList).length).toBe(2);
    expect(sharedImagesList[firstKey].length).toBe(1);

    /* Remove the other too, and see if the whole entry is removed from the map */
    canvasBuffer.removeAlternativeImage(layerAV2);
    expect(Object.keys(sharedImagesList).length).toBe(1);
    expect(sharedImagesList[firstKey]).toBeUndefined();

    /* Removing an image not in the sharedimages list should not give an error */
    canvasBuffer.removeAlternativeImage(layerCV1);
  });
  it('getAlternativeImage should return an alternative image', () => {
    const layerAV1 = 'http://t/w?name=A&BBOX=-10,-10,10,10&WIDTH=5&HEIGHT=5';

    const wmsJSMap = new WMJSMap(document.createElement('div'));
    const canvasBuffer = new WMCanvasBuffer(wmsJSMap, 10, 10, {
      beforecanvasstartdraw: (): void => {},
      beforecanvasdisplay: (): void => {},
      canvasonerror: (): void => {},
    });
    /* Add a first image and check shared images list */
    canvasBuffer.addAlternativeImage({
      imageSource: layerAV1,
      bbox: { left: -10, bottom: -10, right: 10, top: 10 },
      opacity: 1,
      imageAge: getCurrentImageTime(),
      width: 5,
      height: 5,
      areaFraction: 1,
      intersect: 1,
      areaFractionClosest: 1,
    });

    const cachedImage = wmsJSMap.getImageStore().getImage(layerAV1);

    cachedImage._isLoaded = true;
    cachedImage._hasError = false;
    cachedImage._isLoading = false;
    expect(cachedImage.isLoadedWithoutErrors()).toBeTruthy();

    const altImageList = canvasBuffer.getAlternativeImage(
      layerAV1,
      {
        left: -9,
        bottom: -9,
        right: 9,
        top: 9,
      },
      true,
    );

    expect(altImageList).toEqual([
      {
        bbox: {
          bottom: -10,
          left: -10,
          right: 10,
          top: 10,
        },
        height: 5,
        imageAge: expect.any(Number),
        imageSource: 'http://t/w?name=A&BBOX=-10,-10,10,10&WIDTH=5&HEIGHT=5',
        intersect: 0.81,
        opacity: 1,
        areaFraction: 1.2345679012345678,
        areaFractionClosest: 0.19999999999999996,
        width: 5,
      },
    ]);
  });
  it('should not return an alternative image, as quality is too low', () => {
    const layerAV1 = 'http://t/w?name=A&BBOX=-10,-10,10,10&WIDTH=5&HEIGHT=5';

    const wmsJSMap = new WMJSMap(document.createElement('div'));
    const canvasBuffer = new WMCanvasBuffer(wmsJSMap, 10, 10, {
      beforecanvasstartdraw: (): void => {},
      beforecanvasdisplay: (): void => {},
      canvasonerror: (): void => {},
    });
    /* Add a first image and check shared images list */
    canvasBuffer.addAlternativeImage({
      imageSource: layerAV1,
      bbox: { left: -10, bottom: -10, right: 10, top: 10 },
      opacity: 1,
      imageAge: getCurrentImageTime(),
      width: 5,
      height: 5,
      areaFraction: 1,
      intersect: 1,
      areaFractionClosest: 1,
    });

    const cachedImage = wmsJSMap.getImageStore().getImage(layerAV1);

    cachedImage._isLoaded = true;
    cachedImage._hasError = false;
    cachedImage._isLoading = false;
    expect(cachedImage.isLoadedWithoutErrors()).toBeTruthy();

    const altImageList = canvasBuffer.getAlternativeImage(
      layerAV1,
      {
        left: -90,
        bottom: -90,
        right: 90,
        top: 90,
      },
      true,
    );

    expect(altImageList).toEqual([]);
  });

  it('getSortedListOfImages should sort from high to low overlapping area', () => {
    const listToSort: CanvasGeoLayer[] = [
      {
        imageSource: 'nonoverlappingbox',
        bbox: { left: -12, bottom: 40, right: 0, top: 58 },
        opacity: 1,
        imageAge: 4,
        width: 5,
        height: 5,
        areaFraction: 1,
        intersect: 1,
        areaFractionClosest: 1,
      },
      {
        imageSource: 'onedegreeoverlapbox',
        bbox: { left: 0, bottom: 45, right: 1, top: 46 },
        opacity: 1,
        imageAge: 3,
        width: 5,
        height: 5,
        areaFraction: 1,
        intersect: 1,
        areaFractionClosest: 1,
      },
      {
        imageSource: 'twodegreeoverlapbox',
        bbox: { left: 0, bottom: 45, right: 2, top: 48 },
        opacity: 1,
        imageAge: 1,
        width: 5,
        height: 5,
        areaFraction: 1,
        intersect: 1,
        areaFractionClosest: 1,
      },
      {
        imageSource: 'fivedegreeoverlapbox',
        bbox: { left: 0, bottom: 45, right: 5, top: 50 },
        opacity: 1,
        imageAge: 1000,
        width: 5,
        height: 5,
        areaFraction: 1,
        intersect: 1,
        areaFractionClosest: 1,
      },
      {
        imageSource: 'nonoverlappingonedegreebox',
        bbox: { left: -12, bottom: 40, right: -11, top: 51 },
        opacity: 1,
        imageAge: 2323,
        width: 5,
        height: 5,
        areaFraction: 1,
        intersect: 1,
        areaFractionClosest: 1,
      },
      {
        imageSource: 'exactmatch',
        bbox: {
          left: -2,
          bottom: 40,
          right: 10,
          top: 58,
        },
        opacity: 1,
        imageAge: 101,
        width: 5,
        height: 5,
        areaFraction: 1,
        intersect: 1,
        areaFractionClosest: 1,
      },
      {
        imageSource: 'bigbox',
        bbox: { top: 0, left: 0, bottom: 100, right: 100 },
        opacity: 1,
        imageAge: 423424,
        width: 5,
        height: 5,
        areaFraction: 1,
        intersect: 1,
        areaFractionClosest: 1,
      },
    ];

    getSortedListOfImages(listToSort, {
      left: -2,
      bottom: 40,
      right: 10,
      top: 58,
    });

    expect(listToSort.map((item) => item.imageSource)).toEqual([
      'exactmatch',
      'nonoverlappingbox',
      'bigbox',
      'fivedegreeoverlapbox',
      'nonoverlappingonedegreebox',
      'twodegreeoverlapbox',
      'onedegreeoverlapbox',
    ]);

    // Resorting should return the same
    getSortedListOfImages(listToSort, {
      left: -2,
      bottom: 40,
      right: 10,
      top: 58,
    });

    expect(listToSort.map((item) => item.imageSource)).toEqual([
      'exactmatch',
      'nonoverlappingbox',
      'bigbox',
      'fivedegreeoverlapbox',
      'nonoverlappingonedegreebox',
      'twodegreeoverlapbox',
      'onedegreeoverlapbox',
    ]);

    expect(listToSort).toEqual([
      {
        imageSource: 'exactmatch',
        bbox: {
          left: -2,
          bottom: 40,
          right: 10,
          top: 58,
        },
        opacity: 1,
        imageAge: 101,
        width: 5,
        height: 5,
        areaFraction: 1,
        intersect: 1,
        areaFractionClosest: 0,
      },
      {
        imageSource: 'nonoverlappingbox',
        bbox: {
          left: -12,
          bottom: 40,
          right: 0,
          top: 58,
        },
        opacity: 1,
        imageAge: 4,
        width: 5,
        height: 5,
        areaFraction: 1,
        intersect: 0.16666666666666666,
        areaFractionClosest: 0,
      },
      {
        imageSource: 'bigbox',
        bbox: {
          top: 0,
          left: 0,
          bottom: 100,
          right: 100,
        },
        opacity: 1,
        imageAge: 423424,
        width: 5,
        height: 5,
        areaFraction: 46.2962962962963,
        intersect: 0,
        areaFractionClosest: 0.19999999999999996,
      },
      {
        imageSource: 'fivedegreeoverlapbox',
        bbox: {
          left: 0,
          bottom: 45,
          right: 5,
          top: 50,
        },
        opacity: 1,
        imageAge: 1000,
        width: 5,
        height: 5,
        areaFraction: 0.11574074074074074,
        intersect: 1,
        areaFractionClosest: 0.8842592592592593,
      },
      {
        imageSource: 'nonoverlappingonedegreebox',
        bbox: {
          left: -12,
          bottom: 40,
          right: -11,
          top: 51,
        },
        opacity: 1,
        imageAge: 2323,
        width: 5,
        height: 5,
        areaFraction: 0.05092592592592592,
        intersect: 0,
        areaFractionClosest: 0.9490740740740741,
      },
      {
        imageSource: 'twodegreeoverlapbox',
        bbox: {
          left: 0,
          bottom: 45,
          right: 2,
          top: 48,
        },
        opacity: 1,
        imageAge: 1,
        width: 5,
        height: 5,
        areaFraction: 0.027777777777777776,
        intersect: 1,
        areaFractionClosest: 0.9722222222222222,
      },
      {
        imageSource: 'onedegreeoverlapbox',
        bbox: {
          left: 0,
          bottom: 45,
          right: 1,
          top: 46,
        },
        opacity: 1,
        imageAge: 3,
        width: 5,
        height: 5,
        areaFraction: 0.004629629629629629,
        intersect: 1,
        areaFractionClosest: 0.9953703703703703,
      },
    ]);
  });
});
