/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { ParseISOTimeRangeDuration } from '.';
import {
  DateInterval,
  calculateTimeIntervalFromList,
  getNumberOfTimeSteps,
  getTimeValues,
  isTimeOnIntervalStep,
  isValidISOTime,
  parseISO8601DateToDate,
  parseISO8601IntervalToDateInterval,
  parseTimeValue,
} from './WMTime';

import { CustomDate } from './WMTimeTypes';

describe('components/WMTime', () => {
  describe('DateInterval', () => {
    it('should create class with default values', () => {
      const dateInterval = new DateInterval('0', '0', '0', '0', '0', '0');
      expect(dateInterval.year).toEqual(0);
      expect(dateInterval.month).toEqual(0);
      expect(dateInterval.day).toEqual(0);
      expect(dateInterval.hour).toEqual(0);
      expect(dateInterval.minute).toEqual(0);
      expect(dateInterval.second).toEqual(0);
      expect(dateInterval.isRegularInterval).toBeTruthy();
    });

    it('should return time', () => {
      expect(new DateInterval('0', '0', '1', '1', '1', '1').getTime()).toEqual(
        90061000,
      );
      expect(() =>
        new DateInterval('1', '0', '1', '1', '1', '1').getTime(),
      ).toThrow(new Error('year !== 0'));
      expect(() =>
        new DateInterval('0', '1', '1', '1', '1', '1').getTime(),
      ).toThrow(new Error('month !== 0'));
    });

    it('should return toISO8601', () => {
      expect(
        new DateInterval('0', '0', '0', '0', '0', '0').toISO8601(),
      ).toEqual('P');
      expect(
        new DateInterval('1', '0', '0', '0', '0', '0').toISO8601(),
      ).toEqual('P1Y');
      expect(
        new DateInterval('1', '1', '0', '0', '0', '0').toISO8601(),
      ).toEqual('P1Y1M');
      expect(
        new DateInterval('1', '1', '1', '0', '0', '0').toISO8601(),
      ).toEqual('P1Y1M1D');
      expect(
        new DateInterval('1', '1', '1', '1', '0', '0').toISO8601(),
      ).toEqual('P1Y1M1D1H');
      expect(
        new DateInterval('1', '1', '1', '1', '1', '0').toISO8601(),
      ).toEqual('P1Y1M1D1H1M');
      expect(
        new DateInterval('1', '1', '1', '1', '1', '1').toISO8601(),
      ).toEqual('P1Y1M1DT1H1M1S');
    });
  });

  describe('parseISO8601IntervalToDateInterval', () => {
    it('should parse isotime to date interval', () => {
      const isoTime = parseISO8601IntervalToDateInterval('P1Y1M1DT1H1M1S');
      expect(isoTime.year).toEqual(1);
      expect(isoTime.day).toEqual(1);
      expect(isoTime.month).toEqual(1);
      expect(isoTime.hour).toEqual(1);
      expect(isoTime.minute).toEqual(1);
      expect(isoTime.second).toEqual(1);
    });
    it('should parse isotime to date interval', () => {
      const isoTime = parseISO8601IntervalToDateInterval(
        'P10Y10M10DT10H10M10S',
      );
      expect(isoTime.year).toEqual(10);
      expect(isoTime.day).toEqual(10);
      expect(isoTime.month).toEqual(10);
      expect(isoTime.hour).toEqual(10);
      expect(isoTime.minute).toEqual(10);
      expect(isoTime.second).toEqual(10);
    });
    it('should return undefined if not valid', () => {
      expect(
        parseISO8601IntervalToDateInterval('10Y10M10DT10H10M10S'),
      ).toBeUndefined();
      expect(parseISO8601IntervalToDateInterval(null!)).toBeUndefined();
    });
  });

  describe('isTimeOnIntervalStep', () => {
    const time = new Date('2024-01-01T00:00:00Z');
    const start = new Date('2024-01-01T00:00:00Z');

    it('should return true for irregular intervals', () => {
      const interval = new DateInterval('1', '1', '0', '0', '0', '0');
      expect(isTimeOnIntervalStep(time, start, interval)).toBeTruthy();
    });

    it('should return true for zero interval', () => {
      const interval = new DateInterval('0', '0', '0', '0', '0', '0');
      expect(isTimeOnIntervalStep(time, start, interval)).toBeTruthy();
    });

    it('should correctly check hourly interval', () => {
      const hourly = new DateInterval('0', '0', '0', '1', '0', '0');
      expect(isTimeOnIntervalStep(time, start, hourly)).toBeTruthy();
      expect(
        isTimeOnIntervalStep(new Date('2024-01-01T05:00:00Z'), start, hourly),
      ).toBeTruthy();

      expect(
        isTimeOnIntervalStep(new Date('2024-01-01T01:30:00Z'), start, hourly),
      ).toBeFalsy();
      expect(
        isTimeOnIntervalStep(new Date('2024-01-01T00:58:20Z'), start, hourly),
      ).toBeFalsy();
    });

    it('should correctly check two day interval', () => {
      const everyOtherDay = new DateInterval('0', '0', '2', '0', '0', '0');
      expect(isTimeOnIntervalStep(time, start, everyOtherDay)).toBeTruthy();
      expect(
        isTimeOnIntervalStep(
          new Date('2024-01-03T00:00:00Z'),
          start,
          everyOtherDay,
        ),
      ).toBeTruthy();

      expect(
        isTimeOnIntervalStep(
          new Date('2024-01-02T00:00:00Z'),
          start,
          everyOtherDay,
        ),
      ).toBeFalsy();
      expect(
        isTimeOnIntervalStep(
          new Date('2024-01-03T01:00:00Z'),
          start,
          everyOtherDay,
        ),
      ).toBeFalsy();
    });
  });

  describe('ParseISOTimeRangeDuration', () => {
    it('should parse time duration', () => {
      const isoTimeDuration = '2021-03-23T00:00:00Z/2021-03-24T00:00:00Z/PT1H';
      const result = new ParseISOTimeRangeDuration(isoTimeDuration);

      expect(result.startTime).toEqual(
        parseISO8601DateToDate(isoTimeDuration.split('/')[0]),
      );
      expect(result.stopTime).toEqual(
        parseISO8601DateToDate(isoTimeDuration.split('/')[1]),
      );

      expect(result.timeSteps).toEqual(
        getNumberOfTimeSteps(
          result.startTime,
          result.stopTime,
          result.timeInterval,
        ),
      );
    });

    it('should return correct timesteps', () => {
      const isoTimeDuration = '2010-01-01T00:00:00Z/2010-01-01T01:00:00Z/PT1H';
      // [0] 2010-01-01T00:00
      // [1] 2010-01-01T01:00
      const result = new ParseISOTimeRangeDuration(isoTimeDuration);

      expect(result.getTimeSteps()).toEqual(result.timeSteps);

      // wanted time is before first timestep
      expect(() =>
        result.getTimeStepFromDate(
          new Date('2009-12-31T23:59:59Z') as CustomDate,
          true,
        ),
      ).toThrow(new Error('0'));

      // wanted time is equal to first timestep
      expect(
        result.getTimeStepFromDate(
          new Date('2010-01-01T00:00:00Z') as CustomDate,
          true,
        ),
      ).toEqual(0);

      // wanted time is in the middle of first and second timestep, it should round down to first timestep
      expect(
        result.getTimeStepFromDate(
          new Date('2010-01-01T00:30:00Z') as CustomDate,
          true,
        ),
      ).toEqual(0);

      // wanted time is closer to second timestep than first timestep, it should round up
      expect(
        result.getTimeStepFromDate(
          new Date('2010-01-01T00:30:01Z') as CustomDate,
          true,
        ),
      ).toEqual(1);

      // wanted time is equal to last timestep
      expect(
        result.getTimeStepFromDate(
          new Date('2010-01-01T01:00:00Z') as CustomDate,
          true,
        ),
      ).toEqual(1);

      // wanted time is equal to one timestep after last timestep
      expect(() =>
        result.getTimeStepFromDate(
          new Date('2010-01-01T02:00:00Z') as CustomDate,
          true,
        ),
      ).toThrow(new Error('1'));

      expect(result.getTimeStepFromDateWithinRange(result.startTime)).toEqual(
        0,
      );
      expect(result.getTimeStepFromDateWithinRange(result.stopTime)).toEqual(1);

      expect(result.getTimeStepFromDateClipped(result.startTime)).toEqual(0);
      expect(result.getTimeStepFromDateClipped(result.stopTime)).toEqual(1);
    });

    it('should return closest timestep for irregular interval', () => {
      // irregular interval: one month
      const isoTimeDuration = '2010-01-01T00:00:00Z/2010-02-01T00:00:00Z/P1M';
      // [0] 2010-01-01T00:00
      // [1] 2010-02-01T00:00
      const result = new ParseISOTimeRangeDuration(isoTimeDuration);
      expect(result.timeInterval.isRegularInterval).toBeFalsy();

      // wanted time is before first timestep
      expect(() =>
        result.getTimeStepFromDate(
          new Date('2009-12-31T23:59:59Z') as CustomDate,
          true,
        ),
      ).toThrow(new Error('0'));

      // wanted time is equal to first timestep
      expect(
        result.getTimeStepFromDate(
          new Date('2010-01-01T00:00:00Z') as CustomDate,
          true,
        ),
      ).toEqual(0);

      // wanted time is in the middle between first and second timestep. round down to first timestep
      expect(
        result.getTimeStepFromDate(
          new Date('2010-01-16T12:00:00Z') as CustomDate,
          true,
        ),
      ).toEqual(0);

      // wanted time is closer second timestep. round up to second timestep
      expect(
        result.getTimeStepFromDate(
          new Date('2010-01-16T12:00:01Z') as CustomDate,
          true,
        ),
      ).toEqual(1);

      // wanted time is equal to last timestep
      expect(
        result.getTimeStepFromDate(
          new Date('2010-02-01T00:00:00Z') as CustomDate,
          true,
        ),
      ).toEqual(1);

      // wanted time is equal to one timestep after last timestep
      expect(() =>
        result.getTimeStepFromDate(
          new Date('2010-03-01T00:00:00Z') as CustomDate,
          true,
        ),
      ).toThrow(new Error('1'));
    });

    it('should set default timeinterval to 1M if empty', () => {
      const isoTimeDuration = '2021-03-23T00:00:00Z/2021-03-24T00:00:00Z/';
      const result = new ParseISOTimeRangeDuration(isoTimeDuration);

      expect(result.getTimeSteps()).toEqual(1441);

      expect(() =>
        result.getTimeStepFromDate(
          new Date('2020-03-23T00:00:00Z') as CustomDate,
          true,
        ),
      ).toThrow(new Error('0'));

      expect(result.getTimeStepFromDateWithinRange(result.startTime)).toEqual(
        0,
      );
      expect(result.getTimeStepFromDateWithinRange(result.stopTime)).toEqual(
        1440,
      );

      expect(result.getTimeStepFromDateClipped(result.startTime)).toEqual(0);
      expect(result.getTimeStepFromDateClipped(result.stopTime)).toEqual(1440);
    });

    it('should set default timeinterval to 1M if not given', () => {
      const isoTimeDuration = '2021-03-23T00:00:00Z/2021-03-24T00:00:00Z';
      const result = new ParseISOTimeRangeDuration(isoTimeDuration);

      expect(result.getTimeSteps()).toEqual(1441);

      expect(() =>
        result.getTimeStepFromDate(
          new Date('2020-03-23T00:00:00Z') as CustomDate,
          true,
        ),
      ).toThrow(new Error('0'));

      expect(result.getTimeStepFromDateWithinRange(result.startTime)).toEqual(
        0,
      );
      expect(result.getTimeStepFromDateWithinRange(result.stopTime)).toEqual(
        1440,
      );

      expect(result.getTimeStepFromDateClipped(result.startTime)).toEqual(0);
      expect(result.getTimeStepFromDateClipped(result.stopTime)).toEqual(1440);
    });
  });

  describe('isValidISOTime', () => {
    it('should return true for a isotime with minimal required format', () => {
      expect(isValidISOTime('2021-04-18T22:12Z')).toBeTruthy();
      expect(isValidISOTime('2021-04-18T22:12:54Z')).toBeTruthy();
      expect(isValidISOTime('2021-04-18T22:12:54:120Z')).toBeTruthy();
    });
    it('should return false for a not valid isotime', () => {
      expect(isValidISOTime('')).toBeFalsy();
      expect(isValidISOTime('2021-04-18X22:12Z')).toBeFalsy();
      expect(isValidISOTime('2021-04-18')).toBeFalsy();
      expect(isValidISOTime(undefined!)).toBeFalsy();
      expect(isValidISOTime(null!)).toBeFalsy();
    });
  });

  describe('parseTimeValue', () => {
    it('parse time value', () => {
      expect(parseTimeValue('16')).toEqual(16);
      expect(parseTimeValue('')).toEqual(0);
      expect(parseTimeValue(undefined!)).toEqual(0);
      expect(parseTimeValue(null!)).toEqual(0);
    });
  });

  describe('getTimeValues', () => {
    it('return time values from isotime with seconds', () => {
      const [year, month, day, hours, minutes, seconds] = getTimeValues(
        '2021-04-18T22:12:54Z',
      );
      expect(year).toEqual(2021);
      expect(month).toEqual(4);
      expect(day).toEqual(18);
      expect(hours).toEqual(22);
      expect(minutes).toEqual(12);
      expect(seconds).toEqual(54);
    });

    it('return time values from isotime without seconds', () => {
      const [year, month, day, hours, minutes, seconds] =
        getTimeValues('2021-04-18T22:12Z');
      expect(year).toEqual(2021);
      expect(month).toEqual(4);
      expect(day).toEqual(18);
      expect(hours).toEqual(22);
      expect(minutes).toEqual(12);
      expect(seconds).toEqual(0);
    });
  });

  describe('parseISO8601DateToDate', () => {
    it('should return undefined if not valid', () => {
      expect(parseISO8601DateToDate(null!)).toBeUndefined();
    });
    it('should parse WMS format ccyy correctly', () => {
      expect(parseISO8601DateToDate('2021').toISO8601()).toEqual(
        '2021-01-01T00:00:00Z',
      );
    });
    it('should parse WMS format ccyy-mm correctly', () => {
      expect(parseISO8601DateToDate('2021-04').toISO8601()).toEqual(
        '2021-04-01T00:00:00Z',
      );
    });
    it('should parse WMS format ccyy-mm-dd correctly', () => {
      expect(parseISO8601DateToDate('2021-04-18').toISO8601()).toEqual(
        '2021-04-18T00:00:00Z',
      );
    });
    it('should parse WMS format ccyy-mm-ddThhZ correctly', () => {
      expect(parseISO8601DateToDate('2021-04-18T03Z').toISO8601()).toEqual(
        '2021-04-18T03:00:00Z',
      );
    });
    it('should not parse WMS format when it starts with a dash token', () => {
      expect(parseISO8601DateToDate('-2021-04-18T03Z')).toBeUndefined();
    });
    it('should return correct isotime', () => {
      const isotimeWithSeconds = '2021-04-18T22:12:54Z';
      const isotimeWithMilliSeconds = '2021-04-18T22:12:54:321Z';

      // can accept milliseconds but does not set them
      expect(
        parseISO8601DateToDate(isotimeWithMilliSeconds).toISO8601(),
      ).toEqual(isotimeWithSeconds);

      expect(parseISO8601DateToDate(isotimeWithSeconds).toISO8601()).toEqual(
        isotimeWithSeconds,
      );

      const isotimWithoutSeconds = '2021-04-18T22:12Z';
      expect(parseISO8601DateToDate(isotimWithoutSeconds).toISO8601()).toEqual(
        isotimWithoutSeconds.replace('Z', ':00Z'),
      );
    });
    it('should add the given dateinterval', () => {
      const isotime = '2021-04-18T22:12:54Z';
      const result = parseISO8601DateToDate(isotime);
      expect(result.toISO8601()).toEqual(isotime);
      result.add(null!);
      expect(result.toISO8601()).toEqual(isotime);
      result.add(new DateInterval('0', '0', '0', '0', '0', '0'));
      expect(result.toISO8601()).toEqual(isotime);
      result.add(new DateInterval('1', '1', '1', '1', '1', '1'));
      expect(result.toISO8601()).toEqual('2022-05-19T23:13:55Z');
    });
    it('should substract the given dateinterval', () => {
      const isotime = '2021-04-18T22:12:54Z';
      const result = parseISO8601DateToDate(isotime);
      expect(result.toISO8601()).toEqual(isotime);
      result.substract(null!);
      expect(result.toISO8601()).toEqual(isotime);
      result.substract(new DateInterval('0', '0', '0', '0', '0', '0'));
      expect(result.toISO8601()).toEqual(isotime);
      result.substract(new DateInterval('1', '1', '1', '1', '1', '1'));
      expect(result.toISO8601()).toEqual('2020-03-17T21:11:53Z');
    });
    it('should add the given dateinterval multiple times', () => {
      const isotime = '2021-04-18T22:12:54Z';
      const result = parseISO8601DateToDate(isotime);
      expect(result.toISO8601()).toEqual(isotime);
      result.addMultipleTimes(null!, 1);
      expect(result.toISO8601()).toEqual(isotime);
      result.addMultipleTimes(
        new DateInterval('0', '0', '0', '0', '0', '0'),
        2,
      );
      expect(result.toISO8601()).toEqual(isotime);
      result.addMultipleTimes(
        new DateInterval('1', '1', '1', '1', '1', '1'),
        2,
      );
      expect(result.toISO8601()).toEqual('2023-06-21T00:14:56Z');
      result.addMultipleTimes(
        new DateInterval('0', '0', '0', '0', '0', '5'),
        5,
      );
      expect(result.toISO8601()).toEqual('2023-06-21T00:15:21Z');
    });
    it('should substract the given dateinterval multiple times', () => {
      const isotime = '2021-04-18T22:12:54Z';
      const result = parseISO8601DateToDate(isotime);
      expect(result.toISO8601()).toEqual(isotime);
      result.substractMultipleTimes(null!, 1);
      expect(result.toISO8601()).toEqual(isotime);
      result.substractMultipleTimes(
        new DateInterval('0', '0', '0', '0', '0', '0'),
        2,
      );
      expect(result.toISO8601()).toEqual(isotime);
      result.substractMultipleTimes(
        new DateInterval('1', '1', '1', '1', '1', '1'),
        2,
      );
      expect(result.toISO8601()).toEqual('2019-02-16T20:10:52Z');
      result.substractMultipleTimes(
        new DateInterval('0', '0', '0', '0', '0', '5'),
        5,
      );
      expect(result.toISO8601()).toEqual('2019-02-16T20:10:27Z');
    });
  });

  describe('getNumberOfTimeSteps', () => {
    it('should return undefined if not valid', () => {
      const dummytime = new Date('2020-03-23T00:00:00Z') as CustomDate;
      const dummyinterval = new DateInterval('0', '0', '0', '0', '0', '0');
      expect(getNumberOfTimeSteps(null!, null!, null!)).toBeUndefined();
      expect(getNumberOfTimeSteps(dummytime, null!, null!)).toBeUndefined();
      expect(
        getNumberOfTimeSteps(dummytime, null!, dummyinterval),
      ).toBeUndefined();
    });
    it('should return unequally distributed time steps', () => {
      const starttime = parseISO8601DateToDate('2020-03-23T00:00:00Z');
      const endtime = parseISO8601DateToDate('2021-03-24T00:00:00Z');
      const interval = new DateInterval('0', '2', '0', '0', '0', '0');
      expect(getNumberOfTimeSteps(starttime, endtime, interval)).toEqual(8);
    });
    it('should return equally distributed time steps', () => {
      const starttime = parseISO8601DateToDate('2020-03-23T00:00:00Z');
      const endtime = parseISO8601DateToDate('2021-03-24T00:00:00Z');
      const interval = new DateInterval('0', '0', '1', '0', '0', '0');
      expect(getNumberOfTimeSteps(starttime, endtime, interval)).toEqual(367);
    });
  });

  describe('calculateTimeIntervalFromList', () => {
    it('calculates two months interval correctly for a list of dates', () => {
      const dateStrings = ['2020-01-01T00:00:00Z', '2020-03-01T00:00:00Z'];
      const interval = calculateTimeIntervalFromList(dateStrings);

      expect(interval.year).toBe(0);
      expect(interval.month).toBe(2);
      expect(interval.day).toBe(0);
      expect(interval.hour).toBe(0);
      expect(interval.minute).toBe(0);
      expect(interval.second).toBe(0);
    });
    it('calculates one hour interval correctly for a list of dates', () => {
      const dateStrings = ['2020-01-01T00:00:00Z', '2020-01-01T01:00:00Z'];
      const interval = calculateTimeIntervalFromList(dateStrings);

      expect(interval.year).toBe(0);
      expect(interval.month).toBe(0);
      expect(interval.day).toBe(0);
      expect(interval.hour).toBe(1);
      expect(interval.minute).toBe(0);
      expect(interval.second).toBe(0);
    });
    it('throws an error if less than two dates are provided', () => {
      const dateStrings = ['2020-01-01T00:00:00Z'];

      expect(() => {
        calculateTimeIntervalFromList(dateStrings);
      }).toThrow('Need at least two dates to calculate an interval.');
    });
  });
});
