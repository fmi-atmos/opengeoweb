/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { LayerType, WMBBOX, WMJSMap, WMLayer } from '.';
import { generateLayerId, setWMSGetCapabilitiesFetcher } from '../utils';
import { initWebmapTestI18n } from '../utils/i18n';
import { WMProj4Defs, WMSVersion } from './WMConstants';
import {
  getBBOXandProjString,
  buildWMSGetMapRequest,
  isProjectionSupported,
  getWMSRequests,
  getLayerIndex,
  buildMapLayerDims,
  getWMSGetFeatureInfoRequestURL,
  getGeoCoordFromPixelCoord,
  getPixelCoordFromLatLong,
  getGeoCoordFromLatLong,
  getLatLongFromPixelCoord,
  getPixelCoordFromGeoCoord,
  detectLeftButton,
  detectRightButton,
} from './WMJSMapHelpers';
import { mockGetCapabilities } from '../specs';
import { defaultReduxLayerRadarKNMI } from '../specs/layerSpecs';

beforeAll(() => {
  initWebmapTestI18n();
});

describe('components/WMJSMapHelpers', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  describe('getBBOXandProjString', () => {
    it('should return BBOX and ProjString from a map', () => {
      const mockBaseElement = document.createElement('div');
      mockBaseElement.addEventListener = jest.fn();
      mockBaseElement.appendChild = jest.fn();
      const map = new WMJSMap(mockBaseElement);
      map.setBBOX(1, 2, 3, 4);
      map.setProjection('EPSG:4326');
      const layer = new WMLayer();
      layer.version = WMSVersion.version111;
      layer.wms130bboxcompatibilitymode = false;
      expect(getBBOXandProjString(layer, map.getProjection())).toEqual(
        'SRS=EPSG%3A4326&BBOX=1,2,3,4&',
      );

      layer.version = WMSVersion.version130;
      expect(getBBOXandProjString(layer, map.getProjection())).toEqual(
        'CRS=EPSG%3A4326&BBOX=2,1,4,3&',
      );

      layer.wms130bboxcompatibilitymode = true;
      expect(getBBOXandProjString(layer, map.getProjection())).toEqual(
        'CRS=EPSG%3A4326&BBOX=1,2,3,4&',
      );
    });
  });

  describe('buildWMSGetMapRequest', () => {
    it('should return a GetMap url from buildWMSGetMapRequest for standard projection', () => {
      const mockBaseElement = document.createElement('div');
      mockBaseElement.addEventListener = jest.fn();
      mockBaseElement.appendChild = jest.fn();
      const map = new WMJSMap(mockBaseElement);
      map.setBBOX(1, 2, 3, 4);
      map.setProjection('EPSG:4326');
      const layer = new WMLayer();
      layer.version = WMSVersion.version111;
      layer.wms130bboxcompatibilitymode = false;
      layer.name = 'test';
      layer.getmapURL = 'http://testurl.test/';
      expect(buildWMSGetMapRequest(layer, map.getProjection())!.url).toEqual(
        'http://testurl.test/?&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&LAYERS=test&WIDTH=2&HEIGHT=2&SRS=EPSG%3A4326&BBOX=1,2,3,4&STYLES=&FORMAT=image/png&TRANSPARENT=TRUE&',
      );
    });

    it('should return a GetMap url from buildWMSGetMapRequest for Time elevation projection', () => {
      const mockBaseElement = document.createElement('div');
      mockBaseElement.addEventListener = jest.fn();
      mockBaseElement.appendChild = jest.fn();
      const map = new WMJSMap(mockBaseElement);
      map.setBBOX(1, 2, 3, 4);
      map.setProjection('GFI:TIME_ELEVATION');
      const layer = new WMLayer();
      layer.version = WMSVersion.version111;
      layer.wms130bboxcompatibilitymode = false;
      layer.name = 'test';
      layer.getmapURL = 'http://testurl.test/';
      expect(buildWMSGetMapRequest(layer, map.getProjection())!.url).toEqual(
        'http://testurl.test/&SERVICE=WMS&REQUEST=GetFeatureInfo&VERSION=1.1.1&LAYERS=test&QUERY_LAYERS=test&BBOX=29109.947643979103,6500000,1190890.052356021,7200000&SRS=GFI%3ATIME_ELEVATION&WIDTH=2&HEIGHT=2&X=707&Y=557&FORMAT=image/gif&INFO_FORMAT=image/png&STYLES=&TRANSPARENT=TRUE&time=1970-01-01T00:00:00Z/1970-01-01T00:00:00Z&elevation=2/4',
      );
    });
  });

  describe('isProjectionSupported', () => {
    it('should return true for a supported SRS', () => {
      expect(isProjectionSupported('EPSG:4326')).toBe(true);

      WMProj4Defs.forEach((proj4Definition) => {
        expect(isProjectionSupported(proj4Definition[0])).toBe(true);
      });
    });

    it('should return false for an unsupported SRS', () => {
      expect(isProjectionSupported('EPSG:9999')).toBe(false);
    });

    it('should handle custom projection definitions', () => {
      const customProj4Defs = [
        ['EPSG:9999', '+proj=longlat +datum=WGS84 +no_defs'],
      ];
      expect(isProjectionSupported('EPSG:9999', customProj4Defs)).toBe(true);

      expect(isProjectionSupported('EPSG:4326', [])).toBe(false);
    });
  });

  describe('getWMSRequests', () => {
    it('should return GetMap request for given layer', async () => {
      const webMapJS = new WMJSMap(document.createElement('div'));
      await webMapJS.addLayer(
        new WMLayer({
          service: 'http://testservice',
          name: 'testlayer',
          layerType: LayerType.mapLayer,
          id: generateLayerId(),
          dimensions: [{ name: 'testdim', currentValue: 'testvalA' }],
        }),
      );

      expect(getWMSRequests(webMapJS).map((d) => d.url)).toEqual([
        'http://testservice?&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&LAYERS=testlayer&WIDTH=2&HEIGHT=2&CRS=EPSG%3A4326&BBOX=-90,-180,90,180&STYLES=&FORMAT=image/png&TRANSPARENT=TRUE&DIM_TESTDIM=testvalA',
      ]);
    });
    it('should return GetMap request for given layer with specified dimension value', async () => {
      const webMapJS = new WMJSMap(document.createElement('div'));
      await webMapJS.addLayer(
        new WMLayer({
          service: 'http://testservice',
          name: 'testlayer',
          layerType: LayerType.mapLayer,
          id: generateLayerId(),
          dimensions: [{ name: 'testdim', currentValue: 'testvalA' }],
        }),
      );

      expect(
        getWMSRequests(webMapJS, [
          { name: 'testdim', currentValue: 'testvalB' },
        ]).map((wmsRequest) => wmsRequest.url),
      ).toEqual([
        'http://testservice?&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&LAYERS=testlayer&WIDTH=2&HEIGHT=2&CRS=EPSG%3A4326&BBOX=-90,-180,90,180&STYLES=&FORMAT=image/png&TRANSPARENT=TRUE&DIM_TESTDIM=testvalB',
      ]);
    });
  });

  describe('getLayerIndex', () => {
    it('should return correct layer index in list of layers', () => {
      const layerA = new WMLayer({
        service: 'http://testservice',
        name: 'testlayerA',
        layerType: LayerType.mapLayer,
        id: generateLayerId(),
      });
      const layerB = new WMLayer({
        service: 'http://testservice',
        name: 'testlayerB',
        layerType: LayerType.mapLayer,
        id: generateLayerId(),
      });
      const layerC = new WMLayer({
        service: 'http://testservice',
        name: 'testlayerC',
        layerType: LayerType.mapLayer,
        id: generateLayerId(),
      });

      const layers = [layerA, layerB, layerC];
      expect(getLayerIndex(layerA, layers)).toBe(0);
      expect(getLayerIndex(layerB, layers)).toBe(1);
      expect(getLayerIndex(layerC, layers)).toBe(2);
    });
  });

  describe('buildMapLayerDims', () => {
    it('should return correct dimensions', async () => {
      const layerA = new WMLayer({
        service: 'http://testservice',
        name: 'testlayer',
        layerType: LayerType.mapLayer,
        id: generateLayerId(),
        dimensions: [{ name: 'testdim', currentValue: 'testvalA' }],
      });
      const layerB = new WMLayer({
        service: 'http://testservice',
        name: 'testlayer',
        layerType: LayerType.mapLayer,
        id: generateLayerId(),
        dimensions: [{ name: 'testdim', currentValue: 'testvalB' }],
      });

      const webMapJS = new WMJSMap(document.createElement('div'));

      await webMapJS.addLayer(layerA);
      await webMapJS.addLayer(layerB);
      webMapJS.setDimension('testdim', 'testvalA');
      buildMapLayerDims(webMapJS);

      expect(layerA.getDimension('testdim')?.currentValue).toBe('testvalA');
      expect(layerB.getDimension('testdim')?.currentValue).toBe('testvalA');
    });
  });

  describe('getGeoCoordFromPixelCoord', () => {
    it('should return correct coordinates', () => {
      expect(
        getGeoCoordFromPixelCoord(
          { x: 123.4, y: 456.7 },
          { left: -10, bottom: -20, right: 60, top: 40 },
          678,
          987,
        ),
      ).toEqual({
        x: 2.7404129793510332,
        y: 12.237082066869299,
      });
    });
  });

  describe('getLatLongFromPixelCoord', () => {
    it('should return correct geo lat/lon for pixel coordinates', () => {
      const webMapJS = new WMJSMap(document.createElement('div'));

      webMapJS.setSize(100, 200);
      webMapJS.setBBOX({ left: 0, bottom: 40, top: 60, right: 10 });
      expect(getLatLongFromPixelCoord(webMapJS, { x: 100, y: 200 })).toEqual({
        x: 10,
        y: 40,
      });

      expect(getLatLongFromPixelCoord(webMapJS, { x: 50, y: 100 })).toEqual({
        x: 5,
        y: 50,
      });
    });

    it('should not crash if map is undefined but return the same values', () => {
      const webMapJS = null;
      expect(getLatLongFromPixelCoord(webMapJS!, { x: 5, y: 52 })).toEqual({
        x: 5,
        y: 52,
      });
    });
  });

  describe('getPixelCoordFromGeoCoord', () => {
    it('should return correct pixel coordinates for geo coordinates', () => {
      const webMapJS = new WMJSMap(document.createElement('div'));

      webMapJS.setSize(100, 200);
      webMapJS.setBBOX({ left: 0, bottom: 40, top: 60, right: 10 });
      expect(getPixelCoordFromGeoCoord(webMapJS, { x: 5, y: 50 })).toEqual({
        x: 50,
        y: 100,
      });
    });

    it('should not crash if map is undefined but return the same values', () => {
      const webMapJS = null;
      expect(getPixelCoordFromGeoCoord(webMapJS!, { x: 5, y: 52 })).toEqual({
        x: 5,
        y: 52,
      });
    });
  });

  it('should detect left and right button clicks', () => {
    const mockEventClickLeft = new MouseEvent('click', { button: 0 });
    const mockEventClickLeft2 = new MouseEvent('click', { buttons: 1 });
    const mockEventClickMiddle = new MouseEvent('click', { button: 1 });
    const mockEventClickMiddle2 = new MouseEvent('click', { buttons: 4 });
    const mockEventClickRight = new MouseEvent('click', { button: 2 });
    const mockEventClickRight2 = new MouseEvent('click', { buttons: 2 });

    expect(detectLeftButton(mockEventClickLeft)).toBeTruthy();
    expect(detectLeftButton(mockEventClickLeft2)).toBeTruthy();
    expect(detectLeftButton(mockEventClickMiddle)).toBeFalsy();
    expect(detectLeftButton(mockEventClickMiddle2)).toBeFalsy();
    expect(detectLeftButton(mockEventClickRight)).toBeFalsy();
    expect(detectLeftButton(mockEventClickRight2)).toBeFalsy();

    expect(detectRightButton(mockEventClickLeft)).toBeFalsy();
    expect(detectRightButton(mockEventClickLeft2)).toBeFalsy();
    expect(detectRightButton(mockEventClickMiddle)).toBeFalsy();
    expect(detectRightButton(mockEventClickMiddle2)).toBeFalsy();
    expect(detectRightButton(mockEventClickRight)).toBeTruthy();
    expect(detectRightButton(mockEventClickRight2)).toBeTruthy();
  });

  describe('getWMSGetFeatureInfoRequestURL', () => {
    it('should return correct getfeatureinfo url for layer', async () => {
      const layerA = new WMLayer({
        service: 'http://testservice',
        name: 'testlayer',
        layerType: LayerType.mapLayer,
        id: generateLayerId(),
        dimensions: [{ name: 'testdim', currentValue: 'testvalA' }],
      });
      const webMapJS = new WMJSMap(document.createElement('div'));
      await webMapJS.addLayer(layerA);
      webMapJS.setSize(234, 567);
      expect(
        getWMSGetFeatureInfoRequestURL(layerA, 123, 456, 'application/json'),
      ).toBe(
        'http://testservice?&SERVICE=WMS&REQUEST=GetFeatureInfo&VERSION=1.3.0&LAYERS=testlayer&QUERY_LAYERS=testlayer&CRS=EPSG%3A4326&BBOX=-90,-180,90,180&WIDTH=234&HEIGHT=567&I=123&J=456&FORMAT=image/gif&INFO_FORMAT=application/json&STYLES=&DIM_TESTDIM=testvalA',
      );
    });
    it('should return getfeature info request string for given layer', () => {
      const baseElement = document.createElement('div');
      const map = new WMJSMap(baseElement);
      const testLayer1 = new WMLayer({
        ...defaultReduxLayerRadarKNMI,
        id: 'wmjsmap-testlayer-1',
        enabled: true,
      });
      testLayer1.parentMap = map;
      const url = getWMSGetFeatureInfoRequestURL(testLayer1, 123, 456);
      expect(url).toBe(
        'https://testservice?&SERVICE=WMS&REQUEST=GetFeatureInfo&VERSION=1.3.0&LAYERS=RADNL_OPER_R___25PCPRR_L3_KNMI&QUERY_LAYERS=RADNL_OPER_R___25PCPRR_L3_KNMI&CRS=EPSG%3A4326&BBOX=-90,-180,90,180&WIDTH=2&HEIGHT=2&I=123&J=456&FORMAT=image/gif&INFO_FORMAT=text/html&STYLES=&TIME=2020-03-13T13%3A30%3A00Z',
      );
    });
    it('should return empty string if layer name is not defined', () => {
      const testLayer1 = new WMLayer({
        ...defaultReduxLayerRadarKNMI,
        id: 'wmjsmap-testlayer-1',
        enabled: true,
        name: undefined,
      });
      const url = getWMSGetFeatureInfoRequestURL(testLayer1, 123, 456);
      expect(url).toBe('');
    });

    it('should not destroy if the map was never intialized', () => {
      const map = new WMJSMap(null as unknown as HTMLElement);
      expect(map._initialized).toBeFalsy();
      const spyOnDetachEvents = jest.spyOn(map, '_detachEvents');
      map.destroy();
      expect(spyOnDetachEvents).not.toHaveBeenCalled();
    });

    it('should not destroy if the map is already destroyed', () => {
      const baseElement = document.createElement('div');
      const map = new WMJSMap(baseElement);
      expect(map._initialized).toBeTruthy();
      expect(map.isDestroyed).toBeFalsy();
      const spyOnDetachEvents = jest.spyOn(map, '_detachEvents');
      map.destroy();
      expect(map._initialized).toBeFalsy();
      expect(map.isDestroyed).toBeTruthy();
      expect(spyOnDetachEvents).toHaveBeenCalledTimes(1);
      map.destroy();
      expect(map._initialized).toBeFalsy();
      expect(map.isDestroyed).toBeTruthy();
      expect(spyOnDetachEvents).toHaveBeenCalledTimes(1);
    });
    it('should call layer.setAutoUpdate(false) when destroying the map', async () => {
      const baseElement = document.createElement('div');
      const map = new WMJSMap(baseElement);
      const testLayer1 = new WMLayer({
        ...defaultReduxLayerRadarKNMI,
        id: 'wmjsmap-testlayer-1',
        enabled: true,
      });
      await map.addLayer(testLayer1);
      const spySetAutoUpdateForLayer = jest.spyOn(testLayer1, 'setAutoUpdate');
      map.destroy();
      expect(spySetAutoUpdateForLayer).toHaveBeenCalledWith(false);
    });
  });

  describe('getPixelCoordFromLatLong', () => {
    it('should return correct pixel coordinates for lat/lon', () => {
      const webMapJS = new WMJSMap(document.createElement('div'));

      webMapJS.setSize(100, 200);
      webMapJS.setBBOX({ left: 0, bottom: 40, top: 60, right: 10 });
      expect(getPixelCoordFromLatLong(webMapJS, { x: 5, y: 52 })).toEqual({
        x: 50,
        y: 80,
      });

      expect(getPixelCoordFromLatLong(webMapJS, { x: 10, y: 40 })).toEqual({
        x: 100,
        y: 200,
      });
    });

    it('should not crash if map is undefined but return the same values', () => {
      const webMapJS = null;
      expect(getPixelCoordFromLatLong(webMapJS!, { x: 5, y: 52 })).toEqual({
        x: 5,
        y: 52,
      });
    });
    it('should return pixel coordinates for mercator', () => {
      const map = new WMJSMap(document.createElement('div'));
      map.setSize(512, 256);
      map.setProjection(
        'EPSG:3857',
        new WMBBOX({
          left: -1010048.1871574474,
          bottom: 6176666.999248354,
          right: 1967368.0267185564,
          top: 8020268.370069409,
        }),
      );
      const result = getPixelCoordFromLatLong(map, { x: 5, y: 52 });
      expect(Math.round(result.x)).toBe(267);
      expect(Math.round(result.y)).toBe(169);
      map.destroy();
    });
  });

  describe('getGeoCoordFromLatLong', () => {
    it('should return correct geo coordinates for lat/lon', () => {
      const webMapJS = new WMJSMap(document.createElement('div'));

      webMapJS.setSize(100, 200);
      webMapJS.setBBOX({ left: 0, bottom: 40, top: 60, right: 10 });
      expect(getGeoCoordFromLatLong(webMapJS, { x: 5, y: 52 })).toEqual({
        x: 5,
        y: 52,
      });

      expect(getGeoCoordFromLatLong(webMapJS, { x: 10, y: 40 })).toEqual({
        x: 10,
        y: 40,
      });
    });

    it('should not crash if map is undefined but return the same values', () => {
      const webMapJS = null;
      expect(getGeoCoordFromLatLong(webMapJS!, { x: 5, y: 52 })).toEqual({
        x: 5,
        y: 52,
      });
    });
    it('should return mercator coordinates for latlon', () => {
      const map = new WMJSMap(document.createElement('div'));
      map.setSize(512, 256);
      map.setProjection(
        'EPSG:3857',
        new WMBBOX({
          left: -1010048.1871574474,
          bottom: 6176666.999248354,
          right: 1967368.0267185564,
          top: 8020268.370069409,
        }),
      );
      const result = getGeoCoordFromLatLong(map, { x: 5, y: 52 });
      expect(Math.round(result.x)).toBe(556597);
      expect(Math.round(result.y)).toBe(6800125);
      map.destroy();
    });
  });
});
