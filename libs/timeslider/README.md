# timeslider

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test timeslider` to execute the unit tests via [Jest](https://jestjs.io).

### TypeScript Documentation

- [TypeScript Docs](https://opengeoweb.gitlab.io/opengeoweb/typescript-docs/timeslider/)
