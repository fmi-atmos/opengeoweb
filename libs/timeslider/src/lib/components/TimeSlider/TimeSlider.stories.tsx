/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { dateUtils, StorybookDocsWrapper } from '@opengeoweb/shared';

import type { Meta, StoryObj } from '@storybook/react';
import { TimeSliderLegend } from '../TimeSliderLegend/TimeSliderLegend';
import { TimeSlider } from './TimeSlider';
import { TimeSliderCurrentTimeBox } from '../TimeSliderCurrentTimeBox/TimeSliderCurrentTimeBox';
import { defaultSecondsPerPx } from './timeSliderUtils';
import { TimeSliderButtons } from '../TimeSliderButtons';

const meta: Meta<typeof TimeSlider> = {
  title: 'components/TimeSlider',
  component: TimeSlider,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeSlider',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof TimeSlider>;

const time = dateUtils.unix(new Date('2022-02-25T12:00:00Z'));
const defaultProps = {
  mapId: 'map_1',
  centerTime: time,
  secondsPerPx: defaultSecondsPerPx,
  timeSliderWidth: 100,
  selectedTime: time,
  mapIsActive: false,
  currentTime: 0,
  dataEndTime: 0,
  dataStartTime: 0,
  timeStep: 0,
  onSetCenterTime: (): void => {},
  onSetNewDate: (): void => {},
  isVisible: true,
  buttons: <TimeSliderButtons />,
};

const timesliderProps = {
  ...defaultProps,
  timeBox: (
    <TimeSliderCurrentTimeBox
      {...defaultProps}
      unfilteredSelectedTime={defaultProps.selectedTime}
      setUnfilteredSelectedTime={() => {}}
      onCalendarSelect={(): void => {}}
    />
  ),
  legend: (
    <TimeSliderLegend
      {...defaultProps}
      unfilteredSelectedTime={defaultProps.selectedTime}
      setUnfilteredSelectedTime={() => {}}
    />
  ),
};

export const Component: Story = {
  args: timesliderProps,
  tags: ['snapshot'],
  parameters: {
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac358e8408bddafecdb8/version/62c841de6abcde16dc11ee7b',
      },
    ],
  },
};

Component.storyName = 'Time Slider';

export const ComponentDark: Story = {
  args: timesliderProps,
  tags: ['snapshot', 'dark'],
  parameters: {
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/605b5a7151c9824390c1f83c/version/62c841f11756d41e69927d95',
      },
    ],
  },
  render: (props) => (
    <StorybookDocsWrapper isDark>
      <TimeSlider {...props} />
    </StorybookDocsWrapper>
  ),
};
ComponentDark.storyName = 'Time Slider dark';
