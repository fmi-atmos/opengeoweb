/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { BackwardForwardStepButton } from './BackwardForwardStepButton';
import { DemoWrapper } from '../../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

const propsForward = {
  isForwardStep: true,
  onClickBFButton: jest.fn(),
};

const propsBackward = {
  isForwardStep: false,
  onClickBFButton: jest.fn(),
};

describe('src/components/TimeSlider/TimeSliderButtons/BackwardForwardStepButton', () => {
  const user = userEvent.setup();

  it('should show and hide tooltip for forward button', async () => {
    render(
      <DemoWrapper>
        <BackwardForwardStepButton {...propsForward} />
      </DemoWrapper>,
    );
    const button = screen.getByTestId('step-forward-svg-path');
    expect(button).toBeInTheDocument();
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(
      screen.queryByText(translateKeyOutsideComponents('timeslider-forward')),
    ).not.toBeInTheDocument();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('timeslider-forward')),
    ).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(
      screen.queryByText(translateKeyOutsideComponents('timeslider-forward')),
    ).not.toBeInTheDocument();
  });

  it('should show and hide tooltip for backward button', async () => {
    render(
      <DemoWrapper>
        <BackwardForwardStepButton {...propsBackward} />
      </DemoWrapper>,
    );
    const button = screen.getByTestId('step-backward-svg-path');
    expect(button).toBeInTheDocument();
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(
      screen.queryByText(translateKeyOutsideComponents('timeslider-backward')),
    ).not.toBeInTheDocument();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('timeslider-backward')),
    ).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(
      screen.queryByText(translateKeyOutsideComponents('timeslider-backward')),
    ).not.toBeInTheDocument();
  });
});
