/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { range } from 'lodash';

import { Mark, TimeSliderMenu, TimeSliderMenuProps } from './TimeSliderMenu';
import { DemoWrapper } from '../../../Providers/Providers';

describe('src/components/TimeSlider/TimeSliderButtons/TimeSliderMenu', () => {
  const marks: Mark[] = range(1, 10).map((value) => ({
    text: `t${value}`,
    value,
    label: `label${value}`,
  }));
  const title = 'Test Menu';
  const props: TimeSliderMenuProps = {
    icon: null!,
    marks,
    title,
    value: 4,
    isAutoSelected: true,
    isAutoDisabled: false,
    handleAutoClick: jest.fn(),
    onChangeSliderValue: jest.fn(),
    setOpen: jest.fn(),
    open: false,
  };
  const user = userEvent.setup();

  it('should display a slider button', async () => {
    render(
      <DemoWrapper>
        <TimeSliderMenu {...props} />
      </DemoWrapper>,
    );

    // Should render button with text
    const button = screen.getByRole('button', { name: title });
    expect(button).toHaveTextContent('label4');

    // Should show and hide tooltip
    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(screen.queryByText(title)).not.toBeInTheDocument();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();
    expect(screen.getByText(title)).toBeInTheDocument();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'));
    expect(screen.queryByText(title)).not.toBeInTheDocument();
  });

  it('should display a menu', async () => {
    render(
      <DemoWrapper>
        <TimeSliderMenu {...props} open={true} />
      </DemoWrapper>,
    );
    // Should toggle a menu
    expect(screen.getByRole('menu')).toBeInTheDocument();

    expect(screen.getAllByRole('menuitem')).toHaveLength(11);

    expect(screen.getByRole('menuitem', { name: /auto/i })).toHaveClass(
      'Mui-selected',
    );

    // Menu items should be enabled
    expect(
      screen.getByRole('menuitem', { name: /label2/i }),
    ).not.toHaveAttribute('aria-disabled');

    // onMenuItemClick is called correctly
    await user.click(screen.getByRole('menuitem', { name: /label6/i }));
    expect(props.onChangeSliderValue).toHaveBeenCalledTimes(1);
    expect(props.onChangeSliderValue).toHaveBeenCalledWith(6);

    await user.click(screen.getByRole('menuitem', { name: /label7/i }));
    expect(props.onChangeSliderValue).toHaveBeenCalledTimes(2);
    expect(props.onChangeSliderValue).toHaveBeenLastCalledWith(7);

    expect(screen.getByRole('menuitem', { name: /auto/i })).toHaveClass(
      'Mui-selected',
    );

    await user.click(screen.getByRole('menuitem', { name: /auto/i }));
    expect(props.handleAutoClick).toHaveBeenCalledTimes(1);
  });

  it('should render a menu as disabled and no auto option if passed in as props', async () => {
    const props2: TimeSliderMenuProps = {
      ...props,
      handleAutoClick: undefined,
      allOptionsDisabled: true,
      open: true,
    };
    render(
      <DemoWrapper>
        <TimeSliderMenu {...props2} />
      </DemoWrapper>,
    );

    // No auto button
    expect(
      screen.queryByRole('menuitem', { name: /auto/i }),
    ).not.toBeInTheDocument();

    // Menu items are disabled
    expect(screen.getByRole('menu')).toBeInTheDocument();
    expect(screen.getByRole('menuitem', { name: /label7/i })).toHaveAttribute(
      'aria-disabled',
    );
  });

  it('should render a menu with auto disabled', async () => {
    const props2: TimeSliderMenuProps = {
      ...props,
      isAutoDisabled: true,
      open: true,
    };
    render(
      <DemoWrapper>
        <TimeSliderMenu {...props2} />
      </DemoWrapper>,
    );

    // Auto is disabled when there is no timestep from the layer
    expect(screen.getByRole('menu')).toBeInTheDocument();
    expect(screen.getByRole('menuitem', { name: /auto/i })).toBeInTheDocument();
    expect(screen.getByRole('menuitem', { name: /auto/i })).toHaveAttribute(
      'aria-disabled',
    );
  });

  it('should call onChangeMouseWheel on wheel scroll', async () => {
    jest.useFakeTimers();

    render(
      <DemoWrapper>
        <TimeSliderMenu {...props} />
      </DemoWrapper>,
    );

    const button = screen.getByRole('button', { name: title });

    fireEvent.wheel(button, { deltaY: 1 });
    expect(props.onChangeSliderValue).toHaveBeenCalledTimes(1);
    expect(props.onChangeSliderValue).toHaveBeenCalledWith(5);

    // wait for throttle time
    jest.runOnlyPendingTimers();

    fireEvent.wheel(button, { deltaY: -1 });
    expect(props.onChangeSliderValue).toHaveBeenCalledTimes(2);
    expect(props.onChangeSliderValue).toHaveBeenCalledWith(3);

    jest.useRealTimers();
  });

  it('should render a button at the bottom of the menu', async () => {
    const props2: TimeSliderMenuProps = {
      ...props,
      displayCurrentLength: true,
      open: true,
    };
    render(
      <DemoWrapper>
        <TimeSliderMenu {...props2} />
      </DemoWrapper>,
    );

    expect(
      screen.getByRole('menuitem', { name: /4m Edit/i }),
    ).toBeInTheDocument();
    const testButton = screen.getByTestId('editAnimationOption');
    expect(testButton).toBeInTheDocument();
  });

  it('should not call callback when clicking option that is already selected', async () => {
    const newProps = { ...props, isAutoSelected: false, value: 1, open: true };

    render(
      <DemoWrapper>
        <TimeSliderMenu {...newProps} />
      </DemoWrapper>,
    );

    expect(screen.getByRole('menu')).toBeInTheDocument();

    expect(screen.getByRole('menuitem', { name: /label1/i })).toHaveClass(
      'Mui-selected',
    );

    // onMenuItemClick is not called  when clicking already selected item
    await user.click(screen.getByRole('menuitem', { name: /label1/i }));
    expect(props.onChangeSliderValue).not.toHaveBeenCalled();

    // Clicking another should call onMenuItemClick
    await user.click(screen.getByRole('menuitem', { name: /label7/i }));
    expect(props.onChangeSliderValue).toHaveBeenCalledTimes(1);
    expect(props.onChangeSliderValue).toHaveBeenLastCalledWith(7);
  });
});
