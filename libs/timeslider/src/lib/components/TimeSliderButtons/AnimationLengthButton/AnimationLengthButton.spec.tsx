/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { AnimationLengthButton } from './AnimationLengthButton';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { DemoWrapper } from '../../../Providers/Providers';
import { AnimationLength } from '../../TimeSlider/timeSliderUtils';

describe('src/components/TimeSlider/TimeSliderButtons/AnimationLengthButton', () => {
  const user = userEvent.setup();
  it('should render a button correct name and icon', async () => {
    const props = {
      onChangeAnimationLength: jest.fn(),
      animationLength: AnimationLength.Hours24,
    };

    render(
      <DemoWrapper>
        <AnimationLengthButton {...props} />
      </DemoWrapper>,
    );
    const button = screen.getByRole('button', {
      name: translateKeyOutsideComponents('timeslider-animation'),
    });
    expect(button).toBeTruthy();
    expect(button).toContainHTML('AnimationSvgPath');
  });

  it('should show and hide tooltip', async () => {
    const props = {
      onChangeAnimationLength: jest.fn(),
      animationLength: AnimationLength.Hours24,
    };

    render(
      <DemoWrapper>
        <AnimationLengthButton {...props} />
      </DemoWrapper>,
    );
    const button = screen.getByRole('button', {
      name: translateKeyOutsideComponents('timeslider-animation'),
    });
    expect(button).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(
      screen.queryByText(translateKeyOutsideComponents('timeslider-animation')),
    ).toBeFalsy();

    await user.hover(button);
    await waitFor(
      async () => expect(await screen.findByRole('tooltip')).toBeTruthy(),
      { timeout: 3000 },
    );
    expect(
      screen.getByText(translateKeyOutsideComponents('timeslider-animation')),
    ).toBeTruthy();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'), {
      timeout: 3000,
    });
    expect(
      screen.queryByText(translateKeyOutsideComponents('timeslider-animation')),
    ).toBeFalsy();
  });

  it('menu can be opened and closed and selection is called correctly', async () => {
    const props = {
      onChangeAnimationLength: jest.fn(),
      animationLength: AnimationLength.Hours24,
    };

    render(
      <DemoWrapper>
        <AnimationLengthButton {...props} />
      </DemoWrapper>,
    );
    await user.click(
      screen.getByRole('button', {
        name: /animation/i,
      }),
    );
    await user.click(
      screen.getByRole('menuitem', {
        name: /6 h/i,
      }),
    );

    expect(props.onChangeAnimationLength).toHaveBeenCalledTimes(1);
    expect(props.onChangeAnimationLength).toHaveBeenCalledWith(
      AnimationLength.Hours6,
    );
  });

  it('can change value on mouse scroll weel', async () => {
    const props = {
      onChangeAnimationLength: jest.fn(),
      animationLength: AnimationLength.Hours24,
    };
    jest.useFakeTimers();

    render(
      <DemoWrapper>
        <AnimationLengthButton {...props} />
      </DemoWrapper>,
    );

    const button = screen.getByRole('button', {
      name: translateKeyOutsideComponents('timeslider-animation'),
    });

    fireEvent.wheel(button, { deltaY: 1 });
    expect(props.onChangeAnimationLength).toHaveBeenCalledTimes(1);
    expect(props.onChangeAnimationLength).toHaveBeenCalledWith(
      AnimationLength.Hours12,
    );

    // wait for throttle time
    jest.runOnlyPendingTimers();

    fireEvent.wheel(button, { deltaY: -1 });
    expect(props.onChangeAnimationLength).toHaveBeenCalledTimes(2);
    expect(props.onChangeAnimationLength).toHaveBeenCalledWith(
      AnimationLength.Hours24,
    );

    jest.useRealTimers();
  });
});
