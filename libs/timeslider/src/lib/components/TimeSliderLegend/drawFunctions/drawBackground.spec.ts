/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { dateUtils } from '@opengeoweb/shared';
import { getColorChangeTimesteps } from './drawBackground';
import { Scale } from '../../TimeSlider/timeSliderUtils';

describe('src/components/TimeSlider/drawFunctions/drawBackground', () => {
  describe('getColorChangeTimesteps', () => {
    it('should work for week', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const colorChangeTimesteps = getColorChangeTimesteps(
        Scale.Week,
        dateUtils.unix(time),
        dateUtils.unix(dateUtils.add(time, { weeks: 1 })),
      );
      const startWeek = dateUtils.utc('2022-11-14T00:00:00Z');

      expect(colorChangeTimesteps).toEqual([
        dateUtils.unix(dateUtils.add(startWeek, { days: 1 })),
        dateUtils.unix(dateUtils.add(startWeek, { days: 2 })),
        dateUtils.unix(dateUtils.add(startWeek, { days: 3 })),
        dateUtils.unix(dateUtils.add(startWeek, { days: 4 })),
        dateUtils.unix(dateUtils.add(startWeek, { days: 5 })),
        dateUtils.unix(dateUtils.add(startWeek, { days: 6 })),
        dateUtils.unix(dateUtils.add(startWeek, { days: 7 })),
        dateUtils.unix(dateUtils.add(startWeek, { days: 8 })),
        dateUtils.unix(dateUtils.add(startWeek, { days: 9 })),
      ]);
    });
    it('should work for one hour', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const colorChangeTimesteps = getColorChangeTimesteps(
        Scale.Hour,
        dateUtils.unix(time),
        dateUtils.unix(time),
      );
      expect(colorChangeTimesteps).toEqual([
        dateUtils.unix(dateUtils.sub(time, { hours: 12 })),
        dateUtils.unix(dateUtils.add(time, { hours: 12 })),
      ]);
    });
    it('should work for hours', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const colorChangeTimesteps = getColorChangeTimesteps(
        Scale.Hour,
        dateUtils.unix(time),
        dateUtils.unix(dateUtils.add(time, { days: 1 })),
      );
      expect(colorChangeTimesteps).toEqual([
        dateUtils.unix(dateUtils.sub(time, { hours: 12 })),
        dateUtils.unix(dateUtils.add(time, { hours: 12 })),
        dateUtils.unix(dateUtils.add(time, { hours: 36 })),
      ]);
    });
  });
});
