/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { dateUtils } from '@opengeoweb/shared';
import {
  getCustomTimesteps,
  getRoundedStartAndEnd,
  roundByUnitOfTime,
} from './drawTimeScale';
import { Scale } from '../../TimeSlider/timeSliderUtils';

describe('src/components/TimeSlider/TimeSliderLegendRenderFunctions', () => {
  describe('getRoundedStartAndEnd', () => {
    it('should round the times up/down to nearest 5 minutes', () => {
      const startTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 12:27:27'),
      );
      const endTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 12:51:27'),
      );
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Minutes5,
      );
      expect(roundedStart).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-05 12:25:00')),
      );
      expect(roundedEnd).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-05 12:55:00')),
      );
    });

    it('should round the times up/down to nearest 3 hours', () => {
      const startTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 13:27:27'),
      );
      const endTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 14:27:27'),
      );
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Hours3,
      );
      expect(roundedStart).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-05 12:00:00')),
      );
      expect(roundedEnd).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-05 15:00:00')),
      );
    });

    it('should round the times up/down to nearest 6 hours', () => {
      const startTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 17:27:27'),
      );
      const endTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 20:27:27'),
      );
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Hours6,
      );
      expect(roundedStart).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-05 12:00:00')),
      );
      expect(roundedEnd).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-06 00:00:00')),
      );
    });

    it('should not round if the times are already correct multiples', () => {
      const startTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 12:00:00'),
      );
      const endTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 15:00:00'),
      );
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Hours3,
      );
      expect(roundedStart).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-05 12:00:00')),
      );
      expect(roundedEnd).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-05 15:00:00')),
      );
    });

    it('should round the to up/down by one hour', () => {
      const startTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 12:15:00'),
      );
      const endTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 12:45:00'),
      );
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Hour,
      );
      expect(roundedStart).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-05 12:00:00')),
      );
      expect(roundedEnd).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-05 13:00:00')),
      );
    });

    it('should round up/down by one day', () => {
      const startTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 12:00:00'),
      );
      const endTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 13:00:00'),
      );
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Day,
      );
      expect(roundedStart).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-05 00:00:00')),
      );
      expect(roundedEnd).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-06 00:00:00')),
      );
    });

    it('should round up/down by one week', () => {
      const startTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 12:00:00'),
      );
      const endTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-08 12:00:00'),
      );
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Week,
      );
      expect(roundedStart).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-03 00:00:00')),
      );
      expect(roundedEnd).toEqual(
        dateUtils.unix(dateUtils.utc('2021-05-10 00:00:00')),
      );
    });

    it('should round up/down by one year', () => {
      const startTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-05-05 12:00:00'),
      );
      const endTimeVisible = dateUtils.unix(
        dateUtils.utc('2021-10-05 12:00:00'),
      );
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Year,
      );
      expect(roundedStart).toEqual(
        dateUtils.unix(dateUtils.utc('2021-01-01 00:00:00')),
      );
      expect(roundedEnd).toEqual(
        dateUtils.unix(dateUtils.utc('2022-01-01 00:00:00')),
      );
    });
  });
  describe('roundByUnitOfTime', () => {
    it('should round hour down', () => {
      const time = dateUtils.utc('2022-11-15T23:59:00Z');
      const startOfHour = dateUtils.utc('2022-11-15T23:00:00Z');
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'hour', false);
      expect(rounded).toEqual(dateUtils.unix(startOfHour));
    });
    it('should round hour up', () => {
      const time = dateUtils.utc('2022-11-15T23:59:00Z');
      const startOfHour = dateUtils.utc('2022-11-16T00:00:00Z');
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'hour', true);
      expect(rounded).toEqual(dateUtils.unix(startOfHour));
    });
    it('should round day down', () => {
      const time = dateUtils.utc('2022-11-15T23:59:00Z');
      const startOfDay = dateUtils.utc('2022-11-15T00:00:00Z');
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'day', false);
      expect(rounded).toEqual(dateUtils.unix(startOfDay));
    });
    it('should round day up', () => {
      const time = dateUtils.utc('2022-11-15T23:59:00Z');
      const startOfDay = dateUtils.utc('2022-11-16T00:00:00Z');
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'day', true);
      expect(rounded).toEqual(dateUtils.unix(startOfDay));
    });
    it('should round week down', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const startOfWeek = dateUtils.utc('2022-11-14T00:00:00Z');
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'week', false);
      expect(rounded).toEqual(dateUtils.unix(startOfWeek));
    });
    it('should round week up', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const startOfWeek = dateUtils.utc('2022-11-14T00:00:00Z');
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'week', true);

      expect(rounded).toEqual(
        dateUtils.unix(dateUtils.add(startOfWeek, { weeks: 1 })),
      );
    });

    it('should round month down', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const startOfMonth = dateUtils.utc('2022-11-01T00:00:00Z');
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'month', false);
      expect(rounded).toEqual(dateUtils.unix(startOfMonth));
    });
    it('should round month up', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const startOfMonth = dateUtils.utc('2022-11-01T00:00:00Z');
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'month', true);
      expect(rounded).toEqual(
        dateUtils.unix(dateUtils.add(startOfMonth, { months: 1 })),
      );
    });
    it('should round year down', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const startOfYear = dateUtils.utc('2022-01-01T00:00:00Z');
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'year', false);
      expect(rounded).toEqual(dateUtils.unix(startOfYear));
    });
    it('should round year up', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const startOfYear = dateUtils.utc('2022-01-01T00:00:00Z');
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'year', true);
      expect(rounded).toEqual(
        dateUtils.unix(dateUtils.add(startOfYear, { years: 1 })),
      );
    });
    it('should round day down', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const startOfDay = dateUtils.startOfDayUTC(time);
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'day', false);

      expect(rounded).toEqual(dateUtils.unix(startOfDay));
    });
    it('should round day up', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const startOfDay = dateUtils.startOfDayUTC(time);
      const rounded = roundByUnitOfTime(dateUtils.unix(time), 'day', true);

      expect(rounded).toEqual(
        dateUtils.unix(dateUtils.add(startOfDay, { days: 1 })),
      );
    });
  });
  describe('getCustomTimesteps', () => {
    it('should work for week', () => {
      const time = dateUtils.utc('2022-11-15T12:00:00Z');
      const timesteps = getCustomTimesteps(
        dateUtils.unix(time),
        dateUtils.unix(dateUtils.add(time, { weeks: 2 })),
        'weeks',
      );

      expect(timesteps).toEqual([
        dateUtils.unix(time),
        dateUtils.unix(dateUtils.add(time, { weeks: 1 })),
        dateUtils.unix(dateUtils.add(time, { weeks: 2 })),
      ]);
    });
    it('should work for day', () => {
      const time = dateUtils.utc('2022-11-15T06:30:00Z');
      const timesteps = getCustomTimesteps(
        dateUtils.unix(time),
        dateUtils.unix(dateUtils.add(time, { days: 1 })),
        'days',
      );

      expect(timesteps).toEqual([
        dateUtils.unix(time),
        dateUtils.unix(dateUtils.add(time, { days: 1 })),
      ]);
    });
  });
});
