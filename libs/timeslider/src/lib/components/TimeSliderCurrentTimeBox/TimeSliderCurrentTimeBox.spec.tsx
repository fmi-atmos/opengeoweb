/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen } from '@testing-library/react';

import { dateUtils } from '@opengeoweb/shared';
import i18n from 'i18next';
import {
  getTimeBoxWidth,
  TimeSliderCurrentTimeBox,
  TimeSliderCurrentTimeBoxProps,
} from './TimeSliderCurrentTimeBox';
import { DemoWrapper } from '../../Providers/Providers';
import { secondsInYear } from './TimeSliderCurrentTimeBoxRenderFunctions';
import { timeBoxGeom } from '../TimeSlider/timeSliderUtils';

describe('src/components/TimeSlider/TimeSliderCurrentTime', () => {
  const date = dateUtils.utc('01-01-2020');
  const props: TimeSliderCurrentTimeBoxProps = {
    secondsPerPx: 50,
    centerTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
    unfilteredSelectedTime: dateUtils.unix(dateUtils.set(date, { hours: 6 })),
    setUnfilteredSelectedTime: jest.fn(),
    onCalendarSelect: jest.fn(),
  };
  it('should render the canvas', () => {
    render(
      <DemoWrapper>
        <TimeSliderCurrentTimeBox {...props} />
      </DemoWrapper>,
    );
    expect(screen.getByRole('presentation')).toBeTruthy();
  });

  it('should contain the calendar button', async () => {
    render(
      <DemoWrapper>
        <TimeSliderCurrentTimeBox {...props} />
      </DemoWrapper>,
    );
    expect(
      screen.getByRole('button', {
        name: i18n.t('timeslider-marker-placement'),
      }),
    ).toBeTruthy();
  });

  it('should move the slider when CurrentTimeBox is out of view', async () => {
    const date = dateUtils.utc('01-01-2020');
    const onSetCenterTimeFn = jest.fn();
    const props: TimeSliderCurrentTimeBoxProps = {
      secondsPerPx: 50,
      centerTime: dateUtils.unix(dateUtils.set(date, { hours: 12 })),
      unfilteredSelectedTime: dateUtils.unix(
        dateUtils.set(date, { hours: 12 }),
      ),
      setUnfilteredSelectedTime: jest.fn(),
      onSetCenterTime: onSetCenterTimeFn,
      onCalendarSelect: jest.fn(),
    };
    const { rerender } = render(
      <DemoWrapper>
        <TimeSliderCurrentTimeBox {...props} />
      </DemoWrapper>,
    );

    // make canvasComponent return a width of 1000
    const canvasContainerDiv = screen.getByRole('button', { name: 'canvas' })
      .firstChild as HTMLElement;
    jest.spyOn(canvasContainerDiv, 'clientWidth', 'get').mockReturnValue(1000);
    fireEvent(window, new Event('resize'));

    expect(onSetCenterTimeFn).not.toHaveBeenCalled();
    const changedProps: TimeSliderCurrentTimeBoxProps = {
      ...props,
      unfilteredSelectedTime: dateUtils.unix(dateUtils.sub(date, { weeks: 1 })),
    };
    rerender(
      <DemoWrapper>
        <TimeSliderCurrentTimeBox {...changedProps} />
      </DemoWrapper>,
    );
    expect(onSetCenterTimeFn).toHaveBeenCalled();
  });

  it('should calculate correct time box width', () => {
    const { smallWidth, largeWidth, iconWidth, calendarWidth } = timeBoxGeom;
    expect(getTimeBoxWidth(secondsInYear + 1000)).toBe(
      largeWidth + iconWidth + calendarWidth,
    );
    expect(getTimeBoxWidth(10 * 60)).toBe(
      smallWidth + iconWidth + calendarWidth,
    );
  });
});
