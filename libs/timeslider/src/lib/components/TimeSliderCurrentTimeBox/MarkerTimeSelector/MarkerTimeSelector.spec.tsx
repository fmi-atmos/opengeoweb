/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import i18n from 'i18next';
import userEvent from '@testing-library/user-event';
import { DemoWrapper } from '../../../Providers/Providers';
import {
  disabledButtonStyle,
  enabledButUnSelectedButtonStyle,
  MarkerTimeSelector,
  MarkerTimeSelectorProps,
} from './MarkerTimeSelector';

describe('src/lib/components/TimeSliderCurrentTimeBox/MarkerTimeSelector', () => {
  const date = '2020-11-06T00:00:00.000Z';
  const utcDate = dateUtils.utc(date);
  const startDate = '2020-11-05T00:00:00.000Z';
  const utcStartDate = dateUtils.utc(startDate);

  const endDate = '2020-11-07T00:00:00.000Z';
  const utcEndDate = dateUtils.utc(endDate);

  const props: MarkerTimeSelectorProps = {
    value: dateUtils.unix(utcDate),
    format: dateUtils.DATE_FORMAT_DATEPICKER,
    dataStartTime: dateUtils.unix(utcStartDate),
    dataEndTime: dateUtils.unix(utcEndDate),
    onSelect: jest.fn(),
    onCancel: jest.fn(),
  };

  it('should open a marker time selector with proper contents', async () => {
    render(
      <DemoWrapper>
        <MarkerTimeSelector value={dateUtils.unix(utcDate)} />
      </DemoWrapper>,
    );

    expect(screen.getByText('UTC')).toBeTruthy();
    expect(screen.getByText(i18n.t('timeslider-marker'))).toBeTruthy();
    expect(
      screen.getByLabelText(i18n.t('timeslider-marker-label')),
    ).toBeTruthy();

    const markerTimeSelector = await screen.findByTestId('markerTimeSelector');
    expect(markerTimeSelector).toBeTruthy();
  });

  it('should show a (small-sized) openPickerButton', () => {
    render(
      <DemoWrapper>
        <MarkerTimeSelector {...props} />
      </DemoWrapper>,
    );

    expect(
      screen.getByRole('button').classList.contains('MuiIconButton-sizeSmall'),
    ).toBeTruthy();
  });

  it('should show given date value as a formatted UTC date on text field', async () => {
    render(
      <DemoWrapper>
        <MarkerTimeSelector {...props} />
      </DemoWrapper>,
    );

    const dateBox = screen.queryByRole('textbox');
    expect(dateBox).toBeTruthy();

    const propsUTCDate = dateUtils.fromUnix(props.value!);

    // Set new date value differing from currently selected "now" time
    fireEvent.change(dateBox!, { target: { value: propsUTCDate } });

    const attrUTCDate = dateBox!.getAttribute('value');
    const formattedPropsUTCDate = dateUtils.dateToString(
      propsUTCDate,
      dateUtils.DATE_FORMAT_DATEPICKER,
    );

    expect(attrUTCDate).toEqual(formattedPropsUTCDate);
  });

  it('should call props.onCancel() when "Escape" is hit on text field', async () => {
    render(
      <DemoWrapper>
        <MarkerTimeSelector {...props} />
      </DemoWrapper>,
    );

    const dateBox = screen.queryByRole('textbox');
    expect(dateBox).toBeTruthy();

    const user = userEvent.setup();

    await user.keyboard('{Escape}');
    await waitFor(() => expect(props.onCancel).toHaveBeenCalled());
  });

  it('should call props.onSelect() when "Enter" is hit on text field', async () => {
    render(
      <DemoWrapper>
        <MarkerTimeSelector {...props} />
      </DemoWrapper>,
    );

    const dateBox = screen.queryByRole('textbox');
    expect(dateBox).toBeTruthy();

    const user = userEvent.setup();

    await user.keyboard('{Enter}');
    await waitFor(() => expect(props.onSelect).toHaveBeenCalled());
  });

  it('should open up a calendar view showing some commonly known things about the days in November', async () => {
    render(
      <DemoWrapper>
        <MarkerTimeSelector {...props} />
      </DemoWrapper>,
    );

    // Open the calendar view for November
    const openPickerButton = screen.queryByRole('button');
    expect(openPickerButton).toBeTruthy();
    expect(openPickerButton).toHaveClass('MuiIconButton-sizeSmall');
    const user = userEvent.setup();
    await user.click(openPickerButton!);

    // A calendar view for November should be seen
    expect(screen.getByLabelText(/november/i)).toBeInTheDocument();
    expect(screen.getByTitle('Previous month')).toBeTruthy();
    expect(screen.getByTitle('Next month')).toBeTruthy();

    // Check out some basic conditions for days in calendar view
    const daysInNovember = await screen.findAllByRole('gridcell');

    expect(
      daysInNovember.filter((day) => day.textContent!.match(/\0{1}/)),
    ).toHaveLength(0); // No "zero day" can ever exist
    expect(
      daysInNovember.filter((day) => day.textContent!.match(/\d/)),
    ).toHaveLength(30); // Number of days in November
    expect(
      daysInNovember.filter((day) => day.textContent!.match(/31/)),
    ).toHaveLength(0); // November 31 cannot exist
    expect(
      daysInNovember.filter(
        (day) => day.getAttribute('aria-selected') === 'true',
      ),
    ).toHaveLength(1); // Only a single one day should be selected at a time
    expect(
      daysInNovember.filter(
        (day) => day.getAttribute('aria-selected') === 'false',
      ),
    ).toHaveLength(29); // All other days should not be selected
  });

  it('should open up a calendar view showing one selected (current) day and all the unselected days in November', async () => {
    render(
      <DemoWrapper>
        <MarkerTimeSelector {...props} />
      </DemoWrapper>,
    );

    // Open the calendar view for November
    const openPickerButton = screen.queryByRole('button');
    expect(openPickerButton).toBeTruthy();
    expect(openPickerButton).toHaveClass('MuiIconButton-sizeSmall');
    const user = userEvent.setup();
    await user.click(openPickerButton!);

    // Check out the selection state of certain days shown in calendar view

    // Last day before available data range should not be selectable
    const day4 = screen.getByText('4');
    expect(day4).toBeTruthy();
    expect(day4).toHaveAttribute('aria-selected', 'false');

    // First available day should not be selectable
    const day5 = screen.getByText('5');
    expect(day5).toBeTruthy();
    expect(day5).toHaveAttribute('aria-selected', 'false');

    // Current day (default) should be selectable
    const day6 = screen.getByText('6');
    expect(day6).toBeTruthy();
    expect(day6).toHaveAttribute('aria-selected', 'true');

    // Last available day should not be selectable
    const day7 = screen.getByText('7');
    expect(day7).toBeTruthy();
    expect(day7).toHaveAttribute('aria-selected', 'false');

    // First day after available data range should not be selectable
    const day8 = screen.getByText('8');
    expect(day8).toBeTruthy();
    expect(day8).toHaveAttribute('aria-selected', 'false');
  });

  it('should open a monthly calendar view showing data availability/unavailability for each day in November', async () => {
    render(
      <DemoWrapper>
        <MarkerTimeSelector {...props} />
      </DemoWrapper>,
    );

    // Open the calendar view for November
    const openPickerButton = screen.queryByRole('button');
    expect(openPickerButton).toBeTruthy();
    expect(openPickerButton).toHaveClass('MuiIconButton-sizeSmall');
    const user = userEvent.setup();
    await user.click(openPickerButton!);

    // A calendar view for November should be seen
    expect(screen.getByLabelText(/november/i)).toBeInTheDocument();
    expect(screen.getByTitle('Previous month')).toBeTruthy();
    expect(screen.getByTitle('Next month')).toBeTruthy();

    // Check out the data availability for certain days shown in calendar view

    // Last day before available data range should have a "disabled" button style
    const day4 = screen.getByText('4');
    expect(day4).toBeTruthy();
    expect(day4).toHaveStyle({ ...disabledButtonStyle });

    // First available day should have an "enabled" button style
    const day5 = screen.getByText('5');
    expect(day5).toBeTruthy();
    expect(day5).toHaveStyle({ ...enabledButUnSelectedButtonStyle });

    // Current day (default) should be selectable
    const day6 = screen.getByText('6');
    expect(day6).toBeTruthy();
    expect(day6).toHaveAttribute('aria-selected', 'true');

    // Last available day should have an "enabled" button style
    const day7 = screen.getByText('7');
    expect(day7).toBeTruthy();
    expect(day7).toHaveStyle({ ...enabledButUnSelectedButtonStyle });

    // First day after available data range should have a "disabled button style"
    const day8 = screen.getByText('8');
    expect(day8).toBeTruthy();
    expect(day8).toHaveStyle({ ...disabledButtonStyle });
  });
});
