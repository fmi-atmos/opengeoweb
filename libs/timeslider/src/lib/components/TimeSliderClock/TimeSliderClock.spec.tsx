/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import { TimeSliderClock } from './TimeSliderClock';
import { DemoWrapper } from '../../Providers/Providers';

describe('components/TimeSlider/TimeSliderClock/TimeSliderClock', () => {
  const time = dateUtils.unix(dateUtils.utc('2020-03-13T13:30:00Z'));

  it('renders correctly', () => {
    render(
      <DemoWrapper>
        <TimeSliderClock time={time} />
      </DemoWrapper>,
    );
    screen.getByText('Fri 13 Mar 13:30 UTC');

    expect(
      screen.queryByRole('button', { name: /animation options/i }),
    ).not.toBeInTheDocument();
  });

  it('shows animation options', () => {
    render(
      <DemoWrapper>
        <TimeSliderClock time={time} hideButton={false} />
      </DemoWrapper>,
    );

    expect(
      screen.queryByRole('button', { name: /time span/i }),
    ).not.toBeInTheDocument();

    fireEvent.click(screen.getByRole('button', { name: /animation options/i }));

    expect(
      screen.getByRole('button', { name: /time span/i }),
    ).toBeInTheDocument();
  });
});
