/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

// wrap around webmap i18n exports so geoweb app
// doesn't have to depend on webmap directly
import { WEBMAP_NAMESPACE, webmapTranslations } from '@opengeoweb/webmap';
import webmapReactTranslations from '../locales/webmap-react.json';
import {
  OpenLayersLayerProps,
  OpenLayersLayer,
} from './lib/components/OpenLayers/layers/OpenLayersLayer';
import { DefaultBaseLayers } from './lib/components/OpenLayers/layers/DefaultBaseLayers';
import {
  OpenLayersFeatureLayer,
  WMSLayer,
} from './lib/components/OpenLayers/layers';
import TimeContext from './lib/components/OpenLayers/context/TimeContext';
import { TimeContextType } from './lib/components/OpenLayers/types/Timespan';
import { TimeawareImageSource } from './lib/components/OpenLayers/utils/TimeawareImageSource';

export * from './lib';
export { webmapReactTranslations };
export { WEBMAP_NAMESPACE, webmapTranslations };

export {
  TimeContext,
  TimeawareImageSource,
  OpenLayersFeatureLayer,
  WMSLayer,
  DefaultBaseLayers,
  OpenLayersLayer,
};
export type { TimeContextType, OpenLayersLayerProps };

// eslint-disable-next-line @typescript-eslint/naming-convention
let _useOpenLayers = true;

export const setOpenLayersEnabled = (openLayersOnOrOff: boolean): void => {
  _useOpenLayers = openLayersOnOrOff;
};

export const isOpenLayersEnabled = (): boolean => {
  return _useOpenLayers;
};
