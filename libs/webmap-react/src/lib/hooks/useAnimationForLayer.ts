/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Dimension } from '@opengeoweb/webmap';
import React, { useRef } from 'react';
import { OpenLayersLayerProps } from '../components/OpenLayers/layers';
import { useGetWMLayerInstance } from './useHooksGetCapabilities';
import { useSetIntervalWhenVisible } from './useSetInterValWhenVisible';

export const useAnimationForLayer = (
  layerProps: OpenLayersLayerProps,
  intervalInMs: number,
  numStepsTillLatest = 48,
  checkForUpdatesMs = 60000,
): Dimension[] => {
  const animationStep = useRef<number>(0);
  const [timeValue, setTimeValue] = React.useState<string>('');

  const wmLayer = useGetWMLayerInstance(
    layerProps.service!,
    layerProps.name!,
    layerProps.id,
  );

  const trigger = React.useCallback((): void => {
    animationStep.current += 1;
    if (animationStep.current >= numStepsTillLatest) {
      animationStep.current = 0;
    }
    if (wmLayer) {
      const timeDim = wmLayer.getDimension('time');
      if (timeDim) {
        const numTimeSteps = timeDim.size();
        const timeString = timeDim.getValueForIndex(
          animationStep.current + numTimeSteps - numStepsTillLatest,
        ) as string;
        setTimeValue(timeString);
      }
    }
  }, [numStepsTillLatest, wmLayer]);

  useSetIntervalWhenVisible(trigger, intervalInMs);

  const refetchLayer = React.useCallback(async () => {
    await wmLayer?.parseLayer(true);
  }, [wmLayer]);

  useSetIntervalWhenVisible(refetchLayer, checkForUpdatesMs);

  return [{ name: 'time', currentValue: timeValue }];
};
