/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { View } from 'ol';
import { useRef, useEffect } from 'react';
import { OpenLayersLayerProps } from '../components/OpenLayers/layers';
import { useGetWMLayerInstance } from './useHooksGetCapabilities';

export const useViewFromLayer = (
  layerProps: OpenLayersLayerProps,
  baseView = {
    projection: 'EPSG:3857',
    zoom: 1,
    center: [0, 0],
  },
): View => {
  const viewRef = useRef(new View(baseView));

  const wmLayer = useGetWMLayerInstance(
    layerProps.service!,
    layerProps.name!,
    layerProps.id,
  );

  useEffect(() => {
    if (!wmLayer) {
      return;
    }
    const layerProjectionForCode = wmLayer.getProjection(
      viewRef.current.getProjection().getCode(),
    );
    if (!layerProjectionForCode) {
      return;
    }
    viewRef.current.fit(layerProjectionForCode.bbox.getExtent());
  }, [wmLayer]);
  return viewRef.current;
};
