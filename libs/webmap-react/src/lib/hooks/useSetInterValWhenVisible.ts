/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useEffect, useRef, useLayoutEffect, useCallback } from 'react';

export const useSetIntervalWhenVisible = (
  autoUpdate: () => void,
  refetchInterval: number,
  shouldAutoFetch = true,
): void => {
  const intervalIdRef = useRef<NodeJS.Timeout>();
  const lastRefetched = useRef(new Date().getTime());

  const refetch = useCallback((): void => {
    if (document.visibilityState === 'visible') {
      lastRefetched.current = new Date().getTime();
      if (shouldAutoFetch) {
        autoUpdate();
      }
    }
  }, [autoUpdate, shouldAutoFetch]);

  const refetchIfTimeHasPassed = useCallback(() => {
    if (
      document.visibilityState === 'visible' &&
      new Date().getTime() - lastRefetched.current > refetchInterval
    ) {
      refetch();
    }
  }, [refetch, refetchInterval]);

  // Triggers when document becomes visible
  useLayoutEffect(() => {
    const onVisibilityChange = (): void => {
      refetchIfTimeHasPassed();
    };
    document.addEventListener('visibilitychange', onVisibilityChange);
    return (): void => {
      document.removeEventListener('visibilitychange', onVisibilityChange);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [refetchInterval, shouldAutoFetch]);

  // Sets a timer
  useEffect(() => {
    intervalIdRef.current = setInterval(refetch, refetchInterval);
    refetchIfTimeHasPassed();
    return (): void => {
      clearInterval(intervalIdRef.current);
    };
  }, [refetch, refetchIfTimeHasPassed, refetchInterval, shouldAutoFetch]);
};
