/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import * as React from 'react';
import { LayerType, WMLayer, webmapUtils } from '@opengeoweb/webmap';
import MapDimensionSelect from './MapDimensionSelect';
import { WebmapReactThemeProvider } from '../Providers/Providers';

describe('components/MapDimensionSelect/MapDimensionSelect', () => {
  const layerId = 'layerId';

  const multiDimensionLayer = {
    service: 'https://testservice',
    id: layerId,
    name: 'air_temperature__at_pl',
    title: 'air_temperature__at_pl',
    layerType: LayerType.mapLayer,
    enabled: true,
    dimensions: [
      {
        name: 'elevation',
        units: 'hPa',
        currentValue: '850',
        values: '850,925',
        synced: true,
        validSyncSelection: true,
      },
      {
        name: 'ensemble_member',
        units: 'member',
        currentValue: '7',
        values:
          '0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29',
        synced: true,
        validSyncSelection: true,
      },
      {
        name: 'modellevel',
        units: '',
        currentValue: '',
        values: '_0_,_1_,_2_',
        synced: true,
        validSyncSelection: false,
      },
    ],
  };
  const index = 0;
  const props = {
    layerId,
    dimensionName: multiDimensionLayer.dimensions[index].name,
    layerDimension: multiDimensionLayer.dimensions[index],
    handleDimensionValueChanged: jest.fn(),
    handleSyncChanged: jest.fn(),
  };

  const wmMultiDimensionLayer = new WMLayer(multiDimensionLayer);
  webmapUtils.registerWMLayer(wmMultiDimensionLayer, multiDimensionLayer.id);

  describe('MapDimensionSelect', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    test('renders with provided props', () => {
      render(
        <WebmapReactThemeProvider>
          <MapDimensionSelect {...props} />
        </WebmapReactThemeProvider>,
      );
      expect(screen.getByTestId('syncButton')).toBeTruthy();
      expect(screen.getByTestId('syncIcon')).toBeTruthy();
    });

    test('calls handleSyncChanged when sync button is clicked', () => {
      render(<MapDimensionSelect {...props} />);
      fireEvent.click(screen.getByTestId('syncButton'));
      expect(props.handleSyncChanged).toHaveBeenCalledWith(
        layerId,
        props.dimensionName,
        props.layerDimension.currentValue,
        false,
      );
    });
  });
});
