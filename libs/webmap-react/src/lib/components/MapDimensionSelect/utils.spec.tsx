/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { WMJSDimension } from '@opengeoweb/webmap';
import { marksByDimension } from './utils';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';

beforeAll(() => {});

describe('libs/core/src/lib/utils/DimensionUtils', () => {
  describe('marksByDimension', () => {
    it('should handle elevation values', () => {
      const config = {
        name: 'elevation',
        values: '850,925',
        currentValue: '850',
        units: 'NWP_VERT_CS:1001',
      };
      const dimension = new WMJSDimension(config);
      const hPa = translateKeyOutsideComponents('webmap-react-hpa');
      expect(marksByDimension(i18n.t, dimension)).toEqual([
        {
          label: `850 ${hPa}`,
          value: 850,
        },
        {
          label: `925 ${hPa}`,
          value: 925,
        },
      ]);
    });
    it('should handle number values', () => {
      const config = {
        name: 'some number dimension',
        values: '1,2,3',
        currentValue: '2',
        units: 'someUnit',
        unitSymbol: 'test',
      };
      const dimension = new WMJSDimension(config);
      expect(marksByDimension(i18n.t, dimension)).toEqual([
        {
          label: '1 test',
          value: 1,
        },
        {
          label: '2 test',
          value: 2,
        },
        {
          label: '3 test',
          value: 3,
        },
      ]);
    });
    it('should handle string values', () => {
      const config = {
        name: 'some string dimension',
        values: '_000_,_001_,_002_',
        currentValue: '_000_',
        units: '-',
        unitSymbol: undefined,
      };
      const dimension = new WMJSDimension(config);
      expect(marksByDimension(i18n.t, dimension)).toEqual([
        {
          label: '_000_ ',
          value: '_000_',
        },
        {
          label: '_001_ ',
          value: '_001_',
        },
        {
          label: '_002_ ',
          value: '_002_',
        },
      ]);
    });
    it('should handle an empty dimension', () => {
      expect(marksByDimension(i18n.t, null!)).toEqual([]);
      expect(marksByDimension(i18n.t, {} as WMJSDimension)).toEqual([]);
      expect(
        marksByDimension(i18n.t, { name: 'some dim name' } as WMJSDimension),
      ).toEqual([]);
    });
  });
});
