/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { I18nextProvider } from 'react-i18next';
import {
  lightTheme,
  ThemeWrapper,
  ThemeProviderProps,
} from '@opengeoweb/theme';
import { i18n, initWebmapReactTestI18n } from '../../utils/i18n';

export const WebmapReactThemeProvider: React.FC<ThemeProviderProps> = ({
  children,
  theme = lightTheme,
}: ThemeProviderProps) => (
  <WebmapReactI18nProvider>
    <ThemeWrapper theme={theme}>{children}</ThemeWrapper>
  </WebmapReactI18nProvider>
);

interface WebmapReactTranslationWrapperProps {
  children?: React.ReactNode;
}

const WebmapReactI18nProvider: React.FC<WebmapReactTranslationWrapperProps> = ({
  children,
}) => {
  initWebmapReactTestI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};
