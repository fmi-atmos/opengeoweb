/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';

import { webmapUtils } from '@opengeoweb/webmap';
import type { Meta, StoryObj } from '@storybook/react';
import {
  MapView as WebMapMapView,
  MapViewLayer as WebMapMapViewLayer,
} from '../MapView';
import { publicLayers, defaultLayers } from '../../layers';

const meta: Meta<typeof WebMapMapView> = {
  title: 'components/WebMapMapView',
  component: WebMapMapView,
};
export default meta;

type Story = StoryObj<typeof WebMapMapView>;

export const HoldShiftToZoom: Story = {
  args: {
    mapId: webmapUtils.generateMapId(),
    holdShiftToScroll: true,
    showScaleBar: false,
  },
  render: (props) => (
    <div style={{ height: '100vh' }}>
      <WebMapMapView {...props}>
        <WebMapMapViewLayer {...publicLayers.baseLayer} />
        <WebMapMapViewLayer
          {...publicLayers.radarLayer}
          onLayerReady={(layer): void => {
            layer.zoomToLayer();
          }}
        />
        <WebMapMapViewLayer {...defaultLayers.overLayer} />
      </WebMapMapView>
    </div>
  ),
};

HoldShiftToZoom.storyName = 'Map zooms when shift is pressed';
