/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { generateLayerId, generateMapId } from '@opengeoweb/webmap';
import type { Meta, StoryObj } from '@storybook/react';
import {
  MapView as WebMapMapView,
  MapViewLayer as WebMapMapViewLayer,
} from '../MapView';
import { publicLayers } from '../../layers';
import {
  profileDataLayer,
  profileStationLayer,
} from '../../layers/profileLayers';
import { connectProfileMapViewToProfileStationsMapById } from '../Axis/profileUtils';

const meta: Meta<typeof WebMapMapView> = {
  title: 'components/WebMapMapView',
  component: WebMapMapView,
};
export default meta;

type Story = StoryObj<typeof WebMapMapView>;

export const MapViewProfile: Story = {
  render: () => {
    const projection = {
      srs: 'GFI:TIME_ELEVATION',
      bbox: {
        left: new Date('2023-12-03T00:00:00Z').getTime(),
        bottom: -5000,
        right: new Date('2023-12-03T24:00:00Z').getTime(),
        top: 25000,
      },
    };

    const mapIdWithCeilometerProfile = generateMapId();
    const mapIdWithCeilometerStations = generateMapId();

    // eslint-disable-next-line react-hooks/rules-of-hooks
    React.useEffect(() => {
      connectProfileMapViewToProfileStationsMapById(
        mapIdWithCeilometerProfile,
        mapIdWithCeilometerStations,
      );
    }, [mapIdWithCeilometerProfile, mapIdWithCeilometerStations]);

    const profileDataLayerWithId = {
      ...profileDataLayer,
      id: generateLayerId(),
    };

    const profileStationLayerWithId = {
      ...profileStationLayer,
      id: generateLayerId(),
    };

    return (
      <div style={{ height: '100vh' }}>
        <div style={{ display: 'flex' }}>
          <div style={{ width: '30%', height: '100vh', position: 'relative' }}>
            <WebMapMapView
              mapId={mapIdWithCeilometerStations}
              bbox={{
                left: 317390.3974565875,
                bottom: 6240696.274470518,
                right: 795824.1435791275,
                top: 7798854.573570839,
              }}
            >
              <WebMapMapViewLayer {...publicLayers.baseLayerArcGisCanvas} />
              <WebMapMapViewLayer {...profileStationLayerWithId} />
            </WebMapMapView>
          </div>
          <div style={{ width: '70%', height: '100vh', position: 'relative' }}>
            <WebMapMapView
              mapId={mapIdWithCeilometerProfile}
              srs={projection.srs}
              bbox={projection.bbox}
            >
              <WebMapMapViewLayer {...profileDataLayerWithId} />
            </WebMapMapView>
          </div>
        </div>
      </div>
    );
  },
};

MapViewProfile.storyName = 'WebMapMapView with profile data';
