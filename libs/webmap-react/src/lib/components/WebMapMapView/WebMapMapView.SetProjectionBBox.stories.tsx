/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { Paper, Card, CardContent, Typography } from '@mui/material';

import { PROJECTION } from '@opengeoweb/shared';
import type { Meta, StoryObj } from '@storybook/react';
import {
  MapView as WebMapMapView,
  MapViewLayer as WebMapMapViewLayer,
} from '../MapView';
import { publicLayers, defaultLayers } from '../../layers';

const meta: Meta<typeof WebMapMapView> = {
  title: 'components/WebMapMapView',
  component: WebMapMapView,
};
export default meta;

type Story = StoryObj<typeof WebMapMapView>;

const TextboxDiv = (props: {
  text: {
    title: string;
    body: string;
  };
}): React.ReactElement => {
  const { text } = props;
  return (
    <div
      style={{
        position: 'absolute',
        left: '50px',
        top: '10px',
        zIndex: 10000,
      }}
    >
      <Paper>
        <Card>
          <CardContent>
            <Typography variant="subtitle1">{text.title}</Typography>
            {text.body !== '' && (
              <Typography variant="body2">{text.body}</Typography>
            )}
          </CardContent>
        </Card>
      </Paper>
    </div>
  );
};

export const SetProjectionBBox: Story = {
  render: () => (
    <div style={{ height: '100vh' }}>
      <div style={{ display: 'flex', height: '50%' }}>
        <div style={{ position: 'relative', width: '50%', height: '100%' }}>
          <TextboxDiv text={{ title: 'Default SRS and BBOX', body: '' }} />
          {/* 'Europe North Pole' */}
          <WebMapMapView mapId="map-projection-example-1">
            <WebMapMapViewLayer {...publicLayers.baseLayer} />
            <WebMapMapViewLayer {...defaultLayers.overLayer} />
          </WebMapMapView>
        </div>
        <div style={{ position: 'relative', width: '50%', height: '100%' }}>
          <TextboxDiv
            text={{
              title: 'Europe stereographic',
              body: PROJECTION.EPSG_32661.value,
            }}
          />
          <WebMapMapView
            mapId="map-projection-example-2"
            srs={PROJECTION.EPSG_32661.value}
            bbox={{
              left: -2776118.977564746,
              bottom: -6499490.259201691,
              right: 9187990.785775745,
              top: 971675.53185069,
            }}
          >
            <WebMapMapViewLayer {...publicLayers.baseLayer} />
            <WebMapMapViewLayer {...defaultLayers.overLayer} />
          </WebMapMapView>
        </div>
      </div>
      <div style={{ display: 'flex', height: '50%' }}>
        <div style={{ position: 'relative', width: '50%', height: '100%' }}>
          <TextboxDiv
            text={{
              title: 'Southern Hemisphere',
              body: PROJECTION.EPSG_3412.value,
            }}
          />
          {/* 'Southern Hemisphere' */}
          <WebMapMapView
            mapId="map-projection-example-3"
            srs={PROJECTION.EPSG_3412.value}
            bbox={{
              left: -4589984.273212382,
              bottom: -2752857.546211313,
              right: 5425154.657417289,
              top: 2986705.2537886878,
            }}
          >
            <WebMapMapViewLayer {...publicLayers.baseLayer} />
            <WebMapMapViewLayer {...defaultLayers.overLayer} />
          </WebMapMapView>
        </div>
        <div style={{ position: 'relative', width: '50%', height: '100%' }}>
          <TextboxDiv
            text={{
              title: 'Northern Hemisphere',
              body: PROJECTION.EPSG_3411.value,
            }}
          />
          {/* Northern Hemisphere */}
          <WebMapMapView
            mapId="map-projection-example-4"
            srs={PROJECTION.EPSG_3412.value}
            bbox={{
              left: -5661541.927991125,
              bottom: -3634073.745615984,
              right: 5795287.923063262,
              top: 2679445.334384017,
            }}
          >
            <WebMapMapViewLayer {...publicLayers.baseLayer} />
            <WebMapMapViewLayer {...defaultLayers.overLayer} />
          </WebMapMapView>
        </div>
      </div>
    </div>
  ),
};

SetProjectionBBox.storyName = 'Set projection and BBOX';
