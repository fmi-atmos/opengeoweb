/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useContext, useEffect, useState } from 'react';
import { StyleLike } from 'ol/style/Style';
import { Feature, MapBrowserEvent } from 'ol';
import { Point } from 'ol/geom';
import { ListenerFunction } from 'ol/events';
import BaseEvent from 'ol/events/Event';
import { FeatureLayer } from '../layers/FeatureLayer';
import MapContext from '../context/MapContext';

export interface ClickOnMapToolProps {
  isActive: boolean;
  isVisible: boolean;
  layerZIndex?: number;
  pointStyle: StyleLike;
  pointSelected: (coords: number[]) => void;
}

export const ClickOnMapTool: React.FC<ClickOnMapToolProps> = ({
  isActive,
  isVisible,
  pointStyle,
  layerZIndex = 999999,
  pointSelected,
}: ClickOnMapToolProps) => {
  const { map } = useContext(MapContext);
  const [features, setFeatures] = useState<Feature[]>([]);

  useEffect(() => {
    if (!map || !isActive) {
      return (): void => {};
    }

    const clickHandler: ListenerFunction = (event: Event | BaseEvent): void => {
      const coords = (event as MapBrowserEvent<MouseEvent>).coordinate;
      setFeatures([new Feature(new Point(coords))]);
      pointSelected(coords);
    };

    map.addEventListener('click', clickHandler);

    return (): void => {
      map.removeEventListener('click', clickHandler);
    };
  }, [map, isActive, pointSelected]);

  return (
    <FeatureLayer
      features={features}
      style={isVisible ? pointStyle : []}
      zIndex={layerZIndex}
    />
  );
};
