/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { LayerType, tilesettings, webmapUtils } from '@opengeoweb/webmap';
import { MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { PROJECTION } from '@opengeoweb/shared';
import type { Meta, StoryObj } from '@storybook/react';
import { publicLayers } from '../../layers';
import { OpenLayersMapView } from '.';
import { OpenLayersLayerProps, OpenLayersLayer } from './layers';
import { useProjectionDropDown } from './useProjectionDropDown';

const meta: Meta<typeof OpenLayersMapView> = {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
export default meta;

type Story = StoryObj<typeof OpenLayersMapView>;

export interface MapDropDownItem {
  name: string;
  title: string;
}
export interface MapDropDownProps {
  value: string;
  change: (updatedValue: string) => void;
  options: MapDropDownItem[];
  top?: number;
  left?: number;
}

const MapDropDown = ({
  value,
  change,
  options,
  top,
  left,
}: MapDropDownProps): React.ReactElement<MapDropDownProps> => (
  <div
    style={{
      position: 'absolute',
      top: top ? `${top}px` : '20px',
      left: left ? `${left}px` : '50px',
      zIndex: 1000,
    }}
  >
    <Select
      value={value}
      onChange={(event: SelectChangeEvent<string>): void => {
        change(event.target.value);
      }}
    >
      {options &&
        options.map((option) => {
          return (
            <MenuItem key={option.name} value={option.name}>
              {option.title}
            </MenuItem>
          );
        })}
    </Select>
  </div>
);

export const MapViewTiledBaseLayers: Story = {
  render: () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const { view, selectComponent } = useProjectionDropDown('EPSG:3857');

    // Make list of baselayers to select
    const baseLayerOptions: OpenLayersLayerProps[] = Object.keys(
      tilesettings,
    ).map((tileSettingName) => {
      return {
        name: tileSettingName,
        title: tileSettingName,
        type: 'twms',
        layerType: LayerType.baseLayer,
        enabled: true,
        id: `tile_${webmapUtils.generateLayerId()}`,
      };
    });

    // This is the custom tiled layer
    const customTiledBaseLayer: OpenLayersLayerProps = {
      tileServer: {
        mytileserver: {
          [PROJECTION.EPSG_3857.value]: {
            home: 'https://tile.openstreetmap.org/',
            minLevel: 0,
            maxLevel: 9,
            tileServerType: 'osm',
            copyRight: 'tile.openstreetmap.org',
          },
        },
      },
      name: 'mytileserver',
      title: 'mytileserver',
      type: 'twms',
      layerType: LayerType.baseLayer,
      enabled: true,
      id: `tile_${webmapUtils.generateLayerId()}`,
    };

    // Add the custom tiled layer to the options to choose from
    baseLayerOptions.push(customTiledBaseLayer);

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [selectedBaseLayer, setSelectedBaseLayer] = React.useState<string>(
      baseLayerOptions[0].name || '',
    );

    // Find the matching baselayer in the list
    const baseLayer: OpenLayersLayerProps = {
      ...(baseLayerOptions.find(
        (baseLayer) => baseLayer.name === selectedBaseLayer,
      ) || publicLayers.baseLayer),
    };

    return (
      <div style={{ height: '100vh' }}>
        <OpenLayersMapView view={view}>
          <OpenLayersLayer {...baseLayer} />
        </OpenLayersMapView>
        <MapDropDown
          value={selectedBaseLayer}
          options={baseLayerOptions as MapDropDownItem[]}
          change={setSelectedBaseLayer}
        />
        <div
          style={{
            position: 'absolute',
            top: '120px',
            left: '50px',
            zIndex: 1000,
          }}
        >
          {selectComponent}
        </div>
      </div>
    );
  },
};
MapViewTiledBaseLayers.storyName = 'MapView displaying custom tiled baselayers';
