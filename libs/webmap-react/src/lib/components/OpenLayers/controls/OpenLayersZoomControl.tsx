/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useContext } from 'react';
import { Extent, getCenter } from 'ol/extent';
import { View } from 'ol';

import { ZoomControls } from '../../MapControls';
import MapContext from '../context/MapContext';
import { OPENLAYERS_MOUSEWHEEL_DURATION } from '../OpenLayersConstants';

interface OpenLayersZoomControlProps {
  homeExtent?: Extent;
}

export const OpenLayersZoomControl: React.FC<OpenLayersZoomControlProps> = ({
  homeExtent,
}: OpenLayersZoomControlProps) => {
  const { map } = useContext(MapContext);

  if (!map) {
    return null;
  }

  const getView = (): View | undefined =>
    map && map.getView() && map.getView().getZoom() !== undefined
      ? map.getView()
      : undefined;

  const duration = OPENLAYERS_MOUSEWHEEL_DURATION;

  return (
    <ZoomControls
      onZoomIn={(): void => {
        const view = getView();
        view && view.animate({ zoom: view.getZoom()! + 1, duration });
      }}
      onZoomOut={(): void => {
        const view = getView();
        view && view.animate({ zoom: view.getZoom()! - 1, duration });
      }}
      onZoomReset={(): void => {
        const view = getView();
        if (!homeExtent || !view) {
          return;
        }
        const center = getCenter(homeExtent);
        const resolution = view.getResolutionForExtent(homeExtent);
        view.animate({ center, resolution, duration });
      }}
    />
  );
};
