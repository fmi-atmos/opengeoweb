/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen } from '@testing-library/react';

import { Map, View } from 'ol';
import { OpenLayersZoomControl } from './OpenLayersZoomControl';
import MapContext, { MapContextType } from '../context/MapContext';
import { WebmapReactThemeProvider } from '../../Providers';
import { OPENLAYERS_MOUSEWHEEL_DURATION } from '../OpenLayersConstants';

const mockMapContext = (view: View | null): MapContextType => ({
  map: {
    getView: () => view,
  } as unknown as Map,
});

describe('OpenLayersZoomControl', () => {
  it('should render the buttons when map is present and it has a view', async () => {
    const mockContext = mockMapContext(new View());

    render(
      <WebmapReactThemeProvider>
        <MapContext.Provider value={mockContext}>
          <OpenLayersZoomControl />
        </MapContext.Provider>
      </WebmapReactThemeProvider>,
    );

    expect(screen.getByTestId('zoom-reset')).toBeDefined();
    expect(screen.getByTestId('zoom-in')).toBeDefined();
    expect(screen.getByTestId('zoom-out')).toBeDefined();
  });

  it('should render the buttons when there is a map, but no view', async () => {
    const mockContext = mockMapContext(null);

    render(
      <WebmapReactThemeProvider>
        <MapContext.Provider value={mockContext}>
          <OpenLayersZoomControl />
        </MapContext.Provider>
      </WebmapReactThemeProvider>,
    );

    expect(screen.getByTestId('zoom-reset')).toBeDefined();
    expect(screen.getByTestId('zoom-in')).toBeDefined();
    expect(screen.getByTestId('zoom-out')).toBeDefined();
  });

  it('should not render the buttons when no map is present', async () => {
    const mockContext = {
      map: null,
    };

    render(
      <WebmapReactThemeProvider>
        <MapContext.Provider value={mockContext}>
          <OpenLayersZoomControl />
        </MapContext.Provider>
      </WebmapReactThemeProvider>,
    );

    expect(screen.queryByTestId('zoom-reset')).toBeNull();
    expect(screen.queryByTestId('zoom-in')).toBeNull();
    expect(screen.queryByTestId('zoom-out')).toBeNull();
  });

  it('should call View.animate() when zoom in is pressed', async () => {
    const mockView = {
      animate: jest.fn().mockImplementation(() => void 0),
      getZoom: jest.fn().mockImplementation((): number => 4),
    };

    const mockContext = mockMapContext(mockView as unknown as View);

    render(
      <WebmapReactThemeProvider>
        <MapContext.Provider value={mockContext}>
          <OpenLayersZoomControl />
        </MapContext.Provider>
      </WebmapReactThemeProvider>,
    );

    screen.getByTestId('zoom-in').click();
    expect(mockView.getZoom).toHaveBeenCalled();
    expect(mockView.animate).toHaveBeenCalledTimes(1);
    expect(mockView.animate).toHaveBeenCalledWith({
      zoom: 5,
      duration: OPENLAYERS_MOUSEWHEEL_DURATION,
    });
  });

  it('should call View.animate() when zoom out is pressed', async () => {
    const mockView = {
      animate: jest.fn().mockImplementation(() => void 0),
      getZoom: jest.fn().mockImplementation((): number => 4),
    };

    const mockContext = mockMapContext(mockView as unknown as View);

    render(
      <WebmapReactThemeProvider>
        <MapContext.Provider value={mockContext}>
          <OpenLayersZoomControl />
        </MapContext.Provider>
      </WebmapReactThemeProvider>,
    );

    screen.getByTestId('zoom-out').click();
    expect(mockView.getZoom).toHaveBeenCalled();
    expect(mockView.animate).toHaveBeenCalledTimes(1);
    expect(mockView.animate).toHaveBeenCalledWith({
      zoom: 3,
      duration: OPENLAYERS_MOUSEWHEEL_DURATION,
    });
  });

  it('should NOT call View.animate() when zoom out is pressed, if there is no homeExtent defined', async () => {
    const mockView = {
      animate: jest.fn().mockImplementation(() => void 0),
      getZoom: jest.fn().mockImplementation((): number => 4),
    };

    const mockContext = mockMapContext(mockView as unknown as View);

    render(
      <WebmapReactThemeProvider>
        <MapContext.Provider value={mockContext}>
          <OpenLayersZoomControl />
        </MapContext.Provider>
      </WebmapReactThemeProvider>,
    );

    screen.getByTestId('zoom-reset').click();
    expect(mockView.animate).toHaveBeenCalledTimes(0);
  });

  it('should call View.animate() when zoom out is pressed (has home extent)', async () => {
    const mockView = {
      animate: jest.fn().mockImplementation(() => void 0),
      getZoom: jest.fn().mockImplementation((): number => 4),
      getResolutionForExtent: jest.fn().mockImplementation((): number => 700),
    };

    const mockContext = mockMapContext(mockView as unknown as View);

    const homeExtent = [0, 0, 1, 1];

    render(
      <WebmapReactThemeProvider>
        <MapContext.Provider value={mockContext}>
          <OpenLayersZoomControl homeExtent={homeExtent} />
        </MapContext.Provider>
      </WebmapReactThemeProvider>,
    );

    screen.getByTestId('zoom-reset').click();
    expect(mockView.animate).toHaveBeenCalledTimes(1);
    expect(mockView.getResolutionForExtent).toHaveBeenCalledTimes(1);
  });
});
