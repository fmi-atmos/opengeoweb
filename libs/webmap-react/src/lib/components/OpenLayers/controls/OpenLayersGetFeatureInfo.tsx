/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useContext, useEffect, useMemo } from 'react';
import { Feature } from 'ol';
import { Point } from 'ol/geom';
import { unByKey } from 'ol/Observable';
import { fromLonLat, toLonLat } from 'ol/proj';
import { locationPath } from '@opengeoweb/theme';
import { FeatureLayer } from '../layers';
import MapContext from '../context/MapContext';
import { MapLocation } from '../../ReactMapView';
import { useIconStyle } from '../styles/useIconStyle';

export interface OpenLayersGetFeatureInfoProps {
  displayMapPin?: boolean;
  mapPinLocation?: MapLocation;
  selectLocation: (location: MapLocation) => void;
}

export const OpenLayersGetFeatureInfo: React.FC<
  OpenLayersGetFeatureInfoProps
> = ({
  displayMapPin,
  mapPinLocation,
  selectLocation,
}: OpenLayersGetFeatureInfoProps) => {
  const { map } = useContext(MapContext);

  const { icon: style } = useIconStyle({
    svgPath: locationPath,
    options: {
      icon: {
        anchor: [0.5, 1],
      },
    },
  });

  const features = useMemo((): Feature<Point>[] => {
    if (!mapPinLocation || !map) {
      return [];
    }
    const projection = map.getView().getProjection();
    const coords = fromLonLat(
      [mapPinLocation.lon, mapPinLocation.lat],
      projection,
    );
    return [new Feature<Point>(new Point(coords))];
  }, [mapPinLocation, map]);

  useEffect(() => {
    if (!displayMapPin || !map) {
      return (): void => {};
    }

    const key = map.on('click', (e) => {
      const projection = map.getView().getProjection();
      const resolution = map.getView().getResolution();
      const location = toLonLat(e.coordinate, projection);

      selectLocation({
        lon: location[0],
        lat: location[1],
        srs: projection.getCode(),
        projectionX: e.coordinate[0],
        projectionY: e.coordinate[1],
        resolution,
      });
    });

    return (): void => {
      unByKey(key);
    };
  }, [map, displayMapPin, selectLocation]);

  if (!displayMapPin) {
    return null;
  }

  return <FeatureLayer zIndex={9000} features={features} style={style} />;
};
