/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { generateLayerId } from '@opengeoweb/webmap';
import type { Meta, StoryObj } from '@storybook/react';
import { useRef } from 'react';
import { publicLayers, defaultLayers } from '../../layers';
import { OpenLayersMapView } from '.';
import { OpenLayersLayer } from './layers';
import { useAnimationForLayer } from '../../hooks/useAnimationForLayer';
import { useViewFromLayer } from '../../hooks/useViewFromLayerById';

const meta: Meta<typeof OpenLayersMapView> = {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
export default meta;

type Story = StoryObj<typeof OpenLayersMapView>;

const RadarAnimationComponent = (): React.ReactElement => {
  const radarLayer = {
    ...publicLayers.radarLayer,
    id: useRef(generateLayerId()).current,
  };

  const view = useViewFromLayer(radarLayer);

  const layerDimensions = useAnimationForLayer(radarLayer, 100, 48);

  return (
    <div style={{ height: '100vh' }}>
      <OpenLayersMapView view={view}>
        <OpenLayersLayer {...publicLayers.baseLayer} />
        <OpenLayersLayer {...radarLayer} dimensions={layerDimensions} />
        <OpenLayersLayer {...defaultLayers.overLayer} />
      </OpenLayersMapView>
    </div>
  );
};

export const RadarAnimation: Story = {
  render: () => {
    return <RadarAnimationComponent />;
  },
};

RadarAnimation.storyName = 'MapView with radar animation';
