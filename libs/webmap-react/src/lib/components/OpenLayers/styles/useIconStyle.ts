/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useTheme } from '@mui/material';
import { memoize } from 'lodash';
import { Icon, Style } from 'ol/style';
import { Options } from 'ol/style/Icon';
import { useMemo } from 'react';

type IconSVG = string | { path: string; width?: number; height?: number };
interface IconParameters {
  render?: {
    fillStyle?: string | CanvasGradient | CanvasPattern;
    strokeStyle?: string | CanvasGradient | CanvasPattern;
    lineWidth?: number;
  };
  icon?: Omit<Options, 'img'>;
}

interface UseIconStyleProps {
  svgPath: string;
  options: IconParameters;
}

interface IconStyle {
  icon: Style;
}

export const createIconStyle = memoize(
  (input: IconSVG, options: IconParameters): Style => {
    const svgPath = typeof input === 'object' ? input.path : input;
    const width = typeof input === 'object' && input.width ? input.width : 23;
    const height =
      typeof input === 'object' && input.height ? input.height : 21;

    const canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    const ctx = canvas.getContext('2d')!;
    const path = new Path2D(svgPath);
    if (options.render?.strokeStyle) {
      ctx.strokeStyle = options.render.strokeStyle;
      ctx.lineWidth = options.render.lineWidth || 1;
      ctx.stroke(path);
    }
    if (options.render?.fillStyle) {
      ctx.fillStyle = options.render.fillStyle;
      ctx.fill(path);
    }
    ctx.restore();
    ctx.save();

    return new Style({
      image: new Icon({
        ...options.icon,
        img: canvas,
      }),
    });
  },
  (path, opt) => {
    return JSON.stringify({ path, opt });
  },
);

export const useIconStyle = ({
  svgPath,
  options,
}: UseIconStyleProps): IconStyle => {
  const theme = useTheme();

  const icon = useMemo(() => {
    return createIconStyle(svgPath, {
      render: {
        fillStyle: theme.palette.geowebColors.typographyAndIcons.icon || '#000',
        ...options.render,
      },
      ...options,
    });
  }, [svgPath, options, theme]);

  return { icon };
};
