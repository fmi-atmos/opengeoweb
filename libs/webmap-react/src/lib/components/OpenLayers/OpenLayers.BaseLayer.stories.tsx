/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { View } from 'ol';

import type { Meta, StoryObj } from '@storybook/react';
import { XYZLayer } from './layers/XYZLayer';
import { WMTSLayer } from './layers/WMTSLayer';
import { OpenLayersMapView } from '.';

const meta: Meta<typeof OpenLayersMapView> = {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
  args: {
    passiveMap: false,
    isMouseWheelZoomEnabled: true,
    holdShiftToScroll: false,
  },
  parameters: {
    docs: {
      description: {
        component: 'OpenLayersMapView component',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof OpenLayersMapView>;

export const BaseLayerStory: Story = {
  args: {
    isFocused: false,
    view: new View({
      projection: 'EPSG:3857',
      zoom: 7,
      center: [576423.573584, 6818457.618608],
    }),
  },
  render: (props) => {
    return (
      <div style={{ height: 400, width: '100%' }}>
        <OpenLayersMapView {...props}>
          <WMTSLayer
            getCapsURL="https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0?SERVICE=WMTS&REQUEST=GetCapabilities"
            layerName="pastel"
            tileMatrixSet="EPSG:3857"
            layerOptions={{
              zIndex: 3,
              opacity: 1,
            }}
          />
          <XYZLayer
            urlTemplate="https://geoweb-maps-assets.pmc.knmi.cloud/WorldMap_Light_Grey_Canvas/EPSG3857/{z}/{x}/{y}.png"
            layerOptions={{
              zIndex: 1,
              opacity: 0.9,
            }}
          />
        </OpenLayersMapView>
      </div>
    );
  },
  tags: ['!dev'],
};

BaseLayerStory.storyName = 'Map with baselayers';
