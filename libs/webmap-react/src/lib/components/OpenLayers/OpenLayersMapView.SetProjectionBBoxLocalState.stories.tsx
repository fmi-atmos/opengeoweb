/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Paper, Box } from '@mui/material';

import type { Meta, StoryObj } from '@storybook/react';
import { publicLayers, defaultLayers } from '../../layers';
import { initializeOpenLayersProjections, OpenLayersMapView } from '.';
import { OpenLayersLayer } from './layers';
import { useProjectionDropDown } from './useProjectionDropDown';

initializeOpenLayersProjections();

const meta: Meta<typeof OpenLayersMapView> = {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
export default meta;

type Story = StoryObj<typeof OpenLayersMapView>;

export const SetProjectionBBoxLocalState: Story = {
  args: {},
  render: () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const { view, selectComponent } = useProjectionDropDown();
    return (
      <>
        <div style={{ height: '100vh' }}>
          {/* 'Europe North Pole' */}
          <OpenLayersMapView view={view}>
            <OpenLayersLayer {...publicLayers.baseLayer} />
            <OpenLayersLayer {...defaultLayers.overLayer} />
          </OpenLayersMapView>
        </div>
        <div
          style={{
            position: 'absolute',
            top: '20px',
            left: '50px',
            zIndex: 1000,
          }}
        >
          <Box>
            <Paper>{selectComponent}</Paper>
          </Box>
        </div>
      </>
    );
  },
};

SetProjectionBBoxLocalState.storyName =
  'Set projection and BBOX via local state';
