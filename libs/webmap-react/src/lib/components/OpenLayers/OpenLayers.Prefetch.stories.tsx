/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { View } from 'ol';

import { wmsQueryClient } from '@opengeoweb/webmap';
import { QueryClientProvider } from '@tanstack/react-query';

import TimeContext from './context/TimeContext';
import { DefaultBaseLayers } from './layers/DefaultBaseLayers';
import { TimeawareImageSourceWMSLayer } from './layers/TimeawareImageSourceWMSLayer';
import { OpenLayersMapView } from './component/OpenLayersMapView';
import { TimeContextType } from './types/Timespan';

export default {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};

const layerOptionsTemperatureModel = { opacity: 0.33, zIndex: 1 };
const layerOptionsPressure = { opacity: 0.5, zIndex: 2 };
const layerOptionsTemperature = { opacity: 1, zIndex: 5 };
const layerOptionsRadar = { opacity: 1, zIndex: 10 };

const now = new Date();
const offset = -(1000 * 60 * 60 * 18); // 12 hours behind

export const PrefetchMap = (): React.ReactNode => {
  const [animating, setAnimating] = React.useState<boolean>(false);
  const [currentTime, setCurrentTime] = React.useState<number>(
    now.getTime() - (now.getTime() % (1000 * 60 * 60)) + offset,
  );

  const timespan = React.useMemo(() => {
    const timespan = {
      // start: start of today + offset
      start:
        new Date().getTime() - (now.getTime() % (1000 * 60 * 60 * 24)) + offset,
      // end: end of today + offset
      end:
        new Date().getTime() -
        (now.getTime() % (1000 * 60 * 60 * 24)) +
        1000 * 60 * 60 * 24 +
        offset,
      step: 60 * 60 * 1000, // 1 hour
    };

    return timespan;
  }, []);

  const timeContextValue = React.useMemo(
    (): TimeContextType => ({
      timespan,
      isAnimating: false,
    }),
    [timespan],
  );

  React.useEffect(() => {
    if (!animating) {
      return (): void => {};
    }

    const interval = setInterval(() => {
      setCurrentTime((curr) => {
        const next =
          curr + timespan.step > timespan.end
            ? timespan.start
            : curr + timespan.step;
        // Before stepping, I need to check the prefetch state of all layers
        // TODO: how can I access the WMLayers in question?

        return next;
      });
    }, 500);

    return (): void => {
      clearInterval(interval);
    };
  }, [animating, timespan]);

  const view = React.useMemo(() => {
    return new View({
      projection: 'EPSG:3857',
      zoom: 7,
      center: [576423.573584, 6818457.618608],
    });
  }, []);

  const dimensions = [
    {
      name: 'time',
      currentValue: `${new Date(currentTime).toISOString().substring(0, 19)}Z`,
    },
  ];

  return (
    <QueryClientProvider client={wmsQueryClient}>
      <div style={{ height: '100vh' }}>
        <TimeContext.Provider value={timeContextValue}>
          <OpenLayersMapView view={view} isFocused={true}>
            <DefaultBaseLayers />
            <TimeawareImageSourceWMSLayer
              getCapsURL="https://geoservices.knmi.nl/adagucserver?dataset=uwcw_ha43_dini_5p5km&service=WMS&request=GetCapabilities"
              layerName="air_temperature_hagl"
              styleName="temperature_wow/shaded"
              layerOptions={layerOptionsTemperatureModel}
              dimensions={dimensions}
            />
            <TimeawareImageSourceWMSLayer
              getCapsURL="https://geoservices.knmi.nl/adaguc-server?DATASET=OBS&SERVICE=WMS&service=WMS&request=GetCapabilities"
              layerName="10M/ta"
              layerOptions={layerOptionsTemperature}
              dimensions={dimensions}
            />
            <TimeawareImageSourceWMSLayer
              getCapsURL="https://geoservices.knmi.nl/adagucserver?dataset=uwcw_ha43_dini_5p5km&service=WMS&request=GetCapabilities"
              layerName="air_pressure_at_mean_sea_level_hagl"
              styleName="temperature_wow/shaded"
              layerOptions={layerOptionsPressure}
              dimensions={dimensions}
            />
            <TimeawareImageSourceWMSLayer
              getCapsURL="https://geoservices.knmi.nl/adaguc-server?DATASET=RADAR&SERVICE=WMS&service=WMS&request=GetCapabilities"
              layerName="RAD_NL25_PCP_CM"
              layerOptions={layerOptionsRadar}
              dimensions={dimensions}
            />
          </OpenLayersMapView>
          <button
            type="button"
            style={{
              position: 'absolute',
              left: '1em',
              width: '3em',
              bottom: '1em',
              height: '1.5em',
            }}
            onClick={() => setAnimating(!animating)}
          >
            {animating ? '||' : '>'}
          </button>
          <input
            type="range"
            min={timespan.start}
            max={timespan.end}
            value={currentTime}
            onChange={(evt) =>
              setCurrentTime(
                Number(evt.target.value) -
                  (Number(evt.target.value) % timespan.step),
              )
            }
            style={{
              position: 'absolute',
              bottom: '1em',
              left: '5em',
              right: '1em',
              width: '100%-2em',
            }}
          />
        </TimeContext.Provider>
      </div>
    </QueryClientProvider>
  );
};

PrefetchMap.storyName = 'OpenLayers prefetch example';
