/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { FeatureLike } from 'ol/Feature';
import { LineString, MultiPoint, Polygon } from 'ol/geom';
import { Circle, Fill, Stroke, Text } from 'ol/style';
import Style, { StyleFunction, StyleLike } from 'ol/style/Style';
import { locationPath } from '@opengeoweb/theme';
import { ColorLike } from 'ol/colorlike';
import { createIconStyle } from './styles/useIconStyle';
import { DrawModeValue } from '../MapDrawTool';

export enum MapFeatureClass {
  MyLocation = 'MyLocation',
  GenericMarker = 'GenericMarker',
  ObservationStation = 'ObservationStation',
  default = 'default',
}

export const FEATURE_FILL = new Fill({ color: '#e142' });
export const FEATURE_STROKE = new Stroke({ color: '#e14', width: 1.5 });
export const FEATURE_STROKE_EDIT = new Stroke({
  color: '#e14',
  width: 1.5 + 1,
});
export const FEATURE_VERTICE_IMAGE = new Circle({
  radius: 5,
  stroke: new Stroke({
    color: '#333',
    width: 1.5 + 1,
  }),
  fill: new Fill({
    color: '#fff',
  }),
});

export const FEATURE_VERTICE_HANDLE_IMAGE = new Circle({
  radius: 5,
  stroke: new Stroke({
    color: '#3339',
    width: 1.5 + 1,
  }),
  fill: new Fill({
    color: '#fff9',
  }),
});

export const ICON_LOCATIONMARKER = createIconStyle(locationPath, {
  render: {
    fillStyle: '#000',
  },
  icon: {
    anchor: [0.5, 1],
  },
});
export const ICON_LOCATIONMARKER_MODIFY = createIconStyle(locationPath, {
  render: {
    fillStyle: '#eee',
    strokeStyle: '#e14',
    lineWidth: 2,
  },
  icon: {
    anchor: [0.5, 1],
  },
});

export const FEATURE_VERTICES_EDIT_HANDLES = new Style({
  image: FEATURE_VERTICE_IMAGE,
  geometry: (feature: FeatureLike): MultiPoint | undefined => {
    const geom = feature?.getGeometry();
    if (geom && geom.getType() === 'Polygon') {
      const coordinates = (geom as Polygon).getCoordinates()[0];
      return new MultiPoint(coordinates);
    }
    if (geom && geom.getType() === 'LineString') {
      const coordinates = (geom as LineString).getCoordinates();
      return new MultiPoint(coordinates);
    }
    return undefined;
  },
});

const FEATURE_OBSERVATIONSTATION = [
  // Outer circle
  new Style({
    image: new Circle({
      radius: 8,
      fill: new Fill({
        color: '#8888FF66',
      }),
    }),
  }),
  // Inner circle
  new Style({
    image: new Circle({
      radius: 2,
      fill: new Fill({ color: '#000' }),
    }),
  }),
];

const FEATURE_OBSERVATIONSTATION_HOVER = [
  // Outer circle
  new Style({
    image: new Circle({
      radius: 8,
      fill: new Fill({
        color: '#8888FF66',
      }),
      stroke: new Stroke({
        color: '#000',
        width: 1,
      }),
    }),
  }),
  // Inner circle
  new Style({
    image: new Circle({
      radius: 4.5,
      fill: new Fill({ color: '#000' }),
    }),
  }),
];

const labelBaseStyle = new Style({
  text: new Text({
    font: '16px Roboto',
    padding: [8, 8, 8, 8],
    fill: new Fill({ color: '#eee' }),
    text: '-', // f.getProperties().name,
    backgroundFill: new Fill({ color: '#111' }),
    backgroundStroke: new Stroke({
      color: '#111',
      width: 4,
      lineCap: 'round',
      lineJoin: 'round',
    }),
    offsetY: 40,
  }),
});

export const textLableStyle = (text: string): Style => {
  labelBaseStyle.getText()?.setText(text);
  return labelBaseStyle;
};

const stylesByMapFeatureClass: Record<
  MapFeatureClass,
  Style | Style[] | StyleFunction
> = {
  [MapFeatureClass.GenericMarker]: (f: FeatureLike) =>
    !f.get('hover')
      ? ICON_LOCATIONMARKER
      : [ICON_LOCATIONMARKER, textLableStyle(f.getProperties().name)],
  [MapFeatureClass.MyLocation]: [
    // Outer circle
    new Style({
      image: new Circle({
        radius: 12,
        stroke: new Stroke({ color: 'black', width: 1 }),
        fill: new Fill({ color: '#8888ff' }),
      }),
    }),
    // Inner circle
    new Style({
      image: new Circle({
        radius: 2,
        fill: new Fill({ color: '#000' }),
      }),
    }),
  ],
  [MapFeatureClass.ObservationStation]: (f: FeatureLike) =>
    !f.get('hover')
      ? FEATURE_OBSERVATIONSTATION
      : [
          ...FEATURE_OBSERVATIONSTATION_HOVER,
          textLableStyle(f.getProperties().name),
        ],
  [MapFeatureClass.default]: [
    // Polygon / LineString
    new Style({
      fill: FEATURE_FILL,
      stroke: FEATURE_STROKE,
    }),
    // Point
    ICON_LOCATIONMARKER,
  ],
};

export const geowebColorToOpenLayersColor = (
  color: string,
  opacity?: string | number,
): ColorLike => {
  // Return as-is if no opacity, or non-hex color (we do not know how to add opacity to non-hex colors)
  if (color[0] !== '#' || opacity === undefined) {
    return color;
  }

  const decimalToHex = (decimal: number, padding: string): string => {
    const hex = decimal.toString(16).split('');

    if (hex.length < padding.length) {
      hex.push(padding.substring(hex.length));
    }

    return hex.join('');
  };

  const opacityNumber = Math.min(
    Math.max(0, Number.parseFloat(`${opacity}`)),
    255 / 256,
  );

  const bitsPerColor = color.length - 1 >= 6 ? 8 : 4;

  const resultingColor = color.split('');

  // Remove existing opacity bits if set
  if (bitsPerColor === 8 && resultingColor.length > 6 + 1) {
    resultingColor.splice(6 + 1);
  } else if (bitsPerColor === 4 && resultingColor.length > 3 + 1) {
    resultingColor.splice(3 + 1);
  }

  if (bitsPerColor === 8) {
    resultingColor.push(decimalToHex(Math.floor(256 * opacityNumber), '00'));
  } else {
    resultingColor.push(Math.floor(16 * opacityNumber).toString(16));
  }

  return resultingColor.join('');
};

export const inlineFeatureStyle: StyleFunction = (f: FeatureLike) => {
  const fillProperty = f.getProperties()['fill'];
  const strokeProperty = f.getProperties()['stroke'];
  const fillStrokeStyle = new Style();

  const styles: Style[] = [fillStrokeStyle];

  if (fillProperty) {
    const fillColor = geowebColorToOpenLayersColor(
      fillProperty,
      f.getProperties()['fill-opacity'],
    );
    fillStrokeStyle.setFill(
      new Fill({
        color: fillColor,
      }),
    );

    const iconStyle = createIconStyle(locationPath, {
      render: {
        fillStyle: fillColor,
      },
      icon: {
        anchor: [0.5, 1],
      },
    });
    styles.push(iconStyle);
  }
  if (strokeProperty) {
    fillStrokeStyle.setStroke(
      new Stroke({
        color: geowebColorToOpenLayersColor(
          strokeProperty,
          f.getProperties()['stroke-opacity'],
        ),
        width: f.getProperties()['stroke-width'],
      }),
    );
  }

  return styles;
};

export const genericOpenLayersFeatureStyle: StyleFunction = (
  f: FeatureLike,
  resolution: number,
) => {
  const { hidden, fill, stroke } = f.getProperties();
  if (hidden === true) {
    return [];
  }
  if (fill || stroke) {
    return inlineFeatureStyle(f, resolution);
  }
  const mapFeatureClass =
    (f.getProperties().mapFeatureClass as MapFeatureClass) ||
    MapFeatureClass.default;
  const styleOrFunction = stylesByMapFeatureClass[mapFeatureClass];
  if (typeof styleOrFunction === 'function') {
    return styleOrFunction(f, resolution);
  }
  return styleOrFunction;
};

// Used for polygons, boxes and linestrings
const drawPolygonLikeFeatureStyle: StyleLike = (f: FeatureLike) => {
  if (f.getGeometry()?.getType() === 'Polygon') {
    return [new Style({ fill: FEATURE_FILL })];
  }

  // LineStrings, this includes Polygons being drawn (OL Draw has them as LineStrings until the Polygon is ready)
  return [
    // Style for the line
    new Style({ stroke: FEATURE_STROKE_EDIT }),
    // Extract vertices as points
    new Style({
      image: FEATURE_VERTICE_IMAGE,
      geometry: (feature: FeatureLike): MultiPoint | undefined => {
        const geom = feature?.getGeometry();
        if (geom && geom.getType() === 'LineString') {
          const coordinates = (geom as LineString).getCoordinates();
          return new MultiPoint(coordinates);
        }
        return undefined;
      },
    }),
  ];
};

const drawPointLikeFeatureStyle: StyleLike = ICON_LOCATIONMARKER_MODIFY;

const modifyPolygonLikeFeatureStyle: StyleLike = [
  // Area
  new Style({
    fill: FEATURE_FILL,
    stroke: FEATURE_STROKE_EDIT,
  }),
  // Vertices
  FEATURE_VERTICES_EDIT_HANDLES,
];

const modifyPointLikeFeatureStyle: StyleLike = ICON_LOCATIONMARKER_MODIFY;

export const drawStyles: Record<DrawModeValue, StyleLike> = {
  POLYGON: drawPolygonLikeFeatureStyle,
  BOX: drawPolygonLikeFeatureStyle,
  LINESTRING: drawPolygonLikeFeatureStyle,

  POINT: drawPointLikeFeatureStyle,
  MULTIPOINT: drawPointLikeFeatureStyle,

  DELETE: [],
  '': [],
};

export const modifyStyles: Record<DrawModeValue, Style[]> = {
  POLYGON: modifyPolygonLikeFeatureStyle,
  BOX: modifyPolygonLikeFeatureStyle,
  LINESTRING: modifyPolygonLikeFeatureStyle,

  POINT: [modifyPointLikeFeatureStyle],
  MULTIPOINT: [modifyPointLikeFeatureStyle],

  DELETE: [],
  '': [],
};
