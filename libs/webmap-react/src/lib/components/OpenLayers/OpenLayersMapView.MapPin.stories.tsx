/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { Paper, Card, CardContent, Typography } from '@mui/material';

import type { Meta, StoryObj } from '@storybook/react';
import { MapLocation } from '../ReactMapView';
import { publicLayers, defaultLayers } from '../../layers';
import { OpenLayersGetFeatureInfo, OpenLayersMapView } from '.';
import { OpenLayersLayer } from './layers';

const meta: Meta<typeof OpenLayersMapView> = {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
export default meta;

type Story = StoryObj<typeof OpenLayersMapView>;

export const SetMapPin: Story = {
  render: (props) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [mapPinLocation, setMapPinLocation] = React.useState<MapLocation>({
      lat: 52,
      lon: 5,
    });
    return (
      <div style={{ height: '100vh' }}>
        <div style={{ display: 'flex', height: '100%' }}>
          <div style={{ position: 'relative', width: '100%', height: '100%' }}>
            <div
              style={{
                position: 'absolute',
                left: '50px',
                top: '20px',
                zIndex: 10000,
              }}
            >
              <Paper>
                <Card>
                  <CardContent>
                    <Typography variant="subtitle1">
                      Position of map cursor:
                    </Typography>
                    <Typography variant="body2">
                      {`Lon: ${mapPinLocation.lon.toFixed(2)}`}
                      <br />
                      {`Lat: ${mapPinLocation.lat.toFixed(2)}`}
                      <br />
                    </Typography>
                  </CardContent>
                </Card>
              </Paper>
            </div>
            <OpenLayersMapView {...props}>
              <OpenLayersLayer {...publicLayers.baseLayerWorldMap} />
              <OpenLayersLayer {...defaultLayers.overLayer} />
              <OpenLayersGetFeatureInfo
                mapPinLocation={mapPinLocation}
                displayMapPin={true}
                selectLocation={(location) => setMapPinLocation(location)}
              />
            </OpenLayersMapView>
          </div>
        </div>
      </div>
    );
  },
  tags: ['!autodocs'],
};

SetMapPin.storyName = 'Set Map cursor and location';
