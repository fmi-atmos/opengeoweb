/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { View } from 'ol';
import GeoJSON from 'ol/format/GeoJSON';
import { Circle, Fill, RegularShape, Stroke, Style } from 'ol/style';

import { QueryClientProvider } from '@tanstack/react-query';
import { wmsQueryClient } from '@opengeoweb/webmap';

import { StyleFunction } from 'ol/style/Style';
import Feature, { FeatureLike } from 'ol/Feature';
import { Geometry } from 'ol/geom';
import { Grid } from '@mui/system';
import { Typography } from '@mui/material';
import { XYZLayer } from './layers/XYZLayer';
import { FeatureLayer } from './layers/FeatureLayer';
import { OpenLayersMapView } from './component/OpenLayersMapView';
import { ClickOnFeature } from './utils/types';

export default {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};

const styles: Record<string, Style> = {
  emptyCircle: new Style({
    image: new Circle({
      radius: 5,
      stroke: new Stroke({ color: 'red', width: 1.5 }),
    }),
  }),
  filledCircle: new Style({
    image: new Circle({
      radius: 5,
      fill: new Fill({ color: 'blue' }),
    }),
  }),
  filledSquare: new Style({
    image: new RegularShape({
      fill: new Fill({ color: 'green' }),
      stroke: new Stroke({ color: 'green' }),
      radius: 5 / Math.SQRT2,
      radius2: 5,
      points: 4,
      angle: 0,
      scale: [1, 1],
    }),
  }),
};

const style: StyleFunction = (f: FeatureLike): Style | Style[] => {
  return styles[f.getProperties().type] || [];
};

export const ClickFeatureLayerMap = (): React.ReactNode => {
  const [clickedFeatureNames, setClickedFeatureNames] = React.useState<
    string[]
  >([]);
  const projection = 'EPSG:3857';

  const view = React.useMemo(() => {
    return new View({
      projection,
      zoom: 7,
      center: [576423.573584, 6818457.618608],
    });
  }, []);

  const features = React.useMemo(() => {
    const xoffset = 0.17;

    const geojson = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            type: 'emptyCircle',
            name: 'Red circle',
          },
          geometry: {
            coordinates: [5.1776803090635894, 52.101177319887455],
            type: 'Point',
          },
        },
        {
          type: 'Feature',
          properties: {
            type: 'filledCircle',
            name: 'Blue circle',
          },
          geometry: {
            coordinates: [5.1776803090635894 + xoffset, 52.101177319887455],
            type: 'Point',
          },
        },
        {
          type: 'Feature',
          properties: {
            type: 'filledSquare',
            name: 'Green square',
          },
          geometry: {
            coordinates: [5.1776803090635894 - xoffset, 52.101177319887455],
            type: 'Point',
          },
        },
      ],
    };

    const converter = new GeoJSON({ featureProjection: projection });
    return [converter.readFeatures(geojson)].flat();
  }, []);

  const clickOnFeature = React.useMemo((): ClickOnFeature => {
    return {
      hitTolerance: 8,
      onClick: (features: Feature<Geometry>[]) =>
        setClickedFeatureNames(
          features.map((feature) => feature.getProperties().name),
        ),
    };
  }, []);

  return (
    <QueryClientProvider client={wmsQueryClient}>
      <div style={{ height: '100vh' }}>
        <OpenLayersMapView view={view} isFocused={true}>
          <XYZLayer
            urlTemplate="https://geoweb-maps-assets.pmc.knmi.cloud/WorldMap_Light_Grey_Canvas/EPSG3857/{z}/{x}/{y}.png"
            layerOptions={{
              zIndex: 1,
              opacity: 1,
            }}
            layerProps={{
              enabled: true,
            }}
          />
          <FeatureLayer
            features={features}
            style={style}
            clickOnFeature={clickOnFeature}
            zIndex={2}
          />
        </OpenLayersMapView>
      </div>
      <div
        style={{
          position: 'absolute',
          bottom: 2,
          right: 4,
        }}
      >
        <Grid container alignItems="left" direction="row">
          <Typography>
            Clicked on:{' '}
            {clickedFeatureNames.length > 0
              ? clickedFeatureNames.join(', ')
              : 'nothing'}
          </Typography>
        </Grid>
      </div>
    </QueryClientProvider>
  );
};

ClickFeatureLayerMap.storyName = 'OpenLayers FeatureLayer onClick example';
