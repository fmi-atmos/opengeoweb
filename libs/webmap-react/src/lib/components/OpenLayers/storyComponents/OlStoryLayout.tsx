/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { wmsQueryClient } from '@opengeoweb/webmap';
import { QueryClientProvider } from '@tanstack/react-query';
import { View } from 'ol';
import { OpenLayersFeatureLayer } from '../../../../index';
import { DrawModeExitCallback, StoryLayoutGrid } from '../../MapDraw';
import { OpenLayersMapView } from '../component/OpenLayersMapView';
import { DefaultBaseLayers, OpenLayersLayerProps } from '../layers';
import { OpenLayersMapDraw } from '../draw';
import { genericOpenLayersFeatureStyle } from '../OlStyles';
import { DrawModeValue } from '../../MapDrawTool';

interface OlStoryLayoutProps {
  layers: OpenLayersLayerProps[];
  children: React.ReactElement;
  onExitDrawMode: (reason: DrawModeExitCallback) => void;
}

export const OlStoryLayout: React.FC<OlStoryLayoutProps> = ({
  layers,
  children,
  onExitDrawMode,
}: OlStoryLayoutProps) => {
  const view = React.useMemo(() => {
    return new View({
      projection: 'EPSG:3857',
      zoom: 7,
      center: [576423.573584, 6818457.618608],
    });
  }, []);

  return (
    <StoryLayoutGrid
      mapComponent={
        <QueryClientProvider client={wmsQueryClient}>
          <div style={{ height: '100vh' }}>
            <OpenLayersMapView view={view} isFocused={true}>
              <DefaultBaseLayers />
              {layers.map((layer) => (
                <div key={layer.id}>
                  <OpenLayersFeatureLayer
                    featureCollection={
                      !layer.isInEditMode ? layer.geojson : undefined
                    }
                    style={genericOpenLayersFeatureStyle}
                    zIndex={1000}
                  />
                  <OpenLayersMapDraw
                    viewOnlyStyle={genericOpenLayersFeatureStyle}
                    selectedFeatureIndex={layer.selectedFeatureIndex || 0}
                    geojson={layer.geojson!}
                    drawMode={layer.drawMode as DrawModeValue}
                    isInEditMode={layer.isInEditMode || false}
                    updateGeojson={layer.updateGeojson}
                    exitDrawModeCallback={onExitDrawMode}
                  />
                </div>
              ))}
            </OpenLayersMapView>
          </div>
        </QueryClientProvider>
      }
    >
      {children}
    </StoryLayoutGrid>
  );
};
