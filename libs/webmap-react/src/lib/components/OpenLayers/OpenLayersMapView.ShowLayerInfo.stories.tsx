/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';

import type { Meta, StoryObj } from '@storybook/react';
import { publicLayers, defaultLayers } from '../../layers';
import { WebmapReactThemeProvider } from '../Providers/Providers';
import { OpenLayersMapView } from '.';
import { OpenLayersLayer } from './layers';

const meta: Meta<typeof OpenLayersMapView> = {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
export default meta;

type Story = StoryObj<typeof OpenLayersMapView>;

export const MapWithShowLayerInfoEnabled: Story = {
  render: (props) => (
    <WebmapReactThemeProvider>
      <div style={{ height: '100vh' }}>
        <OpenLayersMapView {...props}>
          <OpenLayersLayer
            {...publicLayers.baseLayer}
            debugMode
            zIndex={1010}
          />
          <OpenLayersLayer
            {...publicLayers.radarLayer}
            debugMode
            zIndex={1011}
          />
          <OpenLayersLayer
            {...publicLayers.harmonieRelativeHumidityPl}
            debugMode
            zIndex={1012}
          />
          <OpenLayersLayer
            {...defaultLayers.overLayer}
            debugMode
            zIndex={1013}
          />
        </OpenLayersMapView>
      </div>
    </WebmapReactThemeProvider>
  ),
};
MapWithShowLayerInfoEnabled.storyName = 'Map which shows layer information';
