/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { getWMLayerById } from '@opengeoweb/webmap';
import type { Meta, StoryObj } from '@storybook/react';
import { View } from 'ol';
import { boundingExtent, getCenter } from 'ol/extent';
import { PROJECTION } from '@opengeoweb/shared';
import { defaultLayers, publicLayers } from '../../layers';
import { LegendDialog } from '../Legend';
import { initializeOpenLayersProjections, OpenLayersMapView } from '.';
import { OpenLayersLayer } from './layers';

initializeOpenLayersProjections();
const meta: Meta<typeof OpenLayersMapView> = {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
export default meta;

type Story = StoryObj<typeof OpenLayersMapView>;

export const MapViewRadar: Story = {
  render: () => {
    const view = new View({
      projection: PROJECTION.EPSG_28992.value,
      zoom: 7,
      center: getCenter(
        boundingExtent([
          [-236275.338083, 106727.731651],
          [501527.918656, 900797.079725],
        ]),
      ),
    });

    return (
      <div style={{ height: '100vh' }}>
        <LegendDialog
          layers={[publicLayers.radarLayer]}
          isOpen={true}
          mapId=""
          onClose={(): void => {}}
        />
        <OpenLayersMapView view={view}>
          <OpenLayersLayer {...publicLayers.baseLayer} />
          <OpenLayersLayer
            {...publicLayers.radarLayer}
            onLayerReady={(layerId: string): void => {
              setTimeout(() => {
                const wmLayer = getWMLayerById(layerId);
                const layerProjectionForCode = wmLayer.getProjection(
                  view.getProjection().getCode(),
                );
                layerProjectionForCode?.bbox &&
                  view.fit(layerProjectionForCode.bbox.getExtent());
              }, 100);
            }}
          />
          <OpenLayersLayer {...defaultLayers.overLayer} />
        </OpenLayersMapView>
      </div>
    );
  },
};

MapViewRadar.storyName = 'MapView with radar data';
