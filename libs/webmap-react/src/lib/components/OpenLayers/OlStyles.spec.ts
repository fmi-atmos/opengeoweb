/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { geowebColorToOpenLayersColor } from './OlStyles';

describe('src/components/OpenLayers/OlStyles', () => {
  it('should return the color as-is if no opacity', () => {
    expect(geowebColorToOpenLayersColor('#123')).toBe('#123');
  });
  it('should give correct opacity for a 12 bit color', () => {
    expect(geowebColorToOpenLayersColor('#123', 0.5)).toBe('#1238');
  });
  it('should override opacity of a 16 bit rgba color', () => {
    expect(geowebColorToOpenLayersColor('#123e', 0.5)).toBe('#1238');
  });
  it('should give correct opacity for a 24 bit color', () => {
    expect(geowebColorToOpenLayersColor('#123456', 0.5)).toBe('#12345680');
  });
  it('should override opacity for a 32 bit rgba color', () => {
    expect(geowebColorToOpenLayersColor('#123456ee', 0.5)).toBe('#12345680');
  });
  it('should use opacity value of a 16 bit color as-is when no opacity given', () => {
    expect(geowebColorToOpenLayersColor('#9876')).toBe('#9876');
  });
  it('should use opacity value of a 32 bit color as-is when no opacity given', () => {
    expect(geowebColorToOpenLayersColor('#abcdef12')).toBe('#abcdef12');
  });
  it('should give minimum opacity for negative numbers (4 bit case)', () => {
    expect(geowebColorToOpenLayersColor('#111', -0.1)).toBe('#1110');
  });
  it('should give minimum opacity for zero opacity (4 bit case)', () => {
    expect(geowebColorToOpenLayersColor('#111', 0)).toBe('#1110');
  });
  it('should give maximum opacity for opacity of 1 (4 bit case)', () => {
    expect(geowebColorToOpenLayersColor('#111', 1.0)).toBe('#111f');
  });
  it('should give maximum opacity for numbers beyond 1 (4 bit case)', () => {
    expect(geowebColorToOpenLayersColor('#111', 1.1)).toBe('#111f');
  });
  it('should give minimum opacity for negative numbers (8 bit case)', () => {
    expect(geowebColorToOpenLayersColor('#111111', -0.1)).toBe('#11111100');
  });
  it('should give minimum opacity for zero opacity (8 bit case)', () => {
    expect(geowebColorToOpenLayersColor('#111111', 0)).toBe('#11111100');
  });
  it('should give maximum opacity for opacity of 1 (8 bit case)', () => {
    expect(geowebColorToOpenLayersColor('#111111', 1.0)).toBe('#111111ff');
  });
  it('should give maximum opacity for numbers beyond 1 (8 bit case)', () => {
    expect(geowebColorToOpenLayersColor('#111111', 1.1)).toBe('#111111ff');
  });
});
