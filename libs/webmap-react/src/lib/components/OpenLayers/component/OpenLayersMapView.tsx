/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React, { useEffect, useMemo, useRef, useState } from 'react';

import { Map, MapBrowserEvent, View } from 'ol';
import {
  MouseWheelZoom,
  defaults as defaultInteractions,
} from 'ol/interaction';

import 'ol/ol.css';

import MapContext from '../context/MapContext';
import {
  OPENLAYERS_MOUSEWHEEL_DURATION,
  OPENLAYERS_MOUSEWHEEL_TIMEOUT,
} from '../OpenLayersConstants';
import { MapMousePosition } from '../../MapMousePosition';
import { HoldShiftToZoomMessage } from '../../HoldShiftToZoomMessage/HoldShiftToZoomMessage';

export interface OpenLayersMapViewProps {
  view?: View;
  children?: React.ReactNode;
  isFocused?: boolean;
  isMouseWheelZoomEnabled?: boolean;
  passiveMap?: boolean;
  holdShiftToScroll?: boolean;
}

export const OpenLayersMapView: React.FC<OpenLayersMapViewProps> = ({
  view,
  isFocused = false,
  children,
  isMouseWheelZoomEnabled = true,
  passiveMap = false,
  holdShiftToScroll = false,
}: OpenLayersMapViewProps) => {
  const mapRef = useRef<HTMLDivElement>(null);
  const [map, setMap] = useState<Map | null>(null);
  const dispatchingKeyboardEventRef = useRef(false);

  const viewRef = useRef(
    new View({
      projection: 'EPSG:3857',
      zoom: 1,
      center: [0, 0],
    }),
  );
  const mapView = view || viewRef.current;

  useEffect(() => {
    if (!mapRef.current) {
      return (): void => {};
    }

    const mapObject: Map = new Map({
      interactions: defaultInteractions({
        dragPan: true,
        mouseWheelZoom: false,
      }),
      controls: [],
    });

    mapObject.setTarget(mapRef.current);

    if (passiveMap === true) {
      mapObject.getInteractions().forEach((i) => {
        i.setActive(false);
      });
    }
    setMap(mapObject);

    return (): void => {
      mapObject.setTarget(undefined);
    };
  }, [mapRef, passiveMap]);

  // Propagate key events to the DIV containing OpenLayers
  React.useEffect(() => {
    if (!isFocused) {
      return (): void => {};
    }

    const handleKeyPress = (event: KeyboardEvent): void => {
      if (
        !dispatchingKeyboardEventRef.current &&
        mapRef.current &&
        document.activeElement === document.body &&
        ['+', '-', 'ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown'].includes(
          event.key,
        )
      ) {
        const newEvent = new KeyboardEvent(event.type, event);
        newEvent.preventDefault();

        dispatchingKeyboardEventRef.current = true;
        mapRef.current.dispatchEvent(newEvent);
        dispatchingKeyboardEventRef.current = false;
      }
    };

    window.addEventListener('keydown', handleKeyPress);
    window.addEventListener('keyup', handleKeyPress);
    return (): void => {
      window.removeEventListener('keydown', handleKeyPress);
      window.removeEventListener('keyup', handleKeyPress);
    };
  }, [mapRef, isFocused]);

  const isMouseWheelZoomEnabledAndIsNotPassiveMap =
    isMouseWheelZoomEnabled && !passiveMap;
  useEffect(() => {
    if (!map || !isMouseWheelZoomEnabledAndIsNotPassiveMap) {
      return (): void => {};
    }
    const noModifierKeysGeoWeb = (
      mapBrowserEvent: MapBrowserEvent<KeyboardEvent | MouseEvent | TouchEvent>,
    ): boolean => {
      const { originalEvent } = mapBrowserEvent;
      return !originalEvent.altKey &&
        !(originalEvent.metaKey || originalEvent.ctrlKey) &&
        holdShiftToScroll
        ? originalEvent.shiftKey
        : !originalEvent.shiftKey;
    };

    const mouseWheelZoom = new MouseWheelZoom({
      condition: noModifierKeysGeoWeb,
      duration: OPENLAYERS_MOUSEWHEEL_DURATION,
      timeout: OPENLAYERS_MOUSEWHEEL_TIMEOUT,
    });
    map.addInteraction(mouseWheelZoom);
    return (): void => {
      map.removeInteraction(mouseWheelZoom);
    };
  }, [map, isMouseWheelZoomEnabledAndIsNotPassiveMap, holdShiftToScroll]);

  useEffect(() => {
    if (!map || !mapView || !mapView.isDef()) {
      return;
    }
    map.setView(mapView);
    map.changed();
    mapView.changed();
  }, [mapView, map]);
  const memoizedMapContext = useMemo(() => ({ map }), [map]);

  return (
    <MapContext.Provider value={memoizedMapContext}>
      <div
        ref={mapRef}
        data-testid="openlayers-mapview"
        role="application"
        aria-label="map"
        style={{ width: '100%', height: '100%', position: 'relative' }}
      >
        {children}
        <MapMousePosition />
        {holdShiftToScroll && isMouseWheelZoomEnabled && (
          <HoldShiftToZoomMessage />
        )}
      </div>
    </MapContext.Provider>
  );
};
