/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { View } from 'ol';
import { Stroke, Style } from 'ol/style';

import { QueryClientProvider } from '@tanstack/react-query';
import { wmsQueryClient } from '@opengeoweb/webmap';

import { Geometry, LineString, Point } from 'ol/geom';
import { Switch, Typography } from '@mui/material';
import { Grid } from '@mui/system';
import { FeatureLike } from 'ol/Feature';
import { XYZLayer } from './layers/XYZLayer';
import { ClickOnMapTool } from './tools/ClickOnMapTool';
import { OpenLayersMapView } from './component/OpenLayersMapView';

export default {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
const projection = 'EPSG:3857';

const style = (feature: FeatureLike, resolution: number): Style[] => {
  const crosshairRadius = resolution * 8;

  const crosshair = (xMod: number, yMod: number): Style =>
    new Style({
      stroke: new Stroke({
        color: 'black',
        width: 0.75,
      }),
      geometry: (f: FeatureLike): Geometry => {
        const coords = (f.getGeometry() as Point).getCoordinates();
        const coordinates = [
          [coords[0] - xMod, coords[1] - yMod],
          [coords[0] + xMod, coords[1] + yMod],
        ];
        return new LineString(coordinates);
      },
    });
  return [crosshair(crosshairRadius, 0), crosshair(0, crosshairRadius)];
};

export const ClickOnMapToolExample = (): React.ReactNode => {
  const [coords, setCoords] = React.useState<number[]>([]);

  const [isActive, setIsActive] = React.useState(true);
  const [isVisible, setIsVisible] = React.useState(true);

  const view = React.useMemo(() => {
    return new View({
      projection,
      zoom: 7,
      center: [576423.573584, 6818457.618608],
    });
  }, []);

  return (
    <QueryClientProvider client={wmsQueryClient}>
      <div style={{ height: '100vh' }}>
        <OpenLayersMapView view={view} isFocused={true}>
          <XYZLayer
            urlTemplate="https://geoweb-maps-assets.pmc.knmi.cloud/WorldMap_Light_Grey_Canvas/EPSG3857/{z}/{x}/{y}.png"
            layerOptions={{
              zIndex: 1,
              opacity: 1,
            }}
            layerProps={{
              enabled: true,
            }}
          />
          <ClickOnMapTool
            isActive={isActive}
            isVisible={isVisible}
            pointStyle={style}
            pointSelected={(coords) => setCoords(coords)}
          />
        </OpenLayersMapView>
        <div
          style={{
            position: 'absolute',
            bottom: 2,
            right: 4,
          }}
        >
          <Grid container alignItems="left" direction="row">
            <Switch
              checked={isActive}
              onChange={(_evt, state) => setIsActive(state)}
            />
            <Typography>isActive</Typography>
          </Grid>
          <Grid container alignItems="left" direction="row">
            <Switch
              checked={isVisible}
              onChange={(_evt, state) => setIsVisible(state)}
            />
            <Typography>isVisible</Typography>
          </Grid>
          <Typography>
            {coords.length > 0
              ? `Coords: ${coords.map((c) => c.toFixed(2)).join(',')}`
              : 'click on map'}
          </Typography>
        </div>
      </div>
    </QueryClientProvider>
  );
};

ClickOnMapToolExample.storyName = 'OpenLayers ClickOnMap example';
