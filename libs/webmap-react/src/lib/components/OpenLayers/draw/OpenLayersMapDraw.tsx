/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import { Draw, Interaction, Modify, Snap, Translate } from 'ol/interaction';
import OlGeoJSON from 'ol/format/GeoJSON';
import { createBox, DrawEvent, GeometryFunction } from 'ol/interaction/Draw';

import { ModifyEvent } from 'ol/interaction/Modify';
import { Collection, Feature } from 'ol';
import { Geometry, LineString, MultiPoint, Polygon } from 'ol/geom';
import Style, { StyleLike } from 'ol/style/Style';
import { Type } from 'ol/geom/Geometry';
import { always, never } from 'ol/events/condition';
import { extend, Extent } from 'ol/extent';
import { FeatureLike } from 'ol/Feature';
import RenderFeature from 'ol/render/Feature';
import MapContext from '../context/MapContext';
import {
  DRAWMODE,
  DrawModeExitCallback,
  NEW_FEATURE_CREATED,
} from '../../MapDraw';
import { DrawModeValue, isGeoJSONGeometryEmpty } from '../../MapDrawTool';
import {
  drawStyles,
  FEATURE_FILL,
  FEATURE_STROKE_EDIT,
  FEATURE_VERTICE_HANDLE_IMAGE,
  FEATURE_VERTICE_IMAGE,
  modifyStyles,
} from '../OlStyles';

export interface OpenLayersMapDrawProps {
  geojson: GeoJSON.FeatureCollection;
  drawMode?: DrawModeValue;
  viewOnlyStyle: StyleLike;
  exitDrawModeCallback?: (reason: DrawModeExitCallback) => void;
  isInEditMode: boolean;
  selectedFeatureIndex: number;
  updateGeojson?: (geoJson: GeoJSON.FeatureCollection, text: string) => void;
}

interface DrawOperation {
  type: Type;
  geometryFunction?: GeometryFunction;
}

const createDrawOperation = (type: DrawModeValue): DrawOperation => {
  switch (type) {
    case 'BOX':
      return {
        type: 'Circle',
        geometryFunction: createBox(),
      };
    case 'POINT':
      return {
        type: 'Point',
      };
    case 'LINESTRING':
      return {
        type: 'LineString',
      };
    case 'POLYGON':
    default:
      return {
        type: 'Polygon',
      };
  }
};

// This function creates a box out of a modified polygon. It detects which corner was moved by comparing
// vertices between the two geometries. Then it draws a new box based on the moved vertice and the opposing
// vertice.
const createBoxBasedOnModifiedVertex = (
  modifiedGeometry: Polygon,
  originalGeometry: Polygon,
): Polygon => {
  const originalGeometryCoords = originalGeometry.getCoordinates()[0];

  const draggedCornerIndex = modifiedGeometry
    .getCoordinates()[0]
    .findIndex((coords, idx) => {
      return (
        originalGeometryCoords[idx][0] !== coords[0] ||
        originalGeometryCoords[idx][1] !== coords[1]
      );
    });
  if (draggedCornerIndex === -1) {
    return originalGeometry;
  }
  const draggedCoords =
    modifiedGeometry.getCoordinates()[0][draggedCornerIndex];
  const opposingCoords =
    modifiedGeometry.getCoordinates()[0][(draggedCornerIndex + 2) % 4];

  const box: Extent = extend(
    [draggedCoords[0], draggedCoords[1], draggedCoords[0], draggedCoords[1]],
    [
      opposingCoords[0],
      opposingCoords[1],
      opposingCoords[0],
      opposingCoords[1],
    ],
  );

  const boxPolygon = new Polygon([
    [
      [box[0], box[1]],
      [box[2], box[1]],
      [box[2], box[3]],
      [box[0], box[3]],
      [box[0], box[1]],
    ],
  ]);

  return boxPolygon;
};

const modifyHandleStyle = new Style({
  image: FEATURE_VERTICE_HANDLE_IMAGE,
});

const boxModificationStyle = [
  new Style({
    fill: FEATURE_FILL,
    stroke: FEATURE_STROKE_EDIT,
    geometry: (feature: FeatureLike): Geometry | RenderFeature | undefined => {
      const originalGeometry = feature?.get('originalGeometry') as Polygon;
      if (originalGeometry) {
        return createBoxBasedOnModifiedVertex(
          feature.getGeometry() as Polygon,
          originalGeometry,
        );
      }
      return feature.getGeometry();
    },
  }),
  new Style({
    image: FEATURE_VERTICE_IMAGE,
    geometry: (feature: FeatureLike): MultiPoint | undefined => {
      const originalGeometry = feature?.get('originalGeometry') as Polygon;
      const geom = originalGeometry
        ? createBoxBasedOnModifiedVertex(
            feature.getGeometry() as Polygon,
            originalGeometry,
          )
        : feature?.getGeometry();

      if (geom && geom.getType() === 'Polygon') {
        const coordinates = (geom as Polygon).getCoordinates()[0];
        return new MultiPoint(coordinates);
      }
      if (geom && geom.getType() === 'LineString') {
        const coordinates = (geom as LineString).getCoordinates();
        return new MultiPoint(coordinates);
      }
      return undefined;
    },
  }),
];

const selectionTypeToDrawModeValue = (
  selectionType:
    | 'box'
    | 'fir'
    | 'poly'
    | 'polygon'
    | 'point'
    | 'linestring'
    | 'custom-line'
    | 'delete',
): DrawModeValue => {
  switch (selectionType) {
    case 'box':
      return DRAWMODE.BOX;

    case 'point':
      return DRAWMODE.POINT;

    case 'linestring':
    case 'custom-line':
      return DRAWMODE.LINESTRING;

    case 'delete':
      return 'DELETE';

    case 'fir':
    case 'poly':
    case 'polygon':
    default:
      return DRAWMODE.POLYGON;
  }
};

/**
 * Integrates with OpenLayers Draw and Modify (+Snap) interactions, to implement feature drawing
 * and modification behaviour.
 *
 * There is a corner case where this fails: the user will only be able to edit the existing
 * feature (not add) when there are 1) multiple features in the geojson, and 2) the
 * selectedFeatureIndex points to a feature with of the same type as drawMode is going to draw. To
 * fix this properly, it would be better that the surrounding code would add a placeholder feature
 * in the geojson and then activate drawing at that feature index. This means the openlayers version
 * works slightly differently from the WebMapJS version, but since the multi-feature edit feature
 * is not really a requirement for any production workflows, this functionality was left as is.
 */
export const OpenLayersMapDraw: React.FC<OpenLayersMapDrawProps> = ({
  isInEditMode,
  drawMode,
  viewOnlyStyle,
  geojson,
  selectedFeatureIndex,
  exitDrawModeCallback,
  updateGeojson,
}: OpenLayersMapDrawProps) => {
  const { map } = React.useContext(MapContext);

  React.useEffect(() => {
    if (!map || !isInEditMode) {
      return (): void => {};
    }

    const selectionType =
      geojson?.features[selectedFeatureIndex]?.properties?.selectionType;
    const actualDrawMode =
      drawMode || selectionTypeToDrawModeValue(selectionType);

    const drawOperation = createDrawOperation(actualDrawMode);
    const drawStyle = drawStyles[actualDrawMode];
    const modifyStyle =
      actualDrawMode !== DRAWMODE.BOX
        ? modifyStyles[actualDrawMode]
        : boxModificationStyle;

    const geojsonFormatter = new OlGeoJSON({
      featureProjection: map.getView().getProjection(),
    });

    const allFeatures = geojson ? geojsonFormatter.readFeatures(geojson) : [];

    const viewOnlyFeatures = allFeatures.filter(
      (_f, i) => i !== selectedFeatureIndex,
    );
    const viewOnlySource = new VectorSource({ features: viewOnlyFeatures });
    const viewOnlyLayer = new VectorLayer({
      source: viewOnlySource,
      style: viewOnlyStyle,
      zIndex: 1000000 - 1,
    });
    const editFeatureCollection = new Collection([
      allFeatures[selectedFeatureIndex],
    ]);

    const source = new VectorSource({ features: editFeatureCollection });

    const selectedFeature = geojson
      ? geojson.features[selectedFeatureIndex]
      : null;

    const selectedFeatureHasGeometry =
      selectedFeature && !isGeoJSONGeometryEmpty(selectedFeature);

    const layer = new VectorLayer({
      source,
      style: modifyStyle,
      zIndex: 1000000,
    });

    map.addLayer(layer);
    map.addLayer(viewOnlyLayer);

    const interactions: Interaction[] = [];

    const updateFeature = (feature: Feature<Geometry>): void => {
      const obj = geojsonFormatter.writeFeatureObject(feature);

      // copy properties
      obj.properties = {
        ...selectedFeature?.properties,
      };

      const newGeoJson = {
        ...geojson,
        features: [
          ...geojson.features.slice(0, selectedFeatureIndex),
          obj,
          ...geojson.features.slice(selectedFeatureIndex + 1),
        ],
      };
      updateGeojson && updateGeojson(newGeoJson, NEW_FEATURE_CREATED);
    };

    // Draw functionality, for drawing new polygons/linestrings/boxes/markers
    const draw = new Draw({
      source,
      style: drawStyle,
      ...drawOperation,
    });

    draw.on('drawabort', () => {
      exitDrawModeCallback && exitDrawModeCallback('escaped');
    });

    draw.on('drawend', (evt: DrawEvent): void => {
      updateFeature(evt.feature);
      exitDrawModeCallback && exitDrawModeCallback('doubleClicked');
    });

    // Modifying is a combination of Modify and Translate interactions
    const modify = new Modify({
      features: editFeatureCollection,
      insertVertexCondition: actualDrawMode !== DRAWMODE.BOX ? always : never,
      style: modifyHandleStyle,
    });

    if (actualDrawMode === DRAWMODE.BOX) {
      // Box is a special case:
      // - Use a custom style to present the user with a box when dragging (even though the temp polygon is changing against box conditions)
      // - When modification is done, define the box using the dragged corner and its opposite corner

      modify.on('modifystart', (evt) => {
        const feature = evt.features.getArray()[0];
        feature.set('originalGeometry', feature.getGeometry()?.clone(), true);
      });

      modify.on('modifyend', (evt: ModifyEvent) => {
        const feature = evt.features.getArray()[0];

        const boxPolygon = createBoxBasedOnModifiedVertex(
          feature.getGeometry() as Polygon,
          feature.get('originalGeometry') as Polygon,
        );
        feature.setGeometry(boxPolygon);

        updateFeature(feature);
      });
    } else {
      modify.on('modifyend', (evt: ModifyEvent) => {
        updateFeature(evt.features.getArray()[0]);
      });
    }

    const translate = new Translate({
      features: editFeatureCollection,
    });

    translate.on('translateend', (evt: ModifyEvent) => {
      updateFeature(evt.features.getArray()[0]);
    });

    const abortOnEsc = (evt: KeyboardEvent): void => {
      if (evt.key === 'Escape') {
        evt.preventDefault();
        draw.finishDrawing();
        if (selectedFeatureHasGeometry) {
          exitDrawModeCallback && exitDrawModeCallback('escaped');
        } else {
          draw.abortDrawing();
        }
      }
    };

    if (selectedFeatureHasGeometry) {
      interactions.push(translate, modify);
    } else {
      interactions.push(draw);
    }

    interactions.push(new Snap({ source: viewOnlySource }));

    window.addEventListener('keydown', abortOnEsc);

    interactions.forEach((i) => map.addInteraction(i));

    return (): void => {
      window.removeEventListener('keydown', abortOnEsc);
      interactions.forEach((i) => map.removeInteraction(i));
      map.removeLayer(layer);
      map.removeLayer(viewOnlyLayer);
    };
  }, [
    map,
    isInEditMode,
    geojson,
    viewOnlyStyle,
    selectedFeatureIndex,
    exitDrawModeCallback,
    updateGeojson,
    drawMode,
  ]);

  return null;
};
