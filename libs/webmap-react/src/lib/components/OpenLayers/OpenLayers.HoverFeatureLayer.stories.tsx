/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { View } from 'ol';
import GeoJSON from 'ol/format/GeoJSON';
import { Circle, Fill, Style, Text } from 'ol/style';

import { QueryClientProvider } from '@tanstack/react-query';
import { wmsQueryClient } from '@opengeoweb/webmap';

import { StyleFunction } from 'ol/style/Style';
import Feature, { FeatureLike } from 'ol/Feature';
import { Grid } from '@mui/system';
import { Typography } from '@mui/material';
import { Geometry } from 'ol/geom';
import { XYZLayer } from './layers/XYZLayer';
import { FeatureLayer } from './layers/FeatureLayer';
import { OpenLayersMapView } from './component/OpenLayersMapView';
import { HoverSelect } from './utils/types';

export default {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
const styleFunction: StyleFunction = (f: FeatureLike): Style | Style[] => {
  const hover = !!f.get('hover');
  const radius = hover ? 30 : 10;
  const colors: Record<string, string> = {
    A: '#99000066',
    B: '#00990066',
    C: '#00009966',
  };
  return [
    new Style({
      image: new Circle({
        radius,
        fill: new Fill({ color: colors[f.getProperties().name as string] }),
      }),
    }),
    new Style({
      text: new Text({
        font: '16px Arial',
        text: f.getProperties().name,
      }),
    }),
  ];
};

export const HoverFeatureLayerMap = (): React.ReactNode => {
  const projection = 'EPSG:3857';

  const [hoveredFeature, setHoverLinkedFeature] =
    React.useState<Feature<Geometry>>();

  const view = React.useMemo(() => {
    return new View({
      projection,
      zoom: 7,
      center: [576423.573584, 6818457.618608],
    });
  }, []);

  const features = React.useMemo(() => {
    const xoffset = 0.27;

    const geojson = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            coordinates: [5.1776803090635894, 52.101177319887455],
            type: 'Point',
          },
          properties: {
            name: 'B',
          },
        },
        {
          type: 'Feature',
          geometry: {
            coordinates: [5.1776803090635894 + xoffset, 52.101177319887455],
            type: 'Point',
          },
          properties: {
            name: 'C',
          },
        },
        {
          type: 'Feature',
          geometry: {
            coordinates: [5.1776803090635894 - xoffset, 52.101177319887455],
            type: 'Point',
          },
          properties: {
            name: 'A',
          },
        },
      ],
    };

    const converter = new GeoJSON({ featureProjection: projection });
    return [converter.readFeatures(geojson)].flat();
  }, []);

  const hoverSelect = React.useMemo<HoverSelect>(
    () => ({
      active: true,
      onHover: setHoverLinkedFeature,
    }),
    [],
  );

  return (
    <QueryClientProvider client={wmsQueryClient}>
      <div style={{ height: '100vh' }}>
        <OpenLayersMapView view={view} isFocused={true}>
          <XYZLayer
            urlTemplate="https://geoweb-maps-assets.pmc.knmi.cloud/WorldMap_Light_Grey_Canvas/EPSG3857/{z}/{x}/{y}.png"
            layerOptions={{
              zIndex: 1,
              opacity: 1,
            }}
            layerProps={{
              enabled: true,
            }}
          />
          <FeatureLayer
            features={features}
            hoverSelect={hoverSelect}
            style={styleFunction}
            zIndex={2}
          />
        </OpenLayersMapView>
      </div>
      <div
        style={{
          position: 'absolute',
          bottom: 2,
          right: 4,
        }}
      >
        <Grid container alignItems="left" direction="row">
          <Typography>
            Hovering:{' '}
            {hoveredFeature ? hoveredFeature.getProperties().name : 'N/A'}
          </Typography>
        </Grid>
      </div>
    </QueryClientProvider>
  );
};

HoverFeatureLayerMap.storyName = 'OpenLayers FeatureLayer Hover example';
