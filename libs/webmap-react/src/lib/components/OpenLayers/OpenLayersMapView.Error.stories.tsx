/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import type { Meta, StoryObj } from '@storybook/react';
import { defaultLayers, publicLayers } from '../../layers';
import { OpenLayersMapView } from '.';
import { OpenLayersLayer } from './layers';

const meta: Meta<typeof OpenLayersMapView> = {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
export default meta;

type Story = StoryObj<typeof OpenLayersMapView>;

export const MapError: Story = {
  render: (props) => (
    <>
      <div
        style={{
          top: '10px',
          left: '50px',
          position: 'absolute',
          padding: '20px',
          background: '#DDD',
          zIndex: 10000,
        }}
        // eslint-disable-next-line i18next/no-literal-string
      >
        The console panel of your browser shows the error produced by this
        layer.
      </div>
      <div style={{ height: '100vh' }}>
        <OpenLayersMapView {...props}>
          <OpenLayersLayer {...publicLayers.baseLayer} />
          <OpenLayersLayer
            {...publicLayers.radarLayerWithError}
            // eslint-disable-next-line id-length
            onLayerError={(_, error): void => {
              console.warn(error);
            }}
          />
          <OpenLayersLayer {...defaultLayers.overLayer} />
        </OpenLayersMapView>
      </div>
    </>
  ),
  parameters: {
    docs: {
      description: {
        story:
          'The console panel of your browser shows the error produced by this layer.',
      },
    },
  },
};

MapError.storyName = 'Map with a layer which has an error';
