/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';

import { LayerType } from '@opengeoweb/webmap';
import type { Meta, StoryObj } from '@storybook/react';
import View from 'ol/View';
import { PROJECTION } from '@opengeoweb/shared';
import { defaultLayers, publicLayers } from '../../layers';

import { LegendDialog } from '../Legend';
import { OpenLayersMapView } from './component/OpenLayersMapView';
import { OpenLayersLayerProps, OpenLayersLayer } from './layers';

const meta: Meta<typeof OpenLayersMapView> = {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
export default meta;

type Story = StoryObj<typeof OpenLayersMapView>;

const vfxLayer: OpenLayersLayerProps = {
  service: 'https://wms.geonorge.no/skwms1/wms.sjokartraster2?',
  name: 'all',
  id: 'vfxLayer',
  layerType: LayerType.mapLayer,
};

export const MapViewVFX: Story = {
  args: {},
  render: (props) => {
    const view = new View({
      projection: PROJECTION.EPSG_3857.value,
      zoom: 3,
      center: [0, 15000000],
    });

    return (
      <div style={{ height: '100vh' }}>
        <LegendDialog
          layers={[vfxLayer]}
          isOpen={true}
          mapId=""
          onClose={(): void => {}}
        />
        <OpenLayersMapView {...props} view={view}>
          <OpenLayersLayer {...publicLayers.baseLayer} />
          <OpenLayersLayer {...vfxLayer} />
          <OpenLayersLayer {...defaultLayers.overLayer} />
        </OpenLayersMapView>
      </div>
    );
  },
};

MapViewVFX.storyName = 'MapView with VFX data';
