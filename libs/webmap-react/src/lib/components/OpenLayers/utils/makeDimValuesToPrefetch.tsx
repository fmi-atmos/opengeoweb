/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  WMJSDimension,
  WMInvalidDateValues,
  Dimension,
  WMLayer,
  getCorrectWMSDimName,
} from '@opengeoweb/webmap';
import { memoize, range } from 'lodash';
import { Timespan } from '../types/Timespan';
import { MAX_NUMBER_STEPS_IN_TIMESLIDER_TO_PREFETCH } from './constants';

export const makeDimValuesToPrefetch = (
  wmTimeDim: WMJSDimension,
  stepsToCheck: number[],
  isAnimating: boolean,
): string[] => {
  if (wmTimeDim) {
    if (stepsToCheck.length > 0) {
      const timePrefetchValuesSet = new Set<number>([]);
      stepsToCheck.forEach((t) => {
        if (!t) {
          return;
        }
        const closestValue = wmTimeDim.getClosestValueForTime(t);
        if (!WMInvalidDateValues.has(closestValue)) {
          timePrefetchValuesSet.add(new Date(closestValue).getTime());
        }
      });
      const currentTimeOfLayer = wmTimeDim.currentValue
        ? new Date(wmTimeDim.currentValue).getTime()
        : stepsToCheck[0];
      // Sort it according to the current time value
      const timePrefetchValuesArray = Array.from(timePrefetchValuesSet);

      const futurePrefetchValues = timePrefetchValuesArray.filter(
        (timeStepAsTime) => timeStepAsTime >= currentTimeOfLayer,
      );
      const pastPrefetchValues = timePrefetchValuesArray.filter(
        (timeStepAsTime) => timeStepAsTime < currentTimeOfLayer,
      );

      futurePrefetchValues.sort();
      isAnimating
        ? pastPrefetchValues.sort()
        : pastPrefetchValues.sort().reverse();

      const dimensionValuesToPrefetchCombined = [
        ...futurePrefetchValues,
        ...pastPrefetchValues,
      ];

      const dimValuesToPrefetch = dimensionValuesToPrefetchCombined.map(
        (timeStamp) => wmTimeDim.getClosestValueForTime(timeStamp),
      );

      // TODO: When animating with two maps, the not leading map does get the wrong prefetch values. Use the following to debug this
      // if (dimensionValuesToPrefetch.length === 0) {
      //   console.warn(
      //     'NOTHING TO PREFETCH',
      //     stepsToCheck.map((s) => {
      //       return new Date(s).toISOString();
      //     }),
      //     wmTimeDim.getFirstValue(),
      //     wmTimeDim.getLastValue(),
      //   );
      // }

      return dimValuesToPrefetch;
    }
  }
  return [];
};

export const getDimensionParamsForWMS = (
  dimensions: Dimension[] | undefined,
  wmLayer: WMLayer | null,
): Record<string, string> => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const dimProps: Record<string, string> = {};
  dimensions?.forEach((dimension) => {
    if (!dimension.name) {
      return;
    }
    // Take the closest WMLayer dimension value, if not available just use the already existing value
    const closestValue =
      wmLayer
        ?.getDimension(dimension.name)
        ?.getClosestValue(dimension.currentValue) || dimension.currentValue;

    dimProps[getCorrectWMSDimName(dimension.name)] = closestValue;
  });
  return dimProps;
};

/**
 * Calculates number of timesteps for given timespan
 * @param timespan Object of Timespan, with start/stop/end in milliseconds
 * @returns Timestams array in milliseconds
 */
const getTimeStepsToCheck = (timespan: Timespan): number[] => {
  if (timespan && timespan.start && timespan.end && timespan.step) {
    if (timespan.step <= 0 || timespan.start > timespan.end + timespan.step) {
      return [];
    }
    const numStepsToFetch =
      (timespan.end + timespan.step - timespan.start) / timespan.step;

    if (numStepsToFetch < 1) {
      return [];
    }

    if (numStepsToFetch > MAX_NUMBER_STEPS_IN_TIMESLIDER_TO_PREFETCH) {
      const msg = `too many steps to prefetch: ${numStepsToFetch}`;
      throw new RangeError(msg);
    }

    const stepsToCheck = range(
      timespan.start,
      timespan.end + timespan.step,
      timespan.step,
    );
    // Find out which dimension time values are available for prefetching
    if (stepsToCheck.length > MAX_NUMBER_STEPS_IN_TIMESLIDER_TO_PREFETCH) {
      const msg = `too many steps to prefetch: ${stepsToCheck.length}`;
      throw new RangeError(msg);
    }
    return stepsToCheck;
  }
  return [];
};

/**
 * Calculates number of timesteps for given timespan
 * @param timespan Object of Timespan, with start/stop/end in milliseconds
 * @returns Timestams array in milliseconds
 */
export const getTimeStepsToCheckMemoized = memoize(getTimeStepsToCheck);
