/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { containsExtent, equals, Extent } from 'ol/extent';
import ImageWrapper from 'ol/Image';
import { Projection } from 'ol/proj';
import LayerProperty from 'ol/layer/Property';
import ImageSource, { getRequestExtent } from 'ol/source/Image';
import ImageState from 'ol/ImageState';
import { fromResolutionLike } from 'ol/resolution';
import { debounce, isEqual } from 'lodash';
import { WorkerQueue } from './WorkerQueue';
import TimeAwareImageWrapper from './TimeAwareImageWrapper';
import { WorkerQueueJob } from '../types/WorkerQueue';
import { NUMBER_PREFETCH_WORKERS, MAX_NUMBER_TO_PREFETCH } from './constants';

export const initImageWrapper = (): TimeAwareImageWrapper => {
  const emptyButLoading = new TimeAwareImageWrapper([0, 0, 0, 0], 1, 1);

  // Set to loaded to indicate to ol that it can be displayed immediately
  emptyButLoading.setState(ImageState.LOADED);
  emptyButLoading.setImage(document.createElement('img'));
  return emptyButLoading;
};

interface TimeawareImageSourceOptions {
  wmsUrl: string;
  params?: Record<string, string>;
  layerName: string;
  dimProps?: Record<string, string>;
  styleName?: string | undefined;
  timeStampsToPrefetch?: string[];
  visible?: boolean;
}

interface InternalPrefetchOptions {
  that: TimeawareImageSource;
  extent: Extent;
  resolution: number;
  pixelRatio: number;
  projection: Projection;
}

interface PrefetchState {
  timeStampsToPrefetch: string[];
  extent: Extent;
  layerName: string;
  styleName: string;
  dimProperties: Record<string, string>;
}

export class TimeawareImageSource extends ImageSource {
  private _wmsUrl: string;
  private _extraWMSParams: Record<string, string>;
  private _timeStampsToPrefetch: string[];
  private _prefetchWorkers: WorkerQueue;
  private _wantedExtent!: Extent;
  private _wantedResolution!: number;
  private _previousPrefetchExtent: PrefetchState | null;
  private _dimProperties: Record<string, string>;
  private _imWrapperEmptyButLoading: TimeAwareImageWrapper;
  private _imWrapperForOlSource: TimeAwareImageWrapper;
  private _imWrapperForAltImage: TimeAwareImageWrapper;
  private _styleName: string;
  private _layerName: string;

  // Debounce prefetch to add a slight delay before prefetching commences
  private debouncedPrefetch = debounce(
    ({
      extent,
      resolution,
      pixelRatio,
      projection,
    }: InternalPrefetchOptions): void => {
      // If the extent has changed => trigger prefetching
      if (
        !this._previousPrefetchExtent ||
        !this._previousPrefetchExtent?.extent ||
        !equals(extent, this._previousPrefetchExtent.extent) ||
        this._previousPrefetchExtent.layerName !== this._layerName ||
        this._previousPrefetchExtent.styleName !== this._styleName ||
        this._previousPrefetchExtent.timeStampsToPrefetch !==
          this._timeStampsToPrefetch ||
        this._previousPrefetchExtent.dimProperties !== this._dimProperties
      ) {
        this._previousPrefetchExtent = {
          extent,
          layerName: this._layerName,
          styleName: this._styleName,
          timeStampsToPrefetch: this._timeStampsToPrefetch,
          dimProperties: this._dimProperties,
        };
        // Clear old jobs
        this._prefetchWorkers.clearJobs();
        this.prefetch(extent, resolution, pixelRatio, projection);
      }
    },
    500,
  );
  private _wantedPixelRatio!: number;
  private _wantedProjection!: Projection;
  private _imWrapperForPrefetch: TimeAwareImageWrapper;

  constructor({
    wmsUrl,
    timeStampsToPrefetch,
    layerName,
    styleName,
    dimProps,
    params,
    visible,
  }: TimeawareImageSourceOptions) {
    super({});
    this._wmsUrl = wmsUrl;
    this._extraWMSParams = { ...params };
    this._layerName = layerName;
    this._dimProperties = { ...dimProps };
    this._styleName = styleName || '';
    this._timeStampsToPrefetch = [...(timeStampsToPrefetch || [])];
    this._previousPrefetchExtent = null;
    this._prefetchWorkers = new WorkerQueue(NUMBER_PREFETCH_WORKERS);
    this.prefetch = this.prefetch.bind(this);
    this.hasImageForTimeValue = this.hasImageForTimeValue.bind(this);
    this.loadImageForTimeValue = this.loadImageForTimeValue.bind(this);

    this.debouncedPrefetch = this.debouncedPrefetch.bind(this);
    this.triggerPrefetch = this.triggerPrefetch.bind(this);
    this._imWrapperEmptyButLoading = initImageWrapper();
    this._imWrapperForOlSource = initImageWrapper();
    this._imWrapperForAltImage = initImageWrapper();
    this._imWrapperForPrefetch = initImageWrapper();
    this.set(LayerProperty.VISIBLE, !!visible);
  }

  private prefetch(
    extent: Extent,
    resolution: number,
    pixelRatio: number,
    projection: Projection,
  ): void {
    this._prefetchWorkers.clearJobs();
    const dimensionsToPrefetch = this._timeStampsToPrefetch;

    const jobs = dimensionsToPrefetch.slice(0, MAX_NUMBER_TO_PREFETCH).map(
      (dimValue): WorkerQueueJob => ({
        dimValue,
        cancelled: false,
        task: (): Promise<void> => {
          return this._imWrapperForPrefetch.loadAndResolve(
            extent,
            resolution,
            pixelRatio,
            projection,
            this._wmsUrl,
            this._extraWMSParams,
            this._layerName,
            this._styleName,
            { ...this._dimProperties, TIME: dimValue },
          );
        },
      }),
    );
    dimensionsToPrefetch?.length > 0 && this._prefetchWorkers.addJobs(jobs);
  }

  triggerPrefetch(
    extent: Extent,
    resolution: number,
    pixelRatio: number,
    projection: Projection,
  ): void {
    this.debouncedPrefetch({
      that: this,
      extent,
      resolution,
      pixelRatio,
      projection,
    });
  }

  getImage(
    extent: Extent,
    resolution: number,
    pixelRatio: number,
    projection: Projection,
  ): ImageWrapper {
    const requestExtent = getRequestExtent(extent, resolution, pixelRatio, 1);
    const requestResolution = this.findNearestResolution(resolution);
    const requestDimProperties = { ...this._dimProperties };

    const requestedImageHasWantedExtent =
      ((this._wantedExtent &&
        containsExtent(this._wantedExtent, requestExtent)) ||
        containsExtent(
          this._imWrapperForOlSource.getExtent(),
          requestExtent,
        )) &&
      ((this._wantedResolution &&
        fromResolutionLike(this._wantedResolution) === requestResolution) ||
        fromResolutionLike(this._imWrapperForOlSource.getResolution()) ===
          requestResolution);

    const hasExactImage =
      this._wantedExtent &&
      this._imWrapperForOlSource.hasImage(
        this._wantedExtent,
        this._wantedResolution,
        pixelRatio,
        projection,
        this._wmsUrl,
        this._extraWMSParams,
        this._layerName,
        this._styleName,
        requestDimProperties,
      );

    if (
      hasExactImage &&
      hasExactImage.isLoaded &&
      requestedImageHasWantedExtent
    ) {
      this._imWrapperForOlSource.setGeoReferencedImage(hasExactImage);
      return this._imWrapperForOlSource;
    }

    // Load the newly requested image
    void this._imWrapperForOlSource
      .loadAndResolve(
        requestExtent,
        requestResolution,
        pixelRatio,
        projection,
        this._wmsUrl,
        this._extraWMSParams,
        this._layerName,
        this._styleName,
        requestDimProperties,
      )
      .catch(() => {
        // TODO, enable error handling
        // console.error(e);
      })
      .finally(() => {
        this._wantedExtent = requestExtent;
        this._wantedResolution = requestResolution;
        this._wantedPixelRatio = pixelRatio;
        this._wantedProjection = projection;
        // Loading finished, tell openlayers that we need to change. It will then call getImage again, which will display this loaded image.
        this.changed();

        this.triggerPrefetch(
          requestExtent,
          requestResolution,
          pixelRatio,
          projection,
        );
      });

    // While we are loading, check if there is an alternative image available
    const altImage =
      this._wantedExtent &&
      this._imWrapperForOlSource.getAlternativeImage(
        this._wantedExtent,
        this._wantedResolution,
        pixelRatio,
        projection,
        this._wmsUrl,
        this._extraWMSParams,
        this._layerName,
        this._styleName,
        requestDimProperties,
      );

    if (altImage && altImage.isLoaded) {
      // A similar image is available for display, show it
      this._imWrapperForAltImage.setGeoReferencedImage(altImage);
      return this._imWrapperForAltImage;
    }

    // This thing is has nothing to show yet.
    return this._imWrapperEmptyButLoading;
  }

  setDimProps(params: Record<string, string>): void {
    if (isEqual(this._dimProperties, params)) {
      return;
    }
    this._dimProperties = { ...params };
    this.changed();
  }

  /**
   * Check if an image is available and loaded for requested date.
   * @param timeStampsToPrefetch
   * @returns
   */
  hasImageForTimeValue(timeStampsToPrefetch: string): boolean {
    if (this._wantedExtent?.length !== 4) {
      return false;
    }
    const dimPropWithRequestedDate = { ...this._dimProperties };
    dimPropWithRequestedDate['TIME'] = timeStampsToPrefetch;
    const im = this._imWrapperForOlSource.hasImage(
      this._wantedExtent,
      this._wantedResolution,
      this._wantedPixelRatio,
      this._wantedProjection,
      this._wmsUrl,
      this._extraWMSParams,
      this._layerName,
      this._styleName,
      dimPropWithRequestedDate,
    );
    if (im === false || im?.isLoaded === false) {
      return false;
    }
    return true;
  }

  loadImageForTimeValue(timeStampsToPrefetch: string): void {
    if (this._wantedExtent?.length !== 4) {
      return;
    }
    const dimPropWithRequestedDate = { ...this._dimProperties };
    dimPropWithRequestedDate['TIME'] = timeStampsToPrefetch;

    void this._imWrapperForOlSource
      .loadAndResolve(
        this._wantedExtent,
        this._wantedResolution,
        this._wantedPixelRatio,
        this._wantedProjection,
        this._wmsUrl,
        this._extraWMSParams,
        this._layerName,
        this._styleName,
        dimPropWithRequestedDate,
      )
      .catch((e) => {
        console.warn(e);
      });
  }

  setStyle(styleName: string): void {
    if (this._styleName === styleName) {
      return;
    }
    this._styleName = styleName;
    this.changed();
  }

  setLayerName(layerName: string): void {
    if (this._layerName === layerName) {
      return;
    }
    this._layerName = layerName;
    this.changed();
  }

  setTimeValuesValuesToPrefetch(timeStampsToPrefetch: string[]): void {
    this._timeStampsToPrefetch = timeStampsToPrefetch;
    this._wantedExtent &&
      this.triggerPrefetch(
        this._wantedExtent,
        this._wantedResolution,
        this._wantedPixelRatio,
        this._wantedProjection,
      );
  }
}
