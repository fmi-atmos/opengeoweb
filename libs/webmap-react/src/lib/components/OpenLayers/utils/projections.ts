/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import proj4 from 'proj4';
import { register } from 'ol/proj/proj4';

import { LayerProps, WMProj4Defs } from '@opengeoweb/webmap';
import { useContext, useEffect, useState } from 'react';
import { unByKey } from 'ol/Observable';
import MapContext from '../context/MapContext';

export const initializeOpenLayersProjections = (): void => {
  WMProj4Defs.forEach(([code, value]) => {
    proj4.defs(code, value);
  });

  register(proj4);
};

/**
 * Returns a preferred projection to use with a particular layer. It will search the layer props for
 * a supported srs that either matches the current view srs or an srs that geoweb has reprojection math
 * for. The first priority is the view srs and then any of the other supported srs. If no shared SRS
 * is found, the function will return EPSG:3857 since that is almost always supported. There may be
 * layers for which we do not have LayerProps for (for example layers not visible in a GetCapabilities
 * document for a service) and thus defaulting to EPSG:3857 makes sense.
 *
 * @param viewSrs
 * @param layerProps
 * @returns
 */
export const selectPreferredWMSProjection = (
  viewSrs: string,
  layerProps: LayerProps | null,
): string => {
  const preferredSrs = [viewSrs];

  WMProj4Defs.forEach(([code]) => {
    preferredSrs.push(code);
  });

  const layerSupportedPreferredSrss = preferredSrs.filter((preferred) => {
    return layerProps && layerProps.crs?.find((crs) => crs.name === preferred);
  });

  if (layerSupportedPreferredSrss.length > 0) {
    return layerSupportedPreferredSrss[0];
  }
  return 'EPSG:3857';
};

export const useProjection = (): string | undefined => {
  const { map } = useContext(MapContext);

  const [projection, setProjection] = useState<string>();

  useEffect(() => {
    if (!map) {
      return (): void => {};
    }

    setProjection(map.getView()?.getProjection().getCode());

    const eventKey = map.on('change:view', (): void => {
      setProjection(map.getView()?.getProjection().getCode());
    });

    return (): void => {
      unByKey(eventKey);
    };
  }, [map]);

  return projection;
};
