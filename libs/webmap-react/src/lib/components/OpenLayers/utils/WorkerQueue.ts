/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { without } from 'lodash';
import { WorkerQueueJob } from '../types/WorkerQueue';

export class WorkerQueue {
  maxConcurrency: number;
  tasksInProcess: WorkerQueueJob[] = [];
  taskQueue: WorkerQueueJob[] = [];
  private disposed = false;

  constructor(concurrency: number) {
    this.maxConcurrency = concurrency;
  }

  dispose(): void {
    this.disposed = true;
  }

  ensureWork(): void {
    while (
      !this.disposed &&
      this.taskQueue.length > 0 &&
      this.tasksInProcess.length < this.maxConcurrency
    ) {
      this._startJob();
    }
  }

  clearJobs(): void {
    this.taskQueue.forEach((job) => {
      // eslint-disable-next-line no-param-reassign
      job.cancelled = true;
    });
    this.tasksInProcess.forEach((job) => {
      // eslint-disable-next-line no-param-reassign
      job.cancelled = true;
    });
    this.taskQueue = [];
  }

  addJobs(work: WorkerQueueJob[]): void {
    this.taskQueue = [...this.taskQueue, ...work];

    this.ensureWork();
  }

  addJobsWithoutReorder(work: WorkerQueueJob[]): void {
    this.taskQueue = [...this.taskQueue, ...work];
    this.ensureWork();
  }

  private _startJob(): void {
    const job = this.taskQueue.shift();
    if (!job) {
      throw Error(
        'programming error: _startJob() called with no jobs in queue',
      );
    }
    this.tasksInProcess.push(job);

    job
      .task()
      .then((finalise) => {
        if (!job.cancelled && finalise) {
          finalise();
        }
      })
      .catch((e) => {
        window.console.error(e);
      })
      .finally(() => {
        this.tasksInProcess = without(this.tasksInProcess, job);
        this.ensureWork();
      });
  }
}
