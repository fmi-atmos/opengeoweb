/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Extent } from 'ol/extent';
import ImageState from 'ol/ImageState';
import ImageWrapper from 'ol/Image';
import { getRequestExtent } from 'ol/source/Image';
import { getImageSrc, getRequestParams, ServerType } from 'ol/source/wms';
import { Projection } from 'ol/proj';

import { WMImageStore } from '@opengeoweb/webmap';
import {
  NUM_IMAGES_TO_CACHE,
  TIMEAWAREIMAGEWRAPPER_IMAGEDIDNOTLOADSUCCESFULLY,
  WMS_EXTENT_RATIO_TO_VIEWPORT,
} from './constants';

export const openLayersGetMapImageStore = new WMImageStore(
  NUM_IMAGES_TO_CACHE,
  {
    id: 'ol',
  },
);

interface TimeAwareGeoreferencedImageSrc {
  src: string;
  extent: Extent;
  resolution: number;
}
interface TimeAwareGeoreferencedImage {
  el: HTMLImageElement;
  extent: Extent;
  resolution: number;
  isLoaded: boolean;
  hasError: boolean;
  isLoadedWithoutErrors: boolean;
  isLoading: boolean;
}

interface RejectImageLoad {
  reason: string;
  url: string;
}

class TimeAwareImageWrapper extends ImageWrapper {
  private _extentToLoad: Extent;
  private _resolutionToLoad: number;
  private _pixelRatioToLoad: number;
  private _paramsToLoad: Record<string, string> = {};
  private _dimPropsToLoad: Record<string, string> = {};
  private _getMapUrl: string | null = null;
  private projection: Projection | null = null;
  private _resolvers: {
    resolve: () => void;
    reject: (reason: RejectImageLoad) => void;
  }[] = [];
  private _styleName = 'default';
  private _layerName: string | null = null;

  constructor(extent: Extent, resolution: number, pixelRatio: number) {
    super(extent, resolution, pixelRatio, ImageState.IDLE);
    this.extent = extent;
    this.resolution = resolution;
    this.state = ImageState.IDLE;
    this._extentToLoad = extent;
    this._pixelRatioToLoad = pixelRatio;
    this._resolutionToLoad = resolution;
    this._resolveAll = this._resolveAll.bind(this);
    this._rejectAll = this._rejectAll.bind(this);

    this.getWMSGetMapRequestURL = this.getWMSGetMapRequestURL.bind(this);
    this.hasImage = this.hasImage.bind(this);
    this.load = this.load.bind(this);
    this.loadAndResolve = this.loadAndResolve.bind(this);
    this.setGeoReferencedImage = this.setGeoReferencedImage.bind(this);
  }

  public getWMSGetMapRequestURL(
    extentToLoad: Extent,
    resolutionToLoad: number,
    __pixelRatio: number,
    projection: Projection | null,
    getMapUrl: string | null,
    params: Record<string, string>,
    layerName: string | null,
    styleName: string,
    dimProps: Record<string, string>,
  ): TimeAwareGeoreferencedImageSrc {
    const pixelRatio = 1; // TODO?
    if (!getMapUrl || !layerName) {
      return { src: '', extent: this.extent, resolution: 1 };
    }
    const options = {
      hidpi: false,
      serverType: 'mapserver' as ServerType,
    };

    const resolution = resolutionToLoad;

    const requestResolution = Array.isArray(resolution)
      ? resolution[0]
      : resolution || 1;

    const hidpi = options.hidpi === undefined ? true : options.hidpi;
    const newPixelRatio =
      pixelRatio !== 1 && (!hidpi || options.serverType === undefined)
        ? 1
        : pixelRatio;

    // Load 1.2^2 % larger WMS extents than the current view
    const ratio = WMS_EXTENT_RATIO_TO_VIEWPORT;
    const extent = extentToLoad;
    const newExtent = getRequestExtent(
      extent,
      requestResolution!,
      newPixelRatio,
      ratio,
    );

    const src = getImageSrc(
      newExtent,
      resolution as unknown as number,
      pixelRatio,
      projection!,
      getMapUrl,
      getRequestParams(
        {
          ...params,
          ...dimProps,
          LAYERS: layerName,
          STYLES: styleName,
        },
        'GetMap',
      ),
      options.serverType!,
    );
    return {
      src,
      extent: newExtent,
      resolution: resolution as unknown as number,
    };
  }

  public getAlternativeImage(
    extent: Extent,
    resolution: number,
    pixelRatio: number,
    projection: Projection,
    getMapUrl: string,
    params: Record<string, string>,
    layerName: string,
    styleName: string,
    dimsProps: Record<string, string>,
  ): TimeAwareGeoreferencedImage | false {
    const {
      src,
      extent: newExtent,
      resolution: newResolution,
    } = this.getWMSGetMapRequestURL(
      extent,
      resolution,
      pixelRatio,
      projection,
      getMapUrl,
      params,
      layerName,
      styleName,
      dimsProps,
    );
    const geoRef = openLayersGetMapImageStore.getAltGeoReferencedImage(
      src,
      newExtent,
      newResolution,
    );

    if (geoRef) {
      return {
        el: geoRef.getElement(),
        extent: geoRef.geoReference!.extent,
        resolution: geoRef.geoReference!.resolution,
        isLoaded: geoRef.isLoaded(),
        isLoadedWithoutErrors: geoRef.isLoadedWithoutErrors(),
        hasError: geoRef.hasError(),
        isLoading: geoRef.isLoading(),
      };
    }

    return false;
  }

  public hasImage(
    extent: Extent,
    resolution: number,
    pixelRatio: number,
    projection: Projection,
    getMapUrl: string,
    params: Record<string, string>,
    layerName: string,
    styleName: string,
    dimsProps: Record<string, string>,
  ): TimeAwareGeoreferencedImage | false {
    const {
      src,
      extent: newExtent,
      resolution: newResolution,
    } = this.getWMSGetMapRequestURL(
      extent,
      resolution,
      pixelRatio,
      projection,
      getMapUrl,
      params,
      layerName,
      styleName,
      dimsProps,
    );
    const imageEl = openLayersGetMapImageStore.getGeoReferencedImage(src, {
      extent: newExtent,
      resolution: newResolution,
    });
    if (imageEl) {
      return {
        el: imageEl.getElement(),
        extent: newExtent,
        resolution: newResolution,
        isLoaded: imageEl.isLoaded(),
        isLoadedWithoutErrors: imageEl.isLoadedWithoutErrors(),
        hasError: imageEl.hasError(),
        isLoading: imageEl.isLoading(),
      };
    }

    return false;
  }

  public setGeoReferencedImage(image: TimeAwareGeoreferencedImage): void {
    const { el, extent, resolution } = image;
    this.setImage(el);
    this.extent = extent;
    this.resolution = resolution;
    // this.projection = projection;/?TODO
  }

  public load(): void {
    const {
      src,
      extent: newExtent,
      resolution: newResolution,
    } = this.getWMSGetMapRequestURL(
      this._extentToLoad,
      this._resolutionToLoad,
      this._pixelRatioToLoad,
      this.projection,
      this._getMapUrl,
      this._paramsToLoad,
      this._layerName,
      this._styleName,
      this._dimPropsToLoad,
    );

    const imageEl = openLayersGetMapImageStore.getGeoReferencedImage(src, {
      extent: newExtent,
      resolution: newResolution,
    });
    if (!imageEl) {
      return;
    }

    if (imageEl.isLoaded()) {
      this.state = ImageState.LOADED;
      this.setImage(imageEl.getElement());
      this.extent = newExtent;
      this.resolution = newResolution;
      this.changed();
      if (imageEl.hasError()) {
        this._rejectAll(imageEl.getSrc());
      } else {
        this._resolveAll();
      }
      return;
    }

    this.state = ImageState.LOADING;
    this.changed();
    void imageEl.loadAwait().finally(() => {
      this.load();
    });
  }

  public setState(state: number): void {
    this.state = state;
  }

  private _rejectAll(url: string): void {
    while (this._resolvers.length !== 0) {
      const resolve = this._resolvers.pop();
      resolve &&
        resolve.reject({
          reason: TIMEAWAREIMAGEWRAPPER_IMAGEDIDNOTLOADSUCCESFULLY,
          url,
        });
    }
  }

  private _resolveAll(): void {
    while (this._resolvers.length !== 0) {
      const resolve = this._resolvers.pop();
      resolve && resolve.resolve();
    }
  }

  public loadAndResolve(
    extentToLoad: Extent,
    resolutionToLoad: number,
    pixelRatio: number,
    projection: Projection,
    getMapUrl: string,
    params: Record<string, string>,
    layerName: string,
    styleName: string,
    dimProps: Record<string, string>,
  ): Promise<void> {
    this._extentToLoad = [...extentToLoad];
    this._resolutionToLoad = resolutionToLoad;
    this._paramsToLoad = { ...params };
    this._getMapUrl = getMapUrl;
    this._layerName = layerName;
    this._pixelRatioToLoad = pixelRatio;
    this.projection = projection;
    this._styleName = styleName;
    this._dimPropsToLoad = { ...dimProps };
    return new Promise((resolve, reject) => {
      this._resolvers.push({ resolve, reject });
      this.load();
    });
  }
}

export default TimeAwareImageWrapper;
