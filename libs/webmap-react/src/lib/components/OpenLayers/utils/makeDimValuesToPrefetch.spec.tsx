/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { WMJSDimension } from '@opengeoweb/webmap';
import {
  getTimeStepsToCheckMemoized,
  makeDimValuesToPrefetch,
} from './makeDimValuesToPrefetch';
import { Timespan } from '../types/Timespan';

describe('makeDimValuesToPrefetch', () => {
  it('should return discretized makeDimValuesToPrefetch', () => {
    const wmDim = new WMJSDimension({
      values: '2020-06-01T12:00:00Z/2020-06-01T14:00:00Z/PT5M',
      units: 'ISO8601',
    });
    const timeSpan: Timespan = {
      start: new Date('2020-06-01T11:46:12Z').getTime(),
      end: new Date('2020-06-01T13:14:59Z').getTime(),
      step: 1000 * 60 * 5, // Five minutes
    };
    wmDim.setValue('2020-06-01T12:35:00Z');
    const values = makeDimValuesToPrefetch(
      wmDim,
      getTimeStepsToCheckMemoized(timeSpan),
      false,
    );

    expect(values).toStrictEqual([
      '2020-06-01T12:35:00Z',
      '2020-06-01T12:40:00Z',
      '2020-06-01T12:45:00Z',
      '2020-06-01T12:50:00Z',
      '2020-06-01T12:55:00Z',
      '2020-06-01T13:00:00Z',
      '2020-06-01T13:05:00Z',
      '2020-06-01T13:10:00Z',
      '2020-06-01T13:15:00Z',
      '2020-06-01T12:30:00Z',
      '2020-06-01T12:25:00Z',
      '2020-06-01T12:20:00Z',
      '2020-06-01T12:15:00Z',
      '2020-06-01T12:10:00Z',
      '2020-06-01T12:05:00Z',
      '2020-06-01T12:00:00Z',
    ]);
  });
  it('should throw a range error when the prefetch list is too large', () => {
    const wmDim = new WMJSDimension({
      values: '2010-06-01T12:00:00Z/2020-06-01T14:00:00Z/PT5M',
    });

    const timeSpan: Timespan = {
      start: new Date('2010-06-01T11:46:12Z').getTime(),
      end: new Date('2020-06-01T12:14:59Z').getTime(),
      step: 60 * 5,
    };
    expect(() => {
      makeDimValuesToPrefetch(
        wmDim,
        getTimeStepsToCheckMemoized(timeSpan),
        false,
      );
    }).toThrow(RangeError);
  });
});
