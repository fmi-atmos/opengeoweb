/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

export const MAX_NUMBER_TO_PREFETCH = 200;
export const MAX_NUMBER_STEPS_IN_TIMESLIDER_TO_PREFETCH = 10000;
export const NUMBER_PREFETCH_WORKERS = 4;
export const NUM_IMAGES_TO_CACHE = 10000;
export const WMS_EXTENT_RATIO_TO_VIEWPORT = 1.2;
export const TIMEAWAREIMAGEWRAPPER_IMAGEDIDNOTLOADSUCCESFULLY =
  'Image did not load succesfully';
