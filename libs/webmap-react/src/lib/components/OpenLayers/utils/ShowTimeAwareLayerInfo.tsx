/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { WMInvalidDateValues, WMLayer } from '@opengeoweb/webmap';
import ImageLayer from 'ol/layer/Image';
import { useRef, useState, useEffect, useMemo } from 'react';
import { TimeawareImageSource } from './TimeawareImageSource';

export interface ShowTimeAwareLayerInfoProps {
  wmLayer: WMLayer;
  stepsToCheck: number[];
  layer: ImageLayer<TimeawareImageSource>;
  visible: boolean;
}

export const ShowTimeAwareLayerInfo: React.FC<ShowTimeAwareLayerInfoProps> = ({
  wmLayer,
  stepsToCheck: timeStepsToCheck,
  layer,
  visible,
}: ShowTimeAwareLayerInfoProps) => {
  const wmTimeDim = wmLayer?.dimensions?.find(
    (dimension) => dimension.name === 'time',
  );
  const intervalIdRef = useRef<NodeJS.Timeout>();
  const [render, setRender] = useState(0);

  // Sets a timer
  useEffect(() => {
    intervalIdRef.current = setInterval(() => {
      setRender(render + 1);
    }, 100);

    return (): void => {
      clearInterval(intervalIdRef.current);
    };
  }, [render]);

  const items = useMemo(() => {
    if (!timeStepsToCheck.length) {
      return null;
    }
    const itemWidth = Math.round(1000 / timeStepsToCheck.length);
    return timeStepsToCheck.map((timeStep, index) => {
      const closestValue = wmTimeDim?.getClosestValueForTime(timeStep) || '';
      const requestedStepIsOutsideRange = WMInvalidDateValues.has(closestValue);
      const requestedStepHasImage = layer
        ?.getSource()
        ?.hasImageForTimeValue(closestValue);
      const isSelectedStep =
        visible && wmTimeDim?.currentValue === closestValue;

      const colorImageIsAvailable = requestedStepHasImage
        ? '#40AF50'
        : '#FF0000';
      const colorLayerIsOutsideRange = requestedStepIsOutsideRange
        ? '#5080FA'
        : colorImageIsAvailable;
      const height = requestedStepIsOutsideRange ? '15px' : '26px';
      const top = requestedStepIsOutsideRange ? '5px' : '0px';
      const left = `${index * itemWidth}px`;
      const opacity = isSelectedStep ? 1 : 0.8;
      return (
        <span
          key={timeStep}
          style={{
            background: isSelectedStep ? '#C0FF50' : colorLayerIsOutsideRange,
            display: 'block',
            position: 'absolute',
            width: `${itemWidth}px`,
            height,
            left,
            top,
            margin: 0,
            padding: 0,
            opacity,
          }}
        />
      );
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layer, timeStepsToCheck, visible, wmTimeDim?.currentValue, render]);
  return (
    <div
      style={{
        height: '26px',
        position: 'relative',
        left: '50px',
        right: '10px',
        marginTop: '12px',
        marginBottom: '-10px',
      }}
    >
      <div>
        <div>{items}</div>
      </div>
      <div
        style={{
          position: 'absolute',
          left: '10px',
          paddingTop: '3px',
          fontSize: '12px',
          fontFamily: 'Roboto',
          opacity: 0.85,
        }}
      >
        <span
          style={{
            width: '300px',
            position: 'absolute',
            display: 'inline-block',
            height: '15px',
          }}
        >
          {wmLayer?.title}
        </span>
        <span
          style={{
            position: 'absolute',
            left: '320px',
            display: 'inline-block',
            width: '600px',
            opacity: '0.7',
          }}
        >
          {wmLayer?.dimensions.map((wmLayerDimension) => {
            return (
              <span
                key={wmLayerDimension.name}
                style={{
                  margin: '0 4px',
                  padding: '1px 5px',
                  background: 'blue',
                  color: 'white',
                  fontSize: '11px',
                }}
              >
                {wmLayerDimension.name} = {wmLayerDimension.currentValue}
              </span>
            );
          })}
        </span>
      </div>
    </div>
  );
};
