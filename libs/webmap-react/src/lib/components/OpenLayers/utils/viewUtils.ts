/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Bbox } from '@opengeoweb/webmap';
import { FeatureCollection, Geometry, GeoJsonProperties } from 'geojson';
import { View } from 'ol';

import { fromLonLat } from 'ol/proj';
import { getFeatureExtent } from '../../MapDrawTool';
import { initializeOpenLayersProjections } from './projections';

initializeOpenLayersProjections();
export const setViewFromExtent = (view: View, bbox: Bbox): void => {
  if (!bbox || !Number(bbox.left) || !isFinite(bbox.left) || isNaN(bbox.left)) {
    view.setCenter([0, 0]);
    view.setZoom(1);
    return;
  }
  if (bbox.left === bbox.right || bbox.bottom === bbox.top) {
    view.setCenter([bbox.left, bbox.top]);
    view.setZoom(1);
    return;
  }
  view.fit([bbox.left, bbox.bottom, bbox.right, bbox.top]);
};

export const setViewFromFeature = (
  view: View,
  geojsonFeature: FeatureCollection<Geometry, GeoJsonProperties> | undefined,
): void => {
  const bboxGeoJSON = geojsonFeature && getFeatureExtent(geojsonFeature);
  const coordMin = fromLonLat(
    [bboxGeoJSON?.left!, bboxGeoJSON?.bottom!],
    'EPSG:3857',
  );
  const coordMax = fromLonLat(
    [bboxGeoJSON?.right!, bboxGeoJSON?.top!],
    'EPSG:3857',
  );

  setViewFromExtent(view, {
    left: coordMin[0],
    right: coordMax[0],
    bottom: coordMin[1],
    top: coordMax[1],
  });
};

export const makeView = (projectionCode = 'EPSG:3857'): View => {
  return new View({
    projection: projectionCode,
    zoom: 1,
    center: [0, 0],
  });
};

export const viewUtils = { setViewFromExtent, setViewFromFeature, makeView };
