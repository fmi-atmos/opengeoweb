/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { Paper, Card, CardContent, Typography } from '@mui/material';

import { PROJECTION } from '@opengeoweb/shared';
import type { Meta, StoryObj } from '@storybook/react';

import { View } from 'ol';
import { boundingExtent, getCenter } from 'ol/extent';
import { publicLayers, defaultLayers } from '../../layers';
import { initializeOpenLayersProjections, OpenLayersMapView } from '.';
import { OpenLayersLayer } from './layers';

initializeOpenLayersProjections();

const meta: Meta<typeof OpenLayersMapView> = {
  title: 'components/OpenLayers',
  component: OpenLayersMapView,
};
export default meta;

type Story = StoryObj<typeof OpenLayersMapView>;

const TextboxDiv = (props: {
  text: {
    title: string;
    body: string;
  };
}): React.ReactElement => {
  const { text } = props;
  return (
    <div
      style={{
        position: 'absolute',
        left: '50px',
        top: '10px',
        zIndex: 10000,
      }}
    >
      <Paper>
        <Card>
          <CardContent>
            <Typography variant="subtitle1">{text.title}</Typography>
            {text.body !== '' && (
              <Typography variant="body2">{text.body}</Typography>
            )}
          </CardContent>
        </Card>
      </Paper>
    </div>
  );
};

export const SetProjectionBBox: Story = {
  render: () => {
    return (
      <div style={{ height: '100vh' }}>
        <div style={{ display: 'flex', height: '50%' }}>
          <div style={{ position: 'relative', width: '50%', height: '100%' }}>
            <TextboxDiv text={{ title: 'Default SRS and BBOX', body: '' }} />
            {/* 'Europe North Pole' */}
            <OpenLayersMapView>
              <OpenLayersLayer {...publicLayers.baseLayer} />
              <OpenLayersLayer {...defaultLayers.overLayer} />
            </OpenLayersMapView>
          </div>
          <div style={{ position: 'relative', width: '50%', height: '100%' }}>
            <TextboxDiv
              text={{
                title: 'Europe stereographic',
                body: PROJECTION.EPSG_32661.value,
              }}
            />
            <OpenLayersMapView
              view={
                new View({
                  projection: PROJECTION.EPSG_32661.value,
                  zoom: 3,
                  center: getCenter(
                    boundingExtent([
                      [-2776118.977564746, -6499490.259201691],
                      [9187990.785775745, 971675.53185069],
                    ]),
                  ),
                })
              }
            >
              <OpenLayersLayer {...publicLayers.baseLayer} />
              <OpenLayersLayer {...defaultLayers.overLayer} />
            </OpenLayersMapView>
          </div>
        </div>
        <div style={{ display: 'flex', height: '50%' }}>
          <div style={{ position: 'relative', width: '50%', height: '100%' }}>
            <TextboxDiv
              text={{
                title: 'Southern Hemisphere',
                body: PROJECTION.EPSG_3412.value,
              }}
            />
            {/* 'Southern Hemisphere' */}
            <OpenLayersMapView
              view={
                new View({
                  projection: PROJECTION.EPSG_3412.value,
                  zoom: 4,
                  center: getCenter(
                    boundingExtent([
                      [-4589984.273212382, -2752857.546211313],
                      [5425154.657417289, 2986705.2537886878],
                    ]),
                  ),
                })
              }
            >
              <OpenLayersLayer {...publicLayers.baseLayer} />
              <OpenLayersLayer {...defaultLayers.overLayer} />
            </OpenLayersMapView>
          </div>
          <div style={{ position: 'relative', width: '50%', height: '100%' }}>
            <TextboxDiv
              text={{
                title: 'Northern Hemisphere',
                body: PROJECTION.EPSG_3411.value,
              }}
            />
            {/* Northern Hemisphere */}
            <OpenLayersMapView
              view={
                new View({
                  projection: PROJECTION.EPSG_3411.value,
                  zoom: 4,
                  center: getCenter(
                    boundingExtent([
                      [-4589984.273212382, -2752857.546211313],
                      [5425154.657417289, 2986705.2537886878],
                    ]),
                  ),
                })
              }
            >
              <OpenLayersLayer {...publicLayers.baseLayer} />
              <OpenLayersLayer {...defaultLayers.overLayer} />
            </OpenLayersMapView>
          </div>
        </div>
      </div>
    );
  },
};

SetProjectionBBox.storyName = 'Set projection and BBOX';
