/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useContext, useMemo } from 'react';
import {
  LayerFoundation,
  LayerType,
  TileSettings,
  tilesettings,
} from '@opengeoweb/webmap';
import { EPSGCode } from '@opengeoweb/shared';
import { TimeawareImageSourceWMSLayer } from './TimeawareImageSourceWMSLayer';
import { XYZLayer } from './XYZLayer';
import { WMSLayer } from './WMSLayer';
import type { OnInitializeLayerProps } from '../utils/types';
import { baseLayer } from '../../../layers/publicLayers';
import { DefaultBaseLayers } from './DefaultBaseLayers';
import { DrawModeExitCallback, FeatureEvent } from '../../MapDraw';
import MapContext from '../context/MapContext';

export interface OpenLayersLayerProps extends LayerFoundation {
  zIndex?: number;
  onInitializeLayer?: (payload: OnInitializeLayerProps) => void;
  onLayerReady?: (layerId: string) => void;
  onLayerError?: (layerId: string, message: string) => void;
  debugMode?: boolean;
  id: string;
  // eslint-disable-next-line react/no-unused-prop-types
  isInEditMode?: boolean;
  // eslint-disable-next-line react/no-unused-prop-types
  drawMode?: string;
  // eslint-disable-next-line react/no-unused-prop-types
  updateGeojson?: (geoJson: GeoJSON.FeatureCollection, reason: string) => void;
  // eslint-disable-next-line react/no-unused-prop-types
  exitDrawModeCallback?: (reason: DrawModeExitCallback) => void;
  // eslint-disable-next-line react/no-unused-prop-types
  selectedFeatureIndex?: number;
  // eslint-disable-next-line react/no-unused-prop-types
  onClickFeature?: (event?: FeatureEvent) => void;
  // eslint-disable-next-line react/no-unused-prop-types
  onHoverFeature?: (event: FeatureEvent) => void;
}

export const OpenLayersLayer = (
  layerProps: OpenLayersLayerProps,
): React.ReactElement => {
  const {
    id,
    service,
    enabled,
    name,
    style,
    dimensions,
    opacity,
    zIndex,
    onInitializeLayer,
    onLayerReady,
    onLayerError,
    debugMode,
    layerType,
  } = layerProps;
  const isBaseLayer = layerType === LayerType.baseLayer;
  const isOverlay = layerType === LayerType.overLayer;
  const layerOptions = useMemo(() => {
    return { opacity: opacity || 1, zIndex: zIndex || 0 };
  }, [opacity, zIndex]);
  const { map } = useContext(MapContext);
  // Check standard maplayers
  if (!isOverlay && !isBaseLayer) {
    return (
      <TimeawareImageSourceWMSLayer
        getCapsURL={service!}
        layerName={name!}
        layerOptions={layerOptions}
        layerId={id}
        dimensions={dimensions}
        visible={!!(enabled !== false)}
        styleName={style}
        onInitializeLayer={onInitializeLayer}
        onLayerReady={onLayerReady}
        onLayerError={onLayerError}
        debugMode={debugMode}
      />
    );
  }

  // Check the other layers
  if (isBaseLayer || isOverlay) {
    const { type, name, service, tileServer } = layerProps;
    if (type === 'twms' && name) {
      const tileSettingByName =
        tilesettings[name] || (tileServer && tileServer[name]);
      if (!tileSettingByName) {
        console.warn(
          `Tilesetting with name ${name} not found in `,
          tilesettings,
          tileServer,
        );
      }

      const projectionName = map
        ?.getView()
        ?.getProjection()
        ?.getCode() as EPSGCode;

      const projectionUsedFromTileServer = tileSettingByName[projectionName]
        ? projectionName
        : 'EPSG:3857';

      const tileSettingForCRS =
        tileSettingByName &&
        ((projectionName &&
          tileSettingByName[projectionUsedFromTileServer]) as TileSettings);
      if (tileSettingForCRS) {
        const { home, tileServerType } = tileSettingForCRS;
        if (tileServerType === 'osm') {
          return (
            <XYZLayer
              urlTemplate={`${home}{z}/{x}/{y}.png`}
              layerOptions={layerOptions}
              layerProps={{ ...layerProps, enabled: true }}
              tileProjectionCode={projectionUsedFromTileServer}
            />
          );
        }
        if (tileServerType === 'arcgisonline') {
          return (
            <XYZLayer
              urlTemplate={`${home}{z}/{y}/{x}.png`}
              layerOptions={layerOptions}
              layerProps={{ ...layerProps, enabled: true }}
              tileProjectionCode={projectionUsedFromTileServer}
            />
          );
        }
        if (tileServerType === 'wmst') {
          return (
            <XYZLayer
              urlTemplate={`${home}{z}/{y}/{x}`}
              layerOptions={layerOptions}
              layerProps={{ ...layerProps, enabled: true }}
              tileProjectionCode={projectionUsedFromTileServer}
            />
          );
        }
      }
    } else if (name && service) {
      return (
        <WMSLayer
          layerName={name!}
          layerOptions={layerOptions}
          layerProps={layerProps}
          url={service!}
          styleName={style || 'default'}
        />
      );
    } else if (baseLayer) {
      return <DefaultBaseLayers />;
    }
  }
  return <div />;
};
