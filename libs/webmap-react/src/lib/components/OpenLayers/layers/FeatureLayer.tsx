/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Feature, MapBrowserEvent } from 'ol';
import { StyleLike } from 'ol/style/Style';
import { useContext, useEffect, useState } from 'react';
import { Vector } from 'ol/layer';
import VectorSource from 'ol/source/Vector';

import { unByKey } from 'ol/Observable';
import { FeatureLike } from 'ol/Feature';
import { debounce } from 'lodash';
import MapContext from '../context/MapContext';
import { ClickOnFeature, HoverSelect } from '../utils/types';

export interface FeatureLayerProps {
  features: Feature[];
  style: StyleLike;
  zIndex?: number;
  hoverSelect?: HoverSelect;
  clickOnFeature?: ClickOnFeature;
  testId?: string;
}

export const FeatureLayer: React.FC<FeatureLayerProps> = ({
  features,
  style,
  zIndex,
  hoverSelect,
  clickOnFeature,
  testId,
}: FeatureLayerProps) => {
  const { map } = useContext(MapContext);
  const [source, setSource] = useState<VectorSource>();

  useEffect(() => {
    if (!map) {
      return (): void => {};
    }
    const source = new VectorSource<Feature>({
      features: [],
    });

    const layer = new Vector({
      source,
      zIndex,
      style,
      properties: {
        testId,
      },
    });

    layer.set('layerType', 'FeatureLayer');

    setSource(source);
    map.addLayer(layer);

    return (): void => {
      map.removeLayer(layer);
    };
  }, [map, style, zIndex, testId]);

  useEffect(() => {
    if (!source) {
      return;
    }
    source.clear();
    if (features && features.length > 0) {
      source.addFeatures(features);
    }
  }, [source, features]);

  useEffect((): (() => void) => {
    if (!map || !source || !hoverSelect || !hoverSelect.active) {
      return (): void => {};
    }

    const previousHover: FeatureLike[] = [];

    const pointerMoveFunction = debounce(
      (evt: MapBrowserEvent<MouseEvent>) => {
        const hoveredFeature = map.forEachFeatureAtPixel(
          evt.pixel,
          (feature: FeatureLike, layer): FeatureLike | false => {
            if (!layer || layer.getSource() !== source) {
              return false;
            }
            return feature;
          },
        );

        if (hoveredFeature && previousHover.includes(hoveredFeature)) {
          return;
        }

        const unhovered = previousHover.length > 0;

        previousHover.forEach((feature) => {
          (feature as Feature).set('hover', false);
        });
        previousHover.splice(0);

        if (hoveredFeature) {
          previousHover.push(hoveredFeature);
          (hoveredFeature as Feature).set('hover', true);
          hoverSelect.onHover && hoverSelect.onHover(hoveredFeature as Feature);
        } else if (unhovered) {
          hoverSelect.onHover && hoverSelect.onHover(undefined);
        }
      },
      5,
      {},
    );

    const eventKey = map.on('pointermove', pointerMoveFunction);

    return () => {
      pointerMoveFunction.cancel();
      source.getFeatures().forEach((feature) => {
        (feature as Feature).set('hover', false);
      });
      unByKey(eventKey);
      hoverSelect.onHover && hoverSelect.onHover(undefined);
    };
  }, [map, source, hoverSelect]);

  useEffect((): (() => void) => {
    if (!map || !source || !clickOnFeature) {
      return (): void => {};
    }

    const clickFunction = (evt: MapBrowserEvent<MouseEvent>): void => {
      const features: Feature[] = [];
      map.forEachFeatureAtPixel(
        evt.pixel,
        (f) => {
          features.push(f as Feature);
        },
        { hitTolerance: clickOnFeature.hitTolerance },
      );

      clickOnFeature.onClick(features);
    };

    const eventKey = map.on('click', clickFunction);

    return (): void => {
      unByKey(eventKey);
    };
  }, [map, source, clickOnFeature]);

  return null;
};
