/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import ImageLayer from 'ol/layer/Image';
import { TileWMS } from 'ol/source';
import TileLayer from 'ol/layer/Tile';

import { useContext, useEffect, useMemo, useState } from 'react';

import { LayerFoundation } from '@opengeoweb/webmap';

import MapContext from '../context/MapContext';
import { useQueryGetWMSLayer } from '../../../hooks';
import {
  selectPreferredWMSProjection,
  useProjection,
} from '../utils/projections';

export interface TiledWMSLayerProps {
  url: string;
  layerName: string;
  styleName: string;
  layerOptions: ConstructorParameters<typeof ImageLayer>[0];
  layerProps?: LayerFoundation;
}

export const TiledWMSLayer: React.FC<TiledWMSLayerProps> = ({
  url,
  layerName,
  styleName,
  layerOptions,
  layerProps,
}: TiledWMSLayerProps) => {
  const { map } = useContext(MapContext);
  const [layer, setLayer] = useState<TileLayer<TileWMS>>();

  const layerInformation = useQueryGetWMSLayer(url, layerName);
  const mapProjection = useProjection();
  const projection = useMemo(() => {
    if (!map || !mapProjection) {
      return null;
    }
    return selectPreferredWMSProjection(mapProjection, layerInformation);
  }, [map, mapProjection, layerInformation]);

  useEffect(() => {
    if (!map || !layerName || !projection) {
      return (): void => {};
    }

    const layer = new TileLayer({
      source: new TileWMS({
        url: url!,
        projection,
        params: {
          LAYERS: layerName,
          STYLES: styleName,
        },
      }),
    });

    map.addLayer(layer);
    setLayer(layer);

    return (): void => {
      map.removeLayer(layer);
      setLayer(undefined);
    };
  }, [map, url, projection, layerName, styleName]);

  useEffect(() => {
    if (!layerOptions || !layer) {
      return;
    }

    if (layerOptions.opacity !== undefined && layerOptions.opacity !== null) {
      layer.setOpacity(layerOptions.opacity);
    }
    if (layerOptions.zIndex !== undefined && layerOptions.zIndex !== null) {
      layer.setZIndex(layerOptions.zIndex);
    }
  }, [layer, layerOptions]);

  useEffect(() => {
    if (!layerProps || !layer) {
      return;
    }
    layer.setVisible(!!layerProps.enabled);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layer, layerProps?.enabled]);

  return null;
};
