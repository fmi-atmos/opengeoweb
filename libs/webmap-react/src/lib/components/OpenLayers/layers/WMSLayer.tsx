/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import ImageLayer from 'ol/layer/Image';
import ImageSource from 'ol/source/Image';
import { createLoader } from 'ol/source/wms';
import { load } from 'ol/Image';

import { useContext, useEffect, useState } from 'react';

import { LayerFoundation } from '@opengeoweb/webmap';

import { unByKey } from 'ol/Observable';
import MapContext from '../context/MapContext';
import {
  selectPreferredWMSProjection,
  useProjection,
} from '../utils/projections';
import { useQueryGetWMSLayer } from '../../../hooks';

export interface WMSLayerProps {
  url: string;
  layerName: string;
  styleName: string;
  layerOptions: ConstructorParameters<typeof ImageLayer>[0];
  layerProps?: LayerFoundation;
}

export const WMSLayer: React.FC<WMSLayerProps> = ({
  url,
  layerName,
  styleName,
  layerOptions,
  layerProps,
}: WMSLayerProps) => {
  const { map } = useContext(MapContext);
  const [layer, setLayer] = useState<ImageLayer<ImageSource>>();

  const [projection, setProjection] = useState<string>();

  const layerInformation = useQueryGetWMSLayer(url, layerName);

  const mapProjection = useProjection();

  // Listen to changes in the map projection
  useEffect(() => {
    if (!map || !mapProjection) {
      return (): void => {};
    }

    const setProjectionFromMapAndProps = (): void => {
      const projection = selectPreferredWMSProjection(
        mapProjection,
        layerInformation,
      );
      setProjection(projection);
    };

    if (map.getView()?.getProjection()) {
      setProjectionFromMapAndProps();
    }

    const eventKey = map.on('change:view', setProjectionFromMapAndProps);

    return (): void => {
      unByKey(eventKey);
    };
  }, [map, layerInformation, mapProjection]);

  // Set up layer
  useEffect(() => {
    if (!map || !layerName || !projection) {
      return (): void => {};
    }

    const layer = new ImageLayer({
      source: new ImageSource({
        loader: createLoader({
          url: url!,
          load,
          projection,
          params: {
            LAYERS: layerName,
            STYLES: styleName,
          },
        }),
      }),
    });
    map.addLayer(layer);
    setLayer(layer);

    return (): void => {
      map.removeLayer(layer);
      setLayer(undefined);
    };
  }, [map, url, projection, layerName, styleName]);

  // Update layer options
  useEffect(() => {
    if (!layerOptions || !layer) {
      return;
    }

    if (layerOptions.opacity !== undefined && layerOptions.opacity !== null) {
      layer.setOpacity(layerOptions.opacity);
    }
    if (layerOptions.zIndex !== undefined && layerOptions.zIndex !== null) {
      layer.setZIndex(layerOptions.zIndex);
    }
  }, [layer, layerOptions]);

  useEffect(() => {
    if (!layerProps || !layer) {
      return;
    }
    layer.setVisible(!!layerProps.enabled);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layer, layerProps?.enabled]);

  return null;
};
