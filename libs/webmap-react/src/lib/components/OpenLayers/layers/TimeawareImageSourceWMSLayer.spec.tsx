/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, waitFor } from '@testing-library/react';

import Map from 'ol/Map';
import {
  setWMSGetCapabilitiesFetcher,
  mockGetCapabilities,
  webmapUtils,
  getWMLayerById,
} from '@opengeoweb/webmap';
import { TimeawareImageSourceWMSLayer } from './TimeawareImageSourceWMSLayer';
import TimeContext from '../context/TimeContext';
import MapContext from '../context/MapContext';
import { TimeawareImageSource } from '../utils/TimeawareImageSource';

describe('TimeawareImageSourceWMSLayer', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });

  const timeContextValue = {
    timespan: {
      start: new Date('2021-05-17T07:12:34Z').getTime(),
      end: new Date('2021-05-17T14:56:12Z').getTime(),
      step: 3600000 * 1,
    },
    isAnimating: false,
  };

  const expectedPrefetchValues = [
    '2021-05-17T07:00:00Z',
    '2021-05-17T08:00:00Z',
    '2021-05-17T09:00:00Z',
    '2021-05-17T10:00:00Z',
    '2021-05-17T11:00:00Z',
    '2021-05-17T12:00:00Z',
    '2021-05-17T13:00:00Z',
    '2021-05-17T14:00:00Z',
    '2021-05-17T15:00:00Z',
  ];

  it('should render', async () => {
    const { baseElement } = render(
      <TimeawareImageSourceWMSLayer
        getCapsURL="https://geoservices.knmi.nl/adagucserver?dataset=uwcw_ha43_dini_5p5km&service=WMS&request=GetCapabilities"
        layerName="air_temperature_hagl"
        styleName="temperature_wow/shaded"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
      />,
    );
    await waitFor(() => {
      expect(baseElement).toBeTruthy();
    });
  });

  it('should call onInitializeLayer when mounted', async () => {
    const onInitializeLayer = jest.fn();
    jest
      .spyOn(webmapUtils, 'generateLayerId')
      .mockReturnValueOnce('layerid_usegetwmlayerinstance_1');

    const { baseElement } = render(
      <TimeawareImageSourceWMSLayer
        getCapsURL="WMS130GetCapabilitiesHarmN25"
        layerName="dew_point_temperature__at_2m"
        styleName="auto/bilinear"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
        onInitializeLayer={onInitializeLayer}
      />,
    );
    expect(baseElement).toBeTruthy();
    await waitFor(() => {
      expect(onInitializeLayer).toHaveBeenCalledWith({
        dimensions: [
          {
            currentValue: '2021-05-17T03:00:00Z',
            maxValue: '2021-05-19T03:00:00Z',
            minValue: '2021-05-17T03:00:00Z',
            name: 'time',
            synced: false,
            unitSymbol: '',
            units: 'ISO8601',
            values: '2021-05-10T00:00:00Z/2021-05-19T03:00:00Z/PT1H',
          },
          {
            currentValue: '2021-05-17T03:00:00Z',
            maxValue: '2021-05-17T03:00:00Z',
            minValue: '2021-05-10T00:00:00Z',
            name: 'reference_time',
            synced: false,
            unitSymbol: '',
            units: 'ISO8601',
            values:
              '2021-05-10T00:00:00Z,2021-05-10T03:00:00Z,2021-05-10T06:00:00Z,2021-05-10T09:00:00Z,2021-05-10T12:00:00Z,2021-05-10T15:00:00Z,2021-05-10T18:00:00Z,2021-05-10T21:00:00Z,2021-05-11T00:00:00Z,2021-05-11T03:00:00Z,2021-05-11T06:00:00Z,2021-05-11T09:00:00Z,2021-05-11T12:00:00Z,2021-05-11T15:00:00Z,2021-05-11T18:00:00Z,2021-05-11T21:00:00Z,2021-05-12T00:00:00Z,2021-05-12T03:00:00Z,2021-05-12T06:00:00Z,2021-05-12T09:00:00Z,2021-05-12T12:00:00Z,2021-05-12T15:00:00Z,2021-05-12T18:00:00Z,2021-05-12T21:00:00Z,2021-05-13T00:00:00Z,2021-05-13T03:00:00Z,2021-05-13T06:00:00Z,2021-05-13T09:00:00Z,2021-05-13T12:00:00Z,2021-05-13T15:00:00Z,2021-05-13T18:00:00Z,2021-05-13T21:00:00Z,2021-05-14T00:00:00Z,2021-05-14T03:00:00Z,2021-05-14T06:00:00Z,2021-05-14T09:00:00Z,2021-05-14T12:00:00Z,2021-05-14T15:00:00Z,2021-05-14T18:00:00Z,2021-05-14T21:00:00Z,2021-05-15T00:00:00Z,2021-05-15T03:00:00Z,2021-05-15T06:00:00Z,2021-05-15T09:00:00Z,2021-05-15T12:00:00Z,2021-05-15T15:00:00Z,2021-05-15T18:00:00Z,2021-05-15T21:00:00Z,2021-05-16T00:00:00Z,2021-05-16T03:00:00Z,2021-05-16T06:00:00Z,2021-05-16T09:00:00Z,2021-05-16T12:00:00Z,2021-05-16T15:00:00Z,2021-05-16T18:00:00Z,2021-05-16T21:00:00Z,2021-05-17T00:00:00Z,2021-05-17T03:00:00Z',
          },
        ],
        layerId: 'layerid_usegetwmlayerinstance_1',
        style: 'auto/bilinear',
      });
    });
  });

  it('should not call onInitializeLayer when rerendered', async () => {
    const onInitializeLayer = jest.fn();
    const { baseElement, rerender } = render(
      <TimeawareImageSourceWMSLayer
        getCapsURL="WMS130GetCapabilitiesHarmN25"
        layerName="dew_point_temperature__at_2m"
        styleName="auto/bilinear"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
        onInitializeLayer={onInitializeLayer}
      />,
    );
    expect(baseElement).toBeTruthy();
    await waitFor(() => {
      expect(onInitializeLayer).toHaveBeenCalledTimes(1);
    });
    rerender(
      <TimeawareImageSourceWMSLayer
        getCapsURL="WMS130GetCapabilitiesHarmN25"
        layerName="dew_point_temperature__at_2m"
        styleName="auto/bilinear"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
        onInitializeLayer={onInitializeLayer}
      />,
    );
    await waitFor(() => {
      expect(onInitializeLayer).toHaveBeenCalledTimes(1);
    });
  });

  it('should call onInitializeLayer again when name is changed', async () => {
    const onInitializeLayer = jest.fn();
    const { baseElement, rerender } = render(
      <TimeawareImageSourceWMSLayer
        getCapsURL="WMS130GetCapabilitiesHarmN25"
        layerName="dew_point_temperature__at_2m"
        styleName="auto/bilinear"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
        onInitializeLayer={onInitializeLayer}
      />,
    );
    expect(baseElement).toBeTruthy();
    await waitFor(() => {
      expect(onInitializeLayer).toHaveBeenCalledTimes(1);
    });
    rerender(
      <TimeawareImageSourceWMSLayer
        getCapsURL="WMS130GetCapabilitiesHarmN25"
        layerName="wind__at_10m"
        styleName="auto/bilinear"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
        onInitializeLayer={onInitializeLayer}
      />,
    );
    await waitFor(() => {
      expect(onInitializeLayer).toHaveBeenCalledTimes(2);
    });
  });

  it('should not call onInitializeLayer when not visible', async () => {
    const onInitializeLayer = jest.fn();
    const { baseElement, rerender } = render(
      <TimeawareImageSourceWMSLayer
        getCapsURL="WMS130GetCapabilitiesHarmN25"
        layerName="dew_point_temperature__at_2m"
        styleName="auto/bilinear"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
        onInitializeLayer={onInitializeLayer}
        visible={false}
      />,
    );
    expect(baseElement).toBeTruthy();

    rerender(
      <TimeawareImageSourceWMSLayer
        getCapsURL="WMS130GetCapabilitiesHarmN25"
        layerName="wind__at_10m"
        styleName="auto/bilinear"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
        onInitializeLayer={onInitializeLayer}
        visible={true}
      />,
    );
    await waitFor(() => {
      expect(onInitializeLayer).toHaveBeenCalledTimes(1);
    });
  });

  it('should add and remove openlayers', async () => {
    const map: Map = {
      addLayer: jest.fn(),
      removeLayer: jest.fn(),
    } as unknown as Map;
    const { unmount } = render(
      <TimeContext.Provider value={timeContextValue}>
        <MapContext.Provider value={{ map }}>
          <TimeawareImageSourceWMSLayer
            getCapsURL="WMS130GetCapabilitiesHarmN25"
            layerName="dew_point_temperature__at_2m"
            styleName="auto/bilinear"
            layerOptions={{ opacity: 0.33, zIndex: 1 }}
          />
        </MapContext.Provider>
      </TimeContext.Provider>,
    );
    expect(map.removeLayer).not.toHaveBeenCalled();
    // AddLayer should be called
    await waitFor(() => {
      expect(map.addLayer).toHaveBeenCalled();
    });
    // Should remove layer when it unmounts
    unmount();
    expect(map.removeLayer).toHaveBeenCalled();
  });

  it('should initialize prefetch when context is provided', async () => {
    const onInitializeLayer = jest.fn();

    const setTimeValuesValuesToPrefetchMock = jest.spyOn(
      TimeawareImageSource.prototype,
      'setTimeValuesValuesToPrefetch',
    );

    const map: Map = {
      addLayer: jest.fn(),
      removeLayer: jest.fn(),
    } as unknown as Map;
    const { baseElement, unmount } = render(
      <TimeContext.Provider value={timeContextValue}>
        <MapContext.Provider value={{ map }}>
          <TimeawareImageSourceWMSLayer
            getCapsURL="WMS130GetCapabilitiesHarmN25"
            layerName="dew_point_temperature__at_2m"
            styleName="auto/bilinear"
            layerOptions={{ opacity: 0.33, zIndex: 1 }}
            onInitializeLayer={onInitializeLayer}
          />
        </MapContext.Provider>
      </TimeContext.Provider>,
    );
    expect(baseElement).toBeTruthy();
    expect(map.removeLayer).not.toHaveBeenCalled();

    // AddLayer should be called
    await waitFor(() => {
      expect(map.addLayer).toHaveBeenCalled();
    });

    expect(onInitializeLayer).toHaveBeenCalledTimes(1);

    expect(setTimeValuesValuesToPrefetchMock).toHaveBeenCalledWith(
      expectedPrefetchValues,
    );
    // Should remove layer when it unmounts
    unmount();
    expect(map.removeLayer).toHaveBeenCalled();
  });

  it('should use dimensions', async () => {
    const onInitializeLayer = jest.fn();
    const testidforwmlayer = 'testidforwmlayer';
    const { baseElement, rerender } = render(
      <TimeawareImageSourceWMSLayer
        getCapsURL="WMS130GetCapabilitiesHarmN25"
        layerName="dew_point_temperature__at_2m"
        styleName="auto/bilinear"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
        onInitializeLayer={onInitializeLayer}
        dimensions={[
          { name: 'time', currentValue: '2021-05-17T11:00:00Z' },
          { name: 'reference_time', currentValue: '2021-05-17T15:10:00Z' },
        ]}
        layerId={testidforwmlayer}
      />,
    );
    expect(baseElement).toBeTruthy();
    await waitFor(() => {
      expect(onInitializeLayer).toHaveBeenCalledTimes(1);
    });
    expect(
      getWMLayerById(testidforwmlayer).getDimension('time')?.currentValue,
    ).toBe('2021-05-17T11:00:00Z');
    rerender(
      <TimeawareImageSourceWMSLayer
        getCapsURL="WMS130GetCapabilitiesHarmN25"
        layerName="dew_point_temperature__at_2m"
        styleName="auto/bilinear"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
        onInitializeLayer={onInitializeLayer}
        dimensions={[
          { name: 'time', currentValue: '2021-05-17T15:10:00Z' },
          { name: 'reference_time', currentValue: '2021-05-17T15:10:00Z' },
        ]}
        layerId={testidforwmlayer}
      />,
    );
    await waitFor(() => {
      expect(onInitializeLayer).toHaveBeenCalledTimes(1);
    });

    expect(
      getWMLayerById(testidforwmlayer).getDimension('time')?.currentValue,
    ).toBe('2021-05-17T15:00:00Z');

    rerender(
      <TimeawareImageSourceWMSLayer
        getCapsURL="WMS130GetCapabilitiesHarmN25"
        layerName="dew_point_temperature__at_2m"
        styleName="auto/bilinear"
        layerOptions={{ opacity: 0.33, zIndex: 1 }}
        onInitializeLayer={onInitializeLayer}
        dimensions={[
          { name: 'time', currentValue: '2021-05-17T15:10:00Z' },
          { name: 'reference_time', currentValue: '2021-05-17T15:10:00Z' },
        ]}
        layerId={testidforwmlayer}
      />,
    );
    await waitFor(() => {
      expect(onInitializeLayer).toHaveBeenCalledTimes(1);
    });

    expect(
      getWMLayerById(testidforwmlayer).getDimension('time')?.currentValue,
    ).toBe('2021-05-17T15:00:00Z');
    expect(
      getWMLayerById(testidforwmlayer).getDimension('reference_time')
        ?.currentValue,
    ).toBe('2021-05-17T03:00:00Z');
  });

  it('should prefetch when layername is changed', async () => {
    const onInitializeLayer = jest.fn();

    const setTimeValuesValuesToPrefetchMock = jest.spyOn(
      TimeawareImageSource.prototype,
      'setTimeValuesValuesToPrefetch',
    );

    const map: Map = {
      addLayer: jest.fn(),
      removeLayer: jest.fn(),
    } as unknown as Map;
    const { baseElement, unmount, rerender } = render(
      <TimeContext.Provider value={timeContextValue}>
        <MapContext.Provider value={{ map }}>
          <TimeawareImageSourceWMSLayer
            getCapsURL="WMS130GetCapabilitiesHarmN25"
            layerName="dew_point_temperature__at_2m"
            styleName="auto/bilinear"
            layerOptions={{ opacity: 0.33, zIndex: 1 }}
            onInitializeLayer={onInitializeLayer}
          />
        </MapContext.Provider>
      </TimeContext.Provider>,
    );
    expect(baseElement).toBeTruthy();
    expect(map.removeLayer).not.toHaveBeenCalled();

    // AddLayer should be called
    await waitFor(() => {
      expect(map.addLayer).toHaveBeenCalled();
    });

    expect(onInitializeLayer).toHaveBeenCalledTimes(1);

    expect(setTimeValuesValuesToPrefetchMock).toHaveBeenCalledWith(
      expectedPrefetchValues,
    );

    setTimeValuesValuesToPrefetchMock.mockClear();

    rerender(
      <TimeContext.Provider value={timeContextValue}>
        <MapContext.Provider value={{ map }}>
          <TimeawareImageSourceWMSLayer
            getCapsURL="WMS130GetCapabilitiesHarmN25"
            layerName="air_pressure_at_sea_level"
            styleName="auto/nearest"
            layerOptions={{ opacity: 0.33, zIndex: 1 }}
            onInitializeLayer={onInitializeLayer}
          />
        </MapContext.Provider>
      </TimeContext.Provider>,
    );

    expect(setTimeValuesValuesToPrefetchMock).toHaveBeenCalledWith(
      expectedPrefetchValues,
    );

    // Should remove layer when it unmounts
    unmount();
    expect(map.removeLayer).toHaveBeenCalled();
  });
  it('should prefetch when stylename is changed', async () => {
    const onInitializeLayer = jest.fn();

    const setTimeValuesValuesToPrefetchMock = jest.spyOn(
      TimeawareImageSource.prototype,
      'setTimeValuesValuesToPrefetch',
    );

    const map: Map = {
      addLayer: jest.fn(),
      removeLayer: jest.fn(),
    } as unknown as Map;
    const { baseElement, unmount, rerender } = render(
      <TimeContext.Provider value={timeContextValue}>
        <MapContext.Provider value={{ map }}>
          <TimeawareImageSourceWMSLayer
            getCapsURL="WMS130GetCapabilitiesHarmN25"
            layerName="dew_point_temperature__at_2m"
            styleName="auto/bilinear"
            layerOptions={{ opacity: 0.33, zIndex: 1 }}
            onInitializeLayer={onInitializeLayer}
          />
        </MapContext.Provider>
      </TimeContext.Provider>,
    );
    expect(baseElement).toBeTruthy();
    expect(map.removeLayer).not.toHaveBeenCalled();

    // AddLayer should be called
    await waitFor(() => {
      expect(map.addLayer).toHaveBeenCalled();
    });

    expect(onInitializeLayer).toHaveBeenCalledTimes(1);

    expect(setTimeValuesValuesToPrefetchMock).toHaveBeenCalledWith(
      expectedPrefetchValues,
    );

    setTimeValuesValuesToPrefetchMock.mockClear();

    rerender(
      <TimeContext.Provider value={timeContextValue}>
        <MapContext.Provider value={{ map }}>
          <TimeawareImageSourceWMSLayer
            getCapsURL="WMS130GetCapabilitiesHarmN25"
            layerName="dew_point_temperature__at_2m"
            styleName="auto/nearest"
            layerOptions={{ opacity: 0.33, zIndex: 1 }}
            onInitializeLayer={onInitializeLayer}
          />
        </MapContext.Provider>
      </TimeContext.Provider>,
    );

    expect(setTimeValuesValuesToPrefetchMock).toHaveBeenCalledWith(
      expectedPrefetchValues,
    );

    // Should remove layer when it unmounts
    unmount();
    expect(map.removeLayer).toHaveBeenCalled();
  });

  it('should prefetch when a dimension other then time is changed', async () => {
    const onInitializeLayer = jest.fn();

    const setTimeValuesValuesToPrefetchMock = jest.spyOn(
      TimeawareImageSource.prototype,
      'setTimeValuesValuesToPrefetch',
    );

    const map: Map = {
      addLayer: jest.fn(),
      removeLayer: jest.fn(),
    } as unknown as Map;
    const { baseElement, unmount, rerender } = render(
      <TimeContext.Provider value={timeContextValue}>
        <MapContext.Provider value={{ map }}>
          <TimeawareImageSourceWMSLayer
            getCapsURL="WMS130GetCapabilitiesHarmN25"
            layerName="dew_point_temperature__at_2m"
            styleName="auto/bilinear"
            layerOptions={{ opacity: 0.33, zIndex: 1 }}
            onInitializeLayer={onInitializeLayer}
            dimensions={[
              { name: 'time', currentValue: '2021-05-17T11:00:00Z' },
              { name: 'reference_time', currentValue: '2021-05-17T15:10:00Z' },
            ]}
          />
        </MapContext.Provider>
      </TimeContext.Provider>,
    );
    expect(baseElement).toBeTruthy();
    expect(map.removeLayer).not.toHaveBeenCalled();

    // AddLayer should be called
    await waitFor(() => {
      expect(map.addLayer).toHaveBeenCalled();
    });

    expect(onInitializeLayer).toHaveBeenCalledTimes(1);

    expect(setTimeValuesValuesToPrefetchMock).toHaveBeenCalledWith([
      '2021-05-17T11:00:00Z',
      '2021-05-17T12:00:00Z',
      '2021-05-17T13:00:00Z',
      '2021-05-17T14:00:00Z',
      '2021-05-17T15:00:00Z',
      '2021-05-17T10:00:00Z',
      '2021-05-17T09:00:00Z',
      '2021-05-17T08:00:00Z',
      '2021-05-17T07:00:00Z',
    ]);

    setTimeValuesValuesToPrefetchMock.mockClear();

    rerender(
      <TimeContext.Provider value={timeContextValue}>
        <MapContext.Provider value={{ map }}>
          <TimeawareImageSourceWMSLayer
            getCapsURL="WMS130GetCapabilitiesHarmN25"
            layerName="dew_point_temperature__at_2m"
            styleName="auto/nearest"
            layerOptions={{ opacity: 0.33, zIndex: 1 }}
            onInitializeLayer={onInitializeLayer}
            dimensions={[
              { name: 'time', currentValue: '2021-05-17T11:00:00Z' },
              { name: 'reference_time', currentValue: '2021-05-17T18:00:00Z' },
            ]}
          />
        </MapContext.Provider>
      </TimeContext.Provider>,
    );

    expect(setTimeValuesValuesToPrefetchMock).toHaveBeenCalledWith([
      '2021-05-17T11:00:00Z',
      '2021-05-17T12:00:00Z',
      '2021-05-17T13:00:00Z',
      '2021-05-17T14:00:00Z',
      '2021-05-17T15:00:00Z',
      '2021-05-17T10:00:00Z',
      '2021-05-17T09:00:00Z',
      '2021-05-17T08:00:00Z',
      '2021-05-17T07:00:00Z',
    ]);

    // Should remove layer when it unmounts
    unmount();
    expect(map.removeLayer).toHaveBeenCalled();
  });

  it('should NOT prefetch when opacity, zIndex or time is changed', async () => {
    const onInitializeLayer = jest.fn();

    const setTimeValuesValuesToPrefetchMock = jest.spyOn(
      TimeawareImageSource.prototype,
      'setTimeValuesValuesToPrefetch',
    );

    const map: Map = {
      addLayer: jest.fn(),
      removeLayer: jest.fn(),
    } as unknown as Map;
    const { baseElement, unmount, rerender } = render(
      <TimeContext.Provider value={timeContextValue}>
        <MapContext.Provider value={{ map }}>
          <TimeawareImageSourceWMSLayer
            getCapsURL="WMS130GetCapabilitiesHarmN25"
            layerName="dew_point_temperature__at_2m"
            styleName="auto/bilinear"
            layerOptions={{ opacity: 0.33, zIndex: 1 }}
            onInitializeLayer={onInitializeLayer}
            dimensions={[
              { name: 'time', currentValue: '2021-05-17T11:00:00Z' },
              { name: 'reference_time', currentValue: '2021-05-17T03:00:00Z' },
            ]}
          />
        </MapContext.Provider>
      </TimeContext.Provider>,
    );
    expect(baseElement).toBeTruthy();
    expect(map.removeLayer).not.toHaveBeenCalled();

    // AddLayer should be called
    await waitFor(() => {
      expect(map.addLayer).toHaveBeenCalled();
    });

    expect(onInitializeLayer).toHaveBeenCalledTimes(1);

    expect(setTimeValuesValuesToPrefetchMock).toHaveBeenCalledWith([
      '2021-05-17T11:00:00Z',
      '2021-05-17T12:00:00Z',
      '2021-05-17T13:00:00Z',
      '2021-05-17T14:00:00Z',
      '2021-05-17T15:00:00Z',
      '2021-05-17T10:00:00Z',
      '2021-05-17T09:00:00Z',
      '2021-05-17T08:00:00Z',
      '2021-05-17T07:00:00Z',
    ]);

    setTimeValuesValuesToPrefetchMock.mockClear();

    rerender(
      <TimeContext.Provider value={timeContextValue}>
        <MapContext.Provider value={{ map }}>
          <TimeawareImageSourceWMSLayer
            getCapsURL="WMS130GetCapabilitiesHarmN25"
            layerName="dew_point_temperature__at_2m"
            styleName="auto/bilinear"
            layerOptions={{ opacity: 0.73, zIndex: 2 }}
            onInitializeLayer={onInitializeLayer}
            dimensions={[
              { name: 'time', currentValue: '2021-05-17T11:00:00Z' },
              { name: 'reference_time', currentValue: '2021-05-17T03:00:00Z' },
            ]}
          />
        </MapContext.Provider>
      </TimeContext.Provider>,
    );

    expect(setTimeValuesValuesToPrefetchMock).not.toHaveBeenCalled();

    // Should remove layer when it unmounts
    unmount();
    expect(map.removeLayer).toHaveBeenCalled();
  });
});
