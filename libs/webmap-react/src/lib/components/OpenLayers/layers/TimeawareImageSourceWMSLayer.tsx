/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { useContext, useEffect, useMemo, useState } from 'react';

import ImageLayer from 'ol/layer/Image';

import { Dimension } from '@opengeoweb/webmap';
import MapContext from '../context/MapContext';
import TimeContext from '../context/TimeContext';
import { TimeawareImageSource } from '../utils/TimeawareImageSource';
import { useGetWMLayerInstance } from '../../../hooks/useHooksGetCapabilities';
import {
  getDimensionParamsForWMS,
  getTimeStepsToCheckMemoized,
  makeDimValuesToPrefetch,
} from '../utils/makeDimValuesToPrefetch';
import type { OnInitializeLayerProps } from '../utils/types';

// eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
import { ShowTimeAwareLayerInfo } from '../utils/ShowTimeAwareLayerInfo';

interface TimeawareImageSourceWMSLayerProps {
  layerId?: string;
  getCapsURL: string;
  layerName: string;
  styleName?: string;
  layerOptions: ConstructorParameters<typeof ImageLayer>[0];
  dimensions?: Dimension[];
  visible?: boolean;
  onInitializeLayer?: (payload: OnInitializeLayerProps) => void;
  onLayerReady?: (layerId: string) => void;
  onLayerError?: (layerId: string, message: string) => void;
  debugMode?: boolean;
}

export const TimeawareImageSourceWMSLayer: React.FC<
  TimeawareImageSourceWMSLayerProps
> = ({
  layerId,
  getCapsURL,
  layerOptions,
  styleName,
  layerName,
  dimensions,
  debugMode,
  visible = true,
  onInitializeLayer,
  onLayerReady,
  onLayerError,
}: TimeawareImageSourceWMSLayerProps) => {
  const { map } = useContext(MapContext);
  const { timespan, isAnimating } = useContext(TimeContext);
  const [layer, setLayer] = useState<ImageLayer<TimeawareImageSource>>();

  // get a wmLayer instance
  const wmLayer = useGetWMLayerInstance(
    getCapsURL,
    layerName,
    layerId,
    onLayerError,
  );

  // dimensionsValues is a key/value dictionary with dimension names specifically for WMS GetMap requests
  const dimensionsValues = useMemo(
    () => getDimensionParamsForWMS(dimensions, wmLayer),
    [dimensions, wmLayer],
  );

  // This string can be used safely in a useEffect or other React hook to check if the contents changed
  const dimensionsValuesEffectCheck: string = Object.entries(dimensionsValues)
    .map((dimensionNameAndCurrentValue) => {
      return `${dimensionNameAndCurrentValue[0]}:${dimensionNameAndCurrentValue[1]}`;
    })
    .join(',') as string;

  // Make a TimeawareImageSource and add it to the map, add and remove should only be done when the map, layer id or visibility changes
  useEffect(() => {
    if (!map || !wmLayer || visible === false) {
      return (): void => {};
    }

    // When this component mounts, an ImageLayer is added to the map. Once it unmounts, this layer is removed from the map.
    const olLayer = new ImageLayer<TimeawareImageSource>({
      source: new TimeawareImageSource({
        wmsUrl: wmLayer.getmapURL,
        layerName: wmLayer.name!,
        styleName,
        dimProps: dimensionsValues,
        timeStampsToPrefetch: [],
        visible,
      }),
    });
    map.addLayer(olLayer);
    setLayer(olLayer);
    // Assign olSource in wmlayer for metronome
    wmLayer.olSource = olLayer?.getSource();
    return (): void => {
      wmLayer.olSource = null;
      map.removeLayer(olLayer);
      olLayer.dispose();
    };

    // The dependency list should only contain the map and wmLayer. Other prop changes are handled by other effects.
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [map, wmLayer, visible]);

  useEffect(() => {
    onLayerReady && wmLayer && layerId && onLayerReady(layerId);
  }, [layerId, wmLayer, onLayerReady]);

  // Initialize layer for GeoWeb, call onInitializeLayer
  useEffect(() => {
    const initializeLayer = async (): Promise<void> => {
      if (wmLayer && visible !== false) {
        const wmTimeDim = wmLayer?.dimensions?.find(
          (dimension) => dimension.name === 'time',
        );
        // Set time dimension current value based on layerprops dimension value

        const layerPropsTimeDim = dimensions?.find(
          (dimension) => dimension.name === 'time',
        );

        const timeValueToInitialize =
          layerPropsTimeDim?.currentValue || wmTimeDim?.getValue();
        wmTimeDim &&
          timeValueToInitialize &&
          wmTimeDim.setValue(timeValueToInitialize);

        const refTimeDim = wmLayer?.dimensions?.find(
          (dimension) => dimension.name === 'reference_time',
        );

        // Update reference time dimension value, this will change the time values to prefetch. Depends on model run.
        if (refTimeDim && wmTimeDim) {
          const layerPropsRefTimeDim = dimensions?.find(
            (dimension) => dimension.name === 'reference_time',
          );
          // Set reftime dimension current value based on layerprops dimension value
          refTimeDim &&
            layerPropsRefTimeDim?.currentValue &&
            refTimeDim.setValue(layerPropsRefTimeDim?.currentValue);

          const refTimeToFind = wmTimeDim.getValue();
          const firstRefTime = refTimeDim?.getClosestValue(refTimeToFind, true);

          firstRefTime &&
            refTimeDim &&
            wmTimeDim &&
            wmTimeDim.setTimeValuesForReferenceTime(firstRefTime, refTimeDim);
        }

        // Make sure style is set in wmLayer as well
        styleName && wmLayer.setStyle(styleName);
        onInitializeLayer &&
          onInitializeLayer({
            layerId: wmLayer.id,
            style: wmLayer.currentStyle!,
            dimensions: wmLayer.dimensions.map((dimension) => {
              return {
                name: dimension.name,
                units: dimension.units,
                currentValue: dimension.currentValue,
                maxValue: dimension.getLastValue(),
                minValue: dimension.getFirstValue(),
                synced: dimension.synced,
                values: dimension.values,
                unitSymbol: dimension.unitSymbol,
              };
            }),
          });
      }
    };
    initializeLayer().catch((e) => window.console.error(e));

    return (): void => {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [wmLayer, layerName, visible]);

  // Layer opacity and zIndex useEffect
  useEffect(() => {
    if (!layerOptions || !layer) {
      return;
    }

    if (layerOptions.opacity !== undefined && layerOptions.opacity !== null) {
      layer.setOpacity(layerOptions.opacity);
    }
    if (layerOptions.zIndex !== undefined && layerOptions.zIndex !== null) {
      layer.setZIndex(layerOptions.zIndex);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layer, layerOptions?.zIndex, layerOptions?.opacity]);

  // Layer visible useEffect
  useEffect(() => {
    layer?.setVisible(!(visible === false));
  }, [layer, visible]);

  // Layer styleName properties useEffect
  useEffect(() => {
    wmLayer?.currentStyle !== styleName && wmLayer?.setStyle(styleName || '');
    layer?.getSource()?.setStyle(styleName || '');
  }, [layer, styleName, wmLayer]);

  // layerName  useEffect
  useEffect(() => {
    layer?.getSource()?.setLayerName(layerName);
  }, [layer, layerName]);

  // Layer dim values useEffect,
  useEffect(() => {
    layer?.getSource()?.setDimProps(dimensionsValues);
    dimensions?.forEach((dim) => {
      if (dim.name && dim.currentValue) {
        const layerDim = wmLayer?.getDimension(dim.name);
        layerDim?.setClosestValue(dim.currentValue);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layer, dimensionsValuesEffectCheck, wmLayer]);

  // Prefetch useEffect
  useEffect(() => {
    if (!wmLayer) {
      return;
    }
    const wmTimeDim = wmLayer?.dimensions?.find(
      (dimension) => dimension.name === 'time',
    );
    if (!wmTimeDim) {
      return;
    }
    try {
      const stepsToCheck = getTimeStepsToCheckMemoized(timespan);
      const dimensionValuesToPrefetch = makeDimValuesToPrefetch(
        wmTimeDim,
        stepsToCheck,
        isAnimating,
      );
      layer
        ?.getSource()
        ?.setTimeValuesValuesToPrefetch(dimensionValuesToPrefetch);
    } catch (e) {
      window.console.warn(e);
    }
    // The prefetch useeffect should be triggered when one of the following change:
    // - layer,
    // - wmLayer
    // - timespan.start,
    // - timespan.end,
    // - timespan.step,
    // - layerName,
    // - styleName,
    // - dimensionsValues,
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    layer,
    wmLayer,
    timespan?.start,
    timespan?.end,
    timespan?.step,
    layerName,
    styleName,
    dimensionsValuesEffectCheck,
  ]);

  return debugMode ? (
    <ShowTimeAwareLayerInfo
      wmLayer={wmLayer!}
      stepsToCheck={getTimeStepsToCheckMemoized(timespan)}
      layer={layer!}
      visible={visible}
    />
  ) : null;
};
