/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import TileLayer from 'ol/layer/Tile';
import { XYZ } from 'ol/source';
import { useContext, useEffect, useState } from 'react';

import { LayerFoundation } from '@opengeoweb/webmap';

import MapContext from '../context/MapContext';

export interface XYZLayerProps {
  urlTemplate: string;
  layerOptions: ConstructorParameters<typeof TileLayer>[0];
  layerProps?: LayerFoundation;
  tileProjectionCode?: string;
}

export const XYZLayer: React.FC<XYZLayerProps> = ({
  urlTemplate,
  layerOptions,
  layerProps,
  tileProjectionCode = 'EPSG:3857',
}: XYZLayerProps) => {
  const { map } = useContext(MapContext);
  const [layer, setLayer] = useState<TileLayer>();

  useEffect(() => {
    if (!map) {
      return (): void => {};
    }

    const layer = new TileLayer({
      source: new XYZ({
        url: urlTemplate,
        projection: tileProjectionCode,
      }),
    });

    map.addLayer(layer);
    setLayer(layer);

    return (): void => {
      map.removeLayer(layer);
      setLayer(undefined);
    };
  }, [map, tileProjectionCode, urlTemplate]);

  useEffect(() => {
    if (!layerOptions || !layer) {
      return;
    }

    if (layerOptions.opacity !== undefined && layerOptions.opacity !== null) {
      layer.setOpacity(layerOptions.opacity);
    }
    if (layerOptions.zIndex !== undefined && layerOptions.zIndex !== null) {
      layer.setZIndex(layerOptions.zIndex);
    }
  }, [layer, layerOptions]);

  useEffect(() => {
    if (!layerProps || !layer) {
      return;
    }
    layer.setVisible(!!layerProps.enabled);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [layer, layerProps?.enabled]);

  return null;
};
