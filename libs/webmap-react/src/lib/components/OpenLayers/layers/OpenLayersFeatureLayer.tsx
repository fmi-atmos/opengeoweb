/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Feature } from 'ol';
import GeoJSON from 'ol/format/GeoJSON';
import { StyleLike } from 'ol/style/Style';
import { useMemo } from 'react';

import { FeatureCollection } from 'geojson';
import { Geometry } from 'ol/geom';
import { FeatureLayer } from './FeatureLayer';
import { ClickOnFeature, HoverSelect } from '../utils/types';
import { useProjection } from '../utils/projections';

export interface OpenLayersFeatureLayerProps {
  featureCollection?: FeatureCollection;
  style: StyleLike;
  zIndex?: number;
  hoverSelect?: HoverSelect;
  clickOnFeature?: ClickOnFeature;
  testId?: string;
}

export const OpenLayersFeatureLayer: React.FC<OpenLayersFeatureLayerProps> = ({
  featureCollection,
  ...props
}: OpenLayersFeatureLayerProps) => {
  const projection = useProjection();

  const features = useMemo((): Feature<Geometry>[] => {
    if (!projection || !featureCollection) {
      return [];
    }
    const formatter = new GeoJSON({ featureProjection: projection });
    return formatter.readFeatures(featureCollection);
  }, [featureCollection, projection]);

  return <FeatureLayer features={features} {...props} />;
};
