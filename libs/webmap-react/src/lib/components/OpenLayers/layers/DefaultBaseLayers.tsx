/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React, { ReactNode } from 'react';
import { XYZLayer } from './XYZLayer';
import { WMTSLayer } from './WMTSLayer';
import { TiledWMSLayer } from './TiledWMSLayer';

const layerOptions = {
  zIndex: 1,
  opacity: 1,
};

const layerProps = {
  enabled: true,
};

export enum BaseLayerType {
  LIGHT_GREY_CANVAS,
  WORLD_TOPO,
}

interface DefaultBaseLayersOptions {
  type?: BaseLayerType;
}

export const DefaultBaseLayers: React.FC<DefaultBaseLayersOptions> = ({
  type = BaseLayerType.LIGHT_GREY_CANVAS,
}: DefaultBaseLayersOptions) => {
  const getBase = (): ReactNode => {
    switch (type) {
      case BaseLayerType.WORLD_TOPO:
        return (
          <WMTSLayer
            getCapsURL="https://services.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/WMTS?SERVICE=WMTS&REQUEST=GetCapabilities"
            layerName="World_Topo_Map"
            tileMatrixSet="GoogleMapsCompatible"
            layerOptions={layerOptions}
            layerProps={layerProps}
          />
        );

      case BaseLayerType.LIGHT_GREY_CANVAS:
      default:
        return (
          <XYZLayer
            urlTemplate="https://geoweb-maps-assets.pmc.knmi.cloud/WorldMap_Light_Grey_Canvas/EPSG3857/{z}/{x}/{y}.png"
            layerOptions={layerOptions}
            layerProps={layerProps}
          />
        );
    }
  };

  return (
    <>
      {getBase()}
      <TiledWMSLayer
        url="https://geoservices.knmi.nl/wms?DATASET=baselayers"
        layerName="countryborders"
        styleName=""
        layerOptions={{ zIndex: 999, opacity: 1 }}
        layerProps={{ enabled: true }}
      />
    </>
  );
};
