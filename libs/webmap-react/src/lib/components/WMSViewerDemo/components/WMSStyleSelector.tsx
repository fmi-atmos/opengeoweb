/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2021 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { LayerFoundation, getWMLayerById } from '@opengeoweb/webmap';

interface WMSStyleSelectorProps {
  layer: LayerFoundation;
  onSelectStyle: (layer: LayerFoundation) => void;
}

const WMSStyleSelector = ({
  layer,
  onSelectStyle,
}: WMSStyleSelectorProps): React.ReactElement<WMSStyleSelectorProps> | null => {
  const handleChange = (event: SelectChangeEvent<string>): void => {
    const newLayer: LayerFoundation = { ...layer };
    newLayer.style = event.target.value;
    const wmLayer = getWMLayerById(layer.id!);
    wmLayer.setStyle(newLayer.style);
    wmLayer.parentMap.draw();
    onSelectStyle(newLayer);
  };
  if (!layer || !layer.id) {
    return null;
  }
  const wmLayer = getWMLayerById(layer.id);
  if (!wmLayer) {
    return null;
  }
  const styles = wmLayer.getStyles();
  if (!styles) {
    return null;
  }
  const currentStyle = wmLayer.getStyle();
  const title = 'Style';

  return (
    <Box sx={{ height: '100%' }}>
      <FormControl size="small">
        <InputLabel size="small">{title}</InputLabel>
        <Select
          size="small"
          value={currentStyle}
          label="Style"
          onChange={handleChange}
        >
          {styles.map((l, i) => (
            // eslint-disable-next-line react/no-array-index-key
            <MenuItem key={`${l.name}i${i}`} value={l.name}>
              {l.title}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
};

export default WMSStyleSelector;
