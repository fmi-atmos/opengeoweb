/* eslint-disable react-hooks/rules-of-hooks */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  LayerType,
  LayerProps,
  WMBBOX,
  generateMapId,
  getWMJSMapById,
  getWMLayerById,
  LayerFoundation,
  getWMSGetFeatureInfoRequestURL,
  generateLayerId,
  WMLayer,
  registerWMLayer,
  LayerOptions,
  IWMJSMap,
} from '@opengeoweb/webmap';
import type { Meta, StoryObj } from '@storybook/react';

import axios from 'axios';
import { Grid2 as Grid, Backdrop, CircularProgress } from '@mui/material';
import DOMPurify from 'dompurify';
import { MapView, MapViewLayer, MapViewLayerProps } from '../MapView';

import WMSLayerSelector from './components/WMSLayerSelector';

import { Legend } from '../Legend';
import WMSDimTimeYearSelector, {
  TimeMode,
} from './components/WMSDimTimeYearSelector';
import WMSStyleSelector from './components/WMSStyleSelector';
import WMSTimeSlider from './components/WMSTimeSlider';
import { useQueryGetWMSLayers } from '../../hooks/useHooksGetCapabilities';

type Story = StoryObj;

const selectLayer = async (
  selectedLayer: LayerProps,
  wmsService: string,
): Promise<LayerFoundation> => {
  const wmsLayer = {
    service: wmsService,
    name: selectedLayer.name,
    id: generateLayerId(),
    format: 'image/webp',
    enabled: true,
    layerType: LayerType.mapLayer,
  } as LayerOptions;

  const wmLayer = new WMLayer(wmsLayer);
  registerWMLayer(wmLayer, wmsLayer.id);

  // Use await here since parseLayer returns a Promise
  const newLayer = await wmLayer.parseLayer(false);
  return newLayer;
};

const sanitizeHTML = (
  htmlToBeSanitized: string,
): {
  __html: string;
} => ({
  __html: DOMPurify.sanitize(htmlToBeSanitized, {
    ALLOWED_TAGS: [
      'b',
      'i',
      'em',
      'strong',
      'a',
      'table',
      'td',
      'tr',
      'br',
      'hr',
      'th',
    ],
    ALLOWED_ATTR: ['href'],
  }),
});

const baseLayerWorldMap = {
  name: 'WorldMap',
  title: 'WorldMap',
  type: 'twms',
  baseLayer: true,
  enabled: true,
  id: generateLayerId(),
};

export const DemoDemoWMSViewerStory: Story = {
  render: () => {
    const [loading, setLoading] = React.useState(false);
    const [mapId, setMapId] = React.useState('');

    const [layer, setLayer] = React.useState<LayerFoundation | null>(null);

    const [selectedISOTime, setISOTime] = React.useState('');

    const [gfiInfo, setGFIInfo] = React.useState<string>('');

    const wmsService =
      'https://adaguc-server-ecad-adaguc.pmc.knmi.cloud/adaguc-server?DATASET=eobs_v27.0e&SERVICE=WMS';
    const wmsBaseLayerService =
      'https://adaguc-server-ecad-adaguc.pmc.knmi.cloud/adaguc-server?DATASET=baselayers&SERVICE=WMS';
    const availableLayers = useQueryGetWMSLayers(wmsService);

    React.useEffect(() => {
      if (availableLayers.length) {
        selectLayer(availableLayers[0], wmsService)
          .then(setLayer)
          .catch(() => {});
      }
    }, [availableLayers]);

    React.useEffect(() => {
      setLoading(true);

      // Generate a mapId
      setMapId(generateMapId());
      selectLayer(availableLayers[0], wmsService)
        .then(setLayer)
        .catch(() => {});

      setLoading(false);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    React.useEffect(() => {
      if (layer) {
        const wmLayer = getWMLayerById(layer.id!);
        if (wmLayer) {
          const timeDim = wmLayer.getDimension('time');
          if (timeDim) {
            setISOTime(timeDim.getValue());
          }
        }
      }
    }, [layer]);

    React.useEffect(() => {
      if (layer && selectedISOTime) {
        const wmLayer = getWMLayerById(layer.id!);
        const timeDim = wmLayer.getDimension('time');
        wmLayer.parentMap.setDimension('time', selectedISOTime);
        timeDim!.setValue(selectedISOTime);
        wmLayer.parentMap.draw();
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedISOTime]);

    const handleMapClick = (map: IWMJSMap, event: Event): void => {
      // eslint-disable-next-line no-console
      if (map.getLayers().length < 1) {
        return;
      }
      const tevent = event as unknown as {
        mouseX: number;
        mouseY: number;
      };
      map.mapPin.showMapPin();

      const url = getWMSGetFeatureInfoRequestURL(
        map.getLayers()[0],
        tevent.mouseX,
        tevent.mouseY,
        'text/html',
      );
      setGFIInfo('Loading...');
      axios
        .get(url)
        .then((result) => {
          setGFIInfo(result.data);
        })
        .catch(() => {});
    };

    if (!layer) {
      return <Grid>Loading map and layers...</Grid>;
    }

    const layerId = layer.id!;

    return (
      <Grid container direction="column" style={{ background: 'white' }}>
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={loading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <Grid container style={{ height: '100%' }}>
          <Grid container direction="column" size={5}>
            {/* Left menu */}
            <Grid container direction="row" sx={{ height: '100%' }}>
              <Grid container direction="column" sx={{ mr: 2 }} size={12}>
                <Grid container direction="row" sx={{ mb: 2 }}>
                  <Grid sx={{ ml: 2, mt: 2 }}>
                    <WMSLayerSelector
                      layer={layer}
                      layers={availableLayers}
                      onSelectLayer={(selectedLayer) => {
                        selectLayer(selectedLayer, wmsService)
                          .then(setLayer)
                          .catch(() => {});
                      }}
                    />
                  </Grid>

                  <Grid sx={{ ml: 2, mt: 2, width: '100%' }}>
                    <Grid sx={{ ml: 0, mt: 2, mr: 0, width: '100%' }}>
                      <WMSTimeSlider
                        selectedISOTime={selectedISOTime}
                        layerId={layerId}
                        onSelectTime={setISOTime}
                      />
                    </Grid>

                    <Grid container>
                      <WMSDimTimeYearSelector
                        selectedISOTime={selectedISOTime}
                        layerId={layerId}
                        onSelectTime={setISOTime}
                        mode={TimeMode.YEAR}
                      />
                      <WMSDimTimeYearSelector
                        selectedISOTime={selectedISOTime}
                        layerId={layerId}
                        onSelectTime={setISOTime}
                        mode={TimeMode.MONTH}
                      />

                      <WMSDimTimeYearSelector
                        selectedISOTime={selectedISOTime}
                        layerId={layerId}
                        onSelectTime={setISOTime}
                        mode={TimeMode.DAY}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Grid sx={{ ml: 2, mt: 2, width: '100%' }}>
                  <WMSStyleSelector
                    layer={layer}
                    onSelectStyle={(data) => {
                      setLayer(data);
                    }}
                  />
                </Grid>
                <Grid style={{ margin: '20px' }}>
                  <Legend layer={layer} />
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid
            container
            direction="column"
            style={{ overflow: 'hidden' }}
            size={7}
          >
            <MapView
              mapId={mapId}
              shouldAutoFetch={false}
              listeners={[
                {
                  name: 'onmapready',
                  keep: false,
                  data: null!,
                  callbackfunction: (): void => {
                    setTimeout(() => {
                      const wmjsMap = getWMJSMapById(mapId)!;
                      wmjsMap.setProjection(
                        'EPSG:32661',
                        new WMBBOX(
                          -771068.23748231,
                          -4835133.782045104,
                          5689551.034721555,
                          980464.571049,
                        ),
                      );
                      wmjsMap.draw();
                    }, 100);
                  },
                },
                {
                  name: 'beforemouseup',
                  data: null!,
                  keep: true,
                  callbackfunction: (map: IWMJSMap, event): boolean => {
                    handleMapClick(map, event as unknown as Event);
                    return true;
                  },
                },
              ]}
            >
              <MapViewLayer {...baseLayerWorldMap} />

              <MapViewLayer
                {...(layer as unknown as MapViewLayerProps)}
                id={layerId}
              />
              <MapViewLayer
                {...({
                  service: wmsBaseLayerService,
                  name: 'overlay_europe',
                  format: 'image/webp',
                  enabled: true,
                  layerType: LayerType.overLayer,
                } as unknown as MapViewLayerProps)}
                id="overlay"
              />
              <MapViewLayer
                {...({
                  service: wmsBaseLayerService,
                  name: 'graticules10',
                  format: 'image/webp',
                  enabled: true,
                  layerType: LayerType.overLayer,
                } as unknown as MapViewLayerProps)}
                id="graticules10"
              />
            </MapView>
          </Grid>
        </Grid>
        <Grid container style={{ overflow: 'hidden' }}>
          <div
            style={{
              fontSize: '11px',
              padding: '6px',
              margin: '0',
              fontFamily: 'Roboto,Helvetica,Arial,sans-serif',
              fontWeight: '400',
              lineHeight: '1.75',
              letterSpacing: '0.25px',
            }}
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={sanitizeHTML(gfiInfo)}
          />
        </Grid>
      </Grid>
    );
  },
  tags: ['!autodocs'],
};

DemoDemoWMSViewerStory.storyName = 'WMSViewer';

const storyParams = {
  parameters: {
    docs: {
      description: {
        story: 'MapView component with radarlayer and overlayer.',
      },
    },
  },
};

export const MapComponentDemo: Story = {
  ...storyParams,
  render: () => (
    <div style={{ width: '800px', height: '800px' }}>
      <MapView
        mapId="demo"
        shouldAutoFetch={false}
        srs="EPSG:28992"
        bbox={{
          left: -475000,
          bottom: -210000,
          right: 750000,
          top: 1010000,
        }}
      >
        <MapViewLayer {...baseLayerWorldMap} />

        <MapViewLayer
          id="radarlayer"
          service="https://geoservices.knmi.nl/adagucserver?dataset=RADAR&"
          name="Reflectivity"
        />
        <MapViewLayer
          id="overlay"
          service="https://geoservices.knmi.nl/adagucserver?dataset=baselayers&"
          name="overlay_europe"
          layerType={LayerType.overLayer}
        />
      </MapView>
    </div>
  ),
};

MapComponentDemo.storyName = 'Map';

const meta: Meta<typeof DemoDemoWMSViewerStory> = {
  title: 'Demo',
  parameters: {
    docs: {
      description: {
        component:
          'A demo of opengeoweb combined components from <b>opengeoweb/webmap-react</b>. ',
      },
    },
  },
};
export default meta;
