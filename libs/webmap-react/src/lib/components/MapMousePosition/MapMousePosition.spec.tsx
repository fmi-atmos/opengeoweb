/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen, act } from '@testing-library/react';
import { Map } from 'ol';
import { fromLonLat } from 'ol/proj';
import MapContext from '../OpenLayers/context/MapContext';

import { MapMousePosition } from './MapMousePosition';

describe('src/components/MapMousePosition/MapMousePosition', () => {
  it('should show mouse map position', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any, no-unused-vars
    let onPointerMoveMockFunction = (event: any): void => {};

    const mapProjection = 'EPSG:3857';
    const pointIn4326 = [5.2, 52.0];
    const pointInMapProj = fromLonLat(pointIn4326, mapProjection);

    const map: Map = {
      getView: () => {
        return {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any, no-unused-vars
          getProjection: (): any => {
            return {
              getCode: jest.fn().mockImplementationOnce((): string => {
                return mapProjection;
              }),
              getUnits: jest.fn().mockImplementationOnce((): string => {
                return 'm';
              }),
            };
          },
        };
      },
      on: (name: string, f: VoidFunction) => {
        onPointerMoveMockFunction = f;
      },
      getViewport: () => {
        return document.createElement('div');
      },
    } as unknown as Map;

    const { baseElement } = render(
      <MapContext.Provider value={{ map }}>
        <MapMousePosition />
      </MapContext.Provider>,
    );

    expect(baseElement).toBeTruthy();

    expect(
      screen.getByTestId('map-mouse-position-typography').textContent,
    ).toContain('EPSG:3857');
    expect(
      screen.getByTestId('map-mouse-position-typography').textContent,
    ).toContain('--');

    act(() => {
      onPointerMoveMockFunction({ coordinate: pointInMapProj });
    });
    const projText = screen.getByTestId(
      'map-mouse-position-typography',
    ).textContent;
    expect(projText).toBe(
      `${mapProjection}: (${pointInMapProj[0].toFixed(0)}, ${pointInMapProj[1].toFixed(0)}) mlonlat: ${pointIn4326[0].toFixed(2)}, ${pointIn4326[1].toFixed(2)} deg`,
    );
  });
});
