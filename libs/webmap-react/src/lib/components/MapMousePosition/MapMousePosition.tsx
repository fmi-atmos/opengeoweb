/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { useContext, useState } from 'react';
import { Typography, Box } from '@mui/material';
import { toLonLat } from 'ol/proj';
import { unByKey } from 'ol/Observable';
import { listen, unlistenByKey } from 'ol/events';
import EventType from 'ol/pointer/EventType';
import MapContext from '../OpenLayers/context/MapContext';

interface MousePosition {
  projection: string;
  mousePosition: string;
  mousePositionLatLon: string;
}

export const MapMousePosition: React.FC = () => {
  const { map } = useContext(MapContext);

  const projection = map?.getView().getProjection().getCode();

  const [mousePosition, setMousePosition] = useState<MousePosition>();

  React.useEffect(() => {
    if (!map) {
      return (): void => {};
    }

    let inside = true;

    setMousePosition({
      mousePosition: '--',
      mousePositionLatLon: '---',
      projection: map?.getView().getProjection().getCode() || '??',
    });

    const handleMouseOut = (): void => {
      inside = false;
      setMousePosition({
        mousePosition: '--',
        mousePositionLatLon: '---',
        projection: map?.getView().getProjection().getCode() || '??',
      });
    };
    const handleMouseIn = (): void => {
      inside = true;
    };

    const eventKey = map.on('pointermove', (evt) => {
      if (inside) {
        const projection = map?.getView().getProjection().getCode() || '';
        const units = map.getView().getProjection().getUnits();
        const precision = units === 'm' ? 0 : 2;
        const coordValue = `(${evt.coordinate[0].toFixed(precision)}, ${evt.coordinate[1].toFixed(precision)}) ${units}`;
        const lonlat = toLonLat(evt.coordinate, map.getView().getProjection());
        const lonlatValue = `${lonlat[0].toFixed(2)}, ${lonlat[1].toFixed(2)} deg`;
        setMousePosition({
          projection,
          mousePosition: coordValue,
          mousePositionLatLon: lonlatValue,
        });
      }
    });
    const viewPort = map.getViewport();
    const outkey = listen(viewPort, EventType.POINTERLEAVE, handleMouseOut);
    const inkey = listen(viewPort, EventType.POINTERENTER, handleMouseIn);

    return (): void => {
      unByKey(eventKey);
      unlistenByKey(outkey);
      unlistenByKey(inkey);
    };
  }, [map, projection]);

  if (mousePosition) {
    return (
      <Box
        data-testid="map-mouse-position"
        sx={{
          zIndex: 10,
          justifySelf: 'center',
          bottom: '32px',
          left: '2px',
          position: 'absolute',
        }}
      >
        <Typography
          data-testid="map-mouse-position-typography"
          sx={{
            fontSize: 9,
            textShadow:
              '1px 1px 2px  #ffffffE6, -1px 1px 2px #ffffffE6, -1px -1px 2px  #ffffffE6, 1px -1px 2px #ffffffE6',
          }}
        >
          {mousePosition?.projection}: {mousePosition?.mousePosition}
          <br />
          {`lonlat: ${mousePosition?.mousePositionLatLon}`}
        </Typography>
      </Box>
    );
  }
  return null;
};
