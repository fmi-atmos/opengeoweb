/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  Grid2 as Grid,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  SelectChangeEvent,
  Icon,
} from '@mui/material';

interface FeatureLayersProps {
  geojson: GeoJSON.FeatureCollection;
  onChangeLayerIndex: (newIndex: number, feature: GeoJSON.Feature) => void;
  activeFeatureLayerIndex: number;
  getToolIcon: (selectionType: string) => React.ReactElement;
}

const FeatureLayers: React.FC<FeatureLayersProps> = ({
  geojson,
  onChangeLayerIndex,
  activeFeatureLayerIndex,
  getToolIcon,
}: FeatureLayersProps) => {
  const featureLayerList = geojson
    ? Array.from(Array(geojson.features.length).keys()).map((index) => ({
        index,
        value: geojson.features[index].properties?.selectionType,
        label: `${index + 1}: ${geojson.features[index].geometry.type}`,
      }))
    : [];

  return (
    <Grid size={{ sm: 12 }}>
      <FormControl
        disabled={!featureLayerList.length}
        variant="filled"
        sx={{ width: '100%' }}
      >
        <InputLabel id="demo-feature-number">Features</InputLabel>
        <Select
          labelId="demo-feature-type"
          value={(featureLayerList.length
            ? activeFeatureLayerIndex
            : ''
          ).toString()}
          onChange={(event: SelectChangeEvent): void => {
            const featureNr = parseInt(event.target.value, 10);
            onChangeLayerIndex(featureNr, geojson.features[featureNr]);
          }}
          sx={{ '.MuiSelect-select': { display: 'flex' } }}
        >
          {featureLayerList.map((listItem) => {
            return (
              <MenuItem key={listItem.index} value={listItem.index}>
                <Icon sx={{ transform: 'scale(0.9)' }}>
                  {getToolIcon(listItem.value)}
                </Icon>
                {listItem.label}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </Grid>
  );
};

export default FeatureLayers;
