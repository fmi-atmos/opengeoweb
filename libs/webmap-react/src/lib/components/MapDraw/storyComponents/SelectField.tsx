/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  Box,
  FormControl,
  InputLabel,
  Select,
  SelectChangeEvent,
  MenuItem,
} from '@mui/material';

export const SelectField: React.FC<{
  label: string;
  value: string;
  onChangeValue: (value: string) => void;
  options: string[];
  width?: string;
  showValueAsColor?: boolean;
  labelSuffix?: string;
}> = ({
  label,
  value,
  onChangeValue,
  options = [],
  width = '50%',
  showValueAsColor = false,
  labelSuffix = '',
}) => {
  const onChange = (event: SelectChangeEvent): void => {
    const { value } = event.target;
    onChangeValue(value);
  };

  const getLabel = (currentValue: string): string =>
    `${currentValue}${labelSuffix}`;

  return (
    <FormControl variant="filled" sx={{ width }}>
      <InputLabel>{label}</InputLabel>
      <Select value={value} onChange={onChange}>
        {options.map((value) => (
          <MenuItem value={value} key={value}>
            {showValueAsColor ? (
              <Box sx={{ backgroundColor: value, width: 20, height: 20 }} />
            ) : (
              getLabel(value.toString())
            )}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};
