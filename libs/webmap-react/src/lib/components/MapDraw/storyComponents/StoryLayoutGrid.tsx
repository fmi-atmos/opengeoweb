/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Grid2 as Grid, Box, Paper } from '@mui/material';

export const StoryLayoutGrid: React.FC<{
  children: React.ReactNode;
  mapComponent: React.ReactNode;
}> = ({ mapComponent, children }) => {
  return (
    <Paper>
      <Box
        sx={{
          display: 'grid',
          gridTemplateColumns: '70% 30%',
          gridTemplateRows: '150px 1fr',
          gridTemplateAreas: "'map controls'\n'map textarea'",
          width: '100%',
          height: '100vh',
        }}
      >
        <Box sx={{ gridArea: 'map', width: '100%', height: '100%' }}>
          {mapComponent}
        </Box>
        <Box
          sx={{
            gridArea: 'controls',
            width: '100%',
            height: '100%',
            padding: 2,
          }}
        >
          <Grid spacing={2} container>
            {children}
          </Grid>
        </Box>
      </Box>
    </Paper>
  );
};
