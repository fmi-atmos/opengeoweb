/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { DRAWMODE } from '../MapDraw/MapDraw';
import { emptyGeoJSON } from '../MapDraw/geojsonShapes';
import {
  NEW_FEATURE_CREATED,
  NEW_LINESTRING_CREATED,
  NEW_POINT_CREATED,
} from '../MapDraw/mapDrawUtils';
import {
  addFeatureProperties,
  addGeoJSONProperties,
  addSelectionTypeToGeoJSON,
  createInterSections,
  getFeatureCollection,
  getFeatureExtent,
  getGeoJSONPropertyValue,
  getGeoJson,
  intersectPointGeoJSONS,
  intersectPolygonGeoJSONS,
  isGeoJSONFeatureCreatedByTool,
  isGeoJSONGeometryEmpty,
  moveFeature,
  rewindGeometry,
} from './utils';

describe('addFeatureProperties', () => {
  it('should add properties to geojson', () => {
    const featureProperties = {
      stroke: '#FF0022',
      'stroke-width': 100.0,
      'stroke-opacity': 10,
      fill: '#FF00FF',
      'fill-opacity': 0.3,
    };

    expect(
      addFeatureProperties(
        undefined as unknown as GeoJSON.FeatureCollection,
        featureProperties,
      ),
    ).toEqual(null);

    const simplePolygonGeoJSONHalfOfNL: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.921875, 51.767839887322154],
                [4.7900390625, 49.97242235423708],
                [7.679443359375, 49.79544988802771],
                [7.888183593749999, 51.74743863117572],
                [5.86669921875, 51.984880139916626],
                [4.921875, 51.767839887322154],
              ],
            ],
          },
        },
      ],
    };
    expect(
      addFeatureProperties(simplePolygonGeoJSONHalfOfNL, featureProperties),
    ).toEqual({
      ...simplePolygonGeoJSONHalfOfNL,
      features: [
        {
          ...simplePolygonGeoJSONHalfOfNL.features[0],
          properties: featureProperties,
        },
      ],
    });
  });

  it('should add properties to geojson with given featureIndex', () => {
    const featureProperties = {
      stroke: '#FF0022',
      'stroke-width': 100.0,
      'stroke-opacity': 10,
      fill: '#FF00FF',
      'fill-opacity': 0.3,
    };

    const testFeature1: GeoJSON.Feature = {
      type: 'Feature',
      properties: {
        testProp: 'feature-1',
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [4.921875, 51.767839887322154],
            [4.7900390625, 49.97242235423708],
            [7.679443359375, 49.79544988802771],
            [7.888183593749999, 51.74743863117572],
            [5.86669921875, 51.984880139916626],
            [4.921875, 51.767839887322154],
          ],
        ],
      },
    };

    const testFeature2: GeoJSON.Feature = {
      type: 'Feature',
      properties: {
        testProp: 'feature-2',
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [4.921875, 51.767839887322154],
            [4.7900390625, 49.97242235423708],
            [7.679443359375, 49.79544988802771],
            [7.888183593749999, 51.74743863117572],
            [5.86669921875, 51.984880139916626],
            [4.921875, 51.767839887322154],
          ],
        ],
      },
    };

    const testFeatureCollection: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [testFeature1, testFeature2],
    };

    const resultIndex0 = addFeatureProperties(
      testFeatureCollection,
      featureProperties,
      0,
    );

    expect(resultIndex0.features[0].properties).toEqual({
      ...testFeature1.properties,
      ...featureProperties,
    });

    expect(resultIndex0.features[1].properties).toEqual(
      testFeature2.properties,
    );

    const resultIndex1 = addFeatureProperties(
      testFeatureCollection,
      featureProperties,
      1,
    );

    expect(resultIndex1.features[0].properties).toEqual(
      testFeature1.properties,
    );
    expect(resultIndex1.features[1].properties).toEqual({
      ...testFeature2.properties,
      ...featureProperties,
    });

    const resultOutOfBound = addFeatureProperties(
      testFeatureCollection,
      featureProperties,
      2,
    );

    expect(resultOutOfBound.features[0].properties).toEqual(
      testFeature1.properties,
    );
    expect(resultOutOfBound.features[1].properties).toEqual(
      testFeature2.properties,
    );
  });
});

describe('getGeoJSONPropertyValue', () => {
  it('should return the property value if it exists in properties', () => {
    const property = 'fill';
    const properties = { [property]: '#FF0000' };
    const polygonDrawMode = undefined;
    const result = getGeoJSONPropertyValue(
      property,
      properties,
      polygonDrawMode,
    );
    expect(result).toBe('#FF0000');

    const numberZeroProperty = 'fill-opacity';
    const properties2 = { [numberZeroProperty]: 0 };
    const result2 = getGeoJSONPropertyValue(
      numberZeroProperty,
      properties2,
      polygonDrawMode,
    );
    expect(result2).toBe(0);
  });

  it('should return the polygonDrawMode property value if it exists', () => {
    const property = 'stroke-width';
    const properties = { 'fill-color': '#FF0000' };
    const polygonDrawMode = {
      shape: {
        type: 'Feature',
        properties: { 'stroke-width': 20 },
        geometry: {
          type: 'Point',
          coordinates: [],
        },
      } as GeoJSON.Feature,
      value: DRAWMODE.POINT,
      title: 'Point',
      isSelectable: true,
      drawModeId: 'drawtools-point',
      selectionType: 'point',
    };
    const result = getGeoJSONPropertyValue(
      property,
      properties,
      polygonDrawMode,
    );
    expect(result).toBe(20);

    const numberZeroProperty = 'fill-opacity';
    const pointDrawMode = {
      shape: {
        type: 'Feature',
        properties: { [numberZeroProperty]: 0 },
        geometry: {
          type: 'Point',
          coordinates: [],
        },
      } as GeoJSON.Feature,
      value: DRAWMODE.POINT,
      title: 'Point',
      isSelectable: true,
      drawModeId: 'drawtools-point',
      selectionType: 'point',
    };
    const result2 = getGeoJSONPropertyValue(
      numberZeroProperty,
      properties,
      pointDrawMode,
    );
    expect(result2).toBe(0);
  });

  it('should return the defaultStyle property value if neither properties nor polygonDrawMode has it', () => {
    const property = 'fill';
    const properties = {};
    const polygonDrawMode = undefined;
    const defaultStyleProperties = {
      fill: '#00FF22',
    };

    const result = getGeoJSONPropertyValue(
      property,
      properties,
      polygonDrawMode,
      defaultStyleProperties,
    );
    expect(result).toBe('#00FF22');
  });
});

describe('moveFeature', () => {
  it('should move a Polygon feature', () => {
    const currentGeoJSON = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [9.288704328609223, 54.90546620985156],
                [9.205463858085473, 53.66377913029494],
                [6.60651138951073, 53.94778738589177],
                [9.288704328609223, 54.90546620985156],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const newGeoJSON = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [9.288704328609223, 54.90546620985156],
                [9.205463858085473, 53.66377913029494],
                [6.60651138951073, 53.94778738589177],
                [9.288704328609223, 54.90546620985156],
              ],
              [[9.288704328609223, 54.90546620985156]],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const featureLayerIndex = 0;
    const reason = NEW_FEATURE_CREATED;

    const initialFeatureCount = newGeoJSON.features.length;

    const result = moveFeature(
      currentGeoJSON,
      newGeoJSON,
      featureLayerIndex,
      reason,
    );

    expect(result).toBeDefined();
    expect(result).toEqual(initialFeatureCount);
    expect(newGeoJSON.features.length).toBe(initialFeatureCount + 1);

    expect(
      moveFeature(currentGeoJSON, newGeoJSON, featureLayerIndex, 'other'),
    ).toBeUndefined();
  });

  it('should move a LineString feature', () => {
    const currentGeoJSON = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            selectionType: 'linestring',
          },
          geometry: {
            type: 'LineString',
            coordinates: [
              [7.374173506563056, 53.95323023838055],
              [8.419303858694539, 53.383360912321336],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const newGeoJSON = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            selectionType: 'linestring',
          },
          geometry: {
            type: 'LineString',
            coordinates: [
              [7.374173506563056, 53.95323023838055],
              [8.419303858694539, 53.383360912321336],
              [5.857347154797013, 53.250756446814684],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const featureLayerIndex = 0;
    const reason = NEW_LINESTRING_CREATED;

    const initialFeatureCount = newGeoJSON.features.length;

    const result = moveFeature(
      currentGeoJSON,
      newGeoJSON,
      featureLayerIndex,
      reason,
    );

    expect(result).toBeDefined();
    expect(result).toEqual(initialFeatureCount);
    expect(newGeoJSON.features.length).toBe(initialFeatureCount + 1);

    expect(
      moveFeature(currentGeoJSON, newGeoJSON, featureLayerIndex, 'other'),
    ).toBeUndefined();
  });

  it('should move a Point feature', () => {
    const currentGeoJSON = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            selectionType: 'point',
          },
          geometry: {
            type: 'Point',
            coordinates: [5.5058873903634185, 53.12329030918671],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const newGeoJSON = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            selectionType: 'point',
          },
          geometry: {
            type: 'Point',
            coordinates: [53.12329030918671, 5.5058873903634185],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const featureLayerIndex = 0;
    const reason = NEW_POINT_CREATED;

    const initialFeatureCount = newGeoJSON.features.length;
    const feature1ExpectedResult = { ...newGeoJSON.features[0] };
    const feature2ExpectedResult = { ...currentGeoJSON.features[0] };

    const result = moveFeature(
      currentGeoJSON,
      newGeoJSON,
      featureLayerIndex,
      reason,
    );

    expect(result).toBeDefined();
    expect(result).toEqual(initialFeatureCount);
    expect(newGeoJSON.features.length).toBe(initialFeatureCount + 1);
    expect(newGeoJSON.features[0]).toEqual(feature1ExpectedResult);
    expect(newGeoJSON.features[1]).toEqual(feature2ExpectedResult);

    expect(
      moveFeature(currentGeoJSON, newGeoJSON, featureLayerIndex, 'other'),
    ).toBeUndefined();
  });

  it('should should add selectionType property to geoJSON properties if given', () => {
    const currentGeoJSON = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            selectionType: 'point',
          },
          geometry: {
            type: 'Point',
            coordinates: [5.5058873903634185, 53.12329030918671],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const testGeoJSONPoint = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
          },
          geometry: {
            type: 'Point',
            coordinates: [53.12329030918671, 5.5058873903634185],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const featureLayerIndex = 0;
    const reason = NEW_POINT_CREATED;
    const selectionType = 'my-selectionType';

    const initialFeatureCount = testGeoJSONPoint.features.length;
    const feature1ExpectedResult = { ...testGeoJSONPoint.features[0] };

    const result = moveFeature(
      currentGeoJSON,
      testGeoJSONPoint,
      featureLayerIndex,
      reason,
      selectionType,
    );

    expect(result).toBeDefined();
    expect(result).toEqual(initialFeatureCount);
    expect(testGeoJSONPoint.features.length).toBe(initialFeatureCount + 1);
    expect(testGeoJSONPoint.features[0]).toEqual(feature1ExpectedResult);
    expect(testGeoJSONPoint.features[1]!.properties!.selectionType).toEqual(
      selectionType,
    );

    expect(
      moveFeature(currentGeoJSON, testGeoJSONPoint, featureLayerIndex, 'other'),
    ).toBeUndefined();
  });
});

describe('createInterSections', () => {
  it('should create intersection', () => {
    const shape1: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.0345509144369665, 53.22982420540454],
                [4.831214873562956, 53.22982420540454],
                [4.831214873562956, 53.99777389652837],
                [4.831214873562956, 53.22982420540454],
                [3.6030246032437225, 53.22982420540454],
              ],
            ],
          },
        },
      ],
    };

    const shape2: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.0345509144369665, 52.79449982750051],
                [4.0345509144369665, 53.56244951862434],
                [5.2627411847562, 53.56244951862434],
                [5.2627411847562, 52.79449982750051],
                [4.0345509144369665, 52.79449982750051],
              ],
            ],
          },
        },
      ],
    };

    expect(createInterSections(shape1, shape2)).toEqual({
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#f24a00',
            'stroke-width': 1.5,
            'stroke-opacity': 1,
            fill: '#f24a00',
            'fill-opacity': 0.5,
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.0345509144369665, 53.22982420540454],
                [4.831214873562956, 53.22982420540454],
                [4.831214873562956, 53.99777389652837],
                [4.831214873562956, 53.22982420540454],
                [3.6030246032437225, 53.22982420540454],
              ],
            ],
          },
        },
      ],
    });
  });

  it('should create intersection with custom geoJSON properties', () => {
    const shape1: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.0345509144369665, 53.22982420540454],
                [4.831214873562956, 53.22982420540454],
                [4.831214873562956, 53.99777389652837],
                [4.831214873562956, 53.22982420540454],
                [3.6030246032437225, 53.22982420540454],
              ],
            ],
          },
        },
      ],
    };

    const shape2: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.0345509144369665, 52.79449982750051],
                [4.0345509144369665, 53.56244951862434],
                [5.2627411847562, 53.56244951862434],
                [5.2627411847562, 52.79449982750051],
                [4.0345509144369665, 52.79449982750051],
              ],
            ],
          },
        },
      ],
    };

    const shapeProperties = {
      stroke: '#FF4a00',
      'stroke-width': 2.5,
      'stroke-opacity': 2,
      fill: '#f24aFF',
      'fill-opacity': 0.7,
    };

    expect(createInterSections(shape1, shape2, shapeProperties)).toEqual({
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: shapeProperties,
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.0345509144369665, 53.22982420540454],
                [4.831214873562956, 53.22982420540454],
                [4.831214873562956, 53.99777389652837],
                [4.831214873562956, 53.22982420540454],
                [3.6030246032437225, 53.22982420540454],
              ],
            ],
          },
        },
      ],
    });
  });
});

describe('intersectPolygonGeoJSONS', () => {
  const firShape: GeoJSON.FeatureCollection = {
    ...emptyGeoJSON,
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [5.0, 55.0],
              [4.331914, 55.332644],
              [3.368817, 55.764314],
              [2.761908, 54.379261],
              [3.15576, 52.913554],
              [2.000002, 51.500002],
              [3.370001, 51.369722],
              [3.370527, 51.36867],
              [3.362223, 51.320002],
              [3.36389, 51.313608],
              [3.373613, 51.309999],
              [3.952501, 51.214441],
              [4.397501, 51.452776],
              [5.078611, 51.391665],
              [5.848333, 51.139444],
              [5.651667, 50.824717],
              [6.011797, 50.757273],
              [5.934168, 51.036386],
              [6.222223, 51.361666],
              [5.94639, 51.811663],
              [6.405001, 51.830828],
              [7.053095, 52.237764],
              [7.031389, 52.268885],
              [7.063612, 52.346109],
              [7.065557, 52.385828],
              [7.133055, 52.888887],
              [7.14218, 52.898244],
              [7.191667, 53.3],
              [6.5, 53.666667],
              [6.500002, 55.000002],
              [5.0, 55.0],
            ],
          ],
        },
        properties: {
          selectionType: 'fir',
        },
      },
    ],
  };
  const simplePolygonGeoJSONHalfOfNL: GeoJSON.FeatureCollection = {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [4.921875, 51.767839887322154],
              [4.7900390625, 49.97242235423708],
              [7.679443359375, 49.79544988802771],
              [7.888183593749999, 51.74743863117572],
              [5.86669921875, 51.984880139916626],
              [4.921875, 51.767839887322154],
            ],
          ],
        },
      },
    ],
  };
  it('should run intersectPolygonGeoJSONS successfully', () => {
    const intersection = intersectPolygonGeoJSONS(
      simplePolygonGeoJSONHalfOfNL,
      firShape,
    );
    expect(intersection).toBeTruthy();

    const featureResult = intersection
      .features[0] as GeoJSON.Feature<GeoJSON.Polygon>;

    expect(featureResult.properties!.fill).toEqual('#0000FF');
    expect(featureResult.geometry.coordinates[0].length).toBe(13);
  });

  it('should run intersectPolygonGeoJSONS successfully with custom styling', () => {
    const customStyling = {
      stroke: '#FF0022',
      'stroke-width': 100.0,
      'stroke-opacity': 10,
      fill: '#FF00FF',
      'fill-opacity': 0.3,
    };
    const intersection = intersectPolygonGeoJSONS(
      simplePolygonGeoJSONHalfOfNL,
      firShape,
      customStyling,
    );

    expect(intersection).toBeTruthy();

    const featureResult = intersection
      .features[0] as GeoJSON.Feature<GeoJSON.Polygon>;

    expect(featureResult.properties!.stroke).toEqual(customStyling.stroke);
    expect(featureResult.properties!['stroke-width']).toEqual(
      customStyling['stroke-width'],
    );
    expect(featureResult.properties!['stroke-opacity']).toEqual(
      customStyling['stroke-opacity'],
    );
    expect(featureResult.properties!.fill).toEqual(customStyling.fill);
    expect(featureResult.properties!['fill-opacity']).toEqual(
      customStyling['fill-opacity'],
    );
    expect(featureResult.geometry.coordinates[0].length).toBe(13);
  });
});

describe('intersectPointGeoJSONS', () => {
  const firShape: GeoJSON.FeatureCollection = {
    ...emptyGeoJSON,
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [5.0, 55.0],
              [4.331914, 55.332644],
              [3.368817, 55.764314],
              [2.761908, 54.379261],
              [3.15576, 52.913554],
              [2.000002, 51.500002],
              [3.370001, 51.369722],
              [3.370527, 51.36867],
              [3.362223, 51.320002],
              [3.36389, 51.313608],
              [3.373613, 51.309999],
              [3.952501, 51.214441],
              [4.397501, 51.452776],
              [5.078611, 51.391665],
              [5.848333, 51.139444],
              [5.651667, 50.824717],
              [6.011797, 50.757273],
              [5.934168, 51.036386],
              [6.222223, 51.361666],
              [5.94639, 51.811663],
              [6.405001, 51.830828],
              [7.053095, 52.237764],
              [7.031389, 52.268885],
              [7.063612, 52.346109],
              [7.065557, 52.385828],
              [7.133055, 52.888887],
              [7.14218, 52.898244],
              [7.191667, 53.3],
              [6.5, 53.666667],
              [6.500002, 55.000002],
              [5.0, 55.0],
            ],
          ],
        },
        properties: {
          selectionType: 'fir',
        },
      },
    ],
  };
  const simplePointGeoJSON: GeoJSON.FeatureCollection<GeoJSON.Point> = {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'Point',
          coordinates: [4.921875, 51.767839887322154],
        },
      },
    ],
  };

  const nonIntersectingPointGeoJSON: GeoJSON.FeatureCollection<GeoJSON.Point> =
    {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Point',
            coordinates: [60, 60],
          },
        },
      ],
    };

  it('should return the original point when it intersects the firShape', () => {
    const intersection = intersectPointGeoJSONS(simplePointGeoJSON, firShape);
    expect(intersection).toBeTruthy();

    const featureResult = intersection
      .features[0] as GeoJSON.Feature<GeoJSON.Point>;

    expect(featureResult.properties!.fill).toEqual('#0000FF');
    expect(featureResult.geometry.coordinates).toBe(
      simplePointGeoJSON.features[0].geometry.coordinates,
    );
  });

  it('should return a invalid point when the point does not intersect the firShape', () => {
    const intersection = intersectPointGeoJSONS(
      nonIntersectingPointGeoJSON,
      firShape,
    );
    expect(intersection).toBeTruthy();

    const featureResult = intersection
      .features[0] as GeoJSON.Feature<GeoJSON.Point>;

    expect(featureResult.properties!.fill).toEqual('#0000FF');
    expect(featureResult.geometry.coordinates).toEqual([]);
  });

  it('should run intersectPointGeoJSONS successfully with custom styling', () => {
    const customStyling = {
      stroke: '#FF0022',
      'stroke-width': 100.0,
      'stroke-opacity': 10,
      fill: '#FF00FF',
      'fill-opacity': 0.3,
    };
    const intersection = intersectPointGeoJSONS(
      simplePointGeoJSON,
      firShape,
      customStyling,
    );

    expect(intersection).toBeTruthy();

    const featureResult = intersection
      .features[0] as GeoJSON.Feature<GeoJSON.Point>;

    expect(featureResult.properties!.stroke).toEqual(customStyling.stroke);
    expect(featureResult.properties!['stroke-width']).toEqual(
      customStyling['stroke-width'],
    );
    expect(featureResult.properties!['stroke-opacity']).toEqual(
      customStyling['stroke-opacity'],
    );
    expect(featureResult.properties!.fill).toEqual(customStyling.fill);
    expect(featureResult.properties!['fill-opacity']).toEqual(
      customStyling['fill-opacity'],
    );
    expect(featureResult.geometry.coordinates).toEqual(
      simplePointGeoJSON.features[0].geometry.coordinates,
    );
  });
});

describe('getGeoJson', () => {
  const firShape: GeoJSON.FeatureCollection = {
    ...emptyGeoJSON,
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [5.0, 55.0],
              [4.331914, 55.332644],
              [3.368817, 55.764314],
              [2.761908, 54.379261],
              [3.15576, 52.913554],
              [2.000002, 51.500002],
              [3.370001, 51.369722],
              [3.370527, 51.36867],
              [3.362223, 51.320002],
              [3.36389, 51.313608],
              [3.373613, 51.309999],
              [3.952501, 51.214441],
              [4.397501, 51.452776],
              [5.078611, 51.391665],
              [5.848333, 51.139444],
              [5.651667, 50.824717],
              [6.011797, 50.757273],
              [5.934168, 51.036386],
              [6.222223, 51.361666],
              [5.94639, 51.811663],
              [6.405001, 51.830828],
              [7.053095, 52.237764],
              [7.031389, 52.268885],
              [7.063612, 52.346109],
              [7.065557, 52.385828],
              [7.133055, 52.888887],
              [7.14218, 52.898244],
              [7.191667, 53.3],
              [6.5, 53.666667],
              [6.500002, 55.000002],
              [5.0, 55.0],
            ],
          ],
        },
        properties: {
          selectionType: 'fir',
        },
      },
    ],
  };

  const firShape2: GeoJSON.FeatureCollection = {
    ...firShape,
    features: [
      {
        type: 'Feature',
        properties: {
          stroke: '#8F8',
          'stroke-width': 4,
          'stroke-opacity': 1,
          fill: '#33ccFF',
          'fill-opacity': 0.5,
          _type: 'box',
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [5.046978029159577, 53.447840571533234],
              [5.046978029159577, 54.502005543908375],
              [5.8768363199158165, 54.502005543908375],
              [5.8768363199158165, 53.447840571533234],
              [5.046978029159577, 53.447840571533234],
            ],
            [
              [2.557403156890863, 52.21457337098836],
              [2.557403156890863, 53.47748376414495],
              [4.167328240957963, 53.47748376414495],
              [4.167328240957963, 52.21457337098836],
              [2.557403156890863, 52.21457337098836],
            ],
          ],
        },
      },
    ],
  };
  it('should return correct geoJSON for multiple shapes', () => {
    expect(getGeoJson(firShape, true)).toEqual(firShape);
    expect(getGeoJson(firShape2, true)).toEqual(firShape2);
  });

  it('should return correct geoJSON for single shapes', () => {
    expect(getGeoJson(firShape, false)).toEqual(firShape);
    expect(getGeoJson(firShape2, false)).toEqual({
      ...firShape2,
      features: [
        {
          ...firShape2.features[0],
          geometry: {
            ...firShape2.features[0].geometry,
            coordinates: [
              (firShape2.features[0] as GeoJSON.Feature<GeoJSON.Polygon>)
                .geometry.coordinates[1],
            ],
          },
        },
      ],
    });
  });

  it('should return correct geoJSON for geometry type Point and LineString', () => {
    const pointShape: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Point',
            coordinates: [5.030380863344452, 52.346576119466526],
          },
        },
      ],
    };
    expect(getGeoJson(pointShape, false)).toEqual(pointShape);
    expect(getGeoJson(pointShape, true)).toEqual(pointShape);

    const lineStringShape: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'LineString',
            coordinates: [
              [4.136829504703722, 50.944730810381465],
              [4.450134704406403, 53.81224530783831],
              [2.5703035061903217, 53.78911570560625],
              [5.252979278644519, 56.722490281934554],
              [8.758081200318252, 53.85846624679025],
              [6.290802752659646, 53.84691579421783],
              [6.623689527343743, 51.0556380592049],
              [4.136829504703722, 50.95706693142843],
            ],
          },
          properties: {},
        },
      ],
    };

    expect(getGeoJson(lineStringShape, false)).toEqual(lineStringShape);
    expect(getGeoJson(lineStringShape, true)).toEqual(lineStringShape);
  });
});

describe('getFeatureCollection', () => {
  const feature: GeoJSON.Feature = {
    type: 'Feature',
    properties: {},
    geometry: {
      type: 'Polygon',
      coordinates: [
        [
          [4.0345509144369665, 53.22982420540454],
          [4.831214873562956, 53.22982420540454],
          [4.831214873562956, 53.99777389652837],
          [4.831214873562956, 53.22982420540454],
          [3.6030246032437225, 53.22982420540454],
        ],
      ],
    },
  };
  const feature2: GeoJSON.Feature = {
    type: 'Feature',
    properties: {},
    geometry: {
      type: 'Polygon',
      coordinates: [
        [
          [4.0345509144369665, 53.22982420540454],
          [3.6030246032437225, 53.22982420540454],
          [4.831214873562956, 53.22982420540454],
          [4.831214873562956, 53.99777389652837],
          [4.831214873562956, 53.22982420540454],
        ],
      ],
    },
  };
  it('should return feature as feature collection for a new feature collection', () => {
    expect(getFeatureCollection(feature, false)).toEqual({
      ...emptyGeoJSON,
      features: [feature],
    });
    expect(getFeatureCollection(feature, true)).toEqual({
      ...emptyGeoJSON,
      features: [feature],
    });
  });

  it('should return feature as feature collection for an existing feature collection', () => {
    const featureCollection: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [feature, feature2],
    };

    expect(getFeatureCollection(feature, false, featureCollection)).toEqual({
      ...emptyGeoJSON,
      features: [feature],
    });
    expect(getFeatureCollection(feature, true, featureCollection)).toEqual({
      ...emptyGeoJSON,
      features: [...featureCollection.features, feature],
    });
  });
});

describe('isGeoJSONFeatureCreatedByTool', () => {
  it('should return true when selectionType is found in a geojson feature or feature collection', () => {
    const currentGeoJSON: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'test-selection-type',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.0345509144369665, 53.22982420540454],
                [4.831214873562956, 53.22982420540454],
                [4.831214873562956, 53.99777389652837],
                [4.831214873562956, 53.22982420540454],
                [3.6030246032437225, 53.22982420540454],
              ],
            ],
          },
        },
      ],
    };

    expect(
      isGeoJSONFeatureCreatedByTool(currentGeoJSON, 'test-selection-type'),
    ).toBeTruthy();

    expect(
      isGeoJSONFeatureCreatedByTool(currentGeoJSON, 'test-selection-type'),
    ).toBeTruthy();
  });

  it('should return false when selectionType is not found or different in a geojson feature or feature collection', () => {
    const currentGeoJSON: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'test-selection-type',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.0345509144369665, 53.22982420540454],
                [4.831214873562956, 53.22982420540454],
                [4.831214873562956, 53.99777389652837],
                [4.831214873562956, 53.22982420540454],
                [3.6030246032437225, 53.22982420540454],
              ],
            ],
          },
        },
      ],
    };

    expect(isGeoJSONFeatureCreatedByTool(currentGeoJSON, 'poly')).toBeFalsy();

    expect(isGeoJSONFeatureCreatedByTool(currentGeoJSON, 'poly')).toBeFalsy();

    expect(isGeoJSONFeatureCreatedByTool(currentGeoJSON, '')).toBeFalsy();
  });

  it('should return false when no features in existing geojson', () => {
    const currentGeoJSON: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [],
    };

    expect(
      isGeoJSONFeatureCreatedByTool(currentGeoJSON, 'test-selection-type'),
    ).toBeFalsy();
  });
});

describe('rewindGeometry', () => {
  const clockwisePoly = JSON.parse(
    '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"stroke":"#f24a00","stroke-width":1.5,"stroke-opacity":1,"fill":"#f24a00","fill-opacity":0.25,"selectionType":"poly"},"geometry":{"type":"Polygon","coordinates":[[[3.9727570104176655,52.798401500118025],[3.345035685215513,53.65515457833232],[3.9727570104176655,54.741996695611284],[5.906138692040295,54.33411924173588],[5.680159014967519,53.13111016712073],[3.9727570104176655,52.798401500118025]]]}}]}',
  );
  const counterClockwisePoly = JSON.parse(
    '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"stroke":"#f24a00","stroke-width":1.5,"stroke-opacity":1,"fill":"#f24a00","fill-opacity":0.25,"selectionType":"poly"},"geometry":{"type":"Polygon","coordinates":[[[3.9727570104176655,52.798401500118025],[5.680159014967519,53.13111016712073],[5.906138692040295,54.33411924173588],[3.9727570104176655,54.741996695611284],[3.345035685215513,53.65515457833232],[3.9727570104176655,52.798401500118025]]]}}]}',
  );
  const singlePoint = JSON.parse(
    '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"stroke":"#f24a00","stroke-width":1.5,"stroke-opacity":1,"fill":"#f24a00","fill-opacity":0.25,"selectionType":"point"},"geometry":{"type":"Point","coordinates":[4.262040673801408,54.168394541491786]}}]}',
  );
  const emptyGeoJSON = JSON.parse('{"type":"FeatureCollection","features":[]}');

  it('should rewind a clockwise Polygon', () => {
    expect(rewindGeometry(clockwisePoly)).toEqual(counterClockwisePoly);
  });
  it('should not rewind counterclockwise Polygon', () => {
    expect(rewindGeometry(counterClockwisePoly)).toEqual(counterClockwisePoly);
  });
  it('should not affect point', () => {
    expect(rewindGeometry(singlePoint)).toEqual(singlePoint);
  });

  it('should not affect empty geojson', () => {
    expect(rewindGeometry(emptyGeoJSON)).toEqual(emptyGeoJSON);
  });
});

describe('addGeoJSONProperties', () => {
  it('should add new properties to a single GeoJSON feature', () => {
    const geoJSONFeature: GeoJSON.Feature = {
      type: 'Feature',
      properties: {
        name: 'Feature 1',
      },
      geometry: {
        type: 'Point',
        coordinates: [0, 0],
      },
    };

    const newProperties = {
      population: 1000,
      area: 50,
    };

    const result = addGeoJSONProperties(
      geoJSONFeature,
      newProperties,
    ) as GeoJSON.Feature;

    expect(result.properties).toEqual({
      name: 'Feature 1',
      population: 1000,
      area: 50,
    });
  });

  it('should add new properties to a GeoJSON feature collection', () => {
    const geoJSONFeatureCollection: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            name: 'Feature 1',
          },
          geometry: {
            type: 'Point',
            coordinates: [0, 0],
          },
        },
        {
          type: 'Feature',
          properties: {
            name: 'Feature 2',
          },
          geometry: {
            type: 'Point',
            coordinates: [1, 1],
          },
        },
      ],
    };

    const newProperties = {
      population: 1000,
      area: 50,
    };

    const result = addGeoJSONProperties(
      geoJSONFeatureCollection,
      newProperties,
    ) as GeoJSON.FeatureCollection;

    result.features.forEach((feature) => {
      expect(feature.properties).toEqual(
        expect.objectContaining(newProperties),
      );
    });
  });
});

describe('addSelectionTypeToGeoJSON', () => {
  it('should add selection type to a single GeoJSON feature', () => {
    const geoJSONFeature: GeoJSON.Feature = {
      type: 'Feature',
      properties: {
        name: 'Feature 1',
      },
      geometry: {
        type: 'Point',
        coordinates: [0, 0],
      },
    };

    const selectionType = 'selected';

    const result = addSelectionTypeToGeoJSON(
      geoJSONFeature,
      selectionType,
    ) as GeoJSON.Feature;

    expect(result.properties).toEqual({
      name: 'Feature 1',
      selectionType: 'selected',
    });
  });

  it('should add selection type to a GeoJSON feature collection', () => {
    const geoJSONFeatureCollection: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            name: 'Feature 1',
          },
          geometry: {
            type: 'Point',
            coordinates: [0, 0],
          },
        },
        {
          type: 'Feature',
          properties: {
            name: 'Feature 2',
          },
          geometry: {
            type: 'Point',
            coordinates: [1, 1],
          },
        },
      ],
    };

    const selectionType = 'selected';

    const result = addSelectionTypeToGeoJSON(
      geoJSONFeatureCollection,
      selectionType,
    ) as GeoJSON.FeatureCollection;

    result.features.forEach((feature) => {
      expect(feature.properties).toEqual(
        expect.objectContaining({ selectionType: 'selected' }),
      );
    });
  });

  describe('getFeatureExtent', () => {
    const simplePolygonGeoJSONHalfOfNL: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.921875, 51.767839887322154],
                [4.7900390625, 49.97242235423708],
                [7.679443359375, 49.79544988802771],
                [7.888183593749999, 51.74743863117572],
                [5.86669921875, 51.984880139916626],
                [4.921875, 51.767839887322154],
              ],
            ],
          },
        },
      ],
    };
    expect(getFeatureExtent(simplePolygonGeoJSONHalfOfNL)).toEqual({
      bottom: 49.79544988802771,
      left: 4.7900390625,
      right: 7.888183593749999,
      top: 51.984880139916626,
    });
  });
});

describe('isGeoJSONGeometryEmpty', () => {
  it('should detect GeoJSON spec empty geometry', () => {
    const geojson: GeoJSON.Feature = {
      type: 'Feature',
      geometry: {
        type: 'Polygon',
        coordinates: [[]],
      },
      properties: {},
    };

    expect(isGeoJSONGeometryEmpty(geojson)).toBe(true);
  });

  it('should not detect a polygon with coordinates as empty', () => {
    const geojson: GeoJSON.Feature = {
      type: 'Feature',
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [1, 1],
            [2, 2],
            [3, 3],
            [1, 1],
          ],
        ],
      },
      properties: {},
    };

    expect(isGeoJSONGeometryEmpty(geojson)).toBe(false);
  });

  it('should detect linestring with zero coordinates as empty', () => {
    const geojson: GeoJSON.Feature = {
      type: 'Feature',
      geometry: {
        type: 'LineString',
        coordinates: [],
      },
      properties: {},
    };

    expect(isGeoJSONGeometryEmpty(geojson)).toBe(true);
  });
  it('should not detect a linestring with coordinates as empty', () => {
    const geojson: GeoJSON.Feature = {
      type: 'Feature',
      geometry: {
        type: 'LineString',
        coordinates: [
          [1, 1],
          [2, 2],
          [3, 3],
        ],
      },
      properties: {},
    };

    expect(isGeoJSONGeometryEmpty(geojson)).toBe(false);
  });

  it('should detect point with zero coordinates as empty', () => {
    const geojson: GeoJSON.Feature = {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [],
      },
      properties: {},
    };

    expect(isGeoJSONGeometryEmpty(geojson)).toBe(true);
  });
  it('should not detect a point with coordinates as empty', () => {
    const geojson: GeoJSON.Feature = {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [1, 1],
      },
      properties: {},
    };

    expect(isGeoJSONGeometryEmpty(geojson)).toBe(false);
  });
});
