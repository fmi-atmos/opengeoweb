/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { Grid2 as Grid, Divider } from '@mui/material';

import type { Meta, StoryObj } from '@storybook/react';
import { DrawModeExitCallback } from '../MapDraw/MapDraw';
import { MapDrawToolOptions, useMapDrawTool } from './useMapDrawTool';
import { GeoJSONTextField } from '../MapDraw/storyComponents';
import { IntersectionSelect } from '../MapDraw/storyComponents/IntersectionSelect';
import { StoryLayout } from '../MapDraw/storyComponents/StoryLayout';
import { ToolButton } from '../MapDraw/storyComponents/ToolButton';
import { EditModeButton } from '../MapDraw/storyComponents/EditModeButton';
import {
  exampleIntersectionOptions,
  exampleIntersectionWithShapeOptions,
  exampleIntersections,
  getIntersectionToolIcon,
} from './storyExamplesMapDrawToolIntersection';
import { MapViewLayerProps } from '../MapView';

const meta: Meta<typeof useMapDrawTool> = {
  title: 'components/MapDrawTool/intersection',
  tags: ['!autodocs'],
};
export default meta;

type Story = StoryObj<typeof useMapDrawTool>;

interface MapDrawToolIntersectDemoProps {
  mapDrawOptions: MapDrawToolOptions;
}

const IntersectionDemoStory: React.FC<MapDrawToolIntersectDemoProps> = ({
  mapDrawOptions = exampleIntersectionOptions,
}): React.ReactElement => {
  const {
    geoJSON,
    geoJSONIntersection,
    setGeoJSON,
    setGeoJSONIntersectionBounds,
    drawModes,
    isInEditMode,
    setEditMode,
    activeTool,
    changeActiveTool,
    deactivateTool,
    getLayer,
  } = useMapDrawTool(mapDrawOptions);

  const layers = [
    getLayer('geoJSONIntersectionBounds', 'example-static-layer'),
    getLayer('geoJSONIntersection', 'example-intersection-layer'),
    getLayer('geoJSON', 'example-draw-layer'),
  ];

  const onExitDrawMode = (reason: DrawModeExitCallback): void => {
    if (reason === 'escaped') {
      deactivateTool();
    }
  };

  const onChangeIntersection = (
    newGeoJSON: GeoJSON.FeatureCollection,
  ): void => {
    deactivateTool();
    setGeoJSONIntersectionBounds(newGeoJSON);
  };

  return (
    <StoryLayout
      layers={layers as MapViewLayerProps[]}
      onExitDrawMode={onExitDrawMode}
    >
      <>
        <Grid size={{ xs: 12 }}>
          {drawModes.map((mode) => (
            <ToolButton
              key={mode.drawModeId}
              mode={mode}
              onClick={(): void => {
                changeActiveTool(mode);
              }}
              isSelected={activeTool === mode.drawModeId}
              icon={getIntersectionToolIcon(mode.selectionType)}
            />
          ))}

          <Divider />
        </Grid>
        <IntersectionSelect
          intersections={exampleIntersections}
          onChangeIntersection={onChangeIntersection}
          isDisabled={activeTool !== ''}
        />

        <EditModeButton
          isInEditMode={isInEditMode}
          onToggleEditMode={setEditMode}
          drawMode={activeTool}
        />

        <GeoJSONTextField
          title="drawshape geoJSON result"
          onChangeGeoJSON={setGeoJSON}
          geoJSON={geoJSON}
        />
        {geoJSONIntersection && (
          <GeoJSONTextField
            title="intersection geoJSON result"
            geoJSON={geoJSONIntersection as GeoJSON.FeatureCollection}
          />
        )}
      </>
    </StoryLayout>
  );
};

export const IntersectionDemo: Story = {
  render: () => (
    <IntersectionDemoStory mapDrawOptions={exampleIntersectionOptions} />
  ),
};

export const IntersectionWithShapeDemo: Story = {
  render: () => (
    <IntersectionDemoStory
      mapDrawOptions={exampleIntersectionWithShapeOptions}
    />
  ),
};
