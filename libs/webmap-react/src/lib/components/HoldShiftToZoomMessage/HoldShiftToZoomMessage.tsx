/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React, { useEffect, useRef, useState } from 'react';

import { t } from 'i18next';

export const HoldShiftToZoomMessage = (): React.ReactElement | null => {
  const [showMessage, setShowMessage] = useState(false);
  const timerRef = useRef<NodeJS.Timeout>();
  useEffect(() => {
    const updateScroll = (e: WheelEvent): void => {
      if (e.deltaY) {
        clearTimeout(timerRef.current);
        timerRef.current = setTimeout(() => {
          setShowMessage(false);
        }, 1000);

        !e.shiftKey && setShowMessage(true);
      }
    };

    window.addEventListener('wheel', updateScroll);
    return (): void => {
      window.removeEventListener('wheel', updateScroll);
    };
  }, [showMessage, setShowMessage, timerRef]);

  return (
    <div
      style={{
        position: 'absolute',
        float: 'left',
        top: 0,
        left: 0,
        zIndex: 1000,
        width: '100%',
        padding: '0 40%',
        color: 'white',
      }}
    >
      {showMessage && (
        <div
          style={{
            padding: '5px 20px',
            display: 'block',
            background: 'gray',
            textAlign: 'center',
            width: '200px',
          }}
        >
          {t('hold-shift-to-zoom')}
        </div>
      )}
    </div>
  );
};
