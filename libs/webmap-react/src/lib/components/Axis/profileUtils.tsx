/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  WMLayer,
  getWMJSMapById,
  getWMLayerById,
  IWMJSMap,
  WMJSMapMouseClickEvent,
  getWMSGetFeatureInfoRequestURL,
} from '@opengeoweb/webmap';
import axios from 'axios';

export type GFIResult = [{ data: { '0': number } }];

export const CEILONETSTATIONLAYERNAME = 'hasceilometerWMONAME';
export const CEILONETPROFILELAYERPREFIX = 'ceilonet_';

/**
 * Returns the layer with overview of all ceilonet stations.
 * @param ceilonetMap
 * @returns
 */
const getCeilonetLocationLayer = (
  ceilonetMap: IWMJSMap,
): WMLayer | undefined => {
  return (
    ceilonetMap &&
    ceilonetMap.getLayers().find((layer) => {
      return layer.name === CEILONETSTATIONLAYERNAME;
    })
  );
};

/**
 * Returns the timeheight profile layer name  based on the result from getfeature info
 * @param gfiResult
 * @returns
 */
const getProfileLayerNameFromGFIResult = (
  gfiResult: GFIResult | null,
): string | undefined => {
  if (
    gfiResult &&
    gfiResult.length > 0 &&
    gfiResult[0] &&
    gfiResult[0].data &&
    gfiResult[0].data['0']
  ) {
    const stationNumberAsFloat = gfiResult[0].data['0'];
    if (stationNumberAsFloat > 0 && stationNumberAsFloat < 200000) {
      const stationNumberAsString = parseInt(
        stationNumberAsFloat.toString(),
        10,
      )
        .toString()
        .padStart(5, '0');
      const profileLayerName = `${CEILONETPROFILELAYERPREFIX}${stationNumberAsString}`;
      return profileLayerName;
    }
  }

  console.warn('GFI result from server not recognized');
  return undefined;
};

/**
 * Sets the layername and draws the map
 * @param gfiResult
 */
const setProfileLayerBasedOnGfiData = (
  layerIdToChange: string,
  profileLayerName: string,
): void => {
  const profileLayer = getWMLayerById(layerIdToChange);
  if (profileLayer !== undefined && profileLayerName !== undefined) {
    profileLayer.name = profileLayerName;
    profileLayer.parentMap.draw();
  }
};

const makeGFIRequest = async (featureInfoUrl: string): Promise<GFIResult> => {
  return (await axios.get<GFIResult>(featureInfoUrl)).data;
};

const handleMapMouseClick = async (
  clickedMapLocation: WMJSMapMouseClickEvent,
  profileDataLayerId: string,
): Promise<void> => {
  const { map: ceilonetStationMap, x, y } = clickedMapLocation;
  ceilonetStationMap.mapPin.showMapPin();

  const ceilonetLayer = getCeilonetLocationLayer(ceilonetStationMap);
  if (!ceilonetLayer) {
    console.warn(
      `Ceilonet station layer with id ${CEILONETSTATIONLAYERNAME} not found in map`,
    );
    return;
  }
  const featureInfoUrl = getWMSGetFeatureInfoRequestURL(
    ceilonetLayer,
    x,
    y,
    'application/json',
  );
  const gfiResult = await makeGFIRequest(featureInfoUrl).catch((e) => {
    console.warn(e);
    return null;
  });

  const profileLayerName = getProfileLayerNameFromGFIResult(gfiResult);
  if (profileLayerName) {
    setProfileLayerBasedOnGfiData(profileDataLayerId, profileLayerName!);
  }
};

/**
 * Connects a Map with a WMS layer with ceilometer stations to a Map withj profile layers.
 * Clicking on the station map causes the layer in the profile map to be changed accordingly.
 * The layername comes from a GetFeatureInfo request on the stations layer.
 *
 * @param mapIdWithProfileViewer
 * @param mapIdWithCeilometerStations
 * @returns
 */
export const connectProfileMapViewToProfileStationsMapById = (
  mapIdWithProfileViewer: string,
  mapIdWithCeilometerStations: string,
): void => {
  const stationsMap = getWMJSMapById(mapIdWithCeilometerStations);
  const profileMap = getWMJSMapById(mapIdWithProfileViewer);
  if (!stationsMap) {
    console.warn(
      `Ceilonet stations map with id ${mapIdWithCeilometerStations} not found`,
    );
    return;
  }

  if (!profileMap) {
    console.warn(
      `Ceilonet profile map with id ${mapIdWithProfileViewer} not found`,
    );
    return;
  }

  const profileDataLayer =
    profileMap &&
    profileMap.getLayers().find((l) => {
      return l.name?.startsWith(CEILONETPROFILELAYERPREFIX);
    });

  const mouseClicked = (mouseEvent: WMJSMapMouseClickEvent): void => {
    if (!profileDataLayer) {
      console.warn(
        `Ceilonet profile layer not found in map with id ${mapIdWithProfileViewer}`,
      );
      return;
    }

    const profileLayerId = profileDataLayer.id;
    void handleMapMouseClick(mouseEvent, profileLayerId!);
  };

  stationsMap
    .getListener()
    .removeEventListener(
      'mouseClicked',
      mouseClicked as (param: unknown) => unknown,
    );
  stationsMap.addListener('mouseclicked', mouseClicked, true);
};
