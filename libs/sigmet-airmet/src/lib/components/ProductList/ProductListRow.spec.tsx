/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';

import { dateUtils } from '@opengeoweb/shared';
import {
  fakeShortTestSigmet,
  fakeShortVaTestSigmet,
  fakeSigmetList,
  fakeTestSigmetWithThunderStorm,
  fakeTestSigmetWithVolcanicAshCloud,
} from '../../utils/mockdata/fakeSigmetList';
import ProductListRow, { getActiveStatus } from './ProductListRow';
import { TestWrapper } from '../../utils/testUtils';
import { fakeAirmetTAC, fakeSigmetTAC } from '../../utils/fakeApi';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';
import { Airmet } from '../../types';
import { noTAC } from '../ProductForms/ProductFormTac';
import { airmetConfig, sigmetConfig } from '../../utils/config';

describe('components/ProductList/ProductListRow', () => {
  it('should render sigmet row successfully', async () => {
    render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[0].sigmet}
          productType="sigmet"
          productConfig={sigmetConfig}
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(screen.getByTestId('issueTime'));
    await screen.findByText(fakeSigmetTAC);
  });

  it('should render airmet row successfully', async () => {
    render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[0].airmet}
          productType="airmet"
          productConfig={airmetConfig}
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(screen.getByTestId('issueTime'));
    await screen.findByText(fakeAirmetTAC);
  });

  it('should show the check icon when validity start is in the past and sigmet is not expired', async () => {
    render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[2].sigmet}
          productType="sigmet"
          productConfig={sigmetConfig}
        />
      </TestWrapper>,
    );

    expect(screen.getByTestId('status-active')).toBeTruthy();
  });

  it('should show the check icon when validity start is equal to current time and airmet is not expired', async () => {
    render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[2].airmet}
          productType="airmet"
          productConfig={airmetConfig}
        />
      </TestWrapper>,
    );

    expect(screen.getByTestId('status-active')).toBeTruthy();
  });

  it('should show the clock icon when validity start and end are in the future and sigmet status is published', async () => {
    render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[1].sigmet}
          productType="sigmet"
          productConfig={sigmetConfig}
        />
      </TestWrapper>,
    );

    expect(screen.getByTestId('status-inactive')).toBeTruthy();
  });

  it('should show no icon when sigmet status is expired', async () => {
    const { container } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[4].sigmet}
          productType="sigmet"
          productConfig={sigmetConfig}
        />
      </TestWrapper>,
    );

    expect(container).toBeDefined();
    expect(screen.queryByTestId('status-active')).toBeFalsy();
    expect(screen.queryByTestId('status-inactive')).toBeFalsy();
  });

  it('should not show any status icon when airmet status is draft', async () => {
    const { container } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[0].airmet}
          productType="airmet"
          productConfig={airmetConfig}
        />
      </TestWrapper>,
    );

    expect(container).toBeDefined();
    expect(screen.queryByTestId('status-active')).toBeFalsy();
    expect(screen.queryByTestId('status-inactive')).toBeFalsy();
  });

  it('should not show any status icon when sigmet is expired', async () => {
    const { container } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[4].sigmet}
          productType="sigmet"
          productConfig={sigmetConfig}
        />
      </TestWrapper>,
    );

    expect(container).toBeDefined();
    expect(screen.queryByTestId('status-active')).toBeFalsy();
    expect(screen.queryByTestId('status-inactive')).toBeFalsy();
  });

  it('should show "not published" for a draft sigmet', async () => {
    render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[0].sigmet}
          productType="sigmet"
          productConfig={sigmetConfig}
        />
      </TestWrapper>,
    );

    expect(screen.getByTestId('issueTime').textContent).toEqual(
      '(Not published)',
    );
  });

  it('should show the correct airmet issue date for a published airmet', async () => {
    render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[1].airmet}
          productType="airmet"
          productConfig={airmetConfig}
        />
      </TestWrapper>,
    );

    expect(screen.getByTestId('issueTime').textContent).toEqual(
      `${dateUtils.dateToString(
        dateUtils.utc(fakeSigmetList[1].sigmet.issueDate),
        dateUtils.DATE_FORMAT_NAME_OF_MONTH,
      )} UTC`,
    );
  });

  it('should show the correct Sigmet valid date (same day for start and end)', async () => {
    const now = `${dateUtils.dateToString(
      dateUtils.utc(),
      dateUtils.DATE_FORMAT_YEAR,
    )}T14:00:00Z`;
    jest.useFakeTimers().setSystemTime(new Date(now));
    const { sigmet } = fakeSigmetList[1];
    const sigmetNewDates = {
      ...sigmet,
      validDateStart: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 1 }),
      )!,
      validDateEnd: dateUtils.dateToString(
        dateUtils.add(dateUtils.utc(), { hours: 2 }),
      )!,
    };

    render(
      <TestWrapper>
        <ProductListRow
          product={sigmetNewDates}
          productType="sigmet"
          productConfig={sigmetConfig}
        />
      </TestWrapper>,
    );

    expect(screen.getByTestId('validTime').textContent).toEqual(
      `${dateUtils.dateToString(
        dateUtils.utc(sigmetNewDates.validDateStart),
        dateUtils.DATE_FORMAT_NAME_OF_MONTH,
      )} - ${dateUtils.dateToString(
        dateUtils.utc(sigmetNewDates.validDateEnd),
        dateUtils.DATE_FORMAT_HOURS,
      )} UTC`,
    );
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should show the correct Airmet valid date (different day for start and end)', async () => {
    render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[6].airmet}
          productType="airmet"
          productConfig={airmetConfig}
        />
      </TestWrapper>,
    );

    expect(screen.getByTestId('validTime').textContent).toEqual(
      `${dateUtils.dateToString(
        dateUtils.utc(fakeSigmetList[6].sigmet.validDateStart),
        dateUtils.DATE_FORMAT_NAME_OF_MONTH,
      )} - ${dateUtils.dateToString(
        dateUtils.utc(fakeSigmetList[6].sigmet.validDateEnd),
        dateUtils.DATE_FORMAT_NAME_OF_MONTH,
      )} UTC`,
    );
  });

  it('should show - when validity start or end is empty and no TAC should be retrieved', async () => {
    const { validDateStart, validDateEnd, ...product } =
      fakeAirmetList[2].airmet;

    render(
      <TestWrapper>
        <ProductListRow
          product={product as Airmet}
          productType="airmet"
          productConfig={airmetConfig}
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(screen.getByTestId('issueTime'));

    // Should show Missing data: no TAC can be generated
    await screen.findByText(noTAC);

    const validTime = screen.getByTestId('validTime');
    expect(validTime.innerHTML).toEqual('-');
  });

  it('should show the correct cancel tag for a cancel airmet', async () => {
    render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[3].airmet}
          productType="airmet"
          productConfig={airmetConfig}
        />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.getByText('cancels 113')).toBeTruthy();
    });
  });

  it('should show the correct tag for a published sigmet', async () => {
    render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[2].sigmet}
          productType="sigmet"
          productConfig={sigmetConfig}
        />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.getByText('published')).toBeTruthy();
    });
  });

  describe('getActiveStatus', () => {
    it('should return null if status is expired', async () => {
      expect(getActiveStatus(fakeSigmetList[4].sigmet)).toBe(null);
    });
  });

  describe('checkIfShortTestOrShortVAtest ', () => {
    it('Should show TEST in sigmet list instead of phenomena if sigmet type is "SHORT_TEST', () => {
      render(
        <TestWrapper>
          <ProductListRow
            product={fakeShortTestSigmet.sigmet}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </TestWrapper>,
      );

      expect(screen.getByText('SIGMET')).toBeTruthy();
      expect(screen.getByText('TEST')).toBeTruthy();
    });

    it('Should show TEST in sigmet list instead of phenomena if sigmet type is "TEST', () => {
      render(
        <TestWrapper>
          <ProductListRow
            product={fakeTestSigmetWithThunderStorm.sigmet}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </TestWrapper>,
      );

      expect(screen.getByText('SIGMET')).toBeTruthy();
      expect(screen.getByText('TEST')).toBeTruthy();
    });

    it('Should show VA TEST in sigmet list instead of phenomena if sigmet type is "SHORT_VA_TEST', () => {
      render(
        <TestWrapper>
          <ProductListRow
            product={fakeShortVaTestSigmet.sigmet}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </TestWrapper>,
      );

      expect(screen.getByText('SIGMET')).toBeTruthy();
      expect(screen.getByText('VA TEST')).toBeTruthy();
    });

    it('Should show VA TEST in sigmet list instead of phenomena if sigmet type is "TEST but sigmet has a volcanic ash cloud', () => {
      render(
        <TestWrapper>
          <ProductListRow
            product={fakeTestSigmetWithVolcanicAshCloud.sigmet}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </TestWrapper>,
      );

      expect(screen.getByText('SIGMET')).toBeTruthy();
      expect(screen.getByText('VA TEST')).toBeTruthy();
    });
  });
});
