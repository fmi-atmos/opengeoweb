/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { act, renderHook, waitFor } from '@testing-library/react';
import { Airmet, Sigmet } from '../../types';
import { airmetConfig, sigmetConfig } from '../../utils/config';
import { TestWrapper, TestWrapperProps } from '../../utils/testUtils';
import { renewAirmet, renewSigmet, useProductApi } from './utils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { SigmetAirmetApi } from '../../utils/api';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';
import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';

jest.mock(
  'libs/authentication/src/lib/components/AuthenticationContext',
  () => {
    return {
      // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
      useAuthenticationContext: () => ({ isLoggedIn: true }),
    };
  },
);

describe('components/MetInfoWrapper/utils', () => {
  describe('useProductApi', () => {
    it('fetch sigmet list if valid config', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getSigmetList: jest.fn(() => ({ data: sigmetConfig })),
      } as unknown as SigmetAirmetApi;

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;
      const Wrapper = ({ children }: TestWrapperProps): React.ReactElement => (
        <TestWrapper createApi={createTestApi}>{children}</TestWrapper>
      );

      const { result } = renderHook(() => useProductApi('sigmet'), {
        wrapper: Wrapper,
      });

      await waitFor(() => {
        expect(result.current.isLoading).toBeFalsy();
      });

      await waitFor(() => {
        expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(1);
      });
      expect(result.current.result).toEqual(sigmetConfig);
      expect(result.current.error).toBeUndefined();
      expect(result.current.isLoading).toBeFalsy();

      act(() => {
        void result.current.fetchNewData();
      });
      expect(result.current.isLoading).toBeTruthy();
      expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(2);

      await waitFor(() => expect(result.current.isLoading).toBeFalsy());
    });

    it('should return error on sigmet if fetching fails', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getSigmetList: jest.fn(() =>
          Promise.reject(new Error('error fetching')),
        ),
      };

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;
      const Wrapper = ({ children }: TestWrapperProps): React.ReactElement => (
        <TestWrapper createApi={createTestApi}>{children}</TestWrapper>
      );

      const { result } = renderHook(() => useProductApi('sigmet'), {
        wrapper: Wrapper,
      });
      expect(result.current.isLoading).toBeTruthy();

      await waitFor(() =>
        expect(result.current.error).toEqual({
          name: translateKeyOutsideComponents('error-backend'),
        }),
      );
      expect(result.current.isLoading).toBeFalsy();

      // refetch new data
      act(() => {
        void result.current.fetchNewData();
      });
      expect(result.current.isLoading).toBeTruthy();
      expect(fakeApi.getSigmetList).toHaveBeenCalledTimes(2);

      await waitFor(() => expect(result.current.isLoading).toBeFalsy());
    });

    it('should fetch airmet list if valid config', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getAirmetList: jest.fn(() => ({ data: airmetConfig })),
      } as unknown as SigmetAirmetApi;

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;
      const Wrapper = ({ children }: TestWrapperProps): React.ReactElement => (
        <TestWrapper createApi={createTestApi}>{children}</TestWrapper>
      );

      const { result } = renderHook(() => useProductApi('airmet'), {
        wrapper: Wrapper,
      });

      expect(result.current.isLoading).toBeTruthy();
      await waitFor(() => {
        expect(fakeApi.getAirmetList).toHaveBeenCalledTimes(1);
      });
      expect(result.current.result).toEqual(airmetConfig);
      expect(result.current.error).toBeUndefined();
      expect(result.current.isLoading).toBeFalsy();

      // refetch new data
      act(() => {
        void result.current.fetchNewData();
      });
      expect(result.current.isLoading).toBeTruthy();
      expect(fakeApi.getAirmetList).toHaveBeenCalledTimes(2);

      await waitFor(() => expect(result.current.isLoading).toBeFalsy());
    });

    it('should return error on airmet if fetching fails', async () => {
      const fakeApi: SigmetAirmetApi = {
        ...createFakeApi(),
        getAirmetList: jest.fn(() =>
          Promise.reject(new Error('error fetching')),
        ),
      };

      const createTestApi: () => SigmetAirmetApi = () => fakeApi;
      const Wrapper = ({ children }: TestWrapperProps): React.ReactElement => (
        <TestWrapper createApi={createTestApi}>{children}</TestWrapper>
      );

      const { result } = renderHook(() => useProductApi('airmet'), {
        wrapper: Wrapper,
      });
      expect(result.current.isLoading).toBeTruthy();

      await waitFor(() =>
        expect(result.current.error).toEqual({
          name: translateKeyOutsideComponents('error-backend'),
        }),
      );
      expect(result.current.isLoading).toBeFalsy();

      // refetch new data
      act(() => {
        void result.current.fetchNewData();
      });
      expect(result.current.isLoading).toBeTruthy();
      expect(fakeApi.getAirmetList).toHaveBeenCalledTimes(2);

      await waitFor(() => expect(result.current.isLoading).toBeFalsy());
    });
  });

  describe('renewed Product', () => {
    it('should renew airmet', () => {
      const fakeAirmet = fakeAirmetList[0].airmet as Airmet;
      const renewedAirmet = renewAirmet(fakeAirmetList[0], airmetConfig);
      expect(renewedAirmet.uuid).toEqual(undefined);
      expect(renewedAirmet.canbe).toEqual([
        'DRAFTED',
        'DISCARDED',
        'PUBLISHED',
      ]);
      expect(renewedAirmet.airmet.validDateStart).toEqual(
        fakeAirmet.validDateEnd,
      );
      expect(renewedAirmet.airmet.status).toEqual('DRAFT');
      expect(renewedAirmet.airmet.sequence).toEqual('-1');
    });
    it('should renew sigmet', () => {
      const fakeSigmet = fakeSigmetList[0].sigmet as Sigmet;
      const renewedSigmet = renewSigmet(fakeSigmetList[0], sigmetConfig);
      expect(renewedSigmet.uuid).toEqual(undefined);
      expect(renewedSigmet.canbe).toEqual([
        'DRAFTED',
        'DISCARDED',
        'PUBLISHED',
      ]);
      expect(renewedSigmet.sigmet.validDateStart).toEqual(
        fakeSigmet.validDateEnd,
      );
      expect(renewedSigmet.sigmet.status).toEqual('DRAFT');
      expect(renewedSigmet.sigmet.sequence).toEqual('-1');
    });
  });
});
