/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import {
  act,
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import MetInfoWrapper from './MetInfoWrapper';
import { TestWrapper } from '../../utils/testUtils';
import { fakeAirmetTAC, fakeSigmetTAC } from '../../utils/fakeApi';
import { airmetConfig, sigmetConfig } from '../../utils/config';

jest.mock(
  'libs/authentication/src/lib/components/AuthenticationContext',
  () => {
    return {
      // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
      useAuthenticationContext: () => ({ isLoggedIn: true }),
    };
  },
);

describe('components/MetInfoWrapper/MetInfoWrapper TAC calls', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not send unrendered inputs to the get tac call when SIGMET phenomenon or progress changed', async () => {
    const mockGetSigmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: [] });
      });
    });
    const mockGetSigmetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeSigmetTAC,
        });
      });
    });
    const mockPostSigmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 3000);
      });
    });

    render(
      <TestWrapper
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            getSigmetList: mockGetSigmetList,
            postSigmet: mockPostSigmet,
            getSigmetTAC: mockGetSigmetTac,
          };
        }}
      >
        <MetInfoWrapper productType="sigmet" productConfig={sigmetConfig} />
      </TestWrapper>,
    );

    // Wait until list has loaded
    await waitFor(() => {
      expect(screen.getByTestId('productListCreateButton')).toBeTruthy();
    });
    expect(mockGetSigmetList).toHaveBeenCalledTimes(1);
    expect(mockGetSigmetTac).toHaveBeenCalledTimes(0);

    expect(screen.queryByTestId('productform-dialog')).toBeFalsy();
    fireEvent.click(screen.getByTestId('productListCreateButton'));
    await waitFor(() => {
      expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    });
    expect(screen.getByTestId('dialogTitle').textContent).toEqual('New SIGMET');
    expect(
      within(screen.getByTestId('phenomenon')).getByRole('combobox')
        .textContent,
    ).toEqual('​');

    // choose phenomenon VA_CLD
    fireEvent.mouseDown(
      within(screen.getByTestId('phenomenon')).getByRole('combobox'),
    );
    const menuItem = await screen.findByText('Volcanic ash cloud');
    fireEvent.click(menuItem);
    await waitFor(() =>
      expect(
        within(screen.getByTestId('phenomenon')).getByRole('combobox')
          .textContent,
      ).toEqual('Volcanic ash cloud'),
    );

    await waitFor(() => {
      expect(screen.getByText('Volcano name')).toBeTruthy();
    });
    // fill in VA_CLD fields
    const volcanoName = screen.getByRole('textbox', {
      name: 'Volcano name',
    });
    const latitude = screen.getByRole('textbox', {
      name: 'Latitude',
    });
    const longitude = screen.getByRole('textbox', {
      name: 'Longitude',
    });
    fireEvent.change(volcanoName!, { target: { value: 'Test with Etna' } });
    fireEvent.change(latitude!, { target: { value: '5' } });
    fireEvent.change(longitude!, { target: { value: '-5' } });

    // fill in all other fields required to trigger a tac call
    const obsField = screen.getByTestId('isObservationOrForecast-OBS');
    fireEvent.click(obsField);

    const firButton = within(screen.getByTestId('drawtools-fir')).getByRole(
      'radio',
    );
    fireEvent.click(firButton);

    const levelsAt = screen.getByTestId('levels-AT');
    fireEvent.click(levelsAt);

    const levelInput = screen.getByTestId('level.value');
    fireEvent.change(within(levelInput).getByRole('textbox'), {
      target: { value: 100 },
    });

    const noVaExpected = screen.getByTestId('movementType-NO_VA_EXP');
    fireEvent.click(noVaExpected);

    const noChange = screen.getByTestId('change-NC');
    fireEvent.click(noChange);

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetSigmetTac).toHaveBeenCalledTimes(1));
    await waitFor(() =>
      expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
        expect.objectContaining({
          vaSigmetVolcanoCoordinates: { latitude: 5, longitude: -5 },
          vaSigmetVolcanoName: 'TEST WITH ETNA',
          movementType: 'NO_VA_EXP',
        }),
      ),
    );

    // change phenomenon
    fireEvent.mouseDown(
      within(screen.getByTestId('phenomenon')).getByRole('combobox'),
    );
    const menuItem2 = await screen.findByText('Heavy duststorm');
    fireEvent.click(menuItem2);
    await waitFor(() =>
      expect(screen.getByTestId('phenomenon').textContent).toEqual(
        'Heavy duststorm',
      ),
    );
    expect(screen.getByTestId('phenomenon').textContent).toEqual(
      'Heavy duststorm',
    );
    // choose new Progress value FORECAST_POSITION
    const endPosition = screen.getByTestId('movementType-FORECAST_POSITION');
    fireEvent.click(endPosition);

    const firButton2 = within(
      screen.getAllByTestId('drawtools-fir')[1],
    ).getByRole('radio');
    fireEvent.click(firButton2!);

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetSigmetTac).toHaveBeenCalledTimes(2));
    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        vaSigmetVolcanoCoordinates: { latitude: 5, longitude: -5 },
        vaSigmetVolcanoName: 'TEST WITH ETNA',
        movementType: 'NO_VA_EXP',
      }),
    );
    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        movementType: 'FORECAST_POSITION',
        endGeometry: expect.any(Object),
        endGeometryIntersect: expect.any(Object),
      }),
    );

    // switch Progress to movementType-MOVEMENT
    const movement = screen.getByTestId('movementType-MOVEMENT');
    fireEvent.click(movement);
    fireEvent.mouseDown(
      within(screen.getByTestId('movement-movementDirection')).getByRole(
        'combobox',
      ),
    );
    const menuItemS = await screen.findByText('S');
    fireEvent.click(menuItemS);

    const speedInput = screen.getByTestId('movement-movementSpeed');
    fireEvent.change(within(speedInput).getByRole('textbox'), {
      target: { value: 100 },
    });

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    await waitFor(() => expect(mockGetSigmetTac).toHaveBeenCalledTimes(3));

    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        movementType: 'FORECAST_POSITION',
        endGeometry: expect.any(Object),
        endGeometryIntersect: expect.any(Object),
      }),
    );

    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        movementType: 'MOVEMENT',
        movementSpeed: 100,
        movementDirection: 'S',
        movementUnit: 'KT',
      }),
    );

    // switch Progress to movementType-STATIONARY
    const stationary = screen.getByTestId('movementType-STATIONARY');
    fireEvent.click(stationary);

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    await waitFor(() => expect(mockGetSigmetTac).toHaveBeenCalledTimes(4));

    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        movementType: 'MOVEMENT',
        movementSpeed: 100,
        movementDirection: 'S',
        movementUnit: 'KT',
      }),
    );
    expect(mockGetSigmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        movementType: 'STATIONARY',
      }),
    );
  });

  it('should not send unrendered inputs to the get tac call when AIRMET phenomenon changed', async () => {
    const mockGetAirmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: [] });
      });
    });
    const mockGetAirmetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeAirmetTAC,
        });
      });
    });
    const mockPostAirmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 3000);
      });
    });

    render(
      <TestWrapper
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            getAirmetList: mockGetAirmetList,
            postAirmet: mockPostAirmet,
            getAirmetTAC: mockGetAirmetTac,
          };
        }}
      >
        <MetInfoWrapper productType="airmet" productConfig={airmetConfig} />
      </TestWrapper>,
    );

    // Wait until list has loaded
    await waitFor(() => {
      expect(screen.getByTestId('productListCreateButton')).toBeTruthy();
    });
    expect(mockGetAirmetList).toHaveBeenCalledTimes(1);
    expect(mockGetAirmetTac).toHaveBeenCalledTimes(0);

    expect(screen.queryByTestId('productform-dialog')).toBeFalsy();
    fireEvent.click(screen.getByTestId('productListCreateButton'));
    await waitFor(() => {
      expect(screen.getByTestId('productform-dialog')).toBeTruthy();
    });
    expect(screen.getByTestId('dialogTitle').textContent).toEqual('New AIRMET');
    expect(screen.getByTestId('phenomenon').textContent).toEqual('​');

    // choose phenomenon SFC_VIS
    fireEvent.mouseDown(
      within(screen.getByTestId('phenomenon')).getByRole('combobox'),
    );
    const menuItem = await screen.findByText('Surface visibility');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(screen.getByText('Select visibility')).toBeTruthy();
    });
    // fill in SFC_VIS fields
    const visibilityInput = screen.getByTestId(
      'surfaceVisibility-visibilityValue',
    );
    fireEvent.change(within(visibilityInput).getByRole('textbox'), {
      target: { value: 100 },
    });
    fireEvent.mouseDown(
      within(screen.getByTestId('surfaceVisibility-visibilityCause')).getByRole(
        'combobox',
      ),
    );
    const menuItemDrizzle = await screen.findByText('Drizzle');
    fireEvent.click(menuItemDrizzle);

    // fill in all other fields required to trigger a tac call
    const obsField = screen.getByTestId('isObservationOrForecast-OBS');
    fireEvent.click(obsField);

    const firButton = within(screen.getByTestId('drawtools-fir')).getByRole(
      'radio',
    );
    fireEvent.click(firButton!);

    const stationary = screen.getByTestId('movementType-STATIONARY');
    fireEvent.click(stationary);

    const noChange = screen.getByTestId('change-NC');
    fireEvent.click(noChange);

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetAirmetTac).toHaveBeenCalledTimes(1));
    await waitFor(() =>
      expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
        expect.objectContaining({
          visibilityValue: 100,
          visibilityCause: 'DZ',
        }),
      ),
    );

    // choose phenomenon SFC_WIND
    fireEvent.mouseDown(
      within(screen.getByTestId('phenomenon')).getByRole('combobox'),
    );
    const menuItem2 = await screen.findByText('Surface wind');
    fireEvent.click(menuItem2);
    await waitFor(() => {
      expect(screen.getByText('Set wind direction')).toBeTruthy();
    });

    // fill in SFC_WIND fields
    const windDirectionInput = screen.getByTestId('surfaceWind-windDirection');
    fireEvent.change(within(windDirectionInput).getByRole('textbox'), {
      target: { value: 100 },
    });
    const windSpeedInput = screen.getByTestId('surfaceWind-windSpeed');
    fireEvent.change(within(windSpeedInput).getByRole('textbox'), {
      target: { value: 100 },
    });

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetAirmetTac).toHaveBeenCalledTimes(2));
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        visibilityValue: 100,
        visibilityCause: 'DZ',
      }),
    );
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        windDirection: 100,
        windSpeed: 100,
      }),
    );

    // choose phenomenon OVC_CLD
    fireEvent.mouseDown(
      within(screen.getByTestId('phenomenon')).getByRole('combobox'),
    );
    const menuItem3 = await screen.findByText('Overcast cloud');
    fireEvent.click(menuItem3);
    await waitFor(() => {
      expect(screen.getByText('Upper level')).toBeTruthy();
    });

    // fill in Cloud levels
    const upperLevel = screen.getByTestId('cloudLevel-value');
    fireEvent.change(within(upperLevel).getByRole('textbox'), {
      target: { value: 200 },
    });
    const lowerLevel = screen.getByTestId('cloudLowerLevel-value');
    fireEvent.change(within(lowerLevel).getByRole('textbox'), {
      target: { value: 100 },
    });

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetAirmetTac).toHaveBeenCalledTimes(3));
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        windDirection: 100,
        windSpeed: 100,
      }),
    );
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        cloudLevel: { unit: 'FT', value: 200 },
        cloudLowerLevel: { unit: 'FT', value: 100 },
      }),
    );

    // choose phenomenon MOD_ICE
    fireEvent.mouseDown(
      within(screen.getByTestId('phenomenon')).getByRole('combobox'),
    );
    const menuItem4 = await screen.findByText('Moderate icing');
    fireEvent.click(menuItem4);

    // fill in Levels
    const levelsAt = screen.getByTestId('levels-AT');
    fireEvent.click(levelsAt);
    const levelInput = screen.getByTestId('level.value');
    fireEvent.change(within(levelInput).getByRole('textbox'), {
      target: { value: 100 },
    });

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetAirmetTac).toHaveBeenCalledTimes(4));
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        cloudLevel: { unit: 'FT', value: 200 },
        cloudLowerLevel: { unit: 'FT', value: 100 },
      }),
    );
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        level: { unit: 'FL', value: 100 },
      }),
    );

    // choose phenomenon BKN_CLD
    fireEvent.mouseDown(
      within(screen.getByTestId('phenomenon')).getByRole('combobox'),
    );
    const menuItem5 = await screen.findByText('Broken cloud');
    fireEvent.click(menuItem5);
    await waitFor(() => {
      expect(screen.getByText('Upper level')).toBeTruthy();
    });

    // fill in Cloud levels
    const upperLevel2 = screen.getByTestId('cloudLevel-value');
    fireEvent.change(within(upperLevel2).getByRole('textbox'), {
      target: { value: 200 },
    });
    const lowerLevel2 = screen.getByTestId('cloudLowerLevel-value');
    fireEvent.change(within(lowerLevel2).getByRole('textbox'), {
      target: { value: 100 },
    });

    // skip time to avoid waiting on the tac call debounce
    await act(async () => jest.advanceTimersByTime(1000));
    // check tac call
    await waitFor(() => expect(mockGetAirmetTac).toHaveBeenCalledTimes(5));
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.not.objectContaining({
        level: { unit: 'FL', value: 100 },
      }),
    );
    expect(mockGetAirmetTac).toHaveBeenLastCalledWith(
      expect.objectContaining({
        cloudLevel: { unit: 'FT', value: 200 },
        cloudLowerLevel: { unit: 'FT', value: 100 },
      }),
    );
  });
});
