/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Typography, Button, Box } from '@mui/material';
import ContentDialog from './ContentDialog';
import { getProductFormLifecycleButtons } from '../ProductForms/utils';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';
import { ProductCanbe, ProductType } from '../../types';

export default { title: 'components/Content Dialog', tags: ['!autodocs'] };

export const ContentDialogDemo = (): React.ReactElement => {
  const [open, setOpen] = React.useState(false);
  const handleToggleDialog = (): void => {
    setOpen(!open);
  };

  return (
    <>
      <Button variant="outlined" onClick={handleToggleDialog}>
        Open dialog
      </Button>
      <ContentDialog
        open={open}
        toggleDialogStatus={handleToggleDialog}
        title="Example"
        options={
          <Box sx={{ button: { minWidth: 96, marginLeft: 1 } }}>
            <Button variant="flat">Yes</Button>
            <Button variant="tertiary">No</Button>
            <Button variant="primary">Maybe</Button>
          </Box>
        }
      >
        <Typography variant="subtitle1">Hello World</Typography>
        <Typography variant="body1">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
          tincidunt feugiat est nec porttitor. Sed quis nulla commodo, fringilla
          mi fringilla, hendrerit justo. Curabitur et felis non dolor congue
          tincidunt. Nunc elementum scelerisque dictum. Sed blandit malesuada
          sem quis rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing
          elit. Nullam tincidunt tellus et sagittis dapibus. Cras sed vehicula
          quam, volutpat lobortis massa. Etiam quis elementum orci. Ut iaculis
          posuere bibendum. Nam eu sapien et metus sagittis sagittis id et dui.
          Integer vitae lacinia nulla, et laoreet libero. Duis sit amet quam
          luctus, dapibus odio luctus, pharetra metus. Quisque accumsan, lectus
          nec pharetra mollis, neque neque malesuada quam, dictum dapibus purus
          metus in quam. Vestibulum facilisis ornare tortor, quis accumsan nibh
          rutrum vel. Mauris rhoncus est id eros volutpat maximus. Nunc leo
          enim, semper et neque nec, pellentesque faucibus lorem. Sed pretium
          condimentum ipsum, id commodo purus viverra sit amet. Integer
          elementum velit non elit pellentesque lacinia. Nullam accumsan
          efficitur turpis, eget suscipit purus placerat sed. Ut vestibulum
          tempus vehicula. Quisque eu lectus sit amet libero feugiat
          ullamcorper. Aenean egestas, nulla vel dapibus porta, dolor purus
          pellentesque nisi, vitae iaculis dui nunc accumsan metus. Donec
          efficitur viverra nibh at porttitor. Pellentesque diam metus, mollis
          in placerat nec, auctor vitae est. Sed condimentum efficitur risus id
          vehicula. Nam luctus lacus nec urna varius dignissim.
        </Typography>
      </ContentDialog>
    </>
  );
};

interface DialogDemoProps {
  canbe: ProductCanbe[];
  title: string;
  productType: ProductType;
}

const DialogDemo: React.FC<DialogDemoProps> = ({
  canbe,
  title,
  productType,
}: DialogDemoProps) => (
  <ContentDialog
    open
    toggleDialogStatus={(): void => {
      /* Do nothing */
    }}
    title={`Example ${title}`}
    options={getProductFormLifecycleButtons({
      canBe: canbe,
      onButtonPress: () => {},
      productType,
    })}
  >
    <Box
      sx={{
        minWidth: 1024,
      }}
    >
      <Typography variant="h5">{title}</Typography>
    </Box>
  </ContentDialog>
);

const parametersLightTheme = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4934891039316017011cd/version/63453b21d90f056999965f74',
    },
  ],
};

const parametersDarkTheme = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4935476031e13fb0f3ab6/version/63453b3bb0834e6a236fa210',
    },
  ],
};

// New
export const NewDialogDemoLight = (): React.ReactElement => (
  <DialogDemo
    canbe={fakeAirmetList[0].canbe}
    title="New"
    productType="sigmet"
  />
);

NewDialogDemoLight.storyName = 'Content dialog new light theme';
NewDialogDemoLight.tags = ['snapshot'];
NewDialogDemoLight.parameters = parametersLightTheme;

export const NewDialogDemoDark = (): React.ReactElement => (
  <DialogDemo
    canbe={fakeAirmetList[0].canbe}
    title="New"
    productType="airmet"
  />
);

NewDialogDemoDark.storyName = 'Content dialog new dark theme';
NewDialogDemoDark.tags = ['snapshot', 'dark'];
NewDialogDemoDark.parameters = parametersDarkTheme;

// View
export const ViewDialogDemoLight = (): React.ReactElement => (
  <DialogDemo
    canbe={fakeAirmetList[2].canbe}
    title="View"
    productType="sigmet"
  />
);

ViewDialogDemoLight.storyName = 'Content dialog view light theme';
ViewDialogDemoLight.tags = ['snapshot'];
ViewDialogDemoLight.parameters = parametersLightTheme;

export const ViewDialogDemoDark = (): React.ReactElement => (
  <DialogDemo
    canbe={fakeAirmetList[2].canbe}
    title="View"
    productType="airmet"
  />
);

ViewDialogDemoDark.storyName = 'Content dialog view dark theme';
ViewDialogDemoDark.tags = ['snapshot', 'dark'];
ViewDialogDemoDark.parameters = parametersDarkTheme;

// Expired
export const ExpiredDialogDemoLight = (): React.ReactElement => (
  <DialogDemo
    canbe={fakeAirmetList[7].canbe}
    title="Expired"
    productType="sigmet"
  />
);

ExpiredDialogDemoLight.storyName = 'Content dialog expired view light theme';
ExpiredDialogDemoLight.tags = ['snapshot'];
ExpiredDialogDemoLight.parameters = parametersLightTheme;

export const ExpiredDialogDemoDark = (): React.ReactElement => (
  <DialogDemo
    canbe={fakeAirmetList[7].canbe}
    title="Expired"
    productType="airmet"
  />
);

ExpiredDialogDemoDark.storyName = 'Content dialog expired view dark theme';
ExpiredDialogDemoDark.tags = ['snapshot', 'dark'];
ExpiredDialogDemoDark.parameters = parametersDarkTheme;

// Edit
export const EditDialogDemoLight = (): React.ReactElement => (
  <DialogDemo
    canbe={fakeAirmetList[0].canbe}
    title="Edit"
    productType="sigmet"
  />
);

EditDialogDemoLight.storyName = 'Content dialog edit view light theme';
EditDialogDemoLight.tags = ['snapshot'];
EditDialogDemoLight.parameters = parametersLightTheme;

export const EditDialogDemoDark = (): React.ReactElement => (
  <DialogDemo
    canbe={fakeAirmetList[0].canbe}
    title="Edit"
    productType="airmet"
  />
);

EditDialogDemoDark.storyName = 'Content dialog edit view dark theme';
EditDialogDemoDark.tags = ['snapshot', 'dark'];
EditDialogDemoDark.parameters = parametersDarkTheme;
