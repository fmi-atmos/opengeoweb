/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import _knmiAirmetConfig from './knmi.airmetConfig.json';
import _knmiSigmetConfig from './knmi.sigmetConfig.json';
import _metnorwayAirmetConfig from './metnorway.airmetConfig.json';
import _metnorwaySigmetConfig from './metnorway.sigmetConfig.json';
import _fmiSigmetConfig from './fmi.sigmetConfig.json';
import { AirmetConfig, SigmetConfig } from '../../types';

const knmiAirmetConfig = _knmiAirmetConfig as AirmetConfig;
const knmiSigmetConfig = _knmiSigmetConfig as SigmetConfig;
const metnorwayAirmetConfig = _metnorwayAirmetConfig as AirmetConfig;
const metnorwaySigmetConfig = _metnorwaySigmetConfig as SigmetConfig;
const fmiSigmetConfig = _fmiSigmetConfig as SigmetConfig;

export {
  knmiAirmetConfig,
  knmiSigmetConfig,
  metnorwayAirmetConfig,
  metnorwaySigmetConfig,
  fmiSigmetConfig,
};
