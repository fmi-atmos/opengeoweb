/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';

import {
  LayerManagerConnect,
  MultiMapDimensionSelectConnect,
} from '@opengeoweb/core';

import { TestWrapper } from '../../utils/testUtils';

import { DownloadSigmetAirmetConfigWrapper } from '../MetInfoWrapper';

import {
  knmiAirmetConfig,
  knmiSigmetConfig,
  metnorwayAirmetConfig,
  metnorwaySigmetConfig,
  fmiSigmetConfig,
} from './configurations';

import { createApi as createFakeApi } from '../../utils/fakeApi';
import { ProductConfig, ProductType, SigmetFromBackend } from '../../types';
import { SigmetAirmetApi } from '../../utils/api';
import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';

export default { title: 'demo/Backend Configuration', tags: ['!autodocs'] };

const ConfigurationFromBackendDemo: React.FC<{
  productType: ProductType;
  demoConfig: ProductConfig;
  sigmetList?: SigmetFromBackend[];
}> = ({ productType, demoConfig, sigmetList = fakeSigmetList }) => {
  const fakeApi = (): SigmetAirmetApi => ({
    ...createFakeApi(),
    getAirmetConfiguration: (): Promise<{ data: ProductConfig }> =>
      Promise.resolve({ data: demoConfig }),
    getSigmetConfiguration: (): Promise<{ data: ProductConfig }> =>
      Promise.resolve({ data: demoConfig }),
    getSigmetList: (): Promise<{ data: SigmetFromBackend[] }> =>
      Promise.resolve({ data: sigmetList }),
  });
  return (
    <TestWrapper createApi={fakeApi}>
      <div style={{ height: '100vh' }}>
        <DownloadSigmetAirmetConfigWrapper productType={productType} />
        <LayerManagerConnect bounds="parent" showMapIdInTitle />
        <MultiMapDimensionSelectConnect />
      </div>
    </TestWrapper>
  );
};

export const KNMIAirmetConfiguration = (): React.ReactElement => (
  <ConfigurationFromBackendDemo
    productType="airmet"
    demoConfig={knmiAirmetConfig as ProductConfig}
  />
);

export const KNMISigmetConfiguration = (): React.ReactElement => (
  <ConfigurationFromBackendDemo
    productType="sigmet"
    demoConfig={knmiSigmetConfig as ProductConfig}
  />
);

export const MetNorwayAirmetConfiguration = (): React.ReactElement => (
  <ConfigurationFromBackendDemo
    productType="airmet"
    demoConfig={metnorwayAirmetConfig as ProductConfig}
  />
);

export const MetNorwaySigmetConfiguration = (): React.ReactElement => (
  <ConfigurationFromBackendDemo
    productType="sigmet"
    demoConfig={metnorwaySigmetConfig as ProductConfig}
  />
);

export const FmiSigmetConfiguration = (): React.ReactElement => (
  <ConfigurationFromBackendDemo
    productType="sigmet"
    demoConfig={fmiSigmetConfig as ProductConfig}
    sigmetList={[]}
  />
);
