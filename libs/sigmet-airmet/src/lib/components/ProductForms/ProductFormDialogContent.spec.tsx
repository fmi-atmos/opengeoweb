/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';

import userEvent from '@testing-library/user-event';
import {
  ProductFormDialogContent,
  ProductFormDialogContentProps,
} from './ProductFormDialogContent';
import { Airmet, Sigmet } from '../../types';
import {
  fakeShortVaTestSigmet,
  fakeSigmetList,
  fakeVolcanicCancelSigmetNoMoveTo,
  fakeVolcanicCancelSigmetWithMoveTo,
} from '../../utils/mockdata/fakeSigmetList';
import { noTAC } from './ProductFormTac';
import { TestWrapper } from '../../utils/testUtils';
import {
  fakeAirmetList,
  fakeDraftAirmet,
} from '../../utils/mockdata/fakeAirmetList';
import { fakeSigmetTAC } from '../../utils/fakeApi';
import { airmetConfig, sigmetConfig } from '../../utils/config';
import { translateKeyOutsideComponents } from '../../utils/i18n';

const mockedProductFormDialogContentProps: ProductFormDialogContentProps = {
  isOpen: true,
  toggleDialogStatus: jest.fn(),
  onRenewClick: jest.fn(),
  productType: 'sigmet',
  productConfig: sigmetConfig,
  productCanBe: [],
  product: fakeSigmetList[1].sigmet,
  mode: 'edit',
};
describe('components/ProductForms/ProductFormDialogContent', () => {
  beforeAll(() => {
    jest.spyOn(console, 'log').mockImplementation();
  });

  it('should display only the Cancel button for a published sigmet', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['CANCELLED'],
      product: fakeSigmetList[1].sigmet,
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    expect(screen.getByTestId('productform-dialog-cancel')).toBeTruthy();
    expect(screen.queryByTestId('productform-dialog-draft')).toBeFalsy();
    expect(screen.queryByTestId('productform-dialog-discard')).toBeFalsy();
    expect(screen.queryByTestId('productform-dialog-publish')).toBeFalsy();
  });

  it('should not show the "volcanic ash cloud move to" if not received from the BE', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['CANCELLED'],
      product: fakeVolcanicCancelSigmetNoMoveTo.sigmet,
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    expect(screen.getByTestId('productform-dialog-cancel')).toBeTruthy();
    await waitFor(() =>
      expect(screen.queryByTestId('vaSigmetMoveToFIR')).toBeFalsy(),
    );
  });

  it('should show the "volcanic ash cloud move to" if received from the BE', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['CANCELLED'],
      product: fakeVolcanicCancelSigmetWithMoveTo.sigmet,
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    expect(screen.getByTestId('productform-dialog-cancel')).toBeTruthy();
    expect(screen.getByTestId('vaSigmetMoveToFIR')).toBeTruthy();
  });

  it('should not show the "volcanic ash cloud move to" for short va test sigmets', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['CANCELLED'],
      product: fakeShortVaTestSigmet.sigmet,
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    expect(screen.getByTestId('productform-dialog-cancel')).toBeTruthy();
    await waitFor(() =>
      expect(screen.queryByTestId('vaSigmetMoveToFIR')).toBeFalsy(),
    );
  });

  it('should show errors when pressing publish on an invalid SIGMET', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    expect(
      screen.queryByText('A start position drawing is required'),
    ).toBeFalsy();
    fireEvent.click(screen.getByTestId('productform-dialog-publish'));
    await screen.findByText('A start position drawing is required');
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(0),
    );
  });

  it('should be able to save a SIGMET as draft with only phenomenon as value', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: {
        phenomenon: 'EMBD_TS',
      } as unknown as Sigmet,
      mode: 'new',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    // test publish is disabled
    fireEvent.click(screen.getByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(
        screen.queryAllByText('This field is required').length,
      ).toBeGreaterThan(0),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(0),
    );

    // save as draft
    fireEvent.click(screen.getByTestId('productform-dialog-draft'));
    await waitFor(() =>
      expect(screen.queryAllByText('This field is required')).toHaveLength(0),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1),
    );
  });

  it('should show the confirmation dialog when pressing Publish on a valid SIGMET', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productConfig: sigmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeSigmetList[0].sigmet,
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('productform-dialog-publish'));
    await screen.findByTestId('confirmationDialog');
  });

  it('should show the confirmation dialog when pressing Discard', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productType: 'airmet',
      productConfig: airmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('productform-dialog-discard'));
    await screen.findByTestId('confirmationDialog');
  });

  it('should not show the confirmation dialog when pressing Save on a valid airmet', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productType: 'airmet',
      productConfig: airmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeDraftAirmet,
      mode: 'edit',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('productform-dialog-draft'));
    await waitFor(() => {
      expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
    });
    await waitFor(() => {
      expect(screen.queryByTestId('loader')!.style.opacity).toEqual('1');
    });
    await waitFor(() => {
      expect(screen.queryByTestId('loader')!.style.opacity).toEqual('0');
    });
    expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1);

    expect(props.toggleDialogStatus).toHaveBeenCalledWith(true);
  });

  it('should show the confirmation dialog when pressing Back and form changes have been made', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    const obsField: HTMLInputElement = within(
      screen.getByTestId('isObservationOrForecast-OBS'),
    ).getByRole('radio');

    // trigger form change so form will set isDirty flag
    fireEvent.click(obsField);
    // wait on observation value to be set
    await waitFor(() => {
      expect(obsField.checked).toBeTruthy();
    });
    // close modal
    fireEvent.click(screen.getByTestId('contentdialog-close'));
    await screen.findByTestId('confirmationDialog');
    fireEvent.click(screen.getByTestId('confirmationDialog-cancel'));
    await waitFor(() =>
      expect(screen.queryByTestId('confirmationDialog')).toBeFalsy(),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(false),
    );
  });

  it('should not show the confirmation dialog when pressing Back and no form changes have been made', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    fireEvent.click(screen.queryByTestId('contentdialog-close')!);
    await waitFor(() =>
      expect(screen.queryByTestId('confirmationDialog')).toBeFalsy(),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(false),
    );
  });

  it('should show the confirmation dialog when pressing Cancel', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productType: 'airmet',
      productConfig: airmetConfig,
      productCanBe: ['CANCELLED'],
      product: fakeAirmetList[1].airmet,
      mode: 'view',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('productform-dialog-cancel'));
    await screen.findByTestId('confirmationDialog');
  });

  it('should show the confirmation dialog when pressing Publish after changing movementType from forecast position to stationary', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: {
        ...fakeSigmetList[0].sigmet,
        isObservationOrForecast: 'FCST',
        observationOrForecastTime: dateUtils.dateToString(
          dateUtils.add(dateUtils.utc(), { hours: 2 }),
        ),
      },
      mode: 'edit',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());
    // check that forecast position is checked
    const forecastPosition: HTMLInputElement = within(
      screen.getByTestId('isObservationOrForecast-FCST'),
    ).getByRole('radio');
    await waitFor(() => expect(forecastPosition.checked).toBeTruthy());
    expect(screen.getByTestId('endGeometry')).toBeTruthy();

    // change movementType to stationary
    const stationary: HTMLInputElement = within(
      screen.getByTestId('movementType-STATIONARY'),
    ).getByRole('radio');
    fireEvent.click(stationary);
    await waitFor(() => {
      expect(stationary.checked).toBeTruthy();
    });
    expect(screen.queryByTestId('endGeometry')).toBeFalsy();

    // publish
    fireEvent.click(screen.getByTestId('productform-dialog-publish'));
    await screen.findByTestId('confirmationDialog');
  });

  it('should not contain the movement forecast position option for an airmet', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productType: 'airmet',
      productConfig: airmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeAirmetList[0].airmet,
      mode: 'edit',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    // check that forecast position is not shown
    expect(screen.getByTestId('movementType-MOVEMENT')).toBeTruthy();
    expect(screen.getByTestId('movementType-STATIONARY')).toBeTruthy();
    expect(screen.queryByTestId('movementType-FORECAST_POSITION')).toBeFalsy();
  });

  it('should be able to save a Airmet as draft with only phenomenon as value', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productType: 'airmet',
      productConfig: airmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: {
        phenomenon: 'ISOL_TS',
      } as unknown as Airmet,
      mode: 'new',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    // test publish is disabled
    fireEvent.click(screen.getByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(
        screen.queryAllByText('This field is required').length,
      ).toBeGreaterThan(0),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(0),
    );

    // save as draft
    fireEvent.click(screen.getByTestId('productform-dialog-draft'));
    await waitFor(() =>
      expect(screen.queryAllByText('This field is required')).toHaveLength(0),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledWith(true),
    );
  });

  it('should save a SIGMET with only phenomenon as draft when pressing BACK and choosing Save and Close', async () => {
    const mockGetSigmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: fakeSigmetList });
      });
    });

    const mockGetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeSigmetTAC,
        });
      });
    });

    const mockPostSigmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });

    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    render(
      <TestWrapper
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            getSigmetList: mockGetSigmetList,
            postSigmet: mockPostSigmet,
            getSigmetTAC: mockGetTac,
          };
        }}
      >
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    // choose phenomenon
    fireEvent.mouseDown(
      screen.getByLabelText(
        `${translateKeyOutsideComponents('select')} phenomenon`,
      ),
    );
    const menuItem = await screen.findByText('Severe icing');
    fireEvent.click(menuItem);
    // wait on phenomenon value to be set
    await waitFor(() =>
      expect(
        screen.getByLabelText(
          `${translateKeyOutsideComponents('select')} phenomenon`,
        ).textContent,
      ).toEqual('Severe icing'),
    );

    // press BACK button
    fireEvent.click(screen.getByTestId('contentdialog-close'));
    await screen.findByTestId('confirmationDialog');
    // click save and close
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
    });
    expect(mockPostSigmet).toHaveBeenCalledTimes(1);
    expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1);
    expect(props.toggleDialogStatus).toHaveBeenCalledWith(true);
  });

  it('should not save a SIGMET with only phenomenon as draft when pressing BACK and choosing Discard and Close', async () => {
    const mockGetSigmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: fakeSigmetList });
      });
    });

    const mockGetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeSigmetTAC,
        });
      });
    });

    const mockPostSigmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });

    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    render(
      <TestWrapper
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            getSigmetList: mockGetSigmetList,
            postSigmet: mockPostSigmet,
            getSigmetTAC: mockGetTac,
          };
        }}
      >
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    // choose phenomenon
    fireEvent.mouseDown(screen.getByLabelText('Select phenomenon'));
    const menuItem = await screen.findByText('Severe icing');
    fireEvent.click(menuItem);
    // wait on phenomenon value to be set
    await waitFor(() =>
      expect(screen.getByLabelText('Select phenomenon').textContent).toEqual(
        'Severe icing',
      ),
    );

    // press BACK button
    fireEvent.click(screen.getByTestId('contentdialog-close'));
    await screen.findByTestId('confirmationDialog');
    // click discard and close
    fireEvent.click(screen.getByTestId('confirmationDialog-cancel'));

    await waitFor(() => {
      expect(screen.queryByTestId('confirmationDialog')).toBeFalsy();
    });
    expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1);
    expect(props.toggleDialogStatus).toHaveBeenCalledWith(false);
    expect(mockPostSigmet).not.toHaveBeenCalled();
  });

  it('should not save an AIRMET without a phenomenon when pressing BACK and choosing Save and Close', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productType: 'airmet',
      productConfig: airmetConfig,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null!,
      mode: 'new',
    };

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    const obsField: HTMLInputElement = within(
      screen.getByTestId('isObservationOrForecast-OBS'),
    ).getByRole('radio');
    // trigger form change so form will set isDirty flag
    fireEvent.click(obsField);
    // wait on observation value to be set
    await waitFor(() => {
      expect(obsField.checked).toBeTruthy();
    });
    // press BACK button
    fireEvent.click(screen.getByTestId('contentdialog-close'));
    await screen.findByTestId('confirmationDialog');
    // click save and close
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() =>
      expect(
        screen.queryAllByText('This field is required').length,
      ).toBeGreaterThan(0),
    );
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });
  it('should disable save and publish buttons when in drawing mode', async () => {
    const props: ProductFormDialogContentProps = {
      ...mockedProductFormDialogContentProps,
      productCanBe: ['DRAFTED', 'PUBLISHED'],
      product: fakeSigmetList[1].sigmet,
      mode: 'edit',
    };

    const user = userEvent.setup();

    render(
      <TestWrapper>
        <ProductFormDialogContent {...props} />
      </TestWrapper>,
    );

    expect(screen.getByRole('button', { name: /save/i })).toHaveProperty(
      'disabled',
      false,
    );
    expect(screen.getByRole('button', { name: /publish/i })).toHaveProperty(
      'disabled',
      false,
    );

    const drawTools = await screen.findByTestId('drawtools');
    const firstDrawButton = within(drawTools).getAllByRole('radio')[0];
    await user.click(firstDrawButton);

    expect(screen.getByRole('button', { name: /save/i })).toHaveProperty(
      'disabled',
      true,
    );
    expect(screen.getByRole('button', { name: /publish/i })).toHaveProperty(
      'disabled',
      true,
    );
  });
});
