/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import { useFormContext } from 'react-hook-form';
import { Button } from '@mui/material';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import userEvent from '@testing-library/user-event';

import Progress, {
  getMovementStepValKMHMessage,
  getMovementStepValKTMessage,
  validateMovementSteps,
} from './Progress';
import {
  MovementUnit,
  StartOrEndDrawing,
  AviationPhenomenaCode,
} from '../../../types';
import { sigmetConfig, airmetConfig } from '../../../utils/config';
import {
  getDrawTools,
  getFir,
  getMaxMovementSpeedValue,
  getMaxPolygonPoints,
  getMinMovementSpeedValue,
} from '../utils';
import { testFir, TestWrapper } from '../../../utils/testUtils';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('components/ProductForms/ProductFormFields/Progress', () => {
  const user = userEvent.setup();

  it('should be possible to select a progress', async () => {
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <ReactHookFormProvider>
        <Progress {...props} />
      </ReactHookFormProvider>,
    );
    const field: HTMLInputElement = within(
      screen.getByTestId('movementType-STATIONARY'),
    ).getByRole('radio');

    await waitFor(() => {
      expect(field.checked).toBeFalsy();
    });

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.checked).toBeTruthy();
    });

    expect(screen.getByTestId('movementType-STATIONARY')).toBeTruthy();
    expect(screen.getByTestId('movementType-MOVEMENT')).toBeTruthy();
    expect(screen.getByTestId('movementType-FORECAST_POSITION')).toBeTruthy();
    expect(screen.queryByTestId('movementType-NO_VA_EXP')).toBeFalsy();
  });
  it('expect able to set tools', async () => {
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };

    render(
      <ReactHookFormProvider>
        <Progress {...props} />
      </ReactHookFormProvider>,
    );

    const field: HTMLInputElement = within(
      screen.getByTestId('movementType-FORECAST_POSITION'),
    ).getByRole('radio');

    await waitFor(() => {
      expect(field.checked).toBeFalsy();
    });
    expect(screen.queryByTestId('endGeometry')).toBeFalsy();

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.checked).toBeTruthy();
    });
    props.tools.forEach((tool) =>
      expect(screen.getByTestId(tool.drawModeId)).toBeTruthy(),
    );
  });

  it('expect show tool as active', async () => {
    const tools = getDrawTools(StartOrEndDrawing.end, testFir);
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: tools[0].drawModeId,
      tools,
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };

    render(
      <ReactHookFormProvider>
        <Progress {...props} />
      </ReactHookFormProvider>,
    );

    const field: HTMLInputElement = within(
      screen.getByTestId('movementType-FORECAST_POSITION'),
    ).getByRole('radio');

    await waitFor(() => {
      expect(field.checked).toBeFalsy();
    });
    expect(screen.queryByTestId('endGeometry')).toBeFalsy();

    fireEvent.click(field);

    await waitFor(() => {
      expect(field.checked).toBeTruthy();
    });
  });

  it('should be possible to select a No Volcanish Expected if phenomenon is Volcanich Ash Cloud', async () => {
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'VA_CLD',
          },
        }}
      >
        <Progress {...props} />
      </ReactHookFormProvider>,
    );
    const field: HTMLInputElement = within(
      screen.getByTestId('movementType-NO_VA_EXP'),
    ).getByRole('radio');

    await waitFor(() => {
      expect(field.checked).toBeFalsy();
    });

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.checked).toBeTruthy();
    });

    expect(screen.getByTestId('movementType-STATIONARY')).toBeTruthy();
    expect(screen.getByTestId('movementType-MOVEMENT')).toBeTruthy();
    expect(screen.getByTestId('movementType-FORECAST_POSITION')).toBeTruthy();
    expect(screen.getByTestId('movementType-NO_VA_EXP')).toBeTruthy();
  });
  it('should show progress as disabled', async () => {
    const props = {
      isDisabled: true,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <ReactHookFormProvider>
        <Progress {...props} />
      </ReactHookFormProvider>,
    );

    await waitFor(() => {
      expect(
        screen.getByTestId('movementType-STATIONARY').getAttribute('class'),
      ).toContain('Mui-disabled');
    });
    expect(
      screen.getByTestId('movementType-MOVEMENT').getAttribute('class'),
    ).toContain('Mui-disabled');
    expect(
      screen
        .getByTestId('movementType-FORECAST_POSITION')
        .getAttribute('class'),
    ).toContain('Mui-disabled');
  });
  it('should show direction and speed if MOVEMENT', async () => {
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <TestWrapper>
        <ReactHookFormProvider>
          <Progress {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const field: HTMLInputElement = within(
      screen.getByTestId('movementType-MOVEMENT'),
    ).getByRole('radio');

    await waitFor(() => {
      expect(field.checked).toBeFalsy();
    });
    expect(screen.queryByTestId('movement-movementDirection')).toBeFalsy();
    expect(screen.queryByTestId('movement-movementUnit')).toBeFalsy();
    expect(screen.queryByTestId('movement-movementSpeed')).toBeFalsy();

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.checked).toBeTruthy();
    });
    expect(screen.getByTestId('movement-movementDirection')).toBeTruthy();
    expect(screen.getByTestId('movement-movementUnit')).toBeTruthy();
    expect(screen.getByTestId('movement-movementSpeed')).toBeTruthy();

    // should autoFocus
    expect(
      screen
        .getByLabelText(
          translateKeyOutsideComponents('progress-movement-direction-label'),
        )
        .matches(':focus'),
    ).toBeTruthy();
  });
  it('should show end geometry tools if FORECAST_POSITION', async () => {
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <ReactHookFormProvider>
        <Progress {...props} />
      </ReactHookFormProvider>,
    );
    const field: HTMLInputElement = within(
      screen.getByTestId('movementType-FORECAST_POSITION'),
    ).getByRole('radio');

    await waitFor(() => {
      expect(field.checked).toBeFalsy();
    });
    expect(screen.queryByTestId('endGeometry')).toBeFalsy();

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.checked).toBeTruthy();
    });
    expect(screen.getByTestId('endGeometry')).toBeTruthy();
  });
  it('should show progress as readonly with STATIONARY', () => {
    const props = {
      isDisabled: true,
      isReadOnly: true,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            movementType: 'STATIONARY',
          },
        }}
      >
        <Progress {...props} />
      </ReactHookFormProvider>,
    );

    const field: HTMLInputElement = within(
      screen.getByTestId('movementType-STATIONARY'),
    ).getByRole('radio');
    expect(field.disabled).toBeTruthy();
    expect(field.checked).toBeTruthy();

    const field2: HTMLInputElement = within(
      screen.getByTestId('movementType-MOVEMENT'),
    ).getByRole('radio', { hidden: true });
    expect(field2.disabled).toBeTruthy();
    expect(field2.checked).toBeFalsy();

    const field3: HTMLInputElement = within(
      screen.getByTestId('movementType-FORECAST_POSITION'),
    ).getByRole('radio', { hidden: true });
    expect(field3.disabled).toBeTruthy();
    expect(field3.checked).toBeFalsy();
  });
  it('should show progress as readonly with MOVEMENT', () => {
    const props = {
      isDisabled: true,
      isReadOnly: true,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            defaultValues: {
              movementType: 'MOVEMENT',
              movementSpeed: 5,
              movementUnit: 'KT' as MovementUnit,
              movementDirection: 'N',
            },
          }}
        >
          <Progress {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const field: HTMLInputElement = within(
      screen.getByTestId('movementType-STATIONARY'),
    ).getByRole('radio', { hidden: true });
    expect(field.disabled).toBeTruthy();
    expect(field.checked).toBeFalsy();

    const field2: HTMLInputElement = within(
      screen.getByTestId('movementType-MOVEMENT'),
    ).getByRole('radio');
    expect(field2.disabled).toBeTruthy();
    expect(field2.checked).toBeTruthy();

    const field3: HTMLInputElement = within(
      screen.getByTestId('movementType-FORECAST_POSITION'),
    ).getByRole('radio', { hidden: true });
    expect(field3.disabled).toBeTruthy();
    expect(field3.checked).toBeFalsy();

    expect(
      screen
        .getByLabelText(
          translateKeyOutsideComponents('progress-movement-direction-label'),
        )
        .getAttribute('aria-disabled'),
    ).toEqual('true');
    expect(screen.getByLabelText('Unit').getAttribute('aria-disabled')).toEqual(
      'true',
    );
    expect(
      (screen.getByLabelText('Step speed') as HTMLInputElement).disabled,
    ).toBeTruthy();
  });
  it('should show progress as readonly with FORECAST_POSITION', () => {
    const props = {
      isDisabled: true,
      isReadOnly: true,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            movementType: 'FORECAST_POSITION',
          },
        }}
      >
        <Progress {...props} />
      </ReactHookFormProvider>,
    );

    const field: HTMLInputElement = within(
      screen.getByTestId('movementType-STATIONARY'),
    ).getByRole('radio', { hidden: true });
    expect(field.disabled).toBeTruthy();
    expect(field.checked).toBeFalsy();

    const field2: HTMLInputElement = within(
      screen.getByTestId('movementType-MOVEMENT'),
    ).getByRole('radio', { hidden: true });
    expect(field2.disabled).toBeTruthy();
    expect(field2.checked).toBeFalsy();

    const field3: HTMLInputElement = within(
      screen.getByTestId('movementType-FORECAST_POSITION'),
    ).getByRole('radio');
    expect(field3.disabled).toBeTruthy();
    expect(field3.checked).toBeTruthy();

    expect(screen.queryByTestId('endGeometry-drawTools')).toBeFalsy();
  });

  it('should show an error message when invalid speed is entered in kt', async () => {
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              movementType: 'MOVEMENT',
              movementSpeed: '',
              movementUnit: 'KT' as MovementUnit,
              movementDirection: 'N',
            },
          }}
        >
          <Progress {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const speedInput = screen.getByLabelText(
      translateKeyOutsideComponents('progress-movement-speed-label'),
    );
    await user.type(speedInput, '151');

    await screen.findByText(
      translateKeyOutsideComponents('progress-the-maxium-level-in-movement', {
        movementUnitType: MovementUnit.KT,
        movementUnit: getMaxMovementSpeedValue('KT', 'EHAA', sigmetConfig)!,
      }),
    );
    expect(speedInput.getAttribute('aria-invalid') === 'true').toBeTruthy();

    await user.clear(speedInput);
    await user.type(speedInput, '10');

    await waitFor(() =>
      expect(speedInput.getAttribute('aria-invalid') === 'true').toBeFalsy(),
    );

    await user.clear(speedInput);
    await user.type(speedInput, '18');
    await screen.findByText(
      getMovementStepValKTMessage(
        sigmetConfig.fir_areas['EHAA'].movement_rounding_kt,
      ),
    );
    expect(speedInput.getAttribute('aria-invalid') === 'true').toBeTruthy();

    await user.clear(speedInput);
    await user.type(speedInput, '0');
    await screen.findByText(
      translateKeyOutsideComponents('progress-the-minimum-level-in-movement', {
        movementUnitType: MovementUnit.KT,
        movementUnit: getMinMovementSpeedValue('KT', 'EHAA', sigmetConfig),
      }),
    );
    expect(speedInput.getAttribute('aria-invalid') === 'true').toBeTruthy();

    await user.clear(speedInput);
    await user.type(speedInput, '150');
    expect(speedInput.getAttribute('aria-invalid') === 'true').toBeFalsy();
  });

  it('should show an error message when invalid speed is entered in kmh', async () => {
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'MOVEMENT',
            movementSpeed: '',
            movementUnit: 'KMH' as MovementUnit,
            movementDirection: 'N',
          },
        }}
      >
        <Progress {...props} />
      </ReactHookFormProvider>,
    );

    const speedInput = screen.getByLabelText(
      translateKeyOutsideComponents('progress-movement-speed-label'),
    );
    await user.type(speedInput, '100');
    await screen.findByText(
      translateKeyOutsideComponents('progress-the-maxium-level-in-movement', {
        movementUnitType: 'kmh',
        movementUnit: getMaxMovementSpeedValue('KMH', 'EHAA', sigmetConfig)!,
      }),
    );
    expect(speedInput.getAttribute('aria-invalid') === 'true').toBeTruthy();

    await user.clear(speedInput);
    await user.type(speedInput, '10');
    expect(speedInput.getAttribute('aria-invalid') === 'true').toBeFalsy();

    await user.clear(speedInput);
    await user.type(speedInput, '-0');
    await screen.findByText(
      translateKeyOutsideComponents('progress-the-minimum-level-in-movement', {
        movementUnitType: 'kmh',
        movementUnit: getMinMovementSpeedValue('KMH', 'EHAA', sigmetConfig)!,
      }),
    );
    expect(speedInput.getAttribute('aria-invalid') === 'true').toBeTruthy();

    await user.clear(speedInput);
    await user.type(speedInput, '90');
    expect(speedInput.getAttribute('aria-invalid') === 'true').toBeFalsy();
  });

  it('should remove the end geometry when changing movementType', async () => {
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <ReactHookFormProvider>
        <Progress {...props} />
      </ReactHookFormProvider>,
    );

    const forecastPosition: HTMLInputElement = within(
      screen.getByTestId('movementType-FORECAST_POSITION'),
    ).getByRole('radio');

    fireEvent.click(forecastPosition);
    await waitFor(() => {
      expect(forecastPosition.checked).toBeTruthy();
    });
    expect(screen.getByTestId('endGeometry')).toBeTruthy();

    const stationary: HTMLInputElement = within(
      screen.getByTestId('movementType-STATIONARY'),
    ).getByRole('radio');

    fireEvent.click(stationary);
    await waitFor(() => {
      expect(stationary.checked).toBeTruthy();
    });
    expect(screen.queryByTestId('endGeometry')).toBeFalsy();
    expect(props.onChangeTool).toHaveBeenLastCalledWith(props.tools[4]);
  });

  it('should not show end position for airmet', async () => {
    render(
      <ReactHookFormProvider>
        <Progress
          isDisabled={false}
          productType="airmet"
          productConfig={airmetConfig}
        />
      </ReactHookFormProvider>,
    );

    expect(screen.getByTestId('movementType-STATIONARY')).toBeTruthy();
    expect(screen.getByTestId('movementType-MOVEMENT')).toBeTruthy();
    expect(screen.queryByTestId('movementType-FORECAST_POSITION')).toBeFalsy();
    expect(screen.queryByTestId('movementType-NO_VA_EXP')).toBeFalsy();
  });

  it('should show an error message for maximum 6 points when the end position drawing has more points and is not equal to the FIR', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [1.4359186342468666, 54.09406326927267],
                [2.219743723443398, 50.857775603890964],
                [4.434901584216204, 52.276920619051204],
                [5.116488618300145, 53.20532566834704],
                [5.252806025116932, 54.23372984146369],
                [4.571218991032993, 54.669618903123215],
                [3.242124274569308, 54.669618903123215],
                [1.4359186342468666, 54.09406326927267],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.8285861239569896, 54.53143],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.986539573126695, 52.70658863800587],
                [3.412521, 52.526431],
                [5.184647, 52.877491],
                [5.457282, 53.652036],
                [4.877933, 54.53143],
                [2.8285861239569896, 54.53143],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: end!,
      geoJSONIntersection: intersectionEnd,
    };

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress {...props} />
          <Button onClick={handleSubmit(() => {})}>Validate</Button>
        </>
      );
    };
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              movementType: 'FORECAST_POSITION',
              endGeometry: end,
              endGeometryIntersect: intersectionEnd,
            },
          }}
        >
          <Wrapper />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const firKey = Object.keys(sigmetConfig.fir_areas)[0];
    const firArea = sigmetConfig.fir_areas[firKey];
    const locationIndicatorAtsr = firArea.location_indicator_atsr;
    const maxPoints = getMaxPolygonPoints(locationIndicatorAtsr, sigmetConfig);

    expect(maxPoints).toBeLessThanOrEqual(7);
    expect(screen.getByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(screen.getByText('Validate'));
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('progress-maximumPointsMessage', {
          maxPoints: maxPoints - 1,
        }),
      ),
    ).toBeTruthy();
  });

  it('should show a warning when drawn points exceed 6 points and max polygon points is greater', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [1.4359186342468666, 54.09406326927267],
                [2.219743723443398, 50.857775603890964],
                [4.434901584216204, 52.276920619051204],
                [5.116488618300145, 53.20532566834704],
                [5.252806025116932, 54.23372984146369],
                [4.571218991032993, 54.669618903123215],
                [3.242124274569308, 54.669618903123215],
                [1.4359186342468666, 54.09406326927267],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.8285861239569896, 54.53143],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.986539573126695, 52.70658863800587],
                [3.412521, 52.526431],
                [5.184647, 52.877491],
                [5.457282, 53.652036],
                [4.877933, 54.53143],
                [2.8285861239569896, 54.53143],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress {...props} />
          <Button onClick={handleSubmit(() => {})}>Validate</Button>
        </>
      );
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'FORECAST_POSITION',
            endGeometry: end,
            endGeometryIntersect: intersectionEnd,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    const firKey = Object.keys(sigmetConfig.fir_areas)[1];
    const maxPoints = getMaxPolygonPoints(firKey, sigmetConfig);
    expect(maxPoints).toBeGreaterThan(7);
    const drawnPoints = end.features[0].geometry as GeoJSON.Polygon;
    const totalDrawnPoints = drawnPoints.coordinates[0].length;

    expect(screen.getByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('geometry-more-than-6-points-message', {
            totalDrawnPoints,
          }),
        ),
      ).toBeFalsy(),
    );
  });

  it('should not show an error message for maximum 6 points when FIR is selected as end position', async () => {
    const end = getFir(sigmetConfig);
    const intersectionEnd = getFir(sigmetConfig);

    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress {...props} />
          <Button onClick={handleSubmit(() => {})}>Validate</Button>
        </>
      );
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'FORECAST_POSITION',
            endGeometry: end,
            endGeometryIntersect: intersectionEnd,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    const firKey = Object.keys(sigmetConfig.fir_areas)[0];
    const firArea = sigmetConfig.fir_areas[firKey];
    const locationIndicatorAtsr = firArea.location_indicator_atsr;
    const maxPoints = getMaxPolygonPoints(locationIndicatorAtsr, sigmetConfig);

    expect(screen.getByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('geometry-more-than-6-points-message', {
            totalDrawnPoints: maxPoints - 1,
          }),
        ),
      ).toBeFalsy(),
    );
  });
  it('should not show an error message for maximum 6 points when the end position drawing has 6 points', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [1.4359186342468666, 54.09406326927267],
                [2.219743723443398, 50.857775603890964],
                [4.434901584216204, 52.276920619051204],
                [5.116488618300145, 53.20532566834704],
                [4.571218991032993, 54.669618903123215],
                [3.242124274569308, 54.669618903123215],
                [1.4359186342468666, 54.09406326927267],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.8285861239569896, 54.53143],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.986539573126695, 52.70658863800587],
                [3.412521, 52.526431],
                [5.457282, 53.652036],
                [4.877933, 54.53143],
                [2.8285861239569896, 54.53143],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress {...props} />
          <Button onClick={handleSubmit(() => {})}>Validate</Button>
        </>
      );
    };
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            movementType: 'FORECAST_POSITION',
            endGeometry: end,
            endGeometryIntersect: intersectionEnd,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    const firKey = Object.keys(sigmetConfig.fir_areas)[0];
    const firArea = sigmetConfig.fir_areas[firKey];
    const locationIndicatorAtsr = firArea.location_indicator_atsr;
    const maxPoints = getMaxPolygonPoints(locationIndicatorAtsr, sigmetConfig);

    expect(screen.getByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('progress-maximumPointsMessage', {
            maxPoints: maxPoints - 1,
          }),
        ),
      ).toBeFalsy(),
    );
  });

  it('should show an error message for drawing inside the FIR when the end position drawing has more than 6 points but has no intersection with the FIR ', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [8.899296657466017, 54.37292536640029],
                [8.69482054724084, 52.19343690011639],
                [10.56918489097167, 51.41375109471964],
                [13.056977565378059, 51.41375109471964],
                [15.272135426150864, 52.3810545583907],
                [14.181596171616562, 54.57096052166186],
                [11.830120904026963, 55.15933041390611],
                [8.899296657466017, 54.37292536640029],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [[]],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress {...props} />
          <Button onClick={handleSubmit(() => {})}>Validate</Button>
        </>
      );
    };
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              movementType: 'FORECAST_POSITION',
              endGeometry: end,
              endGeometryIntersect: intersectionEnd,
            },
          }}
        >
          <Wrapper />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(screen.getByText('Validate'));
    await screen.findByText(
      translateKeyOutsideComponents('progress-no-intersection-message'),
    );
  });

  it('should show a warning message if there are multiple intersections with the FIR', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.763574121746228, 55.7517694491503],
                [7.577660215570578, 54.441809771698],
                [4.554179315180896, 56.77870860348328],
                [2.763574121746228, 55.7517694491503],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'MultiPolygon',
            coordinates: [
              [
                [
                  [4.365729269907419, 55.315807175634454],
                  [3.299427839634416, 55.605958027800156],
                  [3.368817, 55.764314],
                  [4.331914, 55.332644],
                  [4.365729269907419, 55.315807175634454],
                ],
              ],
              [
                [
                  [6.500001602574285, 54.735051191917506],
                  [5.526314849017847, 55.0000007017522],
                  [6.500002, 55.000002],
                  [6.500001602574285, 54.735051191917506],
                ],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: intersectionEnd,
    };

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              movementType: 'FORECAST_POSITION',
              endGeometry: end,
              endGeometryIntersect: intersectionEnd,
            },
          }}
        >
          <Progress {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('endGeometry')).toBeTruthy();
    await screen.findByText(
      translateKeyOutsideComponents('geometry-multi-intersections-message'),
    );
  });

  it('should show an error message if more than two polygons are drawn', async () => {
    const end = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [3.996575273929873, 56.338767599638814],
                [5.3666880610038135, 56.951914773869866],
                [5.844634382076118, 55.57175436782723],
                [3.996575273929873, 56.338767599638814],
              ],
              [
                [4.5382477711451505, 53.0637004930805],
                [5.334824972932328, 53.87919160339488],
                [6.099539086648015, 52.446617738370804],
                [4.5382477711451505, 53.0637004930805],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionEnd = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [3.996575273929873, 56.338767599638814],
                [5.3666880610038135, 56.951914773869866],
                [5.844634382076118, 55.57175436782723],
                [3.996575273929873, 56.338767599638814],
              ],
              [
                [4.5382477711451505, 53.0637004930805],
                [5.334824972932328, 53.87919160339488],
                [6.099539086648015, 52.446617738370804],
                [4.5382477711451505, 53.0637004930805],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <Progress {...props} />
          <Button onClick={handleSubmit(() => {})}>Validate</Button>
        </>
      );
    };
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              movementType: 'FORECAST_POSITION',
              endGeometry: end,
              endGeometryIntersect: intersectionEnd,
            },
          }}
        >
          <Wrapper />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('endGeometry')).toBeTruthy();
    fireEvent.click(screen.getByText('Validate'));
    await screen.findByText(
      translateKeyOutsideComponents('progress-only-one-position-drawing'),
    );
  });

  it('should not show an error message when entering a decimal MOVEMENT SPEED but convert it directly to integer', async () => {
    const props = {
      isDisabled: false,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              movementType: 'MOVEMENT',
              movementSpeed: '',
              movementUnit: 'KT' as MovementUnit,
              movementDirection: 'N',
            },
          }}
        >
          <Progress {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const speedInput = screen.getByLabelText(
      translateKeyOutsideComponents('progress-movement-speed-label'),
    );
    await user.type(speedInput, '.5');

    /* wait for the value to be converted to integer */
    await waitFor(() => expect(speedInput.getAttribute('value')).toEqual('5'));
    expect(speedInput.getAttribute('aria-invalid') === 'true').toBeFalsy();
  });

  describe('validateMovementSteps', () => {
    it('should use default values if not set in config', () => {
      const {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        movement_rounding_kt,
        // eslint-disable-next-line @typescript-eslint/naming-convention
        movement_rounding_kmh,
        ...configEHAANoRounding
      } = sigmetConfig.fir_areas['EHAA'];
      const configNoRounding = {
        ...sigmetConfig,
        fir_areas: { EHAA: configEHAANoRounding },
      };

      expect(validateMovementSteps('5', 'KT', configNoRounding, 'EHAA')).toBe(
        true,
      );
      expect(validateMovementSteps('10', 'KMH', configNoRounding, 'EHAA')).toBe(
        true,
      );
      expect(validateMovementSteps('7', 'KT', configNoRounding, 'EHAA')).toBe(
        getMovementStepValKTMessage(),
      );
      expect(validateMovementSteps('15', 'KMH', configNoRounding, 'EHAA')).toBe(
        getMovementStepValKMHMessage(),
      );
    });

    it('should validate the steps correctly for kt using the config', () => {
      expect(validateMovementSteps('', 'KT', sigmetConfig, 'EHAA')).toBe(true);
      // if no FIR passed should get the first FIR
      expect(validateMovementSteps('5', 'KT', sigmetConfig, undefined!)).toBe(
        true,
      );
      expect(validateMovementSteps('65', 'KT', sigmetConfig, 'EHAA')).toBe(
        true,
      );
      expect(validateMovementSteps('10', 'KT', sigmetConfig, 'EHAA')).toBe(
        true,
      );
      expect(validateMovementSteps('40', 'KT', sigmetConfig, 'EZZZ')).toBe(
        true,
      );
      expect(validateMovementSteps('12', 'KT', sigmetConfig, 'EHAA')).toBe(
        getMovementStepValKTMessage(
          sigmetConfig.fir_areas['EHAA'].movement_rounding_kt,
        ),
      );
      expect(validateMovementSteps('12', 'KT', sigmetConfig, 'EZZZ')).toBe(
        getMovementStepValKTMessage(
          sigmetConfig.fir_areas['EZZZ'].movement_rounding_kt,
        ),
      );
    });
    it('should validate the steps correctly for kmh sing the config', () => {
      expect(validateMovementSteps('', 'KMH', sigmetConfig, 'EHAA')).toBe(true);
      // if no FIR passed should get the first FIR
      expect(validateMovementSteps('10', 'KMH', sigmetConfig, undefined!)).toBe(
        true,
      );
      expect(validateMovementSteps('60', 'KMH', sigmetConfig, 'EHAA')).toBe(
        true,
      );
      expect(validateMovementSteps('60', 'KMH', sigmetConfig, 'EZZZ')).toBe(
        true,
      );
      expect(validateMovementSteps('65', 'KMH', sigmetConfig, 'EHAA')).toBe(
        getMovementStepValKMHMessage(
          sigmetConfig.fir_areas['EHAA'].movement_rounding_kmh,
        ),
      );
      expect(validateMovementSteps('12', 'KMH', sigmetConfig, 'EZZZ')).toBe(
        getMovementStepValKMHMessage(
          sigmetConfig.fir_areas['EZZZ'].movement_rounding_kmh,
        ),
      );
    });
  });

  it('should disable all radio buttons in Progress group if phenomenon is Radioactive Cloud', async () => {
    const props = {
      isDisabled: true,
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      activeTool: '',
      tools: getDrawTools(StartOrEndDrawing.end, testFir),
      onChangeTool: jest.fn(),
      geoJSON: undefined,
      geoJSONIntersection: undefined,
    };
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'RDOACT_CLD' as AviationPhenomenaCode,
          },
        }}
      >
        <Progress {...props} />
      </ReactHookFormProvider>,
    );
    const fieldStationary: HTMLInputElement = within(
      screen.getByTestId('movementType-STATIONARY'),
    ).getByRole('radio');
    const fieldMovement: HTMLInputElement = within(
      screen.getByTestId('movementType-MOVEMENT'),
    ).getByRole('radio');
    const fieldForecastPosition: HTMLInputElement = within(
      screen.getByTestId('movementType-FORECAST_POSITION'),
    ).getByRole('radio');

    expect(fieldStationary).toBeTruthy();
    expect(fieldMovement).toBeTruthy();
    expect(fieldForecastPosition).toBeTruthy();
    expect(fieldStationary.disabled).toBeTruthy();
    expect(fieldMovement.disabled).toBeTruthy();
    expect(fieldForecastPosition.disabled).toBeTruthy();
  });
});
