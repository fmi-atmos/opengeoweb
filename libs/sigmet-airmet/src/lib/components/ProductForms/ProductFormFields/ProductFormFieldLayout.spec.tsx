/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { render, screen } from '@testing-library/react';
import React from 'react';
import ProductFormFieldLayout from './ProductFormFieldLayout';

describe('components/ProductForms/ProductFormFields/ProductFormFieldLayout', () => {
  it('should render with default props', async () => {
    render(
      <ProductFormFieldLayout>
        <p>testing</p>
      </ProductFormFieldLayout>,
    );

    expect(await screen.findByText('testing')).toBeTruthy();
  });

  it('should render with title and custom props', async () => {
    const props = {
      title: 'Test title',
      'data-testid': 'test-id',
      children: <p>testing</p>,
    };

    render(<ProductFormFieldLayout {...props} />);

    expect(await screen.findByText('testing')).toBeTruthy();
    expect(await screen.findByText('Test title')).toBeTruthy();
    expect(await screen.findByTestId('test-id')).toBeTruthy();
  });
});
