/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';

import { dateUtils } from '@opengeoweb/shared';

import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import ValidUntil, { getDefaultValidUntilValue } from './ValidUntil';
import ValidFrom from './ValidFrom';
import { sigmetConfig, airmetConfig } from '../../../utils/config';
import { translateKeyOutsideComponents } from '../../../utils/i18n';
import { TestWrapper } from '../../../utils/testUtils';
import { getMaxHoursOfValidity } from '../utils';
import { FIRConfigSigmet, ProductConfig } from '../../../types';

describe('components/ProductForms/ProductFormFields/ValidUntil', () => {
  it('should show current time + 4 hours (default) rounded to the next half-hour for SIGMET', async () => {
    const now = new Date('2023-02-03T14:00:00Z');
    const spy = jest.spyOn(dateUtils, 'utc').mockReturnValue(now);

    const timeToShow = dateUtils.roundToNearestMinutes(
      dateUtils.add(now, {
        hours: 4,
      }),
      { nearestTo: 30 },
    );

    render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const field = screen.getByRole('textbox');
    await waitFor(() => {
      expect(field.getAttribute('value')!).toEqual(
        dateUtils.dateToString(timeToShow, dateUtils.DATE_FORMAT_DATEPICKER),
      );
    });

    expect(screen.getByText(`Select date and time`)).toBeTruthy();
    spy.mockRestore();
  });
  it('should show passed-in date with default max validation time', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              validDateStart: '2020-11-17T13:40:00Z',
            },
          }}
        >
          <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const field = screen.getByRole('textbox');

    await waitFor(() => {
      expect(field.getAttribute('value')).toEqual('17/11/2020 17:40');
    });
  });

  it('should be possible to set a date', async () => {
    const fixedDate = new Date('2024-08-14T07:00:00Z');
    jest.spyOn(dateUtils, 'utc').mockReturnValue(fixedDate);

    const timeToShow = dateUtils.roundToNearestMinutes(
      dateUtils.add(fixedDate, {
        hours: 4,
      }),
      { nearestTo: 30 },
    );

    render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const field = screen.getByRole('textbox');

    await waitFor(() => {
      expect(field.getAttribute('value')!).toEqual(
        dateUtils.dateToString(timeToShow, dateUtils.DATE_FORMAT_DATEPICKER),
      );
    });

    fireEvent.change(field, { target: { value: '17/11/2020 13:03' } });

    // Ensure the field reflects the manually set value
    await waitFor(() => {
      expect(field.getAttribute('value')!).toEqual('17/11/2020 13:03');
    });

    jest.restoreAllMocks();
  });

  it('should show correct error', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              validDateEnd: '2020-11-17T17:03:00Z',
              validDateStart: '2020-11-17T13:03:00Z',
            },
          }}
        >
          <ValidFrom productConfig={sigmetConfig} isDisabled={false} />
          <ValidUntil productConfig={sigmetConfig} isDisabled={false} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const validUntil = screen.getAllByRole('textbox')[1];

    const expectedInitialValue = '17/11/2020 17:03';
    await waitFor(() => {
      expect(validUntil.getAttribute('value')!).toEqual(expectedInitialValue);
    });

    // before validDateStart
    fireEvent.change(validUntil, { target: { value: '17/11/2000 15:30' } }); // Rounded time
    await waitFor(() => {
      expect(
        screen.getByText(translateKeyOutsideComponents('valid-until-time')),
      ).toBeTruthy();
    });

    // too far after validDateStart considering max validity
    const maxHoursOfValidity = 4; // Assuming SIGMET default

    fireEvent.change(validUntil, { target: { value: '17/11/2300 15:30' } }); // Rounded time
    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('valid-until-time-error', {
            hours: maxHoursOfValidity,
          }),
        ),
      ).toBeTruthy();
    });

    // equal to validDateStart considering rounding
    fireEvent.change(validUntil, { target: { value: '17/11/2020 11:30' } });
    await waitFor(() => {
      expect(
        screen.getByText(translateKeyOutsideComponents('valid-until-time')),
      ).toBeTruthy();
    });
  });

  it('should show the correct disabled input', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ValidUntil productConfig={sigmetConfig} isDisabled />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    const field = screen.getByRole('textbox');
    await waitFor(() => {
      expect(field).toBeTruthy();
    });
    expect(field.getAttribute('disabled')).toBeDefined();
  });

  it('should show the correct readonly labels', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ValidUntil isReadOnly productConfig={sigmetConfig} isDisabled />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    expect(
      screen.getByText(translateKeyOutsideComponents('date-and-time')),
    ).toBeTruthy();
  });

  describe('getDefaultValidUntilValue', () => {
    it('should add the correct validity hours and delay', () => {
      const now = new Date('2023-02-03T14:00:00Z');
      const spy = jest.spyOn(dateUtils, 'utc').mockReturnValue(now);

      const expectedSigmetDate = dateUtils.dateToString(
        dateUtils.roundToNearestMinutes(
          dateUtils.add(now, {
            hours: getMaxHoursOfValidity('', '', sigmetConfig),
          }),
          { nearestTo: 30 },
        ),
      );

      expect(getDefaultValidUntilValue(sigmetConfig)).toBe(expectedSigmetDate);

      const expectedAirmetDate = dateUtils.dateToString(
        dateUtils.roundToNearestMinutes(
          dateUtils.add(now, {
            hours: getMaxHoursOfValidity('', '', airmetConfig),
          }),
          { nearestTo: 30 },
        ),
      );

      expect(getDefaultValidUntilValue(airmetConfig)).toBe(expectedAirmetDate);

      spy.mockRestore();
    });

    it('should correctly calculate valid until based on selected phenomenon', () => {
      const mockConfig: ProductConfig = {
        fir_areas: {
          FIR_XYZ: {
            max_hours_of_validity: 4,
            va_max_hours_of_validity: 6,
            tc_max_hours_of_validity: 12,
          } as FIRConfigSigmet,
        },
        valid_from_delay_minutes: 5,
        location_indicator_mwo: '',
        active_firs: [],
        default_validity_minutes: 120,
      };

      const now = new Date('2023-02-03T14:00:00Z');
      const spy = jest.spyOn(dateUtils, 'utc').mockReturnValue(now);

      const validFrom = dateUtils.dateToString(now);

      // Test for VA_CLD phenomenon
      const validUntilForVolcanicAshCloud = getDefaultValidUntilValue(
        mockConfig,
        'VA_CLD',
        'FIR_XYZ',
        validFrom,
      );
      expect(validUntilForVolcanicAshCloud).toBe(
        dateUtils.dateToString(
          dateUtils.roundToNearestMinutes(
            dateUtils.add(now, {
              hours: 6,
              minutes: 0,
            }),
            { nearestTo: 30 },
          ),
        ),
      );

      // Test for TC phenomenon
      const validUntilForTC = getDefaultValidUntilValue(
        mockConfig,
        'TC',
        'FIR_XYZ',
        validFrom,
      );
      expect(validUntilForTC).toBe(
        dateUtils.dateToString(
          dateUtils.roundToNearestMinutes(
            dateUtils.add(now, {
              hours: 12,
              minutes: 0,
            }),
            { nearestTo: 30 },
          ),
        ),
      );

      spy.mockRestore();
    });

    it('should set default validity to 2 hours for thunderstorms', () => {
      const now = new Date('2023-02-03T14:00:00Z');
      const spy = jest.spyOn(dateUtils, 'utc').mockReturnValue(now);
      const mockConfig: ProductConfig = {
        fir_areas: {
          FIR_XYZ: {
            max_hours_of_validity: 4,
            va_max_hours_of_validity: 6,
            tc_max_hours_of_validity: 12,
          } as FIRConfigSigmet,
        },
        valid_from_delay_minutes: 5,
        location_indicator_mwo: '',
        active_firs: [],
        default_validity_minutes: 120,
      };

      const validFrom = dateUtils.dateToString(now);

      const thunderstormPhenomena = [
        'EMBD_TS',
        'EMBD_TSGR',
        'FRQ_TS',
        'FRQ_TSGR',
        'OBSC_TS',
        'OBSC_TSGR',
        'SQL_TS',
        'SQL_TSGR',
        'ISOL_TS',
        'ISOL_TSGR',
        'OCNL_TS',
        'OCNL_TSGR',
      ];

      thunderstormPhenomena.forEach((phenomenon) => {
        const validUntilForThunderstorm = getDefaultValidUntilValue(
          mockConfig,
          phenomenon,
          'FIR_XYZ',
          validFrom,
        );
        expect(validUntilForThunderstorm).toBe(
          dateUtils.dateToString(
            dateUtils.roundToNearestMinutes(
              dateUtils.add(now, {
                hours: 2,
                minutes: 0,
              }),
              { nearestTo: 30 },
            ),
          ),
        );
      });

      spy.mockRestore();
    });
  });
});
