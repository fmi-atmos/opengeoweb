![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/sigmet-airmet/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-sigmet-airmet)

# Content

- [Installation](#installation)
- [Use](#use)
  - [API](#api)
  - [Configuration](#configuration)
    - [Configuration file location](#configuration-file-location)
    - [Configuration schema location](#configuration-schema-location)
    - [Configuration typescript types](#configuration-typescript-types)
    - [Add a new configuration key](#add-a-new-configuration-key)
    - [Using a new configuration key](#using-a-new-configuration-key)
- [Documentation](#documentation)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

# sigmet-airmet

React component library with Sigmet and Airmet components for the opengeoweb project.
This library was generated with [Nx](https://nx.dev).

## Installation

```
npm install @opengeoweb/sigmet-airmet
```

## Use

You can use any component exported from sigmet-airmet by importing them, for example:

```
import { MetInfoWrapper } from '@opengeoweb/sigmet-airmet'
```

### API

Documentation of the API can be found on Swagger (make sure you're connected to the KNMI network).

The front-end uses the [axios](https://github.com/axios/axios) library to fetch data from the api. Importing components from the sigmet-airmet lib should be wrapped by the ApiProvider component with a specified baseURL. Example:

```
import { ApiProvider } from '@opengeoweb/api';
import { MetInfoWrapper } from '@opengeoweb/sigmet-airmet';

const sigmetProductConfig = {
  location_indicator_mwo: 'EHDB',
  fir_areas: {
    EHAA: {
      fir_name: 'AMSTERDAM FIR',
      location_indicator_atsr: 'EHAA',
      location_indicator_atsu: 'EHAA',
      area_preset: 'NL_FIR',
      max_hours_of_validity: 4,
      hours_before_validity: 4,
      tc_max_hours_of_validity: 6,
      tc_hours_before_validity: 12,
      va_max_hours_of_validity: 6,
      va_hours_before_validity: 12,
      adjacent_firs: ['EKDK', 'EDWW', 'EDGG', 'EBBU', 'EGTT', 'EGPX'],
      units: [
        {
          unit_type: 'level_unit',
          allowed_units: ['FT', 'FL'],
        },
        {
          unit_type: 'movement_unit',
          allowed_units: ['KT'],
        },
      ],
    },
  },
  active_firs: ['EHAA'],
};

const myAuth = {
  username: 'string',
  token: 'string',
  refresh_token: 'string',
};

const Component = () => {
  const [auth, onSetAuth] = React.useState(myAuth);

  return (
      <ApiProvider
        baseURL="http://test.com"
        appURL="http://app.com"
        auth={auth}
        onSetAuth={onSetAuth}
        createApi={createApi}
        authTokenURL="url/tokenrefresh"
      >
        <MetInfoWrapper productType='sigmet' productConfig={sigmetProductConfig} />
      </ApiProvider>
  )}
```

### Configuration

Every country has different rules for aviation so a configuration file is required to let the module work correctly. Airmet uses basic configuration which has been expanded for sigmet to contain more custom properties. The validation rules are stored in schema files following the conventions of https://json-schema.org/. The configuration file is fetched from the backend.

#### Configuration file location

These are the default configuration files for both products. They are being used in the stories and snapshots.

- airmet: `libs/sigmet-airmet/src/lib/utils/config/airmet.config.json`
- sigmet: `libs/sigmet-airmet/src/lib/utils/config/sigmet.config.json`

#### Configuration typescript types

The `SigmetConfig` and `AirmetConfig` types can be found in `libs/sigmet-airmet/src/lib/types.ts` and are leading in defining the configuration files.

#### Add a new configuration key

Follow these steps to add a new key to the configuration file.

In this example, we're going to add a new property called `my_new_config`: an array with strings that is used by sigmet and airmet.

1. Since the new prop is used by both sigmet and airmet, we want to add it to the `FIRArea` typescript type. Inside `libs/sigmet-airmet/src/lib/types.ts` add the following:

```
export type FIRArea = {
  // ...rest of props
  units: AllowedUnits[];
  my_new_config: string[];
};
```

If you now run `nx typecheck sigmet-airmet` it will fail, and that's expected as we added a new required prop but we have not added it to our configs. We will fix this later

2. Add `my_new_config` to the configuration files. After this both stories of sigmet and airmet should work again without errors:

- inside `libs/sigmet-airmet/src/lib/utils/config/sigmet.config.json` add `"my_new_config": ["test"]`
- inside `libs/sigmet-airmet/src/lib/utils/config/airmet.config.json` add `"my_new_config": ["test"]`

3. Next fix the typescript errors. When running `nx typecheck sigmet-airmet` it will complain on missing props inside the `libs/sigmet-airmet/src/lib/components/ProductForms/utils.spec.tsx` test. Add the new config keys to config examples. After this typescript should be fixed. We will add tests later for this file when we want to use the new property.

4. Last but not least, don't forget to add the new keys in other configs (here and in opmet-backend).

#### Using a new configuration key

Every config key can be used throughout the aviationmodule. If we have a new property that we want to use, follow these steps.

1. Create a new method inside `libs/sigmet-airmet/src/lib/components/ProductForms/utils.tsx`. This method should have the config as param to extract the value to use inside the code. Afterwards add the tests in `libs/sigmet-airmet/src/lib/components/ProductForms/utils.spec.tsx`.

```
export const getMyNewConfigTest = (
fir: string,
config: ProductConfig,
): string[] => {
const {
  my_new_config,
} = config.fir_areas[fir];

return my_new_config;
};
```

2. Decide on which component the configuration should be used. Let's take `StartGeometry` as example. This component needs a `productConfig` prop. Open the component and extend the props of the component with `ConfigurableFormFieldProps` like `interface StartGeometryProps extends ConfigurableFormFieldProps` and remove unneeded double props. The `StartGeometry` component is imported in `[Sigmet/Airmet]Form`, in these files pass the `productConfig` key. Afterwards you can use the prop inside `StartGeometry` like

```
const StartGeometry: React.FC<StartGeometryProps> = ({
  // other props
  setDrawModeType,
  productConfig
}: StartGeometryProps) => {
  const myConfigValues  = getMyNewConfigTest(productConfig)
```

3. After all is working add the unit tests and make sure typescript linting will pass again, as `ConfigurableFormFieldProps` makes sure you're not forgetting to pass the config.

### TypeScript Documentation

- [TypeScript Docs](https://opengeoweb.gitlab.io/opengeoweb/typescript-docs/sigmet-airmet/)
