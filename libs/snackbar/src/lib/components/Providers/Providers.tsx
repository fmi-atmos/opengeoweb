/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import i18n from 'i18next';
import { I18nextProvider } from 'react-i18next';
import { Store } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { ThemeWrapper } from '@opengeoweb/theme';

import { initSnackbarI18n } from '../../utils/i18n';

interface SnackbarTranslationsWrapperProps {
  children?: React.ReactNode;
}

const SnackbarI18nProvider: React.FC<SnackbarTranslationsWrapperProps> = ({
  children,
}) => {
  initSnackbarI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};

export const DemoWrapper: React.FC<SnackbarTranslationsWrapperProps> = ({
  children,
}) => {
  return (
    <SnackbarI18nProvider>
      <ThemeWrapper>{children}</ThemeWrapper>
    </SnackbarI18nProvider>
  );
};

interface ProviderProps {
  children?: React.ReactNode;
  store: Store;
}

export const DemoWrapperConnect: React.FC<ProviderProps> = ({
  children,
  store,
}) => {
  return (
    <Provider store={store}>
      <DemoWrapper>{children}</DemoWrapper>
    </Provider>
  );
};
