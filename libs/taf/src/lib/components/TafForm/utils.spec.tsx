/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  renderHook,
  screen,
  act,
} from '@testing-library/react';
import Sortable from 'sortablejs';
import { dateUtils } from '@opengeoweb/shared';
import { TFunction } from 'i18next';
import {
  formatValidToString,
  parseValidToObject,
} from './TafFormRow/validations/validField';
import { parseVisibilityToObject } from './TafFormRow/validations/visibility';
import { parseWindToObject } from './TafFormRow/validations/wind';
import {
  clearTafFormData,
  convertTafValuesToObject,
  createChangeGroup,
  emptyBaseForecast,
  emptyChangegroup,
  formatFormValues,
  formatIssueTime,
  getAmendedCorrectedTafTimes,
  getConfirmationDialogButtonLabel,
  getConfirmationDialogContent,
  getConfirmationDialogTitle,
  getFieldNames,
  getIsFormDefaultDisabled,
  getFormRowPosition,
  getMessageType,
  getTafStatusLabel,
  isTafAmendedCorrectedChanged,
  MIN_NUMBER_CHANGE_GROUP,
  parseFormValues,
  prepareTafValues,
  shouldRetrieveTAC,
  useDragOrder,
  useTACGenerator,
} from './utils';
import {
  fakeAmendmentTaf,
  fakeDraftAmendmentTaf,
  fakeDraftCorrectedTaf,
  fakeDraftFixedTaf,
  fakeDraftTaf,
  fakeDraftTafWithSameDates,
  fakeExpiredTaf,
  fakeNewTaf,
  fakePublishedFixedTaf,
  fakePublishedTaf,
  fakePublishedTafWithChangeGroups,
} from '../../utils/mockdata/fakeTafList';
import {
  ChangeGroup,
  Taf,
  TafFormData,
  TAC,
  ChangeGroupFormData,
} from '../../types';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { TafThemeApiProvider } from '../Providers';

describe('components/TafForm/utils', () => {
  describe('formatIssueTime', () => {
    it('should return Not issued for a taf without issuedate', () => {
      expect(formatIssueTime(undefined!)).toEqual('Not issued');
    });

    it('should return DDHHmmZ for a given issuedate', () => {
      expect(formatIssueTime('2020-05-10T12:30:00Z')).toEqual('101230Z');
    });
  });

  describe('formatFormValues', () => {
    it('should format form values with weather', () => {
      const testTaf = {
        change: 'BECMG',
        weather: { weather1: 'MIFG' },
      } as ChangeGroup;

      expect(formatFormValues(testTaf)).toEqual(testTaf);
    });

    it('should format form values with wind', () => {
      const testTaf = {
        change: 'BECMG',
        wind: {
          direction: 1,
          speed: 1,
          unit: 'KT',
        },
      } as ChangeGroup;

      expect(formatFormValues(testTaf)).toEqual({
        ...testTaf,
        wind: '00101',
      });
    });
    it('should format form values with valid fields', () => {
      const testTaf = {
        change: 'BECMG',
        valid: {
          start: '2020-12-07T18:00:00Z',
          end: '2020-12-09T00:00:00Z',
        },
      } as ChangeGroup;
      expect(formatFormValues(testTaf)).toEqual({
        ...testTaf,
        valid: '0718/0824',
      });

      const testTaf2 = {
        change: 'BECMG',
        valid: {
          start: '2020-12-31T22:00:00Z',
          end: '2021-01-01T02:00:00Z',
        },
      } as ChangeGroup;
      expect(formatFormValues(testTaf2)).toEqual({
        ...testTaf2,
        valid: '3122/0102',
      });

      const testTaf3 = {
        change: 'FM',
        valid: {
          start: '2020-12-31T22:00:00Z',
        },
      } as ChangeGroup;
      expect(formatFormValues(testTaf3)).toEqual({
        ...testTaf3,
        valid: '312200',
      });

      const testTaf4 = {
        change: 'FM',
        valid: {
          start: '2021-01-01T22:00:00Z',
        },
      } as ChangeGroup;
      expect(formatFormValues(testTaf4)).toEqual({
        ...testTaf4,
        valid: '012200',
      });

      expect(
        formatFormValues({
          wind: {
            direction: 0,
            speed: 0,
            unit: 'KT',
          },
          valid: {
            start: '2021-01-01T22:00:00Z',
          },
        }),
      ).toEqual({
        wind: '00000',
        valid: '012200',
      });

      expect(
        formatFormValues({
          visibility: { range: 0, unit: 'M' },
          valid: {
            start: '2021-01-01T22:00:00Z',
          },
        }),
      ).toEqual({
        visibility: '0000',
        valid: '012200',
      });
    });

    it('should format form values with visibility and cavOK', () => {
      const testTaf = {
        change: 'BECMG',
        visibility: {
          range: 50,
          unit: 'M',
        },
      } as ChangeGroup;
      expect(formatFormValues(testTaf)).toEqual({
        ...testTaf,
        visibility: '0050',
      });

      // If cavOK then should be removed and visibility added
      const testTaf1 = {
        change: 'BECMG',
        cavOK: true,
      } as ChangeGroup;
      expect(formatFormValues(testTaf1)).toEqual({
        change: 'BECMG',
        visibility: 'CAVOK',
      });
      // cavOK should be removed from the object
      const testTaf2 = {
        change: 'BECMG',
        visibility: {
          range: 50,
          unit: 'M',
        },
        cavOK: false,
      } as ChangeGroup;
      expect(formatFormValues(testTaf2)).toEqual({
        change: 'BECMG',
        visibility: '0050',
      });
    });
  });

  describe('parseFormValues', () => {
    const dummyTafStart = dateUtils.dateToString(
      dateUtils.utc(),
      dateUtils.DATE_FORMAT_YEAR,
    )!;
    const dummyTafEnd = dateUtils.dateToString(
      dateUtils.add(dateUtils.utc(), { days: 1 }),
      dateUtils.DATE_FORMAT_YEAR,
    )!;

    it('should trim string values', () => {
      const testTaf = {
        change: 'BECMG ',
        probability: '  PROB30 ',
        valid: ' 070600',
      };

      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        change: 'BECMG',
        probability: 'PROB30',
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
    });

    it('should parse form values with wind', () => {
      const testTaf = {
        wind: '00101KT',
        valid: '070600',
      };

      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf,
        wind: parseWindToObject(testTaf.wind),
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });

      expect(
        parseFormValues(
          { wind: '00000', valid: '0718/0824' },
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual({
        wind: {
          direction: 0,
          speed: 0,
          unit: 'KT',
        },
        valid: {
          start: '2020-12-07T18:00:00Z',
          end: '2020-12-09T00:00:00Z',
        },
      });
    });

    it('should parse form values with visibility', () => {
      const testTaf = {
        visibility: 'CAVOK',
        valid: '070600',
      };

      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
        cavOK: true,
      });

      const testTaf2 = {
        visibility: '0400',
        valid: '070600',
      };

      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        valid: parseValidToObject(testTaf2.valid, dummyTafStart, dummyTafEnd),
        ...parseVisibilityToObject(testTaf2.visibility),
      });

      expect(
        parseFormValues(
          { visibility: '0000', valid: '0718/0824' },
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual({
        visibility: {
          range: 0,
          unit: 'M',
        },
        valid: {
          start: '2020-12-07T18:00:00Z',
          end: '2020-12-09T00:00:00Z',
        },
      });
    });

    it('should parse form values with valid field', () => {
      const testTaf = {
        change: 'BECMG',
        valid: '3122/0102',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf,
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
      const testTaf2 = {
        change: 'BECMG',
        valid: '0718/0824',
      };
      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf2,
        valid: parseValidToObject(testTaf2.valid, dummyTafStart, dummyTafEnd),
      });

      const testTaf3 = {
        change: 'FM',
        valid: '071800',
      };
      expect(parseFormValues(testTaf3, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf3,
        valid: parseValidToObject(testTaf3.valid, dummyTafStart, dummyTafEnd),
      });

      const testTaf4 = {
        change: 'FM',
        valid: '082200',
      };
      expect(parseFormValues(testTaf4, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf4,
        valid: parseValidToObject(testTaf4.valid, dummyTafStart, dummyTafEnd),
      });

      const testTaf5 = {
        change: 'FM',
        valid: '012200',
      };
      expect(parseFormValues(testTaf5, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf5,
        valid: parseValidToObject(testTaf5.valid, dummyTafStart, dummyTafEnd),
      });
    });

    it('should parse form values with weather', () => {
      const testTaf = {
        change: 'BECMG',
        weather: {
          weather1: 'SN  ',
          weather2: ' ',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf,
        weather: {
          weather1: 'SN',
        },
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });

      const testTaf2 = {
        change: 'BECMG',
        weather: {
          weather1: '-RADZPL  ',
          weather2: '  +SHRA',
          weather3: 'TSSNGSRA',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf2,
        weather: {
          weather1: '-RADZPL',
          weather2: '+SHRA',
          weather3: 'TSSNGSRA',
        },
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });

      const testTaf3 = {
        change: 'BECMG',
        weather: {
          weather1: 'SN  ',
          weather2: '',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf3, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf3,
        weather: {
          weather1: 'SN',
        },
        valid: parseValidToObject(testTaf3.valid, dummyTafStart, dummyTafEnd),
      });
    });
    it('should parse form values with cloud', () => {
      const testTaf = {
        change: 'BECMG',
        cloud: {
          cloud1: 'FEW001  ',
          cloud2: 'SCT050CB',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf,
        cloud: {
          cloud1: {
            coverage: 'FEW',
            height: 1,
          },
          cloud2: {
            coverage: 'SCT',
            height: 50,
            type: 'CB',
          },
        },
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
      const testTaf2 = {
        change: 'BECMG',
        cloud: {
          cloud1: 'FEW001  ',
          cloud2: '   ',
          cloud3: '',
          cloud4: '',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf2,
        cloud: {
          cloud1: {
            coverage: 'FEW',
            height: 1,
          },
        },
        valid: parseValidToObject(testTaf2.valid, dummyTafStart, dummyTafEnd),
      });

      expect(
        parseFormValues(
          { cloud: { cloud1: 'VV000' }, valid: '0718/0824' },
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual({
        cloud: { cloud1: { verticalVisibility: 0 } },
        valid: {
          start: '2020-12-07T18:00:00Z',
          end: '2020-12-09T00:00:00Z',
        },
      });
    });
    it('should remove cloud when undefined or empty', () => {
      const testTaf = {
        change: 'BECMG',
        visibility: 'CAVOK',
        cloud: undefined,
        valid: '070600',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        change: 'BECMG',
        cavOK: true,
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
      const testTaf2 = {
        change: 'BECMG',
        visibility: 'CAVOK',
        cloud: { cloud1: '' },
        valid: '070600',
      };
      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        change: 'BECMG',
        cavOK: true,
        valid: parseValidToObject(testTaf2.valid, dummyTafStart, dummyTafEnd),
      });
    });
    it('should remove weather when all empty', () => {
      const testTaf = {
        change: 'BECMG',
        visibility: 'CAVOK',
        cloud: undefined,
        weather: {
          weather1: '',
          weather2: '',
          weather3: '',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        change: 'BECMG',
        cavOK: true,
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
    });
    it('should remove wind and visibility when left empty', () => {
      const testTaf = {
        change: 'BECMG',
        wind: '',
        visibility: '',
        valid: '070600',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        change: 'BECMG',
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
    });
    it('should remove change, probability and valid when left empty', () => {
      const testTaf = {
        change: '',
        visibility: 'CAVOK',
        probability: '',
        valid: null!,
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        cavOK: true,
      });
      const testTaf2 = {
        change: '',
        visibility: 'CAVOK',
        probability: '',
        valid: '',
      };
      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        cavOK: true,
      });
    });
  });

  describe('convertTafValuesToObject', () => {
    it('should convert a taf with only a baseforecast to a taf object', () => {
      const newTaf = {
        uuid: '5f9583fjdf0f337925',
        baseTime: '2021-01-12T18:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        validDateEnd: '2021-01-14T00:00:00Z',
        location: 'EHAM',
        status: 'NEW',
        type: 'NORMAL',
        messageType: 'ORG',
        baseForecast: {
          valid: '1218/1324',
          visibility: '1000',
          wind: '12010',
        },
      } as TafFormData;
      expect(convertTafValuesToObject(newTaf)).toEqual({
        baseForecast: {
          valid: {
            end: '2021-01-14T00:00:00Z',
            start: '2021-01-12T18:00:00Z',
          },
          visibility: {
            range: 1000,
            unit: 'M',
          },
          wind: {
            direction: 120,
            speed: 10,
            unit: 'KT',
          },
        },
        baseTime: '2021-01-12T18:00:00Z',
        location: 'EHAM',
        messageType: 'ORG',
        status: 'NEW',
        type: 'NORMAL',
        uuid: '5f9583fjdf0f337925',
        validDateEnd: '2021-01-14T00:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
      });
    });
    it('should remove changegroups and issueDate when empty', () => {
      const testTaf = {
        uuid: '5f9583fjdf0f337925',
        baseTime: '2021-01-12T18:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        validDateEnd: '2021-01-14T00:00:00Z',
        location: 'EHAM',
        status: 'NEW',
        messageType: 'ORG',
        baseForecast: {
          valid: '1218/1324',
          visibility: '1000',
          wind: '12010',
        },
        changeGroups: [{}, {}],
        issueDate: '',
      } as TafFormData;
      expect(convertTafValuesToObject(testTaf)).toEqual({
        baseForecast: {
          valid: {
            end: '2021-01-14T00:00:00Z',
            start: '2021-01-12T18:00:00Z',
          },
          visibility: {
            range: 1000,
            unit: 'M',
          },
          wind: {
            direction: 120,
            speed: 10,
            unit: 'KT',
          },
        },
        baseTime: '2021-01-12T18:00:00Z',
        location: 'EHAM',
        messageType: 'ORG',
        status: 'NEW',
        uuid: '5f9583fjdf0f337925',
        validDateEnd: '2021-01-14T00:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
      });
    });
    it('should convert a taf with changegroups and issueDate to a taf object', () => {
      const testTaf = {
        uuid: '5f9583fjdf0f337925',
        baseTime: '2021-01-12T18:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        validDateEnd: '2021-01-14T00:00:00Z',
        location: 'EHAM',
        status: 'NEW',
        type: 'NORMAL',
        messageType: 'ORG',
        baseForecast: {
          valid: '1218/1324',
          visibility: '1000',
          wind: '12010',
        },
        changeGroups: [
          {},
          { change: 'BECMG', valid: '1220/1300', visibility: '2000' },
          { change: 'FM', valid: '1300/1324', visibility: '3000' },
        ],
        issueDate: '2021-01-12T17:00:00Z',
      } as TafFormData;
      expect(convertTafValuesToObject(testTaf)).toEqual({
        baseForecast: {
          valid: {
            end: '2021-01-14T00:00:00Z',
            start: '2021-01-12T18:00:00Z',
          },
          visibility: {
            range: 1000,
            unit: 'M',
          },
          wind: {
            direction: 120,
            speed: 10,
            unit: 'KT',
          },
        },
        baseTime: '2021-01-12T18:00:00Z',
        location: 'EHAM',
        messageType: 'ORG',
        status: 'NEW',
        type: 'NORMAL',
        uuid: '5f9583fjdf0f337925',
        validDateEnd: '2021-01-14T00:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        issueDate: '2021-01-12T17:00:00Z',
        changeGroups: [
          {
            change: 'BECMG',
            valid: {
              end: '2021-01-13T00:00:00Z',
              start: '2021-01-12T20:00:00Z',
            },
            visibility: {
              range: 2000,
              unit: 'M',
            },
          },
          {
            change: 'FM',
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-13T00:00:00Z',
            },
            visibility: {
              range: 3000,
              unit: 'M',
            },
          },
        ],
      });
    });

    it('should convert a taf with an empty changegroup row to a taf object without changegroups', () => {
      const testTaf = {
        uuid: '5f9583fjdf0f337925',
        baseTime: '2021-01-12T18:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        validDateEnd: '2021-01-14T00:00:00Z',
        location: 'EHAM',
        status: 'NEW',
        type: 'NORMAL',
        messageType: 'ORG',
        baseForecast: {
          valid: '1218/1324',
          visibility: '1000',
          wind: '12010',
        },
        changeGroups: [
          {
            probability: '',
            change: '',
            valid: '',
            visibility: '',
            wind: '',
            weather: { weather1: '', weather2: '', weather3: '' },
            cloud: { cloud1: '', cloud2: '', cloud3: '', cloud4: '' },
          },
        ],
        issueDate: '2021-01-12T17:00:00Z',
      } as TafFormData;
      expect(convertTafValuesToObject(testTaf)).toEqual({
        baseForecast: {
          valid: {
            end: '2021-01-14T00:00:00Z',
            start: '2021-01-12T18:00:00Z',
          },
          visibility: {
            range: 1000,
            unit: 'M',
          },
          wind: {
            direction: 120,
            speed: 10,
            unit: 'KT',
          },
        },
        baseTime: '2021-01-12T18:00:00Z',
        location: 'EHAM',
        messageType: 'ORG',
        status: 'NEW',
        type: 'NORMAL',
        uuid: '5f9583fjdf0f337925',
        validDateEnd: '2021-01-14T00:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        issueDate: '2021-01-12T17:00:00Z',
      });
    });
  });

  describe('shouldRetrieveTAC', () => {
    it('should retrieve a TAC only if there is a windspeed value and visibility value', () => {
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-12T18:00:00Z',
            },
            visibility: {
              range: 1000,
              unit: 'M',
            },
            wind: {
              direction: 120,
              speed: 10,
              unit: 'KT',
            },
          },
        } as Taf),
      ).toEqual(true);
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            visibility: {
              range: 1000,
            },
          },
        } as Taf),
      ).toEqual(false);
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            wind: {
              direction: 120,
              speed: 10,
              unit: 'KT',
            },
          },
        } as Taf),
      ).toEqual(false);
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-12T18:00:00Z',
            },
            visibility: {},
            wind: {},
          },
        } as Taf),
      ).toEqual(false);
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-12T18:00:00Z',
            },
            visibility: {
              range: '',
              unit: 'M',
            },
            wind: {
              direction: '',
              speed: 10,
              unit: 'KT',
            },
          },
        } as unknown as Taf),
      ).toEqual(false);
      expect(
        shouldRetrieveTAC({
          baseForecast: {},
        } as unknown as Taf),
      ).toEqual(false);
    });
    it('should retrieve a TAC even if wind or visibility are 00000 or 0000', () => {
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-12T18:00:00Z',
            },
            visibility: {
              range: 0,
              unit: 'M',
            },
            wind: {
              direction: 0,
              speed: 0,
              unit: 'KT',
            },
          },
        } as Taf),
      ).toEqual(true);
    });
    it('should retrieve a TAC if no visibility but cavOK is used', () => {
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-12T18:00:00Z',
            },
            cavOK: true,
            wind: {
              direction: 0,
              speed: 0,
              unit: 'KT',
            },
          },
        } as Taf),
      ).toEqual(true);
    });
  });

  describe('getFieldNames', () => {
    it('should return a object with names for baseForecast', () => {
      const fieldNames = getFieldNames(false, -1);
      expect(fieldNames.messageType).toEqual('messageType');
      expect(fieldNames.location).toEqual('location');
      expect(fieldNames.issueDate).toEqual('issueDate');
      expect(fieldNames.probability).toBeUndefined();
      expect(fieldNames.change).toBeUndefined();
      expect(fieldNames.valid).toEqual(`baseForecast.valid`);
      expect(fieldNames.wind).toEqual(`baseForecast.wind`);
      expect(fieldNames.visibility).toEqual(`baseForecast.visibility`);
      expect(fieldNames.weather1).toEqual(`baseForecast.weather.weather1`);
      expect(fieldNames.weather2).toEqual(`baseForecast.weather.weather2`);
      expect(fieldNames.weather3).toEqual(`baseForecast.weather.weather3`);
      expect(fieldNames.cloud1).toEqual(`baseForecast.cloud.cloud1`);
      expect(fieldNames.cloud2).toEqual(`baseForecast.cloud.cloud2`);
      expect(fieldNames.cloud3).toEqual(`baseForecast.cloud.cloud3`);
      expect(fieldNames.cloud4).toEqual(`baseForecast.cloud.cloud4`);
    });

    it('should return a object with names for changeGroups', () => {
      const index = 4;
      const fieldNames = getFieldNames(true, index);
      expect(fieldNames.messageType).toBeUndefined();
      expect(fieldNames.location).toBeUndefined();
      expect(fieldNames.issueDate).toBeUndefined();
      expect(fieldNames.probability).toEqual(
        `changeGroups[${index}].probability`,
      );
      expect(fieldNames.change).toEqual(`changeGroups[${index}].change`);
      expect(fieldNames.valid).toEqual(`changeGroups[${index}].valid`);
      expect(fieldNames.wind).toEqual(`changeGroups[${index}].wind`);
      expect(fieldNames.visibility).toEqual(
        `changeGroups[${index}].visibility`,
      );
      expect(fieldNames.weather1).toEqual(
        `changeGroups[${index}].weather.weather1`,
      );
      expect(fieldNames.weather2).toEqual(
        `changeGroups[${index}].weather.weather2`,
      );
      expect(fieldNames.weather3).toEqual(
        `changeGroups[${index}].weather.weather3`,
      );
      expect(fieldNames.cloud1).toEqual(`changeGroups[${index}].cloud.cloud1`);
      expect(fieldNames.cloud2).toEqual(`changeGroups[${index}].cloud.cloud2`);
      expect(fieldNames.cloud3).toEqual(`changeGroups[${index}].cloud.cloud3`);
      expect(fieldNames.cloud4).toEqual(`changeGroups[${index}].cloud.cloud4`);
    });
  });

  describe('useTACGenerator', () => {
    it('should show error message when failing', async () => {
      const request = (): Promise<{ data: string }> =>
        new Promise((_resolve, reject) => {
          reject(new Error('Error from backend'));
        });

      const { result } = renderHook(
        () => useTACGenerator(fakePublishedTaf.taf, request),
        { wrapper: TafThemeApiProvider },
      );

      await waitFor(() =>
        expect(result.current[0]).toEqual(
          translateKeyOutsideComponents('TAC-failed'),
        ),
      );
    });
    it('should not update unmounted component', async () => {
      let mockResponse = 'test data received';
      const succesCounter = jest.fn();
      const request = (): Promise<{ data: string }> =>
        new Promise((resolve) => {
          setTimeout(() => {
            resolve({ data: mockResponse });
            succesCounter();
          }, 100);
        });

      const TestComponent: React.FC = () => {
        const [tac, setTAC] = useTACGenerator(fakePublishedTaf.taf, request);

        return (
          <div>
            <p role="heading" aria-level={1}>
              {tac as string}
            </p>
            <button
              type="button"
              onClick={(): void => setTAC(fakePublishedTaf.taf)}
            >
              button
            </button>
          </div>
        );
      };

      const { unmount } = render(
        <TafThemeApiProvider>
          <TestComponent />
        </TafThemeApiProvider>,
      );

      await waitFor(() => {
        expect(succesCounter).toHaveBeenCalledTimes(0);
      });
      expect(screen.getByRole('heading').textContent).toEqual('Retrieving TAC');
      await waitFor(() => {
        expect(succesCounter).toHaveBeenCalledTimes(1);
      });
      expect(screen.getByRole('heading').textContent).toEqual(mockResponse);

      // trigger new fetch
      const newResponseData = 'new test data';
      mockResponse = newResponseData;
      fireEvent.click(screen.getByRole('button'));

      await waitFor(() => {
        expect(succesCounter).toHaveBeenCalledTimes(2);
      });
      expect(screen.getByRole('heading').textContent).toEqual(newResponseData);

      // trigger new fetch and unmount
      fireEvent.click(screen.getByRole('button'));
      unmount();
      expect(succesCounter).toHaveBeenCalledTimes(2);
    });
    it('should be able to change TAC directly', async () => {
      const request = (): Promise<{ data: string }> =>
        new Promise((resolve) => {
          resolve({ data: undefined! });
        });

      const { result } = renderHook(
        () => useTACGenerator(undefined!, request),
        { wrapper: TafThemeApiProvider },
      );
      const testTAC = 'test test test test';

      await waitFor(() => {
        expect(result.current[0]).toEqual(
          translateKeyOutsideComponents('TAC-not-available'),
        );
        result.current[2](testTAC);
      });

      await waitFor(() => {
        expect(result.current[0]).toEqual(testTAC);
      });
    });
    it('should be able to request a TAC in json', async () => {
      const testTAC: TAC = { tac: [[{ text: 'TAF' }]] };
      const request = (): Promise<{ data: TAC }> =>
        new Promise((resolve) => {
          resolve({ data: testTAC });
        });

      const { result } = renderHook(
        () => useTACGenerator(fakePublishedFixedTaf.taf, request, true),
        { wrapper: TafThemeApiProvider },
      );

      await waitFor(() => {
        expect(result.current[0]).toEqual(testTAC);
      });
    });

    it('should request a new TAC when incoming taf changed', async () => {
      const mockRequestTAC = jest.fn().mockResolvedValue('TEST TAC');

      const { rerender } = renderHook(
        ({ taf }) => useTACGenerator(taf, mockRequestTAC),
        {
          initialProps: { taf: fakePublishedFixedTaf.taf },
          wrapper: TafThemeApiProvider,
        },
      );
      await waitFor(() => {
        expect(mockRequestTAC).toHaveBeenCalledTimes(1);
      });
      // rerender for same taf, should not request a new TAC
      rerender({ taf: fakePublishedFixedTaf.taf });
      await waitFor(() => {
        expect(mockRequestTAC).toHaveBeenCalledTimes(1);
      });
      // rerender for new taf, should request a new TAC
      rerender({ taf: fakeDraftTaf.taf });
      await waitFor(() => {
        expect(mockRequestTAC).toHaveBeenCalledTimes(2);
      });
    });
  });

  describe('useDragOrder', () => {
    it('should handle order', async () => {
      const trigger = jest.fn();
      const move = jest.fn();
      const fakeEvent = { oldIndex: 1, newIndex: 0 } as Sortable.SortableEvent;
      const { result } = renderHook(() => useDragOrder(trigger, move));

      const { onChangeOrder, activeDraggingIndex, onStartDrag } =
        result.current;

      expect(onChangeOrder).toBeDefined();
      expect(onStartDrag).toBeDefined();
      expect(activeDraggingIndex).toBeNull();
      await act(async () => {
        onChangeOrder(fakeEvent);
      });
      expect(trigger).toHaveBeenCalled();

      expect(move).toHaveBeenCalledWith(fakeEvent.oldIndex, fakeEvent.newIndex);
    });
  });

  describe('getConfirmationDialogTitle', () => {
    const t = ((key: string) => key) as unknown as TFunction;

    it('Should return Save properties when action = CLEAR', () => {
      expect(getConfirmationDialogTitle('CLEAR', t)).toEqual(
        'confirm-clear-title',
      );
    });
    it('Should return Save properties when action = PUBLISH', () => {
      expect(getConfirmationDialogTitle('PUBLISH', t)).toEqual(
        'button-publish',
      );
    });
    it('Should return Save properties when action = AMEND', () => {
      expect(getConfirmationDialogTitle('AMEND', t)).toEqual(
        'confirm-amend-title',
      );
    });
    it('Should return Save properties when action = CORRECT', () => {
      expect(getConfirmationDialogTitle('CORRECT', t)).toEqual(
        'confirm-correct-title',
      );
    });
  });

  describe('isTafAMDCORChanged', () => {
    it('should return false if isFormDirty = false', () => {
      expect(
        isTafAmendedCorrectedChanged(
          fakeDraftAmendmentTaf.taf,
          fakeDraftAmendmentTaf.taf,
        ),
      ).toBeFalsy();
    });
    it('should return false if no changes have been made to the taf even if isFormDirty = true', () => {
      expect(
        isTafAmendedCorrectedChanged(
          fakeDraftAmendmentTaf.taf,
          fakeDraftAmendmentTaf.taf,
        ),
      ).toBeFalsy();
    });
    it('should return false if originalTaf or currentTaf are not available', () => {
      expect(
        isTafAmendedCorrectedChanged(null!, fakeDraftAmendmentTaf.taf),
      ).toBeFalsy();
      expect(
        isTafAmendedCorrectedChanged(fakeDraftAmendmentTaf.taf, null!),
      ).toBeFalsy();
      expect(isTafAmendedCorrectedChanged(null!, null!)).toBeFalsy();
    });
  });

  describe('getConfirmationDialogContent', () => {
    const t = ((key: string) => key) as unknown as TFunction;

    it('Should return correct content when action = CLEAR', () => {
      expect(
        getConfirmationDialogContent(
          'CLEAR',
          () =>
            isTafAmendedCorrectedChanged(
              fakeDraftAmendmentTaf.taf,
              fakeDraftAmendmentTaf.taf,
            ),
          t,
        ),
      ).toEqual('confirm-clear-content');
    });
    it('Should return correct content when action = PUBLISH', () => {
      expect(
        getConfirmationDialogContent(
          'PUBLISH',
          () =>
            isTafAmendedCorrectedChanged(
              fakeDraftTaf.taf,
              fakePublishedTafWithChangeGroups.taf,
            ),
          t,
        ),
      ).toEqual('confirm-publish-content');
    });
    it('Should return correct content when action = AMEND', () => {
      expect(
        getConfirmationDialogContent(
          'AMEND',
          () =>
            isTafAmendedCorrectedChanged(
              fakePublishedTafWithChangeGroups.taf,
              fakeAmendmentTaf.taf,
            ),
          t,
        ),
      ).toEqual('confirm-correct-amend-content');
      expect(
        getConfirmationDialogContent(
          'AMEND',
          () =>
            isTafAmendedCorrectedChanged(
              fakePublishedTafWithChangeGroups.taf,
              fakePublishedTafWithChangeGroups.taf,
            ),
          t,
        ),
      ).toEqual('confirm-correct-amend-content-no-changes');
      expect(
        getConfirmationDialogContent(
          'AMEND',
          () =>
            isTafAmendedCorrectedChanged(
              fakePublishedTafWithChangeGroups.taf,
              fakePublishedTafWithChangeGroups.taf,
            ),
          t,
        ),
      ).toEqual('confirm-correct-amend-content-no-changes');
    });
    it('Should return correct content when action = CORRECT', () => {
      expect(
        getConfirmationDialogContent(
          'CORRECT',
          () =>
            isTafAmendedCorrectedChanged(
              fakePublishedTafWithChangeGroups.taf,
              fakeAmendmentTaf.taf,
            ),
          t,
        ),
      ).toEqual('confirm-correct-amend-content');
      expect(
        getConfirmationDialogContent(
          'CORRECT',
          () =>
            isTafAmendedCorrectedChanged(
              fakePublishedTafWithChangeGroups.taf,
              fakePublishedTafWithChangeGroups.taf,
            ),
          t,
        ),
      ).toEqual('confirm-correct-amend-content-no-changes');
      expect(
        getConfirmationDialogContent(
          'CORRECT',
          () =>
            isTafAmendedCorrectedChanged(
              fakePublishedTafWithChangeGroups.taf,
              fakePublishedTafWithChangeGroups.taf,
            ),
          t,
        ),
      ).toEqual('confirm-correct-amend-content-no-changes');
    });
  });

  describe('getConfirmationDialogButtonLabel', () => {
    const t = ((key: string) => key) as unknown as TFunction;
    it('Should return correct button label for CLEAR', () => {
      expect(getConfirmationDialogButtonLabel('CLEAR', t)).toEqual(
        'button-clear',
      );
    });
    it('Should return correct default button label', () => {
      expect(getConfirmationDialogButtonLabel('PUBLISH', t)).toEqual(
        'button-publish',
      );
      expect(getConfirmationDialogButtonLabel('AMEND', t)).toEqual(
        'button-publish',
      );
      expect(getConfirmationDialogButtonLabel('CORRECT', t)).toEqual(
        'button-publish',
      );
    });
  });

  describe('getTafStatusLabel', () => {
    const t = ((key: string) => key) as unknown as TFunction;
    it('should return correct labels for button statusses', () => {
      expect(getTafStatusLabel(t, 'DRAFT_AMENDED')).toEqual(
        'status-draft-amendment',
      );
      expect(getTafStatusLabel(t, 'DRAFT_CORRECTED')).toEqual(
        'status-draft-correction',
      );
      expect(getTafStatusLabel(t, 'NEW')).toEqual('status-new');
      expect(getTafStatusLabel(t, 'PUBLISHED')).toEqual('status-published');
      expect(getTafStatusLabel(t, 'AMENDED')).toEqual(
        'status-published-amendment',
      );
      expect(getTafStatusLabel(t, 'CORRECTED')).toEqual(
        'status-published-correction',
      );
      expect(getTafStatusLabel(t, 'CANCELLED')).toEqual('status-cancelled');
      expect(getTafStatusLabel(t, 'EXPIRED')).toEqual('status-expired');
    });
    it('should return correct labels for button statusses and action combinations', () => {
      expect(getTafStatusLabel(t, 'PUBLISHED', 'AMEND', false)).toEqual(
        'status-new-amendment',
      );
      expect(getTafStatusLabel(t, 'PUBLISHED', 'CORRECT', false)).toEqual(
        'status-new-correction',
      );
      expect(getTafStatusLabel(t, 'AMENDED', 'AMEND', false)).toEqual(
        'status-new-amendment',
      );
      expect(getTafStatusLabel(t, 'CORRECTED', 'CORRECT', false)).toEqual(
        'status-new-correction',
      );
      expect(getTafStatusLabel(t, 'CORRECTED', 'AMEND', false)).toEqual(
        'status-new-amendment',
      );
      expect(getTafStatusLabel(t, 'AMENDED', 'CORRECT', false)).toEqual(
        'status-new-correction',
      );
    });
    it('should only use status and ignore action if isFormDisabled true or not passed in', () => {
      expect(getTafStatusLabel(t, 'PUBLISHED', 'AMEND')).toEqual(
        'status-published',
      );
      expect(getTafStatusLabel(t, 'PUBLISHED', 'CORRECT')).toEqual(
        'status-published',
      );
      expect(getTafStatusLabel(t, 'AMENDED', 'AMEND')).toEqual(
        'status-published-amendment',
      );
      expect(getTafStatusLabel(t, 'CORRECTED', 'CORRECT')).toEqual(
        'status-published-correction',
      );
      expect(getTafStatusLabel(t, 'CORRECTED', 'AMEND')).toEqual(
        'status-published-correction',
      );
      expect(getTafStatusLabel(t, 'AMENDED', 'CORRECT')).toEqual(
        'status-published-amendment',
      );
      expect(getTafStatusLabel(t, 'PUBLISHED', 'AMEND', true)).toEqual(
        'status-published',
      );
      expect(getTafStatusLabel(t, 'PUBLISHED', 'CORRECT', true)).toEqual(
        'status-published',
      );
      expect(getTafStatusLabel(t, 'AMENDED', 'AMEND', true)).toEqual(
        'status-published-amendment',
      );
      expect(getTafStatusLabel(t, 'CORRECTED', 'CORRECT', true)).toEqual(
        'status-published-correction',
      );
      expect(getTafStatusLabel(t, 'CORRECTED', 'AMEND', true)).toEqual(
        'status-published-correction',
      );
      expect(getTafStatusLabel(t, 'AMENDED', 'CORRECT', true)).toEqual(
        'status-published-amendment',
      );
    });
  });
  describe('getMessageType', () => {
    it('should return the original messageType if action is not CORRECT or AMEND', () => {
      expect(getMessageType(fakePublishedTaf.taf, 'PUBLISH')).toEqual(
        fakePublishedTaf.taf.messageType,
      );
      expect(getMessageType(fakePublishedTaf.taf, 'DRAFT_AMEND')).toEqual(
        fakePublishedTaf.taf.messageType,
      );
    });
    it('should return the original messageType if action is not CORRECT or AMEND', () => {
      expect(getMessageType(fakePublishedTaf.taf, 'PUBLISH')).toEqual(
        fakePublishedTaf.taf.messageType,
      );
      expect(getMessageType(fakePublishedTaf.taf, 'DRAFT_AMEND')).toEqual(
        fakePublishedTaf.taf.messageType,
      );
    });
    it('should return the AMD if action is AMEND', () => {
      expect(getMessageType(fakePublishedTaf.taf, 'AMEND')).toEqual('AMD');
    });
    it('should return the COR if action is CORRECT', () => {
      expect(getMessageType(fakePublishedTaf.taf, 'CORRECT')).toEqual('COR');
    });
    it('should return the CNL if action is CANCEL', () => {
      expect(getMessageType(fakePublishedTaf.taf, 'CANCEL')).toEqual('CNL');
    });
  });

  describe('getAmendedCorrectedTafTimes', () => {
    it('should add previousStart and End for CORRECT and DRAFT_CORRECT', () => {
      expect(
        getAmendedCorrectedTafTimes(fakePublishedTaf.taf, 'CORRECT'),
      ).toEqual({
        ...fakePublishedTaf.taf,
        previousValidDateStart: fakePublishedTaf.taf.validDateStart,
        previousValidDateEnd: fakePublishedTaf.taf.validDateEnd,
      });
      expect(
        getAmendedCorrectedTafTimes(fakePublishedTaf.taf, 'DRAFT_CORRECT'),
      ).toEqual({
        ...fakePublishedTaf.taf,
        previousValidDateStart: fakePublishedTaf.taf.validDateStart,
        previousValidDateEnd: fakePublishedTaf.taf.validDateEnd,
      });
    });
    it('should add previousStart and End for AMEND and DRAFT_AMEND AND ALSO UPDATE VALID START', () => {
      const now = `${dateUtils.dateToString(
        dateUtils.utc(),
        dateUtils.DATE_FORMAT_YEAR,
      )}T14:00:00Z`;

      const validObj = {
        start: now,
        end: fakePublishedTaf.taf.baseForecast.valid.end,
      };

      jest.useFakeTimers().setSystemTime(new Date(now));

      expect(
        getAmendedCorrectedTafTimes(fakePublishedTaf.taf, 'AMEND'),
      ).toEqual({
        ...fakePublishedTaf.taf,
        previousValidDateStart: fakePublishedTaf.taf.validDateStart,
        previousValidDateEnd: fakePublishedTaf.taf.validDateEnd,
        validDateStart: now,
        baseForecast: { ...fakePublishedTaf.taf.baseForecast, valid: validObj },
      });
      expect(
        getAmendedCorrectedTafTimes(fakePublishedTaf.taf, 'DRAFT_AMEND'),
      ).toEqual({
        ...fakePublishedTaf.taf,
        previousValidDateStart: fakePublishedTaf.taf.validDateStart,
        previousValidDateEnd: fakePublishedTaf.taf.validDateEnd,
        validDateStart: now,
        baseForecast: { ...fakePublishedTaf.taf.baseForecast, valid: validObj },
      });
    });
    it('should not update previous dates if there are already previous dates for DRAFT_AMENDMENT', () => {
      const now = `${dateUtils.dateToString(
        dateUtils.utc(),
        dateUtils.DATE_FORMAT_YEAR,
      )}T14:00:00Z`;

      const validObj = {
        start: now,
        end: fakeDraftAmendmentTaf.taf.baseForecast.valid.end,
      };
      jest.useFakeTimers().setSystemTime(new Date(now));
      expect(
        getAmendedCorrectedTafTimes(fakeDraftAmendmentTaf.taf, 'DRAFT_AMEND'),
      ).toEqual({
        ...fakeDraftAmendmentTaf.taf,
        previousValidDateStart:
          fakeDraftAmendmentTaf.taf.previousValidDateStart,
        previousValidDateEnd: fakeDraftAmendmentTaf.taf.previousValidDateEnd,
        validDateStart: now,
        baseForecast: {
          ...fakeDraftAmendmentTaf.taf.baseForecast,
          valid: validObj,
        },
      });
    });
    it('should update previous dates if there are already previous dates for AMENDMENT', () => {
      const now = `${dateUtils.dateToString(
        dateUtils.utc(),
        dateUtils.DATE_FORMAT_YEAR,
      )}T14:00:00Z`;

      const validObj = {
        start: now,
        end: fakeAmendmentTaf.taf.baseForecast.valid.end,
      };
      jest.useFakeTimers().setSystemTime(new Date(now));
      expect(
        getAmendedCorrectedTafTimes(fakeAmendmentTaf.taf, 'AMEND'),
      ).toEqual({
        ...fakeAmendmentTaf.taf,
        previousValidDateStart: fakeAmendmentTaf.taf.validDateStart,
        previousValidDateEnd: fakeAmendmentTaf.taf.validDateEnd,
        validDateStart: now,
        baseForecast: {
          ...fakeAmendmentTaf.taf.baseForecast,
          valid: validObj,
        },
      });
    });
  });

  describe('createChangeGroup', () => {
    it('should create a changeGroup with all props', () => {
      expect(createChangeGroup({} as ChangeGroupFormData)).toEqual(
        emptyChangegroup,
      );
      const testChangeGroup = {
        weather: {
          weather1: 'test',
          weather2: 'test-2',
        },
        change: 'test-3',
        valid: 'test-4',
      };
      expect(createChangeGroup(testChangeGroup)).toEqual({
        ...emptyChangegroup,
        ...testChangeGroup,
        weather: {
          ...emptyChangegroup.weather,
          ...testChangeGroup.weather,
        },
      });
      const testChangeGroup2 = {
        weather: {
          weather1: 'test',
          weather2: 'test-2',
        },
        cloud: {
          cloud1: 'test-6',
        },
        change: 'test-3',
        valid: 'test-4',
      };
      expect(createChangeGroup(testChangeGroup2)).toEqual({
        ...emptyChangegroup,
        ...testChangeGroup,
        weather: {
          ...emptyChangegroup.weather,
          ...testChangeGroup.weather,
        },
        cloud: {
          ...emptyChangegroup.cloud,
          ...testChangeGroup2.cloud,
        },
      });
    });
  });

  describe('prepareTafValues', () => {
    it('should add up to 5 changegroups if addEmptyChangeGroup is set to true', () => {
      // Add 5 empty ones if no changeGroups present
      expect(fakeNewTaf.taf.changeGroups).toBeUndefined();
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT', true).changeGroups,
      ).toHaveLength(5);
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT', true).changeGroups![0],
      ).toEqual(emptyChangegroup);

      // Fill up to 5 when changegroups are already present
      expect(fakeDraftAmendmentTaf.taf.changeGroups).toHaveLength(1);
      const preparedTaf = prepareTafValues(
        fakeDraftAmendmentTaf.taf,
        'DRAFT',
        true,
      );
      expect(preparedTaf.changeGroups).toHaveLength(5);
      expect(preparedTaf.changeGroups![0].change).toEqual(
        fakeDraftAmendmentTaf.taf.changeGroups![0].change,
      );
      expect(preparedTaf.changeGroups![0].probability).toEqual(
        fakeDraftAmendmentTaf.taf.changeGroups![0].probability,
      );
      expect(preparedTaf.changeGroups![1]).toEqual(emptyChangegroup);

      expect(fakeExpiredTaf.taf.changeGroups).toHaveLength(2);
      const preparedTaf2Groups = prepareTafValues(
        fakeExpiredTaf.taf,
        'DRAFT',
        true,
      );
      expect(preparedTaf2Groups.changeGroups).toHaveLength(5);
      expect(preparedTaf2Groups.changeGroups![0].change).toEqual(
        fakeExpiredTaf.taf.changeGroups![0].change,
      );
      expect(preparedTaf2Groups.changeGroups![0].weather!.weather1).toEqual(
        fakeExpiredTaf.taf.changeGroups![0].weather!.weather1,
      );
      expect(preparedTaf2Groups.changeGroups![1].change).toEqual(
        fakeExpiredTaf.taf.changeGroups![1].change,
      );
      expect(preparedTaf2Groups.changeGroups![1].weather!.weather1).toEqual(
        fakeExpiredTaf.taf.changeGroups![1].weather!.weather1,
      );
      expect(preparedTaf2Groups.changeGroups![2]).toEqual(emptyChangegroup);

      // If already 5 or more, don't add any
      expect(fakeDraftTafWithSameDates.taf.changeGroups).toHaveLength(7);
      const preparedTaf5Groups = prepareTafValues(
        fakeDraftTafWithSameDates.taf,
        'DRAFT',
        true,
      );
      expect(preparedTaf5Groups.changeGroups).toHaveLength(7);
      expect(preparedTaf5Groups.changeGroups![0].change).toEqual(
        fakeDraftTafWithSameDates.taf.changeGroups![0].change,
      );
      expect(preparedTaf5Groups.changeGroups![0].weather!.weather1).toEqual(
        fakeDraftTafWithSameDates.taf.changeGroups![0].weather!.weather1,
      );
      expect(preparedTaf5Groups.changeGroups![1].change).toEqual(
        fakeDraftTafWithSameDates.taf.changeGroups![1].change,
      );
      expect(preparedTaf5Groups.changeGroups![1].weather!.weather1).toEqual(
        fakeDraftTafWithSameDates.taf.changeGroups![1].weather!.weather1,
      );
      expect(preparedTaf5Groups.changeGroups![5].change).toEqual(
        fakeDraftTafWithSameDates.taf.changeGroups![5].change,
      );
      expect(preparedTaf5Groups.changeGroups![5].weather!.weather1).toEqual(
        fakeDraftTafWithSameDates.taf.changeGroups![5].weather!.weather1,
      );
      expect(preparedTaf5Groups.changeGroups![6].change).toEqual(
        fakeDraftTafWithSameDates.taf.changeGroups![6].change,
      );
      expect(preparedTaf5Groups.changeGroups![6].weather!.weather1).toEqual(
        fakeDraftTafWithSameDates.taf.changeGroups![6].weather!.weather1,
      );
    });

    it('should add empty hidden values', () => {
      expect(prepareTafValues(fakeNewTaf.taf, 'DRAFT', true).issueDate).toEqual(
        '',
      );
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT', true).previousId,
      ).toEqual(null);
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT', true).previousValidDateStart,
      ).toEqual(null);
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT', true).previousValidDateEnd,
      ).toEqual(null);
    });

    it('should not add an empty changeGroup if addEmptyChangeGroup is false or not passed', () => {
      // If no changegroup in original TAF then changegroup should be empty
      expect(fakeNewTaf.taf.changeGroups).toBeUndefined();
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT', false).changeGroups,
      ).toHaveLength(0);
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT').changeGroups,
      ).toHaveLength(0);

      // If changegroups already present they should not be affected
      expect(fakeExpiredTaf.taf.changeGroups).toHaveLength(2);
      expect(
        prepareTafValues(fakeExpiredTaf.taf, 'DRAFT', false).changeGroups,
      ).toHaveLength(2);
      expect(
        prepareTafValues(fakeExpiredTaf.taf, 'DRAFT').changeGroups,
      ).toHaveLength(2);
    });

    it('should set IS_DRAFT to default true', () => {
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT', true).IS_DRAFT,
      ).toBeTruthy();
    });

    it('should add empty values for baseForecast keys', () => {
      const testTaf = {
        ...fakeNewTaf.taf,
        baseForecast: {},
      } as Taf;

      expect(prepareTafValues(testTaf, 'DRAFT', false).baseForecast).toEqual(
        emptyBaseForecast,
      );

      const testTaf2 = {
        ...fakeNewTaf.taf,
        baseForecast: {
          valid: {
            start: '2020-12-07T06:00:00Z',
            end: '2020-12-08T12:00:00Z',
          },
          wind: { direction: 50, speed: 5, unit: 'KT' },
          visibility: { range: 500, unit: 'M' },
        },
      } as Taf;

      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.visibility,
      ).toEqual(formatFormValues(testTaf2.baseForecast).visibility);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.valid,
      ).toEqual(formatFormValues(testTaf2.baseForecast).valid);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.wind,
      ).toEqual(formatFormValues(testTaf2.baseForecast).wind);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.weather!
          .weather1,
      ).toEqual(emptyBaseForecast.weather.weather1);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.weather!
          .weather2,
      ).toEqual(emptyBaseForecast.weather.weather2);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.weather!
          .weather3,
      ).toEqual(emptyBaseForecast.weather.weather3);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.cloud!.cloud1,
      ).toEqual(emptyBaseForecast.cloud.cloud1);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.cloud!.cloud2,
      ).toEqual(emptyBaseForecast.cloud.cloud2);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.cloud!.cloud3,
      ).toEqual(emptyBaseForecast.cloud.cloud3);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.cloud!.cloud4,
      ).toEqual(emptyBaseForecast.cloud.cloud4);

      const testTaf3 = {
        ...fakeNewTaf.taf,
        baseForecast: {
          valid: {
            start: '2020-12-07T06:00:00Z',
            end: '2020-12-08T12:00:00Z',
          },
          wind: { direction: 50, speed: 5, unit: 'KT' },
          visibility: { range: 500, unit: 'M' },
          cloud: {
            cloud1: {
              coverage: 'FEW',
              height: 1,
            },
          },
          weather: {
            weather1: 'RA',
          },
        },
      } as Taf;

      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.visibility,
      ).toEqual(formatFormValues(testTaf3.baseForecast).visibility);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.valid,
      ).toEqual(formatFormValues(testTaf3.baseForecast).valid);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.wind,
      ).toEqual(formatFormValues(testTaf3.baseForecast).wind);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.weather!
          .weather1,
      ).toEqual(formatFormValues(testTaf3.baseForecast).weather!.weather1);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.weather!
          .weather2,
      ).toEqual(emptyBaseForecast.weather.weather2);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.weather!
          .weather3,
      ).toEqual(emptyBaseForecast.weather.weather3);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.cloud!.cloud1,
      ).toEqual(formatFormValues(testTaf3.baseForecast).cloud!.cloud1);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.cloud!.cloud2,
      ).toEqual(emptyBaseForecast.cloud.cloud2);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.cloud!.cloud3,
      ).toEqual(emptyBaseForecast.cloud.cloud3);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.cloud!.cloud4,
      ).toEqual(emptyBaseForecast.cloud.cloud4);
    });
  });
  describe('getIsFormDefaultDisabled', () => {
    it('should return true for a published or expired taf', () => {
      expect(getIsFormDefaultDisabled('AMENDED')).toBeTruthy();
      expect(getIsFormDefaultDisabled('CANCELLED')).toBeTruthy();
      expect(getIsFormDefaultDisabled('CORRECTED')).toBeTruthy();
      expect(getIsFormDefaultDisabled('EXPIRED')).toBeTruthy();
      expect(getIsFormDefaultDisabled('PUBLISHED')).toBeTruthy();
    });

    it('should return false for a new or draft taf', () => {
      expect(getIsFormDefaultDisabled('NEW')).toBeFalsy();
      expect(getIsFormDefaultDisabled('DRAFT')).toBeFalsy();
      expect(getIsFormDefaultDisabled('DRAFT_AMENDED')).toBeFalsy();
      expect(getIsFormDefaultDisabled('DRAFT_CORRECTED')).toBeFalsy();
    });
  });
  describe('clearTafFormData', () => {
    const emptyChangeGroups = [] as (typeof emptyChangegroup)[];
    Array.from(Array(MIN_NUMBER_CHANGE_GROUP)).map(() =>
      emptyChangeGroups.push(emptyChangegroup),
    );

    it('should clear out all content and only leave validstart/end intact', () => {
      const tafFormValues = prepareTafValues(
        fakeDraftFixedTaf.taf,
        'DRAFT',
        true,
      );
      expect(clearTafFormData(tafFormValues)).toEqual({
        ...tafFormValues,
        baseForecast: {
          valid: formatValidToString(fakeDraftFixedTaf.taf.baseForecast.valid),
          ...emptyBaseForecast,
        },
        // Five empty change groups should be added
        changeGroups: emptyChangeGroups,
      });
    });
    it('should clear out all content and keep adjusted valid start for amendments/corrections', () => {
      // Amendment
      const adjustedValidTaf = getAmendedCorrectedTafTimes(
        fakeDraftAmendmentTaf.taf,
        'DRAFT_AMEND',
      );
      const tafFormValuesAmend = prepareTafValues(
        fakeDraftAmendmentTaf.taf,
        'DRAFT_AMEND',
        true,
      );
      expect(clearTafFormData(tafFormValuesAmend)).toEqual({
        ...tafFormValuesAmend,
        baseForecast: {
          valid: formatValidToString(adjustedValidTaf.baseForecast.valid),
          ...emptyBaseForecast,
        },
        changeGroups: emptyChangeGroups,
      });
      // Correction
      const adjustedValidTafDraft = getAmendedCorrectedTafTimes(
        fakeDraftCorrectedTaf.taf,
        'DRAFT_CORRECT',
      );
      const tafFormValuesCorrect = prepareTafValues(
        fakeDraftCorrectedTaf.taf,
        'DRAFT_CORRECT',
        true,
      );
      expect(clearTafFormData(tafFormValuesCorrect)).toEqual({
        ...tafFormValuesCorrect,
        baseForecast: {
          valid: formatValidToString(adjustedValidTafDraft.baseForecast.valid),
          ...emptyBaseForecast,
        },
        changeGroups: emptyChangeGroups,
      });
    });
  });

  describe('getFormRowPosition', () => {
    it('should return last form position', () => {
      const testElement = document.createElement('div');

      const test1 = {
        left: 0,
        top: 0,
        height: 0,
      };
      // mock getBoundingClientRect
      Object.assign(testElement, {
        getBoundingClientRect: () => test1,
      });

      expect(getFormRowPosition(testElement)).toEqual({
        top: test1.top + test1.height,
        left: test1.left,
      });

      const test2 = {
        left: 100,
        top: 0,
        height: 100,
      };
      // mock getBoundingClientRect
      Object.assign(testElement, {
        getBoundingClientRect: () => test2,
      });

      expect(getFormRowPosition(testElement)).toEqual({
        top: test2.top + test2.height,
        left: test2.left,
      });

      const test3 = {
        left: 100,
        top: 100,
        height: 100,
      };
      // mock getBoundingClientRect
      Object.assign(testElement, {
        getBoundingClientRect: () => test3,
      });

      expect(getFormRowPosition(testElement)).toEqual({
        top: test3.top + test3.height,
        left: test3.left,
      });
    });
  });
});
