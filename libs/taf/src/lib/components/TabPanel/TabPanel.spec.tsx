/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import TabPanel from './TabPanel';

describe('components/TabPanel/TabPanel', () => {
  const TestDiv = (): React.ReactElement => <div id="test">test</div>;

  it('should display a TabPanel', () => {
    render(
      <TabPanel index={0} value={0}>
        <TestDiv />
      </TabPanel>,
    );

    expect(screen.getByText('test')).toBeTruthy();
  });

  it('should hide a TabPanel when value is not same as index', () => {
    render(
      <TabPanel index={0} value={1}>
        <TestDiv />
      </TabPanel>,
    );

    expect(screen.queryByText('test')).toBeFalsy();
  });
});
