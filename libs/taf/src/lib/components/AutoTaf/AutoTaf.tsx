/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Box, Card, Typography } from '@mui/material';
import React from 'react';
import TafHeader from '../TafHeader/TafHeader';
import { useTafTranslation } from '../../utils/i18n';
import { TafFromBackend } from '../../types';
import TacContainer, {
  TacHighLight,
} from '../TafLayout/TacOverview/TacContainer';
import { getAutoTafLocations, scrollToPosition } from '../TafLayout/utils';
import TacContainerLayout from '../TafLayout/TacOverview/TacContainerLayout';

const Message: React.FC<{ text: string }> = ({ text }) => (
  <Typography
    variant="body2"
    sx={{
      fontSize: '12px',
      fontWeight: 'normal',
      fontStyle: 'normal',
      lineHeight: 0.96,
      color: 'geowebColors.textInputField.label.rgba',
    }}
  >
    {text}
  </Typography>
);

interface AutoTafProps {
  tafList: TafFromBackend[];
  isFormDisabled?: boolean;
  activeTaf?: TafFromBackend;
}

export const AutoTaf: React.FC<AutoTafProps> = ({
  tafList,
  isFormDisabled = false,
  activeTaf,
}: AutoTafProps) => {
  const { t } = useTafTranslation();
  const scrollRef = React.useRef<HTMLDivElement>(null);

  React.useEffect(() => {
    if (activeTaf) {
      scrollToPosition(scrollRef, activeTaf.taf.location);
    }
  }, [activeTaf]);

  const renderAutoTaf = (tafList: TafFromBackend[]): React.ReactNode => {
    const autoTafLocations = getAutoTafLocations();
    if (!autoTafLocations) {
      return <Message text={t('autotaf-no-config')} />;
    }
    if (tafList.length === 0) {
      return <Message text={t('autotaf-no-data')} />;
    }

    return autoTafLocations.map((tafLocation) => {
      const foundLocation = tafList.find(
        (tafFromBackend) => tafFromBackend.taf.location === tafLocation,
      );

      if (foundLocation) {
        return (
          <TacContainer
            key={tafLocation}
            taf={foundLocation.taf}
            isFormDisabled={isFormDisabled}
            isActive={foundLocation.taf.location === activeTaf?.taf.location}
          />
        );
      }

      return (
        <TacContainerLayout
          key={tafLocation}
          location={tafLocation}
          isActive={tafLocation === activeTaf?.taf.location}
        >
          <TacHighLight isActive={tafLocation === activeTaf?.taf.location}>
            {t('autotaf-no-location')}
          </TacHighLight>
        </TacContainerLayout>
      );
    });
  };

  return (
    <Card
      variant="outlined"
      sx={{
        my: 1,
        display: 'flex',
        flexDirection: 'column',
        maxHeight: '252px',
        overflow: {
          xs: 'hidden',
          sm: 'initial',
        },
      }}
      data-testid="autotaf"
    >
      <TafHeader title={t('autotaf')} sx={{ textTransform: 'uppercase' }} />
      <Box
        ref={scrollRef}
        sx={{
          padding: { xs: '12px', sm: '6px', md: '16px', lg: '24px' },
          overflow: 'auto',
        }}
      >
        {renderAutoTaf(tafList)}
      </Box>
    </Card>
  );
};
