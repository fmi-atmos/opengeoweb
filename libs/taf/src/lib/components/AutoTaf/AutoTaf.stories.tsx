/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import type { Meta, StoryObj } from '@storybook/react';
import { StorybookDocsWrapper } from '@opengeoweb/shared';
import { darkTheme } from '@opengeoweb/theme';
import { TafThemeApiProvider } from '../Providers';
import { fakeTafList } from '../../utils/mockdata/fakeTafList';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { AutoTaf } from './AutoTaf';

const meta: Meta<typeof AutoTaf> = {
  title: 'components/AutoTaf',
  component: AutoTaf,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the AutoTaf',
      },
    },
  },
};
export default meta;
type Story = StoryObj<typeof AutoTaf>;

const fakeAutoTafList = fakeTafList.filter(({ taf }) => taf.status === 'AUTO');

export const Component: Story = {
  args: {
    tafList: fakeAutoTafList,
    isFormDisabled: false,
  },
  render: (props) => (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <AutoTaf {...props} />
    </TafThemeApiProvider>
  ),
};

export const ComponentDark: Story = {
  ...Component,
  render: (props) => (
    <TafThemeApiProvider createApiFunc={createFakeApi} theme={darkTheme}>
      <StorybookDocsWrapper isDark>
        <AutoTaf {...props} />
      </StorybookDocsWrapper>
    </TafThemeApiProvider>
  ),
};

export const ComponentActiveTaf: Story = {
  ...Component,
  args: {
    tafList: fakeAutoTafList,
    isFormDisabled: false,
    activeTaf: fakeAutoTafList[2],
  },
};
