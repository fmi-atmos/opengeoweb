/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import i18n from 'i18next';
import {
  UseTranslationResponse,
  initReactI18next,
  useTranslation,
} from 'react-i18next';
import { SHARED_NAMESPACE, sharedTranslations } from '@opengeoweb/shared';
import { CORE_NAMESPACE, coreTranslations } from '@opengeoweb/core';
import timeseriesTranslations from '../../../locales/timeseries.json';

export const TIMESERIES_NAMESPACE = 'timeseries';

export const initTimeseriesI18n = (): void => {
  void i18n.use(initReactI18next).init({
    lng: 'en',
    ns: TIMESERIES_NAMESPACE,
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        [TIMESERIES_NAMESPACE]: timeseriesTranslations.en,
        [SHARED_NAMESPACE]: sharedTranslations.en,
        [CORE_NAMESPACE]: coreTranslations.en,
      },
      fi: {
        [TIMESERIES_NAMESPACE]: timeseriesTranslations.fi,
        [SHARED_NAMESPACE]: sharedTranslations.fi,
        [CORE_NAMESPACE]: coreTranslations.fi,
      },
      no: {
        [TIMESERIES_NAMESPACE]: timeseriesTranslations.no,
        [SHARED_NAMESPACE]: sharedTranslations.no,
        [CORE_NAMESPACE]: coreTranslations.no,
      },
      nl: {
        [TIMESERIES_NAMESPACE]: timeseriesTranslations.nl,
        [SHARED_NAMESPACE]: sharedTranslations.nl,
        [CORE_NAMESPACE]: coreTranslations.nl,
      },
    },
  });
};

const ns = [TIMESERIES_NAMESPACE, SHARED_NAMESPACE, CORE_NAMESPACE];

export const translateKeyOutsideComponents = (
  key: string,
  params: Record<string, string | number> | undefined = undefined,
): string => i18n.t(key, { ns, ...params });

export const useTimeseriesTranslation = (): UseTranslationResponse<
  typeof TIMESERIES_NAMESPACE,
  typeof i18n
> => useTranslation(TIMESERIES_NAMESPACE);
