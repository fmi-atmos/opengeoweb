/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as utils from '@opengeoweb/api';
import { createFakeApiInstance } from '@opengeoweb/api';
import { createApi } from './api';

describe('src/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  const fakeConfigPath = 'fakeTimeSeriesPresetLocations.json';

  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi();
      expect(api.getTimeSeriesConfiguration).toBeTruthy();
    });

    it('should call with the right params for getTimeSeriesConfiguration', async () => {
      jest
        .spyOn(utils, 'createNonAuthApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      await api.getTimeSeriesConfiguration!(fakeConfigPath);
      expect(spy).toHaveBeenCalledWith(`/assets/${fakeConfigPath}`);
    });
  });
});
