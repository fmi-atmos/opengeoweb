/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { TimeSeriesService } from '@opengeoweb/shared';

export const emptyTimeSeriesPreset: TimeSeriesService[] = [
  {
    name: 'FMI',
    url: 'https://opendata.fmi.fi/edr',
    id: 'fmi',
    description: 'FMI EDR',
    type: 'EDR',
    scope: 'system',
  },
  {
    name: 'KNMI OBS',
    url: 'https://gw-edr.pub.knmi.cloud/edr',
    id: 'knmi',
    description: 'KNMI EDR',
    type: 'EDR',
    scope: 'system',
  },
  {
    name: 'KNMI Geoservices',
    url: 'https://geoservices.knmi.nl/edr',
    id: 'knmi_geoservices_edr',
    description: 'KNMI EDR on geoservices with UWCW HA43 DINI',
    type: 'EDR',
    scope: 'system',
  },
  {
    name: 'MET Norway',
    url: 'https://interpol.met.no',
    id: 'METNorway',
    description: 'MET Norway EDR',
    type: 'EDR',
    scope: 'system',
  },
];
