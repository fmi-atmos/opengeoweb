/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { ThemeTypes, useThemeContext } from '@opengeoweb/theme';
import { FormControlLabel, Switch } from '@mui/material';

export const ToggleThemeButton: React.FC = () => {
  const { namedTheme, setThemeName } = useThemeContext();
  const toggleTheme = (): void => {
    setThemeName?.(
      namedTheme?.name === ThemeTypes.LIGHT_THEME
        ? ThemeTypes.DARK_THEME
        : ThemeTypes.LIGHT_THEME,
    );
  };
  const isDark = namedTheme?.theme.palette.mode === 'dark';

  return (
    <FormControlLabel
      control={<Switch checked={isDark} onChange={toggleTheme} />}
      label={isDark ? 'Dark theme' : 'Light theme'}
    />
  );
};
