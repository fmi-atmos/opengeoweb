/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import { snackbarActions } from '@opengeoweb/snackbar';
import { TimeSeriesModuleState } from '../../store/types';
import { mockTimeSeriesServices as services } from '../TimeSeries/mockTimeSeriesServices';
import { ServiceOptionsButtonConnect } from './ServiceOptionsConnect';
import {
  TimeseriesI18nProvider,
  TimeSeriesThemeStoreProvider,
} from '../Providers';
import { timeSeriesActions } from '../../store';
import { createMockStore } from '../../store/store';

describe('components/TimeSeriesSelect/ServiceOptionsConnect', () => {
  it('should show snackbar on service delete', async () => {
    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: 'mockMapId',
          plots: [{ plotId: 'plotId1', title: 'title' }],
          parameters: [],
        },
        services: [
          ...services,
          {
            id: 'test',
            description: 'test',
            name: 'test',
            type: 'EDR',
            scope: 'user',
            url: 'testurl',
          },
        ],
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {
                [services[0].id]: {
                  serviceId: services[0].id,
                  type: services[0].type,
                  serviceUrl: services[0].url,
                  serviceName: services[0].name,
                  enabled: true,
                  scope: 'system',
                },
              },
              ids: [services[0].id],
            },
            allServiceFilterChipsEnabled: true,
          },
          servicePopup: {
            isOpen: false,
            variant: 'add',
          },
        },
      },
    };

    const mockOpenSnackbar = jest.spyOn(snackbarActions, 'openSnackbar');
    const mockRemoveService = jest.spyOn(timeSeriesActions, 'removeService');
    const store = createMockStore(timeSeriesStore);

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeseriesI18nProvider>
          <ServiceOptionsButtonConnect />
        </TimeseriesI18nProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    const toggleOpen = await screen.findByTestId('toggleMenuButton');
    fireEvent.click(toggleOpen);

    const deleteButton = await screen.findByTestId('remove-button-test');
    fireEvent.click(deleteButton);

    expect(mockOpenSnackbar).toHaveBeenCalledTimes(1);
    expect(mockRemoveService).toHaveBeenCalledTimes(1);
    expect(mockRemoveService).toHaveBeenCalledWith({ id: 'test' });
  });
});
