/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';

import { darkTheme } from '@opengeoweb/theme';
import type { Meta, StoryObj } from '@storybook/react';

import { TimeSeriesService } from '@opengeoweb/shared';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { Parameter } from '../TimeSeries/types';
import { ServiceOptionsDialog } from './ServiceOptionsDialog';
import { createMockStore } from '../../store/store';

const meta: Meta<typeof ServiceOptionsDialog> = {
  title: 'components/TimeSeriesSelect/ServiceOptionsDialog',
  component: ServiceOptionsDialog,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user ServiceOptionsDialog',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof ServiceOptionsDialog>;

const defaultServices: TimeSeriesService[] = [
  {
    id: 'norway_EDR',
    type: 'EDR',
    url: 'https://interpol.met.no',
    description: 'Norwegian EDR service',
    name: 'MetNorway',
    scope: 'system',
  },
  {
    id: 'fmi_EDR',
    type: 'EDR',
    url: 'https://opendata.fmi.fi/edr',
    description: 'Finnish EDR service',
    name: 'FMI',
    scope: 'system',
  },
  {
    id: 'ogc_fake_api',
    type: 'OGC',
    url: 'someURL',
    description: 'Fake OGC service',
    name: 'Fake OGC',
    scope: 'system',
  },
  {
    id: 'personalService',
    type: 'EDR',
    url: 'someURL/personal',
    description: 'Personal added service',
    name: 'Personal service',
    scope: 'user',
  },
];

const defaultParametersInUse: Parameter[] = [
  {
    plotId: 'plot1',
    unit: '°C',
    propertyName: 'Temperature',
    plotType: 'line',
    serviceId: defaultServices[0].id,
    id: 'param1',
    collectionId: 'ecmwf',
  },
  {
    plotId: 'plot1',
    unit: '°C',
    propertyName: 'DewPoint',
    plotType: 'line',
    serviceId: defaultServices[0].id,
    collectionId: 'ecmwf',
  },
  {
    plotId: 'plotId2',
    unit: '%',
    propertyName: 'Humidity',
    plotType: 'line',
    serviceId: defaultServices[0].id,
    collectionId: 'ecmwf',
  },
];

const ServiceOptionsDialogDemo: React.FC<{
  services?: TimeSeriesService[];
  parametersInUse?: Parameter[];
}> = ({
  services = defaultServices,
  parametersInUse = defaultParametersInUse,
}) => {
  return (
    <div style={{ backgroundColor: 'grey', padding: '16px', width: '400px' }}>
      <ServiceOptionsDialog
        services={services}
        parametersInUse={parametersInUse}
        openNewServicePopup={() => {}}
        openServicePopup={() => {}}
        removeService={() => {}}
      />
    </div>
  );
};

export const Component: Story = {
  args: {
    services: defaultServices,
    parametersInUse: defaultParametersInUse,
    openNewServicePopup: () => {},
    openServicePopup: () => {},
    removeService: () => {},
  },
  render: (props) => (
    <TimeSeriesThemeStoreProvider store={createMockStore()}>
      <ServiceOptionsDialog {...props} />
    </TimeSeriesThemeStoreProvider>
  ),
};

export const ServiceOptionsDialogDemoLightTheme: Story = {
  render: () => (
    <TimeSeriesThemeStoreProvider store={createMockStore()}>
      <ServiceOptionsDialogDemo />
    </TimeSeriesThemeStoreProvider>
  ),
  tags: ['snapshot'],
};
ServiceOptionsDialogDemoLightTheme.storyName =
  'ServiceOptionsDialog light theme';

export const ServiceOptionsDialogDemoDarkTheme: Story = {
  render: () => (
    <TimeSeriesThemeStoreProvider store={createMockStore()} theme={darkTheme}>
      <ServiceOptionsDialogDemo />
    </TimeSeriesThemeStoreProvider>
  ),
  tags: ['snapshot', 'dark'],
};

ServiceOptionsDialogDemoDarkTheme.storyName = 'ServiceOptionsDialog dark theme';
