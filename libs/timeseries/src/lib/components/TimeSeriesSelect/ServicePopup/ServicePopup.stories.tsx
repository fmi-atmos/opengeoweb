/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { ThemeWrapper, darkTheme, lightTheme } from '@opengeoweb/theme';

import { Theme } from '@mui/material';
import { Meta } from '@storybook/react';
import { ServiceInterface } from '@opengeoweb/shared';
import { ServicePopup } from './ServicePopup';
import { TimeSeriesTypes } from '../../../store';

export default {
  title: 'components/TimeSeriesSelect/ServicePopup',
  component: ServicePopup,
  tags: ['!autodocs'],
} as Meta<typeof ServicePopup>;

const zeplinLight = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62c695a1ed42f01adee34e63/version/632b1738c788c83cc9d5da8d',
    },
  ],
};
const zeplinDark = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d7b06898ff1c11dbcc0d0a/version/632b1ae593c99c3b8f379504',
    },
  ],
};

const serviceUrl = 'https://opendata.fmi.fi/edr';
const serviceName = 'EDR FMI';
const serviceDescription = 'EDR endpoint of FMI';
const serviceType: ServiceInterface = 'EDR';

const ServicePopupTemplate: React.FC<{
  theme: Theme;
  variant: TimeSeriesTypes.PopupVariant;
}> = ({ theme, variant }) => {
  const isShowPopup = variant === 'show';
  return (
    <ThemeWrapper theme={theme}>
      <ServicePopup
        serviceUrl={isShowPopup ? serviceUrl : undefined}
        serviceName={isShowPopup ? serviceName : undefined}
        serviceDescription={isShowPopup ? serviceDescription : undefined}
        serviceType={isShowPopup ? serviceType : undefined}
        servicePopupVariant={variant}
        hideBackdrop
        isOpen={true}
        shouldDisableAutoFocus={true} // Autofocus will be deliberately disabled here to prevent cursor blinking affect snapshots
        serviceSetService={(): void => {}}
        services={[]}
      />
    </ThemeWrapper>
  );
};

export const ServicePopupAddLight = (): React.ReactElement => (
  <ServicePopupTemplate theme={lightTheme} variant="add" />
);

ServicePopupAddLight.storyName = 'Service Popup Add Light Theme (takeSnapshot)';
ServicePopupAddLight.parameters = zeplinLight;

export const ServicePopupAddDark = (): React.ReactElement => (
  <ServicePopupTemplate theme={darkTheme} variant="add" />
);

ServicePopupAddDark.storyName = 'Service Popup Add Dark Theme (takeSnapshot)';
ServicePopupAddDark.parameters = zeplinDark;

export const ServicePopupShowLight = (): React.ReactElement => (
  <ServicePopupTemplate theme={lightTheme} variant="show" />
);

ServicePopupShowLight.storyName =
  'Service Popup Show Light Theme (takeSnapshot)';
ServicePopupShowLight.parameters = zeplinLight;

export const ServicePopupShowDark = (): React.ReactElement => (
  <ServicePopupTemplate theme={darkTheme} variant="show" />
);

ServicePopupShowDark.storyName = 'Service Popup Show Dark Theme (takeSnapshot)';
ServicePopupShowDark.parameters = zeplinDark;

export const ServicePopupWithInvalidUrl = (): React.ReactElement => (
  <ThemeWrapper>
    <ServicePopup
      serviceUrl="http://www.triggeringerror"
      serviceName=""
      servicePopupVariant="add"
      hideBackdrop
      isOpen={true}
      shouldDisableAutoFocus={true} // Autofocus will be deliberately disabled here to prevent cursor blinking affect snapshots
      serviceSetService={(): void => {}}
      services={[]}
    />
  </ThemeWrapper>
);
