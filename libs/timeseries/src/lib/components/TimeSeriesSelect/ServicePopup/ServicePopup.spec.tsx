/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { ThemeWrapper } from '@opengeoweb/theme';
import { ServiceInterface, TimeSeriesService } from '@opengeoweb/shared';
import { ServicePopup, ServicePopupProps } from './ServicePopup';

import * as utils from './utils';
import { TimeseriesI18nProvider } from '../../Providers';

const ADD_HEADING = 'Add a new TS service';
const EDIT_HEADING = 'Edit Service';
const SAVE_HEADING = 'Save service';
const COPY_URL_MESSAGE = 'Service URL copied to clipboard';

const VALIDATIONS_NAME_EXISTING =
  'This name already exists. Choose another name.';
const VALIDATIONS_SERVICE_EXISTING = 'URL already exists.';
const VALIDATIONS_SERVICE_VALID_URL = 'Not a valid URL.';
const VALIDATIONS_REQUEST_FAILED = 'Download failed. Check the URL.';
const VALIDATIONS_FIELD_REQUIRED = 'This field is required';

const getSuccesMessage = (serviceName: string): string =>
  `Service "${serviceName}" has been saved succesfully`;

// eslint-disable-next-line no-unused-vars
const mockFetchEdrCollections = jest.fn((x: string) =>
  Promise.resolve({ title: '', description: '' }),
);

jest.mock('../../../utils/edrUtils', () => ({
  validateServiceUrl: (
    x: string,
  ): Promise<{ title: string; description: string }> =>
    mockFetchEdrCollections(x),
}));

const ThemeI18nWrapper: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => (
  <TimeseriesI18nProvider>
    <ThemeWrapper>{children}</ThemeWrapper>
  </TimeseriesI18nProvider>
);

const serviceUrlLabel = /this service refers to this url/i;
const serviceNameLabel = /service name/i;
const serviceDescriptionLabel = /description/i;
const serviceTypeLabel = /type/i;

describe('src/lib/components/TimeSeries/ServicePopup/ServicePopup', () => {
  const mockServices: TimeSeriesService[] = [
    {
      id: 'edr_finland',
      name: 'EDR Finland',
      url: 'https://opendata.fmi.fi/edr',
      description: 'Finnish EDR endpoint',
      type: 'EDR',
      scope: 'user',
    },
  ];

  it('should render correctly', async () => {
    jest.spyOn(console, 'warn').mockImplementation();
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );
    expect(screen.getByTestId('servicePopup')).toBeInTheDocument();
  });

  it('should render the add title when using the add variant', () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );
    expect(screen.getByRole('heading').textContent).toBe(ADD_HEADING);

    const input = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    expect(input.getAttribute('disabled')).toBeFalsy();
  });

  it('should render correct description', () => {
    const testDescription = 'testing description';
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="edit"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetService={jest.fn()}
          services={mockServices}
          serviceDescription={testDescription}
        />
      </ThemeI18nWrapper>,
    );

    expect(
      screen.getByRole('textbox', {
        name: serviceDescriptionLabel,
      }).innerHTML,
    ).toEqual(testDescription);
  });

  it('should render the edit title when using the edit variant', () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="edit"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );
    expect(screen.getByRole('heading').textContent).toBe(EDIT_HEADING);

    expect(
      screen
        .getByRole('textbox', {
          name: serviceUrlLabel,
        })
        .getAttribute('disabled'),
    ).toBeDefined();
  });

  it('should render the correct values for edit variant', () => {
    const props = {
      serviceUrl: 'https://opendata.fmi.fi/edr',
      serviceName: 'EDR Finland',
      serviceDescription: 'FMI EDR endpoint',
      serviceType: 'EDR' as ServiceInterface,
    };

    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="edit"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetService={jest.fn()}
          services={mockServices}
          {...props}
        />
      </ThemeI18nWrapper>,
    );
    expect(
      screen
        .getByRole('textbox', {
          name: serviceUrlLabel,
        })
        .getAttribute('value'),
    ).toEqual(props.serviceUrl);
    expect(
      screen
        .getByRole('textbox', {
          name: serviceNameLabel,
        })
        .getAttribute('value'),
    ).toEqual(props.serviceName);
    expect(
      screen.getByRole('textbox', {
        name: serviceDescriptionLabel,
      }).innerHTML,
    ).toEqual(props.serviceDescription);
    expect(
      screen.getByRole('combobox', {
        name: serviceTypeLabel,
      }).innerHTML,
    ).toEqual(props.serviceType);

    const button = screen.getByTestId('saveServiceButton');
    expect(button.getAttribute('disabled')).toBeFalsy();
  });

  it('should render the save title when using the save variant', () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );
    expect(screen.getByRole('heading').textContent).toBe(SAVE_HEADING);
  });

  it('should call the function that closes the popup on click', () => {
    const closePopup = jest.fn();
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={closePopup}
          isOpen={true}
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );

    fireEvent.click(screen.getByRole('button', { name: /close/i }));
    expect(closePopup).toHaveBeenCalledTimes(1);

    fireEvent.click(screen.getByRole('button', { name: /cancel/i }));
    expect(closePopup).toHaveBeenCalledTimes(2);
  });

  it('should not show dialog when isOpen = false', () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={jest.fn()}
          isOpen={false}
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );

    expect(screen.queryByRole('dialog')).toBeFalsy();
  });

  it('should prefill the url field when activeService has serviceUrl', async () => {
    const testUrl = 'https://google.com';
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          services={mockServices}
          isOpen
          serviceUrl={testUrl}
          serviceSetService={jest.fn()}
        />
      </ThemeI18nWrapper>,
    );

    await waitFor(() => {
      const urlField = screen.getByRole('textbox', {
        name: serviceUrlLabel,
      }) as HTMLInputElement;
      expect(urlField.value).toBe(testUrl);
    });
  });

  it('should disable save button if url aready exists in store', async () => {
    const testUrl = 'https://opendata.fmi.fi/edr';

    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceUrl={testUrl}
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );

    const serviceUrlTextField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlTextField, { target: { value: testUrl } });
    await waitFor(() => {
      const saveButton = screen.getByTestId(
        'saveServiceButton',
      ) as HTMLButtonElement;
      expect(saveButton.disabled).toBe(true);
    });
  });

  it('should disable save button if service name aready exists in store', async () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );

    const serviceNameTextField = screen.getByRole('textbox', {
      name: /service name/i,
    });
    fireEvent.change(serviceNameTextField, {
      target: { value: 'EDR Finland' },
    });

    await waitFor(() => {
      const saveButton = screen.getByTestId(
        'saveServiceButton',
      ) as HTMLButtonElement;
      expect(saveButton.disabled).toBe(true);
    });
  });

  it('should show error message, if service URL is already in store', async () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlField, {
      target: { value: 'https://opendata.fmi.fi/edr' },
    });

    const errorText = await screen.findByText(VALIDATIONS_SERVICE_EXISTING);
    expect(errorText).toBeTruthy();
  });

  it('should show error message, if service name is already in store', async () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: /service name/i,
    });

    fireEvent.change(serviceNameField, {
      target: { value: 'EDR Finland' },
    });

    expect(
      await screen.findByText(VALIDATIONS_NAME_EXISTING),
    ).toBeInTheDocument();
  });

  it('should show error message, if service url is invalid', async () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlField, {
      target: { value: 'asdasdasds' },
    });

    const errorText = await screen.findByText(VALIDATIONS_SERVICE_VALID_URL);
    expect(errorText).toBeTruthy();
  });

  it('should show error message, if download fails to start', async () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );

    const serviceNameTextField = screen.getByRole('textbox', {
      name: /service name/i,
    });
    fireEvent.change(serviceNameTextField, {
      target: { value: 'New service' },
    });

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });
    fireEvent.change(serviceUrlField, {
      target: { value: 'https://asdasd' },
    });
    fireEvent.blur(serviceUrlField, {
      target: { value: 'https://asdasd' },
    });

    const saveButton = screen.getByTestId(
      'saveServiceButton',
    ) as HTMLButtonElement;

    fireEvent.click(saveButton);

    const errorText = await screen.findByText(VALIDATIONS_REQUEST_FAILED);
    expect(errorText).toBeTruthy();
  });

  it('should hide buttons and disable input if show mode', async () => {
    const user = userEvent.setup();

    Object.defineProperty(navigator, 'clipboard', {
      value: {
        writeText: () => {},
      },
      writable: true,
      configurable: true,
    });
    jest.spyOn(navigator.clipboard, 'writeText');

    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="show"
          isOpen
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );

    expect(
      screen.getByRole('heading', { name: /service/i }),
    ).toBeInTheDocument();

    expect(
      screen.getAllByRole('button', {
        name: /close/i,
      }),
    ).toHaveLength(2);

    expect(
      screen.queryByRole('button', {
        name: /save/i,
      }),
    ).not.toBeInTheDocument();

    expect(
      screen.getByRole('textbox', { name: serviceNameLabel }),
    ).toHaveAttribute('readonly');
    expect(
      screen.getByRole('textbox', { name: serviceUrlLabel }),
    ).toHaveAttribute('readonly');
    expect(
      screen.getByRole('textbox', { name: serviceDescriptionLabel }),
    ).toHaveAttribute('readonly');

    const copyButton = screen.getByRole('button', { name: /copy url/i });
    await user.click(copyButton);
    expect(navigator.clipboard.writeText).toHaveBeenCalled();
  });

  it('should show error message on focus if service name is invalid', async () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetService={jest.fn()}
          services={mockServices}
          serviceUrl="https://opendata.fmi.fi/edr"
        />
      </ThemeI18nWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    fireEvent.focus(serviceNameField);

    expect(
      await screen.findByText(VALIDATIONS_SERVICE_EXISTING),
    ).toBeInTheDocument();
  });

  it('should NOT show error message on focus if service name is blank', async () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetService={jest.fn()}
          services={mockServices}
          serviceUrl=""
        />
      </ThemeI18nWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    fireEvent.focus(serviceNameField);
    expect(
      screen.queryByText(VALIDATIONS_FIELD_REQUIRED),
    ).not.toBeInTheDocument();
  });

  it('should show error message, if download fails to start on blur', async () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );

    const urlInput = screen.getByRole('textbox', { name: serviceUrlLabel });
    fireEvent.change(urlInput, { target: { value: 'not a valid url' } });
    fireEvent.blur(urlInput);

    const errorText = await screen.findByText(VALIDATIONS_SERVICE_VALID_URL);
    expect(errorText).toBeTruthy();
  });

  it('should fire serviceSetService action with correct layer information when adding a service', async () => {
    const mockUrl = 'https://mockUrlWithChildren.nl';
    const props: ServicePopupProps = {
      servicePopupVariant: 'add',
      closePopup: jest.fn(),
      isOpen: true,
      serviceSetService: jest.fn(),
      services: mockServices,
      showSnackbar: jest.fn(),
    };
    render(
      <ThemeI18nWrapper>
        <ServicePopup {...props} />
      </ThemeI18nWrapper>,
    );
    expect(screen.getByRole('dialog').textContent).toBeTruthy();
    const urlInput = screen.getByRole('textbox', { name: serviceUrlLabel });
    fireEvent.input(urlInput, { target: { value: mockUrl } });
    fireEvent.blur(urlInput);
    const nameInput = screen.getByRole('textbox', { name: serviceNameLabel });
    fireEvent.input(nameInput, { target: { value: 'Name' } });
    fireEvent.blur(nameInput);

    fireEvent.click(screen.getByRole('button', { name: /save/i }));
    await waitFor(() =>
      expect(props.serviceSetService).toHaveBeenCalledWith({
        id: '',
        name: 'Name',
        description: '',
        scope: 'user',
        type: 'EDR',
        url: mockUrl,
      }),
    );
    await waitFor(() => expect(props.showSnackbar).toHaveBeenCalledTimes(1));
    await waitFor(() => expect(props.closePopup).toHaveBeenCalledTimes(1));
  });

  it('should show snackbar after copy a url', async () => {
    const user = userEvent.setup();

    Object.defineProperty(navigator, 'clipboard', {
      value: {
        writeText: () => {},
      },
      writable: true,
      configurable: true,
    });
    jest.spyOn(navigator.clipboard, 'writeText');

    const props: ServicePopupProps = {
      servicePopupVariant: 'show',
      isOpen: true,
      serviceSetService: jest.fn(),
      services: mockServices,
      serviceUrl: 'test.url',
      showSnackbar: jest.fn(),
    };

    render(
      <ThemeI18nWrapper>
        <ServicePopup {...props} />
      </ThemeI18nWrapper>,
    );

    const copyButton = screen.getByRole('button', { name: /copy url/i });
    await user.click(copyButton);
    expect(navigator.clipboard.writeText).toHaveBeenCalledWith(
      props.serviceUrl,
    );

    expect(props.showSnackbar).toHaveBeenCalledWith(COPY_URL_MESSAGE);
  });

  it('should show snackbar after saving a service', async () => {
    const props: ServicePopupProps = {
      servicePopupVariant: 'edit',
      isOpen: true,
      serviceSetService: jest.fn(),
      services: mockServices,
      serviceUrl: mockServices[0].url,
      showSnackbar: jest.fn(),
      closePopup: jest.fn(),
    };
    render(
      <ThemeI18nWrapper>
        <ServicePopup {...props} />
      </ThemeI18nWrapper>,
    );

    const serviceNameField = screen.getByRole('textbox', {
      name: serviceNameLabel,
    });

    fireEvent.input(serviceNameField, { target: { value: 'Name' } });

    const saveButton = screen.getByTestId('saveServiceButton');
    fireEvent.click(saveButton);

    await waitFor(() =>
      expect(props.serviceSetService).toHaveBeenCalledTimes(1),
    );

    await waitFor(() =>
      expect(props.showSnackbar).toHaveBeenCalledWith(getSuccesMessage('Name')),
    );
    await waitFor(() => expect(props.closePopup).toHaveBeenCalledTimes(1));
  });

  it('should autoFocus on serviceUrl for new service', () => {
    render(
      <ThemeI18nWrapper>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetService={jest.fn()}
          services={mockServices}
        />
      </ThemeI18nWrapper>,
    );
    const input = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    expect(input.matches(':focus')).toBeTruthy();
  });

  it('should display loading indicator when adding new service is in process', async () => {
    jest.useFakeTimers();
    const url = 'https://mockurl.fi';
    jest
      .spyOn(utils, 'loadEDRService')
      .mockImplementationOnce(
        () =>
          new Promise((resolve) => {
            setTimeout(() => {
              resolve({
                id: 'id_1',
                name: 'Test',
                url,
                description: '',
                type: 'EDR',
                scope: 'user',
              });
            }, 1000);
          }),
      )
      .mockImplementationOnce(async () => {
        return {
          id: 'id_1',
          name: 'Test',
          url,
          description: '',
          type: 'EDR',
          scope: 'user',
        };
      });
    const props: ServicePopupProps = {
      servicePopupVariant: 'add',
      closePopup: jest.fn(),
      isOpen: true,
      serviceSetService: jest.fn(),
      services: mockServices,
      showSnackbar: jest.fn(),
    };
    render(
      <ThemeI18nWrapper>
        <ServicePopup {...props} />
      </ThemeI18nWrapper>,
    );

    const serviceNameTextField = screen.getByRole('textbox', {
      name: serviceNameLabel,
    });
    fireEvent.change(serviceNameTextField, {
      target: { value: 'Precipitation Radar NL' },
    });

    const serviceUrlField = screen.getByRole('textbox', {
      name: serviceUrlLabel,
    });

    fireEvent.change(serviceUrlField, {
      target: { value: url },
    });

    fireEvent.blur(serviceUrlField, {
      target: { value: url },
    });

    await waitFor(() => {
      expect(screen.getByTestId('loadingIndicator')).toBeTruthy();
    });

    await act(async () => {
      jest.advanceTimersByTime(1000);
    });

    await waitFor(() => {
      expect(screen.queryByTestId('loadingIndicator')).toBeFalsy();
    });

    fireEvent.click(screen.getByTestId('saveServiceButton'));

    await waitFor(() => {
      expect(props.serviceSetService).toHaveBeenCalled();
    });

    jest.clearAllTimers();
    jest.clearAllMocks();
    jest.useRealTimers();
  });
});
