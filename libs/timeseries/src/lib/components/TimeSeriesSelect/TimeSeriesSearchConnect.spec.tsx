/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import { TimeSeriesModuleState } from '../../store/types';
import {
  TimeSeriesThemeStoreProvider,
  TimeseriesI18nProvider,
} from '../Providers';
import SearchFieldConnect from './TimeSeriesSearchConnect';
import { timeSeriesActions } from '../../store';
import { createMockStore } from '../../store/store';

describe('components/TimeSeriesSelect/ServiceOptionsConnect', () => {
  it('should call setServicePopup when saving new service from the search bar', () => {
    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: 'mockMapId',
          plots: [{ plotId: 'plotId1', title: 'title' }],
          parameters: [],
        },
        services: [],
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {},
              ids: [],
            },
            allServiceFilterChipsEnabled: true,
          },
          servicePopup: {
            isOpen: false,
            variant: 'add',
          },
        },
      },
    };

    const spyToggleServicePopup = jest.spyOn(
      timeSeriesActions,
      'setServicePopup',
    );

    const store = createMockStore(timeSeriesStore);

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeseriesI18nProvider>
          <SearchFieldConnect />
        </TimeseriesI18nProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    const textInput = screen.getByRole('textbox', {
      name: 'Search TS or enter a service URL',
    });
    fireEvent.input(textInput, { target: { value: 'https://example.fi' } });
    fireEvent.blur(textInput);

    const addButton = screen.getByText('Save');
    fireEvent.click(addButton);

    expect(spyToggleServicePopup).toHaveBeenCalledTimes(1);
    expect(spyToggleServicePopup).toHaveBeenCalledWith({
      isOpen: true,
      url: 'https://example.fi',
      variant: 'save',
    });
  });
});
