/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { ApiProvider } from '@opengeoweb/api';
import { Store } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import {
  ThemeProviderProps,
  lightTheme,
  ThemeWrapper,
} from '@opengeoweb/theme';

import { Theme } from '@mui/material';
import i18n from 'i18next';
import { I18nextProvider } from 'react-i18next';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import { SnackbarWrapperConnect } from '@opengeoweb/snackbar';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { initTimeseriesI18n } from '../../utils/i18n';

export const TimeSeriesThemeWrapper: React.FC<ThemeProviderProps> = ({
  theme,
  children,
}: ThemeProviderProps) => {
  return (
    <TimeseriesI18nProvider>
      <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;
    </TimeseriesI18nProvider>
  );
};

const ThemeWrapperWithTranslations: React.FC<ThemeProviderProps> = ({
  theme,
  children,
}: ThemeProviderProps) => (
  <TimeseriesI18nProvider>
    <TimeSeriesThemeWrapper theme={theme}>{children}</TimeSeriesThemeWrapper>
  </TimeseriesI18nProvider>
);

interface TimeSeriesApiProviderProps {
  children: React.ReactNode;
  createApi?: () => void;
}

interface TimeSeriesThemeStoreProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
  store: Store;
}

interface TimeseriesTranslationWrapperProps {
  children?: React.ReactNode;
}

/**
 * A Provider component which returns the Api Provider for the TimeSeries library
 * @param children
 * @returns
 */
export const TimeSeriesApiProvider: React.FC<TimeSeriesApiProviderProps> = ({
  children,
  createApi = createFakeApi,
}: TimeSeriesApiProviderProps) => (
  <TimeseriesI18nProvider>
    <ApiProvider createApi={createApi}>{children}</ApiProvider>
  </TimeseriesI18nProvider>
);

export interface TimeSeriesStoreProviderProps extends ThemeProviderProps {
  store: Store;
}

export const TimeSeriesThemeStoreProvider: React.FC<
  TimeSeriesThemeStoreProviderProps
> = ({
  children,
  theme = lightTheme,
  store,
}: TimeSeriesThemeStoreProviderProps) => (
  <Provider store={store}>
    <ThemeWrapperWithTranslations theme={theme}>
      <SnackbarWrapperConnect>
        <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
      </SnackbarWrapperConnect>
    </ThemeWrapperWithTranslations>
  </Provider>
);

export const TimeseriesI18nProvider: React.FC<
  TimeseriesTranslationWrapperProps
> = ({ children }) => {
  initTimeseriesI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};
