/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { act, render, screen, within } from '@testing-library/react';
import { mapActions } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';
import {
  ServiceInterface,
  TimeSeriesService,
  dateUtils,
} from '@opengeoweb/shared';
import { serverMock } from '../../../mocks/server';
import {
  EDR_FINLAND_ECMWF_COLLECTION_ID,
  EDR_FINLAND_URL,
  EDR_NORWAY_COLLECTION_ID,
  EDR_NORWAY_URL,
  latestInstanceMock,
  oldInstanceMock,
} from '../../../mocks';
import { TimeSeriesManagerConnect } from './TimeSeriesManagerConnect';
import { timeSeriesActions } from '../../store';
import TimeSeriesManagerMapButtonConnect from './TimeSeriesManagerMapButtonConnect';
import { TimeSeriesStoreType } from '../../store/types';
import { ogcParameters } from '../TimeSeriesSelect/ogcParameters';
import { Parameter } from '../TimeSeries/types';
import { createMockStore } from '../../store/store';
import { TimeSeriesThemeStoreProvider } from '../Providers';

describe('components/TimeSeriesManager/TimeSeriesManager', () => {
  const mapId = 'mockMapId';
  const plotId = 'plotId1';

  beforeAll(() => serverMock.listen());
  afterEach(() => serverMock.resetHandlers());
  afterAll(() => serverMock.close());

  it('should remove plot', async () => {
    const parameterId = 'parameter1';
    const temperatureParameter = ogcParameters[0];
    const service: TimeSeriesService = {
      id: temperatureParameter.serviceId,
      url: 'serviceUrl',
      type: 'OGC',
      description: '',
      name: 'OGC',
      scope: 'system',
    };

    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerMapButtonConnect viewId={mapId} />
        <TimeSeriesManagerConnect />
      </TimeSeriesThemeStoreProvider>,
    );

    const plotTitle = 'Plot 1';
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId,
            plots: [
              {
                title: plotTitle,
                plotId,
              },
            ],
            parameters: [
              {
                id: parameterId,
                plotId,
                plotType: 'line',
                propertyName: temperatureParameter.propertyName,
                unit: temperatureParameter.unit,
                serviceId: service.id,
                collectionId: temperatureParameter.collectionId,
              },
            ],
          },
          services: [service],
        }),
      );
    });

    const user = userEvent.setup();

    await user.click(
      screen.getByRole('button', { name: 'TimeSeries Manager' }),
    );
    await user.click(
      screen.getByRole('button', {
        name: `Remove ${plotTitle}`,
      }),
    );
    await user.click(screen.getByRole('button', { name: 'Remove' }));
    screen.getByText(`Plot "${plotId}" has been deleted`);

    expect(
      screen.queryByRole('button', {
        name: `Remove ${plotTitle}`,
      }),
    ).not.toBeInTheDocument();
  });

  it('should duplicate a parameter', async () => {
    const parameterId = 'parameter1';
    const temperatureParameter = ogcParameters[0];
    const service: TimeSeriesService = {
      id: temperatureParameter.serviceId,
      url: 'serviceUrl',
      type: 'OGC',
      description: '',
      name: 'OGC',
      scope: 'system',
    };

    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <div>
          <TimeSeriesManagerMapButtonConnect viewId={mapId} />
          <TimeSeriesManagerConnect />
        </div>
      </TimeSeriesThemeStoreProvider>,
    );

    const plotTitle = 'Plot 1';
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId,
            plots: [
              {
                title: plotTitle,
                plotId,
              },
            ],
            parameters: [
              {
                id: parameterId,
                plotId,
                plotType: 'line',
                propertyName: temperatureParameter.propertyName,
                unit: temperatureParameter.unit,
                serviceId: service.id,
                collectionId: temperatureParameter.collectionId,
              },
            ],
          },
          services: [service],
        }),
      );
    });

    const user = userEvent.setup();

    await user.click(
      screen.getByRole('button', { name: 'TimeSeries Manager' }),
    );

    // Due to responsive design, (Small and Normal size) the amount of propertyNames is duplicated
    expect(
      screen.queryAllByText(temperatureParameter.propertyName).length,
    ).toEqual(2);

    await user.click(
      screen.getByRole('button', {
        name: `Duplicate ${temperatureParameter.propertyName}`,
      }),
    );
    expect(
      screen.getAllByText(temperatureParameter.propertyName).length,
    ).toEqual(4);
  });

  it('should show the info dialog', async () => {
    const parameterId = 'parameter1';
    const parameterId2 = 'parameter2';
    const temperatureParameter = ogcParameters[0];
    const parameter2 = ogcParameters[1];
    const service: TimeSeriesService = {
      id: temperatureParameter.serviceId,
      url: 'serviceUrl',
      type: 'OGC',
      description: '',
      name: 'OGC service',
      scope: 'system',
    };

    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <div>
          <TimeSeriesManagerMapButtonConnect viewId={mapId} />
          <TimeSeriesManagerConnect />
        </div>
      </TimeSeriesThemeStoreProvider>,
    );

    const plotTitle = 'Plot 1';
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId,
            plots: [
              {
                title: plotTitle,
                plotId,
              },
            ],
            parameters: [
              {
                id: parameterId,
                plotId,
                plotType: 'line',
                propertyName: temperatureParameter.propertyName,
                unit: temperatureParameter.unit,
                serviceId: service.id,
                collectionId: temperatureParameter.collectionId,
              },
              {
                id: parameterId2,
                plotId,
                plotType: 'line',
                propertyName: parameter2.propertyName,
                unit: parameter2.unit,
                serviceId: service.id,
                collectionId: parameter2.collectionId,
              },
            ],
          },
          services: [service],
        }),
      );
    });

    const user = userEvent.setup();

    await user.click(
      screen.getByRole('button', { name: 'TimeSeries Manager' }),
    );

    // Due to responstive design, the amount of properties found by using all-query is doubled
    expect(
      screen.getAllByText(temperatureParameter.propertyName).length,
    ).toEqual(2);

    // Press the first info button
    await user.click(
      screen.getAllByRole('button', {
        name: `Timeseries parameter info`,
        pressed: false,
      })[0],
    );

    expect(
      screen.getByRole('button', {
        name: `Timeseries parameter info`,
        pressed: true,
      }),
    ).toBeTruthy();

    expect(await screen.findByText('TS Info')).toBeTruthy();

    // We should find parameter name, collection id and related service
    const infoDialog = screen.getByRole('dialog', {
      name: 'TS Info',
    });

    expect(
      within(infoDialog).getByText(temperatureParameter.propertyName),
    ).toBeTruthy();
    expect(
      within(infoDialog).getByText(temperatureParameter.collectionId),
    ).toBeTruthy();
    expect(within(infoDialog).getByText(service.name)).toBeTruthy();
    expect(within(infoDialog).getByText(service.url)).toBeTruthy();
    expect(within(infoDialog).getByText(service.type)).toBeTruthy();

    // Close dialog
    const closeButton = within(infoDialog).getByRole('button', {
      name: 'Close',
    });

    await user.click(closeButton);
    expect(screen.queryByText('TS Info')).toBeFalsy();

    expect(
      screen.getAllByRole('button', {
        name: `Timeseries parameter info`,
        pressed: false,
      }),
    ).toHaveLength(2);

    // Now re-open the info dialog for the first parameter and then while open click it for the second parameter
    await user.click(
      screen.getAllByRole('button', {
        name: `Timeseries parameter info`,
        pressed: false,
      })[0],
    );

    const infoDialog2 = screen.getByRole('dialog', {
      name: 'TS Info',
    });

    expect(
      within(infoDialog2).getByText(temperatureParameter.propertyName),
    ).toBeTruthy();

    await user.click(
      screen.getByRole('button', {
        name: `Timeseries parameter info`,
        pressed: false,
      }),
    );

    const infoDialog3 = screen.getByRole('dialog', {
      name: 'TS Info',
    });

    expect(within(infoDialog3).getByText(parameter2.propertyName)).toBeTruthy();
  });

  it('should allow reordering plots by drag and drop', async () => {
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <div>
          <TimeSeriesManagerMapButtonConnect viewId={mapId} />
          <TimeSeriesManagerConnect />
        </div>
      </TimeSeriesThemeStoreProvider>,
    );

    const plot1Title = 'Plot 1';
    const plot2Title = 'Plot 2';
    const plot1Id = 'plotId1';
    const plot2Id = 'plotId2';
    const plot1 = {
      title: plot1Title,
      plotId: plot1Id,
    };
    const plot2 = {
      title: plot2Title,
      plotId: plot2Id,
    };
    const tooltipTitle = 'Drag';

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId,
            plots: [plot1, plot2],
            parameters: [],
          },
          services: [],
        }),
      );
    });

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', { name: 'TimeSeries Manager' }),
    );

    // eslint-disable-next-line testing-library/no-node-access
    await user.hover(document.getElementsByClassName('handle')[0]);
    expect((await screen.findByRole('tooltip')).textContent).toBe(tooltipTitle);
    // simulate drag and drop
    act(() => {
      store.dispatch(timeSeriesActions.movePlot({ oldIndex: 0, newIndex: 1 }));
    });
    expect(
      (store.getState().timeSeries as TimeSeriesStoreType).plotPreset?.plots,
    ).toEqual([plot2, plot1]);
  });

  it('should change a parameter', async () => {
    const parameterId = 'parameter1';
    const temperatureParameter = ogcParameters[0];
    const service: TimeSeriesService = {
      id: temperatureParameter.serviceId,
      url: 'serviceUrl',
      type: 'OGC',
      description: '',
      name: 'OGC',
    };

    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <div>
          <TimeSeriesManagerMapButtonConnect viewId={mapId} />
          <TimeSeriesManagerConnect />
        </div>
      </TimeSeriesThemeStoreProvider>,
    );

    const plotTitle = 'Plot 1';
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId,
            plots: [
              {
                title: plotTitle,
                plotId,
              },
            ],
            parameters: [
              {
                id: parameterId,
                plotId,
                plotType: 'line',
                propertyName: temperatureParameter.propertyName,
                unit: temperatureParameter.unit,
                serviceId: service.id,
                collectionId: temperatureParameter.collectionId,
              },
            ],
          },
          services: [service],
        }),
      );
    });

    const user = userEvent.setup();
    await user.click(
      screen.getByRole('button', { name: 'TimeSeries Manager' }),
    );

    // Due to responstive design, the amount of properties found by using all-query is doubled
    expect(
      screen.getAllByText(temperatureParameter.propertyName).length,
    ).toEqual(2);

    await user.click(
      screen.getByRole('combobox', {
        name: 'timeseriesManager-parameterName',
      }),
    );
    // Is there a list?
    expect(
      screen.getByRole('option', {
        name: temperatureParameter.propertyName,
      }),
    ).toBeTruthy();

    // Click a new parameter
    await user.click(
      screen.getByRole('option', {
        name: ogcParameters[4].propertyName,
      }),
    );

    // Check if the parameter was changed to the selected one
    expect(
      within(
        screen.getByRole('combobox', {
          name: 'timeseriesManager-parameterName',
        }),
      ).getByText(ogcParameters[4].propertyName),
    ).toBeTruthy();
  });
  it('should change reference time met.no', async () => {
    const parameter1: Parameter = {
      id: 'parameter1',
      plotId: 'Plot_1',
      unit: '°C',
      propertyName: 'Temperature',
      plotType: 'line',
      serviceId: 'serviceId',
      collectionId: EDR_NORWAY_COLLECTION_ID,
      opacity: 70,
      instanceId: '20230927T120000',
      instances: [{ id: '20230927T000000' }, { id: '20230927T120000' }],
    };

    const lastestRefTime = latestInstanceMock.id;
    const oldRefTime = oldInstanceMock.id;

    const timeFormat = "yyyy-MM-dd'T'HH:mm:ss 'Z'";

    const latestRefDate = dateUtils.dateToString(
      dateUtils.parseCustomDateString(lastestRefTime),
      timeFormat,
    );

    const oldRefDate = dateUtils.dateToString(
      dateUtils.parseCustomDateString(oldRefTime),
      timeFormat,
    );

    const service = {
      id: 'serviceId',
      type: 'EDR' as ServiceInterface,
      url: EDR_NORWAY_URL,
      description: 'a description',
      name: 'Norway EDR',
    };

    const store = createMockStore();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <div>
          <TimeSeriesManagerMapButtonConnect viewId={mapId} />
          <TimeSeriesManagerConnect />
        </div>
      </TimeSeriesThemeStoreProvider>,
    );

    const plotTitle = 'Plot 1';
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId,
            plots: [
              {
                title: plotTitle,
                plotId: parameter1.plotId,
              },
            ],
            parameters: [
              {
                id: parameter1.id,
                plotId: parameter1.plotId,
                plotType: 'line',
                propertyName: parameter1.propertyName,
                unit: parameter1.unit,
                serviceId: 'serviceId',
                collectionId: EDR_NORWAY_COLLECTION_ID,
                instances: parameter1.instances,
              },
            ],
          },
          services: [service],
        }),
      );
    });

    const user = userEvent.setup();

    await user.click(
      screen.getByRole('button', { name: 'TimeSeries Manager' }),
    );

    // Due to responsive design, (Small and Normal size) the amount of propertyNames is duplicated
    expect(screen.getAllByText(parameter1.propertyName).length).toEqual(2);

    await user.click(
      screen.getByRole('combobox', {
        name: 'timeseriesManager-refTime',
      }),
    );

    // Is there a list?
    expect(
      screen.getByRole('option', {
        name: latestRefDate,
      }),
    ).toBeTruthy();

    // Click a new ref.time
    await user.click(
      screen.getByRole('option', {
        name: oldRefDate,
      }),
    );

    // Check if the ref.time was changed to the selected one
    expect(
      within(
        screen.getByRole('combobox', {
          name: 'timeseriesManager-refTime',
        }),
      ).getByText(oldRefDate!),
    ).toBeTruthy();
  });
  it('should change reference time FMI', async () => {
    const parameter1: Parameter = {
      id: 'parameter1',
      plotId: 'Plot_1',
      unit: '°C',
      propertyName: 'Temperature',
      plotType: 'line',
      serviceId: 'serviceId',
      collectionId: EDR_FINLAND_ECMWF_COLLECTION_ID,
      opacity: 70,
      instanceId: '20230927T120000',
      instances: [{ id: '20230927T000000' }, { id: '20230927T120000' }],
    };

    const lastestRefTime = latestInstanceMock.id;
    const oldRefTime = oldInstanceMock.id;

    const timeFormat = "yyyy-MM-dd'T'HH:mm:ss 'Z'";

    const latestRefDate = dateUtils.dateToString(
      dateUtils.parseCustomDateString(lastestRefTime),
      timeFormat,
    );

    const oldRefDate = dateUtils.dateToString(
      dateUtils.parseCustomDateString(oldRefTime),
      timeFormat,
    );

    const service = {
      id: 'serviceId',
      type: 'EDR' as ServiceInterface,
      url: EDR_FINLAND_URL,
      description: 'a description',
      name: 'Finland EDR',
    };

    const store = createMockStore();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <div>
          <TimeSeriesManagerMapButtonConnect viewId={mapId} />
          <TimeSeriesManagerConnect />
        </div>
      </TimeSeriesThemeStoreProvider>,
    );

    const plotTitle = 'Plot 1';
    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId,
            plots: [
              {
                title: plotTitle,
                plotId: parameter1.plotId,
              },
            ],
            parameters: [
              {
                id: parameter1.id,
                plotId: parameter1.plotId,
                plotType: 'line',
                propertyName: parameter1.propertyName,
                unit: parameter1.unit,
                serviceId: 'serviceId',
                collectionId: EDR_FINLAND_ECMWF_COLLECTION_ID,
                instances: parameter1.instances,
              },
            ],
          },
          services: [service],
        }),
      );
    });

    const user = userEvent.setup();

    await user.click(
      screen.getByRole('button', { name: 'TimeSeries Manager' }),
    );

    // Due to responsive design, (Small and Normal size) the amount of propertyNames is duplicated
    expect(screen.getAllByText(parameter1.propertyName).length).toEqual(2);

    await user.click(
      screen.getByRole('combobox', {
        name: 'timeseriesManager-refTime',
      }),
    );

    // Is there a list?
    expect(
      screen.getByRole('option', {
        name: latestRefDate,
      }),
    ).toBeTruthy();

    // Click a new ref.time
    await user.click(
      screen.getByRole('option', {
        name: oldRefDate,
      }),
    );

    // Check if the ref.time was changed to the selected one
    expect(
      within(
        screen.getByRole('combobox', {
          name: 'timeseriesManager-refTime',
        }),
      ).getByText(oldRefDate!),
    ).toBeTruthy();
  });
});
