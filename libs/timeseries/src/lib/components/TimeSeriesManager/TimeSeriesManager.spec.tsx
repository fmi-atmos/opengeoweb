/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';

import React from 'react';
import { TimeSeriesManager } from './TimeSeriesManager';
import { Parameter, PlotPreset } from '../TimeSeries/types';
import { mockTimeSeriesServices } from '../TimeSeries/mockTimeSeriesServices';
import { createMockStore } from '../../store/store';
import { TimeSeriesThemeStoreProvider } from '../Providers';

describe('src/components/TimeSeriesManager/TimeSeriesManager', () => {
  const parameter1: Parameter = {
    id: 'parameter1',
    plotId: 'Plot_1',
    unit: '°C',
    propertyName: 'Temperature',
    plotType: 'line',
    serviceId: mockTimeSeriesServices[0].id,
    collectionId: 'ecmwf',
    opacity: 70,
  };

  const plotPreset: PlotPreset = {
    mapId: 'TimeseriesMap',
    plots: [
      {
        title: 'Plot 1',
        plotId: 'Plot_1',
      },
      {
        title: 'Plot 2',
        plotId: 'Plot_2',
      },
    ],
    parameters: [
      parameter1,
      {
        plotId: 'Plot_2',
        unit: 'mm',
        propertyName: 'Precipitation1h',
        plotType: 'bar',
        serviceId: mockTimeSeriesServices[0].id,
        collectionId: 'ecmwf',
        id: 'precipPlot2',
      },
    ],
  };

  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.useRealTimers();
  });

  it('should render rows, delete row, toggle row visibility, change color, type and opacity of parameter', async () => {
    const addPlot = jest.fn();
    const deletePlot = jest.fn();
    const deleteParameter = jest.fn();
    const togglePlot = jest.fn();
    const setSelectPlotId = jest.fn();
    const toggleParameter = jest.fn();
    const patchParameter = jest.fn();
    const addParameter = jest.fn();
    const updateTitle = jest.fn();
    const movePlot = jest.fn();
    const moveParameter = jest.fn();

    const store = createMockStore();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManager
          isOpen={true}
          onClose={jest.fn()}
          onMouseDown={jest.fn()}
          order={1}
          plotState={plotPreset}
          addPlot={addPlot}
          deletePlot={deletePlot}
          togglePlot={togglePlot}
          deleteParameter={deleteParameter}
          toggleParameter={toggleParameter}
          selectPlotId="testPlot1"
          setSelectPlotId={setSelectPlotId}
          patchParameter={patchParameter}
          addParameter={addParameter}
          updateTitle={updateTitle}
          movePlot={movePlot}
          moveParameter={moveParameter}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    // eslint-disable-next-line testing-library/no-node-access
    expect(document.getElementsByClassName('handle').length).toBe(2);

    expect(screen.getByText('Plot 1')).toBeTruthy();
    expect(screen.getAllByText('Temperature')[1]).toBeTruthy();
    expect(screen.getAllByTestId('timeSeriesSelectButton')[0]).toBeTruthy();
    expect(screen.getByTestId('timeSeriesAddPlotButton')).toBeTruthy();

    expect(screen.getByText('Plot 2')).toBeTruthy();
    expect(screen.getAllByText('Precipitation1h')[1]).toBeTruthy();

    fireEvent.click(screen.getAllByTestId('timeSeriesAddPlotButton')[0]);
    expect(addPlot).toHaveBeenCalledTimes(1);

    fireEvent.click(screen.getAllByTestId('deleteButton')[0]);
    expect(deletePlot).toHaveBeenCalledWith('Plot_1');

    fireEvent.click(screen.getAllByTestId('deleteButton')[1]);
    expect(deleteParameter).toHaveBeenCalledWith(parameter1.id);

    // Due to responsive design, the amount of toggle buttons is 4
    const toggleButtons = screen.getAllByTestId('enableButton');
    fireEvent.click(toggleButtons[0]);
    expect(togglePlot).toHaveBeenCalledWith('Plot_1');

    fireEvent.click(toggleButtons[1]);
    expect(toggleParameter).toHaveBeenCalledWith(parameter1.id);

    fireEvent.click(screen.getAllByTestId('timeSeriesSelectButton')[0]);
    expect(setSelectPlotId).toHaveBeenCalledWith('Plot_1');

    fireEvent.mouseDown(
      screen.getAllByRole('combobox', { name: 'timeseriesManager-type' })[0],
    );
    fireEvent.click(screen.getAllByRole('option', { name: /bar/i })[0]);
    expect(patchParameter).toHaveBeenCalledWith({
      id: parameter1.id,
      plotType: 'bar',
    });

    fireEvent.mouseDown(
      screen.getAllByRole('combobox', { name: 'timeseriesManager-type' })[0],
    );
    fireEvent.click(screen.getByRole('option', { name: /area/i }));
    expect(patchParameter).toHaveBeenCalledWith({
      id: parameter1.id,
      plotType: 'area',
    });

    fireEvent.mouseDown(
      screen.getAllByRole('combobox', { name: 'timeseriesManager-type' })[0],
    );
    fireEvent.click(screen.getByRole('option', { name: /scatter/i }));
    expect(patchParameter).toHaveBeenCalledWith({
      id: parameter1.id,
      plotType: 'scatter',
    });

    fireEvent.mouseDown(
      screen.getAllByRole('combobox', { name: 'timeseriesManager-color' })[0],
    );
    fireEvent.click(screen.getByRole('option', { name: /V/i }));
    expect(patchParameter).toHaveBeenCalledWith({
      id: parameter1.id,
      color: 'V',
    });

    fireEvent.mouseDown(
      screen.getAllByRole('combobox', { name: 'timeseriesManager-opacity' })[0],
    );
    fireEvent.click(screen.getByRole('option', { name: /100/i }));
    expect(patchParameter).toHaveBeenCalledWith({
      id: parameter1.id,
      opacity: 100,
    });
  });

  it('should should update plot title', async () => {
    const addPlot = jest.fn();
    const deletePlot = jest.fn();
    const deleteParameter = jest.fn();
    const togglePlot = jest.fn();
    const setSelectPlotId = jest.fn();
    const toggleParameter = jest.fn();
    const patchParameter = jest.fn();
    const addParameter = jest.fn();
    const updateTitle = jest.fn();
    const movePlot = jest.fn();
    const moveParameter = jest.fn();

    const store = createMockStore();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManager
          isOpen={true}
          onClose={jest.fn()}
          onMouseDown={jest.fn()}
          order={1}
          plotState={plotPreset}
          addPlot={addPlot}
          deletePlot={deletePlot}
          togglePlot={togglePlot}
          deleteParameter={deleteParameter}
          toggleParameter={toggleParameter}
          selectPlotId="testPlot1"
          setSelectPlotId={setSelectPlotId}
          patchParameter={patchParameter}
          addParameter={addParameter}
          updateTitle={updateTitle}
          movePlot={movePlot}
          moveParameter={moveParameter}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    fireEvent.click(document.body); // Ensure click does not trigger title update
    await waitFor(() => {
      expect(updateTitle).not.toHaveBeenCalled();
    });

    fireEvent.click(screen.getByText('Plot 1'));
    fireEvent.change(screen.getByDisplayValue('Plot 1'), {
      target: { value: 'Plot 1 changed' },
    });
    await waitFor(() => {
      expect(updateTitle).not.toHaveBeenCalled();
    });
    fireEvent.click(document.body); // Click outside the input field to trigger update
    await waitFor(() => {
      expect(updateTitle).toHaveBeenCalledWith({
        title: 'Plot 1 changed',
        plotId: 'Plot_1',
      });
    });
  });
});
