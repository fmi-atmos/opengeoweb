/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, screen } from '@testing-library/react';
import { TFunction } from 'i18next';
import userEvent from '@testing-library/user-event';
import { createMockStore } from '@opengeoweb/store';
import { mockTimeSeriesServices } from '../../TimeSeries/mockTimeSeriesServices';
import { EDRInstance, PlotType } from '../../TimeSeries/types';
import { TimeSeriesThemeStoreProvider } from '../../Providers';
import { ReferenceTimeSelect } from './ParameterReferenceTimeSelect';

describe('ParameterTypeSelect', () => {
  const parameter = {
    plotId: 'Plot_2',
    unit: 'mm',
    propertyName: 'Precipitation1h',
    plotType: 'bar' as PlotType,
    serviceId: mockTimeSeriesServices[0].id,
    collectionId: 'ecmwf',
    id: 'precipPlot2',
    dimension: {
      id: 'dim1',
      value: '25',
    },
  };
  const patchParameter = jest.fn();

  const t = ((key: string) => key) as unknown as TFunction;

  const refTimes: EDRInstance[] = [
    {
      id: 'A',
    },
    {
      id: 'B',
    },
  ];

  it('should show the reference time selection menu and options should be clickable', async () => {
    const store = createMockStore();
    const user = userEvent.setup();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <div>
          <ReferenceTimeSelect
            rowIsEnabled={true}
            parameter={parameter}
            refTimeList={refTimes}
            patchParameter={patchParameter}
            t={t}
          />
        </div>
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByTestId('parameterReferenceTimeSelect')).toBeTruthy();

    // Open the selection menu
    await user.click(
      screen.getByRole('combobox', {
        name: 'timeseriesManager-refTime',
      }),
    );

    expect(screen.getByTestId(`timeseries-ref-time-A`)).toBeTruthy();

    // Click one of the options
    await user.click(screen.getByTestId(`timeseries-ref-time-B`));

    expect(patchParameter).toHaveBeenCalled();
  });
});
