/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { FeatureCollection, GeoJsonProperties, Point } from 'geojson';
import type { Meta, StoryObj } from '@storybook/react';
import { LocationDropdown } from './LocationDropdown';
import { TimeseriesI18nProvider } from '../../Providers';
import { GeoJsonWithService } from '../types';

const meta: Meta<typeof LocationDropdown> = {
  title: 'components/LocationDropdown',
  component: LocationDropdown,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the LocationDropdown',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof LocationDropdown>;

const featureCollection = 'FeatureCollection';

const testGeoJson1 = {
  type: featureCollection,
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [3.8103, 55.3992],
      },
      properties: {
        id: 'a12cpp',
        name: 'A12-CPP-very very looooooooong name',
        serviceId: 'METNorway',
        collectionId: 'meps-det-vdiv',
        drawFunctionId: 'drawFunctionId_4',
        hoverDrawFunctionId: 'drawFunctionId_6',
      },
    },
  ],
} as FeatureCollection<Point, GeoJsonProperties>;

const testGeoJson2 = {
  type: featureCollection,
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [3.8103, 55.3992],
      },
      properties: {
        id: 'a12cpp',
        name: 'A12-CPP',
        serviceId: 'METNorway',
        collectionId: 'meps-det-vdiv',
        drawFunctionId: 'drawFunctionId_4',
        hoverDrawFunctionId: 'drawFunctionId_6',
      },
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [4.8103, 56.3992],
      },
      properties: {
        id: 'b34def',
        name: 'B34-DEF',
        serviceId: 'METNorway',
        collectionId: 'meps-det-vdiv',
        drawFunctionId: 'drawFunctionId_7',
        hoverDrawFunctionId: 'drawFunctionId_9',
      },
    },
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [5.8103, 57.3992],
      },
      properties: {
        id: 'c56ghi',
        name: 'C56-GHI',
        serviceId: 'METNorway',
        collectionId: 'meps-det-vdiv',
        drawFunctionId: 'drawFunctionId_10',
        hoverDrawFunctionId: 'drawFunctionId_12',
      },
    },
  ],
} as FeatureCollection<Point, GeoJsonProperties>;

const splitGeoJson: GeoJsonWithService[] = [
  {
    collectionName: 'Collection-1',
    geoJson: testGeoJson1,
    serviceName: 'TestDataset-1',
  },
  {
    collectionName: 'Collection-2',
    geoJson: testGeoJson2,
    serviceName: 'TestDataset-2',
  },
];

export const Component: Story = {
  args: {
    handleChange: () => {},
    splitGeoJson,
    selectedLocation: 'Helsinki',
  },
  render: (props) => (
    <TimeseriesI18nProvider>
      <LocationDropdown {...props} />
    </TimeseriesI18nProvider>
  ),
  tags: ['autodocs', '!dev'],
};

export const ComponentDark: Story = {
  ...Component,
  tags: ['autodocs', '!dev', 'dark'],
};

export const LocationDropdownDemoLight: Story = {
  render: () => (
    <TimeseriesI18nProvider>
      <LocationDropdown
        splitGeoJson={splitGeoJson}
        handleChange={() => {}}
        selectedLocation="Helsinki"
      />
    </TimeseriesI18nProvider>
  ),
  tags: ['snapshot', '!autodocs'],
};

export const LocationDropdownDemoDark: Story = {
  render: () => (
    <TimeseriesI18nProvider>
      <LocationDropdown
        splitGeoJson={splitGeoJson}
        handleChange={() => {}}
        selectedLocation="Helsinki"
      />
    </TimeseriesI18nProvider>
  ),
  tags: ['snapshot', 'dark', '!autodocs'],
};
