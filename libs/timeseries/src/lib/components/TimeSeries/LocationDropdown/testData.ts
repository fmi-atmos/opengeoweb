/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { GeoJsonWithService } from '../types';

export const testDataWith3Services: GeoJsonWithService[] = [
  {
    geoJson: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [10.7522, 59.9139], // Oslo, Norway
          },
          properties: {
            name: 'Oslo',
            temperature: '5°C',
          },
        },
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [5.7331, 58.969], // Stavanger, Norway
          },
          properties: {
            name: 'Stavanger',
            temperature: '7°C',
          },
        },
        // Add more features here...
      ],
    },
    collectionName: 'TestCollection-2',
    serviceName: 'METNorway',
  },
  {
    geoJson: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [24.9355, 60.1695], // Helsinki, Finland
          },
          properties: {
            name: 'Helsinki',
            temperature: '2°C',
          },
        },
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [25.7471, 60.9939], // Lahti, Finland
          },
          properties: {
            name: 'Lahti',
            temperature: '1°C',
          },
        },

        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [27.7278, 64.2222], // Kajaani, Finland
          },
          properties: {
            name: 'Kajaani',
            temperature: '3°C',
          },
        },
        // Add more features here...
      ],
    },
    serviceName: 'fmi',
    collectionName: 'TestCollection-2',
  },
  {
    geoJson: {
      type: 'FeatureCollection',
      bbox: [-180, -90, 180, 90],
      features: [
        {
          id: 'Australia',
          type: 'Feature',
          properties: {
            name: 'Australia',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [113.338953, -43.634597],
                [113.338953, -10.668186],
                [153.569469, -10.668186],
                [153.569469, -43.634597],
                [113.338953, -43.634597],
              ],
            ],
          },
        },
        {
          id: 'Brazil',
          type: 'Feature',
          properties: {
            name: 'Brazil',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [-73.987235, -33.768378],
                [-73.987235, 5.244486],
                [-34.729993, 5.244486],
                [-34.729993, -33.768378],
                [-73.987235, -33.768378],
              ],
            ],
          },
        },
      ],
    },
    collectionName: 'TestCollection-3',
    serviceName: 'MetOfficeUK',
  },
];
