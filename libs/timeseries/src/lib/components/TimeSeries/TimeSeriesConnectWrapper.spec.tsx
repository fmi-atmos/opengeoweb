/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { act, render, screen, waitFor } from '@testing-library/react';
import axios from 'axios';
import {
  TimeSeriesConnectWrapper,
  ERROR_TITLE,
} from './TimeSeriesConnectWrapper';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { TimeSeriesApi } from '../../utils/api';
import {
  TimeSeriesThemeStoreProvider,
  TimeSeriesApiProvider,
} from '../Providers';
import { TimeSeriesStoreType } from '../../store/types';
import { mockTimeSeriesServices } from './mockTimeSeriesServices';
import { createMockStore } from '../../store/store';

jest.mock('axios');

describe('components/TimeSeries/TimeSeriesConnectWrapper', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  const timeSeriesPreset: TimeSeriesStoreType = {
    services: mockTimeSeriesServices,
    plotPreset: {
      mapId: 'map-1',
      plots: [],
      parameters: [],
    },
  };

  it('should render correctly with config', async () => {
    const mockedAxios = axios as jest.Mocked<typeof axios>;

    mockedAxios.get.mockResolvedValue({
      data: { features: [] },
    });

    const timeseriesConfigKey = 'exampleTimeSeriesPresetLocations.json';

    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesApiProvider createApi={createFakeApi}>
          <TimeSeriesConnectWrapper
            productConfigKey={timeseriesConfigKey}
            timeSeriesPreset={timeSeriesPreset}
            panelId="panelId"
          />
        </TimeSeriesApiProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    await act(async () => jest.advanceTimersByTime(2000));
    await waitFor(() => {
      expect(screen.queryByTestId('loadingSpinner')).toBeFalsy();
    });
    expect(screen.getByTestId('TimeSeriesConnect')).toBeTruthy();
  });

  it('should fetch timeseries config from api', async () => {
    const timeseriesRequest = jest.fn();
    const fakeApi = (): TimeSeriesApi =>
      ({
        ...createFakeApi(),
        getTimeSeriesConfiguration: timeseriesRequest,
      }) as unknown as TimeSeriesApi;

    const timeseriesConfigKey = 'exampleTimeSeriesPresetLocations.json';
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesApiProvider createApi={fakeApi}>
          <TimeSeriesConnectWrapper
            productConfigKey={timeseriesConfigKey}
            timeSeriesPreset={timeSeriesPreset}
            panelId="panelId"
          />
        </TimeSeriesApiProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('ConfigurableMap')).toBeFalsy();
    });

    expect(timeseriesRequest).toHaveBeenCalledWith(
      'exampleTimeSeriesPresetLocations.json',
      expect.any(String),
    );
  });

  it('should show error if configuration is missing', async () => {
    const timeseriesRequest = jest.fn();
    const fakeApi = (): TimeSeriesApi =>
      ({
        ...createFakeApi(),
        getTimeSeriesConfiguration: timeseriesRequest,
      }) as unknown as TimeSeriesApi;

    const emptyConfig = '';
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesApiProvider createApi={fakeApi}>
          <TimeSeriesConnectWrapper
            productConfigKey={emptyConfig}
            timeSeriesPreset={timeSeriesPreset}
            panelId="panelId"
          />
        </TimeSeriesApiProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    expect(await screen.findByText(ERROR_TITLE)).toBeTruthy();
  });

  it('should not render without config', async () => {
    const timeseriesRequest = jest.fn();
    const fakeApi = (): TimeSeriesApi =>
      ({
        ...createFakeApi(),
        getTimeSeriesConfiguration: timeseriesRequest,
      }) as unknown as TimeSeriesApi;

    const emptyConfig = '';
    const store = createMockStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesApiProvider createApi={fakeApi}>
          <TimeSeriesConnectWrapper
            productConfigKey={emptyConfig}
            timeSeriesPreset={timeSeriesPreset}
            panelId="panelId"
          />
        </TimeSeriesApiProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('ConfigurableMap')).toBeFalsy();
    });
  });

  it('should hide location markers if configured', async () => {
    const mockedAxios = axios as jest.Mocked<typeof axios>;

    mockedAxios.get.mockResolvedValue({
      data: { features: [] },
    });

    const timeseriesConfigKey = 'exampleTimeSeriesPresetLocations.json';
    const store = createMockStore();

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesApiProvider createApi={createFakeApi}>
          <TimeSeriesConnectWrapper
            productConfigKey={timeseriesConfigKey}
            timeSeriesPreset={timeSeriesPreset}
            panelId="panelId"
            hideLocationMarkers={true}
          />
        </TimeSeriesApiProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    await act(async () => jest.advanceTimersByTime(2000));
    await waitFor(() => {
      expect(screen.queryByTestId('loadingSpinner')).toBeFalsy();
    });
    expect(screen.getByTestId('TimeSeriesConnect')).toBeTruthy();
    expect(screen.getByRole('checkbox')).not.toBeChecked();
  });
});
