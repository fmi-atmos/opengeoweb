/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { ThemeTypes, ThemeWrapper, darkTheme } from '@opengeoweb/theme';
import type { Meta, StoryObj } from '@storybook/react';
import { TimeSeriesChart } from './TimeSeriesChart';
import {
  plotsWithData,
  plotsWithNullData,
} from './storyUtils/chartSeriesExampleData';
import { ToggleThemeButton } from '../../storybookUtils/ToggleThemeButton';
import { PlotWithData } from './types';

const meta: Meta<typeof TimeSeriesChart> = {
  title: 'components/TimeSeriesChart',
  component: TimeSeriesChart,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the TimeSeriesChart',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof TimeSeriesChart>;

export const Component: Story = {
  args: {
    plotsWithData,
    animation: true,
  },
};

export const ThemeChartDemo: Story = {
  render: () => (
    <div style={{ width: '100vw', height: '100vh' }}>
      <ToggleThemeButton />
      <TimeSeriesChart plotsWithData={plotsWithData} />
    </div>
  ),
  tags: ['!autodocs'],
};

interface ChartDemoProps {
  data: PlotWithData[];
}
const ChartDemo: React.FC<ChartDemoProps> = ({ data }) => (
  <div style={{ width: '800px', height: '500px' }}>
    <TimeSeriesChart
      plotsWithData={data}
      animation={document.location.hash !== '#snapshot'}
    />
  </div>
);

export const StaticChartDemoLight: Story = {
  render: () => <ChartDemo data={plotsWithData} />,
  tags: ['snapshot'],
};

export const StaticChartDemoDark: Story = {
  render: () => (
    <ThemeWrapper theme={darkTheme} defaultThemeName={ThemeTypes.DARK_THEME}>
      <ChartDemo data={plotsWithData} />
    </ThemeWrapper>
  ),
  tags: ['snapshot', 'dark'],
};

export const ThemeChartWithNullValues: Story = {
  render: () => <ChartDemo data={[plotsWithNullData]} />,
  tags: ['snapshot'],
  parameters: {
    docs: {
      description: {
        story: 'Example with null values',
      },
    },
  },
};

ThemeChartWithNullValues.storyName = 'Static Chart with gaps';
