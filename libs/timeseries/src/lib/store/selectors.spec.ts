/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { PlotPreset, TimeSeriesService } from '@opengeoweb/shared';
import { TimeSeriesModuleState } from './types';
import {
  getCurrentParameterInfoDisplayed,
  getMapId,
  getParameterById,
  getParameterInfoParameter,
  getPlotState,
  getPlotWithParameters,
  getSearchFilter,
  getServicePopupDetails,
  getTimeSeriesPreset,
} from './selectors';
import { Parameter } from '../components/TimeSeries/types';
import { mockTimeSeriesServices as services } from '../components/TimeSeries/mockTimeSeriesServices';

describe('store/ui/selectors', () => {
  const mapId = 'mapId';
  const plotId1 = 'plotId1';

  const parameter1: Parameter = {
    plotId: plotId1,
    unit: '°C',
    propertyName: 'Temperature',
    plotType: 'line',
    serviceId: services[0].id,
    id: 'param1',
    collectionId: 'ecmwf',
  };
  const parameter2: Parameter = {
    plotId: plotId1,
    unit: '°C',
    propertyName: 'DewPoint',
    plotType: 'line',
    serviceId: services[0].id,
    collectionId: 'ecmwf',
  };
  const plot1 = {
    title: 'Plot 1',
    plotId: plotId1,
  };

  const customServices: TimeSeriesService[] = [
    {
      id: 'customServiceId',
      description: 'custom service',
      url: 'https://custom.service',
      type: 'EDR',
      name: 'Custom Service',
      scope: 'system',
    },
    {
      id: 'customServiceId2',
      description: 'custom service 2',
      url: 'https://custom.service2',
      type: 'EDR',
      name: 'Custom Service 2',
      scope: 'system',
    },
  ];

  const mockStore: TimeSeriesModuleState = {
    timeSeries: {
      plotPreset: {
        mapId,
        plots: [
          plot1,
          {
            title: 'Plot 2',
            plotId: 'plotId2',
          },
        ],
        parameters: [
          parameter1,
          parameter2,
          {
            plotId: 'plotId2',
            unit: '%',
            propertyName: 'Humidity',
            plotType: 'line',
            serviceId: services[0].id,
            collectionId: 'ecmwf',
          },
        ],
      },
      services,
      timeseriesSelect: {
        filters: {
          searchFilter: 'some search filter',
          serviceFilterChips: {
            entities: {
              [services[0].id]: {
                serviceId: services[0].id,
                type: services[0].type,
                serviceUrl: services[0].url,
                serviceName: services[0].name,
                enabled: true,
                scope: 'system',
              },
            },
            ids: [services[0].id],
          },
          allServiceFilterChipsEnabled: true,
        },
        servicePopup: {
          isOpen: false,
          variant: 'save',
        },
        currentParameterInfo: parameter1.id,
      },
    },
  };

  const emptyMockStore: TimeSeriesModuleState = {
    timeSeries: undefined,
  };
  describe('getPlotState', () => {
    it('should return plots', () => {
      expect(getPlotState(mockStore)).toEqual(mockStore.timeSeries!.plotPreset);
    });
    it('should return undefined if no plots', () => {
      expect(getPlotState(emptyMockStore)).toEqual(undefined);
    });
    describe('getMapId', () => {
      it('should return mapId', () => {
        expect(getMapId(mockStore)).toEqual(mapId);
      });
      it('should return undefined if no state', () => {
        expect(getMapId(emptyMockStore)).toEqual(undefined);
      });
    });

    describe('getPlotWithParameters', () => {
      it('should return plot with parameters', () => {
        const plot = getPlotWithParameters(mockStore, plotId1);
        expect(plot).toEqual({
          ...plot1,
          parameters: [parameter1, parameter2],
        });
      });
      it('should return undefined if no state', () => {
        expect(getPlotWithParameters(emptyMockStore, plotId1)).toEqual(
          undefined,
        );
      });
      it('should return undefined if no plot id', () => {
        expect(getPlotWithParameters(mockStore, '')).toEqual(undefined);
      });
      it('should return undefined if plot id is not in store', () => {
        expect(getPlotWithParameters(mockStore, 'plotIdNotInStore')).toEqual(
          undefined,
        );
      });
    });

    describe('getSearchFilter', () => {
      it('should return the search filter', () => {
        expect(getSearchFilter(mockStore)).toEqual('some search filter');
      });
      it('should return empty string if no state', () => {
        expect(getSearchFilter(emptyMockStore)).toEqual('');
      });
    });

    describe('getCurrentParameterInfoDisplayed', () => {
      it('should return the paramter info', () => {
        expect(getCurrentParameterInfoDisplayed(mockStore)).toEqual(
          parameter1.id,
        );
      });
      it('should return undefined if no state', () => {
        expect(getCurrentParameterInfoDisplayed(emptyMockStore)).toEqual('');
      });
    });

    describe('getParameterInfoParameter', () => {
      it('should return the parameter with correct id', () => {
        expect(getParameterInfoParameter(mockStore)).toEqual(parameter1);
      });

      it('should return memory item when no id specified', () => {
        expect(
          getParameterInfoParameter({
            ...mockStore,
            timeSeries: {
              ...mockStore.timeSeries,
              timeseriesSelect: {
                filters: mockStore.timeSeries!.timeseriesSelect!.filters,
                currentParameterInfo: {
                  plotId: 'plotTest',
                  propertyName: 'propertyTest',
                  serviceId: 'serviceTest',
                  collectionId: 'collectionTest',
                  plotType: 'line',
                },
                servicePopup: {
                  isOpen: false,
                  variant: 'add',
                },
              },
            },
          }),
        ).toEqual({
          plotId: 'plotTest',
          propertyName: 'propertyTest',
          serviceId: 'serviceTest',
          collectionId: 'collectionTest',
          plotType: 'line',
        });
      });

      it('should return undefined if no plotPreset', () => {
        expect(
          getParameterInfoParameter({
            ...mockStore,
            timeSeries: {
              ...mockStore.timeSeries,
              plotPreset: undefined,
            },
          }),
        ).toEqual(undefined);
      });

      it('should return undefined if no invalid id', () => {
        expect(
          getParameterInfoParameter({
            ...mockStore,
            timeSeries: {
              ...mockStore.timeSeries,
              timeseriesSelect: {
                filters: mockStore.timeSeries!.timeseriesSelect!.filters,
                currentParameterInfo: 'invalidId',
                servicePopup: {
                  isOpen: false,
                  variant: 'add',
                },
              },
            },
          }),
        ).toEqual(undefined);
      });

      it('should return undefined if no state', () => {
        expect(getParameterInfoParameter(emptyMockStore)).toEqual(undefined);
      });
    });

    describe('getParameterById', () => {
      it('should return the related parameter', () => {
        expect(getParameterById(mockStore, parameter1.id as string)).toEqual(
          parameter1,
        );
      });
      it('should return undefined if no state', () => {
        expect(
          getParameterById(emptyMockStore, parameter1.id as string),
        ).toEqual(undefined);
      });
    });

    describe('getServicePopupDetails', () => {
      it('should return the related object', () => {
        expect(getServicePopupDetails(mockStore)).toEqual({
          isOpen: false,
          variant: 'save',
        });
      });
      it('should return initial state if no state', () => {
        expect(getServicePopupDetails(emptyMockStore)).toEqual({
          isOpen: false,
          variant: 'add',
        });
      });
    });

    describe('getTimeSeriesPreset', () => {
      it('should return the timeseries preset', () => {
        expect(getTimeSeriesPreset(mockStore)).toEqual({
          plotPreset: mockStore.timeSeries!.plotPreset!,
          services: mockStore.timeSeries!.services!,
        });
      });
      it('should return only services used in paramters', () => {
        const plotPreset: PlotPreset = {
          ...mockStore.timeSeries!.plotPreset!,
          parameters: [
            parameter1,
            parameter2,
            {
              plotId: 'plotId2',
              unit: 'Custom',
              propertyName: 'Custom Property',
              plotType: 'line',
              serviceId: customServices[0].id,
              collectionId: 'customCollection',
            },
          ],
        };
        expect(
          getTimeSeriesPreset({
            ...mockStore,
            timeSeries: {
              ...mockStore.timeSeries,
              plotPreset,
              services: [...services, ...customServices],
            },
          }),
        ).toEqual({
          plotPreset,
          services: [...services, customServices[0]],
        });
      });
      it('should return parameters without instanceId (ref time)', () => {
        const plotPreset: PlotPreset = {
          ...mockStore.timeSeries!.plotPreset!,
          parameters: [
            parameter1,
            {
              ...parameter2,
              instanceId: '2000-01-01T00:00:00Z',
            },
          ],
        };
        expect(parameter2.instanceId).toBeUndefined();
        expect(
          getTimeSeriesPreset({
            ...mockStore,
            timeSeries: {
              ...mockStore.timeSeries,
              plotPreset,
            },
          }),
        ).toEqual({
          plotPreset: {
            ...plotPreset,
            parameters: [parameter1, parameter2],
          },
          services: mockStore.timeSeries!.services!,
        });
      });
      it('should return undefined if no state', () => {
        expect(getTimeSeriesPreset(emptyMockStore)).toEqual(undefined);
      });
    });
  });
});
