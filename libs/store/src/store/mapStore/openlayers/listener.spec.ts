/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Bbox } from '@opengeoweb/webmap';
import { mapActions } from '../../map/map';

import {
  clearSyncGroupViewStore,
  getViewObjectForMapId,
  getViewObjectForSyncGroup,
} from './syncGroupViewStore';
import { genericActions } from '../../generic';
import { WebMap, WebMapState } from '../../map/types';
import {
  SynchronizationGroupState,
  SynchronizationSource,
  SyncType,
} from '../../generic/syncGroups/types';
import { SYNCGROUPS_TYPE_SETBBOX } from '../../generic/syncGroups/constants';
import { createMockStore } from '../../store';

const bbox1 = {
  left: 0,
  bottom: 1,
  right: 2,
  top: 3,
};

const bbox2 = {
  left: 4,
  bottom: 5,
  right: 6,
  top: 7,
};

const mockGroupId = 'group-1';

const createMockSyncGroupStore = (
  mapIds: string[],
  bbox: Bbox,
): SynchronizationGroupState => {
  return {
    sources: {
      allIds: [...mapIds],
      byId: mapIds.reduce(
        (memo, mapId) => ({
          ...memo,
          [mapId]: {
            types: [SYNCGROUPS_TYPE_SETBBOX] as SyncType[],
            payloadByType: {
              SYNCGROUPS_TYPE_SETBBOX: {
                sourceId: mapIds[0],
                bbox: { ...bbox },
                srs: 'EPSG:3857',
              },
            },
          },
        }),
        {} as Record<string, SynchronizationSource>,
      ),
    },
    groups: {
      allIds: [mockGroupId],
      byId: {
        [mockGroupId]: {
          type: SYNCGROUPS_TYPE_SETBBOX as SyncType,
          payloadByType: {
            SYNCGROUPS_TYPE_SETBBOX: {
              sourceId: mapIds[0],
              bbox: { ...bbox },
              srs: 'EPSG:3857',
            },
          },
          targets: {
            allIds: [...mapIds],
            byId: mapIds.reduce(
              (memo, mapId) => ({
                ...memo,
                [mapId]: { linked: true },
              }),
              {} as Record<string, { linked: boolean }>,
            ),
          },
        },
      },
    },
    isTimeScrollingEnabled: true,
    linkedState: {
      links: {},
      sharedData: {},
    },
    viewState: {
      level: {
        groups: [],
        sourcesById: [],
      },
      timeslider: {
        groups: [],
        sourcesById: [],
      },
      zoompane: {
        groups: [],
        sourcesById: [],
      },
    },
  };
};

const createWebMapStore = (mapIds: string[], bbox: Bbox): WebMapState => ({
  allIds: [...mapIds],
  byId: mapIds.reduce(
    (memo, mapId) => ({
      ...memo,
      [mapId]: {
        id: mapId,
        bbox,
        srs: 'EPSG:3857',
      } as unknown as WebMap,
    }),
    {} as Record<string, WebMap>,
  ),
});

describe('store/mapStore/openlayers/listener', () => {
  beforeEach(() => {
    clearSyncGroupViewStore();
  });

  it('should set OpenLayers View center when setBbox is fired', () => {
    const mapId = 'map-x';

    const store = createMockStore({
      syncGroups: createMockSyncGroupStore([mapId], bbox1),
    });

    const viewObject = getViewObjectForMapId(mapId);
    expect(viewObject.view.getCenter()).toBeUndefined();
    store.dispatch(
      mapActions.setBbox({ bbox: bbox1, srs: 'EPSG:3857', mapId }),
    );

    expect(viewObject.view.getCenter()).toBeDefined();
    expect(viewObject.view.getCenter()).toEqual([1, 2]);

    store.dispatch(
      mapActions.setBbox({ bbox: bbox2, srs: 'EPSG:3857', mapId }),
    );
    expect(viewObject.view.getCenter()).toEqual([5, 6]);
  });

  it('should set OpenLayers View resolution when setBbox is fired', () => {
    const mapId = 'map-x';

    const myBbox = {
      top: 7438773.776232235,
      left: -450651.2255879827,
      right: 1428345.8183648037,
      bottom: 6490531.093143953,
    };

    const store = createMockStore({
      syncGroups: createMockSyncGroupStore([mapId], bbox1),
    });

    const viewObject = getViewObjectForMapId(mapId);
    expect(viewObject.view.getResolution()).toBeUndefined();
    store.dispatch(
      mapActions.setBbox({ bbox: myBbox, srs: 'EPSG:3857', mapId }),
    );

    expect(viewObject.view.getResolution()).toBeDefined();
    // Note: This is based on the hard coded fallback resolution 1024x1024 in listener.ts
    expect(viewObject.view.getResolution()).toBeCloseTo(1835, 1);
  });

  it('should only set OpenLayers View for that map when setBbox is fired', () => {
    const mapId1 = 'map-x';
    const mapId2 = 'map-y';

    const store = createMockStore({
      syncGroups: createMockSyncGroupStore([mapId1, mapId2], bbox1),
    });
    const viewObject1 = getViewObjectForMapId(mapId1);
    const viewObject2 = getViewObjectForMapId(mapId2);
    expect(viewObject1.view.getCenter()).toBeUndefined();
    expect(viewObject2.view.getCenter()).toBeUndefined();

    store.dispatch(
      mapActions.setBbox({ bbox: bbox1, srs: 'EPSG:3857', mapId: mapId1 }),
    );

    expect(viewObject1.view.getCenter()).toBeDefined();
    expect(viewObject1.view.getCenter()).toEqual([1, 2]);
    expect(viewObject2.view.getCenter()).toBeUndefined();
  });

  it('should never set OpenLayers Group View locations when the generic setbbox is fired', () => {
    const mapId1 = 'map-x';
    const mapId2 = 'map-y';
    const store = createMockStore({
      syncGroups: createMockSyncGroupStore([mapId1, mapId2], bbox1),
      webmap: createWebMapStore([mapId1, mapId2], bbox1),
    });

    const groupViewObject = getViewObjectForSyncGroup(mockGroupId);
    expect(groupViewObject.view.getCenter()).toBeUndefined();
    expect(store.getState().webmap.byId[mapId1].bbox).toEqual(bbox1);
    expect(store.getState().webmap.byId[mapId2].bbox).toEqual(bbox1);

    store.dispatch(
      genericActions.setBbox({
        bbox: bbox2,
        srs: 'EPSG:3857',
        sourceId: mapId1,
      }),
    );

    // OpenLayers Views are synchronized via the OpenLayers Map components, so the reducers + middleware should not touch the view
    expect(groupViewObject.view.getCenter()).toBeUndefined();
    // .. however the bounding box in the store should change for the maps in question
    expect(store.getState().webmap.byId[mapId1].bbox).toEqual(bbox2);
    expect(store.getState().webmap.byId[mapId2].bbox).toEqual(bbox2);
  });
});
