/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { View } from 'ol';
import { boundingExtent, getCenter } from 'ol/extent';

import { createListenerMiddleware } from '@reduxjs/toolkit';

import { setBboxSync } from '../../generic/synchronizationActions/actions';
import {
  copyView,
  getViewObjectForMapId,
  getViewObjectForSyncGroup,
} from './syncGroupViewStore';

import { syncConstants, syncGroupsSelectors } from '../../generic';

import { WebMapStateModuleState } from '../../map/types';
import { mapActions } from '../../map/map';
import {
  syncGroupAddGroup,
  syncGroupAddTarget,
} from '../../generic/syncGroups/reducer';

export const openlayersListener =
  createListenerMiddleware<WebMapStateModuleState>();

const getGroupForMap = (
  mapId: string,
  state: WebMapStateModuleState,
): string | undefined => {
  const syncGroupState =
    syncGroupsSelectors.getSynchronizationGroupState(state);

  return syncGroupState.groups.allIds.find((id) => {
    const group = syncGroupState.groups.byId[id];
    return (
      group.type === syncConstants.SYNCGROUPS_TYPE_SETBBOX &&
      group.targets.allIds.indexOf(mapId!) !== -1
    );
  });
};

// This is fired when the preset is set up and when the projection is changed
openlayersListener.startListening({
  actionCreator: mapActions.setBbox,
  effect: ({ payload }, listenerApi) => {
    const { mapId, bbox, srs } = payload;

    const extent = boundingExtent([
      [bbox.left, bbox.bottom],
      [bbox.right, bbox.top],
    ]);

    const storedViewForMap = getViewObjectForMapId(mapId);

    const view = new View({
      projection: srs,
      center: getCenter(extent),
    });

    storedViewForMap.setView(view);

    // 1024 is the current best guess for width / height of a map
    // Feature request made for OL to help us get this right https://github.com/openlayers/openlayers/issues/16572
    view.setViewportSize([1024, 1024]);
    const resolution = view.getResolutionForExtent(extent);
    view.setResolution(resolution);

    // Also: if this map is part of a group and the srs has changed => change group view
    const mapSyncGroup = getGroupForMap(mapId, listenerApi.getState());
    if (!mapSyncGroup) {
      return;
    }

    const groupView = getViewObjectForSyncGroup(mapSyncGroup);
    if (
      !groupView.view.isDef() ||
      groupView.view.getProjection().getCode() !== srs
    ) {
      groupView.setView(copyView(view));
    }
  },
});

// This is fired when the map view is updated (regardless of if the map is in a group or not)
// When a map is in a group, this needs to ensure the view of the map is kept up-to-date
openlayersListener.startListening({
  actionCreator: setBboxSync,
  effect: ({ payload }, listenerApi) => {
    const mapId = payload.source?.payload.sourceId!;
    const mapSyncGroup = getGroupForMap(mapId, listenerApi.getState());

    if (!mapSyncGroup) {
      return;
    }

    const groupView = getViewObjectForSyncGroup(mapSyncGroup);
    const mapView = getViewObjectForMapId(mapId);

    if (mapView.view.getProjection() === groupView.view.getProjection()) {
      mapView.view.setCenter(groupView.view.getCenter());
      mapView.view.setResolution(groupView.view.getResolution());
    } else {
      mapView.setView(copyView(groupView.view));
    }
  },
});

openlayersListener.startListening({
  actionCreator: syncGroupAddGroup,
  effect: ({ payload }) => {
    if (payload.type !== syncConstants.SYNCGROUPS_TYPE_SETBBOX) {
      return;
    }

    // Ensure there is an openlayers view for the sync group
    getViewObjectForSyncGroup(payload.groupId);
  },
});

// Ensure sync group view has contents: when a target is added to a group whose view is undefined, copy the view from the target
openlayersListener.startListening({
  actionCreator: syncGroupAddTarget,
  effect: ({ payload }, listenerApi) => {
    const group = syncGroupsSelectors.getSynchronizationGroup(
      listenerApi.getState(),
      payload.groupId,
    );
    if (!group || group.type !== syncConstants.SYNCGROUPS_TYPE_SETBBOX) {
      return;
    }

    const groupView = getViewObjectForSyncGroup(payload.groupId);
    if (groupView.view.isDef()) {
      return;
    }

    const mapView = getViewObjectForMapId(payload.targetId);
    groupView.setView(copyView(mapView.view));
  },
});
