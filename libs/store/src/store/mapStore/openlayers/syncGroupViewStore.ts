/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { Observable, View } from 'ol';

export const copyView = (view: View): View =>
  new View({
    center: view.getCenter(),
    resolution: view.getResolution(),
    projection: view.getProjection(),
  });

/**
 * The StoredView objects keep references to OpenLayers View objects that may be shared
 * between multiple maps (when maps are pan/zoom synced).
 * This file then offers and API for each map to retrieve the StoredView object
 * that is relevant for the current workspace configuration.
 *
 * React is functional, but OpenLayers is object oriented.
 * This component sits in between the two and thus you can make an argument for either style.
 * Object oriented was chosen since:
 * - This allows us to use OpenLayers's Observable as a base class to provide event handling,
 *   which is useful for the implementation.
 * - For this mechanism to work smoothly with OpenLayers,
 *   we need to pass the exact same View object reference for all maps that are synchronized. Redux stores cannot be used in this context and prop drilling and/or context providers would likely lead to more invasive implementations.
 */
export class StoredView extends Observable {
  name: string;
  view: View;

  constructor(name: string, view: View) {
    super();
    this.name = name;
    this.view = view;
  }

  setView(view: View): void {
    this.view = view;
    this.changed();
  }
}

const VIEW_PER_SYNCGROUP: Record<string, StoredView> = {};
const VIEW_PER_MAP: Record<string, StoredView> = {};

export const clearSyncGroupViewStore = (): void => {
  Object.keys(VIEW_PER_SYNCGROUP).forEach(
    (viewId) => delete VIEW_PER_SYNCGROUP[viewId],
  );
  Object.keys(VIEW_PER_MAP).forEach((viewId) => delete VIEW_PER_MAP[viewId]);
};

export const getViewObjectForMapId = (id: string): StoredView => {
  const storedView = VIEW_PER_MAP[id];
  if (!storedView) {
    const newStoredView = new StoredView(`view:${id}`, new View());
    VIEW_PER_MAP[id] = newStoredView;
    return newStoredView;
  }
  return storedView;
};

export const createViewObjectForNewMap = (
  createFromId: string,
  newId: string,
): void => {
  const orig = VIEW_PER_MAP[createFromId];
  if (!orig) {
    return;
  }

  // If there already is a view for `newId`, use that one but set the view from createFromId
  // this can happen for example when switching presets
  if (VIEW_PER_MAP[newId] !== undefined) {
    VIEW_PER_MAP[newId].setView(copyView(orig.view));
    return;
  }

  VIEW_PER_MAP[newId] = new StoredView(`view:${newId}`, copyView(orig.view));
};

export const getViewObjectForSyncGroup = (id: string): StoredView => {
  const storedView = VIEW_PER_SYNCGROUP[id];
  if (!storedView) {
    const newStoredView = new StoredView(`syncgroup:${id}`, new View());
    VIEW_PER_SYNCGROUP[id] = newStoredView;
    return newStoredView;
  }
  return storedView;
};
