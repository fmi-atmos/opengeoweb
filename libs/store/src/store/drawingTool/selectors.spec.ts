/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { LayerType } from '@opengeoweb/webmap';
import { DRAWMODE } from '@opengeoweb/webmap-react';
import { CoreAppStore } from '..';
import { initialState } from './reducer';
import {
  getDrawingtoolStore,
  selectDrawToolById,
  getDrawModeById,
  getActiveDrawModeId,
  getDrawToolGeoJSONLayerId,
  selectAllDrawingTools,
  getActiveDrawToolId,
  getGeoJSONIntersectionLayerId,
  getGeoJSONIntersectionBoundsLayerId,
  getShouldAllowMultipleShapes,
} from './selectors';
import { testGeoJSON } from './testUtils';

const drawingToolId = 'drawtool-1';
const drawingToolId2 = 'drawtool-2';
const geoJSONLayerId1 = 'drawlayer-drawtool-1';
const geoJSONLayerId2 = 'drawlayer-drawtool-2';
const geoJSONIntersectionLayerId = 'intersection-layer';
const geoJSONIntersectionBoundsLayerId = 'intersection-bounds-layer';

const state: CoreAppStore = {
  drawingtools: {
    ids: [drawingToolId, drawingToolId2],
    entities: {
      [drawingToolId]: {
        drawToolId: drawingToolId,
        geoJSONLayerId: geoJSONLayerId1,
        activeDrawModeId: '',
        drawModes: [
          {
            value: DRAWMODE.POINT,
            title: 'Point',
            shape: {
              type: 'Feature',
              properties: {
                fill: '#ff7800',
                'fill-opacity': 0.2,
                stroke: '#ff7800',
                'stroke-width': 2,
                'stroke-opacity': 1,
              },
              geometry: {
                type: 'Point',
                coordinates: [],
              },
            },
            isSelectable: true,
            drawModeId: 'drawtools-point',
            selectionType: 'point',
          },
          {
            value: DRAWMODE.POLYGON,
            title: 'Polygon',
            shape: {
              type: 'Feature',
              properties: {
                fill: '#ff7800',
                'fill-opacity': 0.2,
                stroke: '#ff7800',
                'stroke-width': 2,
                'stroke-opacity': 1,
              },
              geometry: {
                type: 'Polygon',
                coordinates: [[]],
              },
            },
            isSelectable: true,
            drawModeId: 'drawtools-polygon',
            selectionType: 'polygon',
          },

          {
            value: 'DELETE',
            title: 'Delete',
            shape: {
              type: 'FeatureCollection',
              features: [],
            },
            isSelectable: false,
            drawModeId: 'drawtools-delete',
            selectionType: 'delete',
          },
        ],
        shouldAllowMultipleShapes: false,
        geoJSONIntersectionLayerId,
        geoJSONIntersectionBoundsLayerId,
      },
      [drawingToolId2]: {
        drawToolId: drawingToolId2,
        geoJSONLayerId: geoJSONLayerId2,
        activeDrawModeId: 'drawtools-polygon',
        drawModes: [
          {
            value: DRAWMODE.POINT,
            title: 'Point',
            shape: {
              type: 'Feature',
              properties: {
                fill: '#ff7800',
                'fill-opacity': 0.2,
                stroke: '#ff7800',
                'stroke-width': 2,
                'stroke-opacity': 1,
              },
              geometry: {
                type: 'Point',
                coordinates: [],
              },
            },
            isSelectable: true,
            drawModeId: 'drawtools-point',
            selectionType: 'point',
          },
          {
            value: DRAWMODE.POLYGON,
            title: 'Polygon',
            shape: {
              type: 'Feature',
              properties: {
                fill: '#ff7800',
                'fill-opacity': 0.2,
                stroke: '#ff7800',
                'stroke-width': 2,
                'stroke-opacity': 1,
              },
              geometry: {
                type: 'Polygon',
                coordinates: [[]],
              },
            },
            isSelectable: true,
            drawModeId: 'drawtools-polygon',
            selectionType: 'polygon',
          },

          {
            value: 'DELETE',
            title: 'Delete',
            shape: {
              type: 'FeatureCollection',
              features: [],
            },
            isSelectable: false,
            drawModeId: 'drawtools-delete',
            selectionType: 'delete',
          },
        ],
        shouldAllowMultipleShapes: true,
      },
    },
  },
  layers: {
    byId: {
      [geoJSONLayerId1]: {
        geojson: testGeoJSON,
        layerType: LayerType.featureLayer,
        id: geoJSONLayerId1,
      },
      [geoJSONLayerId2]: {
        geojson: testGeoJSON,
        layerType: LayerType.featureLayer,
        id: geoJSONLayerId2,
      },
      'other-id-with-geojson': {
        geojson: testGeoJSON,
        layerType: LayerType.featureLayer,
        id: 'other-id-with-geojson',
      },
    },
    allIds: [geoJSONLayerId1, geoJSONLayerId2, 'other-id-with-geojson'],
    availableBaseLayers: {
      byId: {},
      allIds: [],
    },
  },
};

describe('Redux Selectors', () => {
  describe('getDrawingtoolStore', () => {
    it('should return the drawingtools state', () => {
      const store: CoreAppStore = {
        drawingtools: initialState,
      };

      const result = getDrawingtoolStore(store);
      expect(result).toEqual(store.drawingtools);
    });

    it('should return null if drawingtools state is not present', () => {
      const store = {};

      const result = getDrawingtoolStore(store);
      expect(result).toBeNull();
    });
  });

  describe('getActiveDrawToolId', () => {
    it('should return the active draw mode', () => {
      const result = getActiveDrawToolId(state);
      expect(result).toEqual(drawingToolId2);

      const result2 = getActiveDrawToolId({
        drawingtools: {
          ids: [drawingToolId, drawingToolId2],
          entities: {
            [drawingToolId]: {
              drawToolId: drawingToolId,
              geoJSONLayerId: geoJSONLayerId1,
              activeDrawModeId: 'drawtools-polygon',
              drawModes: [],
              shouldAllowMultipleShapes: false,
            },
            [drawingToolId2]: {
              drawToolId: drawingToolId2,
              geoJSONLayerId: geoJSONLayerId2,
              activeDrawModeId: '',
              drawModes: [],
              shouldAllowMultipleShapes: false,
            },
          },
        },
      });
      expect(result2).toEqual(drawingToolId);
    });

    it('should return an empty string if no drawtool exist or is active', () => {
      const result = getActiveDrawToolId({});
      expect(result).toEqual('');

      expect(
        getActiveDrawToolId({
          drawingtools: {
            ids: [drawingToolId, drawingToolId2],
            entities: {
              [drawingToolId]: {
                drawToolId: drawingToolId,
                geoJSONLayerId: geoJSONLayerId1,
                activeDrawModeId: '',
                drawModes: [],
                shouldAllowMultipleShapes: false,
              },
              [drawingToolId2]: {
                drawToolId: drawingToolId2,
                geoJSONLayerId: geoJSONLayerId2,
                activeDrawModeId: '',
                drawModes: [],
                shouldAllowMultipleShapes: false,
              },
            },
          },
        }),
      ).toEqual('');
    });
  });

  describe('selectDrawToolById', () => {
    it('should select draw by draw ID', () => {
      const result = selectDrawToolById(state, drawingToolId);
      expect(result).toEqual(state.drawingtools!.entities[drawingToolId]);
    });

    it('should return an empty object if drawToolId is not found', () => {
      const result = selectDrawToolById(state, 'tool-3');
      expect(result).toEqual(undefined);
    });
  });

  describe('selectAllDrawingTools', () => {
    it('should select all drawing tools', () => {
      const result = selectAllDrawingTools(state);
      expect(result).toEqual(
        state.drawingtools!.ids.map((id) => state.drawingtools!.entities[id]),
      );
    });

    it('should return empty list if no drawing tools in store', () => {
      const result = selectAllDrawingTools({});
      expect(result).toEqual([]);
    });
  });

  describe('getDrawModeById', () => {
    it('should return the draw mode by ID', () => {
      expect(getDrawModeById(state, drawingToolId, 'drawtools-point')).toEqual(
        state.drawingtools?.entities[drawingToolId]?.drawModes[0],
      );
    });

    it('should return undefined if draw tool is not found', () => {
      const result = getDrawModeById(state, 'drawingToolId', 'drawtools-point');
      expect(result).toBeUndefined();
    });

    it('should return undefined if draw mode is not found', () => {
      const result = getDrawModeById(state, drawingToolId, 'non exist');
      expect(result).toBeUndefined();
    });
  });

  describe('getActiveDrawModeId', () => {
    it('should return the active draw mode ID', () => {
      expect(getActiveDrawModeId(state, drawingToolId)).toEqual('');
      expect(getActiveDrawModeId(state, drawingToolId2)).toEqual(
        'drawtools-polygon',
      );
    });

    it('should return empty string if draw tool is not found', () => {
      expect(getActiveDrawModeId(state, 'drawingToolId')).toEqual('');
    });
  });
  describe('getDrawToolGeoJSONLayerId', () => {
    it('should return the geoJSON layer id', () => {
      expect(getDrawToolGeoJSONLayerId(state, drawingToolId)).toEqual(
        geoJSONLayerId1,
      );
      expect(getDrawToolGeoJSONLayerId(state, drawingToolId2)).toEqual(
        geoJSONLayerId2,
      );
    });

    it('should return empty string if draw tool is not found', () => {
      expect(getDrawToolGeoJSONLayerId(state, 'drawingToolId')).toEqual('');
    });
  });

  describe('getGeoJSONIntersectionLayerId', () => {
    it('should return the geoJSONIntersectionLayerId layer id', () => {
      expect(getGeoJSONIntersectionLayerId(state, drawingToolId)).toEqual(
        geoJSONIntersectionLayerId,
      );
      expect(
        getGeoJSONIntersectionLayerId(state, drawingToolId2),
      ).toBeUndefined();
    });

    it('should return undefined if draw tool is not found', () => {
      expect(
        getGeoJSONIntersectionLayerId(state, 'drawingToolId'),
      ).toBeUndefined();
    });
  });

  describe('getGeoJSONIntersectionBoundsLayerId', () => {
    it('should return the geoJSONIntersectionBoundsLayerId layer id', () => {
      expect(getGeoJSONIntersectionBoundsLayerId(state, drawingToolId)).toEqual(
        geoJSONIntersectionBoundsLayerId,
      );
      expect(
        getGeoJSONIntersectionBoundsLayerId(state, drawingToolId2),
      ).toBeUndefined();
    });

    it('should return undefined if draw tool is not found', () => {
      expect(
        getGeoJSONIntersectionBoundsLayerId(state, 'drawingToolId'),
      ).toBeUndefined();
    });
  });

  describe('getShouldAllowMultipleShapes', () => {
    it('should return the shouldAllowMultipleShapes layer id', () => {
      expect(getShouldAllowMultipleShapes(state, drawingToolId)).toBeFalsy();
      expect(getShouldAllowMultipleShapes(state, drawingToolId2)).toBeTruthy();
    });

    it('should return false if draw tool is not found', () => {
      expect(getShouldAllowMultipleShapes(state, 'drawingToolId')).toBeFalsy();
    });
  });
});
