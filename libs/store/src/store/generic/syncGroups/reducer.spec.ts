/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { AnyAction } from '@reduxjs/toolkit';
import { PROJECTION } from '@opengeoweb/shared';
import { syncGroupsReducer, initialState, syncGroupsActions } from './reducer';
import { SYNCGROUPS_TYPE_SETBBOX, SYNCGROUPS_TYPE_SETTIME } from './constants';
import { SetBboxPayload, SetTimePayload } from '../types';
import { createSyncGroupMockState } from './__mocks__/mockState';
import * as synchronizationGroupsSelector from './selectors';
import { setBboxSync, setTimeSync } from '../synchronizationActions/actions';

import {
  SetBboxSyncPayload,
  SetTimeSyncPayload,
} from '../synchronizationActions/types';
import { genericActions } from '..';

describe('store/generic/synchronizationGroups/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    expect(syncGroupsReducer(undefined, {} as AnyAction)).toEqual(initialState);
  });

  describe('syncGroupAddGroup', () => {
    it('should add a synchronization group for time with name group1', () => {
      const result = syncGroupsReducer(
        undefined,
        syncGroupsActions.syncGroupAddGroup({
          groupId: 'newGroup',
          title: 'SynchronizationGroup A for time',
          type: SYNCGROUPS_TYPE_SETTIME,
        }),
      );

      expect(result.groups.byId.newGroup).toBeDefined();

      expect(result.groups.byId.newGroup).toEqual({
        title: 'SynchronizationGroup A for time',
        type: SYNCGROUPS_TYPE_SETTIME,
        payloadByType: {
          SYNCGROUPS_TYPE_SETBBOX: null,
          SYNCGROUPS_TYPE_SETTIME: null,
        },
        targets: {
          allIds: [],
          byId: {},
        },
      });

      expect(result.groups.allIds).toContain('newGroup');
    });
    it('should do nothing when a synchronization group already exists', () => {
      const prevResult = syncGroupsReducer(
        undefined,
        syncGroupsActions.syncGroupAddGroup({
          groupId: 'newGroup',
          title: 'SynchronizationGroup A for time',
          type: SYNCGROUPS_TYPE_SETTIME,
        }),
      );
      const result = syncGroupsReducer(
        prevResult,
        syncGroupsActions.syncGroupAddGroup({
          groupId: 'newGroup',
          title: 'SynchronizationGroup A for time',
          type: SYNCGROUPS_TYPE_SETTIME,
        }),
      );
      expect(result.groups.byId.newGroup).toBeDefined();
    });
  });

  describe('syncGroupRemoveGroup', () => {
    it('should remove synchronization group with name group1', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupRemoveGroup({
          groupId: 'group1',
        }),
      );
      expect(result.groups.byId).not.toContain('group1');
      expect(result.groups.allIds).not.toContain('group1');
    });
    it('should not crash but warn when a non existinsynchronization group is removed', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupRemoveGroup({
          groupId: 'NonExistingGroup',
        }),
      );
      expect(result.groups.allIds).toContain('group1');
    });
  });

  describe('syncGroupAddSource', () => {
    it('should add a new source component to the sources list', () => {
      const result = syncGroupsReducer(
        undefined,
        syncGroupsActions.syncGroupAddSource({
          id: 'newSource',
          type: [SYNCGROUPS_TYPE_SETTIME],
        }),
      );

      expect(result.sources.byId.newSource).toBeDefined();
      expect(result.sources.byId.newSource).toEqual({
        types: [SYNCGROUPS_TYPE_SETTIME],
        payloadByType: {
          SYNCGROUPS_TYPE_SETTIME: undefined,
        },
      });
      expect(result.sources.allIds).toContain('newSource');
    });
    it('should not add a source to the sources list if it is already there', () => {
      const prevResult = syncGroupsReducer(
        createSyncGroupMockState(),
        {} as AnyAction,
      );

      expect(prevResult.sources.byId.mapA).toBeDefined();
      expect(prevResult.sources.allIds).toContain('mapA');

      const result = syncGroupsReducer(
        prevResult,
        syncGroupsActions.syncGroupAddSource({
          id: 'mapA',
          type: [SYNCGROUPS_TYPE_SETTIME],
        }),
      );

      expect(result.sources.byId.mapA).toBeDefined();
      expect(result.sources.allIds).toContain('mapA');
    });
  });

  describe('syncGroupRemoveSource', () => {
    it('should remove source from sync group', () => {
      const prevState = syncGroupsReducer(
        createSyncGroupMockState(),
        {} as AnyAction,
      );
      /* Check mapA, mapB and mapC in sources list */
      expect(prevState.sources.byId.mapA).toBeDefined();
      expect(prevState.sources.byId.mapB).toBeDefined();
      expect(prevState.sources.byId.mapC).toBeDefined();
      /* Check mapA, mapB and mapC in synchronization group TimeA */
      expect(prevState.groups.byId.TimeA.targets.byId.mapA).toBeDefined();
      expect(prevState.groups.byId.TimeA.targets.allIds).toContain('mapA');
      expect(prevState.groups.byId.TimeA.targets.byId.mapB).toBeDefined();
      expect(prevState.groups.byId.TimeA.targets.allIds).toContain('mapB');
      expect(prevState.groups.byId.TimeA.targets.byId.mapC).toBeDefined();
      expect(prevState.groups.byId.TimeA.targets.allIds).toContain('mapC');
      /* Check mapA, mapB and maCB in synchronization group AreaA */
      expect(prevState.groups.byId.AreaA.targets.byId.mapA).toBeDefined();
      expect(prevState.groups.byId.AreaA.targets.allIds).toContain('mapA');
      expect(prevState.groups.byId.AreaA.targets.byId.mapB).toBeDefined();
      expect(prevState.groups.byId.AreaA.targets.allIds).toContain('mapB');
      expect(prevState.groups.byId.AreaA.targets.byId.mapC).toBeDefined();
      expect(prevState.groups.byId.AreaA.targets.allIds).toContain('mapC');
      const resultA = syncGroupsReducer(
        prevState,
        syncGroupsActions.syncGroupRemoveSource({
          id: 'mapB',
        }),
      );
      /* Extra check to see if other maps (mapC in this case) is not affected (found by Tineke) */
      const result = syncGroupsReducer(
        resultA,
        syncGroupsActions.syncGroupRemoveSource({
          id: 'mapD',
        }),
      );
      /* Check  mapA, mapB and mapC in sources list */
      expect(result.sources.byId.mapA).toBeDefined();
      expect(result.sources.byId.mapB).not.toBeDefined();
      expect(result.sources.byId.mapC).toBeDefined();
      /* Check  mapA, mapB and mapC in synchronization group TimeA */
      expect(result.groups.byId.TimeA.targets.byId.mapA).toBeDefined();
      expect(result.groups.byId.TimeA.targets.allIds).toContain('mapA');
      expect(
        result.groups.byId.TimeA.targets.byId.mapB,
      ).toBeDefined(); /* Issue found #1309, the source should not be removed from the group, because it might be added back later and should then preserve the same settings */
      expect(result.groups.byId.TimeA.targets.allIds).toContain('mapB');
      expect(result.groups.byId.TimeA.targets.byId.mapC).toBeDefined();
      expect(result.groups.byId.TimeA.targets.allIds).toContain('mapC');
      /* Check  mapA, mapB and mapC in synchronization group AreaA */
      expect(result.groups.byId.AreaA.targets.byId.mapA).toBeDefined();
      expect(result.groups.byId.AreaA.targets.allIds).toContain('mapA');
      expect(result.groups.byId.AreaA.targets.byId.mapB).toBeDefined();
      expect(result.groups.byId.AreaA.targets.allIds).toContain('mapB');
      expect(result.groups.byId.AreaA.targets.byId.mapC).toBeDefined();
      expect(result.groups.byId.AreaA.targets.allIds).toContain('mapC');
    });
    it('should not doing anything if source is not there', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupRemoveSource({
          id: 'mapWhichDoesNotExist',
        }),
      );
      expect(result.sources.byId.mapA).toBeDefined();
    });
  });

  describe('syncGroupAddTarget', () => {
    it('should add a target component to a synchronization group', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupAddTarget({
          groupId: 'group2',
          targetId: 'mapA',
        }),
      );

      expect(result.groups.byId.group2.targets.byId.mapA).toBeDefined();
      expect(result.groups.byId.group2.targets.allIds).toContain('mapA');
      expect(result.groups.byId.group2.targets.byId.mapA.linked).toBeTruthy();
      expect(result.groups.byId.group2.targets.byId).toEqual({
        mapA: {
          linked: true,
        },
      });
    });
    it('should not do anything but warn when the target component in the synchronization group already exists', () => {
      const prevResult = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupAddTarget({
          groupId: 'group2',
          targetId: 'mapA',
        }),
      );

      const result = syncGroupsReducer(
        prevResult,
        syncGroupsActions.syncGroupAddTarget({
          groupId: 'group2',
          targetId: 'mapA',
        }),
      );

      expect(result.groups.byId.group2.targets.byId.mapA).toBeDefined();
      expect(result.groups.byId.group2.targets.allIds).toContain('mapA');
      expect(result.groups.byId.group2.targets.byId.mapA.linked).toBeTruthy();
      expect(result.groups.byId.group2.targets.byId).toEqual({
        mapA: {
          linked: true,
        },
      });
    });
    it('should not crash if a not existing group is referenced', () => {
      jest.spyOn(console, 'warn').mockImplementation();
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupAddTarget({
          groupId: 'groupWhichDoesNotExist',
          targetId: 'mapA',
        }),
      );

      expect(result.groups.byId.group2.targets.byId.mapA).not.toBeDefined();
      expect(console.warn).toHaveBeenCalledWith(
        expect.stringContaining(
          'SYNCGROUPS_ADD_TARGET: Group groupWhichDoesNotExist does not exist.',
        ),
      );
    });

    it('should not crash if a non existing target in an existing group is referenced', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupAddTarget({
          groupId: 'group2',
          targetId: 'mapWhichDoesNotExist',
        }),
      );

      expect(result.groups.byId.group2.targets.byId.mapA).not.toBeDefined();
    });
  });
  describe('syncGroupRemoveTarget', () => {
    it('should remove a target component from a synchronization group', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupRemoveTarget({
          groupId: 'AreaA',
          targetId: 'mapB',
        }),
      );
      /* Sources should not be preserved */
      expect(result.sources.byId.mapA).toBeDefined();
      expect(result.sources.byId.mapB).toBeDefined();
      /* Check mapA and mapB in synchronization group TimeA, should not be affected since we are targeting AreaA */
      expect(result.groups.byId.TimeA.targets.byId.mapA).toBeDefined();
      expect(result.groups.byId.TimeA.targets.allIds).toContain('mapA');
      expect(result.groups.byId.TimeA.targets.byId.mapB).toBeDefined();
      expect(result.groups.byId.TimeA.targets.allIds).toContain('mapB');
      /* Check mapA and mapB in synchronization group AreaA, mapA should not be removed */
      expect(result.groups.byId.AreaA.targets.byId.mapA).toBeDefined();
      expect(result.groups.byId.AreaA.targets.allIds).toContain('mapA');
      /* Check if targets are really removed from synchronization group AreaA */
      expect(result.groups.byId.AreaA.targets.byId.mapB).not.toBeDefined();
      expect(result.groups.byId.AreaA.targets.allIds).not.toContain('mapB');
    });

    it('should not crash when a existing target from a non existing synchronization group is removed', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupRemoveTarget({
          groupId: 'GroupWhichDoesNotExist',
          targetId: 'mapB',
        }),
      );
      expect(result.sources.byId.mapA).toBeDefined();
      expect(result.sources.byId.mapB).toBeDefined();
    });

    it('should not crash when a non existing target from a synchronization group is removed', () => {
      const prevResult = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupRemoveTarget({
          groupId: 'AreaA',
          targetId: 'mapB',
        }),
      );
      const result = syncGroupsReducer(
        prevResult,
        syncGroupsActions.syncGroupRemoveTarget({
          groupId: 'AreaA',
          targetId: 'mapB',
        }),
      );
      expect(result.sources.byId.mapA).toBeDefined();
      expect(result.sources.byId.mapB).toBeDefined();
    });
  });

  describe('syncGroupLinkTargetDisable', () => {
    it('should disable link a target to a synchronization group', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupLinkTarget({
          groupId: 'AreaA',
          targetId: 'mapB',
          linked: false,
        }),
      );
      /* Sources should not be preserved */
      expect(
        result.groups.byId.AreaA.targets.byId.mapB.linked,
      ).not.toBeTruthy();
    });
    it('should not crash when a non existing group is given', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupLinkTarget({
          groupId: 'ThisGroupDoesNotExist',
          targetId: 'mapB',
          linked: false,
        }),
      );
      /* Sources should not be preserved */
      expect(result.groups.byId.AreaA.targets.byId.mapB.linked).toBeTruthy();
    });
    it('should not crash when a non existing targetId in an existing group is given', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupLinkTarget({
          groupId: 'AreaA',
          targetId: 'ThisTargetDoesNotExist',
          linked: false,
        }),
      );
      /* Sources should not be preserved */
      expect(result.groups.byId.AreaA.targets.byId.mapB.linked).toBeTruthy();
    });
  });
  describe('syncGroupLinkTargetEnable', () => {
    it('should enable link a target to a synchronization group', () => {
      const result = syncGroupsReducer(
        createSyncGroupMockState(),
        syncGroupsActions.syncGroupLinkTarget({
          groupId: 'AreaA',
          targetId: 'mapB',
          linked: true,
        }),
      );
      /* Sources should not be preserved */
      expect(result.groups.byId.AreaA.targets.byId.mapB.linked).toBeTruthy();
    });
  });

  describe('syncGroupTriggerSetBBox', () => {
    it('should update targets in a synchronization group for a BBOX update', () => {
      const initState = createSyncGroupMockState();
      const payload = {
        sourceId: 'mapB',
        bbox: {
          left: -10,
          bottom: -10,
          right: 10,
          top: 10,
        },
        srs: PROJECTION.EPSG_4325.value,
      } as SetBboxPayload;

      // prepare action with a selector for testing purposes
      const targets = synchronizationGroupsSelector.getTargets(
        { syncGroups: initState },
        payload,
        SYNCGROUPS_TYPE_SETBBOX,
      );

      const result = syncGroupsReducer(
        initState,
        setBboxSync(payload, targets as SetBboxSyncPayload[], ['AreaA']),
      );
      /* Sources should not be preserved */
      expect(
        result.sources.byId.mapA.payloadByType[SYNCGROUPS_TYPE_SETBBOX]
          .sourceId,
      ).toEqual('mapB');
      expect(
        (
          result.sources.byId.mapA.payloadByType[
            SYNCGROUPS_TYPE_SETBBOX
          ] as SetBboxPayload
        ).bbox,
      ).toEqual({
        left: -10,
        bottom: -10,
        top: 10,
        right: 10,
      });
    });
  });

  describe('syncGroupTriggerSetTime', () => {
    it('should update targets in a synchronization group for a Time update', () => {
      const initState = createSyncGroupMockState();
      const payload = {
        sourceId: 'mapB',
        value: '2000-01-01T12:34:56Z',
      } as SetTimePayload;

      // prepare action with a selector for testing purposes
      const targets = synchronizationGroupsSelector.getTargets(
        { syncGroups: initState },
        payload,
        SYNCGROUPS_TYPE_SETTIME,
      );

      const result = syncGroupsReducer(
        initState,
        setTimeSync(payload, targets as SetTimeSyncPayload[], ['TimeA']),
      );
      /* Sources should not be preserved */
      expect(
        result.sources.byId.mapA.payloadByType[SYNCGROUPS_TYPE_SETTIME]
          .sourceId,
      ).toEqual('mapB');
      expect(
        (
          result.sources.byId.mapA.payloadByType[
            SYNCGROUPS_TYPE_SETTIME
          ] as SetTimePayload
        ).value,
      ).toEqual('2000-01-01T12:34:56Z');
    });

    it('should not crash when an invalid source is given', () => {
      const initState = createSyncGroupMockState();
      const payload = {
        sourceId: 'NonExistingSource',
        value: '2000-01-01T12:34:56Z',
      } as SetTimePayload;

      // prepare action with a selector for testing purposes
      const targets = synchronizationGroupsSelector.getTargets(
        { syncGroups: initState },
        payload,
        SYNCGROUPS_TYPE_SETTIME,
      );

      const result = syncGroupsReducer(
        initState,
        setTimeSync(payload, targets as SetTimeSyncPayload[], ['TimeA']),
      );
      /* Sources should not be preserved */
      expect(result.sources.byId.mapA).toBeTruthy();
    });
  });
  describe('syncGroupTriggerSetBBox and syncGroupTriggerSetTime', () => {
    it('should update targets in a synchronization group for a BBOX update and TIME update', () => {
      const initState = createSyncGroupMockState();
      const payloadBBOX = {
        sourceId: 'mapB',
        bbox: {
          left: -20,
          bottom: -10,
          right: 10,
          top: 10,
        },
        srs: PROJECTION.EPSG_4325.value,
      } as SetBboxPayload;

      const payloadTime = {
        sourceId: 'mapB',
        value: '2100-01-01T12:34:56Z',
      } as SetTimePayload;

      // prepare action with a selector for testing purposes
      const targetsBBOX = synchronizationGroupsSelector.getTargets(
        { syncGroups: initState },
        payloadBBOX,
        SYNCGROUPS_TYPE_SETBBOX,
      );

      const targetsTime = synchronizationGroupsSelector.getTargets(
        { syncGroups: initState },
        payloadBBOX,
        SYNCGROUPS_TYPE_SETBBOX,
      );

      const firstResult = syncGroupsReducer(
        initState,
        setBboxSync(payloadBBOX, targetsBBOX as SetBboxSyncPayload[], [
          'AreaA',
        ]),
      );
      const result = syncGroupsReducer(
        firstResult,
        setTimeSync(payloadTime, targetsTime as SetTimeSyncPayload[], [
          'TimeA',
        ]),
      );
      /* Sources should not be preserved */
      expect(
        result.sources.byId.mapA.payloadByType[SYNCGROUPS_TYPE_SETBBOX]
          .sourceId,
      ).toEqual('mapB');

      expect(
        (
          result.sources.byId.mapA.payloadByType[
            SYNCGROUPS_TYPE_SETBBOX
          ] as SetBboxPayload
        ).bbox,
      ).toEqual({
        left: -20,
        bottom: -10,
        top: 10,
        right: 10,
      });

      expect(
        (
          result.sources.byId.mapA.payloadByType[
            SYNCGROUPS_TYPE_SETTIME
          ] as SetTimePayload
        ).value,
      ).toEqual('2100-01-01T12:34:56Z');
    });
  });
  describe('SyncGroupSetViewState', () => {
    it('should set a new viewState from action payload', () => {
      const initialState = {
        ...genericActions.initialSyncState,
      };

      const newViewState = {
        timeslider: {
          groups: [
            {
              id: 'SYNCGROUPS_TYPE_SETTIME_A',
              selected: [],
            },
          ],
          sourcesById: [
            {
              id: 'radarView',
              name: 'radarView',
            },
          ],
        },
        zoompane: {
          groups: [
            {
              id: 'SYNCGROUPS_TYPE_SETBBOX_A',
              selected: [],
            },
          ],
          sourcesById: [
            {
              id: 'radarView',
              name: 'radarView',
            },
          ],
        },
        level: {
          groups: [],
          sourcesById: [],
        },
      };

      expect(initialState.viewState.timeslider.groups.length).toBe(0);
      expect(initialState.viewState.timeslider.sourcesById.length).toBe(0);
      expect(initialState.viewState.zoompane.groups.length).toBe(0);
      expect(initialState.viewState.zoompane.sourcesById.length).toBe(0);

      const action = syncGroupsActions.syncGroupSetViewState({
        viewState: newViewState,
      });
      expect(syncGroupsReducer(initialState, action)).toEqual({
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: newViewState,
        linkedState: {
          links: {},
          sharedData: {},
        },
        isTimeScrollingEnabled: false,
      });
    });
  });
});
