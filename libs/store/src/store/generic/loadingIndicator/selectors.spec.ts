/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { produce } from 'immer';
import { CoreAppStore } from '../..';
import { ANY_EDR_SERVICE } from './constants';
import {
  isEDRLoading,
  isGetCapabilitiesLoading,
  isGetMapLoading,
  isLoading,
} from './selectors';

const loadingIndicatorTestStore: CoreAppStore = {
  loadingIndicator: {
    entities: {
      id_A: {
        id: 'id_A',
        isGetMapLoading: true,
        isGetCapabilitiesLoading: false,
        isEDRLoading: false,
      },
      any_edr: {
        id: ANY_EDR_SERVICE,
        isGetMapLoading: false,
        isGetCapabilitiesLoading: false,
        isEDRLoading: false,
      },
    },
    ids: ['id_A', ANY_EDR_SERVICE],
  },
};

describe('src/generic/loadingIndicator/selectors', () => {
  describe('getIsMapLoading', () => {
    it('should return true for id_A', () => {
      expect(isGetMapLoading(loadingIndicatorTestStore, 'id_A')).toEqual(true);
      expect(
        isGetCapabilitiesLoading(loadingIndicatorTestStore, 'id_A'),
      ).toEqual(false);
      expect(isLoading(loadingIndicatorTestStore, 'id_A')).toEqual(true);
    });
    it('should return false for non initialized store', () => {
      expect(isGetMapLoading(null!, 'does-not-exist')).toEqual(false);
    });
    it('should return false for non existing id', () => {
      expect(
        isGetMapLoading(loadingIndicatorTestStore, 'does-not-exist'),
      ).toEqual(false);
      expect(
        isGetCapabilitiesLoading(loadingIndicatorTestStore, 'does-not-exist'),
      ).toEqual(false);
      expect(isLoading(loadingIndicatorTestStore, 'does-not-exist')).toEqual(
        false,
      );
    });
  });
  describe('isAnyEDRLoading', () => {
    it('should return false for id_A', () => {
      expect(isEDRLoading(loadingIndicatorTestStore, 'id_A')).toEqual(false);
    });
    it('should return true for any EDR', () => {
      const storeWithAnyEdr = produce(loadingIndicatorTestStore, (draft) => {
        draft.loadingIndicator!.entities.any_edr!.isEDRLoading = true;
      });
      expect(isEDRLoading(storeWithAnyEdr, ANY_EDR_SERVICE)).toEqual(true);
      expect(isLoading(storeWithAnyEdr, 'id_A')).toEqual(true);
    });
  });
});
