/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { AnyAction } from '@reduxjs/toolkit';
import { serviceReducer, initialState, serviceActions } from './reducer';
import { ServiceState } from './types';

const layers = [
  {
    name: 'test',
    title: 'testtext',
    leaf: true,
    path: ['string'],
  },
  {
    name: 'test2',
    title: 'testtext2',
    leaf: true,
    path: ['string'],
  },
];

const createMockState = (): ServiceState => ({
  byId: {
    id_1: {
      serviceUrl: 'someURL',
      layers,
    },
    id_2: {
      serviceUrl: 'otherUrl',
      layers,
    },
  },
  allIds: ['id_1', 'id_2'],
});

describe('store/mapStore/service/reducer', () => {
  const serviceName = 'testName';
  const service = 'someURL';

  it('should return initial state if no state and action passed in', () => {
    expect(serviceReducer(undefined, {} as AnyAction)).toEqual(initialState);
  });

  describe('serviceSetLayers', () => {
    it('should set layers if no state passed', () => {
      const result = serviceReducer(
        undefined,
        serviceActions.serviceSetLayers({
          id: 'serviceid_1',
          name: serviceName,
          serviceUrl: service,
          layers,
          scope: 'system',
        }),
      );

      expect(result.byId['serviceid_1']).toEqual({
        id: 'serviceid_1',
        layers,
        name: serviceName,
        serviceUrl: service,
        scope: 'system',
      });
      expect(result.allIds).toEqual(['serviceid_1']);
    });

    it('should set layers if current state passed', () => {
      const newLayers = [
        {
          name: 'newLayerName',
          title: 'newTestText',
          leaf: true,
          path: ['somePath'],
        },
      ];

      const result = serviceReducer(
        createMockState(),
        serviceActions.serviceSetLayers({
          id: 'id_1',
          name: serviceName,
          serviceUrl: service,
          layers: newLayers,
          scope: 'system',
        }),
      );
      expect(result.byId['id_1']).toEqual({
        id: 'id_1',
        layers: newLayers,
        name: serviceName,
        serviceUrl: service,
        scope: 'system',
      });
      expect(result.allIds).toEqual(['id_1', 'id_2']);
    });
  });

  describe('mapStoreRemoveService', () => {
    it('should remove service', () => {
      const result = serviceReducer(
        createMockState(),
        serviceActions.mapStoreRemoveService({
          id: 'id_1',
          serviceUrl: service,
        }),
      );

      expect(result.allIds).toHaveLength(1);
      expect(result.allIds).toEqual(['id_2']);
      expect(result.byId['id_2']).toBeDefined();
      expect(result.byId['id_1']).toBeUndefined();
    });
  });
});
