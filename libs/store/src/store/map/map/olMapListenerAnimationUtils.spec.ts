/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  LayerType,
  mockGetCapabilities,
  registerWMLayer,
  setWMSGetCapabilitiesFetcher,
  WMLayer,
} from '@opengeoweb/webmap';
import {
  getCurrentStep,
  getNextStep,
  isNextStepAvailable,
  setStep,
} from './olMapListenerAnimationUtils';
import { ReduxLayer } from '../types';

describe('timerStepFunctions', () => {
  beforeEach(() => {
    setWMSGetCapabilitiesFetcher(
      mockGetCapabilities.mockGetCapabilitiesFetcher,
    );
  });
  afterEach(() => {
    setWMSGetCapabilitiesFetcher(null);
  });
  it('getCurrentStep should return the first step when not initialized', () => {
    expect(getCurrentStep('timerId-notexistingB')).toBe(0);
  });
  it('getNextStep should return the next step when not initialized', () => {
    expect(getNextStep('timerId-notexistingA', 10)).toBe(1);
  });
  it('getNextStep should return the next step when initialized', () => {
    setStep('timerIdA', 5);
    expect(getNextStep('timerIdA', 10)).toBe(6);
  });
  it('getNextStep should return the first step when it reached the end', () => {
    setStep('timerIdA', 9);
    expect(getNextStep('timerIdA', 10)).toBe(0);
  });
  it('getNextStep should return the first step when it reached the end multiple times', () => {
    setStep('timerIdA', 5);
    expect(getNextStep('timerIdA', 10, 102)).toBe(7);
  });
  it('getNextStep should return the previous step when it reached the beginning once', () => {
    setStep('timerIdA', 0);
    expect(getNextStep('timerIdA', 10, -1)).toBe(9);
  });
  it('getNextStep should return the previous step when it reached the beginning multiple times', () => {
    setStep('timerIdA', 5);
    expect(getNextStep('timerIdA', 10, -102)).toBe(3);
  });
});

describe('isNextStepAvailable', () => {
  it('should return true if it cannot determine anything', () => {
    expect(isNextStepAvailable([], '')).toBeTruthy();
  });

  it('should return true if no layers are provided', () => {
    expect(isNextStepAvailable([], '2020-01-01T00:00:00Z')).toBeTruthy();
  });
  it('should determine if the next step is available for one layer', () => {
    const dimensionLayer1 = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2017-01-01T00:25:00Z',
          values:
            '2017-01-01T00:25:00Z,2017-01-01T00:30:00Z,2017-01-01T00:35:00Z',
        },
      ],
    };
    const wmLayer = new WMLayer(dimensionLayer1);
    registerWMLayer(wmLayer, dimensionLayer1.id);
    const reduxLayers: ReduxLayer[] = [dimensionLayer1];
    const hasImageForTimeValue = jest.fn();
    const loadImageForTimeValue = jest.fn();
    hasImageForTimeValue.mockReturnValue(false);
    wmLayer.olSource = {
      hasImageForTimeValue,
      loadImageForTimeValue,
    };

    // Next step is outside range of the layer time dimension. Expect it not to trigger loading
    expect(
      isNextStepAvailable(reduxLayers, '2117-01-01T00:25:00Z'),
    ).toBeTruthy();
    expect(loadImageForTimeValue).not.toHaveBeenCalled();

    // Next step should not be available, expect it to trigger loading and return Falsy
    expect(
      isNextStepAvailable(reduxLayers, '2017-01-01T00:25:00Z'),
    ).toBeFalsy();
    expect(loadImageForTimeValue).toHaveBeenCalledWith('2017-01-01T00:25:00Z');
    loadImageForTimeValue.mockClear();

    // Next step should have the timestep available
    hasImageForTimeValue.mockReturnValue(true);

    expect(
      isNextStepAvailable(reduxLayers, '2017-01-01T00:25:00Z'),
    ).toBeTruthy();

    expect(loadImageForTimeValue).not.toHaveBeenCalled();
  });
});
