/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { TimeInterval } from '@opengeoweb/webmap';
import { MapLocation } from '@opengeoweb/webmap-react';
import { SpeedFactorType } from '@opengeoweb/timeslider';
import type { Layer, LayerActionOrigin } from '../types';
import { MapActionOrigin } from './enums';

export interface Dimension {
  name?: string;
  units?: string;
  currentValue: string;
  maxValue?: string;
  minValue?: string;
  timeInterval?: TimeInterval;
  synced?: boolean;
  validSyncSelection?: boolean;
  values?: string;
}

export type WebMapAnimationList = { name: string; value: string }[];
export interface WebMap {
  id: string;
  isAnimating: boolean;
  animationStartTime?: string;
  animationEndTime?: string;
  timeList?: WebMapAnimationList | undefined;
  isAutoUpdating: boolean;
  shouldEndtimeOverride: boolean;
  srs: string;
  bbox: Bbox;
  mapLayers: string[];
  baseLayers: string[];
  overLayers: string[];
  featureLayers: string[];
  dimensions?: Dimension[];
  autoUpdateLayerId?: string;
  autoTimeStepLayerId?: string;
  timeSliderSpan?: number;
  timeStep?: number;
  animationDelay?: number;
  timeSliderWidth?: number;
  timeSliderCenterTime?: number;
  timeSliderSecondsPerPx?: number;
  isTimestepAuto?: boolean;
  isTimeSpanAuto?: boolean;
  isAnimationLengthAuto?: boolean;
  isTimeSliderHoverOn?: boolean;
  isTimeSliderVisible?: boolean;
  shouldShowZoomControls?: boolean;
  mapPinLocation?: MapLocation;
  disableMapPin?: boolean;
  displayMapPin?: boolean;
  legendId?: string;
  dockedLayerManagerSize?: DockedLayerManagerSize;
  isTimeScrollingEnabled?: boolean;
  selectedMapIds?: string[];
}

export type DockedLayerManagerSize =
  | 'sizeSmall'
  | 'sizeMedium'
  | 'sizeLarge'
  | '';
export interface WebMapState {
  byId: Record<string, WebMap>;
  allIds: string[];
  defaultMapSettings?: MapPreset;
}

export interface Bbox {
  left: number;
  right: number;
  top: number;
  bottom: number;
}

export enum AnimationLength {
  Minutes15 = 15,
  Minutes30 = 30,
  Hours1 = 60,
  Hours2 = 2 * 60,
  Hours3 = 3 * 60,
  Hours6 = 6 * 60,
  Hours12 = 12 * 60,
  Hours24 = 24 * 60,
}

export interface AnimationPayloadType {
  duration?: number; // Defined in minutes
  interval?: number; // Defined in minutes, note that if you set an interval, the auto timestep is set to false
  speed?: SpeedFactorType; // Defined as one of the animation speed options to be chosen via the UI [0.1, 0.2, 0.5, 1, 2, 4, 8, 16]
  endTime?: string; // Defined in NOW+PTxHyM/NOW-PTxHyM (NOW = current time utc, x = hours and y = minutes) or TODAY+PTxHyM/TODAY-PTxHyM (TODAY = current date at 00:00 utc)
  shouldEndtimeOverride?: boolean; // Defined as true/false
}

export interface MapPreset {
  layers?: Layer[];
  activeLayerId?: string;
  autoTimeStepLayerId?: string;
  autoUpdateLayerId?: string;
  proj?: {
    bbox: Bbox;
    srs: string;
  };
  dimensions?: Dimension[];
  shouldAnimate?: boolean;
  shouldAutoUpdate?: boolean;
  showTimeSlider?: boolean;
  displayMapPin?: boolean;
  shouldShowZoomControls?: boolean;
  toggleTimestepAuto?: boolean;
  animationPayload?: AnimationPayloadType;
  shouldShowLegend?: boolean;
  shouldShowLayerManager?: boolean;
  shouldShowDockedLayerManager?: boolean;
  dockedLayerManagerSize?: DockedLayerManagerSize;
  selectedMapIds?: string[];
  timeSliderSpan?: number;
  toggleTimeSpanAuto?: boolean;
}

export interface MapPresetInitialProps {
  mapPreset: MapPreset;
  syncGroupsIds?: string[];
}

// Layer actions

export interface MoveLayerPayload {
  mapId: string;
  oldIndex: number;
  newIndex: number;
  origin: string;
}

export interface TimeListType {
  name: string;
  value: string;
}

export interface SetMapAnimationStartPayload {
  mapId: string;
  start?: string;
  initialTime?: string;
  end?: string;
  interval?: number; // in seconds
  timeList?: TimeListType[];
  origin?: MapActionOrigin;
}

export interface SetMapAnimationStopPayload {
  mapId: string;
  origin?: MapActionOrigin;
}

export interface SetAutoLayerIdPayload {
  mapId: string;
  layerId?: string;
  autoUpdateLayerId?: string;
  autoTimeStepLayerId?: string;
  origin?: LayerActionOrigin | string;
}

export interface ToggleAutoUpdatePayload {
  mapId: string;
  shouldAutoUpdate: boolean;
  origin?: MapActionOrigin;
}
export interface SetEndTimeOverriding {
  mapId: string;
  shouldEndtimeOverride: boolean;
}

export interface SetTimeSliderSpanPayload {
  mapId: string;
  timeSliderSpan: number;
  origin?: MapActionOrigin;
}

export interface SetTimeStepPayload {
  mapId: string;
  timeStep?: number;
  origin?: MapActionOrigin;
}

export interface SetAnimationDelayPayload {
  mapId: string;
  animationDelay: number;
  origin?: MapActionOrigin;
}

export interface SetAnimationStartTimePayload {
  mapId: string;
  animationStartTime: string;
  origin?: MapActionOrigin;
}

export interface SetAnimationEndTimePayload {
  mapId: string;
  animationEndTime: string;
  origin?: MapActionOrigin;
}

export interface ToggleTimestepAutoPayload {
  mapId: string;
  timestepAuto: boolean;
  origin?: MapActionOrigin;
}

export interface ToggleTimeSpanAutoPayload {
  mapId: string;
  timeSpanAuto: boolean;
  origin?: MapActionOrigin;
}

export interface ToggleAnimationLengthAutoPayload {
  mapId: string;
  autoAnimationLength: boolean;
  origin?: MapActionOrigin;
}

export interface ToggleTimeSliderHoverPayload {
  mapId: string;
  isTimeSliderHoverOn: boolean;
}

export interface ToggleTimeSliderIsVisiblePayload {
  mapId: string;
  isTimeSliderVisible: boolean;
  origin?: MapActionOrigin;
}

export interface SetTimeSliderWidthPayload {
  mapId: string;
  timeSliderWidth: number;
}
export interface SetTimeSliderCenterTimePayload {
  mapId: string;
  timeSliderCenterTime: number;
}

export interface SetTimeSliderSecondsPerPxPayload {
  mapId: string;
  timeSliderSecondsPerPx: number;
}

export interface ToggleZoomControlsPayload {
  mapId: string;
  shouldShowZoomControls: boolean;
}

export interface DisableMapPinPayload {
  mapId: string;
  disableMapPin: boolean;
}

export interface ToggleMapPinIsVisiblePayload {
  mapId: string;
  displayMapPin: boolean;
}

export interface SetMapPresetPayload {
  mapId: string;
  initialProps: MapPresetInitialProps;
}

export interface SetDockedLayerManagerSize {
  mapId: string;
  dockedLayerManagerSize: DockedLayerManagerSize;
}

export interface ISO8601Interval {
  startTime: string;
  endTime: string;
  duration: string;
}

export interface SetDefaultMapSettingsPayload {
  preset: MapPreset;
}

export interface TimeSliderStartCenterEndTime {
  start: number;
  center: number;
  end: number;
  step: number;
}
