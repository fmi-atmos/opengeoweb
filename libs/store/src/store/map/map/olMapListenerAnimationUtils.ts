/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  getWMLayerById,
  WMInvalidDateValues,
  LayerType,
} from '@opengeoweb/webmap';

import { metronome } from '@opengeoweb/metronome';
import { TimeawareImageSource } from '@opengeoweb/webmap-react';
import { Dispatch, ListenerEffectAPI } from '@reduxjs/toolkit';
import { ReduxLayer } from '../types';
import { CoreAppStore } from '../../types';
import { mapSelectors } from '.';
import { setTimeSync } from '../../generic/synchronizationActions/actions';
import { SetTimeSyncPayload } from '../../generic/synchronizationActions/types';

import * as synchronizationGroupsSelector from '../../generic/syncGroups/selectors';
import { layerSelectors } from '../layer';

import { SYNCGROUPS_TYPE_SETTIME } from '../../generic/syncGroups/constants';

/* A map with all the timerIds and their current step */
const stepMap = new Map<string, number>();
/* A map with a list of timers and their dwell */
const timerDwellMap = new Map<string, number>();

/**
 * Returns the next step for given timerId.
 * @param timerId The timer id
 * @param numberOfStepsInAnimation Animation length in steps
 * @param numStepsToGoForward Amount of steps to go forwards, defaults to 1. Can be positive and negative
 * @returns
 */
export const getNextStep = (
  timerId: string,
  numberOfStepsInAnimation: number,
  numStepsToGoForward = 1,
): number => {
  const currentStep = getCurrentStep(timerId);
  const nextStep = currentStep + numStepsToGoForward;
  const nextStepClipped =
    ((nextStep % numberOfStepsInAnimation) + numberOfStepsInAnimation) %
    numberOfStepsInAnimation;
  return nextStepClipped;
};

/**
 * Handles dwell at the end of the animation loop sequence. The number of steps to wait till proceed can be set by the dwell parameter.
 * @param timerId The timer id
 * @param numberOfStepsInAnimation Number of steps in the animation
 * @param dwell The number of steps to wait at the end of the animation sequence before to continue
 * @returns
 */
export const handleTimerDwell = (
  timerId: string,
  numberOfStepsInAnimation: number,
  dwell = 8,
): boolean => {
  if (dwell > 0) {
    const currentStep = getCurrentStep(timerId);
    // Reset the dwell if we are not at the last animation step
    if (currentStep < numberOfStepsInAnimation - 1) {
      timerDwellMap.set(timerId, dwell);
      return false;
    }

    // We are at the last animation step, check the dwell
    const timerDwell =
      (timerDwellMap.has(timerId) && timerDwellMap.get(timerId)) || 0;

    if (currentStep === numberOfStepsInAnimation - 1 && timerDwell > 0) {
      timerDwellMap.set(timerId, timerDwell - 1);
      return true;
    }
  }
  return false;
};

/**
 * Set step for the timerId
 * @param timerId
 * @param timerStep
 */
export const setStep = (timerId: string, timerStep: number): void => {
  stepMap.set(timerId, timerStep);
};

/**
 * Gets the current step for the timer
 * @param timerId
 * @returns
 */
export const getCurrentStep = (timerId: string): number => {
  return stepMap.get(timerId) || 0;
};

/**
 * Checks if the next step is available for all provided layers
 * @param reduxLayers
 * @param nextTimeString
 * @returns
 */
export const isNextStepAvailable = (
  reduxLayers: ReduxLayer[],
  nextTimeString: string,
): boolean => {
  // Get all wmLayers for the found id's
  const wmLayers = reduxLayers
    .map((reduxLayer) => {
      return reduxLayer.id && getWMLayerById(reduxLayer.id);
    })
    .filter((l) => !!l);

  // Returns a layer which could have an image for the requested time, but which isn't loaded yet.
  const isThereSomethingNotAvailable = wmLayers.map((l) => {
    if (!l || !l.olSource) {
      // No OpenLayers source, do nothing, so do not flag that something is not available
      return { status: true };
    }
    // Check if there is a timevalue available
    const closestValue = l
      .getDimension('time')
      ?.getClosestValue(nextTimeString);
    if (!closestValue || WMInvalidDateValues.has(closestValue)) {
      // No value available, so do not flag that something is not available
      return { status: true };
    }
    const olSource = l.olSource as TimeawareImageSource;
    const hasImageAvailableForRequestedTimeStep =
      olSource.hasImageForTimeValue(closestValue);

    // If there is no image available for requested timestep, return true and flag that this thing is not ready yet.
    if (hasImageAvailableForRequestedTimeStep) {
      return {
        status: true,
      };
    }
    olSource.loadImageForTimeValue(closestValue);
    return {
      status: false,
    };
  });
  const nextStepIsAvailable =
    isThereSomethingNotAvailable.filter((d) => d.status === true).length ===
    wmLayers.length;
  return nextStepIsAvailable;
};

/**
 * This handler is triggered by the metronome. An array of timerIds is given as argument.
 * It will update the animation loop of multiple maps and sliders
 * It will prefetch images for maps
 * @param timerIds string[] array of timerIds
 * @param listenerApi ListenerEffectAPI<CoreAppStore, Dispatch, unknown> listenerApi as received from listener
 */
export const olMetronomeHandler = (
  timerIds: string[],
  listenerApi: ListenerEffectAPI<CoreAppStore, Dispatch, unknown>,
): void => {
  const targetsWithUpdateValue: SetTimeSyncPayload[] = [];

  for (const timerId of timerIds) {
    const animationListValuesNameAndValue = mapSelectors.getAnimationList(
      listenerApi.getState(),
      timerId,
    );

    const animationListValues = animationListValuesNameAndValue.map(
      (nameAndValue) => nameAndValue.value,
    );

    const targets = synchronizationGroupsSelector.getTargets(
      listenerApi.getState(),
      {
        sourceId: timerId,
        origin: timerId,
      },
      SYNCGROUPS_TYPE_SETTIME,
    ) as SetTimeSyncPayload[];

    if (targets.length === 0) {
      // When there are no targets, default to one
      targets.push({
        targetId: timerId,
        value: '',
      });
    }

    const timerIsInDwell = handleTimerDwell(
      timerId,
      animationListValues.length,
    );

    // Collect all layers for this syncgroups target
    const reduxLayers: ReduxLayer[] = [];
    targets.forEach((target) => {
      reduxLayers.push(
        ...layerSelectors
          .getLayersByMapId(listenerApi.getState(), target.targetId)
          .filter((l) => l.layerType === LayerType.mapLayer),
      );
    });

    const nextStep = getNextStep(timerId, animationListValues.length, 1);

    const timerShouldStepForward =
      isNextStepAvailable(reduxLayers, animationListValues[nextStep]) &&
      !timerIsInDwell;

    // Determine the next step
    const timerStep = timerShouldStepForward
      ? getNextStep(timerId, animationListValues.length)
      : getCurrentStep(timerId);
    setStep(timerId, timerStep);

    const updatedValue: string = animationListValues[timerStep];

    targetsWithUpdateValue.push(
      ...targets.map((target: SetTimeSyncPayload): SetTimeSyncPayload => {
        return {
          ...target,
          value: updatedValue,
        } as SetTimeSyncPayload;
      }),
    );

    const speedDelay = mapSelectors.getMapAnimationDelay(
      listenerApi.getState(),
      timerId,
    );
    const speed = 1000 / (speedDelay || 1);
    metronome.setSpeed(timerId, speed);
  }

  // Update all targets of all sync groups in one action.
  if (targetsWithUpdateValue.length > 0) {
    listenerApi.dispatch(
      setTimeSync(null, targetsWithUpdateValue, ['metronomelistener']),
    );
  }
};
