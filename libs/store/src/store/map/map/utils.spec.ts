/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { DateInterval } from '@opengeoweb/webmap';
import {
  generateAnimationList,
  getAnimationDuration,
  getSpeedFactor,
  getTimeStepFromDataInterval,
  moveArrayElements,
  parseTimeDimToISO8601Interval,
} from './utils';

describe('store/mapStore/map/utils', () => {
  describe('moveArrayElements', () => {
    it('should move string array elements', () => {
      const testArray = ['test-1', 'test-2', 'test-3'];
      expect(moveArrayElements(testArray, 0, 1)).toEqual([
        'test-2',
        'test-1',
        'test-3',
      ]);
      expect(moveArrayElements(testArray, 0, 2)).toEqual([
        'test-2',
        'test-3',
        'test-1',
      ]);
      expect(moveArrayElements(testArray, 1, 0)).toEqual([
        'test-2',
        'test-1',
        'test-3',
      ]);
      expect(moveArrayElements(testArray, 1, 2)).toEqual([
        'test-1',
        'test-3',
        'test-2',
      ]);
      expect(moveArrayElements(testArray, 2, 0)).toEqual([
        'test-3',
        'test-1',
        'test-2',
      ]);
      expect(moveArrayElements(testArray, 2, 1)).toEqual([
        'test-1',
        'test-3',
        'test-2',
      ]);
    });
    it('should move other array elements', () => {
      const testArray = [
        { 'test-1': 'one' },
        { 'test-2': 'two' },
        { 'test-3': 'three' },
      ];
      expect(moveArrayElements(testArray, 0, 1)).toEqual([
        { 'test-2': 'two' },
        { 'test-1': 'one' },
        { 'test-3': 'three' },
      ]);
      expect(moveArrayElements(testArray, 1, 2)).toEqual([
        { 'test-1': 'one' },
        { 'test-3': 'three' },
        { 'test-2': 'two' },
      ]);
      expect(moveArrayElements(testArray, 2, 0)).toEqual([
        { 'test-3': 'three' },
        { 'test-1': 'one' },
        { 'test-2': 'two' },
      ]);
      expect(moveArrayElements([0, 1, 2], 2, 0)).toEqual([2, 0, 1]);
    });
  });

  const ti1 = new DateInterval('1', '0', '0', '0', '0', '0');
  const ti2 = new DateInterval('0', '1', '0', '0', '0', '0');
  const ti3 = new DateInterval('0', '0', '1', '0', '0', '0');
  const ti4 = new DateInterval('0', '0', '0', '1', '0', '0');
  const ti5 = new DateInterval('0', '0', '0', '0', '1', '0');
  const ti6 = new DateInterval('0', '0', '0', '0', '0', '1');
  describe.each([
    [ti1, 365 * 24 * 60],
    [ti2, 30 * 24 * 60],
    [ti3, 24 * 60],
    [ti4, 60],
    [ti5, 1],
    [ti6, 1 / 60],
  ])('getTimeStepFromDataInterval', (interval, expected) => {
    it(`should return ${expected} for ${JSON.stringify(interval)}`, () => {
      expect(getTimeStepFromDataInterval(interval)).toBeCloseTo(expected);
    });
  });

  describe('getSpeedFactor', () => {
    it('should return correct speedFactor for the passed delay', () => {
      expect(getSpeedFactor(62.5)).toEqual(16);
      expect(getSpeedFactor(125)).toEqual(8);
      expect(getSpeedFactor(250)).toEqual(4);
      expect(getSpeedFactor(500)).toEqual(2);
      expect(getSpeedFactor(1000)).toEqual(1);
      expect(getSpeedFactor(2000)).toEqual(0.5);
      expect(getSpeedFactor(5000)).toEqual(0.2);
      expect(getSpeedFactor(10000)).toEqual(0.1);
    });
  });

  describe('getAnimationDuration', () => {
    it('should return correct duration for passed start and end times', () => {
      expect(
        getAnimationDuration('2023-01-01T00:30:00Z', '2023-01-01T00:00:00Z'),
      ).toEqual(30);
      expect(
        getAnimationDuration('2023-01-01T00:30:00Z', '2023-01-01T00:00:01Z'),
      ).toEqual(29);
      expect(getAnimationDuration(undefined, undefined)).toEqual(0);
    });
  });
  describe('parseTimeDimToISO8601Interval', () => {
    it('should parse correct ISO8601 interval from time dimension', () => {
      const iso8601Interval = parseTimeDimToISO8601Interval(
        '2024-06-19T00:00:00Z,2024-06-19T03:00:00Z/2024-06-30T00:00:00Z/PT3H,2024-06-30T06:00:00Z/2024-07-04T00:00:00Z/PT6H',
      );
      expect(iso8601Interval).toEqual([
        {
          startTime: '2024-06-19T03:00:00Z',
          endTime: '2024-06-30T00:00:00Z',
          duration: 'PT3H',
        },
        {
          startTime: '2024-06-30T06:00:00Z',
          endTime: '2024-07-04T00:00:00Z',
          duration: 'PT6H',
        },
      ]);
    });
    it('should return empty array when no ISO8601 intervals are present', () => {
      const iso8601Interval = parseTimeDimToISO8601Interval(
        '2024-08-14T18:00:00Z,2024-08-14T19:00:00Z,2024-08-14T20:00:00Z',
      );
      expect(iso8601Interval).toEqual([]);
    });
  });
  describe('generateAnimationList', () => {
    it('should generate correct animation list for layers with multiple ISO8601-intervals', () => {
      const animationStart = 1719208800; // (GMT): Monday, June 24, 2024 6:00:00 AM
      const animationEnd = 1719284400; // (GMT): June 25, 2024 3:00:00 AM
      const values =
        '2024-06-24T00:00:00Z,2024-06-24T06:00:00Z/2024-06-24T15:00:00Z/PT3H,2024-06-24T21:00:00Z/2024-06-25T03:00:00Z/PT6H';
      const animationList = generateAnimationList(
        animationStart,
        animationEnd,
        values,
      );
      expect(animationList).toEqual([
        {
          name: 'time',
          value: '2024-06-24T06:00:00.000Z',
        },
        {
          name: 'time',
          value: '2024-06-24T09:00:00.000Z',
        },
        {
          name: 'time',
          value: '2024-06-24T12:00:00.000Z',
        },
        {
          name: 'time',
          value: '2024-06-24T15:00:00.000Z',
        },
        {
          name: 'time',
          value: '2024-06-24T21:00:00.000Z',
        },
        {
          name: 'time',
          value: '2024-06-25T03:00:00.000Z',
        },
      ]);
    });
    it(' should generate correct animation list for layers with single ISO8601-interval', () => {
      const animationStart = 1719208800; // (GMT): Monday, June 24, 2024 6:00:00 AM
      const animationEnd = 1719284400; // (GMT): June 25, 2024 3:00:00 AM
      const values =
        '2024-06-24T00:00:00Z,2024-06-24T06:00:00Z/2024-06-24T15:00:00Z/PT3H';
      const animationList = generateAnimationList(
        animationStart,
        animationEnd,
        values,
      );
      expect(animationList).toEqual([
        {
          name: 'time',
          value: '2024-06-24T06:00:00.000Z',
        },
        {
          name: 'time',
          value: '2024-06-24T09:00:00.000Z',
        },
        {
          name: 'time',
          value: '2024-06-24T12:00:00.000Z',
        },
        {
          name: 'time',
          value: '2024-06-24T15:00:00.000Z',
        },
      ]);
    });
    it('should return empty array when individual time steps are outside of animation range', () => {
      const animationStart = 1719208800; // (GMT): Monday, June 24, 2024 6:00:00 AM
      const animationEnd = 1719284400; // (GMT): June 25, 2024 3:00:00 AM
      const values =
        '2024-08-14T18:00:00Z,2024-08-14T19:00:00Z,2024-08-14T20:00:00Z';
      const animationList = generateAnimationList(
        animationStart,
        animationEnd,
        values,
      );
      expect(animationList).toEqual([]);
    });
    it('should return empty array when no animation start and end times are present', () => {
      const animationStart = undefined;
      const animationEnd = undefined;
      const values =
        '2024-06-24T00:00:00Z,2024-06-24T06:00:00Z/2024-06-24T15:00:00Z/PT3H';
      const animationList = generateAnimationList(
        animationStart!,
        animationEnd!,
        values,
      );
      expect(animationList).toEqual([]);
    });
  });
});
