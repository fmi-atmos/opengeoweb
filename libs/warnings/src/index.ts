/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import warningsTranslations from '../locales/warnings.json';

export * from './lib/components/ObjectManager';
export * from './lib/components/WarningsModuleProvider';
export * from './lib/components/DrawingTool';
export * from './lib/components/DrawingToolForm';
export * from './lib/components/PublicWarningsFormDialog';
export * from './lib/components/ComponentsLookUp';
export * from './lib/store';
export { WARN_NAMESPACE } from './lib/utils/i18n';
export { warningsTranslations };
export * as publicWarningsSelectors from './lib/store/publicWarnings/selectors';
export * from './lib/store/types';
