/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, screen, act } from '@testing-library/react';
import useOverflowCheck, {
  getWarningWithoutIdStatusEditor,
} from './warningUtils';
import { WarningsThemeProvider } from '../components/Providers/Providers';
import { Warning, PublicWarningStatus } from '../store/publicWarningForm/types';

const TestComponent: React.FC<{ text: string }> = ({ text }) => {
  const { ref, isOverflowing } = useOverflowCheck(text);
  return (
    <div ref={ref} data-testid="test-div">
      {isOverflowing ? 'Overflowing' : 'Not Overflowing'}
    </div>
  );
};

describe('useOverflowCheck', () => {
  it('should detect overflow when text is too long', () => {
    render(
      <WarningsThemeProvider>
        <TestComponent text="This is a very long text that should overflow" />
      </WarningsThemeProvider>,
    );

    const testDiv = screen.getByTestId('test-div');

    act(() => {
      Object.defineProperty(testDiv, 'scrollWidth', {
        value: 200,
        configurable: true,
      });
      Object.defineProperty(testDiv, 'clientWidth', {
        value: 100,
        configurable: true,
      });
      testDiv.dispatchEvent(new Event('resize'));
    });

    expect(testDiv).toHaveTextContent('Overflowing');
  });

  it('should not detect overflow when text fits', () => {
    render(
      <WarningsThemeProvider>
        <TestComponent text="Short text" />
      </WarningsThemeProvider>,
    );

    const testDiv = screen.getByTestId('test-div');

    act(() => {
      Object.defineProperty(testDiv, 'scrollWidth', {
        value: 80,
        configurable: true,
      });
      Object.defineProperty(testDiv, 'clientWidth', {
        value: 100,
        configurable: true,
      });
      testDiv.dispatchEvent(new Event('resize'));
    });

    expect(testDiv).toHaveTextContent('Not Overflowing');
  });
});

describe('getWarningWithoutIdStatusEditor', () => {
  it('removes id, status, and editor from the warning object', () => {
    const warning: Warning = {
      id: '123',
      status: PublicWarningStatus.DRAFT,
      editor: 'editorName',
      warningDetail: {
        id: 'warningDetailId',
        phenomenon: 'High Wind',
        areas: [],
        validFrom: '2024-01-01T00:00:00Z',
        validUntil: '2024-01-02T00:00:00Z',
        level: 'Orange',
        probability: 80,
        descriptionOriginal: 'Strong winds expected',
        descriptionTranslation: 'Sterke wind verwacht',
      },
      lastUpdatedTime: '2024-01-01T12:00:00Z',
      type: 'public',
    };

    const result = getWarningWithoutIdStatusEditor(warning);

    expect(result).toEqual({
      type: 'public',
      warningDetail: {
        phenomenon: 'High Wind',
        areas: [],
        validFrom: '2024-01-01T00:00:00Z',
        validUntil: '2024-01-02T00:00:00Z',
        level: 'Orange',
        probability: 80,
        descriptionOriginal: 'Strong winds expected',
        descriptionTranslation: 'Sterke wind verwacht',
      },
      lastUpdatedTime: '2024-01-01T12:00:00Z',
    } as Warning);
  });

  it('removes published_warning_uuid from areas', () => {
    const warning: Warning = {
      warningDetail: {
        phenomenon: 'Heavy Rain',
        areas: [
          {
            objectName: 'Region A',
            geoJSON: { type: 'FeatureCollection', features: [] },
            areaId: 'area123',
            published_warning_uuid: 'uuid123',
          },
        ],
        validFrom: '2024-01-05T00:00:00Z',
        validUntil: '2024-01-06T00:00:00Z',
        level: 'Red',
        probability: 90,
        descriptionOriginal: 'Heavy rainfall',
        descriptionTranslation: 'Zware regenval',
      },
      type: 'public',
    };

    const result = getWarningWithoutIdStatusEditor(warning);

    expect(result.warningDetail.areas).toEqual([
      {
        objectName: 'Region A',
        geoJSON: { type: 'FeatureCollection', features: [] },
        areaId: 'area123',
      },
    ]);
  });

  it('handles warnings without optional fields gracefully', () => {
    const warning: Warning = {
      warningDetail: {
        phenomenon: 'Snowstorm',
        areas: undefined,
        validFrom: '2024-01-10T00:00:00Z',
        validUntil: '2024-01-11T00:00:00Z',
        level: 'Yellow',
        probability: 70,
        descriptionOriginal: 'Heavy snowfall',
        descriptionTranslation: 'Zware sneeuwval',
      },
      type: 'public',
    };

    const result = getWarningWithoutIdStatusEditor(warning);

    expect(result).toEqual({
      type: 'public',
      warningDetail: {
        phenomenon: 'Snowstorm',
        areas: undefined,
        validFrom: '2024-01-10T00:00:00Z',
        validUntil: '2024-01-11T00:00:00Z',
        level: 'Yellow',
        probability: 70,
        descriptionOriginal: 'Heavy snowfall',
        descriptionTranslation: 'Zware sneeuwval',
      },
    });
  });
});
