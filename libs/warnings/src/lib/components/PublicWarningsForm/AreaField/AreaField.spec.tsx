/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { AreaField, formatDate } from './AreaField';
import { testGeoJSON } from '../../../storybookUtils/testUtils';
import { WarningsThemeProvider } from '../../Providers/Providers';
import { translateKeyOutsideComponents } from '../../../utils/i18n';

describe('components/PublicWarningsForm/AreaField', () => {
  const props = {
    onEditObject: jest.fn(),
    onViewObject: jest.fn(),
    onDeleteObject: jest.fn(),
    onAddObject: jest.fn(),
  };

  const object = {
    id: 'test',
  };

  const defaultValues = {
    object,
    areas: [{ objectName: 'test name', geoJSON: testGeoJSON }],
  };

  it('renders the component with default props', () => {
    const defaultProps = {
      onEditObject: jest.fn(),
      onViewObject: jest.fn(),
      onDeleteObject: jest.fn(),
      onAddObject: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider>
          <AreaField {...defaultProps} />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );

    // Check if the component renders with default labels
    expect(
      screen.getByText(translateKeyOutsideComponents('warning-area')),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('warning-button-add-object'),
      ),
    ).toBeInTheDocument();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('object-manager-options-menu'),
      }),
    ).toBeFalsy();
  });

  it('renders the area field as disabled', () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider options={{ defaultValues }}>
          <AreaField {...props} isDisabled />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );

    // Check if the component renders with default labels
    expect(
      screen.getByText(translateKeyOutsideComponents('warning-area')),
    ).toBeInTheDocument();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('warning-button-add-object'),
      ),
    ).toBeInTheDocument();
    fireEvent.click(screen.getByRole('button', { name: /Object options/i }));
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('object-manager-options-menu'),
      ),
    ).toBeFalsy();

    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-button-add-object'),
      ),
    );
    expect(props.onAddObject).not.toHaveBeenCalled();
  });

  it('should disable object options', () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider options={{ defaultValues }}>
          <AreaField {...props} />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );
    fireEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('object-manager-options-menu'),
      }),
    );
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('object-manager-options-menu'),
      ),
    ).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    ).toBeFalsy();
  });

  it.skip('should edit an object', () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider options={{ defaultValues }}>
          <AreaField {...props} />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );
    fireEvent.click(screen.getByRole('button', { name: /Object options/i }));
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-options-menu'),
      ),
    ).toBeTruthy();
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    );
    expect(props.onEditObject).toHaveBeenCalled();
  });

  it.skip('should view an object', () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider options={{ defaultValues }}>
          <AreaField {...props} />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );
    fireEvent.click(screen.getByRole('button', { name: /Object options/i }));
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-options-menu'),
      ),
    ).toBeTruthy();
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-view'),
      ),
    );
    expect(props.onViewObject).toHaveBeenCalled();
  });

  it.skip('should delete an object', () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider options={{ defaultValues }}>
          <AreaField {...props} />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );
    fireEvent.click(screen.getByRole('button', { name: /Object options/i }));
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-options-menu'),
      ),
    ).toBeTruthy();
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-delete'),
      ),
    );
    expect(props.onDeleteObject).toHaveBeenCalled();
  });

  it('should add an object', () => {
    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider>
          <AreaField {...props} />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-button-add-object'),
      ),
    );
    expect(props.onAddObject).toHaveBeenCalled();
  });

  it('should show correct area values', () => {
    const testObject = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      objectName: 'my test area',
    };

    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider
          options={{
            defaultValues: {
              object: testObject,
              areas: [
                { objectName: testObject.objectName, geoJSON: testGeoJSON },
              ],
            },
          }}
        >
          <AreaField {...props} />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(testObject.objectName)).toBeTruthy();
    expect(
      screen.getByText(formatDate(testObject.lastUpdatedTime)),
    ).toBeTruthy();
    // check that correct date is displayed in UTC
    expect(screen.getByText('01/06/2022 12:34 UTC')).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('object-manager-options-menu'),
      }),
    ).toBeTruthy();
  });

  it('should show area with generic object name if no objectName is given', () => {
    const testObject = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
    };

    render(
      <WarningsThemeProvider>
        <ReactHookFormProvider
          options={{
            defaultValues: {
              object: testObject,
              areas: [{ objectName: '', geoJSON: testGeoJSON }],
            },
          }}
        >
          <AreaField {...props} />
        </ReactHookFormProvider>
      </WarningsThemeProvider>,
    );

    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-object-selected'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(formatDate(testObject.lastUpdatedTime)),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('object-manager-options-menu'),
      }),
    ).toBeTruthy();
  });
});
