/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  CoastalEvent,
  Fog,
  FreezingPrecipitation,
  Haze,
  Lightning,
  ModerateAircraftIcing,
  ModerateTurbulence,
  RadioactiveMaterials,
  Rain,
  SandStorm,
  SevereAircraftIcing,
  SevereTurbulence,
  Snow,
  TemperatureHigh,
  TemperatureLow,
  Thunderstorm,
  VulcanicEruption,
  Wind,
} from '@opengeoweb/theme';
import {
  getWarningPhenomenaIcon,
  warningPhenomena,
  allPhenomenaDict,
  getAviationPhenomenaIcon,
} from './PhenomenonField';

describe('components/PublicWarningsForm/PublicWarningsForm/PhenomenonField', () => {
  it('should return correct icon for phenomenon', () => {
    // Weather phenomena tests
    expect(getWarningPhenomenaIcon('wind')).toEqual(<Wind />);
    expect(getWarningPhenomenaIcon('fog')).toEqual(<Fog />);
    expect(getWarningPhenomenaIcon('snowIce')).toEqual(<Snow />);
    expect(getWarningPhenomenaIcon('thunderstorm')).toEqual(<Lightning />);
    expect(getWarningPhenomenaIcon('rain')).toEqual(<Rain />);
    expect(getWarningPhenomenaIcon('highTemp')).toEqual(<TemperatureHigh />);
    expect(getWarningPhenomenaIcon('lowTemp')).toEqual(<TemperatureLow />);
    expect(getWarningPhenomenaIcon('coastalEvent')).toEqual(<CoastalEvent />);
    expect(getWarningPhenomenaIcon('non-existing')).toBeUndefined();

    // Aviation phenomena tests
    expect(getAviationPhenomenaIcon('RDOACT_CLD')).toEqual(
      <RadioactiveMaterials />,
    );
    expect(getAviationPhenomenaIcon('HVY_SS')).toEqual(<SandStorm />);
    expect(getAviationPhenomenaIcon('SEV_TURB')).toEqual(<SevereTurbulence />);
    expect(getAviationPhenomenaIcon('MOD_TURB')).toEqual(
      <ModerateTurbulence />,
    );
    expect(getAviationPhenomenaIcon('SEV_ICE')).toEqual(
      <SevereAircraftIcing />,
    );
    expect(getAviationPhenomenaIcon('MOD_ICE')).toEqual(
      <ModerateAircraftIcing />,
    );
    expect(getAviationPhenomenaIcon('SEV_ICE_FRZ_RN')).toEqual(
      <FreezingPrecipitation />,
    );
    expect(getAviationPhenomenaIcon('HVY_DS')).toEqual(<Haze />);
    expect(getAviationPhenomenaIcon('VA_CLD')).toEqual(<VulcanicEruption />);

    // Thunderstorm variations (all returning the same icon)
    expect(getAviationPhenomenaIcon('EMBD_TS')).toEqual(<Thunderstorm />);
    expect(getAviationPhenomenaIcon('EMBD_TSGR')).toEqual(<Thunderstorm />);
    expect(getAviationPhenomenaIcon('FRQ_TS')).toEqual(<Thunderstorm />);
    expect(getAviationPhenomenaIcon('FRQ_TSGR')).toEqual(<Thunderstorm />);
    expect(getAviationPhenomenaIcon('OBSC_TS')).toEqual(<Thunderstorm />);
    expect(getAviationPhenomenaIcon('OBSC_TSGR')).toEqual(<Thunderstorm />);
    expect(getAviationPhenomenaIcon('SQL_TSGR')).toEqual(<Thunderstorm />);
    expect(getAviationPhenomenaIcon('ISOL_TS')).toEqual(<Thunderstorm />);
    expect(getAviationPhenomenaIcon('ISOL_TSGR')).toEqual(<Thunderstorm />);
    expect(getAviationPhenomenaIcon('OCNL_TS')).toEqual(<Thunderstorm />);
    expect(getAviationPhenomenaIcon('OCNL_TSGR')).toEqual(<Thunderstorm />);

    expect(getAviationPhenomenaIcon('non-existing')).toBeUndefined();
  });

  it('should make a dict of phenomenon list', () => {
    const dict = allPhenomenaDict;
    const dictKeys = Object.keys(dict);
    dictKeys.forEach((key) => {
      expect(warningPhenomena.includes(dict[key])).toBeTruthy();
    });
    expect(dictKeys).toHaveLength(warningPhenomena.length);
  });
});
