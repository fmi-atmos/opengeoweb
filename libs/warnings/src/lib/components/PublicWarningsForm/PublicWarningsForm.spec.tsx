/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import { errorMessages } from '@opengeoweb/form-fields';
import {
  Airmet,
  Sigmet,
  AviationPhenomenaCode,
  sigmetConfig,
  airmetConfig,
  AviationProduct,
} from '@opengeoweb/sigmet-airmet';
import { FeatureCollection, Geometry, GeoJsonProperties } from 'geojson';
import i18n from 'i18next';
import {
  FormProps,
  PublicWarningsForm,
  getEmptyPublicWarning,
  prepareFormValues,
  warningLevelFeatureColors,
} from './PublicWarningsForm';
import {
  WarningsThemeProvider,
  WarningsThemeStoreProvider,
} from '../Providers/Providers';
import { formatDate } from './AreaField/AreaField';
import { warningPhenomena } from './PhenomenonField/PhenomenonField';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { fakePostError, testGeoJSON } from '../../storybookUtils/testUtils';
import { levelOptions, WarningLevel } from './LevelField/LevelField';
import { PublicWarningDetail } from '../../store/publicWarningForm/types';
import {
  WARN_NAMESPACE,
  translateKeyOutsideComponents,
} from '../../utils/i18n';
import { VALID_FROM_MAX_HOURS_AFTER_CURRENT } from './ValidFromField';
import {
  VALID_UNTIL_MAX_HOURS_AFTER_CURRENT,
  VALID_UNTIL_MIN_HOURS_AFTER_VALID_FROM,
} from './ValidUntilField';
import { useProductConfig } from '../../api/aviation-api/hooks';
import {
  fakeAirmetInWarningList,
  fakeSigmetInWarningList,
} from '../../utils/fakeSigmetAirmet';
import { getAirmetTAC, getSigmetTAC } from '../../api/aviation-api/api';

jest.mock('libs/warnings/src/lib/api/aviation-api/hooks', () => ({
  ...jest.requireActual('libs/warnings/src/lib/api/aviation-api/hooks'),
  useProductConfig: jest.fn(),
}));

const mockUseProductConfig = useProductConfig as jest.Mock;

const object = {
  id: 'test',
  lastUpdatedTime: '2022-06-01T12:34:27Z',
  objectName: 'my test area',
  scope: 'user',
  geoJSON: testGeoJSON,
} as DrawingListItem;

const props = {
  onPublishForm: async (): Promise<null> => null,
  onSaveForm: async (): Promise<null> => null,
  onClearForm: jest.fn(),
  object,
  publicWarningDetails: {
    id: 'test',
    phenomenon: 'coastalEvent',
    validFrom: '2022-06-01T15:00:00Z',
    validUntil: '2022-06-01T18:00:00Z',
    level: 'extreme',
    probability: 80,
    descriptionOriginal:
      'Some pretty intense coastal weather is coming our way',
    descriptionTranslation: 'And this would be the translation',
    areas: [{ geoJSON: testGeoJSON, objectName: object.objectName }],
  } as PublicWarningDetail,
};

describe('components/PublicWarningsForm/PublicWarningsForm', () => {
  const clickPublish = (): void => {
    fireEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    );
  };

  beforeEach(() => {
    mockUseProductConfig.mockReturnValue({
      aviationConfig: sigmetConfig,
    });
  });

  describe('prepareFormValues', () => {
    const object: DrawingListItem = {
      id: '923723984872338768743',
      objectName: 'Drawing object 101',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      scope: 'user',
      geoJSON: testGeoJSON,
    };
    const publicWarningDetails: PublicWarningDetail = {
      id: '923723984872338768743',
      phenomenon: 'coastalEvent',
      validFrom: '2022-06-01T15:00:00Z',
      validUntil: '2022-06-01T18:00:00Z',
      level: 'extreme',
      probability: 80,
      descriptionOriginal:
        'Some pretty intense coastal weather is coming our way',
      descriptionTranslation: 'And this would be the translation',
      areas: [{ geoJSON: testGeoJSON, objectName: 'test name' }],
    };

    it('should remove unneeded fields', () => {
      const result = prepareFormValues({
        IS_DRAFT: true,
        object,
        'translation-select': 'NL',
        ...publicWarningDetails,
      });

      expect(result).toEqual(publicWarningDetails);
    });
    it('should remove empty fields', () => {
      const emptyForm: PublicWarningDetail = {
        id: '',
        phenomenon: '',
        validFrom: '2022-06-01T15:00:00Z',
        validUntil: '2022-06-01T18:00:00Z',
        level: '',
        probability: 80,
        descriptionOriginal: '',
        descriptionTranslation: '',
        areas: [],
      };

      const result = prepareFormValues(emptyForm);

      expect(result).toEqual({
        validFrom: '2022-06-01T15:00:00Z',
        validUntil: '2022-06-01T18:00:00Z',
        probability: 80,
      });
    });

    it('should do nothing with filled in fields', () => {
      const result = prepareFormValues({
        ...publicWarningDetails,
      });

      expect(result).toEqual(publicWarningDetails);
    });
  });

  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should render with default props', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('warning-phenomenon')),
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('warning-valid-from')),
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('warning-valid-until')),
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('warning-level')),
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('warning-probability')),
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('warning-button-clear')),
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('warning-button-save')),
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('warning-button-publish')),
    ).toBeTruthy();
    expect(screen.queryByRole('alert')).toBeFalsy();
  });

  it('should render as readonly', () => {
    const props = {
      formAction: 'readonly' as const,
      isReadOnly: true,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-saving'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('object-manager-button-edit'),
      }),
    ).toBeFalsy();

    screen
      .getAllByRole('textbox')
      .forEach((textbox) =>
        expect(textbox.getAttribute('readonly')).toBeDefined(),
      );
  });

  it('should render as error', () => {
    const testProps = {
      ...props,
      error: { message: 'test error' },
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...testProps} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(testProps.error.message)).toBeTruthy();
    fireEvent.click(screen.getByRole('button', { name: 'CLOSE' }));
    expect(screen.queryByText(testProps.error.message)).toBeFalsy();
  });

  it('should render object', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(props.object.objectName)).toBeTruthy();
    expect(
      screen.getByText(formatDate(props.object.lastUpdatedTime)),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('object-manager-options-menu'),
      }),
    ).toBeTruthy();
  });

  it('should render with default form properties if none passed', () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T14:00:00Z'));

    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('warning-phenomenon')),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-no-object-selected'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('warning-level')),
    ).toBeTruthy();
    expect(screen.getByText('30 %')).toBeTruthy();
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid from',
        })
        .getAttribute('value') || '',
    ).toEqual('20/12/2023 14:15');
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid until',
        })
        .getAttribute('value') || '',
    ).toEqual('20/12/2023 20:15');
  });

  it('should render with form properties if passed', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText('Coastal event')).toBeTruthy();
    expect(screen.getByText('my test area')).toBeTruthy();
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid from',
        })
        .getAttribute('value') || '',
    ).toEqual('01/06/2022 15:00');
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid until',
        })
        .getAttribute('value') || '',
    ).toEqual('01/06/2022 18:00');

    expect(screen.getByText('Extreme')).toBeTruthy();
    expect(screen.getByText('80 %')).toBeTruthy();
  });

  it('should be able to publish a public warning', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T14:00:00Z'));

    const onPublishSpy = jest.spyOn(props, 'onPublishForm');
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(
      screen.getByText(translateKeyOutsideComponents('warning-button-publish')),
    );

    await waitFor(() => {
      expect(onPublishSpy).toHaveBeenCalledWith({
        ...props.publicWarningDetails,
      });
    });
  });

  it('should be able to save a public warning', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T14:00:00Z'));

    const onSaveSpy = jest.spyOn(props, 'onSaveForm');
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(
      screen.getByText(translateKeyOutsideComponents('warning-button-save')),
    );

    await waitFor(() => {
      expect(onSaveSpy).toHaveBeenCalledWith({
        ...props.publicWarningDetails,
      });
    });
  });

  it('should be able to clear the form', async () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    await waitFor(() => screen.findAllByTestId('openlayers-mapview'));

    const phenomenon = screen.getByLabelText('Phenomenon');
    const validFrom = screen.getByRole('textbox', { name: 'Valid from' });
    const validUntil = screen.getByRole('textbox', { name: 'Valid until' });
    const level = screen.getByLabelText('Level');
    const probability = screen.getByLabelText('Probability');
    const descriptionOriginal = screen.getByRole('tabpanel', {
      name: 'Description original',
    });

    // test filled in form
    expect(phenomenon.textContent).toEqual(
      translateKeyOutsideComponents(warningPhenomena[8].translationKey),
    );
    expect(validFrom.getAttribute('value')!).toEqual(
      dateUtils.dateToString(
        new Date(props.publicWarningDetails.validFrom),
        dateUtils.DATE_FORMAT_DATEPICKER,
        true,
      ),
    );
    expect(validUntil.getAttribute('value')!).toEqual(
      dateUtils.dateToString(
        new Date(props.publicWarningDetails.validUntil),
        dateUtils.DATE_FORMAT_DATEPICKER,
        true,
      ),
    );

    expect(level.textContent!.trim()).toEqual(
      translateKeyOutsideComponents(levelOptions[2].translationKey),
    );
    expect(probability.textContent).toEqual(
      `${props.publicWarningDetails.probability} %`,
    );
    expect(descriptionOriginal.textContent).toEqual(
      props.publicWarningDetails.descriptionOriginal,
    );

    // click clear
    fireEvent.click(
      screen.getByText(translateKeyOutsideComponents('warning-button-clear')),
    );

    const expectedResult = getEmptyPublicWarning();
    const getFieldValue = (textContent: Node['textContent']): string =>
      textContent!.replace(/\u200B/g, '');

    // test form values
    expect(getFieldValue(phenomenon.textContent)).toEqual(
      expectedResult.phenomenon,
    );
    expect(validFrom.getAttribute('value')!).toEqual(
      dateUtils.dateToString(
        new Date(expectedResult.validFrom),
        dateUtils.DATE_FORMAT_DATEPICKER,
        true,
      ),
    );
    expect(validUntil.getAttribute('value')!).toEqual(
      dateUtils.dateToString(
        new Date(expectedResult.validUntil),
        dateUtils.DATE_FORMAT_DATEPICKER,
        true,
      ),
    );

    expect(getFieldValue(level.textContent)).toEqual('');
    expect(probability.textContent).toEqual(`${expectedResult.probability} %`);
    expect(descriptionOriginal.textContent).toEqual('');
    expect(props.onClearForm).toHaveBeenCalled();
  });

  it('should show required field errors on publishing an empty form and focus first error field', async () => {
    // TODO, re-enable test after merge. Difficult one to fix.
    const testProps = {
      onPublishForm: props.onPublishForm,
    };
    const onPublishSpy = jest.spyOn(testProps, 'onPublishForm');

    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...testProps} />
      </WarningsThemeProvider>,
    );

    expect(
      screen
        .queryByText(translateKeyOutsideComponents('warning-area'))!
        .getAttribute('aria-errormessage'),
    ).toBeNull();

    fireEvent.click(
      screen.getByText(translateKeyOutsideComponents('warning-button-publish')),
    );
    expect(onPublishSpy).not.toHaveBeenCalled();
    await waitFor(
      () =>
        expect(
          screen.getAllByText(
            translateKeyOutsideComponents(errorMessages.required),
          ),
        ).toHaveLength(4),
      { timeout: 5000 },
    );

    const phenomenon = screen.getByLabelText(
      translateKeyOutsideComponents('warning-phenomenon'),
    );
    expect(phenomenon.matches(':focus')).toBeTruthy();

    expect(
      screen
        .getByText(translateKeyOutsideComponents('warning-area'))
        .getAttribute('aria-errormessage'),
    ).toEqual(errorMessages.required);
  });

  it('should not show required field errors on saving an empty form', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2024-06-01T14:00:00Z'));

    const testProps = {
      onSaveForm: props.onSaveForm,
    };
    const onSaveSpy = jest.spyOn(testProps, 'onSaveForm');
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...testProps} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(
      screen.getByText(translateKeyOutsideComponents('warning-button-save')),
    );
    await waitFor(() =>
      expect(onSaveSpy).toHaveBeenCalledWith(
        prepareFormValues(getEmptyPublicWarning()),
      ),
    );
    expect(
      screen.queryByText(translateKeyOutsideComponents(errorMessages.required)),
    ).toBeFalsy();
  });

  it('should show errors', async () => {
    const testProps = {
      ...props,
      error: { message: fakePostError },
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...testProps} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByRole('alert')).toBeTruthy();
    expect(screen.getByRole('alert').classList).not.toContain(
      'MuiAlert-standardWarning',
    );
    const titleError1 = screen.getByText('body -> warningDetail -> phenomenon');
    const messageError1 = screen.getByText(
      "value is not a valid enumeration member; permitted: 'wind', 'fog', 'thunderstorm', 'snowIce', 'rain', 'highTemp', 'funnelCloud', 'lowTemp', 'coastalEvent' (type=type_error.enum; enum_values=[<PhenomenonEnum.WIND: 'wind'>, <PhenomenonEnum.FOG: 'fog'>, <PhenomenonEnum.THUNDERSTORM: 'thunderstorm'>, <PhenomenonEnum.SNOW_ICE: 'snowIce'>, <PhenomenonEnum.RAIN: 'rain'>, <PhenomenonEnum.HIGH_TEMP: 'highTemp'>, <PhenomenonEnum.FUNNEL_CLD: 'funnelCloud'>, <PhenomenonEnum.LOW_TEMP: 'lowTemp'>, <PhenomenonEnum.COASTAL_EVENT: 'coastalEvent'>])",
    );

    expect(titleError1).toBeTruthy();
    expect(titleError1.tagName).toEqual('I');

    expect(messageError1).toBeTruthy();
    expect(messageError1.tagName).toEqual('P');

    const titleError2 = screen.getByText('body -> warningDetail -> level');
    const messageError2 = screen.getByText(
      "value is not a valid enumeration member; permitted: 'moderate', 'severe', 'extreme' (type=type_error.enum; enum_values=[<LevelEnum.MODERATE: 'moderate'>, <LevelEnum.SEVERE: 'severe'>, <LevelEnum.EXTREME: 'extreme'>])",
    );
    expect(titleError2).toBeTruthy();
    expect(titleError2.tagName).toEqual('I');

    expect(messageError2).toBeTruthy();
    expect(messageError2.tagName).toEqual('P');

    fireEvent.click(screen.getByRole('button', { name: /CLOSE/i }));

    expect(screen.queryByRole('alert')).toBeFalsy();
  });

  it('should show errors as warning', async () => {
    const testProps = {
      ...props,
      error: { message: fakePostError, severity: 'warning' as const },
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...testProps} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByRole('alert')).toBeTruthy();
    expect(screen.getByRole('alert').classList).toContain(
      'MuiAlert-standardWarning',
    );
    const titleError1 = screen.getByText('body -> warningDetail -> phenomenon');
    const messageError1 = screen.getByText(
      "value is not a valid enumeration member; permitted: 'wind', 'fog', 'thunderstorm', 'snowIce', 'rain', 'highTemp', 'funnelCloud', 'lowTemp', 'coastalEvent' (type=type_error.enum; enum_values=[<PhenomenonEnum.WIND: 'wind'>, <PhenomenonEnum.FOG: 'fog'>, <PhenomenonEnum.THUNDERSTORM: 'thunderstorm'>, <PhenomenonEnum.SNOW_ICE: 'snowIce'>, <PhenomenonEnum.RAIN: 'rain'>, <PhenomenonEnum.HIGH_TEMP: 'highTemp'>, <PhenomenonEnum.FUNNEL_CLD: 'funnelCloud'>, <PhenomenonEnum.LOW_TEMP: 'lowTemp'>, <PhenomenonEnum.COASTAL_EVENT: 'coastalEvent'>])",
    );

    expect(titleError1).toBeTruthy();
    expect(titleError1.tagName).toEqual('I');

    expect(messageError1).toBeTruthy();
    expect(messageError1.tagName).toEqual('P');

    const titleError2 = screen.getByText('body -> warningDetail -> level');
    const messageError2 = screen.getByText(
      "value is not a valid enumeration member; permitted: 'moderate', 'severe', 'extreme' (type=type_error.enum; enum_values=[<LevelEnum.MODERATE: 'moderate'>, <LevelEnum.SEVERE: 'severe'>, <LevelEnum.EXTREME: 'extreme'>])",
    );
    expect(titleError2).toBeTruthy();
    expect(titleError2.tagName).toEqual('I');

    expect(messageError2).toBeTruthy();
    expect(messageError2.tagName).toEqual('P');

    fireEvent.click(screen.getByRole('button', { name: /CLOSE/i }));

    expect(screen.queryByRole('alert')).toBeFalsy();
  });

  it('should scroll to top when an backend error appears', async () => {
    const newError = { message: fakePostError, severity: 'warning' as const };

    const { rerender } = render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.queryByRole('alert')).toBeFalsy();
    expect(screen.getByTestId('scroll-container').scrollTop).toEqual(0);

    screen.getByTestId('scroll-container').scrollTop = 100;
    expect(screen.getByTestId('scroll-container').scrollTop).toEqual(100);

    rerender(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} error={newError} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByTestId('scroll-container').scrollTop).toEqual(0);

    expect(screen.getByRole('alert')).toBeTruthy();
    expect(screen.getByRole('alert').classList).toContain(
      'MuiAlert-standardWarning',
    );
    const titleError1 = screen.getByText('body -> warningDetail -> phenomenon');
    const messageError1 = screen.getByText(
      "value is not a valid enumeration member; permitted: 'wind', 'fog', 'thunderstorm', 'snowIce', 'rain', 'highTemp', 'funnelCloud', 'lowTemp', 'coastalEvent' (type=type_error.enum; enum_values=[<PhenomenonEnum.WIND: 'wind'>, <PhenomenonEnum.FOG: 'fog'>, <PhenomenonEnum.THUNDERSTORM: 'thunderstorm'>, <PhenomenonEnum.SNOW_ICE: 'snowIce'>, <PhenomenonEnum.RAIN: 'rain'>, <PhenomenonEnum.HIGH_TEMP: 'highTemp'>, <PhenomenonEnum.FUNNEL_CLD: 'funnelCloud'>, <PhenomenonEnum.LOW_TEMP: 'lowTemp'>, <PhenomenonEnum.COASTAL_EVENT: 'coastalEvent'>])",
    );

    expect(titleError1).toBeTruthy();
    expect(titleError1.tagName).toEqual('I');

    expect(messageError1).toBeTruthy();
    expect(messageError1.tagName).toEqual('P');

    const titleError2 = screen.getByText('body -> warningDetail -> level');
    const messageError2 = screen.getByText(
      "value is not a valid enumeration member; permitted: 'moderate', 'severe', 'extreme' (type=type_error.enum; enum_values=[<LevelEnum.MODERATE: 'moderate'>, <LevelEnum.SEVERE: 'severe'>, <LevelEnum.EXTREME: 'extreme'>])",
    );
    expect(titleError2).toBeTruthy();
    expect(titleError2.tagName).toEqual('I');

    expect(messageError2).toBeTruthy();
    expect(messageError2.tagName).toEqual('P');

    fireEvent.click(screen.getByRole('button', { name: /CLOSE/i }));

    expect(screen.queryByRole('alert')).toBeFalsy();
  });

  /**
   * Find error message related to a form field. This is kinda hard as there is no real relation between the field and the message.
   * The only thing they have in common is that the message is inside an ancestor of the form field.
   * @param el
   * @returns
   */
  function findErrorMessageForFormField(el: HTMLElement): HTMLElement {
    /* eslint-disable testing-library/no-node-access */
    let parent = el.parentElement;
    while (parent) {
      const alert = parent.querySelector('[role="alert"]');
      if (alert) {
        return alert as HTMLElement;
      }
      parent = parent.parentElement;
    }
    throw new Error('Unable to find alert');
  }

  it('should show required field error on valid from field when empty', async () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    const validFrom = screen.getByRole('textbox', {
      name: 'Valid from',
    });
    fireEvent.change(validFrom, {
      target: { value: '' },
    });
    await waitFor(() =>
      expect(validFrom.getAttribute('value') || '').toEqual(''),
    );

    clickPublish();

    await waitFor(() => {
      expect(findErrorMessageForFormField(validFrom)).toHaveTextContent(
        translateKeyOutsideComponents(errorMessages.required),
      );
    });
    expect(validFrom.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show required field error on valid until field when empty', async () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    const validUntil = screen.getByRole('textbox', {
      name: 'Valid until',
    });
    fireEvent.change(validUntil, {
      target: { value: '' },
    });
    await waitFor(() =>
      expect(validUntil.getAttribute('value') || '').toEqual(''),
    );

    clickPublish();

    await waitFor(() => {
      expect(findErrorMessageForFormField(validUntil)).toHaveTextContent(
        translateKeyOutsideComponents(errorMessages.required),
      );
    });
    expect(validUntil.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show error on valid from field when before current time', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T14:00:00Z'));
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const validFrom = screen.getByRole('textbox', {
      name: 'Valid from',
    });
    fireEvent.change(validFrom, {
      target: { value: '20/12/2023 13:59' },
    });

    await waitFor(() =>
      expect(validFrom.getAttribute('value') || '').toEqual('20/12/2023 13:59'),
    );

    await screen.findByText(
      translateKeyOutsideComponents('warning-valid-from-error-before-current'),
    );
    expect(validFrom.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show error on valid from field when too far after current time', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T14:00:00Z'));
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const validFrom = screen.getByRole('textbox', {
      name: 'Valid from',
    });
    fireEvent.change(validFrom, {
      target: { value: '27/12/2023 14:01' },
    });
    await waitFor(() =>
      expect(validFrom.getAttribute('value') || '').toEqual('27/12/2023 14:01'),
    );

    await screen.findByText(
      i18n.t('warning-valid-from-error-max-hours', {
        ns: WARN_NAMESPACE,
        MAX_HOURS: VALID_FROM_MAX_HOURS_AFTER_CURRENT,
      }),
    );
    expect(validFrom.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show error on valid until field when before current time', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T14:00:00Z'));
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const validUntil = screen.getByRole('textbox', {
      name: 'Valid until',
    });
    fireEvent.change(validUntil, {
      target: { value: '20/12/2023 13:59' },
    });
    await waitFor(() =>
      expect(validUntil.getAttribute('value') || '').toEqual(
        '20/12/2023 13:59',
      ),
    );

    clickPublish();

    await screen.findByText(
      translateKeyOutsideComponents('warning-valid-until-error-before-current'),
    );
    expect(validUntil.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show error on valid until field when too far after current time', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T04:00:00Z'));
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const validUntil = screen.getByRole('textbox', {
      name: 'Valid until',
    });
    fireEvent.change(validUntil, {
      target: { value: '27/12/2023 16:01' },
    });
    await waitFor(() =>
      expect(validUntil.getAttribute('value') || '').toEqual(
        '27/12/2023 16:01',
      ),
    );

    await screen.findByText(
      i18n.t('warning-valid-until-error-max-hours', {
        ns: WARN_NAMESPACE,
        MAX_HOURS: VALID_UNTIL_MAX_HOURS_AFTER_CURRENT,
      }),
    );
    expect(validUntil.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show error on valid until field when less than one hour after valid from time', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T04:00:00Z'));
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const validFrom = screen.getByRole('textbox', {
      name: 'Valid from',
    });
    expect(validFrom.getAttribute('value') || '').toEqual('20/12/2023 04:15');
    const validUntil = screen.getByRole('textbox', {
      name: 'Valid until',
    });
    fireEvent.change(validUntil, {
      target: { value: '20/12/2023 05:14' },
    });
    await waitFor(() =>
      expect(validUntil.getAttribute('value') || '').toEqual(
        '20/12/2023 05:14',
      ),
    );

    const VALID_UNTIL_ERROR_AFTER_VALID_FROM = i18n.t(
      'warning-valid-until-error-min-hours',
      {
        ns: WARN_NAMESPACE,
        MIN_HOURS: VALID_UNTIL_MIN_HOURS_AFTER_VALID_FROM,
      },
    );
    await screen.findByText(VALID_UNTIL_ERROR_AFTER_VALID_FROM);
    expect(validUntil.getAttribute('aria-invalid') === 'true').toBeTruthy();

    fireEvent.change(validFrom, {
      target: { value: '20/12/2023 04:14' },
    });

    await waitFor(() =>
      expect(validFrom.getAttribute('value') || '').toEqual('20/12/2023 04:14'),
    );

    await waitFor(() =>
      expect(
        screen.queryByText(VALID_UNTIL_ERROR_AFTER_VALID_FROM),
      ).toBeFalsy(),
    );
    expect(validUntil.getAttribute('aria-invalid') === 'true').toBeFalsy();
  });

  it('should save an update of a published warning as draft and send along the warning_handle_uuid', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T14:00:00Z'));

    const onSaveSpy = jest.spyOn(props, 'onSaveForm');
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    const save = screen.getByRole('button', {
      name: translateKeyOutsideComponents('warning-button-save'),
    });

    expect(save.hasAttribute('disabled')).toBeFalsy();
    fireEvent.click(save);

    await waitFor(() => {
      expect(onSaveSpy).toHaveBeenCalledWith({
        ...props.publicWarningDetails,
      });
    });
  });

  it('should publish an update of a published warning and send along the warning_handle_uuid', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T14:00:00Z'));

    const onPublishSpy = jest.spyOn(props, 'onPublishForm');
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    const publish = screen.getByRole('button', {
      name: translateKeyOutsideComponents('warning-button-publish'),
    });
    expect(publish.hasAttribute('disabled')).toBeFalsy();
    fireEvent.click(publish);

    await waitFor(() => {
      expect(onPublishSpy).toHaveBeenCalledWith({
        ...props.publicWarningDetails,
      });
    });
  });

  it('should publish a draft published warning and send along the linked_to_id', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T14:00:00Z'));
    const testProps = {
      ...props,
      publicWarningDetails: {
        ...props.publicWarningDetails,
        linked_to_id: 'test-published-id',
      },
    };
    const onPublishSpy = jest.spyOn(testProps, 'onPublishForm');
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...testProps} />
      </WarningsThemeProvider>,
    );

    const publish = screen.getByRole('button', {
      name: translateKeyOutsideComponents('warning-button-publish'),
    });

    fireEvent.click(publish);

    await waitFor(() => {
      expect(onPublishSpy).toHaveBeenCalledWith({
        ...testProps.publicWarningDetails,
      });
    });
  });

  it('should show SigmetForm with actions for draft sigmet', () => {
    mockUseProductConfig.mockReturnValue({
      config: sigmetConfig,
    });
    const props: FormProps = {
      selectedAviationProduct: {
        uuid: 'sigmet-1',
        phenomenon: 'SEV_ICE' as AviationPhenomenaCode, // Ensure this matches the expected type
        validDateStart: '2023-12-20T14:00:00Z',
        validDateEnd: '2023-12-20T18:00:00Z',
        level: { value: 500, unit: 'FL' },
        status: 'DRAFT',
        isObservationOrForecast: 'OBS',
        type: 'NORMAL',
        change: 'NC',
        movementType: 'STATIONARY',
        issueDate: '2023-12-20T13:00:00Z',
        firName: 'Amsterdam FIR',
        locationIndicatorATSR: 'EHAA',
        locationIndicatorATSU: 'EHAA',
        locationIndicatorMWO: 'EHDB',
        startGeometry: { type: 'FeatureCollection', features: [] },
        startGeometryIntersect: { type: 'FeatureCollection', features: [] },
      } as Sigmet,
      warningType: 'sigmet',
      mode: 'edit',
      isReadOnly: false,
    };
    render(
      <WarningsThemeStoreProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.getByText('Severe icing')).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeTruthy();
  });

  it('should show readonly AirmetForm for published airmet', () => {
    mockUseProductConfig.mockReturnValue({
      config: airmetConfig,
    });
    const props: FormProps = {
      selectedAviationProduct: {
        uuid: 'airmet-1',
        phenomenon: 'SFC_WIND' as AviationPhenomenaCode,
        validDateStart: '2023-12-20T14:00:00Z',
        validDateEnd: '2023-12-20T18:00:00Z',
        level: { value: 500, unit: 'FL' },
        areas: [{ geoJSON: testGeoJSON, objectName: 'test area' }],
        status: 'PUBLISHED',
        isObservationOrForecast: 'OBS',
        type: 'TEST',
        change: 'NC',
        movementType: 'STATIONARY',
        issueDate: '2023-12-20T13:00:00Z',
        firName: 'Amsterdam FIR',
        locationIndicatorATSR: 'EHAA',
        locationIndicatorATSU: 'EHAA',
        locationIndicatorMWO: 'EHDB',
        startGeometry: { type: 'FeatureCollection', features: [] },
        startGeometryIntersect: { type: 'FeatureCollection', features: [] },
      } as Airmet,
      warningType: 'airmet',
      mode: 'view',
      isReadOnly: true,
    };
    render(
      <WarningsThemeStoreProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.getByText('Surface wind')).toBeTruthy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();
  });

  it('should update form content when switching between aviation products', () => {
    mockUseProductConfig.mockReturnValue({
      config: airmetConfig,
    });
    const props: FormProps = {
      selectedAviationProduct: {
        uuid: 'airmet-1',
        phenomenon: 'SFC_WIND' as AviationPhenomenaCode,
        validDateStart: '2023-12-20T14:00:00Z',
        validDateEnd: '2023-12-20T18:00:00Z',
        level: { value: 500, unit: 'FL' },
        areas: [{ geoJSON: testGeoJSON, objectName: 'test area' }],
        status: 'PUBLISHED',
        isObservationOrForecast: 'OBS',
        type: 'TEST',
        change: 'NC',
        movementType: 'STATIONARY',
        issueDate: '2023-12-20T13:00:00Z',
        firName: 'Amsterdam FIR',
        locationIndicatorATSR: 'EHAA',
        locationIndicatorATSU: 'EHAA',
        locationIndicatorMWO: 'EHDB',
        startGeometry: { type: 'FeatureCollection', features: [] },
        startGeometryIntersect: { type: 'FeatureCollection', features: [] },
      } as AviationProduct,
      warningType: 'airmet',
      mode: 'view',
      isReadOnly: true,
    };
    const { rerender } = render(
      <WarningsThemeStoreProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.getByText('Surface wind')).toBeTruthy();

    const props2: FormProps = {
      ...props,
      selectedAviationProduct: {
        ...props.selectedAviationProduct,
        uuid: 'airmet-2',
        phenomenon: 'MOD_ICE' as AviationPhenomenaCode,
      } as AviationProduct,
    };
    rerender(
      <WarningsThemeStoreProvider>
        <PublicWarningsForm {...props2} />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByText('Moderate icing')).toBeTruthy();
  });

  it('should not show "(not specifield)" in the list of phenomenon, when creating new warnings', async () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const selectTrigger = screen.getByRole('combobox', { name: /Phenomenon/i }); // Use the label to target
    fireEvent.mouseDown(selectTrigger);

    // Wait for the menu to appear and assert the menu items
    await waitFor(() => {
      expect(screen.getByText('Wind')).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText('Fog')).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText('Thunderstorm')).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText('Snow/ice')).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText('Rain')).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText('High temperature')).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText('Low temperature')).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText('Coastal event')).toBeInTheDocument();
    });

    expect(screen.queryByText(/not specified/i)).not.toBeInTheDocument();
  });

  it('should show correct form for cancels Airmet', () => {
    mockUseProductConfig.mockReturnValue({
      config: airmetConfig,
    });
    const props: FormProps = {
      selectedAviationProduct: fakeAirmetInWarningList[1]
        .airmet as AviationProduct,
      warningType: 'airmet',
      mode: 'view',
      isReadOnly: true,
    };
    render(
      <WarningsThemeStoreProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.queryByText('Phenomenon')).toBeFalsy();
    expect(screen.getByText('Issued at')).toBeTruthy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
  });

  it('should show correct form for cancelled Airmet', () => {
    mockUseProductConfig.mockReturnValue({
      config: airmetConfig,
    });
    const props: FormProps = {
      selectedAviationProduct: fakeAirmetInWarningList[2]
        .airmet as AviationProduct,
      warningType: 'airmet',
      mode: 'view',
      isReadOnly: true,
    };
    render(
      <WarningsThemeStoreProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.getByText('Phenomenon')).toBeTruthy();
    expect(screen.getByText('Surface wind')).toBeTruthy();

    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
  });

  it('should show correct form for cancels Sigmet', () => {
    mockUseProductConfig.mockReturnValue({
      config: airmetConfig,
    });
    const props: FormProps = {
      selectedAviationProduct: fakeSigmetInWarningList[1]
        .sigmet as AviationProduct,
      warningType: 'sigmet',
      mode: 'view',
      isReadOnly: true,
    };
    render(
      <WarningsThemeStoreProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.queryByText('Phenomenon')).toBeFalsy();
    expect(screen.getByText('Issued at')).toBeTruthy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
  });

  it('should show correct form for cancelled Sigmet', () => {
    mockUseProductConfig.mockReturnValue({
      config: airmetConfig,
    });
    const props: FormProps = {
      selectedAviationProduct: fakeSigmetInWarningList[2]
        .sigmet as AviationProduct,
      warningType: 'sigmet',
      mode: 'view',
      isReadOnly: true,
    };
    render(
      <WarningsThemeStoreProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.getByText('Phenomenon')).toBeTruthy();
    expect(screen.getByText('Obscured thunderstorm(s)')).toBeTruthy();

    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
  });

  describe('getTAC', () => {
    jest.mock('lodash', () => ({
      debounce: jest.fn((fn) => {
        return fn;
      }),
    }));
    const auth = {
      username: 'testUser',
      token: 'testToken',
      refresh_token: 'testRefreshToken',
    };

    const testSigmet = fakeSigmetInWarningList[0];
    const testAirmet = fakeAirmetInWarningList[0];

    beforeEach(() => {
      global.fetch = jest.fn();
    });

    afterEach(() => {
      jest.resetAllMocks();
    });

    it('should return TAC data for Sigmet when the request is successful', async () => {
      (global.fetch as jest.Mock).mockResolvedValueOnce({
        status: 200,
        text: () => Promise.resolve('TAC message for Obscured thunderstorm(s)'),
      });

      const result = await getSigmetTAC(
        auth,
        testSigmet as unknown as AviationProduct,
      );

      expect(result.data).toBe('TAC message for Obscured thunderstorm(s)');
    });

    it('should throw an error when the request fails', async () => {
      (global.fetch as jest.Mock).mockResolvedValueOnce({
        status: 500,
        statusText: 'Internal Server Error',
      });

      await expect(
        getSigmetTAC(auth, testSigmet as unknown as AviationProduct),
      ).rejects.toThrow('Failed to fetch TAC data: Internal Server Error');
    });

    it('should return TAC data for Airmet when the request is successful', async () => {
      (global.fetch as jest.Mock).mockResolvedValueOnce({
        status: 200,
        text: () =>
          Promise.resolve('TAC message for Isolated thunderstorm(s) with hail'),
      });

      const result = await getAirmetTAC(
        auth,
        testAirmet as unknown as AviationProduct,
      );

      expect(result.data).toBe(
        'TAC message for Isolated thunderstorm(s) with hail',
      );
    });

    it('should throw an error when the request fails', async () => {
      (global.fetch as jest.Mock).mockResolvedValueOnce({
        status: 500,
        statusText: 'Internal Server Error',
      });

      await expect(
        getAirmetTAC(auth, testAirmet as unknown as AviationProduct),
      ).rejects.toThrow('Failed to fetch TAC data: Internal Server Error');
    });
  });

  describe('warningLevelFeatureColors', () => {
    it('should return an empty FeatureCollection if geoJSON is invalid', () => {
      const result = warningLevelFeatureColors(undefined, undefined);
      expect(result).toEqual({
        type: 'FeatureCollection',
        features: [],
      });
    });

    it('should apply colors for aviation warnings based on isStartGeometry', () => {
      const geoJSON: FeatureCollection<Geometry, GeoJsonProperties> = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: { isStartGeometry: true },
            geometry: { type: 'Point', coordinates: [0, 0] },
          },
          {
            type: 'Feature',
            properties: { isStartGeometry: false },
            geometry: { type: 'Point', coordinates: [1, 1] },
          },
        ],
      };

      const result = warningLevelFeatureColors(undefined, geoJSON, true);

      expect(result.features[0].properties).toMatchObject({
        fill: '#E27000',
        stroke: '#E27000',
        'fill-opacity': 0.2,
        'stroke-width': 2,
        'stroke-opacity': 1,
      });

      expect(result.features[1].properties).toMatchObject({
        fill: '#800080',
        stroke: '#800080',
        'fill-opacity': 0.2,
        'stroke-width': 2,
        'stroke-opacity': 1,
      });
    });

    it('should apply colors for public warnings based on level', () => {
      const geoJSON: FeatureCollection<Geometry, GeoJsonProperties> = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: { type: 'Point', coordinates: [0, 0] },
          },
        ],
      };

      const result = warningLevelFeatureColors(WarningLevel.extreme, geoJSON);

      expect(result.features[0].properties).toMatchObject({
        fill: '#D62A20',
        stroke: '#D62A20',
        'fill-opacity': 0.2,
        'stroke-width': 2,
        'stroke-opacity': 1,
      });
    });

    it('should apply default colors if level is undefined', () => {
      const geoJSON: FeatureCollection<Geometry, GeoJsonProperties> = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: { type: 'Point', coordinates: [0, 0] },
          },
        ],
      };

      const result = warningLevelFeatureColors(undefined, geoJSON);

      expect(result.features[0].properties).toMatchObject({
        fill: '#0000FF',
        stroke: '#0000FF',
        'fill-opacity': 0.2,
        'stroke-width': 2,
        'stroke-opacity': 1,
      });
    });
  });
});
