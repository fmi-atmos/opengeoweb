/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { Paper, Theme } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import React from 'react';
import { WarningsThemeProvider } from '../../Providers/Providers';
import { NewWarningButton } from './NewWarningButton';

export default {
  title: 'components/PublicWarningList/NewWarningButton',
};

const Demo: React.FC<{ theme?: Theme }> = ({
  theme = lightTheme,
}): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={theme}>
      <Paper sx={{ width: '200px', height: '250px', padding: 2 }}>
        <NewWarningButton onCreateWarning={(): void => {}} isDefaultOpen />
      </Paper>
    </WarningsThemeProvider>
  );
};

export const NewWarningButtonLight = (): React.ReactElement => <Demo />;

NewWarningButtonLight.tags = ['snapshot'];

export const NewWarningButtonDark = (): React.ReactElement => (
  <Demo theme={darkTheme} />
);

NewWarningButtonDark.tags = ['snapshot'];
