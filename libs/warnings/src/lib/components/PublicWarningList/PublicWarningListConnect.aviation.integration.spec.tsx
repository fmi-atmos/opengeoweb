/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import userEvent from '@testing-library/user-event';
import { setupServer } from 'msw/node';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { PublicWarningListConnect } from './PublicWarningListConnect';
import { PublicWarningsFormDialogConnect } from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store/store';
import {
  BUTTON_AIRMET,
  BUTTON_SIGMET,
} from './NewWarningButton/NewWarningButton';
import { requestHandlers } from '../../utils/fakeApi/index';

jest.mock(
  'react-virtualized-auto-sizer',
  () =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ({ children }: any): React.ReactNode =>
      children({ height: 1800, width: 600 }),
);

const server = setupServer(...requestHandlers);

describe('src/components/PublicWarningList/PublicWarningListConnect - aviation', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  it('should show edit options for new sigmet warning', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    // open create new warning dialog
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-list-button_create'),
      }),
    );
    await userEvent.click(screen.getByText(BUTTON_SIGMET));
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    // should show edit options for new warning
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeTruthy();

    // New warning so no toggle visible until you save
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();

    expect(
      store.getState().publicWarningForm.selectedWarningId,
    ).toBeUndefined();
    expect(store.getState().publicWarningForm.warningType).toEqual('sigmet');

    // close create new warning dialog
    await userEvent.click(
      screen.getByRole('button', {
        name: 'Close',
      }),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();
  });

  it('should show edit options for new airmet warning', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    // open create new warning dialog
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-list-button_create'),
      }),
    );
    await userEvent.click(screen.getByText(BUTTON_AIRMET));
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    // should show edit options for new warning
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeTruthy();

    // New warning so no toggle visible until you save
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();

    expect(
      store.getState().publicWarningForm.selectedWarningId,
    ).toBeUndefined();
    expect(store.getState().publicWarningForm.warningType).toEqual('airmet');

    // close create new warning dialog
    await userEvent.click(
      screen.getByRole('button', {
        name: 'Close',
      }),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();
  });

  it('should switch between create new sigmet and create new airmet and reset the form values', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    // open create new warning dialog
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-list-button_create'),
      }),
    );
    await userEvent.click(screen.getByText(BUTTON_SIGMET));
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    expect(store.getState().publicWarningForm.warningType).toEqual('sigmet');

    // Choose Observed
    const obsField = screen.getByTestId('isObservationOrForecast-OBS');
    fireEvent.click(obsField);
    await waitFor(() =>
      expect(
        (
          screen.getByTestId('isObservationOrForecast-OBS')
            .firstChild as HTMLElement
        ).classList,
      ).toContain('Mui-checked'),
    );

    // Switch to create warning for Airmet
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-list-button_create'),
      }),
    );
    await userEvent.click(screen.getByText(BUTTON_AIRMET));
    await userEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    expect(store.getState().publicWarningForm.warningType).toEqual('airmet');
    await waitFor(() =>
      expect(
        (
          screen.getByTestId('isObservationOrForecast-OBS')
            .firstChild as HTMLElement
        ).classList,
      ).not.toContain('Mui-checked'),
    );
  });

  it('should click the Renew option and check that the form is editable', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    // Set the domain filter to aviation
    const domainChipButton = screen.getByRole('button', {
      name: `${translateKeyOutsideComponents('filter-domain')} 1`,
    });
    fireEvent.click(domainChipButton);

    fireEvent.click(screen.getByRole('checkbox', { name: 'Aviation' }));
    fireEvent.click(screen.getByRole('checkbox', { name: 'Public' }));

    // Close the domain filter dialog
    const closeButton = screen.getByRole('button', { name: 'Close' });
    fireEvent.click(closeButton);

    // Collapse the TODO section
    const todoCollapseButton = screen.getByTestId('warninglist-TODO');
    fireEvent.click(todoCollapseButton);

    // Collapse the DRAFT section
    const draftCollapseButton = screen.getByTestId('warninglist-DRAFT');
    fireEvent.click(draftCollapseButton);

    // Expand the EXPIRED section
    const expiredCollapseButton = screen.getByTestId('warninglist-EXPIRED');
    fireEvent.click(expiredCollapseButton);

    // Wait for the warnings to be loaded
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    // Wait for the options buttons to be available<
    const optionsButtons = await screen.findAllByLabelText('Options');

    // Open the options menu for the warning
    const optionsButton = optionsButtons[0];
    await userEvent.click(optionsButton);

    // Click the Renew option
    const renewButton = await screen.findByText(
      translateKeyOutsideComponents('warning-list-button-renew'),
    );
    await userEvent.click(renewButton);

    // Check that the form is editable
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(store.getState().publicWarningForm.warningType).toEqual('sigmet');

    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeTruthy();
  });

  it('should click the Duplicate option and check that the form is editable', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    // Set the domain filter to aviation
    const domainChipButton = screen.getByRole('button', {
      name: `${translateKeyOutsideComponents('filter-domain')} 1`,
    });
    fireEvent.click(domainChipButton);

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    fireEvent.click(screen.getByRole('checkbox', { name: 'Aviation' }));
    fireEvent.click(screen.getByRole('checkbox', { name: 'Public' }));

    // Close the domain filter dialog
    const closeButton = screen.getByRole('button', { name: 'Close' });
    fireEvent.click(closeButton);

    // Collapse the TODO section
    const todoCollapseButton = screen.getByTestId('warninglist-TODO');
    fireEvent.click(todoCollapseButton);

    // Collapse the DRAFT section
    const draftCollapseButton = screen.getByTestId('warninglist-DRAFT');
    fireEvent.click(draftCollapseButton);

    // Expand the EXPIRED section
    const expiredCollapseButton = screen.getByTestId('warninglist-EXPIRED');
    fireEvent.click(expiredCollapseButton);

    // Wait for the options buttons to be available
    const optionsButtons = await screen.findAllByLabelText('Options');

    // Open the options menu for the warning
    const optionsButton = optionsButtons[0];
    await userEvent.click(optionsButton);

    // Click the Duplicate option
    const duplicateButton = await screen.findByText(
      translateKeyOutsideComponents('warning-list-button-duplicate'),
    );
    await userEvent.click(duplicateButton);

    // Check that the form is editable
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(store.getState().publicWarningForm.warningType).toEqual('sigmet');

    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeTruthy();
  });
});
