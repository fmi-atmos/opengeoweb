/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { breakpoints, darkTheme } from '@opengeoweb/theme';
import React from 'react';
import { Box } from '@mui/system';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { PublicWarningList, PublicWarningListProps } from './PublicWarningList';
import { combinedWarnings } from '../../utils/fakeApi/index';
import { initWarnI18n } from '../../utils/i18n';
import { FilterState, useWarningFilters } from '../FilterList/filter-hooks';
import { initialFilters } from '../FilterList/filter-utils';
import { Warning } from '../../store/publicWarningForm/types';

export default {
  title: 'components/PublicWarningList',
};

initWarnI18n();

const lastUpdatedTime = '13:45';

const defaultFilters: FilterState = {
  ...initialFilters,
  source: {
    unspecified: true,
    'PASCAL-ECMWF-EPS': true,
  },
  incident: {
    unspecified: true,
    '-1': true,
    T01: true,
    Z01: true,
    U01: true,
    U02: true,
  },
};
const useDefaultProps = (
  warnings: Warning[] = combinedWarnings,
): PublicWarningListProps => {
  const filterHookState = useWarningFilters(warnings, () => {}, defaultFilters);
  const filteredWarnings = warnings.filter(filterHookState.byFilterState);
  const [expandedSections, setExpandedSections] = React.useState<{
    TODO: boolean;
    DRAFT: boolean;
    PUBLISHED: boolean;
    EXPIRED: boolean;
  }>({
    TODO: true,
    DRAFT: true,
    PUBLISHED: true,
    EXPIRED: true,
  });

  return {
    filterHookState,
    warnings: filteredWarnings,
    lastUpdatedTime,
    expandedSections,
    setExpandedSections: setExpandedSections as (
      expandedSections: Record<string, boolean>,
    ) => void,
  };
};

const useAviationProps = (): PublicWarningListProps => {
  const aviationWarnings = combinedWarnings;
  const aviationFilters = {
    ...defaultFilters,
    domain: { aviation: true, public: false, maritime: false },
  };
  const filterHookState = useWarningFilters(
    aviationWarnings,
    () => null,
    aviationFilters,
  );
  const filteredWarnings = aviationWarnings.filter(
    filterHookState.byFilterState,
  );
  const [expandedSections, setExpandedSections] = React.useState<{
    TODO: boolean;
    DRAFT: boolean;
    PUBLISHED: boolean;
    EXPIRED: boolean;
  }>({
    TODO: true,
    DRAFT: true,
    PUBLISHED: true,
    EXPIRED: true,
  });

  return {
    filterHookState,
    warnings: filteredWarnings,
    lastUpdatedTime,
    expandedSections,
    setExpandedSections: setExpandedSections as (
      expandedSections: Record<string, boolean>,
    ) => void,
  };
};

const usePublicAndAviationProps = (): PublicWarningListProps => {
  const savedFilters = {
    ...defaultFilters,
    domain: { aviation: true, public: true, maritime: false },
    level: initialFilters.level,
  };
  const warnings = combinedWarnings;
  const filterHookState = useWarningFilters(warnings, () => null, savedFilters);

  const filteredWarnings = warnings.filter(filterHookState.byFilterState);
  const [expandedSections, setExpandedSections] = React.useState<{
    TODO: boolean;
    DRAFT: boolean;
    PUBLISHED: boolean;
    EXPIRED: boolean;
  }>({
    TODO: true,
    DRAFT: true,
    PUBLISHED: true,
    EXPIRED: true,
  });

  return {
    filterHookState,
    warnings: filteredWarnings,
    lastUpdatedTime,
    expandedSections,
    setExpandedSections: setExpandedSections as (
      expandedSections: Record<string, boolean>,
    ) => void,
  };
};

export const PublicWarningListLight = (): React.ReactElement => {
  const defaultProps = useDefaultProps();
  return (
    <Box sx={{ backgroundColor: '#f5f5f5', padding: 1, height: '100vh' }}>
      <WarningsThemeStoreProvider>
        <PublicWarningList
          selectedWarningId="b6daea16-46db-497a-865e-9e7320ad8d12"
          {...defaultProps}
        />
      </WarningsThemeStoreProvider>
    </Box>
  );
};
PublicWarningListLight.tags = ['snapshot'];
PublicWarningListLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6560c48564d81b7bd8be19d2',
    },
  ],
};

export const PublicWarningListDark = (): React.ReactElement => {
  const defaultProps = useDefaultProps();
  return (
    <Box sx={{ backgroundColor: '#2b2b2b', padding: 1, height: '100vh' }}>
      <WarningsThemeStoreProvider theme={darkTheme}>
        <PublicWarningList
          selectedWarningId="b6daea16-46db-497a-865e-9e7320ad8d12"
          {...defaultProps}
        />
      </WarningsThemeStoreProvider>
    </Box>
  );
};
PublicWarningListDark.tags = ['snapshot'];
PublicWarningListDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6560c48564d81b7bd8be19d2',
    },
  ],
};

export const PublicWarningListAviation = (): React.ReactElement => {
  const aviationProps = useAviationProps();
  return (
    <Box sx={{ backgroundColor: '#f5f5f5', padding: 1, height: '100vh' }}>
      <WarningsThemeStoreProvider>
        <PublicWarningList
          selectedWarningId="b6daea16-46db-497a-865e-9e7320ad8d12"
          {...aviationProps}
        />
      </WarningsThemeStoreProvider>
    </Box>
  );
};
PublicWarningListAviation.tags = ['snapshot'];
PublicWarningListAviation.parameters = {
  zeplinLink: [
    {
      name: 'Aviation warnings',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6560c48564d81b7bd8be19d2',
    },
  ],
};

export const PublicWarningListPublicAndAviation = (): React.ReactElement => {
  const publicAndAviationProps = usePublicAndAviationProps();
  return (
    <Box sx={{ backgroundColor: '#f5f5f5', padding: 1, height: '100vh' }}>
      <WarningsThemeStoreProvider>
        <PublicWarningList
          selectedWarningId="b6daea16-46db-497a-865e-9e7320ad8d12"
          {...publicAndAviationProps}
        />
      </WarningsThemeStoreProvider>
    </Box>
  );
};
PublicWarningListPublicAndAviation.tags = ['snapshot'];
PublicWarningListPublicAndAviation.parameters = {
  zeplinLink: [
    {
      name: 'Public and Aviation warnings',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6560c48564d81b7bd8be19d2',
    },
  ],
};

export const PublicWarningListError = (): React.ReactElement => {
  const defaultProps = useDefaultProps([]);
  return (
    <WarningsThemeStoreProvider>
      <PublicWarningList
        error={new Error('Something went wrong')}
        {...defaultProps}
      />
    </WarningsThemeStoreProvider>
  );
};

export const PublicWarningListLoading = (): React.ReactElement => {
  const defaultProps = useDefaultProps([]);
  return (
    <WarningsThemeStoreProvider>
      <PublicWarningList isLoading {...defaultProps} />
    </WarningsThemeStoreProvider>
  );
};

export const PublicWarningListSmallLight = (): React.ReactElement => {
  return (
    <Box sx={{ width: breakpoints.tablet, height: 1500 }}>
      <PublicWarningListLight />
    </Box>
  );
};
PublicWarningListSmallLight.tags = ['snapshot'];

export const PublicWarningListSmallDark = (): React.ReactElement => {
  return (
    <Box sx={{ width: breakpoints.tablet, height: 1500 }}>
      <PublicWarningListDark />
    </Box>
  );
};
PublicWarningListSmallDark.tags = ['snapshot'];
