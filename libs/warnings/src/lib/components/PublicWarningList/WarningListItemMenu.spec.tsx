/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { TFunction } from 'i18next';
import {
  CancelAirmet,
  CancelSigmet,
  ProductStatus,
  Sigmet,
} from '@opengeoweb/api';
import {
  Warning,
  PublicWarningStatus,
  AirmetWarning,
  SigmetWarning,
} from '../../store/publicWarningForm/types';
import WarningListItemMenu from './WarningListItemMenu';

const mockT = ((key: string) => key) as unknown as TFunction;

const mockWarning: Warning = {
  id: '1',
  status: PublicWarningStatus.PUBLISHED,
  warningDetail: {
    validFrom: '2023-12-01T00:00:00Z',
    validUntil: '2023-12-31T23:59:59Z',
    phenomenon: '',
    areas: [],
    level: '',
    probability: 0,
    descriptionOriginal: '',
    descriptionTranslation: '',
  },
  type: 'public',
};

const mockAviationWarning: SigmetWarning = {
  id: '1',
  productStatus: ProductStatus.PUBLISHED,
  warningDetail: {
    validFrom: '2023-12-01T00:00:00Z',
    validUntil: '2023-12-31T23:59:59Z',
    phenomenon: '',
    areas: [],
    level: '',
    probability: 0,
    descriptionOriginal: '',
    descriptionTranslation: '',
  },
  sigmetAirmetDetails: {
    sequence: '123',
  } as Sigmet,
  type: 'sigmet',
};

const mockCancelSigmetWarning: SigmetWarning = {
  id: '1',
  productStatus: ProductStatus.PUBLISHED,
  warningDetail: {
    validFrom: '2023-12-01T00:00:00Z',
    validUntil: '2023-12-31T23:59:59Z',
    phenomenon: '',
    areas: [],
    level: '',
    probability: 0,
    descriptionOriginal: '',
    descriptionTranslation: '',
  },
  sigmetAirmetDetails: {
    cancelsSigmetSequenceId: '123',
  } as CancelSigmet,
  type: 'sigmet',
};

const mockCancelAirmetWarning: AirmetWarning = {
  id: '2',
  productStatus: ProductStatus.PUBLISHED,
  warningDetail: {
    validFrom: '2023-12-01T00:00:00Z',
    validUntil: '2023-12-31T23:59:59Z',
    phenomenon: '',
    areas: [],
    level: '',
    probability: 0,
    descriptionOriginal: '',
    descriptionTranslation: '',
  },
  sigmetAirmetDetails: {
    cancelsAirmetSequenceId: '456',
  } as CancelAirmet,
  type: 'airmet',
};

const mockActions = {
  onEditWarning: jest.fn(),
  onDeleteWarning: jest.fn(),
  onDeleteReviewWarning: jest.fn(),
  onExpireWarning: jest.fn(),
  onWithdrawWarning: jest.fn(),
  onAviationWarning: jest.fn(),
};

describe('WarningListItemMenu', () => {
  it('should render menu items for a published warning', () => {
    const menuItems = WarningListItemMenu({
      warning: mockWarning,
      t: mockT,
      ...mockActions,
    });

    expect(menuItems).toHaveLength(4);
    expect(menuItems[0].text).toBe('warning-list-button-update');
    expect(menuItems[1].text).toBe('warning-list-button-duplicate');
    expect(menuItems[2].text).toBe('warning-list-button-expire');
    expect(menuItems[3].text).toBe('warning-list-button-withdraw');
  });

  it('should render menu items for a draft warning', () => {
    const draftWarning = { ...mockWarning, status: PublicWarningStatus.DRAFT };
    const menuItems = WarningListItemMenu({
      warning: draftWarning,
      t: mockT,
      ...mockActions,
    });

    expect(menuItems).toHaveLength(3);
    expect(menuItems[0].text).toBe('object-manager-button-edit');
    expect(menuItems[1].text).toBe('warning-list-button-duplicate');
    expect(menuItems[2].text).toBe('object-manager-button-delete');
  });

  it('should render menu items for an expired warning', () => {
    const expiredWarning = {
      ...mockWarning,
      status: PublicWarningStatus.EXPIRED,
    };
    const menuItems = WarningListItemMenu({
      warning: expiredWarning,
      t: mockT,
      ...mockActions,
    });

    expect(menuItems).toHaveLength(1);
    expect(menuItems[0].text).toBe('warning-list-button-duplicate');
  });

  it('should render menu items for a TODO warning', () => {
    const todoWarning = { ...mockWarning, status: PublicWarningStatus.TODO };
    const menuItems = WarningListItemMenu({
      warning: todoWarning,
      t: mockT,
      ...mockActions,
    });

    expect(menuItems).toHaveLength(2);
    expect(menuItems[0].text).toBe('warning-list-button-use');
    expect(menuItems[1].text).toBe('object-manager-button-delete');
  });

  it('should render menu items for a published aviation warning', () => {
    const aviationWarning = {
      ...mockAviationWarning,
      status: undefined,
      productStatus: ProductStatus.PUBLISHED,
    };
    const menuItems = WarningListItemMenu({
      warning: aviationWarning,
      t: mockT,
      ...mockActions,
    });

    expect(menuItems).toHaveLength(2);
    expect(menuItems[0].text).toBe('warning-list-button-renew');
    expect(menuItems[1].text).toBe('warning-list-button-duplicate');
  });

  it('should call the correct action when a menu item is clicked', () => {
    const menuItems = WarningListItemMenu({
      warning: mockWarning,
      t: mockT,
      ...mockActions,
    });

    render(
      <div>
        {menuItems.map((item) => (
          <button key={item.text} type="button" onClick={item.action}>
            {item.text}
          </button>
        ))}
      </div>,
    );

    fireEvent.click(screen.getByText('warning-list-button-update'));
    expect(mockActions.onEditWarning).toHaveBeenCalledWith(
      expect.objectContaining({ id: '1' }),
    );

    fireEvent.click(screen.getByText('warning-list-button-duplicate'));
    expect(mockActions.onEditWarning).toHaveBeenCalledWith(
      expect.objectContaining({ id: '1' }),
    );

    fireEvent.click(screen.getByText('warning-list-button-expire'));
    expect(mockActions.onExpireWarning).toHaveBeenCalledWith(
      expect.objectContaining({ id: '1' }),
    );

    fireEvent.click(screen.getByText('warning-list-button-withdraw'));
    expect(mockActions.onWithdrawWarning).toHaveBeenCalledWith(
      expect.objectContaining({ id: '1' }),
    );
  });

  it('should not return any options if the warning is a cancel sigmet', () => {
    const menuItems = WarningListItemMenu({
      warning: mockCancelSigmetWarning,
      t: mockT,
      ...mockActions,
    });

    expect(menuItems).toHaveLength(0);
  });

  it('should not return any options if the warning is a cancel airmet', () => {
    const menuItems = WarningListItemMenu({
      warning: mockCancelAirmetWarning,
      t: mockT,
      ...mockActions,
    });

    expect(menuItems).toHaveLength(0);
  });
});
