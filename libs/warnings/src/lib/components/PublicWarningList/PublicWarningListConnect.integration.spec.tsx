/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import React from 'react';
import userEvent from '@testing-library/user-event';
import { dateUtils } from '@opengeoweb/shared';
import { setupServer } from 'msw/node';
import { http, HttpResponse } from 'msw';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import {
  drawingList,
  requestHandlers,
  warningList,
} from '../../utils/fakeApi/index';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { PublicWarningListConnect } from './PublicWarningListConnect';
import {
  PublicWarningsFormDialogConnect,
  warningFormConfirmationOptions,
} from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import { getWarningWithProposalId } from '../../utils/warningUtils';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { i18n, translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store/store';
import { publicWarningFormActions } from '../../store';

jest.mock(
  'react-virtualized-auto-sizer',
  () =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ({ children }: any): React.ReactNode =>
      children({ height: 1800, width: 600 }),
);

const server = setupServer(...requestHandlers);

const createApi = (): WarningsApi => {
  return {
    ...createFakeApi(),
    getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
      return new Promise((resolve) => {
        resolve({ data: drawingList });
      });
    },
  };
};

describe('src/components/PublicWarningList/PublicWarningListConnectIntegration', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  afterEach(() => {
    jest.restoreAllMocks();
    server.resetHandlers();
  });

  it('should show error if saving/updating of warning fails', async () => {
    const store = createMockStore();

    server.use(
      http.post(
        /^.*\/warnings\/(.*)$/,
        () => {
          return HttpResponse.json({ message: 'save error' }, { status: 500 });
        },
        { once: true },
      ),
    );

    // Ensure the form isn't dirty
    await act(() =>
      store.dispatch(
        publicWarningFormActions.setFormDirty({ isFormDirty: false }),
      ),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const user = userEvent.setup();
    // open create new warning dialog
    await user.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-list-button_create'),
      }),
    );
    await user.click(
      screen.getByText(
        translateKeyOutsideComponents('public-warning-title').toUpperCase(),
      ),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    const saveButton = screen.getByRole('button', {
      name: translateKeyOutsideComponents('warning-button-save'),
    });
    expect(saveButton).toBeTruthy();

    await user.click(saveButton);

    await waitFor(() => {
      expect(screen.getByText('save error')).toBeTruthy();
    });
  });
  it('should be able to use a warning and save it', async () => {
    const store = createMockStore();
    // Mock date so validity validation succeeds
    const now = '2023-12-18T14:00:00Z';
    jest.spyOn(dateUtils, 'utc').mockReturnValue(new Date(now));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[0]);
    await userEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents('warning-list-button-use'),
      ),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    expect(store.getState().publicWarningForm.warning).toEqual(
      getWarningWithProposalId(warningList[0]),
    );

    await userEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents('warning-button-save'),
      ),
    );
    await screen.findByText(
      translateKeyOutsideComponents('warning-save-succes'),
    );

    // correct api call should be made
    jest.restoreAllMocks();
  });

  it('should show warnings after loading', async () => {
    const store = createMockStore();

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByTestId('publicWarningList')).toBeTruthy();
    expect(screen.getByTestId('loading-bar')).toBeTruthy();

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    expect(screen.getAllByTestId('warning-DRAFT')).toHaveLength(5);
    expect(screen.getAllByTestId('warning-PUBLISHED')).toHaveLength(3);
    expect(screen.getAllByTestId('warning-TODO')).toHaveLength(5);
    // Expired is false by default, so should not be shown
    expect(screen.queryAllByTestId('warning-EXPIRED')).toHaveLength(0);
  });

  it('should show edit options for new public warning', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await screen.findAllByLabelText('Options');
    // open create new warning dialog
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-list-button_create'),
      }),
    );
    await userEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('public-warning-title').toUpperCase(),
      ),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    // should show edit options for new warning
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeTruthy();

    // New warning so no toggle visible until you save
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();

    expect(
      store.getState().publicWarningForm.selectedWarningId,
    ).toBeUndefined();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-no-object-selected'),
      ),
    ).toBeTruthy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('object-manager-object-selected'),
      ),
    ).toBeFalsy();

    // close create new warning dialog
    await userEvent.click(
      screen.getByRole('button', {
        name: 'Close',
      }),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();
  });

  it('should not show edit options for proposals', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    await screen.findAllByLabelText('Options');
    // open existing warning dialog
    const listItem = await screen.findByRole('button', {
      name: `warning item for ${warningList[0].id}`,
    });
    await userEvent.click(listItem);
    expect(store.getState().publicWarningForm.selectedWarningId).toEqual(
      warningList[0].id,
    );
    expect(
      screen.getByRole('button', {
        name: `warning item for ${warningList[0].id}`,
      }).classList,
    ).toContain('Mui-selected');
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    // should not show edit options for TODO
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();

    expect(
      screen.queryByText(
        translateKeyOutsideComponents('object-manager-no-object-selected'),
      ),
    ).toBeFalsy();
    expect(
      screen.getAllByText(warningList[0].warningDetail.areas![0].objectName),
    ).toBeTruthy();
    // close dialog
    await userEvent.click(screen.getByTestId('closeBtn'));
    expect(
      store.getState().publicWarningForm.selectedWarningId,
    ).toBeUndefined();
    expect(
      screen.getByRole('button', {
        name: `warning item for ${warningList[0].id}`,
      }).classList,
    ).not.toContain('Mui-selected');
  });

  it('should not show edit options for published warning', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    await screen.findAllByLabelText('Options');

    // open PUBLISHED warning dialog
    const warningPUBLISHED = await screen.findByRole('button', {
      name: `warning item for ${warningList[10].id}`,
    });
    await userEvent.click(warningPUBLISHED);
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();
  });

  it('should not show edit options for expired warning', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
    // Expand the EXPIRED section
    const expiredCollapseButton = await screen.findByTestId(
      'warninglist-EXPIRED',
    );
    fireEvent.click(expiredCollapseButton);

    // open EXPIRED warning dialog
    const warningEXPIRED = await screen.findByRole('button', {
      name: `warning item for ${warningList[13].id}`,
    });
    await userEvent.click(warningEXPIRED);
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-clear'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    ).toBeFalsy();
    expect(
      screen.queryByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    ).toBeFalsy();
  });

  it('should show confirmation dialog if changes are made in the form and user tries to close form', async () => {
    const store = createMockStore();

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(
      screen.queryByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const user = userEvent.setup();
    // open create new warning dialog
    await user.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-list-button_create'),
      }),
    );
    await user.click(
      screen.getByText(
        translateKeyOutsideComponents('public-warning-title').toUpperCase(),
      ),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeTruthy();

    // make a change
    fireEvent.mouseDown(screen.getByRole('combobox', { name: 'Probability' }));
    const menuItem = await screen.findByText('50 %');
    fireEvent.click(menuItem);

    // try to close it
    fireEvent.click(screen.getByTestId('closeBtn'));

    expect(warningFormConfirmationOptions(i18n.t).title).toBeTruthy();
    expect(warningFormConfirmationOptions(i18n.t).description).toBeTruthy();

    await user.click(
      screen.getByRole('button', {
        name: warningFormConfirmationOptions(i18n.t).confirmLabel,
      }),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
    expect(
      screen.queryByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeFalsy();
  });

  it('should show confirmation dialog if changes are made in the form and user tries to select other warning', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(
      screen.queryByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const user = userEvent.setup();
    // open create new warning dialog
    await user.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-list-button_create'),
      }),
    );
    await user.click(
      screen.getByText(
        translateKeyOutsideComponents('public-warning-title').toUpperCase(),
      ),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeTruthy();

    // make a change
    fireEvent.mouseDown(screen.getByRole('combobox', { name: 'Probability' }));
    const menuItem = await screen.findByText('50 %');
    fireEvent.click(menuItem);

    // try to open other warning
    const listItem = screen.getByRole('button', {
      name: `warning item for ${warningList[0].id}`,
    });
    await user.click(listItem);
    expect(
      store.getState().publicWarningForm.selectedWarningId,
    ).toBeUndefined();

    expect(
      screen.getByText(warningFormConfirmationOptions(i18n.t).title!),
    ).toBeTruthy();
    expect(
      screen.getByText(warningFormConfirmationOptions(i18n.t).description!),
    ).toBeTruthy();

    // cancel open other warning
    await user.click(
      screen.getByRole('button', {
        name: warningFormConfirmationOptions(i18n.t).cancelLabel,
      }),
    );

    expect(
      screen.queryByText(warningFormConfirmationOptions(i18n.t).title!),
    ).toBeFalsy();
    expect(
      screen.queryByText(warningFormConfirmationOptions(i18n.t).description!),
    ).toBeFalsy();
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
  });

  it('should be able to refetch warning list', async () => {
    const store = createMockStore();
    const mockGetWarnings = jest
      .fn()
      .mockResolvedValue(HttpResponse.json(warningList));
    server.use(http.get('warnings', mockGetWarnings));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByTestId('loading-bar')).toBeTruthy();

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    expect(mockGetWarnings).toHaveBeenCalledTimes(1);
    // press refresh
    await userEvent.click(screen.getByTestId('refresh-button'));
    await waitFor(() => {
      expect(mockGetWarnings).toHaveBeenCalledTimes(2);
    });
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
  });

  it('should show error if fetching of list fails', async () => {
    const store = createMockStore();
    server.use(
      http.get(
        'warnings',
        () => {
          return HttpResponse.json(
            { message: 'error message' },
            { status: 500 },
          );
        },
        { once: true },
      ),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByTestId('loading-bar')).toBeTruthy();

    await waitFor(() => {
      expect(screen.getByText('error message')).toBeTruthy();
    });
    expect(screen.queryByTestId('loading-bar')).toBeFalsy();
  });
});
