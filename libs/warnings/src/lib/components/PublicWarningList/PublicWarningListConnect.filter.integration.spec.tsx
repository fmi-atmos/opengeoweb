/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { setupServer } from 'msw/node';

import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { drawingList, requestHandlers } from '../../utils/fakeApi/index';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { PublicWarningListConnect } from './PublicWarningListConnect';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store/store';

jest.mock(
  'react-virtualized-auto-sizer',
  () =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ({ children }: any): React.ReactNode =>
      children({ height: 1800, width: 600 }),
);

const server = setupServer(...requestHandlers);

describe('src/components/PublicWarningList/PublicWarningListConnect - filtering', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  const createApi = (): WarningsApi => {
    return {
      ...createFakeApi(),
      getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
        return new Promise((resolve) => {
          resolve({ data: drawingList });
        });
      },
    };
  };

  it('should default show all warnings and not show any warnings when deselecting the All filter chip', async () => {
    const store = createMockStore();
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByTestId('publicWarningList')).toBeTruthy();
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
    expect(screen.getAllByTestId('warning-DRAFT')).toHaveLength(5);

    const nonSelectedStyle = 'MuiChip-outlined';
    const selectedStyle = 'MuiChip-filled';
    const allChip = screen.getByRole('button', {
      name: translateKeyOutsideComponents('filter-all'),
    });
    fireEvent.click(allChip);
    expect(allChip.classList).toContain(selectedStyle);
    expect(allChip.classList).not.toContain(nonSelectedStyle);

    fireEvent.click(allChip);
    expect(allChip.classList).not.toContain(selectedStyle);
    expect(allChip.classList).toContain(nonSelectedStyle);

    expect(screen.queryByTestId('warning-TODO')).toBeFalsy();
    expect(screen.queryByTestId('warning-DRAFT')).toBeFalsy();
    expect(screen.queryByTestId('warning-PUBLISHED')).toBeFalsy();
    expect(screen.queryByTestId('warning-EXPIRED')).toBeFalsy();
  });

  it('should update the list when deselecting all options for a filter chip', async () => {
    const store = createMockStore();

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByTestId('publicWarningList')).toBeTruthy();
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
    expect(screen.getAllByTestId('warning-DRAFT')).toHaveLength(5);

    const nonSelectedStyle = 'MuiChip-outlined';
    const selectedStyle = 'MuiChip-filled';
    const domainChip = screen.getByRole('button', {
      name: `${translateKeyOutsideComponents('filter-domain')} 1`,
    });
    fireEvent.click(domainChip);

    expect(domainChip.classList).toContain(selectedStyle);
    expect(domainChip.classList).not.toContain(nonSelectedStyle);

    fireEvent.click(
      screen.getByLabelText(translateKeyOutsideComponents('filter-select-all')),
    );
    fireEvent.click(
      screen.getByLabelText(translateKeyOutsideComponents('filter-select-all')),
    );

    expect(domainChip.classList).not.toContain(selectedStyle);
    expect(domainChip.classList).toContain(nonSelectedStyle);

    expect(screen.queryByTestId('warning-TODO')).toBeFalsy();
    expect(screen.queryByTestId('warning-DRAFT')).toBeFalsy();
    expect(screen.queryByTestId('warning-PUBLISHED')).toBeFalsy();
    expect(screen.queryByTestId('warning-EXPIRED')).toBeFalsy();
  });

  it('should update the list when clicking a filter option and the only option', async () => {
    const store = createMockStore();

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByTestId('publicWarningList')).toBeTruthy();
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
    expect(screen.getAllByTestId('warning-DRAFT')).toHaveLength(5);

    const domainChipButton = screen.getByRole('button', {
      name: `${translateKeyOutsideComponents('filter-domain')} 1`,
    });
    fireEvent.click(domainChipButton);

    fireEvent.click(screen.getByRole('checkbox', { name: 'Public' }));

    expect(screen.queryAllByTestId('warning-TODO')).toHaveLength(0);

    await userEvent.hover(screen.getByRole('checkbox', { name: 'Public' }));
    fireEvent.click(
      await screen.findByText(translateKeyOutsideComponents('shared-only')),
    );
    expect(screen.queryAllByTestId('warning-DRAFT')).toHaveLength(5);
  });

  it('should update the list and level filter when selecting a domain filter option', async () => {
    const store = createMockStore();

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByTestId('publicWarningList')).toBeTruthy();
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
    expect(screen.getAllByTestId('warning-PUBLISHED')).toHaveLength(3);
    expect(screen.queryAllByTestId('warning-DRAFT')).toHaveLength(5);

    const levelChipButtonPublic = screen.getByRole('button', {
      name: `${translateKeyOutsideComponents('filter-level')} 4`,
    });
    fireEvent.click(levelChipButtonPublic);
    expect(
      screen.getByRole('checkbox', { name: '(not specified)' }),
    ).toBeTruthy();
    expect(screen.getByRole('checkbox', { name: 'Moderate' })).toBeTruthy();
    expect(screen.getByRole('checkbox', { name: 'Severe' })).toBeTruthy();
    expect(screen.getByRole('checkbox', { name: 'Extreme' })).toBeTruthy();
    expect(screen.queryByRole('checkbox', { name: 'Airmet' })).toBeFalsy();
    expect(screen.queryByRole('checkbox', { name: 'Sigmet' })).toBeFalsy();
    fireEvent.click(screen.getByRole('button', { name: 'Close' }));
    expect(screen.queryByRole('checkbox', { name: 'Moderate' })).toBeFalsy();

    const domainChipButton = screen.getByRole('button', {
      name: `${translateKeyOutsideComponents('filter-domain')} 1`,
    });
    fireEvent.click(domainChipButton);
    await userEvent.hover(screen.getByRole('checkbox', { name: 'Aviation' }));
    fireEvent.click(
      await screen.findByText(translateKeyOutsideComponents('shared-only')),
    );
    fireEvent.click(screen.getByRole('button', { name: 'Close' }));
    expect(screen.queryAllByTestId('warning-TODO')).toHaveLength(0);
    expect(screen.queryAllByTestId('warning-DRAFT')).toHaveLength(1);

    const levelChipButtonAviation = screen.getByRole('button', {
      name: `${translateKeyOutsideComponents('filter-level')} 3`,
    });
    fireEvent.click(levelChipButtonAviation);
    expect(
      screen.getByRole('checkbox', { name: '(not specified)' }),
    ).toBeTruthy();
    expect(screen.queryByRole('checkbox', { name: 'Moderate' })).toBeFalsy();
    expect(screen.queryByRole('checkbox', { name: 'Severe' })).toBeFalsy();
    expect(screen.queryByRole('checkbox', { name: 'Extreme' })).toBeFalsy();
    expect(screen.getByRole('checkbox', { name: 'Airmet' })).toBeTruthy();
    expect(screen.getByRole('checkbox', { name: 'Sigmet' })).toBeTruthy();
  });

  it('should show filters saved in preset', async () => {
    const store = createMockStore();
    const filters = {
      domain: { aviation: false, public: true, maritime: false },
      level: {
        unspecified: false,
        moderate: false,
        severe: true,
        extreme: false,
      },
      source: { unspecified: true },
      phenomenon: {
        unspecified: false,
        snowIce: true,
        wind: false,
        thunderstorm: false,
      },
      incident: { unspecified: true },
    };
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect
          panelId="panelId"
          presetFilterState={filters}
        />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByTestId('publicWarningList')).toBeTruthy();
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });
    expect(screen.queryAllByTestId('warning-TODO')).toHaveLength(0);
    expect(screen.queryAllByTestId('warning-DRAFT')).toHaveLength(1);
    expect(screen.queryAllByTestId('warning-PUBLISHED')).toHaveLength(1);
    expect(screen.queryAllByTestId('warning-EXPIRED')).toHaveLength(0);

    await screen.findByRole('button', {
      name: `${translateKeyOutsideComponents('filter-domain')} 1`,
    });
    await screen.findByRole('button', {
      name: `${translateKeyOutsideComponents('filter-phenomenon')} 5`,
    });
    await screen.findByRole('button', {
      name: `${translateKeyOutsideComponents('filter-level')} 1`,
    });
  });
});
