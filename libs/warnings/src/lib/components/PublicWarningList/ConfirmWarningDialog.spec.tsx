/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { ApiProvider } from '@opengeoweb/api';
import { API_NAME } from '../../utils/api';
import { WarningsThemeProvider } from '../Providers/Providers';

import { createApi as createFakeApi } from '../../utils/fakeApi';
import ConfirmWarningDialog from './ConfirmWarningDialog';
import { translateKeyOutsideComponents } from '../../utils/i18n';

describe('components/ConfirmWarningDialog', () => {
  it('renders correctly', () => {
    const props = {
      onClose: jest.fn(),
      title: 'test',
      description: 'this is a test description',
      request: jest.fn(),
      confirmLabel: 'confirm',
      callback: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <ConfirmWarningDialog {...props} />,
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(props.title)).toBeInTheDocument();
    expect(screen.getByText(props.description)).toBeInTheDocument();
    expect(screen.getByText(props.confirmLabel)).toBeInTheDocument();
  });

  it('do a callback when request is confirmed', async () => {
    const props = {
      onClose: jest.fn(),
      title: 'test',
      description: 'this is a test description',
      request: jest.fn(),
      confirmLabel: 'confirm',
      callback: jest.fn(),
    };

    render(
      <ApiProvider createApi={createFakeApi} name={API_NAME}>
        <WarningsThemeProvider>
          <ConfirmWarningDialog {...props} />,
        </WarningsThemeProvider>
      </ApiProvider>,
    );

    const confirmButton = screen.getByText(props.confirmLabel);
    fireEvent.click(confirmButton);

    expect(screen.getByTestId('confirm-dialog-spinner')).toBeTruthy();

    await waitFor(() => {
      expect(props.callback).toHaveBeenCalled();
    });
    expect(screen.queryByTestId('confirm-dialog-spinner')).toBeFalsy();
  });

  it('show error from backend when delete returns error', async () => {
    const message = 'something went wrong test message';
    const props = {
      onClose: jest.fn(),
      title: 'test',
      description: 'this is a test description',
      request: (): Promise<void> => Promise.reject(new Error(message)),
      confirmLabel: 'confirm',
      callback: jest.fn(),
    };

    render(
      <WarningsThemeProvider>
        <ConfirmWarningDialog {...props} />,
      </WarningsThemeProvider>,
    );

    const confirmButton = screen.getByText(props.confirmLabel);
    fireEvent.click(confirmButton);

    expect(screen.getByTestId('confirm-dialog-spinner')).toBeTruthy();

    await waitFor(() => {
      expect(screen.getByText(message)).toBeTruthy();
    });
    expect(props.callback).not.toHaveBeenCalled();
    expect(screen.queryByTestId('confirm-dialog-spinner')).toBeFalsy();
  });

  it('calls on close', async () => {
    const props = {
      onClose: jest.fn(),
      title: 'test',
      description: 'this is a test description',
      request: jest.fn(),
      confirmLabel: 'confirm',
      callback: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <ConfirmWarningDialog {...props} />,
      </WarningsThemeProvider>,
    );

    const cancelButton = screen.getByText(
      translateKeyOutsideComponents('warning-dialog-cancel'),
    );
    fireEvent.click(cancelButton);

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalled();
    });
  });
});
