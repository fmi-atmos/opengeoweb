/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import userEvent from '@testing-library/user-event';
import { dateUtils } from '@opengeoweb/shared';
import { setupServer } from 'msw/node';
import { HttpResponse, http } from 'msw';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import {
  drawingList,
  requestHandlers,
  warningList,
} from '../../utils/fakeApi/index';
import { PublicWarningListConnect } from './PublicWarningListConnect';
import { PublicWarningsFormDialogConnect } from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import {
  getWarningWithLinkedToId,
  getWarningWithoutIdStatusEditor,
} from '../../utils/warningUtils';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { PublicWarningStatus } from '../../store/publicWarningForm/types';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import { createMockStore } from '../../store/store';

jest.mock(
  'react-virtualized-auto-sizer',
  () =>
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ({ children }: any): React.ReactNode =>
      children({ height: 1800, width: 600 }),
);

const server = setupServer(...requestHandlers);

describe('src/components/PublicWarningList/PublicWarningListConnectIntegration - published actions', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  afterEach(() => {
    server.resetHandlers();
  });
  const createApi = (): WarningsApi => {
    return {
      ...createFakeApi(),
      getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
        return new Promise((resolve) => {
          resolve({ data: drawingList });
        });
      },
    };
  };

  it('should allow publishing a warning', async () => {
    const store = createMockStore();

    // Mock date so validity validation succeeds
    const now = '2023-12-05T14:00:00Z';
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
    jest.spyOn(dateUtils, 'getNowUtc').mockReturnValue(new Date(now));
    jest.spyOn(dateUtils, 'utc').mockReturnValue(new Date(now));
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[7].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    fireEvent.click(options[7]);

    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    ).toBeTruthy();
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(screen.getByLabelText('Switch mode')).toBeTruthy();

    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    );

    // Snackbar should be shown
    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('warning-publish-succes'),
        ),
      ).toBeTruthy();
    });

    // Dialog should be closed
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
  });

  it('should show an error if publishing a warning fails', async () => {
    const store = createMockStore();
    server.use(
      http.post(/^.*\/warnings\/(.*)$/, () => {
        return HttpResponse.json({ message: 'publish error' }, { status: 500 });
      }),
    );

    // Mock date so validity validation succeeds
    const now = '2023-12-05T14:00:00Z';
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
    jest.spyOn(dateUtils, 'getNowUtc').mockReturnValue(new Date(now));
    jest.spyOn(dateUtils, 'utc').mockReturnValue(new Date(now));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[7].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    fireEvent.click(options[7]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    ).toBeTruthy();
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(screen.getByLabelText('Switch mode')).toBeTruthy();

    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-publish'),
      }),
    );

    await waitFor(() => {
      expect(screen.getByText('publish error')).toBeTruthy();
    });

    // Dialog should still be open
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
  });

  it('should be able to save a draft published warning', async () => {
    const store = createMockStore();

    const warning = warningList[10];
    // Mock date so validity validation succeeds
    const now = '2023-12-05T14:00:00Z';
    jest.spyOn(dateUtils, 'utc').mockReturnValue(new Date(now));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[10].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[10]);
    const update = await screen.findByText(
      translateKeyOutsideComponents('warning-list-button-update'),
    );
    await userEvent.click(update);

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    // Snackbar should be shown
    await screen.findByText(
      translateKeyOutsideComponents('warning-editor-add-editor'),
    );

    expect(store.getState().publicWarningForm.warning).toEqual({
      ...getWarningWithLinkedToId(warning),
      editor: 'user.name',
    });

    expect(screen.getByLabelText('Switch mode')).toBeTruthy();

    const save = await screen.findByText(
      translateKeyOutsideComponents('warning-button-save'),
    );
    await userEvent.click(save);
    // Snackbar should be shown
    await screen.findByText(
      translateKeyOutsideComponents('warning-save-succes'),
    );

    jest.restoreAllMocks();
  });

  it('should be able to publish a draft published warning', async () => {
    const store = createMockStore();

    // Mock date so validity validation succeeds
    const now = '2024-05-28T14:00:00Z';
    jest.spyOn(dateUtils, 'utc').mockReturnValue(new Date(now));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[6].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[6]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    ).toBeTruthy();
    await userEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('object-manager-button-edit'),
      ),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    expect(store.getState().publicWarningForm.warning).toEqual(warningList[6]);

    await userEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents('warning-button-publish'),
      ),
    );

    // Snackbar should be shown
    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('warning-publish-succes'),
        ),
      ).toBeTruthy();
    });

    jest.restoreAllMocks();
  });

  it('should be able to save a duplicate of a draft published warning', async () => {
    const store = createMockStore();

    // Mock date so validity validation succeeds
    const now = '2024-05-28T14:00:00Z';
    jest.spyOn(dateUtils, 'utc').mockReturnValue(new Date(now));

    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[6].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[6]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-duplicate'),
      ),
    ).toBeTruthy();
    await userEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-duplicate'),
      ),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    expect(store.getState().publicWarningForm.warning).toEqual(
      getWarningWithoutIdStatusEditor(warningList[6]),
    );

    await userEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents('warning-button-save'),
      ),
    );

    // Snackbar should be shown
    await waitFor(() => {
      expect(
        screen.getByText(translateKeyOutsideComponents('warning-save-succes')),
      ).toBeTruthy();
    });

    jest.restoreAllMocks();
  });

  it('should be able to publish a duplicate of a published warning', async () => {
    const store = createMockStore();

    // Mock date so validity validation succeeds
    const now = '2023-12-05T14:00:00Z';
    jest.spyOn(dateUtils, 'utc').mockReturnValue(new Date(now));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[11].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[11]);
    expect(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-duplicate'),
      ),
    ).toBeTruthy();
    await userEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('warning-list-button-duplicate'),
      ),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    expect(store.getState().publicWarningForm.warning).toEqual(
      getWarningWithoutIdStatusEditor(warningList[11]),
    );

    await userEvent.click(
      await screen.findByText(
        translateKeyOutsideComponents('warning-button-publish'),
      ),
    );

    // Snackbar should be shown
    await waitFor(() => {
      expect(
        screen.getByText(
          translateKeyOutsideComponents('warning-publish-succes'),
        ),
      ).toBeTruthy();
    });

    expect(screen.queryByText('publish error')).toBeFalsy();

    // Dialog should still be open
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
    jest.restoreAllMocks();
  });

  it('should be able to publish an update of a published warning', async () => {
    const store = createMockStore();

    const warning = warningList[12];
    // Mock date so validity validation succeeds
    const now = '2023-12-05T14:00:00Z';
    jest.spyOn(dateUtils, 'utc').mockReturnValue(new Date(now));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[12].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[12]);
    const update = await screen.findByText(
      translateKeyOutsideComponents('warning-list-button-update'),
    );
    await userEvent.click(update);

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    expect(store.getState().publicWarningForm.warning).toEqual({
      ...getWarningWithLinkedToId(warning),
      editor: 'user.name',
    });

    expect(screen.getByLabelText('Switch mode')).toBeTruthy();

    // Snackbar should be shown
    await screen.findByText(
      translateKeyOutsideComponents('warning-editor-add-editor'),
    );

    const publish = await screen.findByText(
      translateKeyOutsideComponents('warning-button-publish'),
    );
    await userEvent.click(publish);
    // Snackbar should be shown
    await screen.findByText(
      translateKeyOutsideComponents('warning-publish-succes'),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
  });

  it('should be able to expire an active published warning', async () => {
    const store = createMockStore();

    const warning = warningList[12];

    // Mock date so warning is active
    const now = '2023-12-06T15:00:00Z';
    jest.spyOn(dateUtils, 'getNowUtc').mockReturnValue(new Date(now));

    const updateWarning = jest.fn();
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        updateWarning,
      };
    };
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[12].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[12]);

    const expire = (await screen.findAllByRole('menuitem'))[2];
    expect(expire.textContent).toEqual(
      translateKeyOutsideComponents('warning-list-button-expire'),
    );
    expect(expire.getAttribute('aria-disabled')).not.toEqual('true');

    await userEvent.click(expire);
    const confirm = await screen.findByText(
      translateKeyOutsideComponents('warning-dialog-expire-confirm'),
    );
    await userEvent.click(confirm);

    // Snackbar should be shown
    await screen.findByText(
      translateKeyOutsideComponents('warning-dialog-expire-succes'),
    );

    // correct api call should be made
    expect(updateWarning).toHaveBeenCalledWith(warning.id, {
      ...warning,
      status: PublicWarningStatus.EXPIRED,
    });
  });

  it('should disable expire option for published warning that is not active yet', async () => {
    const store = createMockStore();

    // Mock date so warning is not active yet
    const now = '2023-12-05T14:00:00Z';
    jest.spyOn(dateUtils, 'getNowUtc').mockReturnValue(new Date(now));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[12].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[12]);
    const expire = (await screen.findAllByRole('menuitem'))[2];
    expect(expire.textContent).toEqual(
      translateKeyOutsideComponents('warning-list-button-expire'),
    );
    expect(expire.getAttribute('aria-disabled')).toEqual('true');
  });

  it('should be able to withdraw an active published warning', async () => {
    const store = createMockStore();

    const warning = warningList[12];

    // Mock date so warning is active
    const now = '2023-12-06T15:00:00Z';
    jest.spyOn(dateUtils, 'getNowUtc').mockReturnValue(new Date(now));

    const updateWarning = jest.fn();
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        updateWarning,
      };
    };
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[12].id}`,
    });
    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[12]);

    const withdraw = (await screen.findAllByRole('menuitem'))[3];
    expect(withdraw.textContent).toEqual(
      translateKeyOutsideComponents('warning-list-button-withdraw'),
    );
    expect(withdraw.getAttribute('aria-disabled')).not.toEqual('true');

    await userEvent.click(withdraw);
    const confirm = await screen.findByText(
      translateKeyOutsideComponents('warning-dialog-withdraw-confirm'),
    );
    await userEvent.click(confirm);

    // Snackbar should be shown
    await screen.findByText(
      translateKeyOutsideComponents('warning-dialog-withdraw-succes'),
    );

    // correct api call should be made
    expect(updateWarning).toHaveBeenCalledWith(warning.id, {
      ...warning,
      status: PublicWarningStatus.WITHDRAWN,
    });
  });

  it('should be able to withdraw a not yet active published warning', async () => {
    const store = createMockStore();

    const warning = warningList[12];

    // Mock date so warning is active
    const now = '2023-12-06T13:00:00Z';
    jest.spyOn(dateUtils, 'getNowUtc').mockReturnValue(new Date(now));

    const updateWarning = jest.fn();
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        updateWarning,
      };
    };
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect panelId="panelId" />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    await screen.findByRole('button', {
      name: `warning item for ${warningList[12].id}`,
    });

    const options = await screen.findAllByLabelText('Options');
    await userEvent.click(options[12]);

    const withdraw = (await screen.findAllByRole('menuitem'))[3];
    expect(withdraw.textContent).toEqual(
      translateKeyOutsideComponents('warning-list-button-withdraw'),
    );
    expect(withdraw.getAttribute('aria-disabled')).not.toEqual('true');

    await userEvent.click(withdraw);
    const confirm = await screen.findByText(
      translateKeyOutsideComponents('warning-dialog-withdraw-confirm'),
    );
    await userEvent.click(confirm);

    // Snackbar should be shown
    await screen.findByText(
      translateKeyOutsideComponents('warning-dialog-withdraw-succes'),
    );

    // correct api call should be made
    expect(updateWarning).toHaveBeenCalledWith(warning.id, {
      ...warning,
      status: PublicWarningStatus.WITHDRAWN,
    });
  });
});
