/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import {
  OpenLayersFeatureLayer,
  OpenLayersLayerProps,
  OpenLayersLayer,
  OpenLayersMapView,
  defaultLayers,
  genericOpenLayersFeatureStyle,
  OpenLayersZoomControl,
  MapControls,
  viewUtils,
} from '@opengeoweb/webmap-react';
import { LayerType, generateMapId } from '@opengeoweb/webmap';

import { FeatureCollection } from 'geojson';
import { MapPreset } from '@opengeoweb/store';
import { useRef } from 'react';
import { View } from 'ol';

interface WarningsMapViewProps {
  geojsonFeature: FeatureCollection | undefined;
  defaultMapPreset?: MapPreset;
  isSnapshot?: boolean;
}
export const WarningsMapView: React.FC<WarningsMapViewProps> = ({
  geojsonFeature,
  defaultMapPreset,
  isSnapshot = false,
}: WarningsMapViewProps) => {
  const defaultBbox = defaultMapPreset?.proj;
  const defaultBaseLayer =
    defaultMapPreset?.layers?.find(
      (layer) => layer.layerType === LayerType.baseLayer,
    ) || defaultLayers.baseLayerGrey;
  const defaultOverLayer =
    defaultMapPreset?.layers?.find(
      (layer) => layer.layerType === LayerType.overLayer,
    ) || defaultLayers.overLayer;

  const mapId = React.useRef<string>(
    `WarningsMapView_${generateMapId()}`,
  ).current;

  const warningLayer: OpenLayersLayerProps = {
    id: `${mapId}geojsonfeature`,
    geojson: geojsonFeature,
    layerType: LayerType.featureLayer,
  };

  const viewRef = useRef<View>(viewUtils.makeView());

  React.useEffect(() => {
    if (viewRef.current.getProjection().getCode() !== defaultBbox?.srs) {
      viewRef.current = viewUtils.makeView(defaultBbox?.srs);
    }
    defaultBbox && defaultBbox.bbox
      ? viewUtils.setViewFromExtent(viewRef.current, defaultBbox?.bbox)
      : viewUtils.setViewFromFeature(viewRef.current, geojsonFeature);
  }, [defaultBbox, geojsonFeature]);

  return (
    <div style={{ height: '200px', width: '100%' }}>
      <OpenLayersMapView view={viewRef.current} holdShiftToScroll={true}>
        <MapControls style={{ top: 0 }}>
          <OpenLayersZoomControl
            homeExtent={viewRef.current?.calculateExtent()}
          />
        </MapControls>
        {!isSnapshot && (
          <OpenLayersLayer {...defaultBaseLayer} id={`${mapId}_baseLayer`} />
        )}
        {warningLayer.geojson && (
          <OpenLayersFeatureLayer
            featureCollection={warningLayer.geojson}
            style={genericOpenLayersFeatureStyle}
            zIndex={1000}
          />
        )}
        {!isSnapshot && (
          <OpenLayersLayer {...defaultOverLayer} id={`${mapId}_overlayer`} />
        )}
      </OpenLayersMapView>
    </div>
  );
};
