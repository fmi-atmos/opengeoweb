/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render } from '@testing-library/react';
import { defaultBbox } from '@opengeoweb/core';
import { viewUtils } from '@opengeoweb/webmap-react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { WarningsMapView } from './WarningsMapView';
import { WarningsI18nProvider } from '../Providers/Providers';

const simplePolygonGeoJSONHalfOfNL: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [4.921875, 51.767839887322154],
            [4.7900390625, 49.97242235423708],
            [7.679443359375, 49.79544988802771],
            [7.888183593749999, 51.74743863117572],
            [5.86669921875, 51.984880139916626],
            [4.921875, 51.767839887322154],
          ],
        ],
      },
    },
  ],
};

const simplePolygonGeoJSONModified: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [4.921875, 51.767839887322154],
            [4.7900390625, 49.97242235423708],
            [27.679443359375, 59.79544988802771],
            [7.888183593749999, 51.74743863117572],
            [5.86669921875, 51.984880139916626],
            [4.921875, 51.767839887322154],
          ],
        ],
      },
    },
  ],
};

describe('components/PublicWarningsForm/WarningsMapView', () => {
  describe('WarningsMapView', () => {
    it('should zoom to provided geojson if no defaultBbox', () => {
      const { rerender } = render(
        <QueryClientProvider client={new QueryClient()}>
          <WarningsI18nProvider>
            <WarningsMapView geojsonFeature={simplePolygonGeoJSONHalfOfNL} />
          </WarningsI18nProvider>
        </QueryClientProvider>,
      );

      const setViewFromFeatureSpy = jest.spyOn(
        viewUtils!,
        'setViewFromFeature',
      );
      rerender(
        <QueryClientProvider client={new QueryClient()}>
          <WarningsI18nProvider>
            <WarningsMapView geojsonFeature={simplePolygonGeoJSONModified} />
          </WarningsI18nProvider>
        </QueryClientProvider>,
      );
      expect(setViewFromFeatureSpy).toHaveBeenCalledTimes(1);
      expect(setViewFromFeatureSpy).toHaveBeenCalledWith(
        expect.objectContaining({
          targetCenter_: [1807243.124762136, 7397919.218520803],
        }),
        expect.anything(),
      );
    });
    it('should not zoom to provided geojson if defaultBbox provided', () => {
      const { rerender } = render(
        <QueryClientProvider client={new QueryClient()}>
          <WarningsI18nProvider>
            <WarningsMapView
              geojsonFeature={simplePolygonGeoJSONHalfOfNL}
              defaultMapPreset={{ proj: defaultBbox }}
            />
          </WarningsI18nProvider>
        </QueryClientProvider>,
      );

      const setViewFromExtentSpy = jest.spyOn(viewUtils!, 'setViewFromExtent');
      const setViewFromFeatureSpy = jest.spyOn(
        viewUtils!,
        'setViewFromFeature',
      );

      rerender(
        <QueryClientProvider client={new QueryClient()}>
          <WarningsI18nProvider>
            <WarningsMapView
              geojsonFeature={simplePolygonGeoJSONModified}
              defaultMapPreset={{ proj: defaultBbox }}
            />
          </WarningsI18nProvider>
        </QueryClientProvider>,
      );
      expect(setViewFromExtentSpy).toHaveBeenCalled();
      expect(setViewFromFeatureSpy).not.toHaveBeenCalled();
    });
  });
});
