/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import { DEBOUNCE_TIMEOUT } from '@opengeoweb/shared';
import { WarningsThemeProvider } from '../Providers/Providers';
import { AreaObjectFilter } from './AreaObjectFilter';
import { translateKeyOutsideComponents } from '../../utils/i18n';

const filters = [
  {
    label: 'Coastal Districts',
    id: 'Coastal Districts',
    isSelected: false,
    isDisabled: false,
  },
  {
    label: 'Objects',
    id: 'Objects',
    isSelected: false,
    isDisabled: false,
  },
  {
    label: 'Countries',
    id: 'Countries',
    isSelected: false,
    isDisabled: false,
  },
];

describe('src/components/AreaObjectLoader/AreaObjecFilter', () => {
  it('renders correctly with default props', () => {
    const props = {
      filters: [],
      searchQuery: '',
    };
    render(
      <WarningsThemeProvider>
        <AreaObjectFilter {...props} />
      </WarningsThemeProvider>,
    );

    const filterElement = screen.getByTestId('filterList');
    expect(filterElement).toBeTruthy();
    expect(
      screen.getByText(translateKeyOutsideComponents('shared-filter-search')),
    ).toBeTruthy();
  });

  it('calls onClickFilterChip when a filter chip is clicked', () => {
    const props = {
      filters,
      searchQuery: '',
      onClickFilterChip: jest.fn(),
    };

    render(
      <WarningsThemeProvider>
        <AreaObjectFilter {...props} />
      </WarningsThemeProvider>,
    );

    const filterChip = screen.getByText(props.filters[0].label);
    fireEvent.click(filterChip);
    expect(props.onClickFilterChip).toHaveBeenCalledWith(
      props.filters[0].label,
      true,
    );
  });

  it('calls onSearch when searchQuery changes', () => {
    const props = {
      filters,
      searchQuery: '',
      onSearch: jest.fn(),
    };
    jest.useFakeTimers();

    render(
      <WarningsThemeProvider>
        <AreaObjectFilter {...props} />
      </WarningsThemeProvider>,
    );

    const searchInput = screen.getByRole('textbox');

    fireEvent.change(searchInput, { target: { value: 'new query' } });

    jest.advanceTimersByTime(DEBOUNCE_TIMEOUT);
    jest.clearAllTimers();
    jest.useRealTimers();

    expect(props.onSearch).toHaveBeenCalledWith('new query');
  });

  it('sets isAllSelected correctly when no filters are selected', () => {
    render(
      <WarningsThemeProvider>
        <AreaObjectFilter filters={filters} searchQuery="" />
      </WarningsThemeProvider>,
    );

    const filterElement = screen.getByText('All');
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      filterElement.parentElement?.classList.contains(
        'MuiChip-filledSecondary',
      ),
    ).toBeTruthy();
  });

  it('sets isAllSelected to false when a filter is selected', () => {
    const testFilters = filters.map((filter) => ({
      ...filter,
      isSelected: true,
    }));

    render(
      <WarningsThemeProvider>
        <AreaObjectFilter filters={testFilters} searchQuery="" />
      </WarningsThemeProvider>,
    );

    const filterElement = screen.getByText('All');
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      filterElement.parentElement?.classList.contains(
        'MuiChip-filledSecondary',
      ),
    ).toBeFalsy();
  });
});
