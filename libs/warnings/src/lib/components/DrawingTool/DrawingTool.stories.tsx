/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Box } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { currentlySupportedDrawModes } from '@opengeoweb/webmap-react';
import DrawingToolDialog from './DrawingToolDialog';
import { DrawingToolForm } from '../DrawingToolForm';
import { WarningsThemeProvider } from '../Providers/Providers';
import { DrawingToolFormValues } from '../DrawingToolForm/DrawingToolFormContents';

export default {
  title: 'components/DrawingTool',
};
const formProps = {
  onSubmitForm: (formValues: DrawingToolFormValues): void => {
    // eslint-disable-next-line no-console
    console.log(
      'You have submitted the form. Nothing will happen now.',
      formValues,
    );
  },
  objectName: '23/08/2023 07:00 UTC',
  drawModes: currentlySupportedDrawModes,
  onChangeDrawMode: (): void => {},
  activeDrawModeId: '',
  onDeactivateTool: (): void => {},
  isInEditMode: false,
  geoJSONProperties: {},
  onChangeProperties: (): void => {},
  onChangeName: (): void => {},
  geoJSON: undefined,
};
export const DrawingToolLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <DrawingToolDialog onClose={(): void => {}} isOpen>
          <DrawingToolForm {...formProps} />
        </DrawingToolDialog>
      </Box>
    </WarningsThemeProvider>
  );
};

DrawingToolLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64956cfe39ec23476bc3b1c8',
    },
  ],
};

DrawingToolLight.tags = ['snapshot'];

export const DrawingToolDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <DrawingToolDialog onClose={(): void => {}} isOpen>
          <DrawingToolForm {...formProps} />
        </DrawingToolDialog>
      </Box>
    </WarningsThemeProvider>
  );
};

DrawingToolDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64997ae799d4a822f85e26cc',
    },
  ],
};

DrawingToolDark.tags = ['snapshot'];

export const DrawingToolErrorLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <DrawingToolDialog onClose={(): void => {}} isOpen>
          <DrawingToolForm {...formProps} dialogError="Something went wrong" />
        </DrawingToolDialog>
      </Box>
    </WarningsThemeProvider>
  );
};

DrawingToolErrorLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64956cfe39ec23476bc3b1c8/version/650aae678b26b247bd2fc7c4',
    },
  ],
};
