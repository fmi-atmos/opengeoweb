/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import i18n from 'i18next';
import { mapActions, layerActions, uiActions } from '@opengeoweb/store';
import { dateUtils } from '@opengeoweb/shared';
import {
  addSelectionTypeToGeoJSON,
  defaultModes,
  defaultPolygon,
  emptyGeoJSON,
  rewindGeometry,
} from '@opengeoweb/webmap-react';
import { setupServer } from 'msw/node';
import { ObjectManagerConnect } from './ObjectManagerConnect';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { drawingList, requestHandlers } from '../../utils/fakeApi/index';
import {
  DrawingFromBE,
  DrawingListItem,
} from '../../store/warningsDrawings/types';
import { ObjectManagerMapButtonConnect } from './ObjectManagerMapButtonConnect';
import DrawingToolConnect from '../DrawingTool/DrawingToolConnect';
import DrawingToolMapButtonConnect from '../DrawingTool/DrawingToolMapButtonConnect';
import { PublicWarningsFormDialogConnect } from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { testGeoJSON } from '../../storybookUtils/testUtils';
import { drawingDialogType } from '../../store/warningsDrawings/utils';
import { getDeleteSucces } from './ConfirmDeleteDialog';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store';

const newGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        selectionType: 'poly',
      },
      geometry: {
        type: 'Polygon',
        coordinates: [[]],
      },
    },
  ],
};

const server = setupServer(...requestHandlers);
const createApi = (): WarningsApi => {
  return {
    ...createFakeApi(),
    getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
      return new Promise((resolve) => {
        resolve({ data: drawingList });
      });
    },
  };
};
const store = createMockStore();

describe('src/components/ObjectManager/ObjectManagerConnect', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  it('should show the correct drawings', async () => {
    const mapId = 'map123';

    render(
      <WarningsThemeStoreProvider createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    await screen.findByText(`Object Manager for ${mapId}`);
    await screen.findByText(drawingList[0].objectName);

    await screen.findByText(
      `${dateUtils.dateToString(
        new Date(drawingList[0].lastUpdatedTime),
        dateUtils.DATE_FORMAT_DATEPICKER,
      )} UTC`,
    );

    // Global scope drawing should have Global: in front
    await screen.findByText(drawingList[2].objectName);
    await screen.findByText(
      `Global: ${dateUtils.dateToString(
        dateUtils.utc(drawingList[2].lastUpdatedTime),
        dateUtils.DATE_FORMAT_DATEPICKER,
      )} UTC`,
    );
  });

  it('should add a new geoJSON layer on click, update it and remove it again on click', async () => {
    const mapId = 'map124';
    store.dispatch(mapActions.registerMap({ mapId }));
    store.dispatch(
      uiActions.registerDialog({
        type: drawingDialogType,
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    await screen.findByText(`Object Manager for ${mapId}`);
    expect(store.getState().layers.byId).toEqual({});
    await screen.findByText(drawingList[0].objectName);
    fireEvent.click(screen.getByText(drawingList[0].objectName));
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );
    expect(store.getState().drawings.entities.drawingTool!.id).toEqual(
      drawingList[0].id,
    );
    // click another object
    fireEvent.click(screen.getByText(drawingList[1].objectName));
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[1].geoJSON,
    );
    expect(store.getState().drawings.entities.drawingTool!.id).toEqual(
      drawingList[1].id,
    );

    // click same object again
    fireEvent.click(screen.getByText(drawingList[1].objectName));
    await waitFor(() =>
      expect(store.getState().drawings.entities.drawingTool!.id).toEqual(''),
    );
    expect(
      store.getState().layers.byId[`drawlayer-${mapId}`].geojson!.features,
    ).toEqual([]);
  });

  it('should remove geoJSON layer when closing the dialog', async () => {
    const mapId = 'map125';
    store.dispatch(mapActions.registerMap({ mapId }));
    store.dispatch(
      uiActions.registerDialog({
        type: drawingDialogType,
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getByText(drawingList[0].objectName));
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );
    expect(store.getState().drawings.entities.drawingTool!.id).toEqual(
      drawingList[0].id,
    );

    fireEvent.click(screen.getByRole('button', { name: /close/i }));
    await waitFor(() =>
      expect(store.getState().drawings.entities.drawingTool!.id).toEqual(''),
    );
    expect(
      store.getState().layers.byId[`drawlayer-${mapId}`].geojson!.features,
    ).toEqual([]);
  });

  it('should show an error when Edit fails', async () => {
    const mapId = 'map126';

    const createApiWithErrorOnEdit = (): WarningsApi => {
      return {
        ...createFakeApi(),
        getDrawingDetails: (): Promise<{ data: DrawingFromBE }> => {
          return new Promise((_, reject) => {
            reject(new Error(`Error fetching drawing, please try again.`));
          });
        },
      };
    };

    render(
      <WarningsThemeStoreProvider createApi={createApiWithErrorOnEdit}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getAllByLabelText('Object options')[0]);
    fireEvent.click(screen.getByText('Edit'));
    // Error should be shown
    expect(
      await screen.findByText('Error fetching drawing, please try again.'),
    ).toBeTruthy();
    // List items should still be visible
    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    // Object options should be closed
    expect(screen.queryByText('Edit')).toBeFalsy();
  });

  it('should show an error when getting the list fails', async () => {
    const mapId = 'map127';

    const createApiWithErrorOnList = (): WarningsApi => {
      return {
        ...createFakeApi(),
        getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
          return new Promise((_, reject) => {
            reject(new Error(`Error fetching drawings.`));
          });
        },
      };
    };

    render(
      <WarningsThemeStoreProvider createApi={createApiWithErrorOnList}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    // Error should be shown
    expect(await screen.findByText('Error fetching drawings.')).toBeTruthy();
    // No results message should be shown
    expect(
      await screen.findByText(
        translateKeyOutsideComponents('object-manager-no-objects'),
      ),
    ).toBeTruthy();
  });

  it('should be able to edit a polygon', async () => {
    const mapId = 'map128';

    store.dispatch(mapActions.registerMap({ mapId }));

    const mockUpdateDrawing = jest
      .fn()
      .mockImplementation((): Promise<void> => {
        return new Promise((resolve) => {
          resolve();
        });
      });
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        updateDrawing: mockUpdateDrawing,
      };
    };

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    fireEvent.click(
      screen.queryAllByRole('button', { name: /Object options/i })[0],
    );

    fireEvent.click(screen.getByText('Edit'));

    // object manager should be closed
    await waitFor(() =>
      expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy(),
    );

    // draw dialog should be opened
    await screen.findByText(`Drawing Toolbox ${mapId}`);

    // geojson should be updated
    expect(store.getState().drawings.entities.drawingTool!.id).toEqual(
      drawingList[0].id,
    );

    // dialog should have object data
    expect(
      screen
        .getByRole('textbox', { name: 'Object name' })
        .getAttribute('value'),
    ).toEqual(drawingList[0].objectName);

    expect(
      store.getState().drawingtools.entities.drawingTool?.geoJSONLayerId,
    ).toEqual(`drawlayer-${mapId}`);
    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual(defaultPolygon.drawModeId);
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    // click save button
    expect(screen.getByText('Update object').classList).not.toContain(
      'Mui-disabled',
    );
    fireEvent.click(screen.getByText('Update object'));

    // api should be called
    await waitFor(() =>
      expect(mockUpdateDrawing).toHaveBeenCalledWith(drawingList[0].id, {
        objectName: drawingList[0].objectName,
        scope: 'user',
        geoJSON: rewindGeometry(
          drawingList[0].geoJSON as GeoJSON.FeatureCollection,
        ),
      }),
    );

    // snackbar should show
    await screen.findByText(
      `Object "${drawingList[0].objectName}" has been updated!`,
    );
  });

  it('should be able to delete a polygon', async () => {
    const mapId = 'map128';
    store.dispatch(mapActions.registerMap({ mapId }));

    const mockDeleteDrawing = jest.fn();
    const mockGetDrawings = jest
      .fn()
      .mockImplementation((): Promise<{ data: DrawingListItem[] }> => {
        return new Promise((resolve) => {
          resolve({ data: drawingList });
        });
      });
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        deleteDrawing: mockDeleteDrawing,
        getDrawings: mockGetDrawings,
      };
    };

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(mockGetDrawings).toHaveBeenCalledTimes(1);

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getByText(drawingList[0].objectName));

    await waitFor(() =>
      expect(
        store.getState().layers.byId[`drawlayer-${mapId}`]?.geojson,
      ).toEqual(drawingList[0].geoJSON),
    );

    fireEvent.click(
      screen.queryAllByRole('button', { name: /Object options/i })[0],
    );

    fireEvent.click(screen.getByText('Delete'));
    expect(screen.getByText(i18n.t('delete-dialog-title'))).toBeTruthy();

    fireEvent.click(screen.getByRole('button', { name: /Delete/i }));

    // api should be called
    expect(mockDeleteDrawing).toHaveBeenCalledWith(drawingList[0].id);

    // geojson should be removed
    await waitFor(() =>
      expect(
        store.getState().layers.byId[`drawlayer-${mapId}`].geojson?.features,
      ).toEqual([]),
    );

    // snackbar should show
    await screen.findByText(getDeleteSucces(drawingList[0].objectName, i18n.t));

    // list should update
    expect(mockGetDrawings).toHaveBeenCalledTimes(2);
  });

  it('should close the drawing tool when opening the object manager and vice versa', async () => {
    const mapId = 'map129';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    // open drawing tool
    fireEvent.click(
      screen.getByRole('button', {
        name: `Drawing Toolbox`,
      }),
    );
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();
    expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy();

    // check that drawing tool is active
    const drawingButton = screen.getByRole('button', {
      name: 'Drawing Toolbox',
    });
    expect(drawingButton.classList).toContain('Mui-selected');

    // open object manager
    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(screen.queryByText(`Drawing Toolbox ${mapId}`)).toBeFalsy();
    expect(drawingButton.classList).not.toContain('Mui-selected');

    // select object
    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getByText(drawingList[0].objectName));
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );
    expect(store.getState().drawings.entities.drawingTool!.id).toEqual(
      drawingList[0].id,
    );

    // open drawing tool again
    fireEvent.click(
      screen.getByRole('button', {
        name: `Drawing Toolbox`,
      }),
    );
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();
    expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy();
    expect(drawingButton.classList).toContain('Mui-selected');

    expect(
      store.getState().layers.byId[`drawlayer-${mapId}`].geojson?.features,
    ).toEqual([addSelectionTypeToGeoJSON(defaultModes[1].shape, 'poly')]);
    expect(store.getState().drawings.entities.drawingTool!.id).toEqual('');
  });

  it('should be able to share an object to a public warning and default values should be set', async () => {
    const mapId = 'map128';

    store.dispatch(mapActions.registerMap({ mapId }));

    const mockDeleteDrawing = jest.fn();
    const mockGetDrawings = jest
      .fn()
      .mockImplementation((): Promise<{ data: DrawingListItem[] }> => {
        return new Promise((resolve) => {
          resolve({ data: drawingList });
        });
      });
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        deleteDrawing: mockDeleteDrawing,
        getDrawings: mockGetDrawings,
      };
    };

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(mockGetDrawings).toHaveBeenCalledTimes(1);

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
    expect(store.getState().publicWarningForm.object).toBeUndefined();

    fireEvent.click(
      screen.queryAllByLabelText(i18n.t('object-manager-button-share'))[1],
    );
    fireEvent.click(
      await screen.findByText(i18n.t('public-warning-title').toUpperCase()),
    );

    await waitFor(() =>
      expect(
        store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
      ).toBeTruthy(),
    );
    const expectedResult: DrawingListItem = {
      id: drawingList[1].id,
      lastUpdatedTime: drawingList[1].lastUpdatedTime,
      objectName: drawingList[1].objectName,
      scope: 'user',
      geoJSON: drawingList[1].geoJSON as GeoJSON.FeatureCollection,
      keywords: 'Objects',
    };
    await waitFor(() => {
      expect(store.getState().publicWarningForm.object).toEqual(expectedResult);
    });
    expect(
      store.getState().publicWarningForm.warning?.warningDetail.areas[0],
    ).toEqual({
      geoJSON: expectedResult.geoJSON,
      objectName: drawingList[1].objectName,
    });

    expect(
      screen.getByRole('heading', { name: expectedResult.objectName }),
    ).toBeTruthy();

    // check default values
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid from',
        })
        .getAttribute('value') || '',
    ).not.toEqual('');
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid until',
        })
        .getAttribute('value') || '',
    ).not.toEqual('');
    expect(screen.getByLabelText('Probability').textContent).toEqual('30 %');

    fireEvent.click(screen.queryAllByRole('button', { name: /close/i })[1]);
    await waitFor(() => {
      expect(
        store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
      ).toBeFalsy();
    });
    expect(store.getState().publicWarningForm.object).toBeUndefined();
  });

  it('should be able to save a new object', async () => {
    const mapId = 'map130';

    store.dispatch(mapActions.registerMap({ mapId }));

    const mockSaveDrawing = jest.fn().mockImplementation((): Promise<void> => {
      return new Promise((resolve) => {
        resolve();
      });
    });
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        saveDrawingAs: mockSaveDrawing,
      };
    };

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Drawing Toolbox`,
      }),
    );

    // draw dialog should be opened
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();
    // Object manager should be closed
    expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy();

    // dialog should have default data
    expect(
      screen
        .getByRole('textbox', { name: 'Object name' })
        .getAttribute('value'),
    ).not.toEqual('');

    expect(
      screen.getAllByRole('textbox', { hidden: true })[0].getAttribute('value'),
    ).toEqual('20');

    // trigger drawn shape
    await act(() =>
      store.dispatch(
        layerActions.updateFeature({
          layerId: `drawlayer-${mapId}`,
          geojson: testGeoJSON,
        }),
      ),
    );

    await waitFor(() =>
      expect(
        store.getState().layers.byId[`drawlayer-${mapId}`].geojson,
      ).toEqual(testGeoJSON),
    );

    // click save button
    expect(screen.getByText('Save new object').classList).not.toContain(
      'Mui-disabled',
    );
    await userEvent.click(screen.getByText('Save new object'));

    // api should be called
    expect(mockSaveDrawing).toHaveBeenCalledWith({
      objectName: expect.any(String),
      scope: 'user',
      geoJSON: testGeoJSON,
    });

    // snackbar should show
    expect(
      (await screen.findByTestId('snackbarComponent')).textContent,
    ).toContain('has been saved!');
  });

  it('should handle draw actions', async () => {
    const mapId = 'map131';
    const layerId = `drawlayer-${mapId}`;
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[layerId].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    // open drawing tool
    fireEvent.click(screen.getByRole('button', { name: 'Drawing Toolbox' }));

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual(defaultPolygon.drawModeId);

    expect(store.getState().layers.byId[layerId].geojson!.features).toEqual([
      addSelectionTypeToGeoJSON(defaultModes[1].shape, 'poly'),
    ]);

    const polygonButton = screen.getByRole('button', { name: /Polygon/i });
    expect(polygonButton.classList).toContain('Mui-selected');

    // trigger start drawing
    act(() => {
      store.dispatch(
        layerActions.updateFeature({
          layerId,
          geojson: newGeoJSON,
        }),
      );
    });

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual(defaultPolygon.drawModeId);
    expect(store.getState().layers.byId[layerId].geojson!.features).toEqual(
      newGeoJSON.features,
    );

    await screen.findByText(i18n.t('drawing-tool-form-exit-draw-mode'));

    // click delete and should disable active mode as it's not selectable
    const deleteButton = screen.getByRole('button', { name: /Delete/i });
    fireEvent.click(deleteButton);

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual('');

    await screen.findByText(i18n.t('drawing-tool-form-drawing-required'));

    // activate polygon
    fireEvent.click(polygonButton);
    await waitFor(() =>
      expect(polygonButton.classList).toContain('Mui-selected'),
    );

    // trigger start drawing
    act(() => {
      store.dispatch(
        layerActions.updateFeature({
          layerId,
          geojson: newGeoJSON,
        }),
      );
    });

    await screen.findByText(i18n.t('drawing-tool-form-drawing-required'));
    await screen.findByText(i18n.t('drawing-tool-form-exit-draw-mode'));

    // trigger drawn shape
    act(() => {
      store.dispatch(
        layerActions.updateFeature({
          layerId,
          geojson: testGeoJSON,
        }),
      );
    });

    expect(store.getState().layers.byId[layerId].geojson!.features).toEqual(
      testGeoJSON.features,
    );

    // Dispatch action manually as there is no map included to catch the Escape key press
    act(() => {
      store.dispatch(
        layerActions.exitFeatureDrawMode({
          reason: 'escaped',
          layerId,
          shouldAllowMultipleShapes: false,
        }),
      );
    });

    await waitFor(() => {
      expect(
        store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
      ).toEqual('');
    });

    const { layers } = store.getState();
    expect(layers.byId[layerId].isInEditMode).toBeFalsy();

    await waitFor(() => {
      expect(
        screen.queryByText(i18n.t('drawing-tool-form-drawing-required')),
      ).toBeFalsy();
    });
  });

  it('should handle name and opacity changes', async () => {
    const mapId = 'map132';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    // open drawing tool
    fireEvent.click(screen.getByRole('button', { name: 'Drawing Toolbox' }));

    // change the name
    const nameInput = screen.getByRole('textbox', { name: /Object name/i });
    const newNameValue = 'testing new name';
    fireEvent.change(nameInput, { target: { value: newNameValue } });
    fireEvent.blur(nameInput);

    expect(store.getState().drawings.entities.drawingTool!.objectName).toEqual(
      newNameValue,
    );

    // trigger drawn shape
    act(() => {
      store.dispatch(
        layerActions.updateFeature({
          layerId: `drawlayer-${mapId}`,
          geojson: testGeoJSON,
        }),
      );
    });

    // change opacity
    fireEvent.mouseDown(screen.getByRole('combobox', { name: 'Opacity' }));

    const menuItem = await screen.findByText('50 %');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(
        store.getState().layers.byId[`drawlayer-${mapId}`].geojson?.features[0]!
          .properties!['fill-opacity'],
      ).toEqual(0.5);
    });
  });

  it('should deactivate draw mode and clear geoJSON when closing the drawing toolbox', async () => {
    const mapId = 'map133';
    const layerId = `drawlayer-${mapId}`;
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[layerId].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    // open drawing tool
    fireEvent.click(screen.getByRole('button', { name: 'Drawing Toolbox' }));

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual(defaultPolygon.drawModeId);
    expect(store.getState().layers.byId[layerId].geojson!.features).toEqual([
      addSelectionTypeToGeoJSON(defaultModes[1].shape, 'poly'),
    ]);

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual(defaultPolygon.drawModeId);

    // trigger drawing in progress
    act(() => {
      store.dispatch(
        layerActions.updateFeature({
          layerId,
          geojson: newGeoJSON,
        }),
      );
    });

    await act(() =>
      store.dispatch(
        layerActions.toggleFeatureMode({
          layerId,
          isInEditMode: true,
          drawMode: 'POLYGON',
        }),
      ),
    );

    await waitFor(() => {
      expect(
        store.getState().layers.byId[layerId].geojson!.features,
      ).not.toEqual([]);
    });

    // close drawing toolbox by clicking the button
    fireEvent.click(screen.getByRole('button', { name: 'Drawing Toolbox' }));

    expect(
      screen.getByText(
        translateKeyOutsideComponents('close-draw-dialog-title'),
      ),
    ).toBeTruthy();
    expect(
      screen.getByText(
        translateKeyOutsideComponents('close-dialog-description'),
      ),
    ).toBeTruthy();

    // confirm close
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('close-dialog-confirm'),
      }),
    );

    expect(
      screen.queryByText(
        translateKeyOutsideComponents('close-draw-dialog-title'),
      ),
    ).toBeFalsy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('close-dialog-description'),
      ),
    ).toBeFalsy();

    const { layers, drawingtools } = store.getState();
    expect(layers.byId[`drawlayer-${mapId}`].isInEditMode).toBeFalsy();
    expect(layers.byId[`drawlayer-${mapId}`].drawMode).toEqual('');
    expect(drawingtools.entities.drawingTool?.activeDrawModeId).toEqual('');
  });

  it('should show an error on the drawing toolbox when saving fails and clear the error when closing the dialog', async () => {
    const mapId = 'map134';

    store.dispatch(mapActions.registerMap({ mapId }));

    const mockUpdateDrawing = jest
      .fn()
      .mockImplementation((): Promise<void> => {
        return new Promise((_, reject) => {
          reject(new Error('Example error'));
        });
      });
    const createApi = (): WarningsApi => {
      return {
        ...createFakeApi(),
        updateDrawing: mockUpdateDrawing,
      };
    };

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    fireEvent.click(
      screen.queryAllByRole('button', { name: /Object options/i })[0],
    );

    fireEvent.click(screen.getByText('Edit'));

    // object manager should be closed
    await waitFor(() =>
      expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy(),
    );

    // draw dialog should be opened
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();

    // click save button
    fireEvent.click(screen.getByText('Update object'));

    // error should show
    await screen.findByText('Example error');
    expect(store.getState().ui.dialogs.drawingTool?.error).toEqual(
      'Example error',
    );

    // close dialog
    fireEvent.click(screen.getByRole('button', { name: 'Close' }));

    expect(store.getState().ui.dialogs.drawingTool?.error).toEqual('');
  });

  it('should clear drawn objects when opening the object manager', async () => {
    const mapId = 'map135';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();

    expect(await screen.findByText(drawingList[0].objectName)).toBeTruthy();
    fireEvent.click(screen.getByText(drawingList[0].objectName));

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      drawingList[0].geoJSON,
    );

    // open drawing tool
    fireEvent.click(screen.getByRole('button', { name: 'Drawing Toolbox' }));

    expect(
      store.getState().drawingtools.entities.drawingTool?.activeDrawModeId,
    ).toEqual(defaultPolygon.drawModeId);

    // trigger drawn shape
    act(() => {
      store.dispatch(
        layerActions.updateFeature({
          layerId: `drawlayer-${mapId}`,
          geojson: testGeoJSON,
        }),
      );
    });

    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      testGeoJSON,
    );
    // open object manager
    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    // confirm close
    fireEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('close-dialog-confirm'),
      }),
    );

    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    expect(store.getState().layers.byId[`drawlayer-${mapId}`].geojson).toEqual(
      emptyGeoJSON,
    );
  });

  it('should add a new object from the object manager', async () => {
    const mapId = 'map136';

    render(
      <WarningsThemeStoreProvider createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </WarningsThemeStoreProvider>,
    );

    fireEvent.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );
    expect(await screen.findByText(`Object Manager for ${mapId}`)).toBeTruthy();
    fireEvent.click(
      screen.getByRole('button', {
        name: i18n.t('object-manager-add'),
      }),
    );
    // draw dialog should be opened
    expect(await screen.findByText(`Drawing Toolbox ${mapId}`)).toBeTruthy();
    expect(screen.getByText('Save new object')).toBeTruthy();
    // Object manager should be closed
    expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy();
  });
});
