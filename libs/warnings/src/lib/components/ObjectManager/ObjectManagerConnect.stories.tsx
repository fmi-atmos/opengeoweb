/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { MapViewConnect } from '@opengeoweb/core';
import { MapControls } from '@opengeoweb/webmap-react';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { ObjectManagerConnect } from './ObjectManagerConnect';
import { ObjectManagerMapButtonConnect } from './ObjectManagerMapButtonConnect';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { createMockStore } from '../../store/store';

export default {
  title: 'components/ObjectManagerConnect',
};

const MapWithObjectManager: React.FC<{ mapId: string }> = ({ mapId }) => {
  useDefaultMapSettings({
    mapId,
  });

  return (
    <div style={{ height: '100vh' }}>
      <ObjectManagerConnect />
      <MapControls>
        <ObjectManagerMapButtonConnect mapId={mapId} />
      </MapControls>
      <MapViewConnect mapId={mapId} />
    </div>
  );
};

export const ObjectManagerConnectLight = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider theme={lightTheme} store={createMockStore()}>
      <MapWithObjectManager mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

export const ObjectManagerConnectDark = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider theme={darkTheme} store={createMockStore()}>
      <MapWithObjectManager mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

ObjectManagerConnectLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64956cfe39ec23476bc3b1c8',
    },
  ],
};

ObjectManagerConnectLight.storyName = 'ObjectManagerConnect LightTheme ';

ObjectManagerConnectDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64997ae799d4a822f85e26cc',
    },
  ],
};

ObjectManagerConnectDark.storyName = 'ObjectManagerConnect DarkTheme';
