/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { renderHook, act } from '@testing-library/react';
import { omit, pick } from 'lodash';
import {
  FilterState,
  getPhenomenaForDomainFilter,
  PickedFilterState,
  useWarningFilters,
} from './filter-hooks';
import {
  AllPhenomenaKeys,
  Domain,
  getFiltersFromWarnings,
  initialFilters,
  Level,
  makeFilterFromList,
  Phenomenon,
} from './filter-utils';
import warningListJSON from '../../utils/fakeApi/warningList.json';
import { Warning } from '../../store/publicWarningForm/types';
import airmetListJSON from '../../utils/fakeApi/airmetList.json';

const warningList = warningListJSON as unknown as Warning[];
const airmetList = airmetListJSON as unknown as Warning[];
const allWarnings = warningList.concat(airmetList);

const warningPhenomenon = getFiltersFromWarnings(allWarnings, 'phenomenon');

const aviationFilters = {
  ...initialFilters,
  domain: { aviation: true, maritime: false, public: false },
};
const aviationPickedFilters: PickedFilterState = {
  ...aviationFilters,
  phenomenon: makeFilterFromList(
    getPhenomenaForDomainFilter(aviationFilters.domain).filter((phe) =>
      warningPhenomenon.includes(phe),
    ),
  ),
  level: pick(initialFilters.level, [
    'unspecified',
    Level.sigmet,
    Level.airmet,
  ]),
};
const initialPickedFilters: PickedFilterState = {
  ...initialFilters,
  phenomenon: makeFilterFromList(
    getPhenomenaForDomainFilter(initialFilters.domain).filter((phe) =>
      warningPhenomenon.includes(phe),
    ),
  ),
  level: pick(initialFilters.level, [
    'unspecified',
    Level.extreme,
    Level.moderate,
    Level.severe,
  ]),
};

const filterWithSource = <T extends PickedFilterState | FilterState>(
  filter: T = initialFilters as T,
  warnings: Warning[] = allWarnings,
): T => ({
  ...filter,
  source: makeFilterFromList(
    warnings
      .map((w) => w.warningDetail.warningProposalSource)
      .filter((w) => w !== undefined) as string[],
  ),
  incident: makeFilterFromList(
    warnings
      .map((w) => w.warningDetail.incident)
      .filter((w) => w !== undefined) as string[],
  ),
});

describe('filter-hooks - useWarningFilters', () => {
  it('should update filters when preset changes', async () => {
    const onUpdateFilters = jest.fn();
    const { rerender } = renderHook(
      ({ presetFilterState }) =>
        useWarningFilters(
          allWarnings,
          onUpdateFilters,
          initialFilters,
          presetFilterState,
        ),
      {
        initialProps: {
          presetFilterState: undefined as PickedFilterState | undefined,
        },
      },
    );

    rerender({ presetFilterState: aviationFilters });
    expect(onUpdateFilters).toHaveBeenLastCalledWith(
      filterWithSource(aviationFilters),
      true,
    );
    rerender({ presetFilterState: undefined });
    expect(onUpdateFilters).toHaveBeenLastCalledWith(
      filterWithSource(initialFilters),
      true,
    );
  });
  it('should set initialFilters when preset has no filters saved', async () => {
    const onUpdateFilters = jest.fn();
    renderHook(() =>
      useWarningFilters(allWarnings, onUpdateFilters, initialFilters),
    );
    expect(onUpdateFilters).toHaveBeenCalledWith(
      filterWithSource({} as PickedFilterState),
      true,
    );
    expect(onUpdateFilters).toHaveBeenCalledWith(
      expect.objectContaining(omit(initialFilters, ['incident', 'source'])),
      true,
    );
  });

  it('should consider all selected only when all filters are true', async () => {
    const { result, rerender } = renderHook(
      ({ savedFilterState }) =>
        useWarningFilters([], () => {}, savedFilterState),
      {
        initialProps: {
          savedFilterState: {
            ...initialFilters,
            domain: { aviation: true, public: true, maritime: true },
          } as FilterState,
        },
      },
    );

    const initialState = result.current;
    expect(initialState.allSelected).toBeTruthy();

    rerender({
      savedFilterState: {
        ...initialFilters,
        domain: { aviation: true, public: true, maritime: false },
      },
    });

    const initialState2 = result.current;
    expect(initialState2.allSelected).toBeFalsy();
  });

  it('should only show relevant levels to the domains chosen', async () => {
    const onUpdateFilters = jest.fn();
    const { result, rerender } = renderHook(
      (savedFilter) =>
        useWarningFilters(allWarnings, onUpdateFilters, savedFilter),
      { initialProps: initialFilters },
    );
    expect(result.current.filterState.level).toEqual(
      initialPickedFilters.level,
    );
    rerender({
      ...initialFilters,
      domain: { aviation: true, public: true, maritime: false },
    });
    expect(result.current.filterState.level).toEqual(initialFilters.level);

    rerender(aviationFilters);
    expect(result.current.filterState.level).toEqual(
      aviationPickedFilters.level,
    );
  });

  it('should keep previous level state when updating the domain filter', () => {
    const onUpdateFilters = jest.fn();
    const state: FilterState = {
      ...initialFilters,
      level: {
        ...initialFilters.level,
        severe: false,
        extreme: false,
        moderate: true,
      },
    };

    const { result } = renderHook(() =>
      useWarningFilters(warningList, onUpdateFilters, state),
    );
    const { updateFilter } = result.current;
    act(() => {
      updateFilter('domain', 'aviation' as Domain, true, false);
    });

    expect(onUpdateFilters).toHaveBeenLastCalledWith({
      domain: {
        aviation: true,
        public: true,
        maritime: false,
      },
    });
  });

  it('should update the filter when calling updateFilter', () => {
    const onUpdateFilters = jest.fn();
    const { result } = renderHook(() =>
      useWarningFilters(allWarnings, onUpdateFilters, initialFilters),
    );
    const { updateFilter } = result.current;

    act(() => updateFilter('level', 'AIR' as Level, true, true));
    expect(onUpdateFilters).toHaveBeenLastCalledWith({
      level: {
        ...makeFilterFromList(Object.values(Level), false),
        AIR: true,
      },
    });
    act(() => updateFilter('phenomenon', Phenomenon.wind, true, true));
    expect(onUpdateFilters).toHaveBeenLastCalledWith({
      phenomenon: {
        ...makeFilterFromList(Object.keys(initialFilters.phenomenon), false),
        [Phenomenon.wind]: true,
      },
    });
    act(() => updateFilter('domain', Domain.aviation, true));
    expect(onUpdateFilters).toHaveBeenLastCalledWith({
      domain: {
        [Domain.aviation]: true,
        [Domain.maritime]: false,
        [Domain.public]: true,
      },
    });
    act(() => updateFilter('domain', 'ALL', false));
    expect(onUpdateFilters).toHaveBeenLastCalledWith({
      domain: {
        [Domain.aviation]: false,
        [Domain.maritime]: false,
        [Domain.public]: false,
      },
    });
  });

  it('should update filter when warning list changes', () => {
    const onUpdateFilters = jest.fn();
    const { result, rerender } = renderHook(
      ({ list, savedFilters }) =>
        useWarningFilters(list, onUpdateFilters, savedFilters, aviationFilters),
      {
        initialProps: {
          list: allWarnings as Warning[],
          savedFilters: aviationFilters,
        },
      },
    );
    expect(result.current.filterState).toEqual(aviationPickedFilters);

    rerender({ list: warningList, savedFilters: aviationFilters });
    expect(onUpdateFilters).toHaveBeenLastCalledWith(
      filterWithSource({} as PickedFilterState, warningList),
      true,
    );
    rerender({ list: airmetList, savedFilters: aviationFilters });
    expect(onUpdateFilters).toHaveBeenLastCalledWith(
      filterWithSource({} as PickedFilterState, airmetList),
      true,
    );
  });

  it('should only show relevant phenomena to the domains chosen', async () => {
    const onUpdateFilters = jest.fn();

    const { result, rerender } = renderHook(
      ({ reduxState }) =>
        useWarningFilters(allWarnings, onUpdateFilters, reduxState),
      { initialProps: { reduxState: filterWithSource(initialFilters) } },
    );

    expect(onUpdateFilters).toHaveBeenCalledWith(
      filterWithSource({} as PickedFilterState),
      true,
    );

    rerender({
      reduxState: {
        ...filterWithSource(),
        domain: { aviation: true, public: false, maritime: false },
      },
    });

    expect(result.current.filterState).toEqual({
      ...filterWithSource(),
      domain: { aviation: true, public: false, maritime: false },
      level: { AIR: true, SIG: true, unspecified: true },
      phenomenon: {
        unspecified: true,
        [AllPhenomenaKeys.OBSC_TS]: true,
        [AllPhenomenaKeys.SEV_ICE]: true,
        [AllPhenomenaKeys.SFC_WIND]: true,
        [AllPhenomenaKeys.MOD_TURB]: true,
      },
    });

    rerender({
      reduxState: {
        ...filterWithSource(),
        domain: { aviation: false, public: true, maritime: false },
      },
    });
    expect(result.current.filterState).toEqual({
      ...filterWithSource(),
      domain: { aviation: false, public: true, maritime: false },
      level: { extreme: true, severe: true, moderate: true, unspecified: true },
      phenomenon: {
        unspecified: true,
        [AllPhenomenaKeys.coastalEvent]: true,
        [AllPhenomenaKeys.fog]: true,
        [AllPhenomenaKeys.highTemp]: true,
        [AllPhenomenaKeys.lowTemp]: true,
        [AllPhenomenaKeys.thunderstorm]: true,
        [AllPhenomenaKeys.wind]: true,
        [AllPhenomenaKeys.snowIce]: true,
      },
    });
  });
});
