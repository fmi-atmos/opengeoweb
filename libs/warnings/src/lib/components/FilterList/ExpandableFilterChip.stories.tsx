/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { Box, Stack } from '@mui/material';

import { darkTheme } from '@opengeoweb/theme';
import { WarningsThemeProvider } from '../Providers/Providers';
import ExpandableFilterChip, {
  ExpandableFilterChipProps,
} from './ExpandableFilterChip';
import { emptyFilterId } from './filter-utils';

export default {
  title: 'components/FilterList',
};

const props: ExpandableFilterChipProps = {
  id: 'incident',
  options: {},
  updateFilter: () => {},
};

const props2: ExpandableFilterChipProps = {
  id: 'domain',
  options: {
    [emptyFilterId]: false,
    aviation: false,
  },
  updateFilter: (id): void => {
    // eslint-disable-next-line no-console
    console.log('UpdateFilter:', id);
  },
};

const props3: ExpandableFilterChipProps = {
  id: 'phenomenon',
  options: { wind: true, fog: true, thunderstorm: true, snowIce: true },
  updateFilter: (id): void => {
    // eslint-disable-next-line no-console
    console.log('UpdateFilter:', id);
  },
};

const props4: ExpandableFilterChipProps = {
  id: 'level',
  options: { moderate: true, severe: false, extreme: false },
  updateFilter: (id): void => {
    // eslint-disable-next-line no-console
    console.log('UpdateFilter:', id);
  },
};

const Wrapper = (): React.ReactElement => {
  return (
    <Box
      sx={{
        padding: 3,
        width: '700px',
        height: '100px',
        backgroundColor: 'geowebColors.background.surfaceApp',
      }}
    >
      <Stack
        spacing={1}
        sx={{
          display: 'block',
          '.MuiChip-root': {
            margin: 0,
            marginRight: 1,
            marginBottom: 1,
          },
        }}
      >
        <ExpandableFilterChip {...props} />
        <ExpandableFilterChip {...props2} />
        <ExpandableFilterChip {...props3} />
        <ExpandableFilterChip {...props4} />
      </Stack>
    </Box>
  );
};

export const ExpandableFilterChipLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Wrapper />
    </WarningsThemeProvider>
  );
};
export const ExpandableFilterChipDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Wrapper />
    </WarningsThemeProvider>
  );
};

ExpandableFilterChipLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6629138d2277685234d7cb11/version/662a5f372804b5b51a5c486c',
    },
  ],
};
ExpandableFilterChipLight.tags = ['snapshot'];
ExpandableFilterChipDark.tags = ['snapshot'];
