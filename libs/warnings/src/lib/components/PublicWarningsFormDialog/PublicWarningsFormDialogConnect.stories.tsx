/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { MapViewConnect } from '@opengeoweb/core';
import { AppHeader, AppLayout, dateUtils } from '@opengeoweb/shared';
import { useDispatch } from 'react-redux';
import { uiActions } from '@opengeoweb/store';
import { PublicWarningsFormDialogConnect } from './PublicWarningsFormDialogConnect';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { AppHamburgerMenu } from '../../storybookUtils/storyComponents';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { publicWarningFormActions } from '../../store/publicWarningForm/reducer';
import { testGeoJSON } from '../../storybookUtils/testUtils';
import { PublicWarningStatus } from '../../store/publicWarningForm/types';
import { createMockStore } from '../../store/store';

export default {
  title: 'components/PublicWarningsFormDialogConnect',
};

const MapWithPublicWarnings: React.FC<{ mapId: string }> = ({ mapId }) => {
  useDefaultMapSettings({
    mapId,
  });

  return (
    <AppLayout header={<AppHeader menu={<AppHamburgerMenu />} />}>
      <PublicWarningsFormDialogConnect />
      <MapViewConnect mapId={mapId} />
    </AppLayout>
  );
};

export const PublicWarningsFormDialogConnectMap = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider store={createMockStore()}>
      <MapWithPublicWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

const DemoWithoutMap: React.FC<{ hasDefaultValues?: boolean }> = ({
  hasDefaultValues = false,
}) => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(
      uiActions.setToggleOpenDialog({
        type: publicWarningDialogType,
        setOpen: true,
      }),
    );

    if (hasDefaultValues) {
      const currentTime = new Date();
      const validFromTime = dateUtils.dateToString(
        dateUtils.add(currentTime, { minutes: 15 }),
      )!;
      const validUntilTime = dateUtils.dateToString(
        dateUtils.add(currentTime, { hours: 6, minutes: 15 }),
      )!;
      dispatch(
        publicWarningFormActions.setFormValues({
          object: {
            id: '923723984872338768743',
            objectName: 'Drawing object 101',
            lastUpdatedTime: '2022-06-01T12:34:27Z',
            scope: 'user',
            geoJSON: testGeoJSON,
          },
          warning: {
            warningDetail: {
              id: '923723984872338768743',
              phenomenon: 'coastalEvent',
              validFrom: validFromTime,
              validUntil: validUntilTime,
              level: 'extreme',
              probability: 80,
              descriptionOriginal:
                'Some pretty intense coastal weather is coming our way',
              descriptionTranslation: 'And this would be the translation',
              areas: [
                { geoJSON: testGeoJSON, objectName: 'Drawing object 101' },
              ],
            },
            status: PublicWarningStatus.TODO,
            type: 'public',
          },
        }),
      );
    }
  }, [dispatch, hasDefaultValues]);

  return <PublicWarningsFormDialogConnect />;
};

export const PublicWarningsFormDialogConnectWithoutMap =
  (): React.ReactElement => {
    return (
      <WarningsThemeStoreProvider store={createMockStore()}>
        <DemoWithoutMap />
      </WarningsThemeStoreProvider>
    );
  };

export const PublicWarningsFormDialogConnectWithoutMapWithValues =
  (): React.ReactElement => {
    return (
      <WarningsThemeStoreProvider store={createMockStore()}>
        <DemoWithoutMap hasDefaultValues />
      </WarningsThemeStoreProvider>
    );
  };
