/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { mapActions, uiActions } from '@opengeoweb/store';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { emptyGeoJSON } from '@opengeoweb/webmap-react';
import { setupServer } from 'msw/node';
import { QueryClient } from '@tanstack/react-query';
import { PublicWarningsFormDialogConnect } from './PublicWarningsFormDialogConnect';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { publicWarningFormActions } from '../../store/publicWarningForm/reducer';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { DrawingListItem } from '../../store/warningsDrawings/types';
import {
  PublicWarningDetail,
  PublicWarningStatus,
} from '../../store/publicWarningForm/types';
import { MOCK_USERNAME } from '../../utils/fakeApi';
import { translateKeyOutsideComponents } from '../../utils/i18n';
import { createMockStore } from '../../store/store';
import { requestHandlers } from '../../utils/fakeApi/index';

const server = setupServer(...requestHandlers);

describe('components/PublicWarningsFormDialogConnect', () => {
  beforeAll(() => {
    server.listen();
  });
  afterAll(() => {
    server.close();
  });
  const testObject: DrawingListItem = {
    id: 'test',
    lastUpdatedTime: '2022-06-01T12:34:27Z',
    objectName: 'my test area',
    scope: 'user',
    geoJSON: emptyGeoJSON,
  };

  it('should save a form with phenomenon and close without confirmation after saving', async () => {
    const store = createMockStore();
    const props = {
      source: 'app' as const,
      startPosition: { top: 600, left: 800 },
    };
    const mapId = 'mapId4';
    store.dispatch(mapActions.registerMap({ mapId }));
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormDialogConnect {...props} />
      </WarningsThemeStoreProvider>,
    );
    await waitFor(() => {
      // activate it
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
          source: props.source,
        }),
      );
    });
    await screen.findByText(
      translateKeyOutsideComponents('public-warning-title'),
    );
    await userEvent.click(await screen.findByLabelText('Phenomenon'));
    await userEvent.click(await screen.findByText('Rain'));
    await userEvent.click(
      screen.getByRole('button', {
        name: translateKeyOutsideComponents('warning-button-save'),
      }),
    );
    await userEvent.click(
      within(screen.getByRole('dialog')).getByRole('button', {
        name: /close/i,
      }),
    );
    await waitFor(() =>
      expect(
        screen.queryByText(
          translateKeyOutsideComponents('public-warning-title'),
        ),
      ).toBeFalsy(),
    );
  });
  it('should render with default props', async () => {
    const store = createMockStore();
    const props = {
      source: 'app' as const,
      startPosition: { top: 600, left: 800 },
    };
    const mapId = 'mapId1';

    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormDialogConnect {...props} />
      </WarningsThemeStoreProvider>,
    );
    expect(
      screen.queryByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
    await waitFor(() => {
      // activate it
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
          source: props.source,
        }),
      );
    });
    expect(
      screen.getByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeTruthy();

    const dialog = screen.getByRole('dialog');

    expect(getComputedStyle(dialog!).top).toEqual(
      `${props.startPosition.top}px`,
    );
    expect(getComputedStyle(dialog!).left).toEqual(
      `${props.startPosition.left}px`,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    expect(screen.queryByTestId('loading-bar')).toBeFalsy();

    // close it
    fireEvent.click(screen.getByRole('button', { name: /Close/i }));
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
  });

  it('should show as loading', async () => {
    const store = createMockStore();
    const props = {
      source: 'app' as const,
      startPosition: { top: 600, left: 800 },
    };
    const mapId = 'mapId1';

    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormDialogConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // activate it
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
          source: props.source,
        }),
      );
      store.dispatch(
        uiActions.toggleIsLoadingDialog({
          type: publicWarningDialogType,
          isLoading: true,
        }),
      );
    });
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
  });

  it('should show confirmation if you have got unsaved changes and set to view mode', async () => {
    const store = createMockStore({
      publicWarningForm: {
        selectedWarningId: '923723984872338768743',
      },
    });
    const props = {
      source: 'app' as const,
      startPosition: { top: 600, left: 800 },
    };
    const mapId = 'mapId1';

    store.dispatch(mapActions.registerMap({ mapId }));

    const testPublicWarningDetail: PublicWarningDetail = {
      id: '923723984872338768743',
      phenomenon: 'coastalEvent',
      validFrom: '2022-06-01T15:00:00Z',
      validUntil: '2022-06-01T18:00:00Z',
      level: 'extreme',
      probability: 80,
      descriptionOriginal:
        'Some pretty intense coastal weather is coming our way',
      descriptionTranslation: 'And this would be the translation',
      areas: [{ geoJSON: emptyGeoJSON, objectName: testObject.objectName }],
    };

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
        warning: {
          warningDetail: testPublicWarningDetail,
          status: PublicWarningStatus.DRAFT,
          editor: MOCK_USERNAME,
          type: 'public',
        },
        formState: '',
      }),
    );
    store.dispatch(
      publicWarningFormActions.setFormDirty({
        isFormDirty: true,
      }),
    );
    const queryClient = new QueryClient({
      defaultOptions: { queries: { retry: false } },
    });
    void queryClient.setQueryData(
      ['public-warning-list'],
      [
        {
          id: testPublicWarningDetail.id,
          warningDetail: testPublicWarningDetail,
          status: PublicWarningStatus.DRAFT,
          editor: MOCK_USERNAME,
        },
      ],
    );

    render(
      <WarningsThemeStoreProvider
        store={store}
        optionalQueryClient={queryClient}
      >
        <PublicWarningsFormDialogConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // activate it
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
          source: props.source,
        }),
      );
      store.dispatch(
        uiActions.toggleIsLoadingDialog({
          type: publicWarningDialogType,
          isLoading: true,
        }),
      );
    });

    expect(store.getState().publicWarningForm.object).toEqual(testObject);
    expect(store.getState().publicWarningForm.warning?.warningDetail).toEqual(
      testPublicWarningDetail,
    );
    expect(screen.getByText(testObject.objectName)).toBeTruthy();

    const mode = within(screen.getByLabelText('Switch mode')).getByRole(
      'checkbox',
    );
    fireEvent.click(mode);

    expect(
      screen.getByText(
        'Are you sure you want to switch to view mode? Your unsaved changes will be lost.',
      ),
    ).toBeTruthy();
  });
});
