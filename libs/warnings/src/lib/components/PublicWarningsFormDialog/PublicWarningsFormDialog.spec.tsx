/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { fireEvent, render, screen, within } from '@testing-library/react';
import { PublicWarningsFormDialog } from './PublicWarningsFormDialog';
import { WarningsThemeProvider } from '../Providers/Providers';
import { translateKeyOutsideComponents } from '../../utils/i18n';

describe('components/PublicWarningsFormDialog', () => {
  it('should render with default props', () => {
    const props = {
      isOpen: true,
      onClose: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsFormDialog {...props}>
          some children
        </PublicWarningsFormDialog>
      </WarningsThemeProvider>,
    );

    expect(
      screen.getByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeTruthy();

    fireEvent.click(screen.getByRole('button', { name: /Close/i }));

    expect(props.onClose).toHaveBeenCalled();
    expect(screen.getByLabelText('Switch mode')).toBeTruthy();
    expect(screen.getByText('some children')).toBeTruthy();
  });

  it('should not render when isOpen is false', () => {
    const props = {
      isOpen: false,
      onClose: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsFormDialog {...props}>
          some children
        </PublicWarningsFormDialog>
      </WarningsThemeProvider>,
    );

    expect(
      screen.queryByText(translateKeyOutsideComponents('public-warning-title')),
    ).toBeFalsy();
    expect(screen.queryByText('some children')).toBeFalsy();
  });

  it('should show loader', () => {
    const props = {
      isOpen: true,
      onClose: jest.fn(),
      isLoading: true,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsFormDialog {...props}>
          some children
        </PublicWarningsFormDialog>
      </WarningsThemeProvider>,
    );

    expect(screen.getByTestId('loading-bar')).toBeTruthy();
  });

  it('should hide form mode', () => {
    const props = {
      isOpen: true,
      onClose: jest.fn(),
      isLoading: true,
      shouldHideFormMode: true,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsFormDialog {...props}>
          some children
        </PublicWarningsFormDialog>
      </WarningsThemeProvider>,
    );

    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
  });

  it('should show avatar next to form mode if editor set', async () => {
    const props = {
      isOpen: true,
      onClose: jest.fn(),
      isLoading: true,
      publicWarningEditor: 'user.name',
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsFormDialog {...props}>
          some children
        </PublicWarningsFormDialog>
      </WarningsThemeProvider>,
    );

    expect(screen.getByText('UN')).toBeTruthy();
    fireEvent.mouseOver(screen.getByTestId('avatar'));
    expect(await screen.findByRole('tooltip')).toHaveTextContent(
      `Editor: ${props.publicWarningEditor}`,
    );
  });

  it('should change form mode', () => {
    const props = {
      isOpen: true,
      onClose: jest.fn(),
      isLoading: true,
      onChangeFormMode: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsFormDialog {...props}>
          some children
        </PublicWarningsFormDialog>
      </WarningsThemeProvider>,
    );

    const mode = within(screen.getByLabelText('Switch mode')).getByRole(
      'checkbox',
    );
    fireEvent.click(mode);
    expect(props.onChangeFormMode).toHaveBeenCalledWith(true);
  });
});
