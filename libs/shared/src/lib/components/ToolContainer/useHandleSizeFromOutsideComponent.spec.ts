/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { getNewPositionSize } from './useHandleSizeFromOutsideComponent';

interface MockElement extends Partial<Element> {
  getBoundingClientRect: jest.Mock<DOMRect>;
  style?: Partial<CSSStyleDeclaration>;
}

describe('getNewPositionSize', () => {
  let element: MockElement;
  let boundsNode: MockElement;

  beforeEach(() => {
    element = {
      getBoundingClientRect: jest.fn() as jest.Mock<DOMRect>,
      style: {
        left: '10px',
        right: '10px',
        top: '10px',
      },
    };

    boundsNode = {
      getBoundingClientRect: jest.fn() as jest.Mock<DOMRect>,
    };

    global.getComputedStyle = jest.fn().mockReturnValue({
      left: '10px',
      right: '10px',
      top: '10px',
    });
  });

  it('should return the correct position and size when the element is smaller than the bounds', () => {
    element.getBoundingClientRect.mockReturnValue({
      width: 100,
      height: 100,
      x: 0,
      y: 0,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      toJSON: jest.fn(),
    } as DOMRect);
    boundsNode.getBoundingClientRect.mockReturnValue({
      width: 200,
      height: 200,
      x: 0,
      y: 0,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      toJSON: jest.fn(),
    } as DOMRect);

    const result = getNewPositionSize(
      element as Element,
      boundsNode as Element,
      false,
      { x: 50, y: 50 },
    );

    expect(result).toEqual({
      position: { left: 50, top: 50 },
      size: { width: 100, height: 100 },
    });
  });

  it('should return the correct position and size when the element is larger than the bounds', () => {
    element.getBoundingClientRect.mockReturnValue({
      width: 300,
      height: 300,
      x: 0,
      y: 0,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      toJSON: jest.fn(),
    } as DOMRect);
    boundsNode.getBoundingClientRect.mockReturnValue({
      width: 200,
      height: 200,
      x: 0,
      y: 0,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      toJSON: jest.fn(),
    } as DOMRect);

    const result = getNewPositionSize(
      element as Element,
      boundsNode as Element,
      false,
      { x: 50, y: 50 },
    );

    expect(result).toEqual({
      position: { left: -10, top: -10 },
      size: { width: 200, height: 200 },
    });
  });

  it('should handle right-aligned elements correctly', () => {
    element.getBoundingClientRect.mockReturnValue({
      width: 100,
      height: 100,
      x: 0,
      y: 0,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      toJSON: jest.fn(),
    } as DOMRect);
    boundsNode.getBoundingClientRect.mockReturnValue({
      width: 200,
      height: 200,
      x: 0,
      y: 0,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      toJSON: jest.fn(),
    } as DOMRect);

    const result = getNewPositionSize(
      element as Element,
      boundsNode as Element,
      true,
      { x: 50, y: 50 },
    );

    expect(result).toEqual({
      position: { left: 50, top: 50 },
      size: { width: 100, height: 100 },
    });
  });

  it('should adjust position when element overflows bounds', () => {
    element.getBoundingClientRect.mockReturnValue({
      width: 150,
      height: 150,
      x: 0,
      y: 0,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      toJSON: jest.fn(),
    } as DOMRect);
    boundsNode.getBoundingClientRect.mockReturnValue({
      width: 200,
      height: 200,
      x: 0,
      y: 0,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      toJSON: jest.fn(),
    } as DOMRect);

    const result = getNewPositionSize(
      element as Element,
      boundsNode as Element,
      false,
      { x: 100, y: 100 },
    );

    expect(result).toEqual({
      position: { left: 40, top: 40 },
      size: { width: 150, height: 150 },
    });
  });
});
