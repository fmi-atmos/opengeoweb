/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Add, ButtonVariant } from '@opengeoweb/theme';
import { Box, Typography, useTheme } from '@mui/material';
import type { Meta, StoryObj } from '@storybook/react';
import { CustomToggleButton } from './CustomToggleButton';
import { StorybookDocsWrapper } from '../Storybook';

const meta: Meta<typeof CustomToggleButton> = {
  title: 'components/CustomToggleButton',
  component: CustomToggleButton,
  parameters: {
    docs: {
      description: {
        component: 'CustomToggleButton component',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof CustomToggleButton>;

export const Component: Story = {
  args: {
    children: (
      <>
        <Add /> Custom toggle button
      </>
    ),
    variant: 'primary',
    value: 1,
  },
};

const VariantExamples = ({
  isDark = false,
}: {
  isDark?: boolean;
}): React.ReactElement => {
  const theme = useTheme();
  const gwButtonVariants = Object.keys(
    theme.palette.geowebColors.buttons,
  ) as ButtonVariant[];

  return (
    <StorybookDocsWrapper
      isDark={isDark}
      sx={{ margin: '10px', padding: '20px 0px', width: '600px' }}
    >
      <Box>
        {gwButtonVariants.map((variant) => (
          <Box
            key={variant}
            sx={{ width: '100%', button: { margin: '0px 10px 10px 0' } }}
          >
            <CustomToggleButton variant={variant}>{variant}</CustomToggleButton>
            <CustomToggleButton variant={variant}>
              <Add />
              {variant}
            </CustomToggleButton>
          </Box>
        ))}
        <Typography>
          For more examples, see theme storybook of ToggleButton
        </Typography>
      </Box>
    </StorybookDocsWrapper>
  );
};

const parameters = {
  docs: {
    description: {
      story: 'Variations of CustomToggleButton',
    },
  },
};
export const CustomToggleButtonLight: Story = {
  render: () => <VariantExamples />,
  tags: ['snapshot'],
  parameters,
};
export const CustomToggleButtonDark: Story = {
  render: () => <VariantExamples isDark />,
  tags: ['dark', 'snapshot'],
  parameters,
};
