/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { ErrorBoundary } from './ErrorBoundary';
import { SharedStoryWrapper } from '../Providers';

// mock window.location
const storedWindowLocation = window.location;

Object.defineProperty(window, 'location', {
  value: { reload: (): void => {} },
});
afterAll(() => {
  Object.defineProperty(window, 'location', storedWindowLocation);
});

const TestComponent: React.FC<Record<string, unknown>> = () => {
  const [hasError, triggerError] = React.useState(false);

  if (hasError) {
    throw new Error('Triggered error');
  }
  return (
    <button
      type="button"
      data-testid="test"
      onClick={(): void => triggerError(true)}
    >
      Click to trigger error
    </button>
  );
};

describe('components/ErrorBoundary/ErrorBoundary', () => {
  it('should render with default props', () => {
    render(
      <ErrorBoundary>
        <TestComponent />
      </ErrorBoundary>,
    );

    expect(screen.getByTestId('test')).toBeTruthy();
  });

  it('should handle errors', () => {
    // mute errors in console
    const spy = jest.spyOn(console, 'error');
    spy.mockImplementation(() => null);

    const props = {
      onError: jest.fn(),
    };
    render(
      <SharedStoryWrapper>
        <ErrorBoundary {...props}>
          <TestComponent />
        </ErrorBoundary>
      </SharedStoryWrapper>,
    );

    // trigger error
    fireEvent.click(screen.getByTestId('test'));

    expect(
      screen.getByText(
        'Woops, an error has occurred. Please report the error shown below and reload the page if needed.',
      ),
    ).toBeTruthy();
    expect(props.onError).toHaveBeenCalled();

    // restore mute errors in console
    spy.mockRestore();
  });

  it('should reload the page', () => {
    // mute errors in console
    const spy = jest.spyOn(console, 'error');
    spy.mockImplementation(() => null);

    jest.spyOn(window.location, 'reload');

    const props = {
      onError: jest.fn(),
    };
    render(
      <ThemeWrapper>
        <ErrorBoundary {...props}>
          <TestComponent />
        </ErrorBoundary>
      </ThemeWrapper>,
    );

    // trigger error
    fireEvent.click(screen.getByTestId('test'));
    expect(props.onError).toHaveBeenCalled();

    expect(screen.getByText('Reload page')).toBeTruthy();
    fireEvent.click(screen.getByText('Reload page'));
    expect(window.location.reload).toHaveBeenCalled();

    // restore mute errors in console
    spy.mockRestore();
  });
});
