/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import { render, screen } from '@testing-library/react';
import React from 'react';
import { SearchFieldButton } from './SearchFieldButtonContainer';
import { SharedStoryWrapper } from '../Providers';

describe('components/LayerManager/LayerSelect/SearchField/SearchFieldButtonContainer', () => {
  it('should show clear button when typing non-url', () => {
    const clearFn = jest.fn();

    render(
      <SharedStoryWrapper>
        <SearchFieldButton
          setPopupIsOpen={jest.fn()}
          localSearchString="test"
          onClickClear={clearFn}
        />
      </SharedStoryWrapper>,
    );
    const button = screen.getByRole('button');
    expect(button.getAttribute('aria-label')).toBe('Clear');
  });
  it('should show clear and save buttons when typing url', () => {
    const clearFn = jest.fn();
    render(
      <SharedStoryWrapper>
        <SearchFieldButton
          setPopupIsOpen={jest.fn()}
          localSearchString="https://openwms.fmi.fi/geoserver/"
          onClickClear={clearFn}
        />
      </SharedStoryWrapper>,
    );
    const buttons = screen.getAllByRole('button');
    expect(buttons[0].getAttribute('aria-label')).toBe('Clear');
    expect(buttons[1].textContent).toBe('Save');
  });
});
