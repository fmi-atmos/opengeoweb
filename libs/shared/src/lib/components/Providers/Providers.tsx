/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import React from 'react';
import {
  darkTheme,
  lightTheme,
  ThemeProviderProps,
  ThemeWrapper,
} from '@opengeoweb/theme';

import { I18nextProvider } from 'react-i18next';
import { i18n, initSharedI18n } from '../../utils/i18n';

interface SharedTranslationWrapperProps {
  children?: React.ReactNode;
}

interface SharedStoryWrapperProps {
  children: React.ReactNode;
  theme?: 'light' | 'dark';
}

/**
 * A Provider component which provides the GeoWeb theme
 * @param children
 * @returns
 */
export const SharedThemeProvider: React.FC<ThemeProviderProps> = ({
  children,
  theme = lightTheme,
}: ThemeProviderProps) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

export const SharedStoryWrapper: React.FC<SharedStoryWrapperProps> = ({
  children,
  theme = 'light',
}) => {
  const themeToApply = theme === 'light' ? lightTheme : darkTheme;

  return (
    <SharedI18nProvider>
      <SharedThemeProvider theme={themeToApply}>{children}</SharedThemeProvider>
    </SharedI18nProvider>
  );
};

export const SharedI18nProvider: React.FC<SharedTranslationWrapperProps> = ({
  children,
}) => {
  initSharedI18n();
  return <I18nextProvider i18n={i18n}>{children}</I18nextProvider>;
};
