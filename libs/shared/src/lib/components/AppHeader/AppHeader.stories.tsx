/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Menu as MenuIcon } from '@opengeoweb/theme';
import { Box, List, ListItemText, Tabs } from '@mui/material';
import { Meta, StoryObj } from '@storybook/react/*';
import { AppHeader } from '.';
import { UserMenu } from '../UserMenu';
import { CustomToggleButton } from '../CustomToggleButton';
import { CustomIconButton } from '../CustomIconButton';

const meta: Meta<typeof AppHeader> = {
  title: 'components/AppHeader',
  component: AppHeader,
  parameters: {
    docs: {
      description: {
        component: 'The main app header of Geoweb',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof AppHeader>;

const ammount = 100;

const Menu = (): React.ReactElement => (
  <List>
    {[...Array(ammount)].fill(null).map((_, index) => (
      // eslint-disable-next-line react/no-array-index-key
      <ListItemText key={`key-${index}`}>some content</ListItemText>
    ))}
  </List>
);

const WorkspaceTopBar = (): React.ReactElement => (
  <Tabs
    value={0}
    aria-label="workspace-bar-tabs"
    sx={{
      width: '100%',
      minHeight: 'auto',
      '.MuiTabs-flexContainer': {
        flexDirection: 'column',
      },
    }}
  >
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 1,
      }}
      data-testid="workspaceTab"
    >
      <CustomToggleButton
        variant="tool"
        disableRipple
        sx={{
          fontSize: '12px',
          '&&': {
            color: 'geowebColors.buttons.icon.active.color',
            backgroundColor: 'transparent',

            paddingRight: '4px !important',
            display: 'block',
            width: '100%',
            height: 24,
            fontWeight: 500,
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            lineHeight: '1px',
            textAlign: 'left',
            textTransform: 'none',
            '&.MuiButtonBase-root': {
              paddingLeft: '12px',
              border: 'none',
            },
          },
        }}
      >
        My workspace
        <Menu />
      </CustomToggleButton>
      <div>
        <Box
          sx={{
            padding: 1,
            flexGrow: 1,
          }}
        >
          <CustomIconButton
            data-testid="workspaceMenuButton"
            isSelected
            aria-label="workspace menu"
            variant="tool"
          >
            <MenuIcon />
          </CustomIconButton>
        </Box>
      </div>
    </Box>
  </Tabs>
);

export const Component: Story = {
  args: {
    menu: <Menu />,
    userMenu: <UserMenu initials="VL" />,
    workspaceTopBar: <WorkspaceTopBar />,
    title: 'Geoweb',
    onClick: (): void => {},
  },
};

const AppHeaderDemo = (): React.ReactElement => (
  <Box sx={{ height: 240 }}>
    <AppHeader title="GeoWeb" />
    <br />
    <AppHeader title="GeoWeb long title" />
    <br />
    <AppHeader
      title="GeoWeb"
      menu={<Menu />}
      userMenu={<UserMenu initials="VL" />}
      workspaceTopBar={<WorkspaceTopBar />}
    />
    <br />
    <AppHeader
      title="GeoWeb long title"
      menu={<Menu />}
      userMenu={<UserMenu initials="VL" />}
      workspaceTopBar={<WorkspaceTopBar />}
    />
  </Box>
);

const parameters = {
  docs: {
    description: {
      story: 'Variations of AppHeader',
    },
  },
};

export const AppHeaderLight: Story = {
  render: () => <AppHeaderDemo />,
  tags: ['snapshot'],
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68c87ed4860e1d14f5dd/version/632317948eb865a6a93819ea',
      },
    ],
  },
};

export const AppHeaderDark: Story = {
  render: () => <AppHeaderDemo />,
  tags: ['snapshot', 'dark'],
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68cb9e0ee610eaf967ec/version/63231796a76e27a98cdff529',
      },
      {
        name: 'Tablet 768W',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/626bda2135983197253a785c/version/626bda2135983197253a785d',
      },
      {
        name: 'Desktop 1024W',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/626bda207b01e213a265ab89/version/626bda207b01e213a265ab8a',
      },
    ],
  },
};
