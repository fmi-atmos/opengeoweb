/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { ThemeWrapper } from '@opengeoweb/theme';
import AppHeader from './AppHeader';

interface TestComponentProps {
  title: string;
}

const TestComponent: React.FC<TestComponentProps> = ({
  title,
}: TestComponentProps) => <div>{title}</div>;

describe('components/AppHeader', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <ThemeWrapper>
        <AppHeader />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
  });

  it('should render different layout parts', async () => {
    const props = {
      menu: <TestComponent title="menu" />,
      userMenu: <TestComponent title="userMenu" />,
      workspaceTopBar: <TestComponent title="workspaceTopBar" />,
      title: 'test title',
    };
    render(
      <ThemeWrapper>
        <AppHeader {...props} />
      </ThemeWrapper>,
    );

    const title = await screen.findByText(props.title);
    const userMenu = await screen.findByText('userMenu');
    const workspaceTopBar = await screen.findByText('workspaceTopBar');

    expect(title).toBeTruthy();
    expect(userMenu).toBeTruthy();
    expect(workspaceTopBar).toBeTruthy();

    fireEvent.click(screen.getByTestId('menuButton'));

    const menu = await screen.findByText('menu');
    expect(menu).toBeTruthy();
  });

  it('should run optional onClick callback', () => {
    const clickHandler = jest.fn();
    render(
      <ThemeWrapper>
        <AppHeader title="geoweb" onClick={clickHandler} />
      </ThemeWrapper>,
    );
    expect(clickHandler).not.toHaveBeenCalled();
    fireEvent.click(screen.getByText('geoweb'));
    expect(clickHandler).toHaveBeenCalled();
  });
});
