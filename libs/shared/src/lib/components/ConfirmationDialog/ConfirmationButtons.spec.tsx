/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */
import { render, fireEvent, screen } from '@testing-library/react';
import { SharedI18nProvider, SharedThemeProvider } from '../Providers';
import {
  ConfirmationButtonsProps,
  ConfirmationButtons,
} from './ConfirmationButtons';
import { translateKeyOutsideComponents } from '../../utils/i18n';

describe('src/components/ConfirmationDialog/ConfirmationDialog', () => {
  it('should trigger correct callbacks', () => {
    const props: ConfirmationButtonsProps = {
      onSubmit: jest.fn(),
      onClose: jest.fn(),
    };
    render(
      <SharedI18nProvider>
        <SharedThemeProvider>
          <ConfirmationButtons {...props} />
        </SharedThemeProvider>
      </SharedI18nProvider>,
    );

    // confirm
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('shared-confirmation-submit'),
      ),
    );
    expect(props.onSubmit).toHaveBeenCalled();

    // cancel
    fireEvent.click(
      screen.getByText(
        translateKeyOutsideComponents('shared-confirmation-cancel'),
      ),
    );
    expect(props.onClose).toHaveBeenCalledWith('CANCELLED');
  });

  it('should be able to pass props', () => {
    const props: ConfirmationButtonsProps = {
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      confirmLabel: 'confirm test label',
      cancelLabel: 'cancel test label',
    };
    render(
      <SharedI18nProvider>
        <SharedThemeProvider>
          <ConfirmationButtons {...props} />
        </SharedThemeProvider>
      </SharedI18nProvider>,
    );

    // confirm
    fireEvent.click(screen.getByText(props.confirmLabel!));
    expect(props.onSubmit).toHaveBeenCalled();

    // cancel
    fireEvent.click(screen.getByText(props.cancelLabel!));
    expect(props.onClose).toHaveBeenCalledWith('CANCELLED');
  });

  it('should show as loading', () => {
    const props: ConfirmationButtonsProps = {
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      isLoading: true,
    };
    render(
      <SharedI18nProvider>
        <SharedThemeProvider>
          <ConfirmationButtons {...props} />
        </SharedThemeProvider>
      </SharedI18nProvider>,
    );

    expect(screen.getByTestId('confirm-dialog-spinner')).toBeTruthy();
    expect(
      screen.queryByText(
        translateKeyOutsideComponents('shared-confirmation-submit'),
      ),
    ).toBeFalsy();
  });
});
