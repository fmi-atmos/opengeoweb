/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { ModalProps, SxProps, Theme } from '@mui/material';
import { CustomDialog, DialogAction } from '../CustomDialog';
import { useSharedTranslation } from '../../utils/i18n';
import { ConfirmationButtons } from './ConfirmationButtons';

export interface ConfirmationOptions {
  title?: string;
  description?: string;
  catchOnCancel?: boolean;
  confirmLabel?: string;
  cancelLabel?: string;
  content?: React.ReactNode;
}

export type ConfirmationAction = 'CANCELLED' | DialogAction;

export interface ConfirmationDialogProps extends ConfirmationOptions {
  disableAutoFocus?: boolean;
  onSubmit: () => void;
  onClose: (reason: ConfirmationAction) => void;
  isLoading?: boolean;
  open: ModalProps['open'];
  sx?: SxProps<Theme>;
}

export const ConfirmationDialog: React.FC<ConfirmationDialogProps> = ({
  open,
  title,
  description,
  confirmLabel,
  cancelLabel,
  onSubmit,
  onClose,
  content = null,
  disableAutoFocus = false,
  isLoading = false,
  ...other
}: ConfirmationDialogProps) => {
  const { catchOnCancel, ...rest } = other; // remove unwanted props
  const { t } = useSharedTranslation();

  return (
    <CustomDialog
      data-testid="confirmationDialog"
      open={open}
      title={title}
      description={description ?? t('shared-confirmation-description')}
      onClose={onClose}
      actions={
        <ConfirmationButtons
          onClose={onClose}
          cancelLabel={cancelLabel}
          isLoading={isLoading}
          onSubmit={onSubmit}
          disableAutoFocus={disableAutoFocus}
          confirmLabel={confirmLabel}
        />
      }
      {...rest}
    >
      {content}
    </CustomDialog>
  );
};

export default ConfirmationDialog;
