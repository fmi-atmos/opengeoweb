/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import SearchHighlight from './SearchHighlight';

describe('src/components/SearchHighlight', () => {
  it('should not highlight without a match', async () => {
    render(<SearchHighlight text="some test content" search="nonexistent" />);
    expect(screen.getByText('some test content').nodeName.toLowerCase()).toBe(
      'span',
    );
  });

  it('should highlight matching search', async () => {
    render(<SearchHighlight text="some test content" search="test" />);
    expect(screen.getByText('some').nodeName.toLowerCase()).toBe('span');
    expect(screen.getByText('test').nodeName.toLowerCase()).toBe('mark');
    expect(screen.getByText('content').nodeName.toLowerCase()).toBe('span');
  });

  it('should highlight two parts and escape regex characters', async () => {
    render(
      <SearchHighlight
        text="some (other) \test content"
        search="(other) content"
      />,
    );
    expect(screen.getByText('some').nodeName.toLowerCase()).toBe('span');
    expect(screen.getByText('(other)').nodeName.toLowerCase()).toBe('mark');
    expect(screen.getByText('\\test').nodeName.toLowerCase()).toBe('span');
    expect(screen.getByText('content').nodeName.toLowerCase()).toBe('mark');
  });
});
