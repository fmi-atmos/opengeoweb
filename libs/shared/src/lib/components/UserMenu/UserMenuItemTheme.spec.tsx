/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { ThemeTypes } from '@opengeoweb/theme';
import UserMenuItemTheme from './UserMenuItemTheme';
import { SharedStoryWrapper } from '../Providers';

interface TestWrapperProps {
  children?: React.ReactNode;
}
const TestWrapper: React.FC<TestWrapperProps> = ({
  children,
}: TestWrapperProps) => <SharedStoryWrapper>{children}</SharedStoryWrapper>;

describe('components/UserMenu/UserMenuItemTheme', () => {
  it('should change theming', () => {
    render(
      <TestWrapper>
        <UserMenuItemTheme />
      </TestWrapper>,
    );

    const lightButton: HTMLInputElement = screen.getByRole('radio', {
      name: /Light/i,
    });
    const darkButton: HTMLInputElement = screen.getByRole('radio', {
      name: /Dark/i,
    });

    expect(lightButton.checked).toBe(true);
    expect(darkButton.checked).toBe(false);

    // toggle to dark
    fireEvent.click(darkButton);
    expect(lightButton.checked).toBe(false);
    expect(darkButton.checked).toBe(true);

    // toggle back to light
    fireEvent.click(lightButton);
    expect(lightButton.checked).toBe(true);
    expect(darkButton.checked).toBe(false);
  });

  it('should show not special themes when GW_FEATURE_ENABLE_SPECIAL_THEMES is false in config  ', () => {
    const config = {
      GW_FEATURE_ENABLE_SPECIAL_THEMES: false,
    };
    render(
      <TestWrapper>
        <UserMenuItemTheme
          enableSpecialThemes={config.GW_FEATURE_ENABLE_SPECIAL_THEMES}
        />
      </TestWrapper>,
    );

    const themeButtons = screen.getAllByRole('radio');

    // only light and dark themes should be visible
    expect(themeButtons.length).toBe(2);
  });

  it('should show special themes when GW_FEATURE_ENABLE_SPECIAL_THEMES is true in config  ', () => {
    const config = {
      GW_FEATURE_ENABLE_SPECIAL_THEMES: true,
    };
    render(
      <TestWrapper>
        <UserMenuItemTheme
          enableSpecialThemes={config.GW_FEATURE_ENABLE_SPECIAL_THEMES}
        />
      </TestWrapper>,
    );

    const themeButtons = screen.getAllByRole('radio');

    // light, dark and special themes should be visible
    expect(themeButtons.length).toBe(Object.keys(ThemeTypes).length);
  });
});
