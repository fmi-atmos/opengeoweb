/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { Typography, Box } from '@mui/material';
import { Add } from '@opengeoweb/theme';
import type { Meta, StoryObj } from '@storybook/react';
import { ToggleMenu } from './ToggleMenu';

const iconsMap = {
  add: <Add />,
};
const meta: Meta<typeof ToggleMenu> = {
  title: 'components/ToggleMenu',
  component: ToggleMenu,
  argTypes: {
    buttonIcon: {
      control: 'select',
      options: [undefined, ...Object.keys(iconsMap)],
      mapping: {
        undefined,
        ...iconsMap,
      },
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'A ToggleMenu component.',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof ToggleMenu>;

export const Component: Story = {
  args: {
    isDefaultOpen: false,
    menuItems: [
      { text: 'Without icon', action: (): void => {} },
      { text: 'With icon', action: (): void => {}, icon: <Add /> },
    ],
    children: '',
    menuTitle: 'menu title',
    isDisabled: false,
    tooltipTitle: 'tooltip title',
  },
};

const ExampleMenu = (): React.ReactElement => (
  <div
    style={{
      margin: '10px',
      padding: '20px 0px',
      width: '350px',
      height: '350px',
    }}
  >
    <div style={{ position: 'absolute', left: '300px', top: '100px' }}>
      <ToggleMenu
        isDefaultOpen
        tooltipTitle="example toggle menu tooltip"
        menuItems={[
          { text: 'Without icon', action: (): void => {} },
          { text: 'With icon', action: (): void => {}, icon: <Add /> },
          { text: 'With icon 2', action: (): void => {}, icon: <Add /> },
        ]}
        menuTitle="This is the title"
      />
    </div>
    <div style={{ position: 'absolute', left: '300px', top: '200px' }}>
      <ToggleMenu
        variant="flat"
        menuPosition="right"
        tooltipTitle="example toggle menu tooltip"
        menuItems={[
          { text: 'Button variant flat', action: (): void => {} },
          { text: 'Opens to the right', action: (): void => {} },
        ]}
        menuTitle="This is the title"
      />
    </div>
    <div style={{ position: 'absolute', left: '300px', top: '300px' }}>
      <ToggleMenu
        menuPosition="bottom"
        tooltipTitle="example toggle menu tooltip"
      >
        <Box sx={{ padding: '8px' }}>
          <Typography variant="h4">Custom content</Typography>
          <Typography variant="body1">Menu opens to the bottom</Typography>
        </Box>
      </ToggleMenu>
    </div>
  </div>
);

export const ToggleMenuLight: Story = {
  render: ExampleMenu,
  tags: ['snapshot', '!autodocs'],
};

export const ToggleMenuDark: Story = {
  render: ExampleMenu,
  tags: ['dark', 'snapshot', '!autodocs'],
};
