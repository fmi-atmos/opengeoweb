/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';

import type { Meta, StoryObj } from '@storybook/react';
import Filter from './Filter';
import { ListFilter } from './FilterList';
import { StorybookDocsWrapper } from '../Storybook';

const meta: Meta<typeof Filter> = {
  title: 'components/Filter',
  component: Filter,
  parameters: {
    docs: {
      description: {
        component: 'A component for showing the user Filter',
      },
    },
  },
};
export default meta;

type Story = StoryObj<typeof Filter>;

export const Component: Story = {
  args: {
    filters: [
      {
        label: 'shared-filter-my-presets',
        id: 'user',
        type: 'scope',
        isSelected: false,
        isDisabled: false,
      },
      {
        label: 'shared-filter-system-presets',
        id: 'system',
        type: 'scope',
        isSelected: false,
        isDisabled: true,
      },
    ],
    isAllSelected: true,
    searchQuery: 'My search query',
  },
};

const listFilterOptions: ListFilter[] = [
  {
    label: 'shared-filter-my-presets',
    id: 'user',
    type: 'scope',
    isSelected: false,
    isDisabled: false,
  },
  {
    label: 'shared-filter-system-presets',
    id: 'system',
    type: 'scope',
    isSelected: false,
    isDisabled: false,
  },
  {
    label: 'from backend',
    id: 'system',
    type: 'scope',
    isSelected: false,
    isDisabled: false,
  },
];

const Demo = ({ isDark = false }: { isDark?: boolean }): React.ReactElement => (
  <StorybookDocsWrapper
    isDark={isDark}
    sx={{
      padding: 1,
      width: '400px',
      backgroundColor: 'geowebColors.background.surfaceApp',
    }}
  >
    <Filter filters={listFilterOptions} isAllSelected />
  </StorybookDocsWrapper>
);

const parameters = {
  docs: {
    description: {
      story: 'Variations of Filter',
    },
  },
};

export const FilterLight: Story = {
  render: () => <Demo />,
  tags: ['snapshot'],
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Light theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6405b435ec5def65d04cef1c',
      },
    ],
  },
};

export const FilterDark: Story = {
  render: () => <Demo isDark />,
  tags: ['dark', 'snapshot'],
  parameters: {
    ...parameters,
    zeplinLink: [
      {
        name: 'Dark theme',
        link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d7b070d54ce81683e9c35b',
      },
    ],
  },
};
