/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import React from 'react';
import { render } from '@testing-library/react';

import StatusTag from './StatusTag';

describe('components/StatusTag', () => {
  it('should render successfully', () => {
    const props = {
      content: 'All good',
      color: 'green',
    };
    const { baseElement } = render(<StatusTag {...props} />);
    expect(baseElement).toBeTruthy();
  });
  it('should contain the passed text', () => {
    const props = {
      content: 'All good',
      color: 'green',
    };
    const { baseElement } = render(<StatusTag {...props} />);
    expect(baseElement.textContent).toBe(props.content);
  });
  it('should contain the passed text 2', () => {
    const props = {
      content: 'Cancelled',
      color: 'green',
    };
    const { baseElement } = render(<StatusTag {...props} />);
    expect(baseElement.textContent).toBe(props.content);
  });
});
