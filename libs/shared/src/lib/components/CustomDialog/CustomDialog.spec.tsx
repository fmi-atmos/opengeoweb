/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
import * as React from 'react';
import { render, fireEvent, screen, within } from '@testing-library/react';
import { CustomDialog, CustomDialogProps } from './CustomDialog';
import { SharedI18nProvider, SharedThemeProvider } from '../Providers';

describe('src/components/CustomDialog/CustomDialog', () => {
  it('should close the dialog', () => {
    const props: CustomDialogProps = {
      open: true,
      title: 'Title',
      onClose: jest.fn(),
    };
    render(
      <SharedThemeProvider>
        <CustomDialog {...props} />
      </SharedThemeProvider>,
    );

    expect(screen.getByRole('dialog')).toBeTruthy();
    expect(screen.getByTestId('customDialog-title')).toBeTruthy();
    fireEvent.click(screen.getByTestId('customDialog-close'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onClose).toHaveBeenCalledWith('CLOSED');
  });

  it('should show the given title and description', async () => {
    const props: CustomDialogProps = {
      open: true,
      title: 'Title',
      description: 'This is a test description',
      onClose: jest.fn(),
    };
    render(
      <SharedThemeProvider>
        <CustomDialog {...props} />
      </SharedThemeProvider>,
    );

    expect(await screen.findByText(props.description!)).toBeTruthy();
    expect(screen.getByTestId('customDialog-title').textContent).toEqual(
      props.title,
    );
  });

  it('should be able to close on ESC key', () => {
    const props: CustomDialogProps = {
      open: true,
      title: 'dialog',
      onClose: jest.fn(),
    };
    render(
      <SharedThemeProvider>
        <CustomDialog {...props} />
      </SharedThemeProvider>,
    );
    // close
    fireEvent.keyDown(screen.getByTestId('customDialog'), {
      code: 'Escape',
      key: 'Escape',
    });
    expect(props.onClose).toHaveBeenCalledWith('CLOSED');
  });

  it('should be able to pass children', () => {
    const TestComponent: React.FC = () => (
      <p data-testid="test-component">Testing</p>
    );
    const props: CustomDialogProps = {
      open: true,
      title: 'dialog',
      onClose: jest.fn(),
    };
    render(
      <SharedThemeProvider>
        <CustomDialog {...props}>
          <TestComponent />
        </CustomDialog>
      </SharedThemeProvider>,
    );
    expect(screen.getByTestId('test-component')).toBeTruthy();
  });

  it('should be able to pass action buttons', () => {
    const TestComponent: React.FC = () => (
      <button type="submit" data-testid="confirm">
        Testing
      </button>
    );
    const props: CustomDialogProps = {
      open: true,
      title: 'dialog',
      onClose: jest.fn(),
      actions: <TestComponent />,
    };
    render(
      <SharedThemeProvider>
        <CustomDialog {...props} />
      </SharedThemeProvider>,
    );
    expect(screen.getByTestId('confirm')).toBeTruthy();
  });
  it('should apply passed styling to the dialog', () => {
    const TestComponent: React.FC = () => (
      <button type="submit" data-testid="confirm">
        Testing
      </button>
    );
    const widthStyle = { width: '1000px' };
    const props: CustomDialogProps = {
      open: true,
      title: 'dialog',
      onClose: jest.fn(),
      sx: widthStyle,
      actions: <TestComponent />,
    };
    render(
      <SharedThemeProvider>
        <CustomDialog {...props} />
      </SharedThemeProvider>,
    );
    const styles = getComputedStyle(screen.getByRole('dialog'));
    expect(styles.width).toEqual(widthStyle.width);
  });

  it('be resizable', async () => {
    const props = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      isResizable: true,
    };
    render(
      <SharedThemeProvider>
        <SharedI18nProvider>
          <CustomDialog {...props} />
        </SharedI18nProvider>
      </SharedThemeProvider>,
    );

    const resizeHandles = await screen.findAllByTestId('resizeHandle');
    expect(resizeHandles).toHaveLength(2);

    const dialog = within(screen.getByTestId('customDialog')).getByRole(
      'presentation',
    ).firstChild as HTMLElement;

    expect(getComputedStyle(dialog).height).toEqual('auto');
    expect(getComputedStyle(dialog).width).toEqual('auto');

    // // Test both handles
    fireEvent.mouseDown(resizeHandles[0]);
    fireEvent.mouseUp(resizeHandles[0]);

    expect(getComputedStyle(dialog).height).toEqual('0px');
    expect(getComputedStyle(dialog).width).toEqual('0px');
  });

  it('be resizable with resizableDefaultSize', async () => {
    const props = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      isResizable: true,
      resizableDefaultSize: { width: '200px', height: '300px' },
    };
    render(
      <SharedThemeProvider>
        <SharedI18nProvider>
          <CustomDialog {...props} />
        </SharedI18nProvider>
      </SharedThemeProvider>,
    );

    const resizeHandles = await screen.findAllByTestId('resizeHandle');
    expect(resizeHandles).toHaveLength(2);

    const dialog = within(screen.getByTestId('customDialog')).getByRole(
      'presentation',
    ).firstChild as HTMLElement;

    expect(getComputedStyle(dialog).height).toEqual('300px');
    expect(getComputedStyle(dialog).width).toEqual('200px');
  });

  it('have a custom layout', async () => {
    const TestComponent = (): React.ReactElement => <p>test component</p>;
    const props = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      hasLayout: false,
      children: <TestComponent />,
    };
    render(
      <SharedThemeProvider>
        <SharedI18nProvider>
          <CustomDialog {...props} />
        </SharedI18nProvider>
      </SharedThemeProvider>,
    );

    expect(screen.queryByTestId('customDialog-title')).toBeFalsy();
    expect(screen.queryByTestId('customDialog-close')).toBeFalsy();
    expect(screen.getByText('test component')).toBeTruthy();
  });

  it('render the dialog default in the body', async () => {
    const props = {
      open: true,
      onSubmit: jest.fn(),
      onClose: jest.fn(),
    };
    render(
      <SharedThemeProvider>
        <SharedI18nProvider>
          <div data-testid="parent-wrapper">
            <CustomDialog {...props} />
          </div>
        </SharedI18nProvider>
      </SharedThemeProvider>,
    );

    expect(screen.getByTestId('parent-wrapper').childNodes).toHaveLength(0);
  });

  it('render the dialog inline', async () => {
    const TestComponent = (): React.ReactElement => <p>test component</p>;
    const props = {
      open: true,
      title: 'dialog',
      onSubmit: jest.fn(),
      onClose: jest.fn(),
      children: <TestComponent />,
      shouldRenderInline: true,
    };
    render(
      <SharedThemeProvider>
        <SharedI18nProvider>
          <div data-testid="parent-wrapper">
            <CustomDialog {...props} />
          </div>
        </SharedI18nProvider>
      </SharedThemeProvider>,
    );

    expect(screen.getByTestId('parent-wrapper').childNodes).toHaveLength(1);
  });
});
