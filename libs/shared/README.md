![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/shared/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-shared)

# Shared

Library with shared components, hooks and utils for the opengeoweb project.
This library was generated with [Nx](https://nx.dev).

## Rules for contributing

- Only add components/utils/hooks that are used in at least 2 other libraries.
- Provide a storybook demo
- Provide unit tests with 100% code coverage

## Usage

```
import { Example } from '@opengeoweb/shared';
```

## Running unit tests

Run `nx test shared` to execute the unit tests via [Jest](https://jestjs.io).

### TypeScript Documentation

- [TypeScript Docs](https://opengeoweb.gitlab.io/opengeoweb/typescript-docs/shared/)
