/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
const nxPreset = require('@nx/jest/preset').default;

const esModules = [
  'frappe-gantt',
  'react-dnd',
  'dnd-core',
  '@react-dnd',
  'rdndmb-html5-to-touch',
  'dnd-multi-backend',
  'ol/',
  'ol',
  'pbf',
  'quick-lru',
  'quickselect',
  'rbush',
  'earcut',
  'lodash-es',
  'color-(space|parse|rgba|name)',
].join('|');

module.exports = {
  ...nxPreset,
  testEnvironment: 'jest-fixed-jsdom',
  transformIgnorePatterns: [`/node_modules/(?!${esModules})`],
  transform: {
    '^(?!.*\\.(js|jsx|ts|tsx|css|json)$)': '@nx/react/plugins/jest',
    '^.+\\.svg$': 'jest-transform-stub',
    '^.+\\.[tj]sx?$': [
      'babel-jest',
      { cwd: __dirname, configFile: './babel.config.json' },
    ],
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
  coverageReporters: ['text', 'html'],
  clearMocks: true,
  testTimeout: 50000,
  setupFilesAfterEnv: ['../../setup-jest.ts'],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: '<rootDir>',
        classNameTemplate: '{classname}',
        titleTemplate: '{title}',
        ancestorSeparator: ' / ',
        includeConsoleOutput: true,
        usePathForSuiteName: 'true',
      },
    ],
  ],
  modulePathIgnorePatterns: ['<rootDir>/dist'],
};
