// @ts-check

const fs = require('node:fs/promises');
const path = require('node:path');
const { exec } = require('node:child_process');
const { promisify } = require('node:util');
const assert = require('node:assert');

const execPromise = promisify(exec);

/**
 * @typedef Template
 * @type {object}
 * @property {{(values: object): string}} replace
 */

/**
 * @typedef {(project: string, template: Template) => Promise<string | string[]>} JobGenerator
 */

/** @type {{ head?: string, base: string }} */
let diffBranches;

(async () => {
  diffBranches = await getDiffBranches();
  console.log('NX_BASE', diffBranches.base);
  console.log('NX_HEAD', diffBranches.head);
  await buildDynamicGitlabYml();
  await buildEnv();
})();

async function buildDynamicGitlabYml() {
  const [testJobs, snapshotJobs, e2eJobs] = await Promise.all([
    createJobsForAffectedProject('test', 'templates/test.yml', simpleJob),
    createJobsForAffectedProject(
      'snapshot',
      'templates/snapshot.yml',
      simpleJob,
    ),
    createJobsForAffectedProject('e2e', 'templates/e2e.yml', e2eJobSplitter(3)),
  ]);

  const template = await loadTemplate('templates/static-gitlab-ci.yml');

  const pipeline = template.replace({
    'test-jobs': testJobs,
    'snapshot-jobs': snapshotJobs,
    'e2e-jobs': e2eJobs,
  });

  await fs.writeFile('dynamic-pipeline.yml', pipeline);
}

/**
 * Check whether the commit is available in the pipeline.
 * @param {string} sha
 * @returns {Promise<boolean>}
 */
async function hasCommit(sha) {
  try {
    await execPromise(`git rev-list ${sha}..HEAD`);
    return true;
  } catch (e) {
    console.log(e);
    return false;
  }
}

/**
 * Retrieve the last successful pipeline which we can use as the base branch to compare against.
 * This is better than the previous commit as the pipeline for that commit might have failed.
 * @returns {Promise<string | null>}
 */
async function getLastSuccessfulPipelineSha() {
  const projectId = process.env.CI_PROJECT_ID;
  const branch = process.env.CI_COMMIT_BRANCH;
  const token = process.env.API_READ_TOKEN;

  assert(token, 'Missing API_READ_TOKEN env');

  try {
    const pipelines = await fetch(
      new Request(
        `https://gitlab.com/api/v4/projects/${projectId}/pipelines?ref=${branch}&status=success&per_page=1`,
        {
          headers: {
            'PRIVATE-TOKEN': token,
          },
        },
      ),
    ).then((res) => {
      if (!res.ok) {
        throw new Error('Unable to retrieve pipelines for branch');
      }
      return res.json();
    });

    return pipelines?.length ? pipelines[0].sha : null;
  } catch (e) {
    console.warn(e.message);
    return null;
  }
}

/**
 * Returns the base and head variables that will be used to determine the affected targets
 * @returns {Promise<{base: string, head?: string}>}
 */
async function getDiffBranches() {
  if (
    (process.env.CI_COMMIT_BRANCH &&
      process.env.CI_COMMIT_BRANCH === process.env.CI_DEFAULT_BRANCH) ||
    process.env.CI_COMMIT_TAG
  ) {
    return {
      base: 'remotes/origin/master~1',
      head: 'remotes/origin/master',
    };
  } else {
    let baseSha = await getLastSuccessfulPipelineSha();

    // fix for the bad-object error. Fallback to master branch
    if (baseSha) {
      const isValidCommit = await hasCommit(baseSha);
      if (!isValidCommit) {
        console.warn(`Invalid commit: ${baseSha}. Reverting to master`);
        baseSha = null;
      }
    }

    return {
      base: process.env.NX_BASE ?? baseSha ?? 'remotes/origin/master',
      head: process.env.CI_COMMIT_SHA,
    };
  }
}

/**
 * Build the build.env file that is used to pass some environmental variables to the dynamic pipeline
 */
async function buildEnv() {
  const lines = [`NX_BASE=${diffBranches.base}`];

  if (diffBranches.head) {
    lines.push(`NX_HEAD=${diffBranches.head}`);
  }

  await fs.writeFile('build.env', lines.join('\n'));
}

/**
 * Generate gitlab jobs for each affected project that has the given target
 * @param {string} target
 * @param {string} templateName
 * @param {JobGenerator} jobGenerator runs this function for each project and should return one or more jobs
 * @returns
 */
async function createJobsForAffectedProject(
  target,
  templateName,
  jobGenerator,
) {
  const [projects, template] = await Promise.all([
    getAffectedProjectsForTarget(target),
    loadTemplate(templateName),
  ]);
  let jobs = [];
  for (const project of projects) {
    const newJobs = await jobGenerator(project, template);
    jobs = [...jobs, ...newJobs];
  }
  return jobs.join('\n\n');
}

/**
 * Returns the projects that have the given target and were affected
 * @param {string} requiredTarget
 * @returns {Promise<string>}
 */
async function getAffectedProjectsForTarget(requiredTarget) {
  const cmd = `./node_modules/.bin/nx show projects --affected --json --base=${diffBranches.base} ${diffBranches.head ? `--head=${diffBranches.head}` : ''} --with-target=${requiredTarget}`;
  console.log('Run command', cmd);

  const { stdout } = await execPromise(cmd);
  console.log(
    'Got the following affected projects for target',
    requiredTarget,
    stdout.toString(),
  );
  return JSON.parse(stdout.toString());
}

/**
 * @param {string} project
 * @param {Template} template
 * @returns {Promise<string[]>}
 */
async function simpleJob(project, template) {
  return [template.replace({ project })];
}

/**
 * Returns a function that splits up the cypress specs for the given project into chunks of the given size
 * It returns a cypress job that will only test those chunks
 * @param {number} splitAmount
 * @returns {JobGenerator}
 */
function e2eJobSplitter(splitAmount) {
  return async (project, template) => {
    const specFiles = await getCypressSpecFiles(project);
    const chunkedSpecFiles = chunkArray(specFiles, splitAmount);
    return chunkedSpecFiles.map((files, index) => {
      return template.replace({
        project,
        files: files.join(','),
        num: index + 1,
        total: chunkedSpecFiles.length,
      });
    });
  };
}

/**
 * Simple templating util to replace {{inline}} or ##block## vars in yaml
 * @param {string} name
 * @returns {Promise<Template>}
 */
async function loadTemplate(name) {
  const templateContents = await fs.readFile(
    path.posix.join(__dirname, name),
    'utf8',
  );
  return {
    replace(values) {
      let replaced = templateContents;
      for (let [name, value] of Object.entries(values)) {
        replaced = replaced
          // ignore because ts complains about not supporting replaceAll here
          // @ts-ignore
          .replaceAll(`{{${name}}}`, value)
          .replaceAll(`##${name}##`, value);
      }
      return replaced;
    },
  };
}

/**
 * Retrieve all the cypress spec files for the given project
 * @param {string} project
 * @returns {Promise<string[]>}
 */
async function getCypressSpecFiles(project) {
  const { stdout } = await execPromise(`find apps/${project} -name "*.cy.ts"`);
  const specFiles = stdout.split('\n');
  specFiles.pop();
  return specFiles;
}

/**
 * Split an array in the given amount of chunks
 * @param {any[]} array
 * @param {number} chunkAmount
 * @returns {any[][]}
 */
function chunkArray(array, chunkAmount) {
  let chunks = [];
  const chunkSize = Math.ceil(array.length / chunkAmount);
  for (let i = 0; i < array.length; i += chunkSize) {
    chunks.push(array.slice(i, i + chunkSize));
  }
  return chunks;
}
