/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * Copyright 2024 - The Norwegian Meteorological Institute (MET Norway)
 * */
// @ts-check

const { createCustomRunner } = require('nx-remotecache-custom');
const {
  S3Client,
  HeadObjectCommand,
  GetObjectCommand,
  NotFound,
} = require('@aws-sdk/client-s3');
const { Upload } = require('@aws-sdk/lib-storage');
const assert = require('assert');
const { join } = require('path/posix');
const { fromNodeProviderChain } = require('@aws-sdk/credential-providers');

/**
 * @typedef CustomRunnerOptions
 * @property {string} [endpointUrl]
 * @property {string} [keyPrefix]
 * @property {string} bucket
 */

module.exports = createCustomRunner(
  /**
   *
   * @param {import('nx-remotecache-custom').CustomRunnerOptions<CustomRunnerOptions>} options
   * @returns
   */
  async (options) => {
    const endpointUrl =
      options.endpointUrl ?? process.env.S3_CACHE_ENDPOINT_URL;

    const keyPrefix =
      options.keyPrefix ?? process.env.S3_CACHE_KEY_PREFIX ?? '';
    const bucket = options.bucket ?? process.env.S3_CACHE_BUCKET;

    const readFromCache =
      options.read !== false && process.env.S3_CACHE_FORCE_REFRESH !== 'true';

    assert(bucket, 'Bucket should be defined');

    console.log('s3-cache-runner initialized');
    console.log('s3-cache-runner -> writing to bucket:', bucket);
    console.log('s3-cache-runner -> prefix:', keyPrefix);

    if (!readFromCache) {
      console.log('s3-cache-runner -> not reading from cache');
    }

    const client = new S3Client({
      endpoint: endpointUrl,
      credentials: fromNodeProviderChain({
        profile: process.env.S3_CACHE_AWS_PROFILE,
      }),
      region: process.env.AWS_REGION ?? 'us-east-1', // without region the s3 client fails even though it should not be needed for custom endpoint
      forcePathStyle: true,
    });

    /**
     * @param {string} key
     * @returns {string}
     */
    const keyWithPrefix = (key) => join(keyPrefix, key);

    return {
      name: 's3-cache-runner',

      /**
       * @param {string} filename
       * @returns {Promise<boolean>}
       */
      async fileExists(filename) {
        if (!readFromCache) {
          return false;
        }

        try {
          await client.send(
            new HeadObjectCommand({
              Bucket: bucket,
              Key: keyWithPrefix(filename),
            }),
          );
        } catch (error) {
          if (error instanceof NotFound || error.name === '403') {
            return false;
          }
          throw error;
        }
        return true;
      },
      /**
       *
       * @param {string} filename
       * @returns {Promise<import('stream').Readable>}
       */
      async retrieveFile(filename) {
        const result = await client.send(
          new GetObjectCommand({
            Bucket: bucket,
            Key: keyWithPrefix(filename),
          }),
        );
        // @ts-ignore
        return result.Body;
      },
      /**
       *
       * @param {string} filename
       * @param {import('stream').Readable} data
       */
      async storeFile(filename, data) {
        if (options.write === false) {
          return;
        }

        console.log('uploading to', keyWithPrefix(filename));

        const upload = new Upload({
          client,
          params: {
            Bucket: bucket,
            Key: keyWithPrefix(filename),
            Body: data,
          },
        });

        await upload.done();
      },
    };
  },
);
