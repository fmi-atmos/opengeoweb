/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2025 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2025 - Finnish Meteorological Institute (FMI)
 * Copyright 2025 - The Norwegian Meteorological Institute (MET Norway)
 * */

/**
 * This script is used when building NPM releases. It uses the NX tools to prepare package.json
 * and package-lock.json files. The resulting files will:
 *  - have the same version number as /package.json
 *  - dependencies will only list external dependencies
 *  - the fields `description`, `license`, and `repository` will be copied from the original
 *  - all other fields (including `devDependencies` will be removed)
 *
 * The logice is adapted from NX docs: https://nx.dev/ci/recipes/other/ci-deployment#programmatic-usage
 **/

const {
  createProjectGraphAsync,
  readCachedProjectGraph,
  detectPackageManager,
  writeJsonFile,
  readJsonFile,
} = require('@nx/devkit');
const {
  createLockFile,
  createPackageJson,
  getLockFileName,
} = require('@nx/js');
const { writeFileSync } = require('fs');
const _ = require('lodash');

const PACKAGE_JSON_FIELDS_TO_COPY = ['description', 'license', 'repository'];

function getModuleNames() {
  let projectGraph = readCachedProjectGraph();

  return Object.keys(projectGraph.nodes).filter(
    (moduleName) => projectGraph.nodes[moduleName].type === 'lib',
  );
}

async function main() {
  await createProjectGraphAsync();

  const outputBaseDir = 'libs';

  const moduleNames = await getModuleNames();

  const monorepoVersion = readJsonFile('./package.json').version;
  console.log(
    `Producing package.json and package-lock.json files for all libraries. Setting version to ${monorepoVersion}`,
  );

  await Promise.all(
    moduleNames.map(async (moduleName) => {
      const outputDir = `${outputBaseDir}/${moduleName}`;
      const pm = detectPackageManager();
      let projectGraph = readCachedProjectGraph();

      const originalPackageJsonData = readJsonFile(
        `libs/${moduleName}/package.json`,
      );

      const packageJsonData = createPackageJson(moduleName, projectGraph, {
        isProduction: true,
        root: projectGraph.nodes[moduleName].data.root,
      });

      // modify resulting package.json
      const packageJson = {
        name: `@opengeoweb/${moduleName}`,
        version: monorepoVersion,
        ..._.pick(originalPackageJsonData, ...PACKAGE_JSON_FIELDS_TO_COPY),
        ..._.omit(
          packageJsonData,
          'name',
          'version',
          ...PACKAGE_JSON_FIELDS_TO_COPY,
        ),
      };

      const lockFile = createLockFile(
        packageJson,
        projectGraph,
        detectPackageManager(),
      );

      const lockFileName = getLockFileName(pm);

      writeJsonFile(`${outputDir}/package.json`, packageJson);
      writeFileSync(`${outputDir}/${lockFileName}`, lockFile, {
        encoding: 'utf8',
      });
    }),
  );
}

main();
